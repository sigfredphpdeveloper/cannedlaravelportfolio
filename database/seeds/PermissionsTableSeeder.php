<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('permissions')->truncate();

        DB::table('permissions')->insert([
            'feature' => 'Edit Company / Role / Team',
            'key'=>'edit_company_role_team'
        ]);

        DB::table('permissions')->insert([
            'feature' => 'View Company / Role / Team',
            'key'=>'view_company_role_team'
        ]);

        DB::table('permissions')->insert([
            'feature' => 'Assign Roles',
            'key'=>'assign_roles'
        ]);

        DB::table('permissions')->insert([
            'feature' => 'Assign Teams',
            'key'=>'assign_teams'
        ]);

        DB::table('permissions')->insert([
            'feature' => 'Edit Employees',
            'key'=>'edit_employees'
        ]);

        DB::table('permissions')->insert([
            'feature' => 'Edit General Permissions',
            'key'=>'edit_general_permissions'
        ]);

        DB::table('permissions')->insert([
            'feature' => 'Manage Announcement',
            'key'=>'manage_announcement'
        ]);

        DB::table('permissions')->insert([
            'feature' => 'Manage Contacts',
            'key'=>'manage_contacts'
        ]);

        DB::table('permissions')->insert([
            'feature' => 'View Emergency Contacts',
            'key'=>'view_emergency_contacts'
        ]);

        DB::table('permissions')->insert([
            'feature' => 'Edit Emergency Contacts',
            'key'=>'edit_emergency_contacts'
        ]);

        DB::table('permissions')->insert([
            'feature' => 'View Expenses',
            'key'=>'view_expenses'
        ]);

        DB::table('permissions')->insert([
            'feature' => 'Manage Trainings',
            'key'=>'manage_trainings'
        ]);

        DB::table('permissions')->insert([
            'feature' => 'View Recognition Board',
            'key'=>'view_recognition_board'
        ]);

        DB::table('permissions')->insert([
            'feature' => 'View Asset Management',
            'key'=>'view_asset_management'
        ]);

        DB::table('permissions')->insert([
            'feature' => 'Edit Asset Management',
            'key'=>'edit_asset_management'
        ]);


    }
}
