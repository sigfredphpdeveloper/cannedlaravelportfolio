<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeRecognitionBoard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_recognition_board', function(Blueprint $table){
            $table->increments('id');
            $table->integer('owner_id');
            $table->integer('employee_id');
            $table->integer('company_id');
            $table->string('title', 150);
            $table->string('message', 300);
            $table->enum('status', array('visible', 'hidden', 'deleted'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employee_recognition_board');
    }
}
