<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCpfRecordInPayrollDeductionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $companies = DB::table('company')->where('Country','SG')->get();

        foreach($companies as $company){

            $insert = DB::table('payroll_deductions')->insert([
                'company_id' => $company->id,
                'title' => 'Employee\'s CPF deduction',
                'cpf_deductible' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'default_deduction' => 1
            ]);

        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('payroll_deductions')->where('title',"Employee\'s CPF deduction")->delete();
    }
}
