<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDefaultEmailsv4 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $template1 = [
            'subject'=>'Leave status updated to ##status##',
            'body'=>'
                <p>Dear ##applicant##,</p>

                <p>Administrator of your company (##company_name##) has updated your leave status</p>

                <p>Applicant Name: ##applicant##</p>
                ##leave_details##

                 <br />
                ',
            'slug'=>'leave_notification_status'
        ];

        $query = DB::table('email_templates')->insert($template1);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('email_templates')->where('slug', 'leave_notification_status')->delete();
    }
}
