<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPayrollAddMonths extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::table('sg_payroll', function (Blueprint $table) {
            $table->date('payment_date');
            $table->integer('month');
            $table->integer('year');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sg_payroll', function (Blueprint $table) {
            $table->dropColumn('payment_date');
            $table->dropColumn('month');
            $table->dropColumn('year');
        });
    }
}
