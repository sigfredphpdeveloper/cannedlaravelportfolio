<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSgPayrollItemAdditionDeduction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sg_payroll_item_deduction', function (Blueprint $table) {
            $table->string('title');
        });

        Schema::table('sg_payroll_item_addition', function (Blueprint $table) {
            $table->string('title');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sg_payroll_item_deduction', function (Blueprint $table) {
            $table->dropColumn('title');
        });

        Schema::table('sg_payroll_item_addition', function (Blueprint $table) {
            $table->dropColumn('title');
        });
    }
}
