<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSgPayrollItems2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sg_payroll_item', function (Blueprint $table) {
            $table->integer('payroll_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sg_payroll_item', function (Blueprint $table) {
            $table->dropColumn('payroll_id');
        });
    }
}
