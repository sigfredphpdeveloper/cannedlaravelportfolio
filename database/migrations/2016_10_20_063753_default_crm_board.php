<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DefaultCrmBoard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // get company
        // get user from compay
        $companies = DB::table('company')->join('company_user','company_user.company_id','=','company.id')->where('company_user.is_master',1)->groupBy('company.id')->get();

        foreach($companies as $company){

            $get_default = DB::table('crm_board')->where('board_title','Sales Funnel')->where('company_id', $company->company_id)->first();

            if(empty($get_default)){

                // create default board for old crm
                $board_id = DB::table('crm_board')->insertGetId(
                    [
                        'board_title' => 'Sales Funnel', 'company_id' => $company->company_id,'owner_id' => $company->user_id,
                        'visibility' => 'everyone','created_date' => date('Y-m-d H:i:s'),'last_update_date' => '0000-00-00 00:00:00'
                    ]
                );

                DB::table('stages')->where('company_id', $company->company_id)->whereNull('crm_board_id')
                    ->update(['crm_board_id'=>$board_id]);// set board_id

                DB::table('deals')
                    ->join('contacts','contacts.id','=','deals.contact_id')
                    ->join('organization', 'contacts.organization_id','=','organization.id')
                    ->where('organization.company_id',$company->company_id)
                    ->whereNull('deals.crm_board_id')
                    ->update(['crm_board_id'=>$board_id]);

            }

        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $default_boards = DB::table('crm_board')->where('board_title','Sales Funnel')->get();

        foreach($default_boards as $board){

            DB::table('deals')
                ->join('contacts','contacts.id','=','deals.contact_id')
                ->join('organization', 'contacts.organization_id','=','organization.id')
                ->where('deals.crm_board_id',$board->id)
                ->update(['crm_board_id'=>null]);

            DB::table('stages')->where('crm_board_id', $board->id)->update(['crm_board_id'=>null]);
            DB::table('crm_board')->where('id', $board->id)->delete();

        }

    }
}
