<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDefaultEmailsv5 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $template7 = [
            'subject'=>'Join our ##company_name## intranet',
            'body'=>'
               <p>Dear ##invitee_first_name##, </p>
                 <br />
                 <p>
                Welcome to Zenintranet </p>
                 <p>
                 ##invitor_name## invited you to join the company ##company_name## as a GUEST...
                 </p>
                 <p>
                Please click on the link below to create a password for your account (username ##your_username##)
                 </p>
                <p>##link##</p>
                ',

            'slug'=>'invite_guest'
        ];

        $query = DB::table('email_templates')->insert($template7);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('email_templates')->where('slug', 'invite_guest')->delete();
    }
}
