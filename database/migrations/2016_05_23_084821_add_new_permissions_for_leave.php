<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewPermissionsForLeave extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('permissions')->insert([
            'feature' => 'Manage Leaves',
            'key'=>'manage_leaves'
        ]);

        DB::table('permissions')->insert([
            'feature' => 'Approve Leaves',
            'key'=>'approve_leaves'
        ]);

        DB::table('permissions')->insert([
            'feature' => 'View Leaves',
            'key'=>'view_leaves'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
