<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrmBoard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_board', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->integer('owner_id');
            $table->string('board_title');
            $table->string('visibility');
            $table->dateTime('created_date');
            $table->dateTime('last_update_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('crm_board');
    }
}
