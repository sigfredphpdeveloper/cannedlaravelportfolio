<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSgCpfTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sg_cpf', function (Blueprint $table) {
            $table->increments('id');
            $table->string('employee_category');
            $table->integer('min_age');
            $table->integer('max_age');
            $table->string('employer_rate');
            $table->string('employee_rate');
            $table->string('discount_rate');
            $table->integer('threshold1');
            $table->integer('threshold2');
            $table->integer('threshold3');
            $table->integer('ow_ceiling');
            $table->timestamps();
        });

        DB::table('sg_cpf')->insert([
                'employee_category' => 'Singaporean',
                'min_age' => 0,
                'max_age' => 55,
                'employer_rate' => '17%',
                'employee_rate' => '20%',
                'discount_rate' => '0.6',
                'threshold1' => 50,
                'threshold2' => 500,
                'threshold3' => 750,
                'ow_ceiling' => 6000
            ]);
        DB::table('sg_cpf')->insert([
                'employee_category' => 'Singaporean',
                'min_age' => 56,
                'max_age' => 60,
                'employer_rate' => '13%',
                'employee_rate' => '13%',
                'discount_rate' => '0.39',
                'threshold1' => 50,
                'threshold2' => 500,
                'threshold3' => 750,
                'ow_ceiling' => 6000
            ]);
        DB::table('sg_cpf')->insert([
                'employee_category' => 'Singaporean',
                'min_age' => 61,
                'max_age' => 65,
                'employer_rate' => '9%',
                'employee_rate' => '7.50%',
                'discount_rate' => '0.225',
                'threshold1' => 50,
                'threshold2' => 500,
                'threshold3' => 750,
                'ow_ceiling' => 6000
            ]);
        DB::table('sg_cpf')->insert([
                'employee_category' => 'Singaporean',
                'min_age' => 66,
                'max_age' => 150,
                'employer_rate' => '7.50%',
                'employee_rate' => '5%',
                'discount_rate' => '0.15',
                'threshold1' => 50,
                'threshold2' => 500,
                'threshold3' => 750,
                'ow_ceiling' => 6000
            ]);

        DB::table('sg_cpf')->insert([
                'employee_category' => '1st Year PR',
                'min_age' => 0,
                'max_age' => 55,
                'employer_rate' => '4%',
                'employee_rate' => '5%',
                'discount_rate' => '0.15',
                'threshold1' => 50,
                'threshold2' => 500,
                'threshold3' => 750,
                'ow_ceiling' => 6000
            ]);
        DB::table('sg_cpf')->insert([
                'employee_category' => '1st Year PR',
                'min_age' => 56,
                'max_age' => 60,
                'employer_rate' => '4%',
                'employee_rate' => '5%',
                'discount_rate' => '0.15',
                'threshold1' => 50,
                'threshold2' => 500,
                'threshold3' => 750,
                'ow_ceiling' => 6000
            ]);
        DB::table('sg_cpf')->insert([
                'employee_category' => '1st Year PR',
                'min_age' => 61,
                'max_age' => 65,
                'employer_rate' => '3.50%',
                'employee_rate' => '5%',
                'discount_rate' => '0.15',
                'threshold1' => 50,
                'threshold2' => 500,
                'threshold3' => 750,
                'ow_ceiling' => 6000
            ]);
        DB::table('sg_cpf')->insert([
                'employee_category' => '1st Year PR',
                'min_age' => 66,
                'max_age' => 150,
                'employer_rate' => '3.50%',
                'employee_rate' => '5%',
                'discount_rate' => '0.15',
                'threshold1' => 50,
                'threshold2' => 500,
                'threshold3' => 750,
                'ow_ceiling' => 6000
            ]);

        DB::table('sg_cpf')->insert([
                'employee_category' => '2nd Year PR',
                'min_age' => 0,
                'max_age' => 55,
                'employer_rate' => '9%',
                'employee_rate' => '15%',
                'discount_rate' => '0.45',
                'threshold1' => 50,
                'threshold2' => 500,
                'threshold3' => 750,
                'ow_ceiling' => 6000
            ]);
        DB::table('sg_cpf')->insert([
                'employee_category' => '2nd Year PR',
                'min_age' => 56,
                'max_age' => 60,
                'employer_rate' => '6%',
                'employee_rate' => '12.50%',
                'discount_rate' => '0.375',
                'threshold1' => 50,
                'threshold2' => 500,
                'threshold3' => 750,
                'ow_ceiling' => 6000
            ]);
        DB::table('sg_cpf')->insert([
                'employee_category' => '2nd Year PR',
                'min_age' => 61,
                'max_age' => 65,
                'employer_rate' => '3.50%',
                'employee_rate' => '7.50%',
                'discount_rate' => '0.225',
                'threshold1' => 50,
                'threshold2' => 500,
                'threshold3' => 750,
                'ow_ceiling' => 6000
            ]);
        DB::table('sg_cpf')->insert([
                'employee_category' => '2nd Year PR',
                'min_age' => 66,
                'max_age' => 150,
                'employer_rate' => '3.50%',
                'employee_rate' => '5%',
                'discount_rate' => '0.15',
                'threshold1' => 50,
                'threshold2' => 500,
                'threshold3' => 750,
                'ow_ceiling' => 6000
            ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sg_cpf');
    }
}
