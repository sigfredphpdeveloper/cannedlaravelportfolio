<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Company;
use App\Classes\Helper;


class CreateDefaultLeaveDaysCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        //All Companies
        $companies = Company::where('leave_days',null)->get();

        //create_default_leave_types
        foreach($companies as $company){
            $this->create_default_leave_days($company);
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

    function create_default_leave_days($company){

        $days = [];

        $weekdays = Helper::week_days();

        foreach($weekdays as $day){

            if($day == 'sunday' || $day == 'saturday'){
                $days[$day] = 0;
            }else{
                $days[$day] = 1;
            }

        }

        $company->leave_days = json_encode($days);
        $company->save();

        return $days;
    }

}
