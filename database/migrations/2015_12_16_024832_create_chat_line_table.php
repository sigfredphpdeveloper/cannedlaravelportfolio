<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateChatLineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_line', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('chat_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->longText('text_content')->nullable();
            $table->string('media_content')->nullable();
            $table->timestamp('time_sent');

            //set foreign key constraints
            $table->foreign('chat_id')->references('id')->on('chat');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('chat_line');
    }
}
