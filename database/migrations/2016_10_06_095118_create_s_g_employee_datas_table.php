<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSGEmployeeDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_g_employee_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('full_name_1');
            $table->string('full_name_2');
            $table->date('dob');;
            $table->string('id_number');
            $table->enum('address', ['local', 'foreign', 'local c/o']);
            $table->string('block_house')->nullable();
            $table->text('street')->nullable();
            $table->text('level')->nullable();
            $table->text('unit')->nullable();
            $table->text('line_1')->nullable();
            $table->text('line_2')->nullable();
            $table->text('line_3')->nullable();
            $table->text('postal_code')->nullable();
            $table->string('country')->nullable();
            $table->string('Nationality');
            $table->enum('sex', ['M', 'F']);
            $table->date('employment_start_date');
            $table->integer('probation_period');
            $table->date('confirmation_date');
            $table->date('termination_date');
            $table->text('termination_reason');
            $table->string('job_title');
            $table->enum('fee_frequency', ['monthly', 'hourly', 'daily']);
            $table->float('basic_pay')->nullable();
            $table->float('hourly_rate')->nullable();
            $table->float('daily_rate')->nullable();
            $table->integer('overtime_entitlement')->default(0);;
            $table->integer('cpf_entitlement')->default(0);
            $table->float('overtime_1_rate');
            $table->float('overtime_2_rate');
            $table->string('funds_selection');
            $table->enum('salary_payment_made', ['cheque', 'cash', 'bank transfer']);
            $table->string('bank_acct_name')->nullable();
            $table->string('bank')->nullable();
            $table->string('acct_number')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('s_g_employee_datas');
    }
}
