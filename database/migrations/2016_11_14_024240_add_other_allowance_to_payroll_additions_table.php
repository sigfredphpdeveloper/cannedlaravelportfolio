<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOtherAllowanceToPayrollAdditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payroll_additions', function (Blueprint $table) {
            $table->integer('other_allowance')->after('taxable')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payroll_additions', function (Blueprint $table) {
            $table->dropColumn('other_allowance');
        });
    }
}
