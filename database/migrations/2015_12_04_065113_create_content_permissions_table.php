<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContentPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('content_id');
            $table->integer('role_id');
            $table->integer('team_id');
            $table->integer('read_access');
            $table->integer('write_access');
            $table->integer('no_access');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('content_permissions');
    }

}
