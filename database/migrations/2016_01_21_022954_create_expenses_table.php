<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExpensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('expense_type_id');
            $table->integer('company_id');
            $table->integer('user_id');
            $table->string('title');
            $table->date('creation_date');
            $table->float('total_amount');
            $table->string('status')->default('pending');
            $table->date('approval_date');
            $table->date('paid_date');
            $table->text('request_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('expenses');
    }
}
