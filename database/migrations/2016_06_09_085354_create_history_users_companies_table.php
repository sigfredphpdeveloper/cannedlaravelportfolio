<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoryUsersCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('history_users_companies', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('date');
            $table->integer('day');
            $table->integer('month');
            $table->integer('year');
            $table->integer('total_user');
            $table->integer('total_company');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('history_users_companies');
    }
}
