<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Company;
use App\User;
use App\UserLeaveAllowance;
use App\LeaveTypesRoles;
use App\PayrollDeduction;

class CreateDefaultFundsDeductions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $companies = Company::all();

        foreach($companies as $company){

            if($company->country == 'SG'){

                $company->payroll = 1 ;
                $company->save();

                //add default deductions
                $default_deductions_count = PayrollDeduction::where('company_id',$company->id)->where('default_deduction',1)->count();

                //create only the deductions once so when > 1 dont create it only when zero
                if($default_deductions_count == 0){

                    $payroll_deduction = new PayrollDeduction;
                    $payroll_deduction->company_id = $company->id;
                    $payroll_deduction->title = 'CDAC';
                    $payroll_deduction->cpf_deductible = 0;
                    $payroll_deduction->default_deduction = 1;
                    $payroll_deduction->save();

                    $payroll_deduction = new PayrollDeduction;
                    $payroll_deduction->company_id = $company->id;
                    $payroll_deduction->title = 'MBMF';
                    $payroll_deduction->cpf_deductible = 0;
                    $payroll_deduction->default_deduction = 1;
                    $payroll_deduction->save();

                    $payroll_deduction = new PayrollDeduction;
                    $payroll_deduction->company_id = $company->id;
                    $payroll_deduction->title = 'ECF';
                    $payroll_deduction->cpf_deductible = 0;
                    $payroll_deduction->default_deduction = 1;
                    $payroll_deduction->save();

                    $payroll_deduction = new PayrollDeduction;
                    $payroll_deduction->company_id = $company->id;
                    $payroll_deduction->title = 'SINDA';
                    $payroll_deduction->cpf_deductible = 0;
                    $payroll_deduction->default_deduction = 1;
                    $payroll_deduction->save();

                }
            }
        }



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

}
