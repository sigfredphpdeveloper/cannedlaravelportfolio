<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrganizationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organization', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->integer('owner_id');
            $table->string('organization_name');
            $table->string('organization_address');
            $table->string('visibility');
            $table->dateTime('created_date');
            $table->dateTime('last_update_date');
            $table->integer('nb_of_contacts')->default(0);
            $table->string('cust_field_i');
            $table->string('cust_field_s');
            $table->timestamps();
        });


        Schema::table('organization', function ($table) {

            $table->renameColumn('cust_field_i', 'extra_fields');
            $table->dropColumn(['cust_field_s']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('organization');
    }
}
