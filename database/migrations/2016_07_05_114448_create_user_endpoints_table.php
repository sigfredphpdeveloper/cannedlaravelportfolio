<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserEndpointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_endpoints', function (Blueprint $table) {
            $table->increments('id');
            $table->string("endpoint");
            $table->timestamps();
            $table->integer("user_id")->unsigned();
        });

        Schema::table('user_endpoints',function($table){
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_endpoints');
    }
}
