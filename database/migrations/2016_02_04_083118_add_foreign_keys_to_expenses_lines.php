<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToExpensesLines extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expenses_lines', function (Blueprint $table) {
            $table->integer('expense_id')->unsigned()->change();
            $table->integer('expense_type_id')->unsigned()->change();
            $table->foreign('expense_type_id')->references('id')->on('expenses_type');
            $table->foreign('expense_id')->references('id')->on('expenses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expenses_lines', function (Blueprint $table) {
            $table->dropForeign(['expense_type_id']);
            $table->dropForeign(['expense_id']);
        });
    }
}
