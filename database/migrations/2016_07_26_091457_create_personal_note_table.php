<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalNoteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_note', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('owner_id');
            $table->string('title');
            $table->string('content');
            $table->string('top_position');
            $table->string('left_position');
            $table->string('z_index');
            $table->string('height_note');
            $table->string('width_note');
            $table->string('theme_note');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('personal_note');
    }
}
