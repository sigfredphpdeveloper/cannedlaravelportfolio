<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IdTypePrDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('s_g_employee_datas', function (Blueprint $table) {
            $table->string('id_type');
            $table->string('pr_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('s_g_employee_datas', function (Blueprint $table) {
            $table->dropColumn('id_type');
            $table->dropColumn('pr_date');
        });
    }
}
