<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
 
        DB::table('permissions')->insert([
            'feature' => 'View Recognition Board',
            'key'=>'view_recognition_board'
        ]);
 
        DB::table('permissions')->insert([
            'feature' => 'View Asset Management',
            'key'=>'view_asset_management'
        ]);

        DB::table('permissions')->insert([
            'feature' => 'Edit Asset Management',
            'key'=>'edit_asset_management'
        ]);
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('permissions')->where('key', "=", "view_recognition_board")->delete();
        DB::table('permissions')->where('key', "=", "view_asset_management")->delete();
        DB::table('permissions')->where('key', "=", "edit_asset_management")->delete();
    }
}
