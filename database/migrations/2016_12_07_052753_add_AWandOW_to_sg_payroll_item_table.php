<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAWandOWToSgPayrollItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sg_payroll_item', function (Blueprint $table) {
            $table->decimal('aw',10,2)->after('total_paid_salary')->default(0);
            $table->decimal('ow',10,2)->after('aw')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sg_payroll_item', function (Blueprint $table) {
            $table->dropColumn('aw');
            $table->dropColumn('ow');
        });
    }
}
