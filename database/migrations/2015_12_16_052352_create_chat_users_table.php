<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateChatUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('chat_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('last_read_message')->unsigned()->nullable();

            $table->foreign('chat_id')->references('id')->on('chat');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('last_read_message')->references('id')->on('chat_line');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('chat_users');
    }
}
