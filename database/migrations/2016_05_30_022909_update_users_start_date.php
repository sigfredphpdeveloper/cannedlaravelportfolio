<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;

class UpdateUsersStartDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        $user_no_start_date = User::where('start_date',NULL)->orWhere('start_date','1970-01-01')->orWhere('start_date','0000-00-00')->get();

        foreach($user_no_start_date as $user){

            echo $user->start_date."\n";

            $user->start_date = date('Y-m-d',strtotime($user->created_at));
            $user->save();

        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
