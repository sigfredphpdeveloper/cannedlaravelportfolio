<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSgPayrollItemDeductions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sg_payroll_item_deduction', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('deduction_id');
            $table->integer('item_id');
            $table->integer('user_id');
            $table->decimal('amount',10,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sg_payroll_item_deduction');
    }
}
