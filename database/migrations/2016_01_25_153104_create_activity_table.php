<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActivityTable extends Migration
{


    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity', function (Blueprint $table) {
            $table->increments('activity_id');
            $table->string('activity_title');
            $table->integer('deal_id');
            $table->string('activity_type');
            $table->date('date');
            $table->time('time');
            $table->string('duration');
            $table->integer('owner_id');
            $table->integer('assigned_user_id');
            $table->integer('done')->default(0);
            $table->dateTime('created_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('activity');
    }
}
