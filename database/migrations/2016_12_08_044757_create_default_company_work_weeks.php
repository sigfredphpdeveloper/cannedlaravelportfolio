<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Company;
use App\User;
use App\UserLeaveAllowance;
use App\LeaveTypesRoles;
use App\PayrollDeduction;
use App\CompanyWorkWeek;

class CreateDefaultCompanyWorkWeeks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('users', function (Blueprint $table) {
            $table->integer('work_week_id');
        });

        $companies = Company::all();

        foreach($companies as $company){

            $company_work_week = $company->company_work_week;

            if(count($company_work_week) == 0){

                $work_week = CompanyWorkWeek::create([
                    'company_id'=> $company->id,
                    'title'=>'Normal Work Week',
                    'monday'=>1,
                    'tuesday'=>1,
                    'wednesday'=>1,
                    'thursday'=>1,
                    'friday'=>1,
                    'saturday'=>0,
                    'sunday'=>0
                ]);

                $users = $company->users;

                if(count($users)>0){

                    foreach($users as $user){

                        $user_work_week = $user->work_week;

                        if($user->work_week_id == null OR $user->work_week_id =='' OR $user->work_week_id == '0'){

                            $user->work_week_id = $work_week->id;
                            $user->update();

                        }

                    }

                }

            }


        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('work_week_id');
        });

    }
}
