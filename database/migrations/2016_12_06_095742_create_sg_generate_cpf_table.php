<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSgGenerateCpfTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sg_generate_cpf', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->integer('month');
            $table->integer('year');
            $table->string('file_name');
            $table->text('file');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payroll_roles');
    }
}
