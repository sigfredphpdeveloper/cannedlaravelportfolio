<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Company;
use App\User;
use App\PayrollDeduction;
use App\PayrollAddition;

class AddPayrollAdditionsToExistingCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('payroll_additions', function (Blueprint $table) {
            $table->integer('default_addition')->default(0);
        });

        $companies = Company::all();

        foreach($companies as $company){

            if($company->country == 'SG'){

                $company->payroll = 1 ;
                $company->save();

                //add default deductions
                $default_additions_count = PayrollAddition::where('company_id',$company->id)->where('default_addition',1)->count();

                //create only the deductions once so when > 1 dont create it only when zero
                if($default_additions_count == 0){

                        $payroll_addition = new PayrollAddition;
                        $payroll_addition->company_id = $company->id;
                        $payroll_addition->title = 'Transport Allowance';
                        $payroll_addition->cpf_payable = 1;
                        $payroll_addition->taxable = 1;
                        $payroll_addition->other_allowance = 0;
                        $payroll_addition->category_id = 5;
                        $payroll_addition->default_addition = 1;
                        $payroll_addition->save();

                        $payroll_addition = new PayrollAddition;
                        $payroll_addition->company_id = $company->id;
                        $payroll_addition->title = 'Entertainment Allowance';
                        $payroll_addition->cpf_payable = 1;
                        $payroll_addition->taxable = 1;
                        $payroll_addition->other_allowance = 0;
                        $payroll_addition->category_id = 5;
                        $payroll_addition->default_addition = 1;
                        $payroll_addition->save();


                        $payroll_addition = new PayrollAddition;
                        $payroll_addition->company_id = $company->id;
                        $payroll_addition->title = 'Other Allowances';
                        $payroll_addition->cpf_payable = 1;
                        $payroll_addition->taxable = 1;
                        $payroll_addition->other_allowance = 0;
                        $payroll_addition->category_id = 5;
                        $payroll_addition->default_addition = 1;
                        $payroll_addition->save();

                        $payroll_addition = new PayrollAddition;
                        $payroll_addition->company_id = $company->id;
                        $payroll_addition->title = 'Sales Commissions';
                        $payroll_addition->cpf_payable = 1;
                        $payroll_addition->taxable = 1;
                        $payroll_addition->other_allowance = 0;
                        $payroll_addition->category_id = 5;
                        $payroll_addition->default_addition = 1;
                        $payroll_addition->save();

                        $payroll_addition = new PayrollAddition;
                        $payroll_addition->company_id = $company->id;
                        $payroll_addition->title = 'Bonus';
                        $payroll_addition->cpf_payable = 1;
                        $payroll_addition->taxable = 1;
                        $payroll_addition->other_allowance = 1;
                        $payroll_addition->category_id = 1;
                        $payroll_addition->default_addition = 1;
                        $payroll_addition->save();

                        $payroll_addition = new PayrollAddition;
                        $payroll_addition->company_id = $company->id;
                        $payroll_addition->title = 'Director\'s fees';
                        $payroll_addition->cpf_payable = 1;
                        $payroll_addition->taxable = 1;
                        $payroll_addition->other_allowance = 1;
                        $payroll_addition->category_id = 2;
                        $payroll_addition->default_addition = 1;
                        $payroll_addition->save();

                        $payroll_addition = new PayrollAddition;
                        $payroll_addition->company_id = $company->id;
                        $payroll_addition->title = 'Leave Pay';
                        $payroll_addition->cpf_payable = 1;
                        $payroll_addition->taxable = 1;
                        $payroll_addition->other_allowance = 1;
                        $payroll_addition->category_id = 4;
                        $payroll_addition->default_addition = 1;
                        $payroll_addition->save();

                        $payroll_addition = new PayrollAddition;
                        $payroll_addition->company_id = $company->id;
                        $payroll_addition->title = 'Notice Pay';
                        $payroll_addition->cpf_payable = 0;
                        $payroll_addition->taxable = 1;
                        $payroll_addition->other_allowance = 1;
                        $payroll_addition->category_id = 5;
                        $payroll_addition->default_addition = 1;
                        $payroll_addition->save();

                        $payroll_addition = new PayrollAddition;
                        $payroll_addition->company_id = $company->id;
                        $payroll_addition->title = 'Pension';
                        $payroll_addition->cpf_payable = 0;
                        $payroll_addition->taxable = 1;
                        $payroll_addition->other_allowance = 1;
                        $payroll_addition->category_id = 3;
                        $payroll_addition->default_addition = 1;
                        $payroll_addition->save();

                }
            }
        }



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
