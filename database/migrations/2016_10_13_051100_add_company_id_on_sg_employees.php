<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanyIdOnSgEmployees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('s_g_employee_datas', function (Blueprint $table) {
            $table->integer('company_id')->after('id')->unsigned();
            $table->foreign('company_id')->references('id')->on('company');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('s_g_employee_datas', function (Blueprint $table) {
            $table->dropColumn('company_id');
        });
    }
}
