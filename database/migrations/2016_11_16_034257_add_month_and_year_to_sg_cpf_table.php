<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Sg_cpf;

class AddMonthAndYearToSgCpfTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sg_cpf', function (Blueprint $table) {
            $table->integer('first_month')->after('id');
            $table->integer('first_year')->after('first_month');
            $table->integer('last_month')->after('first_year');
            $table->integer('last_year')->after('last_month');
        });

        // get the row that empty the month and year laps
        $cpf = Sg_cpf::where('first_month', '')->get();

        foreach($cpf as $row){
            $row->first_month = 1;
            $row->first_year = 2016;
            $row->last_month = 4;
            $row->last_year = 2017;
            $row->save();
        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sg_cpf', function (Blueprint $table) {
            $table->dropColumn('first_month');
            $table->dropColumn('first_year');
            $table->dropColumn('last_month');
            $table->dropColumn('last_year');
        });
    }
}
