<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('deal_title');
            $table->integer('contact_id');
            $table->integer('organization_id');
            $table->string('deal_value');
            $table->string('stage');
            $table->string('visibility', 15)->default('me');
            $table->dateTime('create_date');
            $table->dateTime('last_update_date');
            $table->dateTime('last_stage_change_date');
            $table->dateTime('next_activity_date');
            $table->dateTime('won_date');
            $table->dateTime('lost_date');
            $table->string('lost_reason');
            $table->integer('total_activity')->default(0);
            $table->integer('done_activity')->default(0);
            $table->integer('undone_activity')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('deals');
    }
}
