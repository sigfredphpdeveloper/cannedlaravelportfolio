<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSgPayrollItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::table('sg_payroll_item', function (Blueprint $table) {
            $table->dateTime('payment_date');
            $table->decimal('cpf_employee',10,2);
            $table->decimal('cpf_employer',10,2);
            $table->decimal('net_pay',10,2);
            $table->decimal('basic_pay',10,2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sg_payroll_item', function (Blueprint $table) {
            $table->dropColumn('payment_date');
            $table->dropColumn('cpf_employee');
            $table->dropColumn('cpf_employer');
            $table->dropColumn('net_pay');
            $table->dropColumn('basic_pay');
        });
    }
}
