<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetManagementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_management', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->integer('allocated_to');
            $table->string('title');
            $table->string('description');
            $table->enum('status', ['active', 'sold / disposed']);
            $table->date('purchase_date')->nullable();
            $table->date('sale_disposal_date')->nullable();
            $table->double('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('asset_management');
    }
}
