<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSgPayrollItem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sg_payroll_item', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('month');
            $table->integer('year');
            $table->decimal('salary_rate',10,2);
            $table->string('salary_frequency');
            $table->integer('no_frequency');
            $table->decimal('overtime1_rate',10,2);
            $table->integer('overtime1_hours');
            $table->decimal('overtime2_rate',10,2);
            $table->integer('overtime2_hours');
            $table->decimal('total_paid_salary',10,2);
            $table->decimal('fund_donation_total',10,2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sg_payroll_item');
    }
}
