<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserLeaveAllowanceAddLeaveTypeId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_leave_allowance', function (Blueprint $table) {
            $table->integer('leave_types_id')->unsigned();
            $table->foreign('leave_types_id')->references('id')->on('leave_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_leave_allowance', function (Blueprint $table) {
            $table->dropColumn('leave_types_id');
        });
    }
}
