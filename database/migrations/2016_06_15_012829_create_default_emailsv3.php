<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDefaultEmailsv3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $template1 = [
            'subject'=>'Leave application from ##name##',
            'body'=>'
                <p>Dear ##approver##,</p>

                <p>Member of your company (##company_name##)</p>

                <p>
                    Has applied for a Leave , with details :
                </p>

                <p>Name: ##name##</p>
                ##leave_details##

                 <br />
                ',
            'slug'=>'leave_notification_apply'
        ];

        $query = DB::table('email_templates')->insert($template1);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //

        DB::table('email_templates')->where('slug', 'leave_notification_apply')->delete();
    }
}
