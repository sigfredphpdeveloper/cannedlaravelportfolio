<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_username');
            $table->string('company_name');
            $table->string('url');
            $table->string('address');
            $table->string('industry');
            $table->string('size');
            $table->string('zip');
            $table->string('city');
            $table->string('country');
            $table->string('state');
            $table->string('phone_number');
            $table->string('logo');
            $table->timestamps('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company');
    }
}
