<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('board_id');
            $table->integer('list_id'); // task list ID
            $table->string('task_title');
            $table->string('task_details');
            $table->integer('assigned_user_id');
            $table->string('category');
            $table->dateTime('due_date');
            $table->dateTime('create_date');
            $table->dateTime('last_update_date');
            $table->string('status')->default('active');
            $table->string('list');
            $table->integer('visibility')->default(1);
//            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tasks');
    }
}
