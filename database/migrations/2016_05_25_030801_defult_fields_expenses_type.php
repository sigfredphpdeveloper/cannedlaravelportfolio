<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DefultFieldsExpensesType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expenses_fields', function (Blueprint $table) {
            $table->integer('default')->default(0)->after('required');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expenses_fields', function (Blueprint $table) {
            $table->dropColumn('default');
        });
    }
}
