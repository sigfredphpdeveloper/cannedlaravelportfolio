<?php

use App\Admin;
use Illuminate\Database\Migrations\Migration;

class UpdateAdminTableChangePasswordToBcrypt extends Migration
{
    protected $admin;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $admin_user = Admin::where('username', 'admin')->first();

        if ($admin_user) {
            $admin_user->update(['password' => bcrypt('admin')]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $admin_user = Admin::where('username', 'admin')->first();

        if ($admin_user) {
            $admin_user->update(['password' => md5('admin')]);
        }
    }
}
