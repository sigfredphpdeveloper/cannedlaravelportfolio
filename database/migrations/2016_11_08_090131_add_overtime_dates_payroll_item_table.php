<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOvertimeDatesPayrollItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sg_payroll_item', function (Blueprint $table) {
            $table->date('overtime_from');
            $table->date('overtime_to');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sg_payroll_item', function (Blueprint $table) {
            $table->dropColumn('overtime_from');
            $table->dropColumn('overtime_to');
        });

    }
}
