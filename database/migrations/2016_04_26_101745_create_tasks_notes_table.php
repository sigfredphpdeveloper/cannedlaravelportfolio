<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks_notes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('task_id');
            $table->string('note_content');
            $table->dateTime('create_date');
            $table->string('added_by'); // user_id
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tasks_notes');
    }
}
