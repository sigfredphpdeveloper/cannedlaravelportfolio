<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SgIrasForm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sg_iras_form', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->string('year');
            $table->string('type')->nullable();
            $table->string('file_url')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('sg_iras_form');
    }
}
