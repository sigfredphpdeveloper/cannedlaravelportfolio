<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AllRolesViewCalendar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $view_calendar = DB::table('permissions')->where('key','view_calendar')->first();

        $all_roles = DB::table('roles')->get();


        foreach($all_roles as $AR){

            $check = DB::table('role_permission')->where('role_id',$AR->id)->where('permission_id',$view_calendar->id)->count();

            if($check == 0){

                DB::table('role_permission')->insert([
                    'role_id' => $AR->id,
                    'permission_id'=>$view_calendar->id
                ]);
            }

        }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        $view_calendar = DB::table('permissions')->where('key','view_calendar')->first();

        DB::table('role_permission')->where('permission_id',$view_calendar->id)->delete();


    }
}
