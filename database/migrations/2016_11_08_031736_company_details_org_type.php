<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CompanyDetailsOrgType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('s_g_company_details', function (Blueprint $table) {
            $table->string('org_type')->after('org_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('s_g_company_details', function (Blueprint $table) {
            $table->dropColumn('org_type');
        });
    }
}
