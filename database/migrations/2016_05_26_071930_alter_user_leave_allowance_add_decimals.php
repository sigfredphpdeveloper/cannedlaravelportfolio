<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserLeaveAllowanceAddDecimals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::statement('ALTER TABLE user_leave_allowance MODIFY COLUMN allowance DECIMAL(5,2)');

        DB::statement('ALTER TABLE user_leave_allowance MODIFY COLUMN remaining DECIMAL(5,2)');


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {



    }
}
