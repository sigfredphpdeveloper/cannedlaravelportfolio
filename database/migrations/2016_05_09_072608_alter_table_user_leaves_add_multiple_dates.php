<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserLeavesAddMultipleDates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_leaves', function (Blueprint $table) {
            $table->date('date_from');
            $table->string('date_from_type');
            $table->date('date_to');
            $table->string('date_to_type');
            $table->integer('credits');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_leaves', function (Blueprint $table) {
            $table->dropColumn('date_from');
            $table->dropColumn('date_from_type');
            $table->dropColumn('date_to');
            $table->dropColumn('date_to_type');
            $table->dropColumn('credits');
        });
    }
}
