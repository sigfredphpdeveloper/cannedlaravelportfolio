<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameSalaryPaymentMode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('s_g_employee_datas', function (Blueprint $table) {
            $table->string('salary_payment_mode');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('s_g_employee_datas', function (Blueprint $table) {
            $table->dropColumn('salary_payment_mode');
        });
    }
}
