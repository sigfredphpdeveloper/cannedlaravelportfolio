<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddExtraMediaContentFieldsToChatLines extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chat_line', function (Blueprint $table) {
            $table->integer('media_size_in_bytes')->nullable();
            $table->string('media_name')->nullable();
            $table->enum('media_type', ['file', 'image'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chat_line', function (Blueprint $table) {
            $table->dropColumn(['media_size_in_bytes', 'media_name', 'media_type']);
        });
    }
}
