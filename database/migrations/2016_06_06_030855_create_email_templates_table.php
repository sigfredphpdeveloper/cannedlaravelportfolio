<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject');
            $table->text('body');
            $table->string('slug');
            $table->timestamps();
        });

        $template1 = [
            'subject'=>'Welcome to Zenintra.net',
            'body'=>'<p>Dear ##first_name## ##last_name##,</p>

                <p>Thank you for joining Zenintra.net. You will soon be able to manage your company from a single platform for FREE! </p>

                <p>Please click on the link below to activate your account:</p>

                 <p>##link##</p>

                 <br />',
            'slug'=>'registration'
        ];

        $query = DB::table('email_templates')->insert($template1);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        DB::table('email_templates')->where('slug', 'registration')->delete();

        Schema::drop('email_templates');


    }
}
