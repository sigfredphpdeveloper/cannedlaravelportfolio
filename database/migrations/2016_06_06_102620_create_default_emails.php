<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDefaultEmails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $template1 = [
            'subject'=>'Congratulations: ##company_referred## has joined Zenintra',
            'body'=>'

<p>Dear ##referrer##,</p>

    <p>
    Congratulations: ##company_referred## has followed your recommendation and joined Zenintra.net.
    We are pleased to add 200Mb to your memory allowance.
    </p>
                 <br />',
            'slug'=>'referral_success'
        ];

        $query = DB::table('email_templates')->insert($template1);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('email_templates')->where('slug', 'referral_success')->delete();
    }
}
