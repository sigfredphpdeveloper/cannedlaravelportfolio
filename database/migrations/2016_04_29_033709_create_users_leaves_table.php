<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersLeavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('leave_types', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('company');
            $table->string('type');
            $table->text('description');
            $table->integer('default_allowance');
            $table->string('color_code');
            $table->date('start_date_allowance');
            $table->timestamps();

        });


        Schema::create('users_leaves', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('leave_types_id')->unsigned();
            $table->foreign('leave_types_id')->references('id')->on('leave_types');
            $table->date('date');
            $table->string('date_type');
            $table->text('details');
            $table->string('status');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users_leaves');
        Schema::drop('leave_types');
    }
}
