<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExpensesRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenses_roles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->integer('role_id');
            $table->integer('approve')->default(0);
            $table->integer('delete')->default(0);
            $table->integer('pay')->default(0);
            $table->integer('view_all')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('expenses_roles');
    }
}
