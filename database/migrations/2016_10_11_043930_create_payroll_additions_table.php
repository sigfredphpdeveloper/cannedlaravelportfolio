<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollAdditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_additions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->integer('cpf_payable')->nullable()->default(0);
            $table->integer('taxable')->nullable()->default(0);
            $table->integer('tax_category')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('payroll_additions');
    }
}
