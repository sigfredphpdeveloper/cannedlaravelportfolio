<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOwnerIdDealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('deals', function (Blueprint $table) {
            $table->integer('owner_id')->after('organization_id')->unsigned();

//            $table->foreign('owner_id')->references('id')->on('users');
        });

        $deals = DB::table('deals')->select('*', 'deals.id AS deal_id','contacts.owner_id as cont_owner_id')
            ->join('contacts','contacts.id','=','deals.contact_id')
            ->where('deals.owner_id', '=', '0')->get();
        if(count($deals)>=1){
            foreach($deals as $deal){
                DB::table('deals')->where('id',$deal->deal_id)->update(['owner_id'=>$deal->cont_owner_id]);
            }
        }


        Schema::table('deals', function (Blueprint $table) {
            $table->foreign('owner_id')->references('id')->on('users');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('deals', function (Blueprint $table) {
            $table->dropColumn('owner_id');
        });
    }
}
