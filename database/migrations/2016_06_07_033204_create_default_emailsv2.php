<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDefaultEmailsv2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        $template1 = [
            'subject'=>'New announcement for ##company_name##',
            'body'=>'
                <p>Dear ##name##,</p>

                <p>This is a new announcement from ##company_name##</p>

                <p>##announcement_title##</p>

                <p>##announcement_message##</p>
                ',
            'slug'=>'announcement_notification'
        ];

        $query = DB::table('email_templates')->insert($template1);


        $template2 = [
            'subject'=>'Invitation Request to join ##found_company_name## on Zenintra',
            'body'=>'
                <p>Dear ##master_first_name## ##master_last_name##,</p>

                <p>Invitation to join your company (##found_company_name##)</p>

                <p>##invitee## wants to join your company ##found_company_name##</p>

                <p>
                Please click on the link below to accept his invitation.</p>
                </p>

                 <p>
                 ##link##
                 </p>
                ',
            'slug'=>'company_join_request'
        ];

        $query = DB::table('email_templates')->insert($template2);






        $template3 = [
            'subject'=>'Welcome to Zenintra.net ##company_name## (Please Confirm Your Account)',
            'body'=>'
               <p>Dear ##first_name## ##last_name##,</p>

                <p>Thank you for joining Zenintra.net.  </p>

                <p>Please click on the link below to activate your account:</p>

                 <p>##link##</p>

                 <br />
                ',
            'slug'=>'company_signup_user'
        ];

        $query = DB::table('email_templates')->insert($template3);




        $template4 = [
            'subject'=>'Zenintranet CONTACT US ENQUIRY',
            'body'=>'
               <p>Name: ##name##</p>
                <p>Email: ##email##</p>
                <p>Message:</p>
                <p>##message##</p>
                ',
            'slug'=>'contact_us'
        ];

        $query = DB::table('email_templates')->insert($template4);


        $template5 = [
            'subject'=>'Expenses Status Updated',
            'body'=>'
               <p>Dear ##name##,</p>

                <p>Welcome to Zenintranet</p>

                <p>This email is to inform you that you expense request "##title##" has been updated to ##status##.
                    Any question? Contact us at ##contact##</p>
                ',
            'slug'=>'expenses'
        ];

        $query = DB::table('email_templates')->insert($template5);



        $template6 = [
            'subject'=>'Expenses Status Updated',
            'body'=>'
                <p>Dear ##user##,</p>

                <p>This is to inform you the Expense request "##title##"" status is updated to <span style="color:#57889c; text-transform:uppercase">##status##</span><br /></p>
                <p>##details##</p>
                ',

            'slug'=>'expenses_status'
        ];

        $query = DB::table('email_templates')->insert($template6);




        $template7 = [
            'subject'=>'Join our ##company_name## intranet',
            'body'=>'
               <p>Dear ##invitee_first_name##, </p>
                 <br />
                 <p>
                Welcome to Zenintranet </p>
                 <p>
                 ##invitor_name## invited you to join the company ##company_name##
                 </p>
                 <p>
                Please click on the link below to create a password for your account (username ##your_username##)
                 </p>
                <p>##link##</p>
                ',

            'slug'=>'invite_user'
        ];

        $query = DB::table('email_templates')->insert($template7);




        $template8 = [
            'subject'=>'Invitation from ##referrer_name## to use Zenintra.net',
            'body'=>'
                <p>Hello, </p>
                <br />
                <p>
                    My name is ##referrer_name## from ##referrer_company##.
                    I am using Zenintra.net, an all-in-one FREE intranet to manage my team and my business, which I recommend to you.
                </p>

                <br>
                <p>
                    To create an account, just click here or copy the link below:
                    <br>
                    ##link##
                </p>
                ',
            'slug'=>'referral_request'
        ];

        $query = DB::table('email_templates')->insert($template8);




        $template9 = [
            'subject'=>'##title##',
            'body'=>'
                <p>Email: ##email##</p>
                <p>Request Type: ##request_type##</p>
                <br />
                <p>##message_text##</p>
                ',
            'slug'=>'support'
        ];

        $query = DB::table('email_templates')->insert($template9);




        $template10 = [
            'subject'=>'Your Account Is Now Active - ##company_name##',
            'body'=>'
                <p>Congratulations and welcome to the Zenintra.net community!</p>

                <p>In order to get the most out of Zenintra.net, we have prepared some short video tutorials for you:</p>
                <p>##link##</p>

                <br />
                <p>Have a nice day!</p>
                ',
            'slug'=>'user_activated'
        ];

        $query = DB::table('email_templates')->insert($template10);






    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('email_templates')->where('slug', 'announcement_notification')->delete();
        DB::table('email_templates')->where('slug', 'company_join_request')->delete();
        DB::table('email_templates')->where('slug', 'company_signup_user')->delete();
        DB::table('email_templates')->where('slug', 'contact_us')->delete();
        DB::table('email_templates')->where('slug', 'expenses')->delete();
        DB::table('email_templates')->where('slug', 'expenses_status')->delete();
        DB::table('email_templates')->where('slug', 'invite_user')->delete();
        DB::table('email_templates')->where('slug', 'referral_request')->delete();
        DB::table('email_templates')->where('slug', 'support')->delete();
        DB::table('email_templates')->where('slug', 'user_activated')->delete();
    }
}
