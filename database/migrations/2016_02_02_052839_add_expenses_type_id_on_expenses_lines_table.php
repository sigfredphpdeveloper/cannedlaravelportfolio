<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddExpensesTypeIdOnExpensesLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expenses_lines', function (Blueprint $table) {
            $table->integer('expense_type_id')->after('expense_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expenses_lines', function (Blueprint $table) {
            $table->dropColumn('expense_type_id');
        });
    }
}
