<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddIdentifierColumnPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permissions', function (Blueprint $table) {
            $table->string('key')->after('feature');
        });

        DB::table('permissions')->truncate();

        DB::table('permissions')->insert([
            'feature' => 'Edit Company / Role / Team',
            'key'     => 'edit_company_role_team'
        ]);

        DB::table('permissions')->insert([
            'feature' => 'View Company / Role / Team',
            'key'     => 'view_company_role_team'
        ]);

        DB::table('permissions')->insert([
            'feature' => 'Assign Roles',
            'key'     => 'assign_roles'
        ]);

        DB::table('permissions')->insert([
            'feature' => 'Assign Teams',
            'key'     => 'assign_teams'
        ]);

        DB::table('permissions')->insert([
            'feature' => 'Edit Employees',
            'key'     => 'edit_employees'
        ]);

        DB::table('permissions')->insert([
            'feature' => 'Edit General Permissions',
            'key'     => 'edit_general_permissions'
        ]);

        DB::table('permissions')->insert([
            'feature' => 'Manage Announcement',
            'key'     => 'manage_announcement'
        ]);

        DB::table('permissions')->insert([
            'feature' => 'Manage Contacts',
            'key'     => 'manage_contacts'
        ]);

        DB::table('permissions')->insert([
            'feature' => 'View Emergency Contacts',
            'key'     => 'view_emergency_contacts'
        ]);

        DB::table('permissions')->insert([
            'feature' => 'Edit Emergency Contacts',
            'key'     => 'edit_emergency_contacts'
        ]);

        DB::table('permissions')->insert([
            'feature' => 'View Expenses',
            'key'     => 'view_expenses'
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permissions', function (Blueprint $table) {
            $table->dropColumn('key');
        });


    }
}
