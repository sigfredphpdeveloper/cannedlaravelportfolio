<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CategoryIdOnTaxAddition extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payroll_additions', function (Blueprint $table) {
            $table->renameColumn('tax_category', 'category_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payroll_additions', function (Blueprint $table) {
            $table->renameColumn('category_id', 'tax_category');
        });
    }
}
