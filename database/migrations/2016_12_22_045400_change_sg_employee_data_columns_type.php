<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSgEmployeeDataColumnsType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('s_g_employee_datas', function (Blueprint $table) {
            DB::statement('ALTER TABLE s_g_employee_datas MODIFY COLUMN basic_pay VARCHAR(200)');
            DB::statement('ALTER TABLE s_g_employee_datas MODIFY COLUMN daily_rate VARCHAR(200)');
            DB::statement('ALTER TABLE s_g_employee_datas MODIFY COLUMN hourly_rate VARCHAR(200)');
            DB::statement('ALTER TABLE s_g_employee_datas MODIFY COLUMN overtime_1_rate VARCHAR(200)');
            DB::statement('ALTER TABLE s_g_employee_datas MODIFY COLUMN overtime_2_rate VARCHAR(200)');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('s_g_employee_datas', function (Blueprint $table) {
            DB::statement('ALTER TABLE s_g_employee_datas MODIFY COLUMN basic_pay DECIMAL(10,2)');
            DB::statement('ALTER TABLE s_g_employee_datas MODIFY COLUMN daily_rate DECIMAL(10,2)');
            DB::statement('ALTER TABLE s_g_employee_datas MODIFY COLUMN hourly_rate DECIMAL(10,2)');
            DB::statement('ALTER TABLE s_g_employee_datas MODIFY COLUMN overtime_1_rate DECIMAL(10,2)');
            DB::statement('ALTER TABLE s_g_employee_datas MODIFY COLUMN overtime_2_rate DECIMAL(10,2)');
        });
    }
}
