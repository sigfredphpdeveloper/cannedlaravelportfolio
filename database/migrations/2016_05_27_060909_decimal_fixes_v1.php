<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DecimalFixesV1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE user_leave_allowance MODIFY COLUMN allowance DECIMAL(5,1)');

        DB::statement('ALTER TABLE user_leave_allowance MODIFY COLUMN remaining DECIMAL(5,1)');

        DB::statement('ALTER TABLE users_leaves MODIFY COLUMN credits DECIMAL(5,1)');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
