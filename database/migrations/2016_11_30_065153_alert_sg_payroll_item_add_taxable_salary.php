<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlertSgPayrollItemAddTaxableSalary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sg_payroll_item', function (Blueprint $table) {
            $table->decimal('taxable_salary',10,2);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sg_payroll_item', function (Blueprint $table) {
            $table->dropColumn('taxable_salary');
        });

    }
}
