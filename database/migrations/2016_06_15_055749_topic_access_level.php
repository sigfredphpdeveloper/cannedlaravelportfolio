<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TopicAccessLevel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('topics', function (Blueprint $table) {
            $table->integer('access_everyone')->default(0)->after('type');
            $table->string('access_roles')->after('access_everyone');
            $table->string('access_teams')->after('access_roles');
            $table->dropColumn('visibility');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('topics', function (Blueprint $table) {
            $table->dropColumn('access_everyone');
            $table->dropColumn('access_roles');
            $table->dropColumn('access_teams');
            $table->integer('visibility');
        });
    }
}
