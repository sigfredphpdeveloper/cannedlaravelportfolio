<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCheckStatusToTasksCheckList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks_check_list', function (Blueprint $table) {
            $table->integer('check_status')->after('task_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks_check_list', function (Blueprint $table) {
            $table->dropColumn('check_status');
            //
        });
    }
}
