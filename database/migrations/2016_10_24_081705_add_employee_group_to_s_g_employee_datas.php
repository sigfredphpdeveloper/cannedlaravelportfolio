<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmployeeGroupToSGEmployeeDatas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('s_g_employee_datas', function (Blueprint $table) {
            $table->string('cpf_employee_group')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('s_g_employee_datas', function (Blueprint $table) {
            $table->dropColumn('cpf_employee_group');
        });
    }
}
