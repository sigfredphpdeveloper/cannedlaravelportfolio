<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCpfFundsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('cpf_funds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->integer('min_wage');
            $table->integer('max_wage');
            $table->string('from_date');
            $table->decimal('monthly_contribution',10,2);
            $table->timestamps();
        });

            //CDAC
            DB::table('cpf_funds')->insert([
                'type' => 'CDAC',
                'min_wage' => 0,
                'max_wage' => 2000,
                'from_date' => 'January 2015',
                'monthly_contribution' => 0.50,
            ]);

            DB::table('cpf_funds')->insert([
                'type' => 'CDAC',
                'min_wage' => 2001,
                'max_wage' => 3500,
                'from_date' => 'January 2015',
                'monthly_contribution' => 1,
            ]);

            DB::table('cpf_funds')->insert([
                'type' => 'CDAC',
                'min_wage' => 3501,
                'max_wage' => 5000,
                'from_date' => 'January 2015',
                'monthly_contribution' => 1.5,
            ]);

            DB::table('cpf_funds')->insert([
                'type' => 'CDAC',
                'min_wage' => 5001,
                'max_wage' => 7500,
                'from_date' => 'January 2015',
                'monthly_contribution' => 2,
            ]);

            DB::table('cpf_funds')->insert([
                'type' => 'CDAC',
                'min_wage' => 7501,
                'max_wage' => 999999999,
                'from_date' => 'January 2015',
                'monthly_contribution' => 3,
            ]);

            //ECF
            DB::table('cpf_funds')->insert([
                'type' => 'ECF',
                'min_wage' => 0,
                'max_wage' => 1000,
                'from_date' => 'January 2015',
                'monthly_contribution' => 2,
            ]);

            DB::table('cpf_funds')->insert([
                'type' => 'ECF',
                'min_wage' => 1001,
                'max_wage' => 1500,
                'from_date' => 'January 2015',
                'monthly_contribution' => 4,
            ]);

            DB::table('cpf_funds')->insert([
                'type' => 'ECF',
                'min_wage' => 1501,
                'max_wage' => 2500,
                'from_date' => 'January 2015',
                'monthly_contribution' => 6,
            ]);

            DB::table('cpf_funds')->insert([
                'type' => 'ECF',
                'min_wage' => 2501,
                'max_wage' => 4000,
                'from_date' => 'January 2015',
                'monthly_contribution' => 9,
            ]);

            DB::table('cpf_funds')->insert([
                'type' => 'ECF',
                'min_wage' => 4001,
                'max_wage' => 7000,
                'from_date' => 'January 2015',
                'monthly_contribution' => 12,
            ]);

            DB::table('cpf_funds')->insert([
                'type' => 'ECF',
                'min_wage' => 7001,
                'max_wage' => 10000,
                'from_date' => 'January 2015',
                'monthly_contribution' => 16,
            ]);

            DB::table('cpf_funds')->insert([
                'type' => 'ECF',
                'min_wage' => 10001,
                'max_wage' => 99999999,
                'from_date' => 'January 2015',
                'monthly_contribution' => 20,
            ]);



            //SINDA
            DB::table('cpf_funds')->insert([
                'type' => 'SINDA',
                'min_wage' => 0,
                'max_wage' => 1000,
                'from_date' => 'January 2015',
                'monthly_contribution' => 1,
            ]);

            DB::table('cpf_funds')->insert([
                'type' => 'SINDA',
                'min_wage' => 1001,
                'max_wage' => 1500,
                'from_date' => 'January 2015',
                'monthly_contribution' => 3,
            ]);

            DB::table('cpf_funds')->insert([
                'type' => 'SINDA',
                'min_wage' => 1501,
                'max_wage' => 2500,
                'from_date' => 'January 2015',
                'monthly_contribution' => 5,
            ]);

            DB::table('cpf_funds')->insert([
                'type' => 'SINDA',
                'min_wage' => 2501,
                'max_wage' => 4500,
                'from_date' => 'January 2015',
                'monthly_contribution' => 7,
            ]);

            DB::table('cpf_funds')->insert([
                'type' => 'SINDA',
                'min_wage' => 4501,
                'max_wage' => 7500,
                'from_date' => 'January 2015',
                'monthly_contribution' => 9,
            ]);


            DB::table('cpf_funds')->insert([
                'type' => 'SINDA',
                'min_wage' => 7501,
                'max_wage' => 10000,
                'from_date' => 'January 2015',
                'monthly_contribution' => 12,
            ]);

            DB::table('cpf_funds')->insert([
                'type' => 'SINDA',
                'min_wage' => 10001,
                'max_wage' => 15000,
                'from_date' => 'January 2015',
                'monthly_contribution' => 18,
            ]);

            DB::table('cpf_funds')->insert([
                'type' => 'SINDA',
                'min_wage' => 15001,
                'max_wage' => 999999999,
                'from_date' => 'January 2015',
                'monthly_contribution' => 30,
            ]);


            //MBMF
            DB::table('cpf_funds')->insert([
                'type' => 'MBMF',
                'min_wage' => 0,
                'max_wage' => 1000,
                'from_date' => 'June 2015',
                'monthly_contribution' => 3,
            ]);

            DB::table('cpf_funds')->insert([
                'type' => 'MBMF',
                'min_wage' => 1001,
                'max_wage' => 2000,
                'from_date' => 'June 2015',
                'monthly_contribution' => 4.5,
            ]);

            DB::table('cpf_funds')->insert([
                'type' => 'MBMF',
                'min_wage' => 2001,
                'max_wage' => 3000,
                'from_date' => 'June 2015',
                'monthly_contribution' => 6.5,
            ]);

            DB::table('cpf_funds')->insert([
                'type' => 'MBMF',
                'min_wage' => 3001,
                'max_wage' => 4000,
                'from_date' => 'June 2015',
                'monthly_contribution' => 15,
            ]);

            DB::table('cpf_funds')->insert([
                'type' => 'MBMF',
                'min_wage' => 4001,
                'max_wage' => 6000,
                'from_date' => 'June 2015',
                'monthly_contribution' => 19.5,
            ]);

            DB::table('cpf_funds')->insert([
                'type' => 'MBMF',
                'min_wage' => 6001,
                'max_wage' => 8000,
                'from_date' => 'June 2015',
                'monthly_contribution' => 22,
            ]);

            DB::table('cpf_funds')->insert([
                'type' => 'MBMF',
                'min_wage' => 8001,
                'max_wage' => 10000,
                'from_date' => 'June 2015',
                'monthly_contribution' => 24,
            ]);

            DB::table('cpf_funds')->insert([
                'type' => 'MBMF',
                'min_wage' => 10001,
                'max_wage' => 9999999999,
                'from_date' => 'June 2015',
                'monthly_contribution' => 26,
            ]);


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cpf_funds');
    }
}
