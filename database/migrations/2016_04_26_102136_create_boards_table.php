<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boards', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->integer('owner_id');
            $table->dateTime('created_date');
            $table->dateTime('last_update_date'); // user_id
            $table->string('board_title'); // user_id
            $table->string('visibility'); // user_id
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void.
     */
    public function down()
    {
        Schema::drop('boards');
    }
}
