<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSgPayrollItemsColumnsType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sg_payroll_item', function (Blueprint $table) {
            $table->string('salary_rate')->change();
            $table->string('overtime1_rate')->change();
            $table->string('overtime2_rate')->change();
            $table->string('total_paid_salary')->change();
            $table->string('aw')->change();
            $table->string('ow')->change();
            $table->string('cpf_employer')->change();
            $table->string('cpf_employee')->change();
            $table->string('net_pay')->change();
            $table->string('basic_pay')->change();
            $table->string('fund_donation_total')->change();
            $table->string('taxable_salary')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sg_payroll_item', function (Blueprint $table) {
            $table->decimal('salary_rate',10,2)->change();
            $table->decimal('overtime1_rate',10,2)->change();
            $table->decimal('overtime2_rate',10,2)->change();
            $table->decimal('total_paid_salary',10,2)->change();
            $table->decimal('aw',10,2)->change();
            $table->decimal('ow',10,2)->change();
            $table->decimal('cpf_employer',10,2)->change();
            $table->decimal('cpf_employee',10,2)->change();
            $table->decimal('net_pay',10,2)->change();
            $table->decimal('basic_pay',10,2)->change();
            $table->decimal('fund_donation_total',10,2)->change();
            $table->decimal('taxable_salary',10,2)->change();
        });
    }
}
