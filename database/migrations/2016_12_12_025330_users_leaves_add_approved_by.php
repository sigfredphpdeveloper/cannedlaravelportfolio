<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersLeavesAddApprovedBy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::table('users_leaves', function (Blueprint $table) {
            $table->integer('approved_by')->default(0);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_leaves', function (Blueprint $table) {
            $table->dropColumn('approved_by');
        });

    }
}
