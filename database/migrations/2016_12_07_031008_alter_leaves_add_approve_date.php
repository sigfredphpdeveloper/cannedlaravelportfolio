<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\Users_leaves;

class AlterLeavesAddApproveDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users_leaves', function (Blueprint $table) {
            $table->date('approved_date');
        });

        $leaves = Users_leaves::all();

        foreach($leaves as $leave){

                if($leave->status == 'approved'){

                    $leave->approved_date = date('Y-m-d',strtotime($leave->updated_at));
                    $leave->update();
                }

        }



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_leaves', function (Blueprint $table) {
            $table->dropColumn('approved_date');
        });

    }
}
