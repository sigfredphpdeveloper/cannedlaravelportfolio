<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserLeavesAddLeaveType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::table('users_leaves', function (Blueprint $table) {
            $table->enum('leave_application_type', ['single', 'multiple']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users_leaves', function (Blueprint $table) {
            $table->dropColumn('leave_application_type');
        });
    }
}
