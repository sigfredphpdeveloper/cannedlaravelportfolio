<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Company;
use App\User;
use App\UserLeaveAllowance;
use App\LeaveTypesRoles;


class CreateLeaveAllowancesForUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        //COMPANY LEAVE TYPES
        $companies = Company::all();
        //create_default_leave_types
        foreach($companies as $company){

            $leave_types = $company->leave_types;

            if($leave_types->isEmpty()){
                $this->create_default_leave_types($company);
            }

        }

        //USER LEAVE ALLOWANCES
        $users = User::all();

        foreach($users as $user){

            $companies = $user->companies;

            foreach($companies as $company){

                $this->create_default_user_leave_allowance($user,$company);

            }

        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }

    function create_default_user_leave_allowance($user,$company){

        $leave_types = $company->leave_types;

        foreach($leave_types as $leave_type){

            $user_leave_allowance = UserLeaveAllowance::where('user_id',$user->id)->where('leave_types_id','=',$leave_type->id)->first();

            if(!$user_leave_allowance){

                $user_leave_allowance = $user->leave_allowances()->create([
                    'user_id'=>$user['id'],
                    'allowance'=>$leave_type->default_allowance,
                    'remaining'=>$leave_type->default_allowance,
                    'leave_types_id'=>$leave_type->id
                ]);

            }

        }
    }



    function create_default_leave_types($company){

        $leave_type = $company->leave_types()->create([
            'type' => 'Paid Leave',
            'description' => 'Paid Leave',
            'default_allowance'=> 10,
            'color_code'=>'',
            'start_date_allowance' => date('Y-m-d')
        ]);

        $company_roles = $company->roles;

        $leave_types_roles_collection = [];

        foreach($company_roles as $role){

            $role_id = $role->id;

            $leave_types_roles_collection[] = [
                'leave_type_id'=>$leave_type->id,
                'role_id'=>$role_id
            ];

        }

        LeaveTypesRoles::insert($leave_types_roles_collection);

        $leave_type = $company->leave_types()->create([
            'type' => 'Medical Leave',
            'description' => 'Medical Leave',
            'default_allowance'=> 10,
            'color_code'=>'',
            'start_date_allowance' => date('Y-m-d')
        ]);

        $leave_types_roles_collection = [];

        foreach($company_roles as $role){

            $role_id = $role->id;

            $leave_types_roles_collection[] = [
                'leave_type_id'=>$leave_type->id,
                'role_id'=>$role_id
            ];

        }

        LeaveTypesRoles::insert($leave_types_roles_collection);

    }
}
