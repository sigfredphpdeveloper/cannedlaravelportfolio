<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AlterContentAddFileSize extends Migration
{
    public function up()
    {
        Schema::table('content', function (Blueprint $table) {
            $table->string('file_size')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('content', function (Blueprint $table) {
            $table->dropColumn(
                'file_size'
            );
        });
    }
}
