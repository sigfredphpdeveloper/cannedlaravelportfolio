<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FileFieldOnEveryExpenseType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $expenses_types = DB::table('expenses_type')->get();
        foreach( $expenses_types as $expenses_type )
        {
            $expenses_fields = DB::table('expenses_fields')->where( 'expense_type_id', $expenses_type->id )->get();
            $new_fields = array();
            $temp_array = array();
            $count = 0;
            foreach( $expenses_fields as $expenses_field )
            {
                if( $count == 1 ) 
                {
                    if( $expenses_field->label != 'file' )
                    {
                        $temp_array = array(
                            'expense_type_id'   => $expenses_type->id,
                            'label'             => 'file',
                            'type'              => 'file',
                            'required'          => '0',
                            'default'           => 1,
                        );
                        array_push( $new_fields, $temp_array );
                    }
                }

                if( $expenses_field->label == 'amount' && $count == 0 )
                {
                    $temp_array = array(
                        'expense_type_id'   => $expenses_type->id,
                        'label'             => $expenses_field->label,
                        'type'              => $expenses_field->type,
                        'required'          => '1',
                        'default'           => '1',
                    );
                    array_push( $new_fields, $temp_array );
                }
                elseif( $expenses_field->label == 'file' && $count == 1 )
                {
                    $temp_array = array(
                        'expense_type_id'   => $expenses_type->id,
                        'label'             => $expenses_field->label,
                        'type'              => $expenses_field->type,
                        'required'          => $expenses_field->required,
                        'default'           => '1',
                    );
                    array_push( $new_fields, $temp_array );
                }
                else
                {
                    $temp_array = array(
                        'expense_type_id'   => $expenses_type->id,
                        'label'             => $expenses_field->label,
                        'type'              => $expenses_field->type,
                        'required'          => $expenses_field->required,
                        'default'           => ( $expenses_field->label == 'file' ) ? 1 : $expenses_field->default,
                    );
                    array_push( $new_fields, $temp_array );
                }
                $count ++;
            }
            
            if( $count == 1 ) 
                {
                    if( $expenses_field->label != 'file' )
                    {
                        $temp_array = array(
                            'expense_type_id'   => $expenses_type->id,
                            'label'             => 'file',
                            'type'              => 'file',
                            'required'          => '0',
                            'default'           => 1,
                        );
                        array_push( $new_fields, $temp_array );
                    }
                }
            $deletedRows = DB::table('expenses_fields')->where( 'expense_type_id', $expenses_type->id )->delete();
            $query = DB::table('expenses_fields')->insert($new_fields);

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
