<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asset_management extends Model
{
	protected $table = 'asset_management';

	protected $fillable = [
		'company_id',
		'allocated_to',
		'title',
		'description',
		'status',
		'purchase_date',
		'sale_disposal_date',
		'value',
        'note'
	];
}