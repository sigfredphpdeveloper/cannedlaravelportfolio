<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee_recognition_board extends Model
{
    protected $table = 'employee_recognition_board';

    protected $fillable = [
    	'id',
    	'owner_id',
    	'company_id',
    	'employee_id',
    	'title',
    	'message',
    	'status',
    	'created_at',
    	'updated_at'
    ];
}
