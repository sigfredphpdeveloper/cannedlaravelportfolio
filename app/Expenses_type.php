<?php

namespace App;
use App\Expenses_fields;

use Illuminate\Database\Eloquent\Model;

class Expenses_type extends Model
{
    protected $table = 'expenses_type';
    protected $fillable = array('expense_type_id', 'label', 'type', 'required');

    // treated as carbon dates
    protected $dates = [];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }
    

    public function expenses_fields()
    {
    	return $this->hasMany('App\Expenses_fields', 'expense_type_id');
    }

    public function addDefaultFields()
    {

        $data = [ 
                array(
                    'expense_type_id'   => $this->id,
                    'label'             => 'amount',
                    'type'              => 'number',
                    'required'          => 1,
                    'default'           => 1
                ),
                array(
                    'expense_type_id'   => $this->id,
                    'label'             => 'file',
                    'type'              => 'file',
                    'required'          => 0,
                    'default'           => 1
                )
        ];

        $expense_field = Expenses_fields::insert( $data );
    }

}
