<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Training extends Model
{
    protected $table = 'topics';

    public function pages()
    {
    	return $this->hasMany('App\Pages', 'topic_id')->orderBy('order', 'asc');
    }

    public function user_hasAccess( $user )
    {
        if( empty( $user ) )
            return false;

        if( $user->isAdmin() )
            return true;

        if( $this->access_everyone == 1 )
            return true;

        $topic_roles = json_decode( $this->access_roles );
        if( !empty( $topic_roles ) )
        {
            foreach( $user->company_user_roles as $company_user_role )
            {
                if( in_array( $company_user_role->id , $topic_roles) )
                    return true;
            }
        }

        $topic_teams = json_decode( $this->access_teams );
        if( !empty( $topic_teams ) )
        {
            foreach( $user->user_teams as $user_team )
            {
                if( in_array( $user_team->id , $topic_teams) )
                    return true;
            }
        }

        return false;
    }

    public function user_progress( $user_id ) {
    	$topic = TopicPagesTracker::where([ 'user_id' => $user_id, 'topic_id' => $this->id ])->first();

        if( !$topic )
            return 0;

    	$progress = $topic->last_page_view;
    	$total_pages = Pages::where( 'topic_id', $this->id )->count();

        if( $total_pages == 0 )
            return 0;
        
    	return round( ( $progress / $total_pages ) * 100 );
    }
}
