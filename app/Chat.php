<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use DB;

class Chat extends Model
{
    use SoftDeletes;

    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'chat';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    
    protected $guarded = [];

    /**
     * The fields that should be treated as Carbon date instances
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * returns the chat lines for this chat
     */
    public function chat_lines(){
        return $this->hasMany('App\ChatLine');
    }

    public static function name($chat,$current_user)
    {
        if($chat->chat_type == 'group'){
            return $chat->chat_name;
        } else {      
            foreach ($chat->users as $user) {
                if($user->id !== $current_user->id){
                    return $user->first_name." ".$user->last_name;
                }
            }
        }
    }

    public function users(){
        return $this->belongsToMany('App\User', 'chat_users')->withPivot("last_read_message");
    }

    /**
     * Check if a One To One chat exists between these two users already
     */
    public static function existingOneToOne($user_id_1, $user_id_2){
        $sql  = "select * from chat_users cu1 ";
        $sql .= "inner join chat_users cu2 on cu1.chat_id = cu2.chat_id ";
        $sql .= "inner join chat c on c.id = cu1.chat_id ";
        $sql .= "where c.chat_type = 'one-to-one' ";
        $sql .= "and cu1.user_id = ? and cu2.user_id = ?";
        $result = DB::select(
            $sql,
            [$user_id_1,$user_id_2]
        );

        if(count($result) > 0){
            return true;
        }

        return false;
    }

    /** 
     * Gets the group name of group
     */ 
    public static function determineGroupName($chat,$current_user,$reverse = null){
        
        if(isset($chat->chat_name) && $chat->chat_name){
            return $chat->chat_name;
        }

        if($chat->chat_type == 'group'){
            $name = "";
            foreach ($chat->users as $user) {
                $name .= $user->first_name . ", ";
            }
            return substr($name, 0, -2);
        }else{
            
            if($reverse){
                //we want to find the groupname for the other user
                //i.e., the opposite name that current user would see
                return $current_user->first_name." ".$current_user->last_name;
            }

            foreach ($chat->users as $user) {
                if($user->id !== $current_user->id){
                    return $user->first_name." ".$user->last_name;
                }
            }
        }
    }


    public static function determineGroupPhoto($chat,$current_user,$reverse=null){
        if(!empty($chat->photo)){
            return $chat->photo;
        }

        if($chat->chat_type == 'group'){
            return asset('img/defaultIcon.png');
        }else{
            
            if($reverse){
                //we want to find the groupname for the other user
                //i.e., the opposite name that current user would see
                return !empty($current_user->photo) ? $current_user->photo : asset('img/defaultIcon.png');
            }

            foreach ($chat->users as $user) {
                if($user->id !== $current_user->id){
                    return !empty($user->photo) ? $user->photo : asset('img/defaultIcon.png');
                }
            }
        }
    }
    public static function hasGuest($chat) {
        foreach ($chat->users as $user) {
            if($user->guest_company()){
                return true;
            }
        } 
        return false;
    }
}
