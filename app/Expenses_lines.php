<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expenses_lines extends Model
{
    protected $table = 'expenses_lines';

    // treated as carbon dates
    protected $dates = [];

    public function expenses()
    {
    	return $this->belongsTo('App\Expenses');
    }

    public function expenses_types()
    {
    	return $this->belongsTo('App\Expenses_type', 'expense_type_id');
    }
}
