<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyWorkWeek extends Model
{
    protected $table = 'company_work_week';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'company_id',
        'id',
        'title',
        'monday',
        'tuesday',
        'wednesday',
        'thursday',
        'friday',
        'saturday',
        'sunday'
    ];


    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
