<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Crypt;

class PayrollItemAdditions extends Model
{

    protected $table = 'sg_payroll_item_addition';

    protected $fillable = [
        'addition_id',
        'item_id',
        'user_id',
        'amount',
        'title'
    ];

    protected $encryptable = [
        'amount'    
    ];

    public function getAttributeValue($key)
    {
        $value = parent::getAttributeValue($key);

        if (in_array($key, $this->encryptable) && ( ! is_null($value)) && (! is_numeric($value)))
        {
            $value = Crypt::decrypt($value);
        }
 
        return $value;
    }

    public static function boot()
    {
        parent::boot();

        PayrollItemAdditions::saving(function($object)
        {
            if ( $object->amount)  $object->amount= Crypt::encrypt($object->amount);
            return $object;
        });
    }

    public function payroll_item()
    {
        return $this->belongsToMany('App\PayrollItem');
    }

    public function addition(){

        return $this->hasOne('App\PayrollAddition','id','addition_id');

    }

}
