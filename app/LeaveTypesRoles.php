<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveTypesRoles extends Model
{
    protected $table = 'leave_types_roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     *
     *
     */

    public $timestamps = false;
    protected $fillable = [
        'leave_type_id',
        'role_id'
    ];
}
