<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use DB;

class Organization extends Model
{
    protected $table = 'organization';
    public $timestamps = false;

    use SearchableTrait;

    protected $fillable =[
        'owner_id',
        'company_id',
        'organization_name',
        'organization_address',
        'visibility',
        'nb_of_contacts',
        'extra_fields',
        'created_date',
        'last_update_date'
    ];

    protected $searchable = [
        'columns' => [
            'organization.organization_name' => 5,
            'organization.organization_address' => 5,
            'organization.visibility' => 5,
            'users.first_name' => 5,
            'users.last_name' => 5,
            'users.title' => 5,
            'users.email' => 5
        ],
        'joins' => [
            'users' => ['organization.owner_id','users.id'],
        ],
    ];


    public function contact()
    {
        return $this->belongsTo('App\Contact');
    }

	public function organization_fields()
    {
        return $this->hasMany('App\Organization_fields');
    }

    public static function getCrmOrganizations($company_id)
    {
        return Organization::select('*',DB::raw('count(deals.id) as deals_nb'),'organization.id AS org_id',DB::raw('count(contacts.id) as nb_contacts'),'organization.id as organization_id')
            ->join('teams', 'teams.company_id','=','organization.company_id')
            ->leftJoin('contacts', 'contacts.organization_id','=','organization.id')
            ->leftJoin('deals', function($join){
                $join->on('deals.contact_id','=','contacts.id');
                $join->where('deals.status', '!=', 'DELETE');
                $join->where('deals.status', '!=', 'ARCHIVED');
            })
            ->join('users','deals.owner_id','=','users.id')->where('organization.company_id',$company_id)
            ->groupBy('organization.id');
    }

    public static function getOrganizationContacts($company_id, $search = null)
    {
        $organizations = Organization::select('*','organization.id AS organization_id', DB::raw('count(contacts.id) as nb_contacts'))
            ->join('users', 'organization.owner_id','=','users.id')
            ->leftJoin('contacts', 'contacts.organization_id','=','organization.id')
            ->where('organization.company_id', $company_id)
            ->orderBy('organization.id', 'ASC')
            ->groupBy('organization.id');

        if(!empty($search)){
            $organizations = $organizations->where('organization.organization_name', 'like', '%'.$search.'%');
        }

        return $organizations;
    }


}
