<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content_permissions extends Model
{

    public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'content_permissions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'content_id',
        'role_id',
        'team_id',
        'read_access',
        'write_access',
        'no_access',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The fields that should be treated as Carbon date instances
     *
     * @var array
     */


    public function content()
    {
        return $this->belongsToMany('App\Content');
    }

}
