<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use DB;

class Deal extends Model
{
    protected $table = 'deals';
    public $timestamps = false;

    use SearchableTrait;

    protected $fillable = [
        'deal_title',
        'crm_board_id',
        'contact_id',
        'organization_id',
        'owner_id',
        'deal_value',
        'stage',
        'visibility',
        'create_date',
        'last_update_date',
        'last_stage_change_date',
        'next_activity_date',
        'won_date',
        'lost_date',
        'lost_reason',
        'total_activity',
        'done_activity',
        'undone_activity',
    ];

    protected $searchable = [
        'columns' => [
            'deals.deal_title' =>10,

            'contacts.contact_name' => 10,
            'contacts.contact_email' => 10,
            'contacts.position' => 10,
            'contacts.address' => 10,

            'organization.organization_name' => 10,
            'organization.organization_address' => 10,
            'organization.visibility' => 5,

        ],
        'joins' => [
            'users' => ['contacts.owner_id','users.id'],
            'organization' => ['organization.id','contacts.organization_id'],
        ],
    ];


    public function activity()
    {
        return $this->hasMany('App\Activity');
    }

    public function teams()
    {
        return $this->hasMany('App\teams');
    }

    public function deal_teams()
    {
        return $this->hasMany('App\Deal_teams');
    }

    public static function getCrmDeals()
    {
        return Deal::select('*','deals.id AS deal_id','deals.visibility AS visibility', 'deals.owner_id as owner_id','deals.status as status','deals.stage as stage')
            ->join('contacts','contacts.id','=','deals.contact_id')
            ->join('organization AS org','org.id','=','contacts.organization_id')
            ->join('users','deals.owner_id','=','users.id')
            ->join('stages','stages.id','=','deals.stage');
    }

    public function board()
    {
        return $this->belongsTo('App\Crm_board');
    }



}
