<?php

namespace App\Listeners;

use App;
use App\User;
use App\AuthToken;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AuthLoginEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  User  $event
     * @param  string  $remember_token
     * @return void
     */
    public function handle(User $user, $remember_token)
    {

        $user->last_login = date('Y-m-d H:i:s');
        $user->save();

        //STEP 1 Check if user has auth unexpired token type web
        $token = $user->findOrCreateToken(AuthToken::WEB);

        session()->put('jwt', $token->toJWT($user));


        //master user
        $master_company = $user->default_company();
        $guest_company = $user->guest_company();

        if($master_company){
            session()->put('current_company',$master_company);
        }else{
            session()->put('current_company',$guest_company);
        }

    }
}
