<?php

namespace App\Listeners;

use App;
use App\PushNotification;
use Redis;


class PushNotificationEventListener
{
    /**
     *  Publish messages to channel for a specific user's group
     *
     *  @param $array
     *  @return void
     *
     */

    public function pushNotification($array) {
        $redis = Redis::connection();
        $redis->publish('user_notifications', json_encode($array));
    }
}
