<?php

namespace App\Listeners;

use App;
use App\MobileUser;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use \Firebase\JWT\JWT;

class MobileAuthLoginEventListener
{
	/**
     * Handle the event.
     *
     * @param  MobileUser  $user
     * @return jwt token
     */
    public function handle(MobileUser $user){
    	$token = $user->toArray();
    	$jwt = JWT::encode($token, config('app.jwt_secret'));

    	return $jwt;
    }
}