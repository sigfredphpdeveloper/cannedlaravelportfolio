<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLeaveAllowance extends Model
{
    public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_leave_allowance';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idf',
        'user_id',
        'allowance',
        'remaining',
        'leave_types_id'
    ];

    public function User()
    {
        return $this->belongsTo('App\User');
    }


    public function leave_type(){
        return $this->belongsTo('App\Leave_types','leave_types_id');
    }
}
