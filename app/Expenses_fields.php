<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expenses_fields extends Model
{
    protected $table = 'expenses_fields';

    protected $fillable = [
        'expense_type_id',
        'label',
        'type',
        'required'
    ];

    // treated as carbon dates
    protected $dates = ['created_at', 'updated_at'];

    public function expenses_type()
    {
    	return $this->belongsTo('App\Expenses_type', 'expense_type_id');
    }
}
