<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pages extends Model
{
    protected $table = 'topic_pages';

    public function topic()
    {
    	return $this->belongsTo('App\Training', 'topic_id'); 
    }
}
