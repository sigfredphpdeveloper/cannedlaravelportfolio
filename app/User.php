<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Contracts\Billable as BillableContract;
use DB;
use Auth;
use Carbon\Carbon;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Leave_types;
use app\Roles;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract,
                                    BillableContract
{
    use Authenticatable, Authorizable, CanResetPassword, Billable;
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    use SearchableTrait;

    protected $searchable = [
        'columns' => [
            'users.first_name' => 10,
            'users.last_name' => 10,
            'users.email' => 10
        ],
        'joins' => [ ],
    ];

    protected $fillable = [
        'title',
        'first_name',
        'last_name',
        'email',
        'password',
        'company_url',
        'position',
        'photo',
        'phone_number',
        'invite_hash',
        'completed_profile',
        'invited_by',
        'invited_on',
        'last_login',
        'custom_keypairs',
        'status',
        'activate_hash',
        'first_visit',
        'start_date',
        'work_week_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * The fields that should be treated as Carbon date instances
     *
     * @var array
     */
    protected $dates = ['trial_ends_at', 'subscription_ends_at','invited_on','last_login','deleted_at'];

    public function company_user(){
        return $this->belongsToMany('App\Company','company_user')->withPivot('is_master', 'is_guest');
    }

    public function employees()
    {
        return $this->belongsToMany('App\Company');
    }

    public function user_roles(){

        return $this->belongsToMany('App\Roles','user_roles','user_id','role_id');

    }

    public function company_user_roles(){

        $company = session('current_company');
        return $this->belongsToMany('App\Roles','user_roles','user_id','role_id')->where('company_id', $company->id );

    }

    public function user_teams(){

        return $this->belongsToMany('App\Teams','user_teams','user_id','team_id');

    }

    public function chats(){
        return $this->belongsToMany('App\Chat', 'chat_users');
    }

    public function companies(){
        return $this->belongsToMany('App\Company','company_user')->withPivot('is_master', 'is_guest');
    }

    public function default_company(){
        return $this->belongsToMany('App\Company','company_user')
                    ->withPivot('is_master', 'is_guest')
                    ->where(function($query){
                        $query->where('is_master',1)
                            ->orWhere(function($nested_query){
                                $nested_query->where('is_guest',0)
                                ->where('is_master',0);
                            });
                    })
                    ->first();
    }

    public function guest_company(){
        return $this->belongsToMany('App\Company','company_user')
                    ->withPivot('is_master', 'is_guest')
                    ->where(function($query){
                        $query->Where(function($nested_query){
                                $nested_query->where('is_guest',1)
                                ->where('is_master',0)
                                ->where('guest_default',1);
                            });
                    })
                    ->first();
    }

    public function guest_companies(){
        return $this->belongsToMany('App\Company','company_user')
                    ->withPivot('is_master', 'is_guest')
                    ->where(function($query){
                        $query->where('is_guest',1)
                            ->orWhere(function($nested_query){
                                $nested_query->where('is_guest',1)
                                ->where('is_master',0);
                            });
                    })
                    ->get();
    }

    /**
     * Returns true if all users belong to the same company
     * @param Array $user_ids
     */
    public static function belongsToSameCompany($user_ids){
        //TODO modify to check if users belong to the same CURRENTLY SELECTED company
        $result = DB::select(
                        "select * from company_user where user_id in (". implode(",",$user_ids) .") group by company_id having count(*) = ?",
                        [count($user_ids)]
                    );
        if(count($result) > 0){
            return true;
        }

        return false;
    }

    public function referrals()
    {
        return $this->hasMany('App\Referrals');
    }

    public function announcements()
    {
        return $this->hasMany('App\Announcements');
    }

    public function endpoints()
    {
        return $this->hasMany('App\User_endpoint');
    }

    public function expenses()
    {
        return $this->hasMany('App\Expenses');
    }
    
    public function emergency_contact()
    {
        return $this->hasOne('App\EmergencyContact');
    }
    public function isAdmin()
    {
        $roles = $this->user_roles->toArray();
        foreach($roles as $role){
            if($role['role_name'] == 'Administrator'){
                return true;
            }
        }
        return false;
    }
    
    public function leaves()
    {
        return $this->hasMany('App\Users_leaves');
    }

    public function leaves_dataset($user_id=NULL,$where = NULL,$admin = TRUE){

        $where_string = '';


        if($where){
            $where_string = ' AND '.$where;
        }

        if($admin){
            $result = DB::select(
                "select users_leaves.*,leave_types.type,users.first_name,users.last_name,users.email from users_leaves
                INNER JOIN leave_types ON users_leaves.leave_types_id = leave_types.id
                INNER JOIN users ON users.id = users_leaves.user_id
                 where $where"
            );
        }else{
            $result = DB::select(
                "select users_leaves.*,leave_types.type,users.first_name,users.last_name,users.email from users_leaves
                INNER JOIN leave_types ON users_leaves.leave_types_id = leave_types.id
                INNER JOIN users ON users.id = users_leaves.user_id
                 where users_leaves.user_id =  ? $where_string",
                [$user_id]
            );
        }

        return $result;
    }

    public  function leave_types_set(){

        $user_roles = $this->user_roles->pluck('id')->all();

        $leave_types = Leave_types::select('leave_types.*')
            ->join('leave_types_roles', 'leave_types_roles.leave_type_id','=','leave_types.id')
            ->whereIn('leave_types_roles.role_id',$user_roles)
            ->groupBy('leave_types.id')
            ->get();


        return $leave_types;
    }

    public function leave_allowances()
    {
        return $this->hasMany('App\UserLeaveAllowance');
    }

    function leave_allowances_array($leave_types_id,$role_ids,$user_id){

        $role_ids = implode(',',$role_ids);

        $result = DB::select(
            "SELECT user_leave_allowance.id allowance_id,allowance,remaining,leave_types.type FROM user_leave_allowance
            INNER JOIN leave_types ON leave_types.id = user_leave_allowance.leave_types_id
            INNER JOIN leave_types_roles on leave_types_roles.leave_type_id = leave_types.id
            WHERE leave_types_id = ? AND leave_types_roles.role_id IN (?) AND user_id = ?",
            [$leave_types_id,$role_ids,$user_id]
        );

        return $result;

    }
    function team($user_id)
    {
        return User_teams::select('*','user_teams.id AS id')
                ->join('teams','teams.id','=','user_teams.team_id')->where('user_teams.user_id',$user_id)->get()->toArray();
    }

    public function auth_tokens(){
        return $this->hasMany('App\AuthToken');
    }

    public function activeTokens($type = null){
        $now = Carbon::now();
        $activeTokens = $this->auth_tokens()->where('expires_at', '>=', $now)->where('user_id', $this->id);
        if(!empty($type)){
            $activeTokens->where('type', $type);
        }
        return $activeTokens;
    }

    /**
     * Finds the first active token for a user
     * @return App\AuthToken || null
     */
    public function findFirstToken($type = null){
        return $this->activeTokens($type)->first();
    }

    /**
     * Finds the first active token for a user. 
     * If the token doesn't exist one will be created.
     * @return App\AuthToken
     */
    public function findOrCreateToken($type = null){
        if(empty($type)){
            $type = AuthToken::WEB;
        }
        $token = $this->findFirstToken($type);

        if(empty($token)){
            //create token
            $token = new AuthToken($type);
            $token = $this->auth_tokens()->save($token);
        }
        return $token;
    }

    public function hasPermission($key) {
        if ($this->isAdmin()) {
            return true;
        }
        $user_roles = $this->user_roles;

        foreach($user_roles as $role) {
            $roles = Roles::find($role->id);

            foreach($roles->role_permissions as $per) {
                if($per->key == $key) {
                    return true;
                }
            }
        }
        return false;
    }


    public function employee_data()
    {
        return $this->hasOne('App\SGEmployeeData');
    }

    public function payroll_items()
    {
        return $this->hasMany('App\PayrollItem');
    }

    public function leave_notes()
    {
        return $this->hasMany('App\LeaveNotes');
    }

    public function leave_notes_from()
    {
        return $this->belongsTo('App\User');
    }

    public function work_week()
    {
        return $this->hasOne('App\CompanyWorkWeek', 'id', 'work_week_id');

    }

     public function approver()
    {
        return $this->belongsTo('App\User');
    }

    public function all_remaining_leaves($leave_type_id){

        $remaining = 0 ;

        if($this->leave_allowances()->where('leave_types_id', $leave_type_id)->first()){

            $remaining = $remaining + $this->leave_allowances()->where('leave_types_id', $leave_type_id)->first()->remaining;

        }

        return $remaining;
    }

}

   
