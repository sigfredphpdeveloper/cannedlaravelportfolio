<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayrollDeduction extends Model
{


    protected $table = 'payroll_deductions';


    protected $fillable = [
        'company_id',
        'title',
        'cpf_deductible',
        'default_deduction'
    ];

    public function company()
    {
    	return $this->belongsTo('App\Company', 'company_id');
    }
}
