<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expenses_roles extends Model
{
    protected $table = 'expenses_roles';

    // treated as carbon dates
    protected $dates = [];

    public function company()
    {
    	return $this->belongsTo('App\Company');
    }

    public function roles()
    {
    	return $this->belongsTo('App\Roles');
    }
}
