<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tasks_list extends Model
{
    protected $table = 'tasks_lists';

    protected $fillable = [
        'company_id',
        'list_title',
        'create_date',
        'last_update_date',
        'position',
        'board_id'
    ];
    public $timestamps = false;
}
