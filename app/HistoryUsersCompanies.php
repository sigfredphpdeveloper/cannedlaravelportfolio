<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryUsersCompanies extends Model
{

    public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'history_users_companies';


    protected $fillable = [
        'date',
        'day',
        'month',
        'year',
        'total_user',
        'total_company',
    ];

}
