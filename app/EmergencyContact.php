<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;


class EmergencyContact extends Model
{
    protected $table = 'emergency_contact';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'user_id',
        'contact_name',
        'contact_phone',
        'contact_address',
        'relationship',
        'relationship_other',

    ];

    use SearchableTrait;

    protected $searchable = [
        'columns' => [
            'emergency_contact.contact_name' => 10,
            'emergency_contact.contact_phone' => 10,
            'emergency_contact.contact_address' => 10,
            'emergency_contact.relationship' => 10
        ],
        'joins' => [ ],
    ];

    protected $hidden = [];

    /**
     * The fields that should be treated as Carbon date instances
     *
     * @var array
     */
    protected $dates = [];

     public function users()
    {
        return $this->belongsTo('App\User');
    }

}
