<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Files extends Model
{
    protected $table = 'files';


    protected $fillable = [
        'file_name',
        'file_path',
        'owner_id',
        'deal_id'
    ];


}
