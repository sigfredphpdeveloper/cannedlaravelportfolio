<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Nicolaslopezj\Searchable\SearchableTrait;

class Users_leaves extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users_leaves';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     *
     *
     */
    protected $fillable = [
        'user_id',
        'leave_types_id',
        'date',
        'date_type',
        'details',
        'status',
        'date_from',
        'date_from_type',
        'date_to',
        'date_to_type',
        'credits',
        'leave_application_type',
        'approved_date',
        'approved_by'
    ];

    use SearchableTrait;

    protected $searchable = [
        'columns' => [
            'user_leaves.details' => 10,
            'user_leaves.status' => 10,
        ],
        'joins' => [ ],
    ];



    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The fields that should be treated as Carbon date instances
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];


    public function User()
    {

        return $this->belongsTo('App\User');
    }

    public static function getManyUserLeaves($user_ids,$search = NULL,$status = NULL){

        $user_leaves = Self::select('users_leaves.*','leave_types.type','leave_types.color_code','users.first_name','users.last_name')
            ->join('leave_types', 'users_leaves.leave_types_id','=','leave_types.id')
            ->join('users', 'users.id','=','users_leaves.user_id')
            ->whereIn('users_leaves.user_id',$user_ids);

        if(!empty($search)){
            $user_leaves->search($search);
        }

        if(!empty($status)){
            $user_leaves->where('users_leaves.status','=',$status);
        }

        return $user_leaves;
    }

    public function Leave_types()
    {
        return $this->belongsTo('App\Leave_types','leave_types_id');
    }

     public function approver()
    {
        return $this->hasOne('App\User', 'id', 'approved_by');

    }

}