<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TopicPagesTracker extends Model
{
    protected $table = 'topic_pages_tracker';

    protected $fillable = [
        'user_id',
        'topic_id',
        'last_page_view'
    ];
}
