<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;
use \Firebase\JWT\JWT;

class AuthToken extends Model
{
    use SoftDeletes;

    const WEB = "web";
    const ANDROID = "android";
    const IOS = "ios";
    const OTHER = "other";

    protected $dates = ['expires_at','deleted_at'];

    /**
     * Retrives an AuthToken instance from a jwt
     * @param String $jwt encoded JWT
     * @param bool $with_user populate user or not
     */
    public static function retrieve($jwt, $with_user = false){
    	//decode and retrieve jwt
    	$decoded = JWT::decode($jwt, config('app.jwt_secret'), config('app.jwt_alg'));

        if($with_user){
            return static::with('user')->find($decoded->jti);
        }
        
    	return static::find($decoded->jti);
    }

    /**
     * Create a new AuthToken model instance.
     *
     * @param  String $type
     * @return void
     */
    public function __construct($type = null, $attributes = array())
    {	
    	parent::__construct($attributes);
        // if($type === static::WEB){
        // 	$this->expires_at = $this->defaultWebExpiryTime();
        // }
        $this->expires_at = $this->defaultWebExpiryTime();
    }

    public function user(){
    	return $this->belongsTo('App\User');
    }

    private function defaultWebExpiryTime(){
    	return Carbon::now()->addWeek();
    }

    /**
     * Convert this auth token to a JWT
     * @param App\User (optional) if there is an already populated user object it can be passed here to avoid unnecesary db calls
     */
    public function toJWT($user = null){
    	if(empty($user)){
    		$user = $this->user;
    	}

    	$claims = [
    		'sub' => ['id' => $user->id, 'first_name' => $user->first_name, 'last_name' => $user->last_name],
    		'exp' => (!empty($this->expires_at) ? $this->expires_at->timestamp : $this->expires_at),
    		'jti' => $this->id
    	];

    	return JWT::encode($claims, config('app.jwt_secret'));
    }
}
