<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_roles extends Model
{

    public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'role_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The fields that should be treated as Carbon date instances
     *
     * @var array
     */
    protected $dates = [];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function expenses_roles()
    {
        return $this->hasMany('App\Expenses_roles', 'role_id');
    }

}
