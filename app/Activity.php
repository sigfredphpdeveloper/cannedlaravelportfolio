<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $table = 'activity';
    public $timestamps = false;

    protected $fillable = [
        'activity_title',
        'deal_id',
        'activity_type',
        'date',
        'time',
        'duration',
        'owner_id',
        'assigned_user_id',
        'done',
        'created_date',
    ];

    public function Deal()
    {
        return $this->belongsTo('App\Deal');
    }

}
