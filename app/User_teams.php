<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_teams extends Model
{

    public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_teams';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'team_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The fields that should be treated as Carbon date instances
     *
     * @var array
     */
    protected $dates = [];

    public function user(){

        return $this->belongsTo('App\User');

    }
}
