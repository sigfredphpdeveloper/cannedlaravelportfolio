<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tasks extends Model
{
    protected $table = 'tasks';

    protected $fillable = [
        'board_id',
        'list_id',
        'task_title',
        'task_details',
        'assigned_user_id',
        'category',
        'due_date',
        'create_date',
        'last_update_date',
        'status',
        'list',
        'visibility',
        'start_date',
        'duration'
    ];
    public $timestamps = false;

    public function board_teams()
    {
        return $this->hasMany('App\board_team');
    }

}
