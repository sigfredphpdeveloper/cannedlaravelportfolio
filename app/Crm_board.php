<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Crm_board extends Model
{
    protected $table = 'crm_board';

    protected $fillable = [
        'company_id',
        'owner_id',
        'board_title',
        'visibility',
        'created_date',
        'last_update_date',
    ];

    public $timestamps = false;

    public function teams()
    {
        return $this->hasMany('App\Teams');
    }

    public function board_teams()
    {
        return $this->hasMany('App\Crm_board_teams');
    }
    public function deals()
    {
        return $this->hasMany('App\Deal');
    }

}
