<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payroll extends Model
{

    protected $table = 'sg_payroll';

    protected $fillable = [
        'user_id',
        'company_id',
        'month',
        'year',
        'payment_date',
        'status'
    ];

    public function items()
    {
        return $this->hasMany('App\PayrollItem');
    }

    public function items_orderId()
    {
        return $this->hasMany('App\PayrollItem')->orderBy('sg_payroll_item.id','ASC');
    }



}
