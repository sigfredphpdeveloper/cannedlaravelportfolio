<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Nicolaslopezj\Searchable\SearchableTrait;

class Roles extends Model
{


    public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'roles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_name',
        'role_description',
        'company_id',
        'default_role'
    ];

    use SearchableTrait;

    protected $searchable = [
        'columns' => [
            'roles.role_name' => 10,
            'roles.role_description' => 10,
        ],
        'joins' => [],
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The fields that should be treated as Carbon date instances
     *
     * @var array
     */
    protected $dates = [];

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function expenses_roles()
    {
        return $this->hasMany('App\Expenses_roles', 'role_id');
    }

    public function role_permissions(){
        return $this->belongsToMany('App\Permissions', 'role_permission', 'role_id', 'permission_id');
    }

    public function announcements()
    {
        return $this->belongsToMany('App\Announcements', 'announcement_roles', 'role_id', 'announcement_id');
    }


}
