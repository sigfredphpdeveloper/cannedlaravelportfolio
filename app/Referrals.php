<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


use DB;
use Nicolaslopezj\Searchable\SearchableTrait;

class Referrals extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'referrals';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     *
     *
     */
    protected $fillable = [
        'sender_company_id',
        'user_id',
        'referral_status',
        'invitee_email',
        'referral_code',
        'send_date',
        'referral_credit_date',
        'bonus_file_capacity'
    ];

    use SearchableTrait;

    protected $searchable = [
        'columns' => [
            'referrals.invitee_email' => 10,
            'referrals.referral_status' => 10,
        ],
        'joins' => [ ],
    ];



    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The fields that should be treated as Carbon date instances
     *
     * @var array
     */
    protected $dates = ['referral_credit_date','send_date','created_at', 'updated_at'];


    public function User()
    {
        return $this->belongsTo('App\User');
    }

}

