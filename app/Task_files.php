<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task_files extends Model
{
    protected $table = 'tasks_files';


    protected $fillable = [
        'file_name',
        'file_path',
        'task_id'
    ];
}
