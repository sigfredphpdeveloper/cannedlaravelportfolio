<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tasks_categories extends Model
{
    protected $table = 'tasks_categories';

    protected $fillable = [
        'company_id',
        'category_name'
    ];
}
