<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
use DB;

class ChatUser extends Model
{
    use SoftDeletes;

    public $timestamps = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'chat_users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    
    protected $guarded = [];

    /**
     * The fields that should be treated as Carbon date instances
     *
     * @var array
     */
    
    /**
     * returns the user for this chat_user
     */
    public function user(){
        return $this->belongsTo('App\User');
    }

}
