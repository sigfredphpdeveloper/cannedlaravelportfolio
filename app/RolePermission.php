<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolePermission extends Model
{

    public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'role_permission';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id',
        'permission_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The fields that should be treated as Carbon date instances
     *
     * @var array
     */
    protected $dates = [];


}
