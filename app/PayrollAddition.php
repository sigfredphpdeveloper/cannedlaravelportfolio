<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PayrollAddition extends Model
{

    protected $table = 'payroll_additions';

    protected $fillable = [
        'company_id',
        'title',
        'cpf_payable',
        'taxable',
        'other_allowance',
        'category_id',
        'created_at',
        'updated_at'
    ];

	public function company()
    {
    	return $this->belongsTo('App\Company', 'company_id');
    }
    
    public function tax_category()
    {
    	return $this->belongsTo('App\TaxCategory', 'category_id');
    }
}
