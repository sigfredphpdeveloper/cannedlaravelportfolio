<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*************************************************************************
// Public pages
*************************************************************************/
	
Route::get('home', 'PagefsController@home');

Route::get('/support-form', 'PublicController@support_form');
Route::post('/support-form', 'PublicController@support_email');

Route::get('/', function(){
    // if( Helper::detect_user_country() ){
    //     return view('public_home',['lang'=>'fr']);
    // }else
    // {
//        return view('page_finalhome',['lang'=>'en']);
        return view('public_home',['lang'=>'en']);
//    }
});

// Route::get('/fr', function(){
// 	return view('landing_page',['lang'=>'fr']);
// });

Route::get('/finalhome', function(){
	// if( Helper::detect_user_country() ){
	// 	return view('page_finalhome',['lang'=>'fr']);
	// }else
	// {
		return view('page_finalhome',['lang'=>'en']);
	// }
});


// Route::get('fr/finalhome', function(){
// 	return view('page_finalhome',['lang'=>'fr']);
// });


Route::get('/about-us', function(){
	// if( Helper::detect_user_country() ){
	// 	return view('page_about',['lang'=>'fr']);
	// }else
	// {
		return view('page_about',['lang'=>'en']);
	// }
});

// Route::get('fr/about-us', function(){
// 	return view('page_about',['lang'=>'fr']);
// });

Route::get('/price', function(){
	// if( Helper::detect_user_country() ){
	// 	return view('page_price',['lang'=>'fr']);
	// }else
	// {
		return view('page_price',['lang'=>'en']);
	// }
});

// Route::get('fr/price', function(){
// 	return view('page_price',['lang'=>'fr']);
// });

Route::get('/features', function(){

    return Redirect::to('product');

});

Route::get('/thankyou', function(){
	// if( Helper::detect_user_country() ){
	// 	return view('page_thankyou',['lang'=>'fr']);
	// }else
	// {
		return view('page_thankyou',['lang'=>'en']);
	// }
});

// Route::get('fr/features', function(){
// 	return view('page_feature',['lang'=>'fr']);
// });

Route::get('/privacy-policy', function(){
	// if( Helper::detect_user_country() ){
	// 	return view('page_privacy',['lang'=>'fr']);
	// }else
	// {
		return view('page_privacy',['lang'=>'en']);
	// }
});

// Route::get('fr/privacy-policy', function(){
// 	return view('page_privacy',['lang'=>'fr']);
// });

Route::get('/term-of-use', function(){
	// if( Helper::detect_user_country() ){
	// 	return view('page_terms',['lang'=>'fr']);
	// }else
	// {
		return view('page_terms',['lang'=>'en']);
	// }
});

// Route::get('fr/term-of-use', function(){
// 	return view('page_terms',['lang'=>'fr']);
// });


Route::get('/product', function(){
//    return view('page_product',['lang'=>'en']);
    return view('public_features',['lang'=>'en']);
});



/*******************************
 * Payroll public pages
 *******************************/
Route::get('/free-payroll-sg', function(){
	return view('payroll_public.home',['lang'=>'en']);
});

Route::post('email_signup', 'Auth\AuthController@email_signup');



Route::post('contact_us', 'ContactController@receive_message')->name('contact_us');
#Route::controller('adwords', 'AdwordsController');
#Route::controller('adwords-assist', 'AdwordsAssistController');


// These routes were originally on Laravel 5, re-implementing them here
// Login Routes
Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login',['middleware'=>'login_checks','uses'=> 'Auth\AuthController@postLogin']);
Route::get('logout', 'Auth\AuthController@getLogout');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

// Registration / Sign up routes
Route::get('register', 'Auth\AuthController@getRegister');
Route::post('register',['middleware'=>'registration','uses'=> 'Auth\AuthController@postRegister']);

// Pricing & Sign up;
//Route::get('pricing', 'PageController@pricing');
Route::get('pricing', function(){
    // if( Helper::detect_user_country() ){
    //     return view('page_pricing',['lang'=>'fr']);
    // }else
    // {
        return view('page_pricing',['lang'=>'en']);
    // }
});

Route::get('faq', function(){
    if( Helper::detect_user_country() ){
        return view('page_faq',['lang'=>'fr']);
    }else
    {
        return view('page_faq',['lang'=>'en']);
    }
});

//tutorial
Route::get('tutorials/{company_id}', 'TrainingController@tutorial');
Route::get('tutorials/{company_id}/{slug}', 'TrainingController@training_external');


Route::post('apply-leave-process', 'LeavesController@apply_leave_process');


Route::group(['middleware' => ['auth','localize']], function() {

	/*************************************************************************
	// User Private pages
	*************************************************************************/
	Route::get('dashboard', 'UserController@dashboard');

    Route::get('step1', 'UserController@step1');
    Route::get('step2', 'UserController@step2');
    Route::get('step3', 'UserController@step3');
    Route::get('step4', 'UserController@step4');
    Route::get('step5', 'UserController@step5');

	Route::get('upgrade', 'UserController@upgrade');

	Route::get('my_profile', 'UserController@my_profile');

    Route::post('dismiss_first', 'UserController@dismiss_first');

	Route::post('upgrade', 'UserController@do_upgrade');
	Route::post('complete_profile', 'UserController@complete_profile');

	Route::post('my_profile_save', 'UserController@my_profile_save');
	Route::post('my_company_save', 'CompanyController@my_company_save');
    Route::post('upload_company_pic', 'CompanyController@upload_company_pic');
    Route::any('upload_profile_pic/', ['uses' =>'UserController@upload_profile_pic']);

	/*************************************************************************
	// Chat
	 *************************************************************************/
	
	Route::get('chats', 'ChatController@get_user_chats_view');
	Route::get('company/getTeamsInCompany/{id}','CompanyController@getTeamsInCompany');
	

	/*************************************************************************
	// Announcements
	 *************************************************************************/
	Route::get('announcements', 'AnnouncementsController@index');
    Route::get('all_announcements', 'AnnouncementsController@all_announcements');
	Route::get('add_announcement', 'AnnouncementsController@add_announcement');
	Route::post('announcement_add_save', 'AnnouncementsController@announcement_add_save');
	Route::any('announcement_edit/{id}', ['uses' =>'AnnouncementsController@announcement_edit']);
	Route::any('announcement_delete/{id}', ['uses' =>'AnnouncementsController@announcement_delete']);
	Route::any('announcement/{id}', ['uses' =>'AnnouncementsController@view']);

	/*************************************************************************
        // Notes Pages
        *************************************************************************/
        Route::get('notes_view', 'NotesController@index');
        Route::any('update_personal_note', 'NotesController@updatePersonalNote');
        Route::post('update-note/{id}', 'NotesController@update');
        Route::get('notes', 'NotesController@notes');
        Route::post('delete-note', 'NotesController@destroy');
        Route::get('view-note/{id}', 'NotesController@show');
	/*************************************************************************
	// Files Manager pages
	 *************************************************************************/

	Route::get('files_manager', 'FilesController@manager');
	Route::any('files_manager/{id}', ['uses' =>'FilesController@manager']);
	Route::post('add_folder_save', 'FilesController@add_folder_save');
	Route::any('upload_file/{id}', ['uses' =>'FilesController@upload_file']);
	Route::post('upload_file_process', 'FilesController@upload_file_process');
	Route::any('delete_file/{id}', ['uses' =>'FilesController@delete_file']);
	Route::any('delete_folder/{id}', ['uses' =>'FilesController@delete_folder']);
	Route::any('download_file/{id}', ['uses' =>'FilesController@download_file']);
	Route::any('edit_folder/{parent_id}/{content_id}', ['uses' =>'FilesController@edit_folder']);
	Route::any('edit_folder_save/{parent_id}/{content_id}', ['uses' =>'FilesController@edit_folder_save']);
    Route::post('delete_file_all', 'FilesController@delete_file_all');
    Route::post('initialize_folder_move', 'FilesController@initialize_folder_move');



	/*************************************************************************
	// Directory pages
	 *************************************************************************/

    Route::get('directory', 'DirectoryController@index');

	Route::post('employee_assign_roles_save', 'DirectoryController@employee_assign_roles_save');
	Route::post('employee_assign_teams_save', 'DirectoryController@employee_assign_teams_save');

	Route::any('resend_invitation/{id}', ['uses' =>'DirectoryController@resend_invitation']);
	Route::any('user_view/{id}', ['uses' =>'DirectoryController@user_view']);
	Route::any('user_delete_front/{id}', ['uses' =>'DirectoryController@user_delete_front']);
    Route::any('user_import/', ['uses' =>'DirectoryController@user_import']);
    Route::any('user_import_process', 'DirectoryController@user_import_process');
    Route::any('directory_edit_user/{id}', ['uses' =>'DirectoryController@edit_user']);

	Route::get('export_directory', 'DirectoryController@export_directory');

    Route::get('/download_csv_users', function(){

        $filename = "ZenIntra Users Import.csv";
        $handle = fopen($filename, 'w+');
        fputcsv($handle, array('Email','Title','First Name','Last name','Position','Phone Number'));
        fclose($handle);

        $headers = array(
            'Content-Type' => 'text/csv',
        );

        return Response::download($filename, 'ZenIntra Users Import.csv', $headers);
    });


	//specific modules
	Route::post('save_role_permission_recognition_board', 'PermissionsController@save_role_permission_recognition_board');
	Route::post('save_role_permission_asset_management', 'PermissionsController@save_role_permission_asset_management');
    Route::post('save_role_permission_expense', 'PermissionsController@save_role_permission_expense');
    Route::post('save_role_permission_emergency_contact', 'PermissionsController@save_role_permission_emergency_contact');
    Route::post('save_role_permission_organization', 'PermissionsController@save_role_permission_organization');
    Route::post('save_role_permission_announcement', 'PermissionsController@save_role_permission_announcement');
    Route::post('training-permission', 'PermissionsController@save_role_permission_trainings');
    Route::post('save_role_permission_leaves', 'PermissionsController@save_role_permission_leaves');
    Route::post('save_role_permission_payroll', 'PermissionsController@save_role_permission_payroll');






	/*************************************************************************
	// Contact pages
	 *************************************************************************/
	Route::get('contact', 'ContactController@index');
	Route::post('contact', 'ContactController@create');
	Route::post('contact_json', 'ContactController@create_json');
	Route::any('contact_search', 'ContactController@contact_search');
	Route::any('contact_edit/{id}', ['uses' =>'ContactController@contact_edit']);
	Route::any('contact_delete/{id}', ['uses' =>'ContactController@contact_delete']);
	Route::post('contact_module_status', 'ContactController@contact_module_status');
	Route::get('contact_list', 'ContactController@contact_list');
	Route::post('create_organization_in_contact', 'ContactController@create_organization_in_contact');

	/*************************************************************************
	// Organization pages
	 *************************************************************************/
	Route::get('organization', 'OrganizationController@index');
	Route::post('organization', 'OrganizationController@create');
	Route::get('organization_list', 'OrganizationController@organization_list');
	Route::any('organization_search', 'OrganizationController@search');
    Route::post('get_contact', 'OrganizationController@get_contact');

	Route::post('create_organization_field','OrganizationController@create_organization_field');
	Route::any('organization_field_delete/{id}', ['uses' =>'OrganizationController@organization_field_delete']);

	Route::any('organization_field_edit/{id}','OrganizationController@organization_field_edit');
	Route::any('organization_edit/{id}', ['uses' =>'OrganizationController@organization_edit']);
	Route::any('organization_edit/{id}', ['uses' =>'OrganizationController@organization_edit']);
	Route::any('export-contacts/{type}', 'OrganizationController@export_contacts');

	Route::group( ['middleware' => 'expenses'], function(){

		/*************************************************************************
		// Expenses
		 *************************************************************************/
		// My expenses routes
		Route::get('my-expenses', 'ExpensesController@myexpenses');
        Route::any('approve_expense', 'ExpensesController@approve_expense');

		Route::get('add-expense', 'ExpensesController@create');
		Route::post('add-expense', 'ExpensesController@store');
		Route::get('edit-expense/{id}', 'ExpensesController@edit');
		Route::post('edit-expense/{id}', 'ExpensesController@update');
		Route::get('delete-expense/{id}', 'ExpensesController@delete');
		Route::post('destroy-expense/{id}', 'ExpensesController@destroy');
		Route::get('expense-item/{id}', 'ExpensesController@show');

		Route::post('send_to_approver', 'ExpensesController@send_to_approver');


		// Approve expenses route
		Route::group( ['middleware' => 'expensesroles:approve'], function(){
			Route::get('approve-expenses', 'ExpensesController@approve_expenses');
			Route::get('approve-expenses-item/{id}', 'ExpensesController@approve_expenses_show');
			Route::post('approve-expenses-item/{id}', 'ExpensesController@approve_expenses_update');
			Route::post('approve-this-expense', 'ExpensesController@approve_this_expense');
		});

		Route::group( ['middleware' => 'expensesroles:pay'], function(){
			Route::get('pay-expenses', 'ExpensesController@pay_expenses');
			Route::get('pay-expenses-item/{id}', 'ExpensesController@show_pay_expenses_item');

			Route::get('pay-expense/{id}', 'ExpensesController@pay_expense_item');
			Route::post('pay-expenses-item/{id}', 'ExpensesController@pay_expenses_update');
		});

        Route::post('resubmit_details/{id}', 'ExpensesController@resubmit_details');

		// Expenses type routes
		Route::get('add-expense-type', 'ExpensesTypeController@create');
		Route::post('add-expense-type', 'ExpensesTypeController@store');
		Route::get('edit-expense-type/{id}', 'ExpensesTypeController@edit');
		Route::post('edit-expense-type/{id}', 'ExpensesTypeController@update');
		Route::get('delete-expense-type/{id}', 'ExpensesTypeController@delete');
		Route::post('destroy-expense-type/{id}', 'ExpensesTypeController@destroy');
		Route::get('exepenses-type-manage-fields/{id}', 'ExpensesTypeController@managefields');
		Route::post('save-expenses-type-fields', 'ExpensesFieldsController@store');

		// Expenses Lines Routes
		Route::get('add-expense-lines/{expense_id}', 'ExpensesLinesController@create');
		Route::get('get-fields/{expense_id}/{id}', 'ExpensesLinesController@get_fields');
		Route::post('save-expense-lines', 'ExpensesLinesController@store');
		Route::get('edit-expense-lines/{id}', 'ExpensesLinesController@edit');
		Route::post('edit-expense-lines/{id}', 'ExpensesLinesController@update');
		Route::get('delete-expense-line/{id}/{expense_id}', 'ExpensesLinesController@delete');
		Route::post('destroy-expense-line/{id}', 'ExpensesLinesController@destroy');

		// Expenses roles routes
		Route::get('edit-expenses-role/', 'ExpensesRolesController@updaterole');

		Route::get('expenses-history', 'ExpensesController@history');
		Route::get('expenses-report', 'ExpensesController@report');

		Route::get('get-expense-data-status', 'ExpensesController@expenseStatusDataByDate');
		Route::get('get-expense-data-types', 'ExpensesController@expenseTypeDataByDate');

	} );

	Route::group( ['middleware' => 'trainings'], function(){

		Route::get('trainings', 'TrainingController@index');
		Route::get('training/{id}', 'TrainingController@training_internal');
		Route::get('all-topics', 'TrainingController@manage');
		Route::get('topic-statistics/{id}', 'TrainingController@statistics');
		Route::get('add-topic', 'TrainingController@create');
		Route::post('add-topic', 'TrainingController@store');
		Route::get('edit-topic/{id}', 'TrainingController@edit');
		Route::post('edit-topic/{id}', 'TrainingController@update');
		Route::get('delete-topic/{id}', 'TrainingController@delete');
		Route::post('delete-topic/{id}', 'TrainingController@destroy');
		Route::get('topic/{id}', 'TrainingController@show');
		Route::post('topic-order', 'TrainingController@topic_order');
		Route::get('add-page/{topic_id}', 'TrainingPagesController@create');
		Route::post('add-page', 'TrainingPagesController@store');
		Route::post('page-position', 'TopicPagesController@position');
		Route::get('page-builder/{id}', 'TopicPagesController@page_build');
		Route::post('save-build/{id}', 'TopicPagesController@save_build');
		Route::get('delete-page/{id}', 'TopicPagesController@delete');
		Route::post('delete-page/{id}', 'TopicPagesController@destroy');

		Route::get('load-images', 'MediaController@load_images');
	    Route::post('upload-media', 'MediaController@upload_media');
    	// End trainings routes
	 });

	// Payrol routes
	Route::get('payroll-settings', 'PayrollController@getSettings');
	Route::post('payroll-settings', 'PayrollController@postSettings');


		Route::get('new-payroll-addition',  'PayrollController@newPayrollAddition');
		Route::post('new-payroll-addition',  'PayrollController@savePayrollAddition');
		Route::get('update-payroll-addition/{id}',  'PayrollController@editPayrollAddition');
		Route::post('update-payroll-addition/{id}',  'PayrollController@updatePayrollAddition');

		Route::get('new-payroll-deduction',  'PayrollController@newPayrollDeduction');
		Route::post('new-payroll-deduction',  'PayrollController@savePayrollDeduction');
		Route::get('update-payroll-deduction/{id}',  'PayrollController@editPayrollDeduction');
		Route::post('update-payroll-deduction/{id}',  'PayrollController@updatePayrollDeduction');

//        Route::group(['middleware' => 'payroll_roles'], function() {
            Route::get('payroll-company', 'SGCompanyDetailController@editCompanyDetails');
            Route::post('payroll-company', 'SGCompanyDetailController@updateCompanyDetails');

            Route::get('payroll-manage-employees', 'SGEmployeeDataController@index');
            Route::get('payroll-new-employee/{id}', 'SGEmployeeDataController@create');
            Route::get('payroll-new-employee', 'SGEmployeeDataController@create');
            Route::post('payroll-new-employee', 'SGEmployeeDataController@store');

//        });

		Route::get('payroll-update-employee/{id}', 'SGEmployeeDataController@edit');
		Route::post('payroll-update-employee/{id}', 'SGEmployeeDataController@update');

		Route::get('payroll-delete-employee/{id}', 'SGEmployeeDataController@destroy');

		Route::get('payslips', 'SGPayrollController@payslips');
		Route::get('my-payroll', 'SGPayrollController@my_payroll');
		Route::get('my-payslips', 'SGPayrollController@my_payslips');
		Route::get('my-iras-forms', 'SGPayrollController@my_irasForms');
		Route::get('cpf-computation', 'SGPayrollController@cpf_computation');

        Route::get('process_payroll',  'PayrollController@process_payroll');

        Route::post('process_payroll_save', 'PayrollController@process_payroll_save');

        Route::get('review_process_payroll/{id}',  'PayrollController@review_process_payroll');
        Route::post('process_payroll_save_finalize', 'PayrollController@process_payroll_save_finalize');

        Route::get('load_payroll/{id}',  'PayrollController@load_payroll');

        Route::post('payroll-roles', 'PayrollController@payroll_roles');

        Route::get('payroll-history', 'PayrollController@history');
        Route::get('payroll-item/{id}', 'PayrollController@show');
        Route::get('payroll-item-employee/{id}', 'PayrollController@showEmployeePayroll');
        Route::get('my-payslips', 'PayrollController@myPayslips');
        Route::get('show-payslip-item/{id}', 'PayrollController@showPayslipItem');

        Route::group( ['middleware' => 'hasPermissions:manage_payroll'], function(){
	        Route::get('generate-cpf', 'PayrollController@generate_cpf');
	        Route::get('cpf_reports', 'PayrollController@cpf_reports');
	        Route::post('get-cpf-employees', 'PayrollController@get_cpf_employees');
	        Route::post('generate-cpf', 'PayrollController@generate_cpf_save');
	        Route::get('cpf-header-record', 'PayrollController@cpf_employee_contribution');
	        Route::get('download-report/{id}', 'PayrollController@cpf_download_report');
	    });

		Route::get('download-payroll-item/{id}', 'PayrollController@download');
		Route::get('payroll-bulk-download/{id}', 'PayrollController@bulk_download');


		Route::get('iras-forms', 'PayrollController@getCompanyIras');
		Route::get('generate-iras', 'PayrollController@getGenerateIras');
		Route::post('generate-iras', 'PayrollController@postGenerateIras');
		Route::get('get-employees-from-year', 'PayrollController@getEmployeesFromYear');
		Route::get('get-iras-per-employee', 'PayrollController@getIrasPerEmployee');
	});
	// End Payroll routes

	/*************************************************************************
	// CRM pages
	 *************************************************************************/
	Route::get('deals', 'CrmController@deals');
	Route::post('stage_update', 'CrmController@stage_update');
	Route::post('create_deal', 'CrmController@create_deal');
	Route::any('deal_edit_form/{id}', 'CrmController@deal_edit_form');
	Route::any('activity_edit_form/{id}', 'CrmController@activity_edit_form');
	Route::post('delete_sub_info', 'CrmController@delete_sub_info');

	Route::post('edit_activity', 'CrmController@edit_activity');
	Route::any('activity_details/{id}', 'CrmController@activity_details');

	Route::post('company_user_list', 'CrmController@company_user_list');
	Route::any('organization_deals', 'CrmController@organization_deals');
	Route::any('contacts_deals', 'CrmController@contacts_deals');

	Route::post('create_activity', 'CrmController@create_activity');
	Route::post('win_lose_update', 'CrmController@win_lose_update');
//	Route::any('deal_view/{id}', 'CrmController@deal_view');
	Route::any('deal_view/{id}/{tab?}', 'CrmController@deal_view');

	Route::any('crm/{id}', 'CrmController@crm');

	Route::any('notes/{id}', 'CrmController@notes');
	Route::post('add_note', 'CrmController@add_note');
	Route::any('files/{id}', 'CrmController@files');
	Route::post('add_file', 'CrmController@add_file');
	Route::post('create_stages', 'AdminController@create_stages');
	Route::any('stages_field_edit/{id}', 'AdminController@stages_field_edit');
	Route::any('stage_field_delete/{id}', 'AdminController@stage_field_delete');
	Route::post('update_stage_rank', 'AdminController@update_stage_rank');

    Route::post('archived_deal', 'CrmController@archived_deal');
    Route::post('delete_deal', 'CrmController@delete_deal');

	Route::any('crm-board', 'CrmController@index');
	Route::post('create_crm_board', 'CrmController@create_crm_board');
	Route::post('open_crm_board', 'CrmController@open_crm_board');
	Route::post('update_crm_board', 'CrmController@update_crm_board');


	/*************************************************************************
	// Employee Recognition Page
	 *************************************************************************/
	Route::group( ['middleware' => 'hasPermissions:view_recognition_board'], function(){
		Route::get('employee_recognition_board', 'EmployeeRecognitionBoardController@index');
		Route::any('add_new_recognition', 'EmployeeRecognitionBoardController@add_new_recognition');
		Route::any('edit_recognition/{id}', 'EmployeeRecognitionBoardController@edit_recognition');
		Route::any('delete_recognition/{id}', 'EmployeeRecognitionBoardController@delete_recognition');
		Route::any('multiple_delete_recognition', 'EmployeeRecognitionBoardController@multiple_delete_recognition');
	});

	Route::group( ['middleware' => 'hasPermissions:view_asset_management,edit_asset_management,view_recognition_board'], function(){
		Route::get('get_employee_name_list', 'EmployeeRecognitionBoardController@get_employee_name_list');
		Route::any('get_edited_employee_information/{id}', 'EmployeeRecognitionBoardController@get_edited_employee_information');
	});
	/*************************************************************************
	// Asset Management Page
	 *************************************************************************/
	Route::group( ['middleware' => 'hasPermissions:view_asset_management,edit_asset_management'], function(){
		Route::get('asset_management', 'AssetManagementController@index');
		Route::get('get_edited_asset/{id}', 'AssetManagementController@get_edited_asset');
		Route::any('add_new_asset', 'AssetManagementController@create');
		Route::any('edit_asset/{id}', 'AssetManagementController@edit_asset');
		Route::any('delete_asset/{id}', 'AssetManagementController@delete_asset');
		Route::get('multiple_delete_assets', 'AssetManagementController@multiple_delete_assets');
	});
	/*************************************************************************
	// Emergency pages
	 *************************************************************************/
	Route::any('emergency_contacts', 'EmergencyContactsController@index');
    Route::post('emergency_contacts_save', 'EmergencyContactsController@emergency_contacts_save');
	Route::post('save_role_permission_ec', 'EmergencyContactsController@save_role_permission');
	Route::any('manage_emergency_contacts', 'EmergencyContactsController@manage_emergency_contacts');
	Route::any('emegency_contact_view/{id}', ['uses' =>'EmergencyContactsController@view']);
	Route::get('export_emergency_contacts','EmergencyContactsController@export_emergency_contacts');
	Route::any('emergency_contact_edit/{id}', ['uses' =>'EmergencyContactsController@emergency_contact_edit']);

	/*************************************************************************
	// Referrals pages
	 *************************************************************************/
	Route::get('referrals', 'ReferralsController@index');
	Route::get('referrals_add', 'ReferralsController@referrals_add');
	Route::post('referrals_add_save', 'ReferralsController@referrals_add_save');
	Route::get('referrals_import', 'ReferralsController@referrals_import');
	Route::any('referrals_import_process', 'ReferralsController@referrals_import_process');

    /*************************************************************************
    // Leaves pages
     *************************************************************************/
    Route::get('leaves_calendar', 'LeavesController@leaves_calendar');
    Route::get('apply-leave', 'LeavesController@apply_leave');
    Route::get('approve-leaves', 'LeavesController@approve_leaves');

    Route::get('leaves-report', 'LeavesController@leaves_report');
    Route::get('view-leaves', 'LeavesController@view_leaves');
    Route::get('leaves-setting', 'LeavesController@settings');
    Route::post('leaves-setting-save', 'LeavesController@settings_save');
    Route::get('leaves-history', 'LeavesController@leaves_history');
    Route::get('leaves-account', 'LeavesController@leaves_account');
    Route::any('view_leave_record/{id}', ['uses' =>'LeavesController@view_leave_record']);
    Route::any('leave-types-edit/{id}', 'LeavesController@leave_types_edit');
    Route::any('leave-types-add', 'LeavesController@leave_types_add');
    Route::post('update_leave_status', 'LeavesController@update_leave_status');
    Route::any('leaves-summary', 'LeavesController@employees_leaves_summary');
    Route::any('leave_summary_edit/{id}', 'LeavesController@leave_summary_edit');
    Route::post('leave_summary_edit_save', 'LeavesController@leave_summary_edit_save');
    Route::post('leavedays_setting_save', 'LeavesController@leavedays_setting_save');
    Route::any('user_leave_notes/{id}', 'LeavesController@user_leave_notes');
    Route::any('add_leave_note/{id}', 'LeavesController@add_leave_note');
    Route::post('add_leave_note_save', 'LeavesController@add_leave_note_save');

    Route::get('company_work_week_edit', 'LeavesController@company_work_week_edit');
    Route::post('save_edit_company_work_week', 'LeavesController@save_edit_company_work_week');
    Route::post('save_add_company_work_week', 'LeavesController@save_add_company_work_week');
    Route::any('delete_company_work_week/{id}', ['uses' =>'LeavesController@delete_company_work_week']);
    Route::post('update_user_work_week', 'LeavesController@update_user_work_week');


    Route::post('leave_summary_edit_bulk', 'LeavesController@leave_summary_edit_bulk');
    Route::post('leave_summary_edit_bulk_save', 'LeavesController@leave_summary_edit_bulk_save');


    Route::post('cancel_leave_record', 'LeavesController@cancel_leave_record');
    Route::get('export_leaves_data', 'LeavesController@export_leaves_data');



    Route::get('company_switch', 'CompanyController@company_switch');


    Route::get('all_leaves','LeavesController@all_leaves');


    Route::get('leaves-my-account', 'LeavesController@leaves_my_account');
    Route::get('manage-leaves', 'LeavesController@manage_leaves');

    Route::group(['middleware' => 'company_admin'], function() {

        /*************************************************************************
        // Company Private pages
         *************************************************************************/
        Route::get('admin_settings','CompanyController@admin_settings');
        Route::get('invite_user', 'CompanyController@invite_user');
        Route::get('invite_guest', 'CompanyController@invite_guest');
        Route::get('process_invite_user', 'CompanyController@process_invite_user');
        Route::post('send_invitation', 'CompanyController@send_invitation');
        Route::get('personalized_fields', 'CompanyController@personalized_fields');
        Route::get('pfields_add', 'CompanyController@pfields_add');
        Route::post('pfields_add_save', 'CompanyController@pfields_add_save');
        Route::any('pfields_edit/{id}', ['uses' =>'CompanyController@pfields_edit']);
        Route::any('pfields_delete/{id}', ['uses' =>'CompanyController@pfields_delete']);
        Route::get('my_company', 'CompanyController@my_company');



        // Expenses Settings
        Route::get('expenses-setting', 'ExpensesController@settings');
        Route::post('expenses-settings-save', 'ExpensesController@settings_save');

        Route::get('announcement_setting', 'AnnouncementsController@settings');
        Route::post('announcement_settings_save', 'AnnouncementsController@settings_save');

        Route::get('file_settings', 'FilesController@settings');
        Route::post('files_settings_save', 'FilesController@settings_save');

        Route::any('organization_setting', 'OrganizationController@settings');

        // Training Module routes
        Route::get('trainings-setting', 'TrainingController@settings');
        Route::post('trainings-setting', 'TrainingController@settings_save');

        Route::any('emergency_contacts_settings', 'EmergencyContactsController@settings');

        Route::get('crm_setting', 'AdminController@crm_setting');
        Route::post('save_crm_setting', 'AdminController@save_crm_setting');
        Route::any('crm-export/{type}', 'AdminController@crm_export');

        /*************************************************************************
        // Roles pages
         *************************************************************************/
        Route::get('roles', 'RolesController@index');
        Route::get('roles_add', 'RolesController@roles_add');
        Route::post('roles_add_save', 'RolesController@roles_add_save');

        Route::get('roles_create_process','RolesController@roles_create_process');
        Route::post('roles_create_process_save','RolesController@roles_create_process_save');

        Route::any('roles_edit/{id}', ['uses' =>'RolesController@roles_edit']);
        Route::any('roles_delete/{id}', ['uses' =>'RolesController@roles_delete']);

        /*************************************************************************
        // Teams pages
        *************************************************************************/
        Route::get('teams', 'TeamsController@index');
        Route::get('teams_add', 'TeamsController@teams_add');
        Route::post('teams_add_save', 'TeamsController@teams_add_save');

        Route::get('teams_create_process','TeamsController@teams_create_process');
        Route::post('teams_create_process_save','TeamsController@teams_create_process_save');

        Route::any('teams_edit/{id}', ['uses' =>'TeamsController@teams_edit']);
        Route::any('teams_delete/{id}', ['uses' =>'TeamsController@teams_delete']);

        /*************************************************************************
        // Permissions pages
        *************************************************************************/
        Route::get('permissions', 'PermissionsController@index');
        Route::post('save_role_permission', 'PermissionsController@save_role_permission');



        /*************************************************************************
        // Employee Management pages
        *************************************************************************/
        Route::get('employee_recognition_board_settings', 'EmployeeRecognitionBoardController@settings');
        Route::get('asset_management_settings', 'AssetManagementController@settings');
    });

    Route::get('project-calendar/', 'ProjectController@calendar');


Route::post('chat/notif', 'ChatController@get_notif');
Route::group(['middleware' => 'auth.ajax'],function(){
	Route::post('user/endpoint', 'UserController@add_endpoint');
	Route::post('user/delete_endpoint', 'UserController@delete_endpoint');
	Route::post('chat/notif', 'ChatController@get_notif');
	Route::post('chat_messages/{chat_id}/{part}', 'ChatController@get_chat_messages_part');
	Route::post('chat/send_message', 'ChatController@send_message_new');
	Route::post('chat_messages/{chat_id}', 'ChatController@get_chat_messages');
	Route::post('create_chat', 'ChatController@create_chat');
	Route::post('chat/upload_file', 'ChatController@send_message_new')->name("upload_chat_file");
	Route::post('read_chat/{chat_id}', 'ChatController@mark_read_chat');
	Route::post('user/changeLang', 'UserController@changeLang');
	Route::get('user/getUsersInCompanyExceptSelf/{id}', 'UserController@getUsersByCompanyExceptSelf');


});
Route::post('searchelastic','ChatController@searchelastic');
Route::post('searchelasticchat','ChatController@searchelasticChat');
Route::get('searchelastic','ChatController@searchelastic');

		
Route::group(['middleware' => 'auth.token'], function(){
	Route::post('chat/send_message', 'ChatController@send_message_new');
	Route::post('read_chat/{chat_id}', 'ChatController@mark_read_chat');
	Route::post('user/mobile_endpoint', 'UserController@add_endpoint');
	Route::post('chat/chats', 'ChatController@get_user_chats_data');
	Route::post('chat_messages/{chat_id}', 'ChatController@get_chat_messages');
	Route::post('chat/upload_file', 'ChatController@send_message_new')->name("upload_chat_file");
	Route::post('user/mobile_delete_endpoint', 'UserController@delete_endpoint');
	Route::get('user/getUsersInCompany/{id}', 'ChatController@getUsersByCompany');
	Route::get('mobile_login/getAllAnnouncementsAndDirectories', 'MobileLoginController@getAllAnnouncementsAndDirectories');
});
/*************************************************************************
// Admin Private pages
*************************************************************************/
Route::group(['prefix' => 'admin'], function () {
    Route::get('dashboard', 'AdminController@dashboard');
});

Route::any('accept_invite/{id}', ['uses' =>'CompanyController@accept_invite']);
Route::post('accept_invite_process', 'CompanyController@accept_invite_process');

Route::any('company_join_request/{id}/{email}', ['uses' =>'CompanyController@company_join_request']);
Route::any('accept_company_join_request/{id}/{email}', ['uses' =>'CompanyController@accept_company_join_request']);
Route::any('activate_user/{hash}', ['uses' =>'CompanyController@activate_user']);

/*************************************************************************
// Referrals pages
 *************************************************************************/
Route::any('accept_referral_invite/{hash}', ['uses' =>'ReferralsController@accept_referral_invite']);
Route::any('view_referral_details/{id}', ['uses' =>'ReferralsController@view_referral_details']);

Route::get('/download_csv_referral', function(){

    $filename = "ZenIntra Email Referrals.csv";
    $handle = fopen($filename, 'w+');
    fputcsv($handle, array('Email'));

    fclose($handle);

    $headers = array(
        'Content-Type' => 'text/csv',
    );

    return Response::download($filename, 'ZenIntra Email Referrals.csv', $headers);
});


/*************************************************************************
// Upload pages
 *************************************************************************/
Route::any('upload/excel', 'UploadController@excel');
Route::any('upload_contact', 'UploadController@excel');
Route::any('upload_organization', 'UploadController@upload_organization');

/*************************************************************************
// User routes
 *************************************************************************/



/*************************************************************************
// Admin pages
 *************************************************************************/

Route::get('admin_login', 'AdminController@admin_login');
Route::post('admin_login', 'AdminController@process_admin_login');
Route::post('admin_logout', 'AdminController@logout');

Route::group(['middleware' => 'admin_auth'], function() {

	Route::get('admin_dashboard', 'AdminController@admin_dashboard');
	Route::get('admin_logout', 'AdminController@admin_logout');
	Route::get('manage_users', 'AdminUserController@manage_users');
	Route::post('admin_users_add', 'AdminUserController@users_add');
	Route::any('companies_list', 'AdminUserController@companies_list');
	Route::any('users_edit/{id}/{company_id}', ['uses' =>'AdminUserController@users_edit']);
	Route::any('user_delete/{id}', ['uses' =>'AdminUserController@user_delete']);
	Route::get('manage_companies', 'AdminCompanyController@manage_companies');
	Route::get('users-reports', 'AdminUserController@users_reports');
	Route::get('companies-reports', 'AdminCompanyController@reports');
	Route::post('admin_company_add', 'AdminCompanyController@admin_company_add');
	Route::any('company_edit/{id}', ['uses' =>'AdminCompanyController@company_edit']);
	Route::any('company_delete/{id}', ['uses' =>'AdminCompanyController@company_delete']);
    Route::get('export_users', 'AdminUserController@export_users');
    Route::get('export_companies','AdminCompanyController@export_companies');
	Route::any('admin_login_user/{id}', ['uses' =>'AdminUserController@admin_login_user']);

    Route::resource('email_templates', 'EmailTemplatesController');
	Route::get('companies-graphs', 'AdminCompanyController@graph_report');
    Route::get('companies-graphs-cumulative', 'AdminCompanyController@graph_report_cumulative');
	Route::get('users-graphs', 'AdminUserController@graph_report');
    Route::get('users-report-cumulative', 'AdminUserController@graph_report_cumulative');


	Route::get('admin-company-stats','AdminCompanyController@company_stats');
    Route::get('admin_approved_leaves','AdminLeavesController@admin_approved_leaves');
    Route::get('admin_pending_leaves','AdminLeavesController@admin_pending_leaves');
    Route::get('admin_rejected_leaves','AdminLeavesController@admin_rejected_leaves');
	Route::any('admin_add_leave/{id}', ['uses' =>'AdminLeavesController@admin_add_leave']);
    Route::post('apply-leave-process-admin', 'AdminLeavesController@apply_leave_process');


});

/*************************************************************************
// Public pages
 *************************************************************************/
Route::any('contact-us', 'PublicController@contact_us');




/*************************************************************************
// Task pages
 *************************************************************************/
Route::any('tasks/{id}', 'TaskController@index');
Route::any('list-view/{id}', 'TaskController@list_view');
Route::get('task-setting', 'TaskController@setting');
Route::any('user-list', 'TaskController@user_list');
Route::any('category-list', 'TaskController@category_list');
Route::post('create_task/{id}', 'TaskController@create_task');
Route::post('create_task_people', 'TaskController@create_task_people');
Route::post('create_list', 'TaskController@create_list');
Route::post('create_category', 'TaskController@create_category');
Route::post('save_list_position', 'TaskController@save_list_position');
Route::post('change_task_list', 'TaskController@change_task_list');
Route::post('save_pm_module', 'TaskController@save_pm_module');
Route::post('save_task_module', 'TaskController@save_task_module');

Route::post('task-setting-delete', 'TaskController@setting_delete');
Route::post('create_board', 'TaskController@create_board');
Route::post('update_board', 'TaskController@update_board');
Route::post('open_board', 'TaskController@open_board');
Route::post('delete_board', 'TaskController@delete_board');

Route::any('view-task','TaskController@get_task_info');
Route::post('save-view-task','TaskController@save_view_task');
Route::post('delete-task','TaskController@delete_task');
Route::post('update_task','TaskController@update_task');

Route::get('export_task_data/{string}', 'TaskController@export_task_data');

Route::post('set-archived', 'TaskController@set_archived');




/*************************************************************************
// Boards pages
 *************************************************************************/
Route::get('boards', 'BoardsController@index');


/*************************************************************************
// Projects pages
 *************************************************************************/
Route::get('projects/{name?}/{id?}', 'ProjectController@index');
Route::any('get_taskdata', 'ProjectController@get_taskdata');
Route::any('people-view/{name?}/{id?}', 'ProjectController@people_view');
Route::any('save_project', 'ProjectController@save_project');
Route::any('milestone', 'ProjectController@milestone');

/*
Route::get('teams_add', 'TeamsController@teams_add');
Route::post('teams_add_save', 'TeamsController@teams_add_save');

Route::any('teams_edit/{id}', ['uses' =>'TeamsController@teams_edit']);
Route::any('teams_delete/{id}', ['uses' =>'TeamsController@teams_delete']);

*/



Route::get('cron_history_logger', 'CronController@history_logger');
Route::get('carry_over_leave_types', 'CronController@carry_over_leave_types');

Route::post('get_access_now_subscribe', 'UserController@get_access_now_subscribe');

Event::listen('illuminate.query', function($query)
{
    #var_dump($query);
});




/*************************************************************************
// Mobile Application
**************************************************************************/
Route::post('mobile_login', 'MobileLoginController@login');

Route::get('test/{year}', 'PayrollController@irasDetails');
