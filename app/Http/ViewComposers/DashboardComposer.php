<?php

namespace App\Http\ViewComposers;

use DB;
use JavaScript;
use App\Company;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

class DashboardComposer
{

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {

        //Fetch and pass all the data that should be available to EVERY view in views/dashboard/
        $user = Auth::user();
        $company = Company::find(session('current_company')->id);

        $user_roles = $user->user_roles->toArray();
        $role_id = '';

        foreach($user_roles as $k=>$role){

            $role_permissions = DB::table('role_permission')->where('role_id',$role['id'])->get();
            $role_id = $role['id'];

            if(count($role_permissions)>0){

                foreach($role_permissions as $RP){

                    $permission = DB::table('permissions')->where('id',$RP->permission_id)->first();

                    $user_roles[$k]['permissions'][] = $permission->key;
                }
            }

        }

        $expenses_roles = DB::table('expenses_roles')->where('role_id', $role_id)->first();

        $is_admin = $company->isMasterUser($user->id);

        foreach($user_roles as $ur){
            if(array_key_exists('role_name',$ur) AND $role['role_name'] =='Administrator'){
                $is_admin=TRUE;
            }
        }

        #print_r($user_roles);

        /*
        1 Edit Company / Role / Team
        2 View Company / Role / Team
        3 Assign Roles
        4 Assign Teams
        5 Edit Employees
        6 Edit General Permissions*/

        $edit_crt = TRUE;
        $view_crt = TRUE;
        $assign_roles = TRUE;
        $assign_teams = TRUE;
        $edit_employees = TRUE;
        $edit_general_permissions = TRUE;
        $manage_announcement = TRUE;
        $manage_contacts = TRUE;
        $manage_expenses = TRUE;
        $manage_trainings = TRUE;
        $approve_expenses = TRUE;
        $pay_expenses = TRUE;
        $view_emergency_contacts = TRUE;
        $edit_emergency_contacts = TRUE;
        $manage_leaves = TRUE;
        $approve_leaves = TRUE;
        $view_leaves = TRUE;
        $is_guest = FALSE;
        $view_calendar = TRUE;
        $view_recognition_board = TRUE;
        $manage_payroll = TRUE;


        if($is_admin == FALSE){

            $edit_crt = FALSE;
            $view_crt = FALSE;
            $assign_roles = FALSE;
            $assign_teams = FALSE;
            $edit_employees = FALSE;
            $edit_general_permissions = FALSE;
            $manage_announcement = FALSE;
            $manage_contacts = FALSE;
            $manage_expenses = FALSE;
            $manage_trainings = FALSE;
            $approve_expenses = (isset($expenses_roles) AND $expenses_roles->approve == '1' ) ? TRUE : FALSE;
            $pay_expenses = (isset($expenses_roles) AND $expenses_roles->pay == '1' ) ? TRUE : FALSE;
            $view_emergency_contacts = FALSE;
            $edit_emergency_contacts = FALSE;
            $manage_leaves = FALSE;
            $approve_leaves = FALSE;
            $view_calendar = FALSE;
            $view_recognition_board = FALSE;
            $manage_payroll = FALSE;

            foreach($user_roles as $ur){

                if(!empty($ur['permissions'])){

                    if(in_array('edit_company_role_team',$ur['permissions'])){
                        $edit_crt = TRUE;
                    }
                    if(in_array('view_company_role_team',$ur['permissions'])){
                        $view_crt = TRUE;
                    }
                    if(in_array('assign_roles',$ur['permissions'])){
                        $assign_roles = TRUE;
                    }
                    if(in_array('assign_teams',$ur['permissions'])){
                        $assign_teams = TRUE;
                    }
                    if(in_array('edit_employees',$ur['permissions'])){
                        $edit_employees = TRUE;
                    }
                    if(in_array('edit_general_permissions',$ur['permissions'])){
                        $edit_general_permissions = TRUE;
                    }
                    if(in_array('manage_announcement',$ur['permissions'])){
                        $manage_announcement = TRUE;
                    }
                    if(in_array('manage_contacts',$ur['permissions'])){
                        $manage_contacts = TRUE;
                        $manage_expenses = TRUE;
                    }
                    if(in_array('view_emergency_contacts',$ur['permissions'])){
                        $view_emergency_contacts = TRUE;
                    }
                    if(in_array('edit_emergency_contacts',$ur['permissions'])){
                        $edit_emergency_contacts = TRUE;
                    }
                    if(in_array('manage_trainings',$ur['permissions'])){
                        $manage_trainings = TRUE;
                    }

                    if(in_array('manage_leaves',$ur['permissions'])){
                        $manage_leaves = TRUE;
                    }
                    if(in_array('approve_leaves',$ur['permissions'])){
                        $approve_leaves = TRUE;
                    }
                    if(in_array('view_calendar',$ur['permissions'])){
                        $view_calendar = TRUE;
                    }
                    if(in_array('view_recognition_board',$ur['permissions'])){
                        $view_recognition_board = TRUE;
                    }

                     if(in_array('manage_payroll',$ur['permissions'])){
                        $manage_payroll = TRUE;
                    }

                }

            }

        }

        JavaScript::put([
        'base_url' => url('/')
        ]);


        $_SERVER['REQUEST_URI_PATH'] = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $segments = explode('/', $_SERVER['REQUEST_URI_PATH']);
        $action = $segments[1];


        if($user->guest_company()){
            $is_guest = TRUE;
        }

        $guest_companies = $user->guest_companies();

        $months = [
            1 => 'January',
            2 => 'February',
            3 => 'March',
            4 => 'April',
            5 => 'May',
            6 => 'June',
            7 => 'July',
            8 => 'August',
            9 => 'September',
            10 => 'October',
            11 => 'November',
            12 => 'December'
        ];



        if($company->country != 'SG'){
            $manage_payroll = FALSE;
        }

        $view->with(compact('action','is_admin','user','company','user_roles','edit_crt','view_crt','assign_roles','assign_teams',
        'edit_employees','edit_general_permissions','manage_announcement','manage_contacts', 'manage_expenses', 'manage_trainings',
         'approve_expenses', 'pay_expenses', 'view_emergency_contacts','edit_emergency_contacts','manage_leaves',
         'approve_leaves','view_leaves','is_guest','guest_companies','view_calendar','view_recognition_board','months', 'manage_payroll'
         ));
    }
}