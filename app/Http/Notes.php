<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Notes extends Model {
	protected $table = "personal_notes";

	protected $fillable = [
		'owner_id',
		'title',
		'content'
	];
}