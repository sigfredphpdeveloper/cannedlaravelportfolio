<?php

namespace App\Http\Middleware;

use App;
use Auth;
use Closure;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user_language = Auth::user()->preferred_lang;

        $user_language = 'en';

        App::setLocale($user_language);
        return $next($request);
    }
}
