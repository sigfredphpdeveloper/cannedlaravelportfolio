<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Redirect;

class CompanyAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        $is_admin = false;

        foreach ( $user->company_user_roles as $role)
        {  
            if( $role->role_name == 'Administrator' )
            {
                $is_admin = true;
                break;
            }
        }

        if( $is_admin )
        {
            return $next($request);
        }
        else
        {
            $message = 'Access declined.';
            return redirect()->to('admin_login')->withInput()->with('warning_message', $message);
        }
        
    }
}
