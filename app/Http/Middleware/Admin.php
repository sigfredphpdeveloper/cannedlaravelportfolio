<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Redirect;
use Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $session = Auth::getSession();

        if(!$session->has('admin')){

            $message = 'Access declined.';

            return Redirect::to('admin_login')->withInput()->with('warning_message', $message);

        }

        return $next($request);
    }

}
