<?php

namespace App\Http\Middleware;

use Closure;

class HasPermissionMiddleware
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role
     * @return mixed
     */
    public function handle($request, Closure $next, $key)
    {
        if (! isset($key)) {
            return redirect('/directory');
        }

        $args= func_get_args();
        $permissions= array_slice($args,2);
        foreach($permissions as $value) {  
            if ($request->user()->hasPermission($value)) {  
                return $next($request);
            }
        }
        
        return redirect('/directory');
        
    }

}