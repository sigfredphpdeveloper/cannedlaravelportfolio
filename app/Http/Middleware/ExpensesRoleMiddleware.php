<?php

namespace App\Http\Middleware;

use Auth;
use App\Company;
use App\Company_user;
use App\User;
use App\User_roles;
Use App\Roles;
use App\Expenses_roles;
use App;
use Closure;

class ExpensesRoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $action = '' )
    {
        if( $action == '' )
            return;

        $company = session('current_company');
        $role = User_roles::where('user_id', $request->user()->id)->first();

        $expenses_role = Expenses_roles::where('role_id', $role->role_id)
            ->where('company_id', '=', $company->id)
            ->first();

        if( $action == 'approve' )
        {
            if( $expenses_role->approve != 1 )
            {
                App::abort(403, 'Unauthorized action.');
            }
        }
        elseif( $action == 'pay' )
        {
            if( $expenses_role->pay != 1 )
            {
                App::abort(403, 'Unauthorized action.');
            }
        }

        return $next($request);
    }
}
