<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Redirect;

use App\Referrals;
use App\Company;
use App\User;


class Login
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        //Handle Email Checks
        if($request->has('email')){

            $email = $request->input('email');

            $user = DB::table('users')->where('email', $email)->first();

            if(count($user)>0){

                if($user->status == 'unverified' OR $user->status == '') {

                    $message = 'Please activate your account first.';

                    return Redirect::to('login')->withInput()->with('warning_message', $message);
                }
            }
        }

        return $next($request);
    }

}
