<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
    	'contact_us',
    	'chat/*',
        'chat_messages/*',
    	'user/getUsersInCompany/*',
        'user/endpoint',
        'user/mobile_endpoint',
        'user/delete_endpoint',
        'read_chat/*',
        'user/mobile_delete_endpoint',
    	'mobile_login'
    ];
}