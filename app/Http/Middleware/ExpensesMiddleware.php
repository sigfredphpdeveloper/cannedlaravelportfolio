<?php

namespace App\Http\Middleware;

use App\Company;
use App\Company_user;
use App\User;
use App;
use Closure;

class ExpensesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $company = session('current_company');
        $company = Company::find( $company->id );

        if( $company->expenses == 0 ){
            App::abort(404, 'Not Found');
        }
        return $next($request);
    }
}
