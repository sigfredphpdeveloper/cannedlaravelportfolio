<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use JavaScript;
use DB;
class Permissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$action = '')
    {

        if( $action == '' )
            return;

        $user = Auth::user();
        $company = $user->company_user->first()->toArray();
        $user_roles = $user->user_roles->toArray();
        $role_id = '';

        foreach($user_roles as $k=>$role){

            $role_permissions = DB::table('role_permission')->where('role_id',$role['id'])->get();
            $role_id = $role['id'];

            if(count($role_permissions)>0){

                foreach($role_permissions as $RP){
                    $user_roles[$k]['permissions'][] = $RP->permission_id;
                }
            }

        }

        $is_admin = FALSE;

        if(isset($company) AND !empty($company['pivot']['is_master']) AND $company['pivot']['is_master'] ==1){
            $is_admin = TRUE;
        }

        foreach($user_roles as $ur){
            if(array_key_exists('role_name',$ur) AND $role['role_name'] =='Administrator'){
                $is_admin=TRUE;
            }
        }

        $edit_crt = TRUE;
        $view_crt = TRUE;
        $assign_roles = TRUE;
        $assign_teams = TRUE;
        $edit_employees = TRUE;
        $edit_general_permissions = TRUE;
        $manage_announcement = TRUE;
        $manage_contacts = TRUE;
        $manage_expenses = TRUE;
        $approve_expenses = TRUE;
        $pay_expenses = TRUE;
        $view_emergency_contacts = TRUE;
        $edit_emergency_contacts = TRUE;

        if($is_admin == FALSE){

            $edit_crt = FALSE;
            $view_crt = FALSE;
            $assign_roles = FALSE;
            $assign_teams = FALSE;
            $edit_employees = FALSE;
            $edit_general_permissions = FALSE;
            $manage_announcement = FALSE;
            $manage_contacts = FALSE;
            $manage_expenses = FALSE;
            $approve_expenses = (isset($expenses_roles) AND $expenses_roles->approve == '1' ) ? TRUE : FALSE;
            $pay_expenses = (isset($expenses_roles) AND $expenses_roles->pay == '1' ) ? TRUE : FALSE;
            $view_emergency_contacts = FALSE;
            $edit_emergency_contacts = FALSE;

            foreach($user_roles as $ur){

                if(array_key_exists('permissions',$ur)){

                    if(in_array(1,$ur['permissions'])){
                        $edit_crt = TRUE;
                    }
                    if(in_array(2,$ur['permissions'])){
                        $view_crt = TRUE;
                    }
                    if(in_array(3,$ur['permissions'])){
                        $assign_roles = TRUE;
                    }
                    if(in_array(4,$ur['permissions'])){
                        $assign_teams = TRUE;
                    }
                    if(in_array(5,$ur['permissions'])){
                        $edit_employees = TRUE;
                    }
                    if(in_array(6,$ur['permissions'])){
                        $edit_general_permissions = TRUE;
                    }
                    if(in_array(7,$ur['permissions'])){
                        $manage_announcement = TRUE;
                    }
                    if(in_array(8,$ur['permissions'])){
                        $manage_contacts = TRUE;
                        $manage_expenses = TRUE;
                    }
                    if(in_array(9,$ur['permissions'])){
                        $view_emergency_contacts = TRUE;
                    }
                    if(in_array(10,$ur['permissions'])){
                        $edit_emergency_contacts = TRUE;
                    }
                }

            }

        }


        if( $action == 'directory' )
        {
            if(!$assign_roles or !$assign_teams) return response('You are not allowed to access this page',400);
        }


        return $next($request);
    }
}
