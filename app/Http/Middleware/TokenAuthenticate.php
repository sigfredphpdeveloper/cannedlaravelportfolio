<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
Use Illuminate\Http\Response;

use App\AuthToken;

use Carbon\Carbon;

class TokenAuthenticate
{
    /**
     * Handle an incoming Request.
     *
     * @param  \Illuminate\Http\Request  $req
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($req, Closure $next)
    {

        $token = $req->input('jwt');

        if(empty($token)){
            //return bad response, missing token
            return response('Missing Token', Response::HTTP_BAD_REQUEST);
        }

        try{
            $with_user = true;
            $auth_token = AuthToken::retrieve($token, $with_user);
        }
        catch(\Exception $e){
            return response('Invalid Token', Response::HTTP_BAD_REQUEST);
        }

        if(empty($auth_token)){
            //return bad response, bad token
            return response('Invalid Token', Response::HTTP_BAD_REQUEST);
        }

        //if auth token is expired
        if(!empty($auth_token->expires_at) && $auth_token->expires_at->lte(Carbon::now())){
            return response('Expired Token', Response::HTTP_BAD_REQUEST);
        }

        $req->merge(['auth_token' => $auth_token, 'user' => $auth_token->user]);

        // Also making the user accessible via to $req->user() function, else it is confusing
        // Because if you go by mobile client, it is $req->user and if you go by web client,
        // it is $req->user()
        // Trying to reuse code both in web client and also mobile client
        Auth::onceUsingId($auth_token->user->id);

        return $next($req);
    }
}
