<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Redirect;

use App\Referrals;
use App\Company;
use App\User;


class Registration
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        //Handle Email Checks
        if($request->has('email')){

            $email = $request->input('email');

            $extracted_domain = substr(strrchr($email, "@"), 1);

            $company = Company::where('url', 'LIKE', "%$extracted_domain")->first();

            if(!empty($company)){

                $link = url('/company_join_request/'.$company->id.'/'.urlencode($email));
                $here = '<a href="'.$link.'" style="color:blue;text-decoration:underline;">here</a>';

                $message = 'It seems an account has already been created for a company using the same domain
  name as your email, please click '.$here.' to ask for access to the software.';

                return Redirect::to('register')->withInput()->with('warning_message', $message);
            }
        }




        return $next($request);
    }

}
