<?php

namespace App\Http\Controllers;

use App\Teams;
use Illuminate\Http\Request;

//use App\Activity;
//use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Organization;
//use Request;
use Auth;
use Illuminate\Support\Facades\File;
use Redirect;
use Input;
use DB;
use Storage;
use Config;
use Response;

use Carbon\Carbon;
use App\Deal;
use App\Deal_notes;
use App\Activity;
use App\Contact;
use App\Notes;
use App\Files;
use App\Company_user;
use App\Stages;
use App\Deal_teams;
use App\Crm_board;
use App\Crm_board_team;

class CrmController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $is_admin = $user->isAdmin();

        $company = session('current_company');

        $boards = Crm_board::where('company_id', $company->id)->get()->toArray();

        if (!$is_admin) {
            $boards = $this->board_visibility($boards);
        }

        $teams = Teams::where('company_id', session('current_company')->id)->get()->toArray();

        return view('dashboard.crm.board', compact('boards', 'teams'));

    }

    private function default_crm_board()
    {
        $user = Auth::user();
        $company = session('current_company');

        // get default table
        $default = Crm_board::where('board_title','Sales Funnel')->where('company_id', $company->id)->first();

        if(empty($default)){
            $crm_board['board_title'] = 'Sales Funnel';
            $crm_board['company_id'] = $company->id;
            $crm_board['owner_id'] = $user->id;
            $crm_board['visibility'] = 'everyone';
            $crm_board['created_date'] = date('Y-m-d H:i:s');
            $crm_board['last_update_date'] = '0000-00-00 00:00:00';
            $board = Crm_board::create($crm_board);

            $stage = Stages::insertGetId(['company_id'=>$company->id, 'crm_board_id'=>$board->id, 'stage_label'=>'To Do', 'stage_position'=>1]);
            Stages::insertGetId(['company_id'=>$company->id, 'crm_board_id'=>$board->id, 'stage_label'=>'Urgent', 'stage_position'=>2]);

            $deals = Deal::select('*','deals.id AS deal_id','deals.visibility AS visibility', 'deals.owner_id as owner_id')
                ->join('contacts','contacts.id','=','deals.contact_id')
                ->join('organization', 'contacts.organization_id','=','organization.id')
                ->where('organization.company_id',$company->id)
                ->whereNull('deals.crm_board_id')
                ->update(['deals.crm_board_id'=>$board->id, 'deals.stage'=>$stage]);
        }
    }

    private function board_visibility($boards)
    {
        $user_id = Auth::user()->id;
        $teams = Auth::user()->team($user_id);
        $team_member = array();
        foreach ($teams as $team) {
            $team_member[] = $team['team_id'];
        }

        foreach ($boards as $key => $board) {
            if ($board['visibility'] == 'team') {
                $board_team = Board_teams::where('board_id', $board['id'])->get()->toArray();

                if (count($board_team) > 1) {
                    $btm = array();
                    foreach ($board_team as $bt) {
                        $btm[] = $bt['team_id'];
                    }
                    if (!array_intersect($team_member, $btm)) {
                        unset($boards[$key]);
                    }

                } elseif (empty($board_team['team_id']) || !in_array($board_team['team_id'], $team_member)) {
                    unset($boards[$key]);
                }
            } elseif ($board['visibility'] == 'me') {
                if ($board['owner_id'] != $user_id) {
                    unset($boards[$key]);
                }
            }
        }

        return $boards;
    }

    public function create_crm_board()
    {
        $post = Input::all();

        if (empty($post['columns']) || count($post['columns']) <= 0) {
            return Redirect::to('crm-board')->withInput()->with('error', 'Column is required');
        }

        $user = Auth::user()->toArray();
        $company = session('current_company');

        $board['company_id'] = $company->id;
        $board['owner_id'] = $user['id'];
        $board['board_title'] = $post['name'];
        $board['visibility'] = $post['visibility'];
        $board['created_date'] = date('Y-m-d H:i:s');
        $board['last_update_date'] = '0000-00-00 00:00:00';
        $board = Crm_board::create($board);
        $board_id = $board->id;

        if ($board['visibility'] == 'team') {
            foreach ($post['teams'] as $v) {
                $team[] = new Crm_board_team(['board_id' => $board->id, 'team_id' => $v]);
            }
            $board->teams()->saveMany($team);
        }

        foreach ($post['columns'] as $k => $column) {
            $array = array('crm_board_id' => $board_id, 'stage_label' => $column, 'company_id' => $company->id, 'position' => ($k + 1));
            Stages::create($array);
        }

        return Redirect::to('crm/' . $board->id)->withInput()->with('success', 'Successfully Created Board');
    }

    public function open_crm_board()
    {
        $post = Input::all();

        $board = Crm_board::where('id', $post['id'])->first()->toArray();

        $board_teams = Crm_board_team::join('teams', 'teams.id', '=', 'crm_board_team.team_id')
            ->where('crm_board_id', $post['id'])->get()->toArray();

        foreach ($board_teams as $k => $v) {
            $board_teams[$k] = $v['team_id'];
        }

        $teams = Teams::where('company_id', session('current_company')->id)->get()->toArray();

        $stages = Stages::where('company_id', session('current_company')->id)->where('crm_board_id', $post['id'])->orderBy('stage_position', 'ASC')->get()->toArray();

        echo view('dashboard.crm.update_form', compact('board', 'board_teams', 'teams', 'stages'));
    }

    public function update_crm_board()
    {
        $post = Input::all();

        $company = session('current_company');

        if(empty($post['columns']) || count($post['columns'])<= 0){
            return Redirect::to('crm-board')->withInput()->with('error', 'Column is required');
        }

        $board = Crm_board::where('id', $post['id'])->where('company_id',$company->id)->first()->toArray();

        if(empty($board)){
            return Response::json('Undefined board ID');
        }

        $board = Crm_board::find($post['id']);
        $board->board_title = $post['name'];
        $board->visibility = $post['visibility'];
        $board->save();

        Crm_board_team::where('crm_board_id', $post['id'])->delete();
        if($post['visibility'] == 'team'){
            foreach($post['teams'] as $team){
                $board_team['crm_board_id'] = $post['id'];
                $board_team['team_id'] = $team;
                Crm_board_team::create($board_team);
            }
        }

        // get all columns and check if deleted
        $columns = Stages::where('crm_board_id',$post['id']);
        foreach($post['columns'] as $k => $column){
            $columns = $columns->where('id','!=',$k);
        }
        $columns->delete();


        $numbering = 1;
        foreach($post['columns'] as $k => $column){

            $list = Stages::where('crm_board_id',$post['id'])->where('id',$k);
            $check_existing =$list ->count();

            if($check_existing == 0 ){

                $array = array('crm_board_id' => $post['id'], 'stage_label' => $column, 'company_id'=>$company->id, 'stage_position'=>$numbering);
                Stages::create($array);

            }else{

                $list->update(['stage_position' =>$numbering]);

            }

            $numbering++;

        }

        return Redirect::to('crm'.'/'.$post['id'])->withInput()->with('success', 'Successfully updated Board');
    }




    public function deals()
    {
        $post = Input::all();
        $company = session('current_company');
        $post['search'] = !empty($post['search'])?$post['search']:'';

        $deals = Deal::select('*','deals.id AS deal_id')
            ->join('contacts','contacts.id','=','deals.contact_id')
            ->join('organization', 'contacts.organization_id','=','organization.id')
            ->where('organization.company_id',$company->id)
            ->get()->toArray();
//            ->paginate(5);

        $stages = Stages::where('company_id',$company->id)->get()->toArray();

        $array = array();
        foreach($stages as $stage){
            $array[$stage['id']]['header'] = $stage['stage_label'];
            $array[$stage['id']]['html'] = ''; // set key
        }

        return view('dashboard.crm.index', compact('deals', 'stages', 'array'));
    }

    public function create_deal()
    {
        $user = Auth::user()->toArray();

        $request = Input::all();

        $request['crm_board_id']=$request['crm_board_id'];
        $request['organization_id']=0;
        $request['stage']=$request['stage'];
        $request['owner_id']=$user['id'];
        $request['create_date']=date('Y-m-d H:i:s');
        $request['visibility']=!empty($request['visibility'])?$request['visibility']:'everyone';
        $request['total_activity']=1;
        $deal = Deal::create($request);

        return Response::json(true);
    }

    public function deal_view($deal_id, $tab = null)
    {
        $activity_page = Input::get('activity_page',1);
        $file_page = Input::get('file_page',1);
        $note_page = Input::get('note_page',1);

        $perpage=config('app.page_count');

        $activity_skip = ($activity_page*$perpage)-$perpage;
        $file_skip = ($file_page*$perpage)-$perpage;
        $note_skip = ($note_page*$perpage)-$perpage;

        $deal = Deal::select('*','deals.last_update_date AS last_update_date')
            ->join('contacts', 'deals.contact_id','=','contacts.id')
            ->join('organization', 'contacts.organization_id','=','organization.id')
            ->join('stages', 'stages.id','=','deals.stage')
            ->where('deals.id', $deal_id)
            ->first()->toArray();

        $activities = Activity::select('*','deals2.last_update_date AS last_update_date')
            ->join('deals AS deals2', 'activity.deal_id','=','deals2.id')
            ->join('contacts', 'deals2.contact_id','=','contacts.id')
            ->join('organization', 'contacts.organization_id','=','organization.id')
            ->where('deals2.id', $deal_id)
            ->skip($activity_skip)->take($perpage)->get();
//                ->paginate(5);
        $total_activity = Activity::select('*','deals2.last_update_date AS last_update_date')
            ->join('deals AS deals2', 'activity.deal_id','=','deals2.id')
            ->join('contacts', 'deals2.contact_id','=','contacts.id')
            ->join('organization', 'contacts.organization_id','=','organization.id')
            ->where('deals2.id', $deal_id)->count();

        $files = Files::where('deal_id',$deal_id)->skip($file_skip)->take($perpage)->get();
        $total_files = Files::where('deal_id',$deal_id)->count();


        $notes = Deal_notes::select('*','notes.created_date as note_date')
            ->join('users','notes.owner_id','=','users.id')
            ->where('deal_id',$deal_id)
            ->skip($note_skip)->take($perpage)->get();

        $total_notes = Deal_notes::select('*','notes.created_date as note_date')
            ->join('users','notes.owner_id','=','users.id')
            ->where('deal_id',$deal_id)->count();
//            ->paginate(5);

        return view('dashboard.crm.deal_view', compact('deal','deal_id','perpage','activities','total_activity','activity_page','files','total_files','file_page','notes','total_notes','note_page', 'tab'));
    }

    public function create_activity()
    {
        $request = Input::all();

        $request['date'] = $request['date_submit'];
        $request['time'] = date('H:i:s', strtotime($request['time']));

        $request['assigned_user_id'] = $request['assigned_user_id'];
        $request['owner_id'] = $request['owner_id'];
        $request['created_date'] = date('Y-m-d H:i:s');
        $request['done'] = !empty($request['done'])?$request['done']:0;

        Activity::create($request);

        $update_deal = Deal::find($request['deal_id']);
        $update_deal->total_activity = $update_deal->total_activity+1;
        $update_deal->last_update_date = date('Y-m-d H:i:s');
        $update_deal->save();


        return redirect('deal_view'.'/'.$request['deal_id']);
    }


    public function win_lose_update(Request $request)
    {

        $request = Input::all();
        $status = '';

        $update_deal = Deal::find($request['deal_id']);

        if(!empty($request['won_date'])){
            $status = 'WON';
            $update_deal->status = 'WON';
            $update_deal->won_date = $request['won_date_submit']; // this should be save because this handled 2000-12-12
        }

        if(!empty($request['lost_date'])){
            $status = 'LOST';
            $update_deal->status = 'LOST';
            $update_deal->lost_date = $request['lost_date_submit']; // this should be save because this handled 2000-12-12
            $update_deal->lost_reason = $request['lost_reason'];
        }

        $update_deal->last_update_date = date('Y-m-d H:i:s');
        $update_deal->save();


        return Redirect::to('deal_view'.'/'.$request['deal_id'])->withInput()->with('success', 'Successfully Set '.$status.' date');

    }


    public function crm($board_id)
    {
        $get = Input::get('display','list_view');

        $post = Input::all();
        $post['search'] = !empty($post['search'])?$post['search']:'';
        $post['column_name'] = !empty($post['column_name'])?$post['column_name']:'';
        $post['column_value'] = !empty($post['column_value'])?$post['column_value']:'';
        $user = Auth::user();
        $company = session('current_company');

        $display = $get;

        $page = Input::get('page',1);
        $perpage = 25;
        $skip = ($page*$perpage)-$perpage;

        $is_admin = $user->isAdmin();
        $user = $user->toArray();


        $teams = Teams::where('company_id', $company->id)->get()->toArray();

        $boards = Crm_board::where('company_id', $company->id)->get()->toArray();
        if (!$is_admin) {
            $boards = $this->board_visibility($boards);
        }

        switch($get){
            case 'deal_per_contacts':

                $crm_contacts = Contact::getCrmContacts()->skip($skip)->take($perpage);
                if(!empty($post['column_value'])){
                    $crm_contacts = $crm_contacts->where($post['column_name'],'like', '%'.$post['column_value'].'%');
                }
                $crm_contacts = $crm_contacts->get();


                $total = Contact::getCrmContacts();
                if(!empty($post['column_value'])){
                    $total = $total->where($post['column_name'],'like', '%'.$post['column_value'].'%');
                }
                $total = $total->get()->count();


                $search_options['contact_name'] = 'Contact name';
                $search_options['organization_name'] = 'Organization name';
                $search_options['organization_name'] = 'Organization name';

                return view('dashboard.crm.deal_per_contact',compact('crm_contacts','deal_teams','perpage', 'display','total','page','search_options','post', 'user', 'is_admin','board_id','boards'));

                break;
            case 'deal_table_view':

                $crm_deals = Deal::getCrmDeals()->where('deals.crm_board_id', $board_id)->where('org.company_id',$company->id)->where('deals.status','!=','DELETE')->skip($skip)->take($perpage);

                if(!empty($post['column_value'])){
                    if($post['column_name']=='status'){
                        $crm_deals = $this->getDealStatus($post,$crm_deals);
                    }else{
                        $total = $crm_deals->where($post['column_name'],'like', '%'.$post['column_value'].'%');
                    }
                }

                $crm_deals = $this->visibility_filter($crm_deals->get());


                if(!$is_admin){
                    foreach($crm_deals as $k => $deal){

                    if($deal['visibility'] == 'team'){
                        $team = array();
                        $team_members = Deal_teams::where('deal_id',$deal['deal_id'])
                            ->join('user_teams','user_teams.team_id','=','deal_teams.team_id')
                            ->get()->toArray();
                        if(!empty($team_members)){
                            foreach($team_members as $team_member){
                                $team[] = $team_member['user_id'];
                            }
                        }
                    }

                    if((($deal['visibility']=='me' && $deal['owner_id']!=$user['id']) && !$is_admin) || $deal['visibility']=='team' && !in_array($user['id'],$team) && !$is_admin){
                        unset($crm_deals[$k]);
                    }else{
                        $crm_deals[$k]=$deal;
                    }
                    }
                }

                $total = Deal::getCrmDeals()->where('org.company_id',$company->id);

                if(!empty($post['column_value'])){
                    if($post['column_name']=='status'){
                        $total = $this->getDealStatus($post,$total);
                    }else{
                        $total = $total->where($post['column_name'],'like', '%'.$post['column_value'].'%');
                    }
                }
                $total = $total->get()->count();

                $search_options['status'] = 'Status';
                $search_options['stages.stage_label'] = 'Stage';
                $search_options['contacts.contact_name'] = 'Owner';
                return view('dashboard.crm.deal_table_view',compact('crm_deals','deal_teams','perpage', 'display','total','page','search_options','post', 'user', 'is_admin','board_id','boards'));

                break;
            case 'deal_per_organization':

                $crm_orgs = Organization::getCrmOrganizations($company->id)->skip($skip)->take($perpage);
                if(!empty($post['column_value'])){
                    $crm_orgs = $crm_orgs->where($post['column_name'],'like', '%'.$post['column_value'].'%');
                }
                $crm_orgs = $crm_orgs->get();

                if($crm_orgs->count() > 0){
                    foreach($crm_orgs->toArray() as $k => $v){
                        $crm_orgs[$k]->deals_nb = Deal::join('contacts','contacts.id','=','deals.contact_id')->where('contacts.organization_id',$v['organization_id'])->where('status', '!=', 'DELETE')->where('status', '!=', 'ARCHIVED')->count();
                        $crm_orgs[$k]->nb_contacts = Contact::where('organization_id',$v['organization_id'])->count();
                    }
                }

                $total = Organization::getCrmOrganizations($company->id);
                if(!empty($post['column_value'])){
                    $total = $total->where($post['column_name'],'like', '%'.$post['column_value'].'%');
                }
                $total = $total->get()->count();


                $search_options['organization_name'] = 'Organization name';
                return view('dashboard.crm.deal_per_organization',compact('crm_orgs','deal_teams','perpage', 'display','total','page','search_options','post', 'user', 'is_admin','board_id','boards'));

                break;
            default:

                $post = Input::all();
                $post['search'] = !empty($post['search'])?$post['search']:'';

                $deals = Deal::select('*','deals.id AS deal_id','deals.visibility AS visibility', 'deals.owner_id as owner_id')
                    ->join('contacts','contacts.id','=','deals.contact_id')
                    ->join('organization', 'contacts.organization_id','=','organization.id')
                    ->where('organization.company_id',$company->id)
                    ->where('deals.crm_board_id','=', $board_id)
                    ->where('deals.status','=', 'ON PROGRESS')
                    ->orderBy('deals.id', 'DESC')
                    ->get()->toArray();

                $stages = Stages::where('crm_board_id',$board_id)->where('company_id',$company->id)->orderBy('stage_position', 'ASC')->get()->toArray();

                $array = array();
                $stages_id = array();
                foreach($stages as $stage){
                    $stages_id[]=$stage['id'];
                    $array[$stage['id']]['header'] = $stage['stage_label'];
                    $array[$stage['id']]['html'] = ''; // set key
                }

                if(!$is_admin){
                    foreach($deals as $k => $deal){
                        if($deal['visibility'] == 'team'){
                            $team = array();
                            $team_members = Deal_teams::where('deal_id',$deal['deal_id'])->join('user_teams','user_teams.team_id','=','deal_teams.team_id')->get()->toArray();
                            foreach($team_members as $team_member){
                                $team[] = $team_member['user_id'];
                            }
                        }
                        // visibility filter
                        if(($deal['visibility']=='me' && $deal['owner_id']!=$user['id']) || $deal['visibility']=='team' && !in_array($user['id'],$team)){
                            unset($deals[$k]);
                        }else{
                            $deals[$k]=$deal;
                        }


                    }

                    // visibility filter
                    if((($deal['visibility']=='me' && $deal['owner_id']!=$user['id']) && !$is_admin) || $deal['visibility']=='team' && !in_array($user['id'],$team) && !$is_admin){
                        unset($deals[$k]);
                    }else{

                        $deals[$k]=$deal;

                    }
                }

                return view('dashboard.crm.index', compact('deals','teams', 'stages', 'array','display', 'stages_id', 'user', 'is_admin','board_id','boards'));

                break;
        }

    }


    public function getDealStatus($post,$crm_deals)
    {
        switch($post['column_value']){
            case 'won':
                $crm_deals = $crm_deals->where('deals.status','WON');
//                $crm_deals = $crm_deals->where('won_date','!=', '0000-00-00 00:00:00');
                break;
            case 'lost':
                $crm_deals = $crm_deals->where('deals.status','LOST');
//                $crm_deals = $crm_deals->where('lost_date','!=', '0000-00-00 00:00:00');
                break;
            case 'on progress':
                $crm_deals = $crm_deals->where('deals.status','ON PROGRESS');
//                $crm_deals = $crm_deals->where('won_date','=', '0000-00-00 00:00:00');
//                $crm_deals = $crm_deals->where('lost_date','=', '0000-00-00 00:00:00');
            default:
                break;
        }
        return $crm_deals;
    }

    public function visibility_filter($deals){

        $user = Auth::user();
        $is_admin = $user->isAdmin();
        $user = $user->toArray();


        foreach($deals as $k => $deal){
            // visibility filter
            if(($deal['visibility']=='me' && $deal['owner_id']!=$user['id']) && !$is_admin){
                unset($deals[$k]);
            }else{
                $deals[$k]=$deal;
            }
        }

        return $deals;
    }

    public function notes($deal_id)
    {
        $notes = Deal_notes::select('*','notes.created_date as note_date')
            ->join('users','notes.owner_id','=','users.id')
            ->where('deal_id',$deal_id)->paginate(5);
//dd($notes);
        return view('dashboard.crm.notes',compact('deal_id', 'notes'));
    }

    public function add_note(Request $request)
    {
        $user = Auth::user()->toArray();

        $post = Input::all();

        $insert['created_date'] = date('Y-m-d H:i:s');
        $insert['owner_id'] = $user['id'];
        $insert['note_details'] = $post['note_details'];
        $insert['deal_id'] = $post['deal_id'];
        Deal_notes::create($insert);

        $update_deal = Deal::find($post['deal_id']);
        $update_deal->last_update_date = date('Y-m-d H:i:s');
        $update_deal->save();

        return Redirect::to('deal_view'.'/'.$request['deal_id'].'/notes')->withInput()->with('success', 'Successfully added new note');
//        return redirect('notes'.'/'.$request['deal_id']);

    }

    public function files($deal_id)
    {
        $files = Files::where('deal_id',$deal_id)->paginate(5);

        return view('dashboard.crm.files', compact('deal_id','files'));
    }

    public function add_file(Request $request)
    {
        $post = Input::all();
        $user = Auth::user();
        $company = $user->company_user->first()->toArray();

        if($request->file('upload') && $request->file('upload')->isValid()){
            $file_size = $request->file('upload')->getSize();
            $type = $request->file('upload')->getType();
            $file = $request->file('upload');

            $fileName = $post['file_name'].'-'.$user->id . '-File-' . time() . '.'. $request->file('upload')->getClientOriginalExtension();

            $zen_url = config('app.bucket_base_url');
            $company_special_folder = '/company-'.$company['id'];


            $s3 = Storage::disk('s3');
            $s3->makeDirectory($company_special_folder);

            $company_special_folder = $company_special_folder.'/crmfiles/';
            $s3->makeDirectory($company_special_folder);

            $s3_upload_path = $company_special_folder.$fileName;

            $file_uploaded = $s3->put($s3_upload_path, file_get_contents($file),'public');

            $file_path = $zen_url.$company_special_folder;

            $insert['file_name'] = $fileName;
            $insert['file_path'] = $file_path;
            $insert['owner_id'] = $user->id;
            $insert['deal_id'] = $post['deal_id'];
            Files::create($insert);

            $update_deal = Deal::find($post['deal_id']);
            $update_deal->last_update_date = date('Y-m-d H:i:s');
            $update_deal->save();

        }

        return redirect('deal_view'.'/'.$post['deal_id'].'/files')->withInput()->with('success', 'Successfully Added new file');

    }

    public function stage_update(Request $request){
        $post = Input::all();

        $update_deal = Deal::find($post['id']);
        $update_deal->stage = $post['group'];
        $update_deal->last_stage_change_date = date('Y-m-d H:i:s');
        $update_deal->save();

        echo json_encode(true);die();

    }

    public function deal_edit_form($id)
    {
        if(!$id) return response('No ID Received',404);

        $post = Input::all();

        if (Input::isMethod('post')){

            $deal = Deal::find($id);

            if($deal->stage != $post['stage']){
                $deal->last_stage_change_date = date('Y-m-d H:i:s');
            }

            $deal->deal_title = $post['deal_title'];
            $deal->contact_id = $post['contact_id'];
            $deal->organization_id = $post['organization_id'];
            $deal->stage = $post['stage'];
            $deal->visibility = !empty($post['visibility'])?$post['visibility']:'everyone';
            $deal->last_update_date = date('Y-m-d H:i:s');
            $deal->save();

            return Redirect::to('crm/'.$deal->crm_board_id.'?display=list_view')->withInput()->with('success', 'Successfully Edited deal '.$post['deal_title']);

        }

        $user = Auth::user();
        $company = session('current_company');

        $deal = Deal::select('*','deals.id AS deal_id','deals.visibility AS visibility')
            ->join('contacts','contacts.id','=','deals.contact_id')
            ->join('organization AS organization2', 'contacts.organization_id','=','organization2.id')
            ->where('deals.id',$id)
            ->where('organization2.company_id',$company->id)
            ->first()->toArray();


        $deal_teams = Deal::find($deal['deal_id'])->deal_teams()->Select('team_id')->get()->toArray();

        foreach($deal_teams as $k => $v){
            $deal_teams[$k] = $v['team_id'];
        }

//        $teams = Teams::where('company_id', $company->id)->get()->toArray();
        $teams = $company->teams()->get()->toArray();

        $stages = Stages::where('company_id',$company->id)->where('crm_board_id',$deal['crm_board_id'])->orderBy('stage_position','ASC')->get()->toArray();

        echo view('dashboard.crm.deal_edit_form',compact('deal', 'stages', 'deal_teams', 'teams'));
    }

    public function activity_edit_form($id)
    {
        if(!$id) return response('No ID Received',404);

        $post = Input::all();

        if (Input::isMethod('post')){

            $deal = Deal::find($id);

            if($deal->stage != $post['stage']){
                $deal->last_stage_change_date = date('Y-m-d H:i:s');
            }

            $deal->deal_title = $post['deal_title'];
            $deal->contact_id = $post['contact_id'];
            $deal->organization_id = $post['organization_id'];
            $deal->stage = $post['stage'];
            $deal->visibility = $post['visibility'];
            $deal->last_update_date = date('Y-m-d H:i:s');

            $deal->save();

            return Redirect::to('deals')->withInput()->with('success', 'Successfully Edited deal '.$post['deal_title']);

        }

        $user = Auth::user();
        $company = session('current_company');

        $activity = Activity::select('*',DB::raw('concat(users.title, " ", users.first_name," ", users.last_name) AS owner_name'), DB::raw('concat(users2.title, " ", users2.first_name," ", users2.last_name) AS assigned_name'))
            ->join('deals', 'deals.id','=','activity.deal_id')
            ->leftjoin('users', 'users.id','=','activity.owner_id')
            ->leftjoin('users AS users2', 'users2.id','=','activity.assigned_user_id')
            ->where('activity.activity_id',$id)
            ->get()->toArray();
        $activity = $activity[0];
//dd($activity);
        echo view('dashboard.crm.activity_edit_form',compact('activity', 'id', 'post'));
    }

    public function edit_activity(Request $request)
    {
        $post = Input::all();

        $update['activity_title'] = $post['activity_title'];
        $update['activity_type'] = $post['activity_type'];
//        $update['date'] = $post['date'];
        $update['date'] = $request['date_submit'];
        $update['time'] = $post['time'];
        $update['duration'] = $post['duration'];
        $update['owner_id'] = !empty($post['owner_id'])?$post['owner_id']:0;
        $update['assigned_user_id'] = !empty($post['assigned_user_id'])?$post['assigned_user_id']:0;
        $update['done'] = !empty($post['done'])?$post['done']:0;
        $activity = Activity::where('activity_id',$post['activity_id'])->update($update);

        $update_deal = Deal::find($post['deal_id']);
        $update_deal->last_update_date = date('Y-m-d H:i:s');
        $update_deal->save();


        return Redirect::to('deal_view'.'/'.$post['deal_id'])->withInput()->with('success', 'Successfully Edited Activity');

    }

    public function company_user_list()
    {
        $post = Input::all();

        $compnay_user = Company_user::select('user_id',DB::raw('concat(users.title, " ", users.first_name," ", users.last_name) AS label'),DB::raw('concat(users.title, " ", users.first_name," ", users.last_name) AS value'))
            ->join('users', 'users.id','=','company_user.user_id')
            ->where('company_user.company_id', session('current_company')->id)
            ->where(DB::raw('concat(users.title, " ", users.first_name," ", users.last_name)'), 'like', '%'.$post['q'].'%')
            ->get()->toArray();

        return Response::json($compnay_user);
    }

    public function activity_details($id){

        $activity = Activity::select('*',DB::raw('concat(users.title, " ", users.first_name," ", users.last_name) AS owner_name'), DB::raw('concat(users2.title, " ", users2.first_name," ", users2.last_name) AS assigned_name'))
            ->join('deals', 'deals.id','=','activity.deal_id')
            ->leftjoin('users', 'users.id','=','activity.owner_id')
            ->leftjoin('users AS users2', 'users2.id','=','activity.assigned_user_id')
            ->where('activity.activity_id',$id)
            ->get()->toArray();
        $activity = $activity[0];
//dd($activity);
        echo view('dashboard.crm.activity_info',compact('activity'));
    }

    public function organization_deals()
    {
        $post = Input::all();
        $company = session('current_company');

        $deals = Deal::select('*','deals.id AS deal_id','deals.status as status')
            ->join('contacts','contacts.id','=','deals.contact_id')
            ->join('organization AS org','org.id','=','contacts.organization_id')
            ->join('users','org.owner_id','=','users.id')
            ->where('org.company_id',$company->id)
            ->where('org.id',$post['id'])
            ->get()->toArray();
//dd($deals);

        echo view('dashboard.crm.organization_deals',compact('deals'));

    }
    public function contacts_deals()
    {
        $post = Input::all();
        $company = session('current_company');

        $deals = Deal::select('*','deals.id AS deal_id','deals.status as status')
            ->join('contacts','contacts.id','=','deals.contact_id')
            ->join('organization AS org','org.id','=','contacts.organization_id')
            ->join('users','org.owner_id','=','users.id')
            ->where('org.company_id',$company->id)
            ->where('contacts.id',$post['id'])
            ->get()->toArray();


        echo view('dashboard.crm.contact_deals',compact('deals'));

    }

    public function delete_sub_info()
    {
        $post = Input::all();

        $sub_info = '';
        switch($post['table']){
            case 'notes':
                Deal_notes::where('note_id', $post['id'])->delete();
                $sub_info = 'Note';
                break;
            case 'files':
                Files::where('file_id', $post['id'])->delete();
                $sub_info = 'File';
                break;
            case 'activity':
                Activity::where('activity_id', $post['id'])->delete();
                $sub_info = 'Activity';
                break;
        }

        return Redirect::to('deal_view'.'/'.$post['deal_id'].'/'.$post['table'])->withInput()->with('success', 'Successfully deleted '.$sub_info);
    }

    public function delete_deal()
    {
        $post = Input::all();
        $update_deal = Deal::find($post['deal_id']);
        $update_deal['status'] = 'DELETE';
        $update_deal->save();

        return Redirect::to('deal_view'.'/'.$post['deal_id'])->withInput()->with('success', 'Successfully Deleted ');
    }

    public function archived_deal()
    {
        $post = Input::all();
        $update_deal = Deal::find($post['deal_id']);
        $update_deal['status'] = 'ARCHIVED';
        $update_deal->save();

        return Redirect::to('deal_view'.'/'.$post['deal_id'])->withInput()->with('success', 'Successfully Archived ');
    }


}
