<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use Auth;
use App\Company;
use App\Permissions;
use App\Roles;

use Carbon\Carbon;
use DateTime;
use Redirect;

class AssetManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        $company = Company::find(session('current_company')->id);

        $asset_list = DB::table('asset_management as asset')
                        ->select('asset.*', 'employee.first_name as e_first_name', 'employee.last_name as e_last_name')
                        ->join('users as employee', 'employee.id', '=', 'asset.allocated_to')
                        ->where('asset.company_id', '=', $company->id)
                        ->orderBy('asset.id')
                        ->get();

        // User Permission
        $user_roles = DB::table('user_roles')->where('user_id', $user->id)->lists('role_id');

        $view_permission = false; 
        $edit_permission = false;

        foreach($user_roles as $role) {
            $user_permissions = Roles::find($role);

            foreach($user_permissions->role_permissions as $per) {
                if($per->key == "view_asset_management") {
                    $view_permission = true;
                }

                if($per->key == "edit_asset_management") {
                    $edit_permission = true;
                }
            }
        }

        return view('dashboard.employee_management.asset_management', 
            compact('asset_list', 'edit_permission', 'view_permission'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $company = Company::find(session('current_company')->id);

        $this->validate($request, ['title'=>'required', 'employee_id'=>'required']);

        $row = DB::table('asset_management')->insert([
            'company_id' => $company->id,
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'allocated_to' => $request->input('employee_id'),
            'status' => $request->input('status'),
            'value' => $request->input('value'),
            'note' => $request->input('note'),
            'purchase_date' => $this->setNull($request->input('purchase_date')),
            'sale_disposal_date' => $this->setNull($request->input('sale_disposal_date')),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }


    public function edit_asset($id, Request $request)
    {
        $company = Company::find(session('current_company')->id);

        $this->validate($request, ['title'=>'required', 'employee_id'=>'required']);

        $row = DB::table('asset_management')->where('id', $id)->update([
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'allocated_to' => $request->input('employee_id'),
            'status' => $request->input('status'),
            'value' => $request->input('value'),
            'note' => $request->input('note'),
            'purchase_date' => $this->setNull($request->input('purchase_date')),
            'sale_disposal_date' => $this->setNull($request->input('sale_disposal_date')),
            'updated_at' => Carbon::now()
        ]);
    }


    public function multiple_delete_assets(Request $request) 
    {
        $ids = $request->input('ids');

        foreach($ids as $id) 
        {
            $del_record = DB::table('asset_management')->where('id', $id)->delete();
        }

        return Redirect::to('asset_management')->withInput()->with('success', 'Successfully Delete Many Assets');
    }


    public function delete_asset($id) 
    {
        DB::table('asset_management')->where('id', $id)->delete();
    }


    public function settings()
    {
        $user = Auth::user();

        $company = Company::find(session('current_company')->id);

        $roles = $company->roles()->with('role_permissions')->get();

        $permissions = Permissions::whereIn('key',['view_asset_management', 'edit_asset_management'])->get()->toArray();

        return view('dashboard.employee_management.asset_management_settings', 
            compact('user', 'company', 'permissions', 'roles'));
    }

    public function get_edited_asset($id) 
    {   
        $row = DB::table('asset_management')->where('id', $id)->first();

        $employee_row = DB::table('users')->where('id', $row->allocated_to)->first();

        $employee_name = $employee_row->first_name .' '. $employee_row->last_name;

        return json_encode([
            'id' => $row->id,
            'title' => $row->title,
            'description' => $row->description,
            'value' => $row->value,
            'note' => $row->note,
            'employee_name' => $employee_name,
            'employee_id' => $row->allocated_to,
            'purchase_date' => $row->purchase_date,
            'sale_date' => $row->sale_disposal_date,
            'status' => $row->status,
            'sale_disposal_date' => $row->sale_disposal_date
        ]);
    }


    public function setNull($input) 
    {
        $value = $input;

        if(empty($input)) {
            return null;
        }

        $ymd = DateTime::createFromFormat('Y-m-d', $input)->format('Y-m-d');

        return $ymd;
    }
}