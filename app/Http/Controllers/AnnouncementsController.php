<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Listeners\PushNotificationEventListener;

use App\Company;
use App\Company_user;
use App\Roles;
use App\User;
use App\Teams;
use App\User_teams;
use App\Announcements;
use App\Announcement_roles;
use App\Announcement_teams;

use App\Permissions;
use Auth;
use Input;
use Redirect;

use Illuminate\Mail\Mailer;
use Mail;

use Intervention\Image\Facades\Image;

use DB;
use AWS;
use Storage;
use App\EmailTemplates;
use App\Classes\Helper;
use Config;
use Carbon\Carbon;


class AnnouncementsController extends Controller
{

    public function index()
    {
        $user = Auth::user();

        $company = Company::find(session('current_company')->id);

        $announcements = DB::table('announcements')
            ->select('announcements.*','users.first_name','users.last_name')
            ->join('users', 'users.id','=','announcements.user_id')
            ->where('company_id',$company->id)
            ->orderBy('created_at','DESC')
            ->get();

        return view('dashboard.announcements.manage_announcements',compact('announcements'));
    }


    public function add_announcement()
    {
        $user = Auth::user();

        $company = Company::find(session('current_company')->id);

        $roles = $company->roles;

        $teams = $company->teams;

        return view('dashboard.announcements.announcement_add',compact('roles','teams'));
    }

    function announcement_add_save(Request $request){

        $this->validate($request, ['title' => 'required','message' => 'required']);

        $user = Auth::user();

        $company = Company::find(session('current_company')->id);

        $post = Input::all();

        $new_announcement = Announcements::create([
            'user_id' => $post['user_id'],
            'company_id' => $post['company_id'],
            'title' => $post['title'],
            'message' => $post['message'],
            'permission'=>$post['permission']
        ]);

        $new_announcement->save();

        $involved          = []; $roles_room_array  = []; 
        $teams_room_array  = []; $all_room_array    = []; $all_arr = [];

        $add_date          = Carbon::now();
        $push_notification = new PushNotificationEventListener();
        $user_data         = User::find($post['user_id']);

        $user_first_name   = $user_data->first_name;
        $user_last_name    = $user_data->last_name;

        if(isset($post['roles'])){

            foreach($post['roles'] as $role_id){

                array_push($roles_room_array, 'company_'.$post['company_id'].'_role_'.$role_id);

                Announcement_roles::create([
                    'announcement_id'=>$new_announcement->id,
                    'role_id'=>$role_id
                ]);

                //get involved for roles
                $user_roles = DB::table('user_roles')
                    ->where('role_id',$role_id)
                    ->get();

                if($user_roles){

                    foreach($user_roles as $user_role){

                        $user_involved = DB::table('users')
                            ->where('id',$user_role->user_id)
                            ->first();

                        if($user_involved) {
                           $involved[] = ['email'=>$user_involved->email,'name'=>$user_involved->first_name .' '.$user_involved->last_name];

                        }

                    }

                }
            }

            // Push notification to Nodejs server
            $array = array(
                'event'  =>  Config::get('constants.EVENT_ANNOUNCEMENT.ADD_ROLE'),
                'title'  =>  $post['title'],
                'creator'=>  $user_first_name .' '. $user_last_name,
                'date'   =>  $add_date,
                'message'=>  $post['message'],
                'rooms'  =>  $roles_room_array
            );

            array_push($all_arr, $array);

            if($_SERVER['SERVER_NAME'] != 'zenintra.local') {
                $push_notification->pushNotification($all_arr);
            }


        } 

        if(isset($post['teams'])){
            foreach($post['teams'] as $team_id){
                array_push($teams_room_array, 'company_'.$post['company_id'].'_team_'.$team_id);

                Announcement_teams::create([
                    'announcement_id'=>$new_announcement->id,
                    'team_id'=>$team_id
                ]);

                //get involved for Teams
                $user_teams = DB::table('user_teams')
                    ->where('team_id',$team_id)
                    ->get();

                if($user_teams){

                    foreach($user_teams as $user_team){

                        $user_involved = DB::table('users')
                            ->where('id',$user_team->user_id)
                            ->first();

                        if($user_involved)
                            $involved[] = ['email'=>$user_involved->email,'name'=>$user_involved->first_name .' '.$user_involved->last_name];
                    }
                }
            }

            // Push notification to Nodejs server
            $array = array(
                'event'  =>  Config::get('constants.EVENT_ANNOUNCEMENT.ADD_TEAM'),
                'title'  =>  $post['title'],
                'creator'=>  $user_first_name .' '. $user_last_name,
                'date'   =>  $add_date,
                'message'=>  $post['message'],
                'rooms'  =>  $teams_room_array
            );

            array_push($all_arr, $array);

            if($_SERVER['SERVER_NAME'] != 'zenintra.local') {
                $push_notification->pushNotification($all_arr);
            }

        }

        if(isset($post['permission']) AND $post['permission'] == 'all'){

            $employees = User::select('users.*')
                ->join('company_user', 'users.id','=','company_user.user_id')
                ->join('company', 'company_user.company_id','=','company.id')
                ->where('company_user.company_id',$company->id)
                ->get();

            foreach($employees as $employee){
                $involved[] = ['email'=>$employee->email,'name'=>$employee->first_name .' '.$employee->last_name];
            }

            // Push notification to Nodejs server
            $array = array(
                'event' =>  Config::get('constants.EVENT_ANNOUNCEMENT.ADD_ALL'),
                'title'  =>  $post['title'],
                'creator'=>  $user_first_name .' '. $user_last_name,
                'date'   =>  $add_date,
                'message'=>  $post['message']
            );

            array_push($all_arr, $array);
            if($_SERVER['SERVER_NAME'] != 'zenintra.local') {
                $push_notification->pushNotification($all_arr);
            }

        }


        if ($request->file('image') AND $request->file('image')->isValid()) {

            $imageName = 'Announcement-Picture-' . $company->id . '-'. time(). '.' .
                $request->file('image')->getClientOriginalExtension();

            $request->file('image')->move(
                base_path() . '/public/uploads/', $imageName
            );

            $img = Image::make(base_path() . '/public/uploads/' . $imageName);

            // prevent possible upsizing
            $img->resize(null, 200, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $img->save(base_path() . '/public/uploads/' . $imageName);

            //https://zenintra-staging.s3.amazonaws.com/test.jpg
            // $zen_url = 'https://zenintra-staging.s3.amazonaws.com';
            $zen_url = config('app.bucket_base_url');
            $company_special_folder = '/company-'.$company['id'];

            $s3 = Storage::disk('s3');

            $s3->makeDirectory($company_special_folder);

            $filePath =  './uploads/'.$imageName;
            $s3_upload_path = $company_special_folder.'/'.$imageName;

            $image = $s3->put($s3_upload_path, file_get_contents($filePath),'public');

            $s3_file_url = $zen_url.$s3_upload_path;

            $new_announcement->picture = $s3_file_url;

            $new_announcement->save();

            unlink($filePath);

        }

        if(isset($post) AND array_key_exists('notification_email',$post)){

            //save first
            $new_announcement->notification_email = $post['notification_email'];
            $new_announcement->save();


            if($post['notification_email'] == 1){
                $publisher = $user->email;
                $company_name = $company->company_name;
                $announcement_title = $new_announcement->title;
                $announcement_message = $new_announcement->message;

                $email_template = EmailTemplates::where('slug','announcement_notification')->first();

                $for_parsing = [
                    'company_name' => $company->company_name
                ];

                $subject = Helper::parse_email($email_template->subject,$for_parsing);

                foreach($involved as $details){

                    $for_parsing = [
                        'name' => $details['name'],
                        'company_name' => $company->company_name,
                        'announcement_title' => $announcement_title,
                        'announcement_message' => $announcement_message
                    ];

                    $body = Helper::parse_email($email_template->body,$for_parsing);

                    Mail::send('emails.default_email', compact('body','company'), function($message)
                    use($subject,$details,$company_name,$publisher,$user)
                    {
                        $message->subject($subject);
                       # $message->from(config('mail.from.address'), $company_name);
                        $message->from('no-reply@zenintra.net', $company_name);
                        $message->replyTo($publisher, $user->first_name .' '.$user->last_name);
                        $message->to($details['email'], $details['name']);
                    });
                }

            }


        }



        return Redirect::to('dashboard')->withInput()->with('success', 'Successfully Added Announcement!');

    }


    public function announcement_edit($id,Request $request){

        $announcement = Announcements::find($id);

        if(!$announcement) return response('Announcement Not Found',400);

        $post = Input::all();

        if ($post)
        {

            // Get information for pushing notifications
            $roles_room_array = []; $teams_room_array = [];
            $all_arr = [];

            $push_notification = new PushNotificationEventListener();
            $edit_date         = Carbon::now();
            $company_id        = $announcement->company_id;

            // User data
            $user_data       = User::find($post['user_id']);
            $user_first_name = $user_data->first_name;
            $user_last_name  = $user_data->last_name;

            $this->validate($request, ['title' => 'required','message'=>'required']);

            $user = Auth::user();

            $company = session('current_company');

            $post = Input::all();

            if(isset($post['roles'])){

                DB::table('announcement_roles')->where('announcement_id',$id)->delete();

                foreach($post['roles'] as $role_id){
                    array_push($roles_room_array, 'company_'.$company_id.'_role_'.$role_id);

                    Announcement_roles::create([
                        'announcement_id'=>$id,
                        'role_id'=>$role_id
                    ]);
                }

                // Push notification to Nodejs server
                $array = array(
                    'event'  =>  Config::get('constants.EVENT_ANNOUNCEMENT.EDIT_ROLE'),
                    'title'  =>  $post['title'],
                    'creator'=>  $user_first_name .' '. $user_last_name,
                    'date'   =>  $edit_date,
                    'message'=>  $post['message'],
                    'rooms'  =>  $roles_room_array
                );

                array_push($all_arr, $array);
                $push_notification->pushNotification($all_arr);

            } else if(isset($post['teams'])){
                DB::table('announcement_teams')->where('announcement_id',$id)->delete();

                foreach($post['teams'] as $team_id){
                    array_push($teams_room_array, 'company_'.$company_id.'_team_'.$team_id);

                    Announcement_teams::create([
                        'announcement_id'=>$id,
                        'team_id'=>$team_id
                    ]);
                }

                // Push notification to Nodejs server
                $array = array(
                    'event' =>  Config::get('constants.EVENT_ANNOUNCEMENT.EDIT_TEAM'),
                    'title'  =>  $post['title'],
                    'creator'=>  $user_first_name .' '. $user_last_name,
                    'date'   =>  $edit_date,
                    'message'=>  $post['message'],
                    'rooms'  =>  $teams_room_array
                );

                array_push($all_arr, $array);
                $push_notification->pushNotification($all_arr);
            }

            $announcement = Announcements::find($id);

            $announcement->title = $post['title'];
            $announcement->message = $post['message'];
            $announcement->permission = $post['permission'];

            $announcement->save();

            if ($request->file('image') AND $request->file('image')->isValid()) {

                $imageName = 'Announcement-Picture-' . $company->id . '-'. time(). '.' .
                    $request->file('image')->getClientOriginalExtension();

                $request->file('image')->move(
                    base_path() . '/public/uploads/', $imageName
                );

                $img = Image::make(base_path() . '/public/uploads/' . $imageName);

                // prevent possible upsizing
                $img->resize(null, 200, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });

                $img->save(base_path() . '/public/uploads/' . $imageName);

                //https://zenintra-staging.s3.amazonaws.com/test.jpg
                $zen_url = config('app.bucket_base_url');
                $company_special_folder = '/company-'.$company['id'];

                $s3 = Storage::disk('s3');

                $s3->makeDirectory($company_special_folder);

                $filePath =  './uploads/'.$imageName;
                $s3_upload_path = $company_special_folder.'/'.$imageName;

                $image = $s3->put($s3_upload_path, file_get_contents($filePath),'public');

                $s3_file_url = $zen_url.$s3_upload_path;

                $announcement->picture = $s3_file_url;

                $announcement->save();


                unlink($filePath);

            }

            return Redirect::to('announcement_edit/'.$id)->withInput()->with('success', 'Successfully Edited Team!');

        }

        $user = Auth::user();

        $company = $user->company_user->first();

        $roles = $company->roles;

        $teams = $company->teams;

        $announcement_roles = $announcement->roles;
        $announcement_teams = $announcement->teams;

        $roles_array = [];
        foreach($announcement_roles as $role){
            $roles_array[] = $role['role_id'];
        }

        $teams_array = [];
        foreach($announcement_teams as $team){
            $teams_array[] = $team['team_id'];
        }

        $announcement['roles'] = $roles_array;
        $announcement['teams'] = $teams_array;

        return view('dashboard.announcements.announcement_edit',compact('announcement','roles','teams'));

    }


    public function announcement_delete($id){
        if(!$id){
            return Redirect::to('announcements')->withInput()->with('error', 'Error Occured!');
        }

        $announcement = Announcements::find($id);

        // Push Notifications To Nodejs Server
        if($announcement){ 

            $del_room_array  = []; $all_arr = [];

            $company_id      = $announcement->company_id;
            $user_id         = $announcement->user_id;

            $user            = User::find($user_id);
            $user_first_name = $user->first_name;
            $user_last_name  = $user->last_name;
            
            // Team + Role Room
            $user_role_list = DB::table('announcement_roles')
                ->where('announcement_id', $id)
                ->lists('role_id');

            $user_team_list = DB::table('announcement_teams')
                ->where('announcement_id', $id)
                ->lists('team_id');

            foreach($user_role_list as $role) {
                array_push($del_room_array, 'company_'.$company_id.'_role_'.$role);
            }

            foreach($user_team_list as $team) {
                array_push($del_room_array, 'company_'.$company_id.'_team_'.$team);
            }

            $array = array(
                'event'  =>  Config::get('constants.EVENT_ANNOUNCEMENT.DELETE_ANNOUNCEMENT'),
                'title'  =>  $announcement->title,
                'creator'=>  $user_first_name .' '. $user_last_name,
                'date'   =>  Carbon::now(),
                'message'=>  $announcement->message,
                'rooms'  =>  $del_room_array
            );

            array_push($all_arr, $array);

            $push_notification = new PushNotificationEventListener();
            $push_notification->pushNotification($all_arr);

            //-------------------------------------------------------//

            $announcement->delete();

            DB::table('announcement_roles')->where('announcement_id',$id)->delete();
            DB::table('announcement_teams')->where('announcement_id',$id)->delete();

            return Redirect::to('announcements')->withInput()->with('success', 'Successfully Deleted Announcement!');
        }else{
            return Redirect::to('announcements')->withInput()->with('error', 'Error Occured!');
        }

    }

    function settings(){

        $user = Auth::user();

        $company = Company::find(session('current_company')->id);

        $roles = $company->roles()->with('role_permissions')->get();

        $permissions = Permissions::whereIn('key',['manage_announcement'])->get()->toArray();

        return view('dashboard.announcements.settings',compact('user','company','permissions','roles'));


    }

    function settings_save(){

        $user = Auth::user();

        $company = $user->company_user->first()->toArray();

        $post = Input::all();

        if ($post) {

            $save_company = Company::find($company['id']);

            $save_company->announcements = (array_key_exists('announcements',$post) AND $post['announcements'] == '1')?1:0;

            $save_company->save();

            return Redirect::to('announcement_setting')->withInput()->with('success', 'Successfully Updated!');

        }

    }

    function view($announcement_id=NULL){

        if(!$announcement_id) return response('Announcement ID Missing');

        $announcement = Announcements::find($announcement_id);

        if(!$announcement) return response('Announcement Record Not Found');

        return view('dashboard.announcements.announcement_view',compact('announcement'));

    }

    function all_announcements(){

        $data = session()->all();

        $request = Auth::getSession();

        $user = Auth::user();

        $roles = $user->user_roles->toArray();

        $teams = $user->user_teams->toArray();

        $user_roles= [];

        foreach($roles as $role){
            array_push($user_roles,$role['id']);
        }

        $user_teams = [];

        foreach($teams as $team){
            array_push($user_teams,$team['id']);
        }


        $company = $user->company_user->first();

        $announcement_array = array();

        $announcements = DB::table('announcements')
            ->select('announcements.*','users.photo','users.first_name','users.last_name')
            ->join('users', 'users.id','=','announcements.user_id')
            ->where('company_id',$company->id)
            ->orderBy('created_at','DESC')
            ->get();

        foreach($announcements as $announcement){

            if($announcement->permission=='all'){

                $announcement_array[] = (array)$announcement;

            }elseif($announcement->permission == 'roles'){ //roles

                $announcement_roles = DB::table('announcement_roles')
                    ->where('announcement_id',$announcement->id)
                    ->get();
                $all_roles_allowed=[];

                foreach($announcement_roles as $role_record){
                    $all_roles_allowed[] = $role_record->role_id;
                }

                foreach($all_roles_allowed as $allowed_role){

                    if(in_array($allowed_role,$user_roles)){
                        $announcement_array[] =  (array)$announcement;
                    }

                }

            }elseif($announcement->permission == 'teams'){ //teams

                $announcement_teams = DB::table('announcement_teams')
                    ->where('announcement_id',$announcement->id)
                    ->get();

                $all_teams_allowed=[];

                foreach($announcement_teams as $team_record){
                    $all_teams_allowed[] = $team_record->team_id;
                }

                foreach($all_teams_allowed as $allowed_team){

                    if(in_array($allowed_team,$user_teams)){
                        $announcement_array[] =  (array)$announcement;
                    }

                }

            }

        }

        return view('dashboard.announcements.all_announcements',compact('announcement_array'));


    }
}
