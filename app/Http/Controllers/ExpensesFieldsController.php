<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Expenses_fields;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use Input;
use Redirect;
use DB;

class ExpensesFieldsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $inputs = $request->all();
        $deletedRows = Expenses_fields::where('expense_type_id', $inputs['fields'][0]['expense_type_id'])->delete();

        $fields = $inputs['fields'];
        $expenses_fields = array();
        foreach ($fields as $key => $field) {
            $field['created_at'] = \Carbon\Carbon::now()->toDateTimeString();
            $field['updated_at'] = \Carbon\Carbon::now()->toDateTimeString();
            array_push($expenses_fields, $field);
        }

        $query = Expenses_fields::insert($expenses_fields);
        if( $query )
        {
            return Redirect::back()->with([ 'success' => 'Expenses Type Fields Updated !' ]);
        }
        else
        {
            return Redirect::back()->with([ 'error' => 'Something went wrong !' ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
