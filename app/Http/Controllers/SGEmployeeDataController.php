<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Company;
use App\SGEmployeeData;
use DB;
use Auth;
use App\Http\Utilities\Country;

use Redirect;

class SGEmployeeDataController extends Controller
{

    public function __construct()
    {
        $company = Company::find(session('current_company')->id);

        if($company->country != 'SG'){
            return Redirect::to('dashboard')->send();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $is_admin = $user->isAdmin();


        $company = session('current_company');
        $countries = Country::countries();
        $nationalities = Country::nationalities();
        return view('dashboard.payroll.employees.index', compact('company','countries','nationalities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($employee_id = NULL)
    {
        $company = session('current_company');
        $users = User::join('company_user', 'users.id','=','company_user.user_id')
                    ->where('company_user.company_id',$company->id)
                    ->whereNotIn('user_id', function($query) use ($company){
                        $query->select('user_id')
                            ->from('s_g_employee_datas')
                            ->where('s_g_employee_datas.company_id', $company->id)->lists('user_id');
                    })
                    ->select('users.*')
                    ->get();

        return view('dashboard.payroll.employees.create', compact('users','employee_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      #  dd($request->input());
        $company = session('current_company');







        $employee = new SGEmployeeData;
        $employee->company_id           = $company->id;
        $employee->user_id              = $request->input('user_id');
        $employee->full_name_1          = $request->input('full_name_1');
        $employee->full_name_2          = $request->input('full_name_2');
        $employee->dob                  = $request->input('dob');
        $employee->id_number            = $request->input('id_number');
        $employee->id_type              = $request->input('id_type');
        $employee->pr_date              = $request->input('pr_date');
        $employee->address              = $request->input('address');
        $employee->block_house          = $request->input('block_house');
        $employee->street               = $request->input('street');
        $employee->level                = $request->input('level');
        $employee->unit                 = $request->input('unit');
        $employee->line_1               = $request->input('line_1');
        $employee->line_2               = $request->input('line_2');
        $employee->line_3               = $request->input('line_3');
        $employee->postal_code          = $request->input('postal_code');
        $employee->country              = $request->input('country');
        $employee->nationality          = $request->input('nationality');
        $employee->sex                  = $request->input('sex');
        $employee->employment_start_date = $request->input('employment_start_date');
        $employee->probation_period     = $request->input('probation_period');
        $employee->confirmation_date    = $request->input('confirmation_date');
        $employee->termination_date     = $request->input('termination_date');
        $employee->termination_reason   = $request->input('termination_reason');
        $employee->job_title            = $request->input('job_title');
        $employee->fee_frequency        = ($request->input('fee_frequency')AND $request->input('fee_frequency') != NULL)?$request->input('fee_frequency'):0;
        $employee->basic_pay            = ($request->input('basic_pay')AND $request->input('basic_pay') != NULL)?$request->input('basic_pay'):0;
        $employee->hourly_rate          = ($request->input('hourly_rate')AND $request->input('hourly_rate') != NULL)?$request->input('hourly_rate'):0;
        $employee->daily_rate           = ($request->input('daily_rate')AND $request->input('daily_rate') != NULL)?$request->input('daily_rate'):0;
        $employee->overtime_entitlement = $request->input('overtime_entitlement');
        $employee->cpf_entitlement      = $request->input('cpf_entitlement');
        $employee->cpf_employee_group = $request->input('employee_group'); // additional field for cpf computationloyee->
        $employee->overtime_1_rate      = ($request->input('overtime_1_rate')AND $request->input('overtime_1_rate') != NULL)?$request->input('overtime_1_rate'):0;
        $employee->overtime_2_rate      = ($request->input('overtime_2_rate')AND $request->input('overtime_2_rate') != NULL)?$request->input('overtime_2_rate'):0;
        $employee->funds_selection      = json_encode($request->input('funds_selection'));
        $employee->salary_payment_mode  = $request->input('salary_payment_mode');
        $employee->bank_acct_name       = $request->input('bank_acct_name');
        $employee->bank                 = $request->input('bank');
        $employee->acct_number = $request->input('acct_number');

        $employee->save();

       // return redirect('payroll-update-employee/'.$employee->id)->with('success', 'Employee Succeffuly Added!');

         return redirect('payroll-manage-employees/')->with('success', 'Employee Succeffuly Added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = session('current_company');
        $users = User::join('company_user', 'users.id','=','company_user.user_id')
                    ->where('company_user.company_id',$company->id)
                    ->select('users.*')
                    ->get();

        $employee = SGEmployeeData::findOrFail($id);
        return view('dashboard.payroll.employees.edit', compact('users','employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee = SGEmployeeData::findOrFail($id);
        $employee->full_name_1          = $request->input('full_name_1');
        $employee->full_name_2          = $request->input('full_name_2');
        $employee->dob                  = $request->input('dob');
        $employee->id_number            = $request->input('id_number');
        $employee->id_type              = $request->input('id_type');
        $employee->pr_date              = $request->input('pr_date');
        $employee->address              = $request->input('address');
        $employee->block_house          = $request->input('block_house');
        $employee->street               = $request->input('street');
        $employee->level                = $request->input('level');
        $employee->unit                 = $request->input('unit');
        $employee->line_1               = $request->input('line_1');
        $employee->line_2               = $request->input('line_2');
        $employee->line_3               = $request->input('line_3');
        $employee->postal_code          = $request->input('postal_code');
        $employee->country              = $request->input('country');
        $employee->nationality          = $request->input('nationality');
        $employee->sex                  = $request->input('sex');
        $employee->employment_start_date = $request->input('employment_start_date');
        $employee->probation_period     = $request->input('probation_period');
        $employee->confirmation_date    = $request->input('confirmation_date');
        $employee->termination_date     = $request->input('termination_date');
        $employee->termination_reason   = $request->input('termination_reason');
        $employee->job_title            = $request->input('job_title');
        $employee->fee_frequency        = $request->input('fee_frequency');
        $employee->basic_pay            = $request->input('basic_pay');
        $employee->hourly_rate          = $request->input('hourly_rate');
        $employee->daily_rate           = $request->input('daily_rate');
        $employee->overtime_entitlement = $request->input('overtime_entitlement');
        $employee->cpf_entitlement      = $request->input('cpf_entitlement');
        $employee->overtime_1_rate      = $request->input('overtime_1_rate');
        $employee->overtime_2_rate      = $request->input('overtime_2_rate');
        $employee->funds_selection      = json_encode($request->input('funds_selection'));
        $employee->salary_payment_mode  = $request->input('salary_payment_mode');
        $employee->bank_acct_name       = $request->input('bank_acct_name');
        $employee->bank                 = $request->input('bank');
        $employee->acct_number = $request->input('acct_number');

        $employee->save();

        return redirect('payroll-update-employee/'.$employee->id)->with('success', 'Employee Succeffuly Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = SGEmployeeData::findOrFail($id);
        $employee->delete();
        return redirect()->back()->with('success', 'Employee Succeffuly Deleted!');
    }
}
