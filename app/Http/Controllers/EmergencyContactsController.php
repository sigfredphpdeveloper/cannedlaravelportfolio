<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Company;

use Config;
use Auth;
use Input;
use Intervention\Image\Facades\Image;
use Redirect;
use App\User;
use App\Classes\CustomFields;
use App\EmergencyContact;
use App\Permissions;

use DB;
use AWS;
use Storage;

use Excel;

class EmergencyContactsController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }


    public function index(Request $request)
    {

        #$this->validate($request, ['contact_name' => 'required','contact_phone'=>'required','contact_address'=>'required']);

        $user = Auth::user();

        $ec = $user->emergency_contact;

        if($ec){
            $ec = $ec->toArray();
        }else{
            $ec['contact_name'] = '';
            $ec['contact_phone'] = '';
            $ec['contact_address'] = '';
            $ec['relationship'] = '';
            $ec['relationship_other'] = '';
            $ec['id'] = '';
        }
        return view('dashboard.emergency.my_emergency_contact',compact('ec'));
    }

    function emergency_contacts_save(Request $request){

        $user = Auth::user();
        $ec = $user->emergency_contact;
        $post = Input::all();

        if($request->isMethod('post')){

            if($post['id'] == ''){

                $ec = EmergencyContact::create([
                    'user_id'=>$user->id,
                    'contact_name' => $post['contact_name'],
                    'contact_phone' => $post['contact_phone'],
                    'contact_address'=>$post['contact_address'],
                    'relationship' => $post['relationship'],
                    'relationship_other'=>$post['relationship_other'],
                ]);

            }else{

                $ec->contact_name = $post['contact_name'];
                $ec->contact_phone = $post['contact_phone'];
                $ec->contact_address = $post['contact_address'];
                $ec->relationship = $post['relationship'];
                $ec->relationship_other = $post['relationship_other'];
                $ec->save();
            }
            return Redirect::to('my_profile')->withInput()->with('success', 'Successfully Saved!');
        }

    }

    function settings(){

        $user = Auth::user();

        $company = Company::find(session('current_company')->id);

        $roles = $company->roles()->with('role_permissions')->get();

        $permissions = Permissions::whereIn('key',['view_emergency_contacts','edit_emergency_contacts'])->get()->toArray();

        return view('dashboard.emergency.emergency_contacts_settings',compact('user','company','permissions','roles'));

    }


    function manage_emergency_contacts(){

        $post = Input::all();

        $user = Auth::user();

        $company = session('current_company');

        $roles = $company->roles;

        $teams = $company->teams;

        $search = (isset($post['search']))?$post['search']:'';

        $emergency_contacts = User::select('users.*','emergency_contact.id AS id' ,'emergency_contact.*')
            ->join('company_user', 'users.id','=','company_user.user_id')
            ->join('emergency_contact', 'users.id','=','emergency_contact.user_id')
            ->where('company_user.company_id',$company->id)
            ->search($search)->get();

        $emergency_contacts = paginateCollection($emergency_contacts, config('app.page_count'));

        $emergency_contacts->appends(['search' => $search])->render();

        return view('dashboard.emergency.emergency_contacts_manage',compact('emergency_contacts','search'));

    }

    function view($id){

        if(!$id) return response('No ID Passed',404);

        $ec = User::select('users.*','emergency_contact.id AS id' ,'emergency_contact.*')
            ->join('company_user', 'users.id','=','company_user.user_id')
            ->join('emergency_contact', 'users.id','=','emergency_contact.user_id')
            ->where('emergency_contact.id',$id)
            ->first();

        if(!$ec) return response('not found',404);

        $ec = $ec->toArray();

        return view('dashboard.emergency.emergency_contact_view',compact('ec'));

    }


    function export_emergency_contacts(){

        Excel::create('Zenintra Emergency Contacts '.date('m/d/Y'), function($excel) {

            $excel->sheet('emergency_contact', function($sheet) {

                $emergency_contact = EmergencyContact::select('emergency_contact.*')
                    ->groupBy('emergency_contact.id')
                    ->get();

                foreach($emergency_contact as $k => $contact){
                     $emergency_contact[$k]->email = $this->__get_user($contact,'email');
                     $emergency_contact[$k]->employee = $this->__get_user($contact,'first_name').' '.$this->__get_user($contact,'last_name');
                }

                $sheet->fromArray($emergency_contact, null, 'A1', false, TRUE);

            });


        })->download('csv');

    }

    function __get_user($contact,$col){

        $user = DB::table('users')
            ->where('id',$contact->user_id)
            ->first();

        if($user){

            return $user->$col;
        }else{
            return '';
        }
    }


    public function emergency_contact_edit($id,Request $request){

        if(!$id) return response('No ID Received',404);

        $post = Input::all();

        if ($request->isMethod('post')){

            $ec = EmergencyContact::find($id);

            if(!$ec) return response('No Company Found',404);

            $ec->contact_name = $post['contact_name'];
            $ec->contact_phone = $post['contact_phone'];
            $ec->contact_address = $post['contact_address'];
            $ec->relationship = $post['relationship'];
            $ec->relationship_other = $post['relationship_other'];
            $ec->save();

            return Redirect::to('manage_emergency_contacts')->withInput()->with('success', 'Successfully Edited Emergency Contact!');
        }

        $ec = EmergencyContact::find($id);

        return view('dashboard.emergency.emergency_contact_edit',compact('ec'));
    }


}
