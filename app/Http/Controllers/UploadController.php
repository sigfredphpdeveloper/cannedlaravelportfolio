<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Organization;
use Excel;
use File;
use Input;
use Redirect;
use Request;
use Auth;
use DB;


// Models and Eloquent
use App\Contact;

class UploadController extends Controller
{

    public function excel()
    {
        $post = Input::all();

        // Ex. get all data
        $datas = Excel::load($_FILES['upload']['tmp_name'])->get()->toArray();

        $user = Auth::user();
        $roles = $user->user_roles->toArray();
        $user = $user->toArray(); // set into array;

        $contacts = Contact::select('contact_email')->join('organization','organization.id','=','contacts.organization_id')
            ->where('organization.company_id',session('current_company')->id)->get()->toArray();

        foreach($contacts as $k => $contact){
            $contacts[$k] = $contact['contact_email'];
        }

        $extention = File::extension($_FILES['upload']['name']);

        $datas = ($extention=='csv' || (!empty($datas[1])&&!empty($datas[2])))?$datas:$datas[0];
        
        foreach($datas as $data)
        {
            if(in_array ($data['contact_email'],$contacts)){
                return Redirect::to($post['redirect'])->withInput()->with('errors', 'Email address is already exist!');
            }

            if(!empty($data['contact_name']) && !empty($data['contact_email']) && !empty($data['organization'])){
                // get first organization
                $org = Organization::where('company_id', session('current_company')->id)
                        ->where('organization_name', $data['organization'])->get()->first();

                if(empty($org)){
                    $request['company_id']=session('current_company')->id;
                    $request['owner_id']=$user['id'];
                    $request['created_date']=date('Y-m-d H:i:s');
                    $request['nb_of_contacts']=1;
                    $request['organization_name']=$data['organization'];
                    $request['organization_address']='';
                    $request['visibility']='everyone'; // set as default
                    $request['extra_fields'] = json_encode(array());
                    $org = Organization::create($request);
                }

                $request['created_date']=date('Y-m-d H:i:s');
                $request['contact_name'] = $data['contact_name'];
                $request['contact_email'] = $data['contact_email'];
                $request['contact_phone'] = $data['contact_phone'];
                $request['organization_id'] = $org->id; // temporarily not appointed
                $request['owner_id'] = $user['id'];
                $request['position'] = !empty($data['position'])?$data['position']:'';
                $request['address'] = !empty($data['address'])?$data['address']:'';
                $request['contact_notes'] = !empty($data['notes'])?$data['notes']:'';

                $result = Contact::create($request);
            }else{
//                return Redirect::to($post['redirect'])->withInput()->with('errors', 'Contact Name, Contact Email, Organization, Contact Phone is important fields');
                return Redirect::to($post['redirect'])->withInput()->with('errors', 'Contacts have not been imported because some required fields were missing. Contact email, name and Organization are mandatory');
            }
        }

        return Redirect::to($post['redirect'])->withInput()->with('success', 'Successfully Uploaded Contacts, but some may not added because it exist!');

    }

    public function upload_organization(){
        $post = Input::all();

        // Ex. get all data
        $datas = Excel::load($_FILES['upload']['tmp_name'])->get()->toArray();

        $user = Auth::user();
        $roles = $user->user_roles->toArray();
        $user = $user->toArray(); // set into array;


        foreach($datas[0] as $data){

            if(!empty($data['organization_name'])){
                $request['company_id'] = session('current_company')->id;
                $request['owner_id'] = $user['id'];
                $request['organization_name'] = $data['organization_name'];
                $request['organization_address'] = $data['organization_address'];
                $request['visibility'] = $data['visibility'];
                $request['created_date'] = date('Y-m-d H:i:s');

                $result = Organization::create($request);
            }

        }

        return Redirect::to($post['redirect'])->withInput()->with('success', 'Successfully Uploaded Contacts!');
    }



}
