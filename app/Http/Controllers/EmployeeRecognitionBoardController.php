<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Company;
use App\User;
use App\Permissions;

use Input;
use Auth;
use Redirect;
use DB;
use Carbon\Carbon;

class EmployeeRecognitionBoardController extends Controller
{

    /**
     * Display a listing of the resource in Employee Management Board.
     *
     * @param void
     * @return view
     */
    public function index()
    {
        $company = Company::find(session('current_company')->id);

        // Company_id is for both owner & employee.
        $recognition_board_arr = DB::table('employee_recognition_board as board')
            ->select('board.*', 'employee.first_name as employee_first_name', 'employee.last_name as employee_last_name', 
                    'owner.first_name as owner_first_name', 'owner.last_name as owner_last_name')
            ->join('users as employee', 'employee.id', '=', 'board.employee_id')
            ->join('users as owner', 'owner.id', '=', 'board.owner_id')
            ->where('board.company_id', $company->id)
            ->where('board.status', '<>', 'deleted')
            ->orderBy('created_at', 'DESC')
            ->get();
        
        return view('dashboard.employee_management.recognition_board', compact('recognition_board_arr'));
    }


    /**
     * Creat a new recognition record.
     * 
     * @param Request $request
     * @return string
     */
    public function add_new_recognition(Request $request)
    {
        $owner = Auth::user();
        $company = Company::find(session('current_company')->id);

        $this->validate($request, ['employee_id' => 'required', 'title' => 'required', 'message' => 'required']);

        DB::table('employee_recognition_board')->insert([
            'owner_id' => $owner->id,
            'employee_id' => $request->input('employee_id'),
            'company_id' => $company->id,
            'title' => $request->input('title'),
            'message' => $request->input('message'),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        //Session::flash('success', 'Successfully Add New Recognition Record');

        return Redirect::to('employee_recognition_board')->withInput()->with('success', 'Successfully Add New Recognition');
    }


    /**
     * Edit the existing recognition record
     *
     * @param  \Illuminate\Http\Request  $request, $id
     * @return string
     */
    public function edit_recognition($id, Request $request)
    {
        $this->validate($request, ['employee_id' => 'required', 'title' => 'required', 'message' => 'required', 'status' => 'required']);

        DB::table('employee_recognition_board')->where('id', $id)->update([
            'employee_id' => $request->input('employee_id'), 
            'title' => $request->input('title'), 
            'message' => $request->input('message'), 
            'status' => $request->input('status'),
            'updated_at' => Carbon::now()
        ]);

        //Session::flash('success', 'Successfully Edit Recognition Record');

        return Redirect::to('employee_recognition_board')->withInput()->with('success', 'Successfully Edit Recognition');
    }


    /**
     * Delete mutiple records
     *
     * @param \Illuminate\Http\Request  $request
     * @return string
     */
    public function multiple_delete_recognition(Request $request) 
    {
        $ids = $request->input('ids');

        foreach($ids as $id) 
        {
            $del_record = DB::table('employee_recognition_board')->where('id', $id)->delete();
        }

        //Session::flash('success', 'Successfully Delete Many Recognition Records');

        return Redirect::to('employee_recognition_board')->withInput()->with('success', 'Successfully Delete Many Recognition');
    }


    /**
     * Delete one record
     *
     * @param \Illuminate\Http\Request $request, $id
     * @return  string
     */
    public function delete_recognition($id) 
    {
        $del_record = DB::table('employee_recognition_board')->where('id', $id)->delete();

        //Session::flash('success', 'Successfully Delete Recognition Record');

        return Redirect::to('employee_recognition_board')->withInput()->with('success', 'Successfully Delete Recognition');
    }


    public function settings() 
    {
        $user = Auth::user();

        $company = Company::find(session('current_company')->id);

        $roles = $company->roles()->with('role_permissions')->get();

        $permissions = Permissions::whereIn('key',['view_recognition_board'])->get()->toArray();

        return view('dashboard.employee_management.recognition_board_settings',compact('user','company','permissions','roles'));
    }


    public function get_employee_name_list() 
    {
        $company_id = session('current_company')->id;

        $employee_ids = DB::table('company_user')->where('company_id', $company_id)->lists('user_id');

        $employee_names = [];

        foreach($employee_ids as $id) {
            $row = DB::table('users')->find($id);
            array_push($employee_names, $row->first_name .' '. $row->last_name);
        }

        return json_encode([
            'employee_names' => $employee_names,
            'employee_ids' => $employee_ids
        ]);
    }


    public function get_edited_employee_information($id)
    {
        $employee_info = DB::table('employee_recognition_board')->where('id', $id)->first();

        $employee = DB::table('users')->where('id', $employee_info->employee_id)->first();

        $employee_name = $employee->first_name .' '.$employee->last_name;   

        return json_encode([
            'employee_name' => $employee_name,
            'status' => $employee_info->status,
            'title' => $employee_info->title,
            'message' => $employee_info->message
        ]);
    }
}
