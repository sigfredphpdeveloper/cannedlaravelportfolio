<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\HistoryUsersCompanies;
use App\Company;
use App\Company_user;
use App\User;
use App\UserLeaveAllowance;
use App\Leave_types;
use DB;
use App\LeaveTypesRoles;

class CronController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function history_logger()
    {

        if(!empty($_GET['key'])){

            $key = $_GET['key'];
            $app_cron_key = config('app.cronkey');

            if($key == $app_cron_key){

                $insert = [
                    'date' => date('Y-m-d H:i:s'),
                    'day' => date('d'),
                    'month' => date('m'),
                    'year' => date('Y'),
                    'total_user' => User::all()->count(),
                    'total_company' => Company::all()->count()
                ];

                HistoryUsersCompanies::create($insert);


            }else{
                abort(403, 'Unauthorized action.');
            }

        }else{
            abort(403, 'Unauthorized action.');

        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function carry_over_leave_types(){

        $last_year = date('Y-m-d',strtotime('-1 year'));

        //logic if start_date_allowance = january 2016 and oday is nov 28 2016 so last year is nov 28 2016 so meaning
        // nov 28 2015 is still <  january 2016
        // so only it will expired when january 2 2016 is greater than last year if for example last year is january 3 so meaning january 2 is expired
        $expired_leave_types = DB::table('leave_types')->where('start_date_allowance','<=',$last_year)->where('roll_over','=',1)->get();
#dd($expired_leave_types);
        //if carry over then refresh all user leave allowances
        foreach($expired_leave_types as $val){ //leave type loop

            $user_leave_allowance = UserLeaveAllowance::where('leave_types_id',$val->id)->get();
#dd($user_leave_allowance);
#echo $val->id.'<br />';
            $company_roll_over_for_leave_type_record = Leave_types::where('company_id',$val->company_id)->where('type',$val->type .' - Rollover')->first();
            $company_roll_over_for_leave_type_count = Leave_types::where('company_id',$val->company_id)->where('type',$val->type .' - Rollover')->count();

          # dump($company_roll_over_for_leave_type_record);
            //check if there's any consume on this specific leave type then if ever we will then create a roll over new leave type and create a user leave allowance for it
            if($company_roll_over_for_leave_type_count == 0){

                $company_roll_over_for_leave_type_record = Leave_types::create([
                    'company_id' => $val->company_id,
                    'type' => $val->type .' - Rollover',
                    'description' => $val->description,
                    'default_allowance' => 0,
                    'color_code' => $val->color_code,
                    'start_date_allowance' => date('Y-m-d'),
                    'roll_over'=>1,
                    'advance_leave'=>0,
                ]);

                //create the leave type roles
                $leave_type_roles = LeaveTypesRoles::where('leave_type_id',$val->id)->get();

                foreach($leave_type_roles as $ltr){
                    LeaveTypesRoles::create([
                        'leave_type_id'=>$company_roll_over_for_leave_type_record->id,
                        'role_id'=>$ltr->role_id
                    ]);
                }

            }

            //here is where the regen for users will happen
            foreach($user_leave_allowance as $allowance){

                //NORMAL REGEN
                $remaining = $allowance->remaining;

                $to_regen = $allowance->allowance;

                //if negative then subtract it to the allowance
                if($remaining < 0){

                    $to_regen = $allowance->allowance + $remaining;

                }

                //refresh for the normal leaves so if there's anything left do it for the roll overs
                //make the remaining and allowance same again
                $allowance->remaining = $to_regen;
                $allowance->update();

                $user = User::find($allowance->user_id);

                //find the roll over
                if(!$user OR !$company_roll_over_for_leave_type_record) continue;

                $check_user_leave_type_allowance = UserLeaveAllowance::where('user_id',$user->id)->where('leave_types_id',$company_roll_over_for_leave_type_record->id)->first();


                if(count($check_user_leave_type_allowance) > 0){

                    $remaining_record = $check_user_leave_type_allowance->remaining;

                    $to_regen = $remaining_record + $remaining;

                    if($remaining < 0){

                        $to_regen = $remaining_record;

                    }


                    $check_user_leave_type_allowance->allowance =  $to_regen;
                    $check_user_leave_type_allowance->remaining =  $to_regen;
                    $check_user_leave_type_allowance->update();

                }else{

                    $check_leave_type_allowance = $user->leave_allowances()->create([
                    'allowance'=>($remaining<0)?0:$remaining,
                    'remaining'=>($remaining<0)?0:$remaining,
                    'leave_types_id'=>$company_roll_over_for_leave_type_record->id
                   ]);

                }

            }


            //FINALLY
            //reset leave type start date to today so next year it will cycle again
            DB::table('leave_types')
                ->where('id', $val->id)
                ->update(['start_date_allowance' => date('Y-m-d')]);


        }

    }

}
