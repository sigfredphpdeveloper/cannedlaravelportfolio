<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Config;
use Input;
use Intervention\Image\Facades\Image;
use Redirect;
use App\User;
use App\Classes\CustomFields;

use DB;
use AWS;
use Storage;


use Auth;
use App\Company;
use App\User_roles;
use App\Company_user;
use App\Roles;

use Response;

use Excel;
use Carbon\Carbon;


class AdminUserController extends Controller
{

    function manage_users(){

        $input = Input::all();

        $search = (isset($input['search']))?$input['search']:NULL;
        $type = (isset($input['type']))?$input['type']:NULL;

        $users = User::select('users.*')
            ->join('company_user', 'users.id','=','company_user.user_id')
            ->join('company', 'company_user.company_id','=','company.id')
            ->orderBy('users.created_at','DESC')
            ->groupBy('users.id');

        if($search){

                if($type == 'id'){
                    $users = $users->where('users.id','like','%'.$search.'%');
                }elseif($type =='username'){
                    $users = $users->where('email','like','%'.$search.'%');
                }elseif($type =='name'){
                    $users = $users->where('first_name','like','%'.$search.'%')->orWhere('last_name','like','%'.$search.'%');
                }
        }


         $users = $users->get();




        foreach($users as $k=> $user){
            $users[$k]->is_master = $this->__check_if_user_is_master($user);
            $users[$k]->companies = $this->__get_commpanies_lists($user);
        }


        return view('admin.dashboard.users.users_manage',compact('users','search','type'));

    }

    function __check_if_user_is_master($user){

        $company_user_record = DB::table('users')
            ->select('company_user.is_master')
            ->join('company_user', 'users.id','=','company_user.user_id')
            ->where('users.id',$user->id)
            ->get();

        $is_master = FALSE;

        foreach($company_user_record as $record){
            if($record->is_master==1) $is_master=TRUE;
        }

        return $is_master;
    }

    function __get_commpanies_lists($user){

        $companies = DB::table('company_user')
            ->join('company', 'company_user.company_id','=','company.id')
            ->where('company_user.user_id',$user->id)
            ->get();

        return $companies;
    }

    function users_add(Request $request){

        $this->validate($request, ['email' => 'required|email|max:255|unique:users','password' => 'required|min:6']);

        $post = Input::all();

        if($post) {

            /*
             *
             * array:9 [?
              "_token" => "GYKk1iTuSOwOENNx7Xl0uQdi30MY0AcKeQDuOhqs"
              "email" => ""
              "password" => ""
              "title" => ""
              "first_name" => ""
              "position" => ""
              "phone_number" => ""
              "company_url" => "Fewstones"
              "company_id" => "1"
            ]

            Master <input  type="radio" name="role" value="is_master" />
            Guest <input type="radio" name="role" value="is_guest"  />

            */

            $role = $post['role'];

            //Create User
            $user = User::create([
                'first_name' => $post['first_name'],
                'last_name' => $post['last_name'],
                'title' => $post['title'],
                'email' => $post['email'],
                'password' => bcrypt($post['password']),
                'user_level' => $role,
                'position' => $post['position'],
                'phone_number' => $post['phone_number']
            ]);

            $company = DB::table('company')
                ->where('url',$post['company_url'])
                ->first();

            //company not yet existing ZERO
            if($post['company_id'] == '' AND !$company){

                $company_save = Company::create([
                    'file_sharing'=>1,
                    'announcements'=>1,
                    'url' => $post['company_url']
                ]);

                $role_admin = Roles::create([
                    'role_name' => 'Administrator',
                    'role_description' => 'Administrator of the company',
                    'default_role' => '1',
                    'company_id' => $company_save->id
                ]);

                $user_role = User_roles::create([
                    'user_id' => $user->id,
                    'role_id' => $role_admin->id
                ]);

                Roles::create([
                    'role_name' => 'Employee',
                    'role_description' => 'Employee',
                    'default_role' => '1',
                    'company_id' => $company_save->id
                ]);

                $user_company_mapping = Company_user::create(
                    [
                        'company_id' => $company_save->id,
                        'user_id' => $user->id,
                        'is_master'=> 1
                    ]
                );

            }else{//company existing

                $is_master = 0;

                if($post['role'] == 'is_master'){ //find master role ID of company

                    $roles = DB::table('roles')->where('role_name','Administrator')->where('company_id',$company->id)->first();

                    $user_role = User_roles::create([
                        'user_id' => $user->id,
                        'role_id' => $roles->id
                    ]);

                    $is_master = 1;

                }else{

                    $roles = DB::table('roles')->where('role_name','Employee')->where('company_id',$company->id)->first();

                    $user_role = User_roles::create([
                        'user_id' => $user->id,
                        'role_id' => $roles->id
                    ]);
                }

                $is_guest = ($is_master==0)?1:0;

                $user_company_mapping = Company_user::create(
                    [
                        'company_id' => $company->id,
                        'user_id' => $user->id,
                        'is_master'=> $is_master,
                        'is_guest'=>$is_guest //if ! master then just guest
                    ]
                );

            }

            return Redirect::to('manage_users');


        }
    }


    public function users_edit($id,$company_id,Request $request){

        if(!$id) return response('No ID Received',404);

        $post = Input::all();

        $company = Company::find($company_id);

        if ($request->isMethod('post')){

            $user = User::find($id);

            if(!$user) return response('No User Found',404);

            $company_user = DB::table('company')
                ->select('company.*','company_user.is_master','company_user.is_guest')
                ->join('company_user', 'company_user.company_id','=','company.id')
                ->where('company_user.user_id', $id)
                ->where('company_user.company_id',$company_id)
                ->first();

            $check_company = DB::table('company')
                ->where('url', $post['company_url'])
                ->first();

            //meaning original company changed
            if($post['company_url'] != $company_user->url){

                //check if company is existing URL
                $check_company = DB::table('company')
                    ->where('url', $post['company_url'])
                    ->first();

                //if company URL is valid and existing
                if ($check_company) {

                    //change company_user assignment
                    $update = ['company_id'=>$check_company->id];

                    DB::table('company_user')
                        ->where('company_id', $company->id)
                        ->where('user_id',$user->id)
                        ->update($update);

                }
            }

            if($post['password'] != ''){
                $user->password = bcrypt($post['password']);
            }

            $user->first_name = $post['first_name'];
            $user->last_name = $post['last_name'];
            $user->title = $post['title'];
            $user->email = $post['email'];
            $user->position = $post['position'];
            $user->phone_number = $post['phone_number'];

            $user->save();

            $is_master = 0;
            if($post['role'] == 'is_master'){
                $is_master = 1;
            }
            $is_guest = ($is_master==0)?1:0;

            $update = ['is_master'=>$is_master,'is_guest'=>$is_guest];

            DB::table('company_user')
                ->where('company_id', $company_user->id)
                ->where('user_id',$user->id)
                ->update($update);

            return Redirect::to('manage_users')->withInput()->with('success', 'Successfully Edited User!');
        }

        $user = User::find($id);

        $company_user = DB::table('company')
            ->select('company.*','company_user.is_master','company_user.is_guest')
            ->join('company_user', 'company_user.company_id','=','company.id')
            ->where('company_user.user_id', $id)
            ->where('company_user.company_id',$company_id)
            ->first();

        return view('admin.dashboard.users.users_edit',compact('user','company_user'));
    }

    function companies_list(){

        $post = Input::all();

        $user = Auth::user();


        $companies = Company::select('id','url AS label','url AS value')
            ->where('url', 'LIKE', '%'.$post['q'].'%')
            ->get()->toArray();

        return Response::json($companies);

    }

    function user_delete($id){

        if(!$id) return response('ID not found',404);

        $user = User::find($id);

        if($user){

            $user->delete();

            return Redirect::to('manage_users')->withInput()->with('success', 'Successfully Deleted User!');

        }else{

            return Redirect::to('manage_users')->withInput()->with('error', 'Error Occured!');

        }

    }

    function export_users(){

        Excel::create('Zenintra Users '.date('m/d/Y'), function($excel) {

            $excel->sheet('users', function($sheet) {

                $users = User::select('users.*')
                    ->join('company_user', 'users.id','=','company_user.user_id')
                    ->join('company', 'company_user.company_id','=','company.id')
                    ->groupBy('users.id')->get();

                foreach($users as $k=> $user){
                    $users[$k]->is_master = $this->__check_if_user_is_master($user);
                    $users[$k]->companies = $this->__get_commpanies_lists($user);
                }


                $sheet->fromArray($users, null, 'A1', false, TRUE);


            });


        })->download('csv');


    }

    function admin_login_user($user_id,Request $request){
            session()->flush();
            $user = User::find($user_id);
            Auth::login($user);
             return redirect('/dashboard');

    }

    public function users_reports(Request $request)
    {

        return view('admin.dashboard.users.users_reports');
    }

    public function graph_report(Request $request)
    {

        $range = $this->get_from_to($request->input('days'));

        $start = $range['start'];
        $to = $range['to'];

        $stats = DB::table('users')
            ->groupBy('date')
            ->orderBy('date', 'ASC');

        if($start !='' AND $to != ''){
            $stats = $stats->whereBetween('created_at', array( $start, $to));
        }

        $stats = $stats
            ->get([
                DB::raw('DATE(created_at) as date'),
                DB::raw('COUNT(*) as value')
            ]);

        return json_encode($stats);
    }


    public function graph_report_cumulative(Request $request)
    {

        $range = $this->get_from_to($request->input('days'));

        $start = $range['start'];
        $to = $range['to'];

        $stats = DB::table('history_users_companies')
            ->select(DB::raw('DATE(date) AS date'),
                DB::raw('total_user AS value'));

        if($start != '' AND $to != ''){
            $stats = $stats->whereBetween('date', array( $start, $to));
        }

        $stats = $stats->orderBy('date', 'ASC')->get();

        return json_encode($stats);
    }

    function get_from_to($days){

        $range = [];
        $start = new Carbon(date("Y-m-d", strtotime("-30 day")));
        $to = Carbon::now();

        if( $days == '6-months-ago' )
        {
            $start = new Carbon(date("Y-m-d", strtotime("-6 months")));
            $to = Carbon::now();
        }
        elseif( $days == '12-months-ago' )
        {
            $start = new Carbon(date("Y-m-d", strtotime("-12 months")));
            $to = Carbon::now();
        }
        elseif( $days == 'all-time' )
        {
            $start = '';
            $to = '';
        }
        else
        {
            $start = new Carbon(date("Y-m-d", strtotime("-$days day")));
            $to = Carbon::now();
        }

        $range['start'] = $start;
        $range['to'] = $to;

        return $range;

    }



}
