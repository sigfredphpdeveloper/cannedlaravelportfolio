<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use Config;
use Input;
use Intervention\Image\Facades\Image;
use Redirect;
use App\User;
use App\Classes\CustomFields;

use DB;
use AWS;
use Storage;


use Auth;
use App\Company;
use App\User_roles;
use App\Company_user;
use App\Roles;
use App\EmailTemplates;

use Response;

use Excel;

use App\Http\Requests\EmailTemplatesRequest;

class EmailTemplatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $email_templates = EmailTemplates::all();

        return view('admin.dashboard.emails.emails_manage', compact('email_templates'));
    }

    public function edit($id, Request $request)
    {

        $email_template = EmailTemplates::find($id);

        return view('admin.dashboard.emails.emails_edit', compact('email_template'));
    }

    public function update(Request $request, $id)
    {

        $email_template = EmailTemplates::find($id);

        $email_template->update($request->all());

        return Redirect::to('email_templates/'.$id.'/edit')->withInput()->with('success', 'Successfully Updated!');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }




    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
