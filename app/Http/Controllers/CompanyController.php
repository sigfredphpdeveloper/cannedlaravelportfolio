<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Listeners\PushNotificationEventListener;


use Config;
use Auth;
use Input;
use Redirect;

use App\User;
use App\Company;
use App\Company_user;
use App\Roles;
use App\User_roles;
use App\User_teams;
use App\User_fields;
use App\EmailTemplates;

use Illuminate\Mail\Mailer;
use Mail;

use Intervention\Image\Facades\Image;

use DB;
use AWS;
use Storage;


use Vansteen\Sendinblue\Facades\SendinblueWrapper;
use Sendinblue\Mailin;
use App\Classes\Helper;


use App\PayrollDeduction;
use App\PayrollAddition;

class CompanyController extends Controller
{

    public function __construct(){
        $this->middleware('auth',['except'=>['company_join_request','accept_company_join_request','accept_invite','accept_invite_process','activate_user']]);
    }

    public function my_company(){

        return view('dashboard.my_company');

    }

    public function my_company_save(Request $request){
        $user = Auth::user();

        $post = Input::all();

        if ($post) {

            if(array_key_exists('company_id',$post))
            {

                $this->validate($request, ['industry' => 'required','address'=>'required','country'=>'required']);

                $company = Company::find($post['company_id']);

                $company->company_name = $post['company_name'];
                $company->url = $post['url'];
                $company->industry = $post['industry'];
                $company->size = $post['size'];
                $company->address = $post['address'];
                $company->zip = $post['zip'];
                $company->city = $post['city'];
                $company->country = $post['country'];
                $company->state = $post['state'];
                $company->phone_number = $post['phone_number'];

                if(!empty($post['logo']))
                    $company->logo = $post['logo'];

                    if($post['country'] == 'SG'){

                    $company->payroll = 1 ;

                    //add default deductions

                    $default_deductions_count = PayrollDeduction::where('company_id',$company->id)->where('default_deduction',1)->count();
                    $default_additions_count = PayrollAddition::where('company_id',$company->id)->where('default_addition',1)->count();

                    //create only the deductions once so when > 1 dont create it only when zero
                    if($default_deductions_count == 0){

                        $payroll_deduction = new PayrollDeduction;
                        $payroll_deduction->company_id = $company->id;
                        $payroll_deduction->title = 'CDAC';
                        $payroll_deduction->cpf_deductible = 0;
                        $payroll_deduction->default_deduction = 1;
                        $payroll_deduction->save();


                        $payroll_deduction = new PayrollDeduction;
                        $payroll_deduction->company_id = $company->id;
                        $payroll_deduction->title = 'MBMF';
                        $payroll_deduction->cpf_deductible = 0;
                        $payroll_deduction->default_deduction = 1;
                        $payroll_deduction->save();


                        $payroll_deduction = new PayrollDeduction;
                        $payroll_deduction->company_id = $company->id;
                        $payroll_deduction->title = 'ECF';
                        $payroll_deduction->cpf_deductible = 0;
                        $payroll_deduction->default_deduction = 1;
                        $payroll_deduction->save();


                        $payroll_deduction = new PayrollDeduction;
                        $payroll_deduction->company_id = $company->id;
                        $payroll_deduction->title = 'SINDA';
                        $payroll_deduction->cpf_deductible = 0;
                        $payroll_deduction->default_deduction = 1;
                        $payroll_deduction->save();

                    }

                    if($default_additions_count == 0){

                        $payroll_addition = new PayrollAddition;
                        $payroll_addition->company_id = $company->id;
                        $payroll_addition->title = 'Transport Allowance';
                        $payroll_addition->cpf_payable = 1;
                        $payroll_addition->taxable = 1;
                        $payroll_addition->other_allowance = 0;
                        $payroll_addition->category_id = 5;
                        $payroll_addition->default_addition = 1;
                        $payroll_addition->save();

                        $payroll_addition = new PayrollAddition;
                        $payroll_addition->company_id = $company->id;
                        $payroll_addition->title = 'Entertainment Allowance';
                        $payroll_addition->cpf_payable = 1;
                        $payroll_addition->taxable = 1;
                        $payroll_addition->other_allowance = 0;
                        $payroll_addition->category_id = 5;
                        $payroll_addition->default_addition = 1;
                        $payroll_addition->save();


                        $payroll_addition = new PayrollAddition;
                        $payroll_addition->company_id = $company->id;
                        $payroll_addition->title = 'Other Allowances';
                        $payroll_addition->cpf_payable = 1;
                        $payroll_addition->taxable = 1;
                        $payroll_addition->other_allowance = 0;
                        $payroll_addition->category_id = 5;
                        $payroll_addition->default_addition = 1;
                        $payroll_addition->save();

                        $payroll_addition = new PayrollAddition;
                        $payroll_addition->company_id = $company->id;
                        $payroll_addition->title = 'Sales Commissions';
                        $payroll_addition->cpf_payable = 1;
                        $payroll_addition->taxable = 1;
                        $payroll_addition->other_allowance = 0;
                        $payroll_addition->category_id = 5;
                        $payroll_addition->default_addition = 1;
                        $payroll_addition->save();

                        $payroll_addition = new PayrollAddition;
                        $payroll_addition->company_id = $company->id;
                        $payroll_addition->title = 'Bonus';
                        $payroll_addition->cpf_payable = 1;
                        $payroll_addition->taxable = 1;
                        $payroll_addition->other_allowance = 1;
                        $payroll_addition->category_id = 1;
                        $payroll_addition->default_addition = 1;
                        $payroll_addition->save();

                        $payroll_addition = new PayrollAddition;
                        $payroll_addition->company_id = $company->id;
                        $payroll_addition->title = 'Director\'s fees';
                        $payroll_addition->cpf_payable = 1;
                        $payroll_addition->taxable = 1;
                        $payroll_addition->other_allowance = 1;
                        $payroll_addition->category_id = 2;
                        $payroll_addition->default_addition = 1;
                        $payroll_addition->save();

                        $payroll_addition = new PayrollAddition;
                        $payroll_addition->company_id = $company->id;
                        $payroll_addition->title = 'Leave Pay';
                        $payroll_addition->cpf_payable = 1;
                        $payroll_addition->taxable = 1;
                        $payroll_addition->other_allowance = 1;
                        $payroll_addition->category_id = 4;
                        $payroll_addition->default_addition = 1;
                        $payroll_addition->save();

                        $payroll_addition = new PayrollAddition;
                        $payroll_addition->company_id = $company->id;
                        $payroll_addition->title = 'Notice Pay';
                        $payroll_addition->cpf_payable = 0;
                        $payroll_addition->taxable = 1;
                        $payroll_addition->other_allowance = 1;
                        $payroll_addition->category_id = 5;
                        $payroll_addition->default_addition = 1;
                        $payroll_addition->save();

                        $payroll_addition = new PayrollAddition;
                        $payroll_addition->company_id = $company->id;
                        $payroll_addition->title = 'Pension';
                        $payroll_addition->cpf_payable = 0;
                        $payroll_addition->taxable = 1;
                        $payroll_addition->other_allowance = 1;
                        $payroll_addition->category_id = 3;
                        $payroll_addition->default_addition = 1;
                        $payroll_addition->save();

                    }

                }



                $company->save();

                return Redirect::to('my_company')->withInput()->with('success', 'Successfully Updated Your Company!');

            }

        }

    }

    public function invite_user(){

        $user = Auth::user();

        $company = Company::find(session('current_company')->id);

        $roles = $company->roles;

        $teams = $company->teams;

        return view('dashboard.invite_user',compact('roles','teams'));

    }



    public function send_invitation(Request $request){

        #$this->validate($request, ['email' => 'required|email']);

        $user = Auth::user();

        $sess_company = session('current_company');

        $company = Company::find($sess_company->id);

        $post = Input::all();

        $guest = FALSE;

        if(!empty($post['is_guest'])){
            $guest = TRUE;
        }

        $all_array = []; $push_array = [];

        if(!empty($post['email'])){

            $count=1;

            // Push Directory to Node Server
            foreach($post['email'] as $key=>$val){

                $push_array = $this->process_send_invitation($post,$key,$user,$company,$count++,$guest);

                array_push($all_array, $push_array);
            }

            if($_SERVER['SERVER_NAME'] != 'zenintra.local'){
                $push_notification = new PushNotificationEventListener();
                $push_notification->pushNotification($all_array);
            }

        }

        $message = 'Successfully Invited User!';

        if($guest){
            $message = 'Successfully Invited Guest';
        }

        return Redirect::to('directory')->withInput()->with('success', $message);


    }

    function process_send_invitation($post,$key,$user,$company,$count,$guest = FALSE){

        $roles_arr = []; $teams_arr = []; $rooms_arr = [];

        //userid current + bcript of time
        $hash = $user['id'].'-'.md5(time()).$count;

        $invitee_name = $post['first_name'][$key] .' ' . $post['last_name'][$key];

        //check if user has recently been deleted
        $check_user = User::withTrashed()->where('email', $post['email'][$key])
            ->first();

        if (!is_null($check_user) AND $check_user->trashed()) { //found but trash

            $check_user->restore();

            $update = [
                'first_name' => $post['first_name'][$key],
                'last_name' => $post['last_name'][$key],
                'user_level' => 'employee',
                'invite_hash' => $hash,
                'invited_by' => $user['id'],
                'invited_on' => date('Y-m-d H:i:s'),
                'status'=>'unverified'
            ];

            DB::table('users')
                ->where('id',$check_user->id)
                ->update($update);

            $new_user = $check_user;

        }else{ //found or not found

            //check if found
            $check_user_found = User::where('email', $post['email'][$key])
                ->first();

            if(!is_null($check_user_found) AND !$guest){
                return Redirect::to('invite_user')->withInput()->with('error', 'User is already existing!');
            }

            if($check_user_found AND $guest){ // found already and its a guest
                //set all to zero
                Company_user::where('user_id', $check_user_found->id)
                    ->update(['guest_default' => 0]);

                $user_company_mapping = Company_user::create(
                    [
                        'company_id' => $company->id,
                        'user_id' => $check_user_found->id,
                        'is_master'=> 0,
                        'is_guest'=> ($guest==TRUE)?1:0,
                        'guest_default'=>($guest==TRUE)?1:0

                    ]
                );

                $this->email_new_invited($check_user_found->invite_hash,$user,$company,$check_user_found,$invitee_name,$guest,$post,$key);

                return;

            }


        }

        $new_user = User::create([
                'email' => $post['email'][$key],
                'first_name' => $post['first_name'][$key],
                'last_name' => $post['last_name'][$key],
                'user_level' => 'employee',
                'invite_hash' => $hash,
                'invited_by' => $user['id'],
                'invited_on' => date('Y-m-d H:i:s'),
                'status'=>'unverified',
                'first_visit'=>1,
                'start_date'=>date('Y-m-d')
            ]);

        $user_company_mapping = Company_user::create(
            [
                'company_id' => $company->id,
                'user_id' => $new_user->id,
                'is_master'=> 0,
                'is_guest'=>($guest==TRUE)?1:0,
                'guest_default'=>($guest==TRUE)?1:0

            ]
        );



        $company_work_week = DB::table('company_work_week')->where('company_id',$company->id)->first();

        if(count($company_work_week)>0){
            $new_user->work_week_id = $company_work_week->id;
            $new_user->update();

        }

        $this->email_new_invited($hash,$user,$company,$new_user,$invitee_name,$guest,$post,$key);




    }

    function email_new_invited($hash,$user,$company,$new_user,$invitee_name,$guest,$post = NULL,$key = NULL){
        $url = url('/accept_invite/'.$hash);
        $link = '<a href="'.$url.'" >'.$url.'</a>';

        $username = $user->username;
        $company_name = $company->company_name;
        $your_username = $new_user->email;
        $invitor_name = $user->first_name.' '.$user->last_name;
        $invitee_first_name = $new_user->first_name;


        if($guest){
            $email_template = EmailTemplates::where('slug','invite_guest')->first();
        }else{
            $email_template = EmailTemplates::where('slug','invite_user')->first();
        }


        $for_parsing = [
            'company_name' => $company_name
        ];

        $subject = Helper::parse_email($email_template->subject,$for_parsing);

         $for_parsing = [
             'invitee_first_name' => $invitee_first_name,
             'invitor_name' => $invitor_name,
             'company_name' => $company_name,
             'your_username' => $your_username,
             'link'=>$link
         ];

        $body = Helper::parse_email($email_template->body,$for_parsing);

        $to_name = $your_username;

        Mail::send('emails.default_email', compact('body'), function($message)
        use($subject,$invitee_name,$new_user)
        {
            $message->subject($subject);
            $message->to($new_user->email, $invitee_name);
        });


        $post_email = [];
        $post_roles = [];
        $post_teams = [];

        if(!empty($post['email'][$key])){
            $post_email = $post['email'][$key];
        }

        if(!empty($post['roles'][$key])){
            $post_roles = $post['roles'][$key];
        }

        if(!empty($post['teams'][$key])){
            $post_teams = $post['teams'][$key];
        }

        $rooms_arr = [];
        if($guest == FALSE){

                if(!empty($post['roles'][$key])) {
                    foreach ($post['roles'][$key] as $role) {

                        $user_role = User_roles::create([
                            'user_id' => $new_user->id,
                            'role_id' => $role
                        ]);


                        array_push($rooms_arr, 'company_' . $company->id . '_role_' . $role);
                    }
                }

                if (!empty($post['teams'][$key])) {
                    foreach ($post['teams'][$key] as $team) {

                        $user_team = User_teams::create([
                            'user_id' => $new_user->id,
                            'team_id' => $team
                        ]);


                        array_push($rooms_arr, 'company_' . $company->id . '_team_' . $team);
                    }

                }

            // Push Directory To Mobile Application
            $array = array(
                'user_id'       => $new_user->id,
                'event'         => Config::get('constants.EVENT_DIRECTORY.ADD_DIRECTORY'),
                'invitee_name'  => $invitee_name,
                'invitee_email' => $post_email,
                'invitee_roles' => $post_roles,
                'invitee_teams' => $post_teams,
                'rooms'         => $rooms_arr
            );

            return $array;



        }


    }

    function personalized_fields(){

        $user = Auth::user();

        $company = $user->company_user->first();

        $user_fields = $company->user_fields->toArray();

        return view('dashboard.personalized_fields.manage_pfields',compact('user_fields'));

    }

    public function pfields_add()
    {

        return view('dashboard.personalized_fields.pfields_add');
    }

    public function pfields_add_save(Request $request)
    {

        $this->validate($request, ['field_name' => 'required','field_type'=>'required']);

        $user = Auth::user();

        $company = $user->company_user->first();

        $user_fields = $company->user_fields;

        $post = Input::all();

        if ($post)
        {
            if(count($user_fields)==10){
                return Redirect::to('personalized_fields')->withInput()->with('error', '10 Fields limit reached!');
            }

            $user_fields = User_fields::create([
                'company_id' => $post['company_id'],
                'field_name' => $post['field_name'],
                'field_description' => $post['field_description'],
                'field_type' => $post['field_type'],
                'field_value' => $post['field_value'],
                'field_visibility' => (array_key_exists('field_visibility',$post))?$post['field_visibility']:0,
            ]);

        }

        return Redirect::to('personalized_fields')->withInput()->with('success', 'Successfully Added Field!');
    }


    public function pfields_edit($id){

        $post = Input::all();

        if ($post)
        {
            $user_field = User_fields::find($id);

            $user_field->field_name = $post['field_name'];
            $user_field->field_type = $post['field_type'];
            $user_field->field_value = $post['field_value'];
            $user_field->field_description = $post['field_description'];
            $user_field->field_visibility = (array_key_exists('field_visibility',$post))?1:0;

            $user_field->save();

            return Redirect::to('pfields_edit/'.$id)->withInput()->with('success', 'Successfully Edited Team!');

        }

        $user_field = User_fields::find($id);

        $field_types = ['text'=>'Text','date'=>'Date','number'=>'Number','list_dropdown'=>'List Dropdown'];

        return view('dashboard.personalized_fields.pfields_edit',compact('user_field','field_types'));

    }

    public function pfields_delete($id){

        if(!$id){
            return Redirect::to('personalized_fields')->withInput()->with('error', 'Error Occured!');
        }

        $user_field = User_fields::find($id);

        if($user_field){
            $user_field->delete();
            return Redirect::to('personalized_fields')->withInput()->with('success', 'Successfully Deleted Field!');
        }else{
            return Redirect::to('personalized_fields')->withInput()->with('error', 'Error Occured!');
        }

    }

    function company_join_request($company_id = NULL,$invitee = NULL){

        if(!$company_id OR !$invitee) abort(404);

        $found_company = DB::table('company')->find($company_id);

        if(!$found_company) return response("Company not found",400);

        $company_user = DB::table('company_user')->where('is_master',1)->where('company_id',$found_company->id)->first();

        if(!$company_user) return response("Company User Not found",400);

        $master_user = DB::table('users')->find($company_user->user_id);

        if(!$master_user) return response("Master User Not found",400);

        $url = url('/accept_company_join_request/'.$company_id.'/'.$invitee);

        $link = '<a href="'.$url.'" >'.$url.'</a>';

        $email_template = EmailTemplates::where('slug','company_join_request')->first();

        $for_parsing = [
            'found_company_name' => $found_company->company_name,
        ];

        $subject = Helper::parse_email($email_template->subject,$for_parsing);

        $for_parsing = [
            'master_first_name'=>$master_user->first_name,
            'master_last_name'=>$master_user->last_name,
            'found_company_name' => $found_company->company_name,
            'invitee'=>$invitee,
            'link'=>$link
        ];

        $body = Helper::parse_email($email_template->body,$for_parsing);

        Mail::send('emails.default_email', compact('body'), function($message)
        use($subject,$master_user)
        {
            $message->subject($subject);
            $message->to($master_user->email, $master_user->first_name .' ' .$master_user->last_name);
        });

        return Redirect::to('register')->withInput()->with('success', 'Successfully Requested Access to '.$found_company->company_name);

    }

    function accept_company_join_request($company_id = NULL,$invitee = NULL){
        if(!$company_id OR !$invitee) abort(404);


        $found_company = DB::table('company')->find($company_id);

        if(!$found_company) return response("Company not found",400);

        $company_user = DB::table('company_user')->where('is_master',1)->where('company_id',$found_company->id)->first();

        if(!$company_user) return response("Company User Not found",400);

        $master_user = DB::table('users')->find($company_user->user_id);

        if(!$master_user) return response("Master User Not found",400);

        //userid current + bcript of time
        $hash = $master_user->id.'-'.md5(time());

        $post = Input::all();

        $invitee_name = '';

        $new_user = User::create([
            'email' => $invitee,
            'first_name' => '',
            'last_name' => '',
            'user_level' => 'employee',
            'invite_hash' => $hash,
            'invited_by' => $master_user->id,
            'invited_on' => date('Y-m-d H:i:s')
        ]);

        $user_company_mapping = Company_user::create(
            [
                'company_id' => $found_company->id,
                'user_id' => $new_user->id,
                'is_master'=> 0
            ]
        );

        $roles = DB::table('roles')->where('company_id',$found_company->id)->get();

        $role_id = '';

        foreach($roles as $role)
            if($role->role_name == 'Employee') $role_id = $role->id;

        if($role_id){
            $user_role = User_roles::create([
                'user_id' => $new_user->id,
                'role_id' => $role_id
            ]);
        }

        $url = url('/accept_invite/'.$hash);
        $link = '<a href="'.$url.'" >'.$url.'</a>';

        $username = $master_user->email;
        $company_name = $found_company->company_name;
        $your_username = $new_user->email;
        $invitor_name = $master_user->first_name.' '.$master_user->last_name;
        $invitee_first_name = $new_user->first_name;

        $subject = 'Invitation to join '.$company_name.' on Zenintra';

        $to_name = $your_username;

        Mail::send('emails.invite_user', compact('username','company_name','your_username','link','invitor_name','invitee_first_name'), function($message)
        use($subject,$invitee_name,$new_user)
        {
            $message->subject($subject);
            $message->to($new_user->email, $invitee_name);
        });

        return Redirect::to('register')->withInput()->with('success', 'Successfully Invited User!');

    }

    function accept_invite($hash = NULL){

        $invited_user = User::where('invite_hash',$hash)->first();
        if($invited_user){

            /*
            if($invited_user->guest_company()){

                //meaning there is already a guest company
                session()->flush();
                Auth::login($invited_user);
                return redirect('/dashboard');

            }
*/

            //we need to add the is_guest exception to see if he has a guest account already
            $invitor_user = User::find($invited_user->invited_by);
            $invitor_user_company = $invitor_user->company_user()->first()->toArray();

            return view('auth.accept_invite',compact('invited_user','invitor_user','invitor_user_company'));

        }else{
            abort(404);
        }

    }

    function accept_invite_process(Request $request){

        $this->validate($request, ['password' => 'required|confirmed|min:6']);

        $post = Input::all();

        if($post){

            $user = User::find($post['user_id']);

            $user->password = bcrypt($post['password']);

            $user->save();

            $hash = $user->id.'-'.md5(time());

            $user->activate_hash = $hash;

            $user->save();

            $company = $user->company_user->first();

            //send in blue users list before redirect
            $list_id = config('app.sendinblue_list_users');

            $data = array( "email" => $user->email,
                "attributes" => array("NAME"=>$user->first_name, "SURNAME"=>$user->last_name),
                "listid" => array($list_id)
            );

            $response = (SendinblueWrapper::create_update_user($data));

            //Welcome OPT IN Email
            $url = url('/activate_user/'.$hash);
            $link = '<a href="'.$url.'" >'.$url.'</a>';


            $email_template = EmailTemplates::where('slug','company_signup_user')->first();

            $for_parsing = [
                'company_name' => $company->company_name,
            ];

            $subject = Helper::parse_email($email_template->subject,$for_parsing);

            $for_parsing = [
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'link' => $link
            ];

            $body = Helper::parse_email($email_template->body,$for_parsing);


            Mail::send('emails.default_email', compact('body'), function($message)
            use($subject,$user)
            {
                $message->subject($subject);
                $message->to($user->email, $user->first_name.' '.$user->last_name);
            });

            $credentials = ['email'=>$user['email'],'password'=>$post['password']];

            Auth::attempt($credentials, false);

            return Redirect::to('dashboard')->withInput();

        }

    }

    function activate_user($hash){

        if(!$hash) return response('No Hash Found',404);

        $user = User::where('activate_hash',$hash)->where('status','unverified')->first();

        if(!$user) return response('No User Found',404);

        $user->status='verified';
        $user->save();

        $company = $user->company_user->first();

        //Account Activated Email
        $url = url('/login/');
        $link = '<a href="'.$url.'" >'.$url.'</a>';

        $subject = 'Your Account Is Now Active - '.$company['company_name'];


        $email_template = EmailTemplates::where('slug','user_activated')->first();

        $for_parsing = [
            'company_name' => $company->company_name,
        ];

        $subject = Helper::parse_email($email_template->subject,$for_parsing);

        $for_parsing = [
            'link' => $link
        ];

        $body = Helper::parse_email($email_template->body,$for_parsing);

        Mail::send('emails.default_email', compact('body'), function($message)
        use($subject,$user)
        {
            $message->subject($subject);
            $message->to($user->email, $user->first_name.' '.$user->last_name);
        });


        return Redirect::to('login')->withInput()->with('success', 'Successfully Activated User!');

    }

    function upload_company_pic(Request $request){

        $post = Input::all();

        if ($post) {
            if($request->file('Filedata') AND $request->file('Filedata')->isValid()){

                $company = Company::find($post['company_id']);

                $imageName = 'Company-Logo-'.$company->id . '-' .time() . '.'.
                    $request->file('Filedata')->getClientOriginalExtension();

                $request->file('Filedata')->move(
                    base_path() . '/public/uploads/', $imageName
                );

                $img = Image::make(base_path() . '/public/uploads/'.$imageName);

                // prevent possible upsizing
                $img->resize(null, 150, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });

                $img->save(base_path() . '/public/uploads/'.$imageName);

                //https://zenintra-staging.s3.amazonaws.com/test.jpg
                $zen_url = config('app.bucket_base_url');
                $company_special_folder = '/company-'.$company['id'];

                $s3 = Storage::disk('s3');

                $s3->makeDirectory($company_special_folder);

                $filePath =  './uploads/'.$imageName;
                $s3_upload_path = $company_special_folder.'/'.$imageName;

                $image = $s3->put($s3_upload_path, file_get_contents($filePath),'public');

                $bucket = Config::get('app.bucket');

                $s3_file_url = $zen_url.$s3_upload_path;

                echo $s3_file_url;

            }
        }
    }

    function admin_settings(){
        return view('dashboard.admin.settings',compact(''));

    }

    public function process_invite_user(){

        $company = Company::find(session('current_company')->id);

        $roles = $company->roles;

        $teams = $company->teams;

        return view('dashboard.process_invite_user',compact('roles','teams'));

    }


    public function invite_guest(){

        $user = Auth::user();

        $company = Company::find(session('current_company')->id);

        $roles = $company->roles;

        $teams = $company->teams;

        return view('dashboard.invite_guest',compact('roles','teams'));

    }

    function company_switch()
    {

        $post = Input::all();

        if ($post) {

            $company_id = $post['company_id'];

            $user = Auth::user();

            //set all to zero
            Company_user::where('user_id', $user->id)
                ->update(['guest_default' => 0]);

            Company_user::where('user_id', $user->id)
                ->where('company_id', $company_id)
                ->update(['guest_default' => 1]);


            Auth::logout();

            Auth::login($user);

        }
    }


    public function getTeamsInCompany($id) {
        $company= Company::where('id','=',$id)->with('teams.users','users')->first();
        //dd($company);
        if($company === null){
            return response("Company does not exist", 400);
        }
        $users= $company->users;

        $current_user_in_company = $users->contains('id', Auth::user()->id);
        //user from same company as requested
        if(!$current_user_in_company){
            return response("User not in this company", 400);
        }
        
        return $company->teams->toJson();
    }

}