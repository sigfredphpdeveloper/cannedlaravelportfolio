<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Medias;
use App\Facades\Helpers;
use Image;

use App\Classes\Helper;
use AWS;
use Storage;

class MediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    public function load_images()
    {
        $user = Auth::user();
        $media = Medias::where('user_id', $user->id)->get();

        $output = '';
        $type = '';
        $img_array = array("jpeg", "jpg", "JPG", "gif", "GIF", "png", "PNG");

        // $output .= '<table class="table table-bordered" width="100%">';
        foreach ($media->chunk(3) as $chunk) {
            $output .= '<div class="row">';
            foreach( $chunk as $file ){
                $output .= '<div class="col-sm-4" style="overflow: hidden;">';
                    // $output .= '<center>';

                        if ( in_array($file->type, $img_array) ) 
                        {
                            $output .= '<img src="'.$this->get_thumbnail_source($file->source).'" border=0>';
                            $type = 'image';
                        }
                        elseif (($file->type == "mp3") || ($file->type == "wma") || ($file->type == "wav") ) 
                        {
                            $output .= '<i class="fa fa-file-audio-o fa-fw " style="font-size:101px;margin: 21px 0;color: #5D95C5;"></i>';
                            $type = 'audio';
                        }
                        else
                        {
                            $output .= '<i class="fa fa-file-text-o fa-fw " style="font-size:101px;margin: 21px 0;color: #f4645f;"></i>';
                            $type = 'file';
                        }

                    // $output .= '</center>';
                    $output .= '<span style="font-size:11px">'.$file->name.'</span>';
                    $output .= '<br />';
                    $output .= '<a href="javascript:void(0);" onclick="inserisci(this);" class="insert-image" data-image="'.$file->source.'" data-type="'.$type.'" data-extension="'.$file->type.'" data-name="'.$file->name.'"><span class="glyphicon glyphicon-download"></span></a>';
                    $output .= '<br />';
                $output .= '</div>';
            }
            $output .= '</div>';
        }
        // $output .= '</table>';

        return $output;
    }

    public function get_thumbnail_source( $source )
    {
        $source = str_replace('https://', '', $source);
        $file = explode('/', $source);

        $new_source = '';
        for ($i=0; $i < count($file); $i++) { 
            if( $i == count($file)-1 )
            {
                $new_source .= 'thumbnail-'.$file[$i];
            }
            else
            {
                $new_source .= $file[$i].'/';
            }
        }
        return 'https://'.$new_source;
    }

    public function upload_media(Request $request)
    {
        $user = Auth::user();
        $media = new Medias;

        $media->user_id = $user->id;

        $file = $this->save_files( $request->file('nomefile') );
        $media->name = $file['name'];
        $media->source = $file['source'];
        $media->type = $file['type'];

        $media->save();
    }

    private function save_files( $input ){

        $company = session('current_company');
        $file = array();
        $filename = time().$input->getClientOriginalName();
        $input->move(
            base_path() . '/public/uploads/', $filename
        );

        $zen_url = config('app.bucket_base_url');
        $company_special_folder = '/company-'. $company->id;

        $s3 = Storage::disk('s3');
        $filePath =  base_path() . '/public/uploads/'.$filename;
        $s3_upload_path = $company_special_folder.'/'.$filename;
        $image = $s3->put($s3_upload_path, file_get_contents($filePath),'public');
        $s3_file_url = $zen_url.$s3_upload_path;

        $file = array(
            'name' => $filename,
            'source' => $s3_file_url,
            'type' => $input->getClientOriginalExtension(),
        );

        $image_type = array( 'jpeg', 'jpg', 'JPG', 'gif', 'GIF', 'png', 'PNG');
        if ( in_array( $input->getClientOriginalExtension() , $image_type ) ) 
        {
            $img = Image::make($filePath);
            $img->crop(140, 140);
            $thumb_name = 'thumbnail-'.$filename;
            $img->save( base_path() . '/public/uploads/'.$thumb_name);

            $s3 = Storage::disk('s3');
            $thumb_filePath =  base_path() . '/public/uploads/'.$thumb_name;
            $s3_upload_path = $company_special_folder.'/'.$thumb_name;
            $image = $s3->put($s3_upload_path, file_get_contents($thumb_filePath),'public');

            unlink( base_path() ."/public/uploads/". $thumb_name);
        }
        unlink( base_path().'/public/uploads/'.$filename );
        return $file;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
