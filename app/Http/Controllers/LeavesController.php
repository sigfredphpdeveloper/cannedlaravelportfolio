<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Company;
use App\Company_user;
use App\Roles;
use App\User;
use App\Teams;
use App\User_teams;
use App\Users_leaves;
use App\Permissions;
use App\Leave_types;
use App\LeaveTypesRoles;
use App\UserLeaveAllowance;
use App\LeaveNotes;
use App\CompanyWorkWeek;

use Auth;
use Input;
use Redirect;

use DB;

use Illuminate\Mail\Mailer;
use Mail;
use App\Classes\Helper;

use App\EmailTemplates;

use Excel;


class LeavesController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function apply_leave()
    {

        $user = Auth::user();

        $company = Company::find(session('current_company')->id);

        if($company->leave_days == NULL){

            $this->create_default_leave_days($company);

        }

        $work_weeks = '';

        if($user->work_week_id != NULL  AND $user->work_week_id != 0){

            $ww = $user->work_week;

            if($ww){

                if($ww->monday == 0){
                    $work_weeks .= ' Monday,';
                }
                if($ww->tuesday == 0){
                    $work_weeks .= ' Tuesday,';
                }
                if($ww->wednesday == 0){
                    $work_weeks .= ' Wednesday,';
                }
                if($ww->thursday == 0){
                    $work_weeks .= ' Thursday,';
                }
                if($ww->friday == 0){
                    $work_weeks .= ' Friday,';
                }
                if($ww->saturday == 0){
                    $work_weeks .= ' Saturday,';
                }
                if($ww->sunday == 0){
                    $work_weeks .= ' Sunday,';
                }

            }

        }


        $leave_types_set = $user->leave_types_set();

        return view('dashboard.leaves.apply_leave',compact('user','leave_types_set','company','work_weeks'));
    }


    function create_default_leave_days($company){

        $days = [];

        $weekdays = Helper::week_days();

        foreach($weekdays as $day){

            if($day == 'sunday' || $day == 'saturday'){
                $days[$day] = 0;
            }else{
                $days[$day] = 1;
            }

        }

        $company->leave_days = json_encode($days);
        $company->save();

        return $days;
    }

    public function apply_leave_process()
    {

        $post = Input::all();

        if ($post) {

            $from_admin = FALSE;

            if(!empty($post['admin'])){
                $from_admin = TRUE;
            }

            if($from_admin){
                $user = User::find($post['user_id']);
            }else{
                $user = Auth::user();
            }

            $company = Company::find(session('current_company')->id);

            $leave_type_id = $post['leave_types_id'];


            $check_leave_type_allowance = $user->leave_allowances()->where('leave_types_id','=',$leave_type_id)->first();

            if(!$check_leave_type_allowance){ //no user leave allowance record yet

               //create one
               $leave_type = Leave_types::find($leave_type_id);

               if($leave_type){

                   $check_leave_type_allowance = $user->leave_allowances()->create([
                    'allowance'=>$leave_type->default_allowance,
                    'remaining'=>$leave_type->default_allowance,
                    'leave_types_id'=>$leave_type->id
                   ]);
               }

            }

            $leave_type = Leave_types::find($leave_type_id);

            if(!empty($post['leave_application_type']) ){

                  $no_of_days_taken = 0;

                  switch($post['leave_application_type']) {

                      case 'single';

                          if ($post['date_type'] == 'All Day') {
                              $no_of_days_taken = 1;
                          } else {
                              $no_of_days_taken = 0.5;
                          }

                          break;

                      case 'multiple';

                          $work_week = $user->work_week;

                          $leave_days_count = $this->getWorkingDays2($post['from'],$post['to'],$company,$work_week);

                            $minus = 0;

                            if($post['date_type_from']!='All Day'){
                                $minus = $minus + 0.5;
                            }

                            if($post['date_type_to']!='All Day'){
                                $minus = $minus + 0.5;
                            }

                            $no_of_days_taken = $leave_days_count - $minus;



                      break;
                  }


                $passed = FALSE;

                //it will pass if there are still remaining
                if( ($check_leave_type_allowance->remaining  - $no_of_days_taken) >= 0 ){

                    $passed = TRUE;

                }elseif( ($check_leave_type_allowance->remaining - $no_of_days_taken) < 0 AND $leave_type->advance_leave == 1){ //no more credits but the leave type is good

                    $passed = TRUE;

                }

                if($passed == TRUE){

                    switch($post['leave_application_type']){

                        case 'single';

                            if($post['date_type'] == 'All Day'){
                                $credits = 1;
                            }else{
                                $credits = 0.5;
                            }

                            $leave = $user->leaves()->create([
                                'leave_types_id' => $leave_type_id,
                                'date' => date('Y-m-d',strtotime($post['date'])),
                                'date_type' => $post['date_type'],
                                'details' => $post['details'],
                                'status' =>'pending',
                                'credits'=> $credits,
                                'leave_application_type'=>'single'
                            ]);

                            $this->send_email_apply_leave($leave);


                            if($from_admin){
                                return Redirect::to('admin_pending_leaves')->withInput()->with('success', 'Successfully Applied!');
                            }else{
                                return Redirect::to('apply-leave')->withInput()->with('success', 'Successfully Applied!');
                            }


                        break;

                        case 'multiple';

                            //loop daily

                            $work_week = $user->work_week;

                            $leave_days_count = $this->getWorkingDays2($post['from'],$post['to'],$company,$work_week);

                            $minus = 0;

                            if($post['date_type_from']!='All Day'){
                                $minus = $minus + 0.5;
                            }

                            if($post['date_type_to']!='All Day'){
                                $minus = $minus + 0.5;
                            }

                            $credits = $leave_days_count - $minus;

                            if(!empty($credits) ){

                                $leave = $user->leaves()->create([
                                    'leave_types_id' => $leave_type_id,
                                    'date' => date('Y-m-d',strtotime($post['from'])),
                                    'date_type' => $post['date_type'],
                                    'date_from'=>date('Y-m-d',strtotime($post['from'])),
                                    'date_from_type'=>$post['date_type_from'],
                                    'date_to'=>date('Y-m-d',strtotime($post['to'])),
                                    'date_to_type'=> $post['date_type_to'],
                                    'credits'=> $credits,
                                    'details' => $post['details'],
                                    'status' =>'pending',
                                    'leave_application_type'=>'multiple'
                                ]);

                                $this->send_email_apply_leave($leave);

                                if($from_admin){
                                    return Redirect::to('admin_pending_leaves')->withInput()->with('success', 'Successfully Applied!');
                                }else{
                                    return Redirect::to('apply-leave')->withInput()->with('success', 'Successfully Applied!');
                                }



                            }

                        break;

                    }
                }else{//end else PASSED

                    return Redirect::to('apply-leave')->withInput()->with('error', 'No more leave credits left for ('.$leave_type->type.')');

                }


            }


        }

        return view('dashboard.leaves.apply_leave',compact('user','leave_types'));
    }


    function getWorkingDays2($from,$to,$company,$work_week){

        $leaves = $this->createDateRangeArray($from,$to);
        $allowed = [];

        $company_leave_days['monday'] = $work_week->monday;
        $company_leave_days['tuesday'] = $work_week->tuesday;
        $company_leave_days['wednesday'] = $work_week->wednesday;
        $company_leave_days['thursday'] = $work_week->thursday;
        $company_leave_days['friday'] = $work_week->friday;
        $company_leave_days['saturday'] = $work_week->saturday;
        $company_leave_days['sunday'] = $work_week->sunday;

        #foreach(json_decode($company->leave_days) as $k=>$ld){
        foreach($company_leave_days as $k=>$ld){

            if($ld == 1)
            $allowed[] = date('N',strtotime($k));

        }

        $leave_counts = 0;

        foreach($leaves as $k=>$val){

            $week_day = date('N',strtotime($val));

            if(in_array($week_day,$allowed)){
                $leave_counts++;
            }

        }

        return $leave_counts;

    }


    function send_email_apply_leave($leave){

        $user = User::find($leave->user_id);

        $masters = $user->default_company()->administrator_users();

        $company = $user->default_company();

        $company_name = $company->company_name;

        foreach($masters as $master) {

            $name = $user->first_name . ' ' . $user->last_name;

            $approver = $master['first_name']. ' ' . $master['last_name'];

            $leave_details = '';

            $leave_type = Leave_types::find($leave->leave_types_id);

            $leave_details = $this->get_leave_details($leave, $leave_type);

            $email_template = EmailTemplates::where('slug', 'leave_notification_apply')->first();

            $for_parsing = [
                'name' => $name
            ];

            $subject = Helper::parse_email($email_template->subject, $for_parsing);

            $for_parsing = [
                'approver' => $approver,
                'company_name' => $company_name,
                'name' => $name,
                'leave_details' => $leave_details
            ];

            $body = Helper::parse_email($email_template->body, $for_parsing);

            Mail::send('emails.default_email', compact('body'), function ($message)
            use ($subject, $master, $approver) {
                $message->subject($subject);
                $message->to($master['email'], $approver);
            });
        }

    }


    function getWorkingDays($startDate,$endDate,$holidays=NULL){
        // do strtotime calculations just once
        $endDate = strtotime($endDate);
        $startDate = strtotime($startDate);


        //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
        //We add one to inlude both dates in the interval.
        $days = ($endDate - $startDate) / 86400 + 1;

        $no_full_weeks = floor($days / 7);
        $no_remaining_days = fmod($days, 7);

        //It will return 1 if it's Monday,.. ,7 for Sunday
        $the_first_day_of_week = date("N", $startDate);
        $the_last_day_of_week = date("N", $endDate);

        //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
        //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
        if ($the_first_day_of_week <= $the_last_day_of_week) {
            if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
            if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
        }
        else {
            // (edit by Tokes to fix an edge case where the start day was a Sunday
            // and the end day was NOT a Saturday)

            // the day of the week for start is later than the day of the week for end
            if ($the_first_day_of_week == 7) {
                // if the start date is a Sunday, then we definitely subtract 1 day
                $no_remaining_days--;

                if ($the_last_day_of_week == 6) {
                    // if the end date is a Saturday, then we subtract another day
                    $no_remaining_days--;
                }
            }
            else {
                // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
                // so we skip an entire weekend and subtract 2 days
                $no_remaining_days -= 2;
            }
        }

        $workingDays = $no_full_weeks * 5;
        if ($no_remaining_days > 0 )
        {
            $workingDays += $no_remaining_days;
        }

        if($holidays){
            //We subtract the holidays
            foreach($holidays as $holiday){
                $time_stamp=strtotime($holiday);
                //If the holiday doesn't fall in weekend
                if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
                    $workingDays--;
            }
        }

        return $workingDays;
    }


    function createDateRangeArray($strDateFrom,$strDateTo)
    {
        // takes two dates formatted as YYYY-MM-DD and creates an
        // inclusive array of the dates between the from and to dates.

        // could test validity of dates here but I'm already doing
        // that in the main script

        $aryRange=array();

        $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
        $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

        if ($iDateTo>=$iDateFrom)
        {
            array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
            while ($iDateFrom<$iDateTo)
            {
                $iDateFrom+=86400; // add 24 hours
                array_push($aryRange,date('Y-m-d',$iDateFrom));
            }
        }
        return $aryRange;
    }


    public function settings()
    {
        $user = Auth::user();

        $company = Company::find(session('current_company')->id);

        $roles = $company->roles()->with('role_permissions')->get();

       # $permissions = Permissions::whereIn('key',['manage_leaves','approve_leaves'])->get()->toArray();
        $permissions = Permissions::whereIn('key',['manage_leaves','approve_leaves','view_calendar'])->get()->toArray();

        $permissions = Helper::super_unique($permissions,'feature');

        $leave_types = $company->leave_types;

        $leave_days = json_decode($company->leave_days,TRUE);

        $compay_work_week = $company->company_work_week;

        return view('dashboard.leaves.leaves_settings', compact('company_roles','permissions','roles','leave_types' ,'leave_days','compay_work_week') );
    }





    public function settings_save(){

        $user = Auth::user();
        $company = session('current_company');
        $post = Input::all();

        if ($post) {

            $save_company = Company::find( $company->id );
            $save_company->leaves = (array_key_exists('leaves',$post) AND $post['leaves'] == '1')?1:0;
            $save_company->leaves_halfday = (array_key_exists('leaves_halfday',$post) AND $post['leaves_halfday'] == '1')?1:0;
            $save_company->save();


            return Redirect::to('leaves-setting')->withInput()->with('success', 'Successfully Updated!');
        }

    }

    public function leavedays_setting_save(){

        $user = Auth::user();
        $company = session('current_company');
        $post = Input::all();
        if ($post) {

            $days = [];

            $weekdays = Helper::week_days();

            foreach($weekdays as $day){

                $days[$day] = $post[$day];

            }

            $company->leave_days = json_encode($days);
            $company->save();


            return Redirect::to('leaves-setting')->withInput()->with('success', 'Successfully Updated!');
        }

    }




    function leaves_history(){

        $user = Auth::user();

        $search = (isset($input['search']))?$input['search']:'';

        $user_leaves = $user->leaves()->select('users_leaves.*','leave_types.type')
            ->join('leave_types', 'users_leaves.leave_types_id','=','leave_types.id')
            ;

        if(!empty($_GET['status'])){
            $user_leaves = $user_leaves->where('users_leaves.status','=',$_GET['status']);
        }

        if(!empty($_GET['type'])){
            $user_leaves = $user_leaves->where('users_leaves.leave_types_id','=',$_GET['type']);
        }

        $user_leaves = $user_leaves->search($search)->get();

        $user_leaves = paginateCollection($user_leaves, config('app.page_count'));

        $user_leaves->appends(['search' => $search])->render();


        return view('dashboard.leaves.leaves_history',compact('user','user_leaves'));
    }

    function leaves_account(){

        $user = Auth::user();

        $user_roles = $user->user_roles->pluck('id')->toArray();

        $company = Company::find( session('current_company')->id );

        $leave_types = $company->leave_types();

        $leave_types = $leave_types->join('leave_types_roles', 'leave_types_roles.leave_type_id','=','leave_types.id')
            ->whereIn('role_id',$user_roles)->get();

//dd($leave_types);
        $leave_types_collections = [];

        foreach($leave_types as $item){
            $user_leave_allowance = $user->leave_allowances->where('leave_types_id',$item->leave_type_id)->first();

            if($user_leave_allowance){
                $leave_types_collections[] = [
                    'id'=>$item['leave_type_id'],
                    'type'=>$item['type'],
                    'type_id'=>$item['leave_type_id'],
                    'allowance'=>$user_leave_allowance->allowance,
                    'used'=>$this->get_leave_type_user_used($user,$item['leave_type_id']),
                    'planned'=>$this->get_leave_type_user_planned($user,$item['leave_type_id']),
                    'balance'=>$this->get_leave_type_user_balance($user,$item['leave_type_id']),
                ];
            }
        }


        $user_notes = $user->leave_notes;

        return view('dashboard.leaves.my_account',compact('user','leave_types','leave_types_collections','user_notes'));
    }

    function leaves_my_account(){

        $user = Auth::user();

        $user_roles = $user->user_roles->pluck('id')->toArray();

        $company = Company::find( session('current_company')->id );

        $leave_types = $company->leave_types();

        $leave_types = $leave_types->join('leave_types_roles', 'leave_types_roles.leave_type_id','=','leave_types.id')
            ->whereIn('role_id',$user_roles)->groupBy('type')->get();

        $leave_types_collections = [];

        foreach($leave_types as $item){
            $user_leave_allowance = $user->leave_allowances->where('leave_types_id',$item->leave_type_id)->first();

            if($user_leave_allowance){
                $leave_types_collections[] = [
                    'id'=>$item['leave_type_id'],
                    'type'=>$item['type'],
                    'type_id'=>$item['leave_type_id'],
                    'allowance'=>$user_leave_allowance->allowance,
                    'used'=>$this->get_leave_type_user_used($user,$item['leave_type_id']),
                    'planned'=>$this->get_leave_type_user_planned($user,$item['leave_type_id']),
                    'balance'=>$this->get_leave_type_user_balance($user,$item['leave_type_id']),
                ];
            }
        }


        $roles = $company->roles;

        #$user_ids = [$user->id];

        $user_ids = $company->users_userid();

        $user_leaves = Users_leaves::getManyUserLeaves($user_ids,NULL,NULL);

        if(!empty($_GET['role'])){

            $user_roles = $user->user_roles;

            $user_roles_array = [];

            foreach($user_roles as $role)
                $user_roles_array[] = $role->id;

            $user_leaves = $user_leaves->join('user_roles', 'user_roles.user_id','=','users_leaves.user_id')
            ->where('user_roles.role_id','=',$_GET['role']);

        }

        if(!empty($_GET['type'])){
            $user_leaves = $user_leaves->where('users_leaves.leave_types_id','=',$_GET['type']);
        }

        if(!empty($_GET['user_id'])){
            $user_leaves = $user_leaves->where('users_leaves.user_id','=',$_GET['user_id']);
        }


        $user_leaves = $user_leaves->get();

        $events = [];

        foreach($user_leaves as $item){

           switch($item['leave_application_type']){

               case 'single':


                   $start = date('Y-m-d',strtotime($item->date)).'T0000';
                   $end = date('Y-m-d',strtotime($item->date)).'T2359';

                   switch($item->date_type){

                       case 'All Day':
                           $start = date('Y-m-d',strtotime($item->date)).'T0000';
                           $end = date('Y-m-d',strtotime($item->date)).'T2359';
                           break;

                       case 'Morning':
                           $start = date('Y-m-d',strtotime($item->date)).'T0600';
                           $end = date('Y-m-d',strtotime($item->date)).'T1200';
                           break;

                       case 'Afternoon':
                           $start = date('Y-m-d',strtotime($item->date)).'T1200';
                           $end = date('Y-m-d',strtotime($item->date)).'T1800';
                           break;

                   }

                   $color = $item->color_code;
                   $textColor = '#000';

                   if($item->status=='pending'){
                        $color = $this->hex2rgb($item->color_code);
                       $textColor = '#fff';
                   }

                   $events[] = \Calendar::event(
                       $item->first_name .' '. $item->last_name . ' - ' . $item->type . ' ( '.$item->date_type .' )', //event title
                       false, //full day event?
                       $start, //start time (you can also use Carbon instead of DateTime)
                       $end, //end time (you can also use Carbon instead of DateTime)
                       $item->id.'-'.$item->user_id,//optionally, you can specify an event ID
                       array('color'=>$color,'textColor'=>$textColor)
                   );


               break;


               case 'multiple':

                    $dates  =  $this->createDateRangeArray($item->date_from,$item->date_to);

                    if($dates){

                        $numItems = count($dates);
                        $i = 0;

                        foreach($dates as $date){

                            $start = date('Y-m-d',strtotime($date)).'T0000';
                            $end = date('Y-m-d',strtotime($date)).'T2359';

                            $date_type = (++$i === $numItems)?$item->date_to_type:$item->date_from_type;

                            switch($date_type){

                                case 'All Day':
                                    $start = date('Y-m-d',strtotime($date)).'T0000';
                                    $end = date('Y-m-d',strtotime($date)).'T2359';
                                    break;

                                case 'Morning':

                                    $start = date('Y-m-d',strtotime($date)).'T0600';
                                    $end = date('Y-m-d',strtotime($date)).'T1200';

                                    break;

                                case 'Afternoon':

                                    $start = date('Y-m-d',strtotime($date)).'T1200';
                                    $end = date('Y-m-d',strtotime($date)).'T1800';

                                    break;
                            }

                            $color = $item->color_code;
                            $textColor = '#000';

                            if($item->status=='pending'){
                                $color = $this->hex2rgb($item->color_code);
                                $textColor = '#fff';
                            }


                            $events[] = \Calendar::event(
                                $item->first_name .' '. $item->last_name . ' - ' . $item->type . ' ( '.$date_type .' )', //event title
                                false, //full day event?
                                $start, //start time (you can also use Carbon instead of DateTime)
                                $end, //end time (you can also use Carbon instead of DateTime)
                                $item->id.'-'.$item->user_id,//optionally, you can specify an event ID
                                array('color'=>$color,'textColor'=>$textColor)
                            );

                        }

                    }


               break;

           }


        }

        $company = Company::find($company->id);

        $leave_days = json_decode($company->leave_days);

        $offs = '';

        foreach($leave_days as $key=>$off){

            if($off == 0){

                $offs .= Helper::convert_week_number($key).",";

            }

        }

        $offs = rtrim($offs,',');

        $calendar = \Calendar::addEvents($events) //add an array with addEvents
        ->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
                'eventClick' => "function(calEvent, jsEvent, view) {

                    //alert('Event: ' + calEvent.title);
                    console.log(calEvent);

                    eventajaxmodal(calEvent.id);
                }",
                'dayRender' => "function(date,cell){
                   // console.log(date);


                    Date.prototype.getWeek = function() {
                        var onejan = new Date(this.getFullYear(), 0, 1);
                        return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
                    }

                    var weekNumber = (new Date(date)).getDay();

                    var offs = [$offs];


                    function isInArray(value, array) {
                      return array.indexOf(value) > -1;
                    }

                    if(isInArray(weekNumber, offs)){
                        cell.css('backgroundColor', '#EDEDED');
                    }else{
                        cell.css('backgroundColor', '#ffffff');

                    }

                }"

            ])->setOptions([ //set fullcalendar options
            'height' => 350
        ]);

        $leave_types = $company->leave_types;
        $users = $company->users;



        $pending_user_leaves_count = Users_leaves::where('user_id',$user->id)->where('status','pending')->count();
        $upcoming_leave = Users_leaves::where('user_id',$user->id)->where('date','>',date('Y-m-d'))->orderBy('date','asc')->first();

        return view('dashboard.leaves.leaves_my_account',compact('calendar','roles','user','leave_types','leave_types_collections',
            'users','pending_user_leaves_count','upcoming_leave'));
    }


    function manage_leaves(){


        $user = Auth::user();

        $user_roles = $user->user_roles->pluck('id')->toArray();

        $company = Company::find( session('current_company')->id );

        $user_ids = $company->users_userid();

        $company_pending_leaves = Users_leaves::getManyUserLeaves($user_ids,NULL,'pending')->count();

        $company = Company::with('users','users.user_roles','users.leave_allowances','leave_types','users.leave_allowances.leave_type.leave_types_roles')->find( session('current_company')->id );

        foreach($company['users'] as $i=>$user){

                $roles = [];

                foreach($user['user_roles'] as $role){
                    $roles[] = $role['id'];
                }

                foreach($company['leave_types'] as $key=>$leave_type){

                    $company['users'][$i]['allowance-'.$leave_type->id] = $user->leave_allowances_array($leave_type->id,$roles,$user->id);;

                }

        }

        $employees = $company->toArray();

        $user_leaves = NULL;
        $employee = NULL;


            if(!empty($_GET) AND array_key_exists('user_id',$_GET)){

                $employee = User::find($_GET['user_id']);

                $user_leaves = $employee->leaves()->select('users_leaves.*','leave_types.type')
                    ->join('leave_types', 'users_leaves.leave_types_id','=','leave_types.id')
                    ;


                $user_leaves = $user_leaves->get();

                if($employee->work_week == NULL){


                    $company_work_week = DB::table('company_work_week')->where('company_id',$company->id)->first();

                    if(count($company_work_week)>0){
                        $employee->work_week_id = $company_work_week->id;
                        $employee->update();
                    }

                }


            }


        $compay_work_week = $company->company_work_week;

        return view('dashboard.leaves.manage_leaves',compact('user','company_pending_leaves','employees','user_leaves','employee','compay_work_week'));


    }


    function get_leave_type_user_used($user,$leave_types_id){

        $used_leaves = $user->leaves()->where(DB::raw('YEAR(created_at)'),'=',date('Y'))
        ->where('leave_types_id','=',$leave_types_id)
        ->where('status','approved')
        ->sum('credits');

        return $used_leaves;

    }

    function get_leave_type_user_planned($user,$leave_types_id){

        $used_leaves = $user->leaves()->where(DB::raw('YEAR(created_at)'),'=',date('Y'))
            ->where('leave_types_id','=',$leave_types_id)
            ->where('created_at','>=',DB::raw('CURDATE()'))
            ->where('status','pending')
            ->sum('credits');

        return $used_leaves;
    }

    function get_leave_type_user_balance($user,$leave_types_id)
    {

        $user_leave_allowance = $user->leave_allowances()->where('leave_types_id', $leave_types_id)->first();

        $used_leaves = $this->get_leave_type_user_used($user, $leave_types_id);

        if ($user_leave_allowance)
        {
               #return $user_leave_allowance->allowance - (float)$used_leaves;
            return $user_leave_allowance->remaining;
        }else{
            return '-';
        }
    }

    function view_leave_record($id){

        $user = Auth::user();

        $leave = $user->leaves_dataset($user->id,"users_leaves.id = $id");

        return view('dashboard.leaves.view_leave_record',compact('leave'));
    }

    public function approve_leaves()
    {
        $user = Auth::user();

        $company = Company::find( session('current_company')->id );

        $search = (isset($input['search']))?$input['search']:'';

        $user_ids = $company->users_userid();

        $user_leaves = Users_leaves::getManyUserLeaves($user_ids,$search,'pending')->get();

        $user_leaves = paginateCollection($user_leaves, config('app.page_count'));

        $user_leaves->appends(['search' => $search])->render();

        return view('dashboard.leaves.approve_leaves',compact('user','user_leaves'));
    }

    function leaves_calendar(){

        $input = Input::all();

        $user = Auth::user();

        $company = Company::find( session('current_company')->id );

        $roles = $company->roles;
		
        $user_ids = $company->users_userid();

        $user_leaves = Users_leaves::getManyUserLeaves($user_ids,NULL,NULL);

        if(!empty($_GET['role'])){

            $user_roles = $user->user_roles;

            $user_roles_array = [];

            foreach($user_roles as $role)
                $user_roles_array[] = $role->id;

            $user_leaves = $user_leaves->join('user_roles', 'user_roles.user_id','=','users_leaves.user_id')
            ->where('user_roles.role_id','=',$_GET['role']);

        }

        if(!empty($_GET['type'])){
            $user_leaves = $user_leaves->where('users_leaves.leave_types_id','=',$_GET['type']);
        }

        if(!empty($_GET['user_id'])){
            $user_leaves = $user_leaves->where('users_leaves.user_id','=',$_GET['user_id']);
        }


        $user_leaves = $user_leaves->get();


        $events = [];

        foreach($user_leaves as $item){

           switch($item['leave_application_type']){

               case 'single':


                   $start = date('Y-m-d',strtotime($item->date)).'T0000';
                   $end = date('Y-m-d',strtotime($item->date)).'T2359';

                   switch($item->date_type){

                       case 'All Day':
                           $start = date('Y-m-d',strtotime($item->date)).'T0000';
                           $end = date('Y-m-d',strtotime($item->date)).'T2359';
                           break;

                       case 'Morning':
                           $start = date('Y-m-d',strtotime($item->date)).'T0600';
                           $end = date('Y-m-d',strtotime($item->date)).'T1200';
                           break;

                       case 'Afternoon':
                           $start = date('Y-m-d',strtotime($item->date)).'T1200';
                           $end = date('Y-m-d',strtotime($item->date)).'T1800';
                           break;

                   }

                   $color = $item->color_code;
                   $textColor = '#000';

                   if($item->status=='pending'){
                        $color = $this->hex2rgb($item->color_code);
                       $textColor = '#fff';
                   }

                   $events[] = \Calendar::event(
                       $item->first_name .' '. $item->last_name . ' - ' . $item->type . ' ( '.$item->date_type .' )', //event title
                       false, //full day event?
                       $start, //start time (you can also use Carbon instead of DateTime)
                       $end, //end time (you can also use Carbon instead of DateTime)
                       $item->id.'-'.$item->user_id,//optionally, you can specify an event ID
                       array('color'=>$color,'textColor'=>$textColor)
                   );


               break;


               case 'multiple':

                    $dates  =  $this->createDateRangeArray($item->date_from,$item->date_to);

                    if($dates){

                        $numItems = count($dates);
                        $i = 0;

                        foreach($dates as $date){

                            $start = date('Y-m-d',strtotime($date)).'T0000';
                            $end = date('Y-m-d',strtotime($date)).'T2359';

                            $date_type = (++$i === $numItems)?$item->date_to_type:$item->date_from_type;

                            switch($date_type){

                                case 'All Day':
                                    $start = date('Y-m-d',strtotime($date)).'T0000';
                                    $end = date('Y-m-d',strtotime($date)).'T2359';
                                    break;

                                case 'Morning':

                                    $start = date('Y-m-d',strtotime($date)).'T0600';
                                    $end = date('Y-m-d',strtotime($date)).'T1200';

                                    break;

                                case 'Afternoon':

                                    $start = date('Y-m-d',strtotime($date)).'T1200';
                                    $end = date('Y-m-d',strtotime($date)).'T1800';

                                    break;
                            }

                            $color = $item->color_code;
                            $textColor = '#000';

                            if($item->status=='pending'){
                                $color = $this->hex2rgb($item->color_code);
                                $textColor = '#fff';
                            }


                            $events[] = \Calendar::event(
                                $item->first_name .' '. $item->last_name . ' - ' . $item->type . ' ( '.$date_type .' )', //event title
                                false, //full day event?
                                $start, //start time (you can also use Carbon instead of DateTime)
                                $end, //end time (you can also use Carbon instead of DateTime)
                                $item->id.'-'.$item->user_id,//optionally, you can specify an event ID
                                array('color'=>$color,'textColor'=>$textColor)
                            );

                        }

                    }


               break;

           }

//createDateRangeArray



        }

        $company = Company::find($company->id);

        $leave_days = json_decode($company->leave_days);

        $offs = '';
        /*
        foreach($leave_days as $key=>$off){

            if($off == 0){

                $offs .= Helper::convert_week_number($key).",";

            }

        }*/

        $work_week = $user->work_week;

        if($work_week){

            if($work_week->monday == 0){
                $offs .= "1,";
            }


            if($work_week->tuesday == 0){
                $offs .= "2,";
            }


            if($work_week->wednesday == 0){
                $offs .= "3,";
            }


            if($work_week->thursday == 0){
                $offs .= "4,";
            }


            if($work_week->friday == 0){
                $offs .= "5,";
            }


            if($work_week->saturday == 0){
                $offs .= "6,";
            }


            if($work_week->sunday == 0){
                $offs .= "0,";
            }

        }

        $offs = rtrim($offs,',');

        $calendar = \Calendar::addEvents($events) //add an array with addEvents
        ->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
                'eventClick' => "function(calEvent, jsEvent, view) {

                    //alert('Event: ' + calEvent.title);
                    console.log(calEvent);

                    eventajaxmodal(calEvent.id);
                }",
                'dayRender' => "function(date,cell){
                   // console.log(date);


                    Date.prototype.getWeek = function() {
                        var onejan = new Date(this.getFullYear(), 0, 1);
                        return Math.ceil((((this - onejan) / 86400000) + onejan.getDay() + 1) / 7);
                    }

                    var weekNumber = (new Date(date)).getDay();

                    var offs = [$offs];


                    function isInArray(value, array) {
                      return array.indexOf(value) > -1;
                    }

                    if(isInArray(weekNumber, offs)){
                        cell.css('backgroundColor', '#EDEDED');
                    }else{
                        cell.css('backgroundColor', '#ffffff');

                    }

                }"

            ])->setOptions([ //set fullcalendar options
            'height' => 350
        ]);
//dd($events);
        $leave_types = $company->leave_types;
        $users = $company->users;
        return view('dashboard.leaves.calendar',compact('calendar','roles','leave_types','users'));
    }

    function leave_types_edit($id){

        $user = Auth::user();

        $company = Company::find( session('current_company')->id );

        $roles = $company->roles;

        $post = Input::all();

        if ($post)
        {
            $leave_type = Leave_types::find($id);

            $leave_type->type = $post['type'];
            $leave_type->description = $post['description'];
            $leave_type->default_allowance = $post['default_allowance'];
            $leave_type->color_code = (!empty($post['color_code']))?$post['color_code']:'#ffffff';
            $leave_type->start_date_allowance = date('Y-m-d',strtotime($post['start_date_allowance']));
            $leave_type->roll_over = $post['roll_over'];
            $leave_type->advance_leave = $post['advance_leave'];

            $leave_type->save();

            $all_users = $company->users;

            //create leave allowances for all users
            foreach($all_users as $user_record){

                $user_leave_allowance = UserLeaveAllowance::where('user_id',$user_record->id)->where('leave_types_id','=',$leave_type->id)->first();

                if(!$user_leave_allowance){

                    $user_leave_allowance = $user_record->leave_allowances()->create([
                        'user_id'=>$user_record['id'],
                        'allowance'=>$leave_type->default_allowance,
                        'remaining'=>$leave_type->default_allowance,
                        'leave_types_id'=>$leave_type->id
                    ]);

                }
            }

            if(isset($post['roles'])){

                DB::table('leave_types_roles')->where('leave_type_id',$id)->delete();

                $leave_types_roles_collection = [];

                foreach($post['roles'] as $role_id){

                    $leave_types_roles_collection[] = [
                        'leave_type_id'=>$id,
                        'role_id'=>$role_id
                    ];

                }

                LeaveTypesRoles::insert($leave_types_roles_collection);

            }


            return Redirect::to('leave-types-edit/'.$id)->withInput()->with('success', 'Successfully Edited Leave Type!');

        }

        $leave_type = Leave_types::find($id);

        $roles_array = $leave_type->leave_types_roles->pluck('role_id')->all();

        $leave_type['roles'] = $roles_array;

        return view('dashboard.leaves.leave_types_edit',compact('leave_type','roles'));

    }

    function leave_types_add(Request $request){

        $post = Input::all();

        $user = Auth::user();

        $company = Company::find( session('current_company')->id );

        $roles = $company->roles;

        if ($post)
        {
            $leave_type = $company->leave_types()->create([
                'type'=>$post['type'],
                'description' => $post['description'],
                'default_allowance' => $post['default_allowance'],
                'color_code'=>$post['color_code'],
                'start_date_allowance'=>date('Y-m-d',strtotime($post['start_date_allowance'])),
                'roll_over'=>$post['roll_over'],
                'advance_leave'=>$post['advance_leave'],
            ]);

            $all_users = $company->users;

            //create leave allowances for all users
            foreach($all_users as $user_record){

                $user_leave_allowance = UserLeaveAllowance::where('user_id',$user_record->id)->where('leave_types_id','=',$leave_type->id)->first();

                if(!$user_leave_allowance){

                    $user_leave_allowance = $user_record->leave_allowances()->create([
                        'user_id'=>$user_record['id'],
                        'allowance'=>$leave_type->default_allowance,
                        'remaining'=>$leave_type->default_allowance,
                        'leave_types_id'=>$leave_type->id
                    ]);

                }
            }


            if(isset($post['roles'])){



                $leave_types_roles_collection = [];

                foreach($post['roles'] as $role_id){

                    $leave_types_roles_collection[] = [
                        'leave_type_id'=>$leave_type->id,
                        'role_id'=>$role_id
                    ];

                }

                LeaveTypesRoles::insert($leave_types_roles_collection);


            }

            return Redirect::to('leaves-setting')->withInput()->with('success', 'Successfully Added Leave Type!');
        }

        return view('dashboard.leaves.leave_types_add',compact('company','roles'));

    }

    function update_leave_status(){

        $post = Input::all();

        $user = Auth::user();

        if ($post)
        {
            if(!empty($post['id'])){

                $leave = Users_leaves::find($post['id']);

                $leave->approved_date = date('Y-m-d');
                $leave->approved_by = $user->id;
                $leave->status = $post['status'];
                $leave->save();

                $leave = Users_leaves::find($post['id']);

                //deduct to leaves allowance now
                if($post['status'] == 'approved'){

                    //deduct leave now
                    $credits = $leave->credits;

                    $leave_type_id = $leave->leave_types_id;
                    $user_id = $leave->user_id;

                    $check_leave_type_allowance = UserLeaveAllowance::where('user_id',$user_id)->where('leave_types_id','=',$leave_type_id)->first();

                    $remaining = $check_leave_type_allowance->remaining - $credits;

                    $check_leave_type_allowance->remaining = $remaining;
                    $check_leave_type_allowance->save();

                    $this->send_email_change_status($leave,$remaining);

                }

                echo 'success';
            }

        }

    }

    function send_email_change_status($leave,$remaining){

        $user = User::find($leave->user_id);

        $company = $user->default_company();

        $company_name = $company->company_name;

        $applicant = $user->first_name .' ' . $user->last_name;

        $leave_details = '';

        $leave_type = Leave_types::find($leave->leave_types_id);

        $leave_details = $this->get_leave_details($leave,$leave_type);

        $email_template = EmailTemplates::where('slug','leave_notification_status')->first();

        $for_parsing = [
            'status' => $leave->status
        ];

        $subject = Helper::parse_email($email_template->subject,$for_parsing);

        $for_parsing = [
            'applicant' => $applicant,
            'company_name' => $company_name,
            'leave_details' => $leave_details
        ];

        $body = Helper::parse_email($email_template->body,$for_parsing);

        Mail::send('emails.default_email', compact('body'), function($message)
        use($subject,$user,$applicant)
        {
            $message->subject($subject);
            $message->to($user->email, $applicant);
        });

    }

    function get_leave_details($leave,$leave_type){

        $leave_details = '';

        if($leave->leave_application_type == 'single'){

            $leave_details .= '<h3>Status : '. ucwords($leave->status).'</h3>';
            $leave_details .= '<p>Date : '. date('m/d/Y',strtotime($leave->date)).' ('.$leave->date_type.')</p>';
            $leave_details .= '<p>Leave Type : '. $leave_type->type .'</p>';
            $leave_details .= '<p>Details : '. $leave->details.'</p>';

        }else if ($leave->leave_application_type == 'multiple') {

            $leave_details .= '<h3>Status : ' . ucwords($leave->status) . '</h3>';
            $leave_details .= '<p>Leave Type : '. $leave_type->type .'</p>';
            $leave_details .= '<p>Date From: ' . date('m/d/Y', strtotime($leave->date_from)) . ' (' . $leave->date_from_type . ')</p>';
            $leave_details .= '<p>Date To: ' . date('m/d/Y', strtotime($leave->date_to)) . ' (' . $leave->date_to_type . ')</p>';
            $leave_details .= '<p>Details : ' . $leave->details . '</p>';

        }

        return $leave_details;

    }

    function employees_leaves_summary(){

        $input = Input::all();

        $user = Auth::user();

        $company = Company::with('users','users.user_roles','users.leave_allowances','leave_types','users.leave_allowances.leave_type.leave_types_roles')->find( session('current_company')->id );

        foreach($company['users'] as $i=>$user){

                $roles = [];

                foreach($user['user_roles'] as $role){
                    $roles[] = $role['id'];
                }

                foreach($company['leave_types'] as $key=>$leave_type){

                    $company['users'][$i]['allowance-'.$leave_type->id] = $user->leave_allowances_array($leave_type->id,$roles,$user->id);;

                }

        }
        $employees = $company;

        $compay_work_week = $company->company_work_week;

        return view('dashboard.leaves.employees_leaves_summary',compact('employees','compay_work_week'));

    }


    function leave_summary_edit($id){

        $input = Input::all();

        $employee = User::find($id);

        $company = Company::find( session('current_company')->id );

        $leave_types = $company->leave_types;

        $employee = $employee->toArray();


        if($employee){

            foreach($leave_types as $leave_type){

                $user_leave_allowance = UserLeaveAllowance::where('user_id','=',$employee['id'])->where('leave_types_id','=',$leave_type->id)->first();

                $user = User::find($employee['id']);
                $roles = $user->user_roles->pluck('id')->toArray();

                $check = $user->leave_allowances_array($leave_type->id,$roles,$employee['id']);

                foreach($check as $item){
                    $employee['leave_allowance'][] =
                        [
                            'type'=>$item->type,
                            'allowance_id'=>$item->allowance_id,
                            'allowance'=>$item->allowance,
                            'remaining'=>$item->remaining
                        ];
                }

            }

        }

        return view('dashboard.leaves.leave_summary_edit',compact('leave_types','employee'));

    }



    function leave_summary_edit_save(){


        $input = Input::all();

        if(!empty($input['start_date'])){

            foreach($input['start_date'] as $key=> $item){

                $user = User::find($key);

                $user->start_date = date('Y-m-d',strtotime($item));

                $user->save();

            }


        }
        if(!empty($input['leave_allowance'])){

           foreach($input['leave_allowance'] as $key => $item){

               $user_leave_allowance = UserLeaveAllowance::find($key);

               if($user_leave_allowance){

                   $remaining = $user_leave_allowance->remaining;

                   $user = User::find($user_leave_allowance->user_id);

                   $used_leaves = $this->get_leave_type_user_used($user,$user_leave_allowance->leave_types_id);

                   $remaining =  $item - (float)$used_leaves;

                   $allowance = $item;

                   if($allowance < $remaining){
                        $user_leave_allowance->remaining = $item;
                   }else{
                       $user_leave_allowance->remaining = $remaining;
                   }

                   $user_leave_allowance->allowance = $item;

                   $user_leave_allowance->save();


               }

           }


        }

        if(!empty($input['leave_note'])){

            $from_user = Auth::user();

            $user = User::find($input['employee_id']);

            LeaveNotes::create([
                'user_id' => $user->id,
                'from_user_id' =>$from_user->id,
                'date' => date('Y-m-d'),
                'note' => $input['leave_note']
            ]);

        }


        return Redirect::back()->withInput()->with('success', 'Successfully Updated Allowance!');




    }

    function cancel_leave_record(){

         $post = Input::all();

          $leave = Users_leaves::find($post['id']);

          if($leave->status =='approved'){

              $user_leave_allowance = UserLeaveAllowance::where('leave_types_id',$leave->leave_types_id)->where('user_id',$leave->user_id)->first();

              $remaining = (float)$user_leave_allowance->remaining + (float)$leave->credits;

              $user_leave_allowance->remaining = $remaining;

              $user_leave_allowance->save();


          }

          $leave->delete();

            echo 'success';

    }

    function hex2rgb($hex) {
        $hex = str_replace("#", "", $hex);

        if(strlen($hex) == 3) {
            $r = hexdec(substr($hex,0,1).substr($hex,0,1));
            $g = hexdec(substr($hex,1,1).substr($hex,1,1));
            $b = hexdec(substr($hex,2,1).substr($hex,2,1));
        } else {
            $r = hexdec(substr($hex,0,2));
            $g = hexdec(substr($hex,2,2));
            $b = hexdec(substr($hex,4,2));
        }
        $rgb = array($r*0.4, $g*0.4, $b*0.4);
        //return implode(",", $rgb); // returns the rgb values separated by commas
        return $this->rgb2hex($rgb); // returns an array with the rgb values
    }

    function rgb2hex($rgb) {
        $hex = "#";
        $hex .= str_pad(dechex($rgb[0]), 2, "0", STR_PAD_LEFT);
        $hex .= str_pad(dechex($rgb[1]), 2, "0", STR_PAD_LEFT);
        $hex .= str_pad(dechex($rgb[2]), 2, "0", STR_PAD_LEFT);

        return $hex; // returns the hex value including the number sign (#)
    }

    function leave_summary_edit_bulk(){

        $input = Input::all();

        if(empty($input['user_ids'])) {
            exit('error');
        }

        $user_ids = $input['user_ids'];

        $user_names = '';
        foreach($user_ids as $user_id){
            $user = User::find($user_id);

            $user_names .= $user->first_name.' '.$user->last_name.',';

        }

        $user_names = rtrim($user_names,',');

        $company = Company::find( session('current_company')->id );

        $leave_types = $company->leave_types;

        return view('dashboard.leaves.leave_summary_edit_bulk',compact('leave_types','user_ids','user_names'));

    }

    function leave_summary_edit_bulk_save(){

        $input = Input::all();


        if(empty($input['user_ids'])) {
            exit('error');
        }

        $user_ids = $input['user_ids'];
        $leave_types = $input['leave_types'];

        foreach($user_ids as $user_id){ //loop users

            $user = User::find($user_id);
            $roles = $user->user_roles->pluck('id')->toArray();

            foreach($leave_types as $type_id => $value){ //loop leave_types

                $check = $user->leave_allowances_array($type_id,$roles,$user_id);

                if($check){

                    foreach($check as $k=>$allowance){

                        $user_leave_allowance = UserLeaveAllowance::find($allowance->allowance_id);

                        if($user_leave_allowance){

                            $remaining = $user_leave_allowance->remaining;

                            $user = User::find($user_leave_allowance->user_id);

                            $used_leaves = $this->get_leave_type_user_used($user,$user_leave_allowance->leave_types_id);

                            $remaining =  $value - (float)$used_leaves;

                            $allowance = $value;

                            if($allowance < $remaining){
                                $user_leave_allowance->remaining = $value;
                            }else{
                                $user_leave_allowance->remaining = $remaining;
                            }

                            $user_leave_allowance->allowance = $value;

                            $user_leave_allowance->save();

                        }

                    }



                }

            }


        }

        return Redirect::to('leaves-summary')->withInput()->with('success', 'Successfully Updated!');



    }


    public function all_leaves()
    {

        $company = Company::find(session('current_company')->id);

        $user_ids = $company->users_userid();

        $leaves = Users_leaves::with('user','Leave_types')->whereIn('user_id',$user_ids);

        $leaves = $leaves->get();

        $title = trans("leaves.Approved Leaves");

        return view('dashboard.leaves.leaves_table', compact('leaves', 'title'));

    }

    function export_leaves_data(){



        Excel::create('Zenintra Leaves Data Export '.date('m/d/Y'), function ($excel) {

            $excel->sheet('companies', function ($sheet) {


                $company = Company::find(session('current_company')->id);

                $user_ids = $company->users_userid();

                $leaves = Users_leaves::with('user','Leave_types')->whereIn('user_id',$user_ids)->get();

                $for_export = [];

                $approver = '';

                foreach($leaves as $leave){

                    $user = User::find($leave->user_id);
                    $leave_type = Leave_types::find($leave->leave_types_id);

                    if($leave->approver){
                        $approver = $leave->approver->first_name .' '.$leave->approver->last_name;
                    }else{
                        $approver = '';
                    }

                    $for_export[] = [
                        'user' => $user->first_name .' '. $user->last_name,
                        'leave_type' => $leave_type->type,
                        'date'=>$leave->date,
                        'date_type'=>$leave->all_day,
                        'details'=>$leave->details,
                        'status'=>$leave->status,
                        'date_from'=>$leave->date_from,
                        'date_from_type'=>$leave->date_from_type,
                        'date_to'=>$leave->date_to,
                        'date_to_type'=>$leave->date_to_type,
                        'credits'=>$leave->credits,
                        'leave_application_type'=>$leave->leave_application_type,
                        'approved_date'=>$leave->approved_date,
                        'approved_by'=>$approver,
                    ];

                }

                $sheet->fromArray($for_export, null, 'A1', false, TRUE);
            });

        })->download('csv');

    }

    function user_leave_notes($id){

        $input = Input::all();

        $employee = User::find($id);

        $user_notes = $employee->leave_notes;

        return view('dashboard.leaves.user_leave_notes',compact('user_notes','employee'));

    }

    function add_leave_note($user_id){

        $user_find = User::find($user_id);

        return view('dashboard.leaves.add_leave_note',compact('user_find'));
    }


    function add_leave_note_save(){

        $input = Input::all();

        $user = User::find($input['user_id']);

        $from_user = Auth::user();

        LeaveNotes::create([
            'user_id' => $user->id,
            'from_user_id' =>$from_user->id,
            'date' => date('Y-m-d'),
            'note' => $input['leave_note']
        ]);

        return Redirect::back()->withInput()->with('success', 'Successfully Added Leave Note!');

    }

    public function company_work_week_edit()
    {

        if(!empty($_GET['id'])){

            $id = $_GET['id'];

            $work_week = CompanyWorkWeek::find($id);

            echo view('dashboard.leaves.company_work_week_edit', compact('work_week') );

        }

    }

    function save_edit_company_work_week(){

        $input = Input::all();

        $company_work_week = CompanyWorkWeek::find($input['id']);

        $company_work_week->title = $input['title'];
        $company_work_week->monday = $input['monday'];
        $company_work_week->tuesday = $input['tuesday'];
        $company_work_week->wednesday = $input['wednesday'];
        $company_work_week->thursday = $input['thursday'];
        $company_work_week->friday = $input['friday'];
        $company_work_week->saturday = $input['saturday'];
        $company_work_week->sunday = $input['sunday'];
        $company_work_week->update();

        return Redirect::to('leaves-setting')->withInput()->with('success', 'Successfully Updated Company Work Week!');


    }

    function save_add_company_work_week(){

        $input = Input::all();

        $company = Company::find(session('current_company')->id);

        $company_work_week = CompanyWorkWeek::create([
            'title'=>$input['title'],
            'company_id'=>$company->id,
            'monday'=>$input['monday'],
            'tuesday'=>$input['tuesday'],
            'wednesday'=>$input['wednesday'],
            'thursday'=>$input['thursday'],
            'friday'=>$input['friday'],
            'saturday'=>$input['saturday'],
            'sunday'=>$input['sunday']
        ]);

        return Redirect::to('leaves-setting')->withInput()->with('success', 'Successfully Added Company Work Week!');
    }

    function delete_company_work_week($work_week_id){

        $input = Input::all();

        $company = Company::find(session('current_company')->id);

        DB::table('company_work_week')->where('id',$work_week_id)->delete();

        $next_work_week = null;

        foreach($company->company_work_week as $cw){

            $next_work_week = $cw->id;

        }

        if($next_work_week){

             DB::table('users')
            ->where('work_week_id', $work_week_id)
            ->update(['work_week_id' => $next_work_week]);

        }else{

             DB::table('users')
            ->where('work_week_id', $work_week_id)
            ->update(['work_week_id' => null]);


        }

        return Redirect::to('leaves-setting')->withInput()->with('success', 'Successfully Deleted Company Work Week!');

    }




    function update_user_work_week(){

        $input = Input::all();

        $user = User::find($input['user_id']);
        $user->work_week_id = $input['work_week_id'];
        $user->update();

        return Redirect::back()->withInput()->with('success', 'Successfully Updated User Work Week!');


    }




}
