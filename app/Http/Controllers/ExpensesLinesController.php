<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\Expenses_fields;
use App\Expenses_lines;
use App\Expenses;
use App\Company;
use App\Company_user;
use App\Expenses_type;

use App\Http\Controllers\Controller;

use Intervention\Image\Facades\Image;
use Redirect;
use DB;

use App\Classes\Helper;
use AWS;
use Storage;

class ExpensesLinesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create( $expense_id )
    {
        // $expense_fields = Expenses_fields::where( 'expense_type_id', $expense_type_id )->get();
        // $fields = $this->form_field_builder($expense_fields);
        // return view('dashboard.expenses.modal.add-expense-lines', [ 'expense_id' => $expense_id, 'fields' => $fields ] );

        $user = Auth::user();
        $company = session('current_company');
        $expenses_types = Expenses_type::where('company_id', $company->id)->get();
        return view('dashboard.expenses.modal.add-expense-lines', [ 'expense_id' => $expense_id, 'expenses_types' => $expenses_types ]);
    }

    public function get_fields( $expense_type_id, $id = 0 )
    {
        $expense_fields = Expenses_fields::where( 'expense_type_id', $expense_type_id )->get();
        $expense_field_file = Expenses_fields::where( ['expense_type_id' => $expense_type_id, 'label' => 'file'] )->first();

        $file_required = 0;
        if( $expense_field_file->required == 1 ){
            $file_required = 1;
        }

        $values = array();
        $expense_line = Expenses_lines::find($id);
        if( count($expense_line) != 0)
        {
            $values = unserialize( $expense_line->value );
        }
        
        return view('dashboard.expenses.expenses-fields', [ 'expense_fields' => $expense_fields, 'values' => $values, 'file_required' => $file_required ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $company = session('current_company');
        $expense_line = new Expenses_lines;
        $expense_line->expense_id = $request->input('expense-id');
        $expense_line->expense_type_id = $request->input('expenses_types');
        $expense_line->title = $request->input('title');
        $expense_line->amount = $request->input('amount');
        $values = $request->all();

        foreach ($values as $key => $value) {
            if ($request->hasFile($key)) 
            {
                if ( $request->file($key)->isValid() ) {
                    $filename = time().$request->file($key)->getClientOriginalName();
                    $request->file($key)->move(
                        base_path() . '/public/uploads/', $filename
                    );

                    $zen_url = config('app.bucket_base_url');
                    $company_special_folder = '/company-'. $company->id;

                    $s3 = Storage::disk('s3');
                    $s3->makeDirectory($company_special_folder);
                    $filePath =  base_path() . '/public/uploads/'.$filename;
                    $s3_upload_path = $company_special_folder.'/'.$filename;
                    $image = $s3->put($s3_upload_path, file_get_contents($filePath),'public');
                    $s3_file_url = $zen_url.$s3_upload_path;
                    unlink( base_path().'/public/uploads/'.$filename );
                    $values[$key] = $s3_file_url;

                    if( $key == 'file' )
                        $expense_line->file = $s3_file_url;
                }
            } 
            else
            {
                $values[$key] = $request->input($key);
                if( $key == 'file' )
                        $expense_line->file = $s3_file_url;
            }
        }

        // Exclude _token, expense_type_id, amount
        // and title to be save in expenses_field column value
        unset($values['_token']);
        unset($values['expense-id']);
        unset($values['expenses_types']);
        unset($values['title']);
        // end

        $expense_line->value = serialize( $values );
        $expense_line->save();
        $this->update_total_amount( $request->input('expense-id') );

        return Redirect::back()->with('success','Expense Line Added !');
    }

    private function update_total_amount($id){
        $total_expenses = DB::select('select SUM(amount) as amount FROM expenses_lines where expense_id = ?', [ $id ] );
        $total_amount = 0;
        foreach ($total_expenses as $total_amount) {
             $total_amount = $total_amount->amount;

        }
        $expenses = Expenses::find($id);
        $expenses->total_amount = $total_amount;
        $expenses->update();
        return;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $company = session('current_company');
        $expense_line = Expenses_lines::find($id);
        $expense_fields = Expenses_fields::where( 'expense_type_id', $expense_line->expense_type_id )->get();
        $expenses_types = Expenses_type::where('company_id', $company->id)->get();
        $values = unserialize( $expense_line->value );
        
        return view('dashboard.expenses.modal.edit-expense-lines', [ 'expense_line' => $expense_line, 'expenses_types' => $expenses_types, 'expense_fields' => $expense_fields, 'values' => $values ] );

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();
        $company = session('current_company');
        $expense_line = Expenses_lines::find($id);
        $expense_line->expense_id = $request->input('expense-id');
        $expense_line->expense_type_id = $request->input('expenses_types');
        $expense_line->title = $request->input('title');
        $expense_line->amount = $request->input('amount');
        $values = $request->all();

        foreach ($values as $key => $value) {
            if ($request->hasFile($key)) 
            {
                if ( $request->file($key)->isValid() ) {
                    $filename = time().$request->file($key)->getClientOriginalName();
                    $request->file($key)->move(
                        base_path() . '/public/uploads/', $filename
                    );

                    $zen_url = config('app.bucket_base_url');
                    $company_special_folder = '/company-'. $company->id;

                    $s3 = Storage::disk('s3');
                    $s3->makeDirectory($company_special_folder);
                    $filePath =  './uploads/'.$filename;
                    $s3_upload_path = $company_special_folder.'/'.$filename;
                    $image = $s3->put($s3_upload_path, file_get_contents($filePath),'public');
                    $s3_file_url = $zen_url.$s3_upload_path;
                    unlink( base_path().'/public/uploads/'.$filename );
                    $values[$key] = $s3_file_url;
                }
            }
        }
        // Exclude _token, expense_type_id, amount
        // and title to be save in expenses_field column value
        unset($values['_token']);
        unset($values['expense-id']);
        unset($values['expenses_types']);
        unset($values['title']);
        // unset($values['amount']);
        // end

        $expense_line->value = serialize( $values );
        $expense_line->update();
        $this->update_total_amount( $request->input('expense-id') );

        return Redirect::back()->with('success','Expense Line Updated !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function delete($id, $expense_id)
    {
        $expense_line = Expenses_lines::find($id);
        return view('dashboard.expenses.modal.delete-expense-line', [ 'expense_line' => $expense_line ] );
    }

    public function destroy($id)
    {
        $expense_line = Expenses_lines::find($id);
        $expense_line->delete();
        $expense_id = $expense_line->expense_id;
        $this->update_total_amount( $expense_id );
        return Redirect::back()->with([ 'success' => 'Expense Line Deleted !' ]);
    }
}
