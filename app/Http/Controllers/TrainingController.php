<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use DB;
use App;

use App\Training;
use App\Company;
use App\Pages;
use App\Permissions;
use App\TopicPagesTracker;
use App\Company_user;

class TrainingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $company_session = session('current_company');
        $trainings_internal = Training::where('company_id', $company_session->id)->where('type', 'internal')->get();
        $trainings_external = Training::where('company_id', $company_session->id)->where('type', 'external')->get();
        return view('dashboard.trainings.index', compact('trainings_internal', 'trainings_external'));
    }

    public function training_internal( Request $request, $id)
    {
        $topic = Training::find($id);
        $user = Auth::user();

        if( ! $topic->user_hasAccess( $user ) )
             abort(403, 'Unauthorized access.');

        $pages = Pages::where('topic_id', $id)->orderBy('order', 'asc')->paginate(1);
        

        $page_tracker = TopicPagesTracker::firstOrNew( [ 'user_id' => $user->id, 'topic_id' => $topic->id ]  );
        $page_tracker->user_id = $user->id;
        $page_tracker->topic_id = $topic->id;

        $page = $request->input('page');

        if( empty($page) && empty($page_tracker->last_page_view) )
        {
            $page_tracker->last_page_view = 1; 
        } 
        elseif( empty($page) && !empty($page_tracker->last_page_view) )
        {
            $page_tracker->last_page_view = $page_tracker->last_page_view; 
        }
        elseif( !empty($page) && empty($page_tracker->last_page_view))
        {
            $page_tracker->last_page_view = $page;
        }
        else 
        {
            if( $page >  $page_tracker->last_page_view )
                  $page_tracker->last_page_view = $page;
        }
         
        $page_tracker->save();
        
        return view('dashboard.trainings.training', compact('pages', 'topic'));
        
    }

    public function training_external($company_id, $slug)
    {
        $topic = Training::where('company_id', $company_id)->where('slug', $slug)->first();
        $pages = Pages::where('topic_id', $topic->id)->orderBy('order', 'asc')->paginate(1);
        $lang = 'en';
        return view('page_tutorials_training', compact('pages', 'topic', 'company_id', 'lang'));
    }

    public function manage()
    {
        $company_session = session('current_company');
        $trainings = Training::where('company_id', $company_session->id)->orderBy('order')->get();
        return view('dashboard.trainings.manage', compact('trainings'));
    }

    public function settings()
    {
        $company_session = session('current_company');
        $current_company = Company::find($company_session->id);

        $user = Auth::user();

        $company = Company::find(session('current_company')->id);

        $roles = $company->roles()->with('role_permissions')->get();

        $permissions = Permissions::whereIn('key',['manage_trainings'])->get()->toArray();

        return view('dashboard.trainings.settings', compact('current_company', 'permissions', 'roles'));
    }

    public function settings_save(Request $request)
    {
        $company_session = session('current_company');
        $current_company = Company::find($company_session->id);
        $current_company->training = ( $request->input('trainings') == 1 ) ? $request->input('trainings') : 0;
        $current_company->update();
        return redirect()->back()->with(['success' => 'Successfully Updated !']);
    }

    public function training_permission(Request $request)
    {
        dd($request->input());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company_session = session('current_company');
        $session = Auth::getSession();
        $company_roles = $company_session->roles()->get();
        $company_teams = $company_session->teams()->get();
        return view('dashboard.trainings.create', compact('company_roles', 'company_teams', 'session'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        $user = Auth::user();
        $company = session('current_company');
        $training = new Training;
        $training->company_id = $company->id;
        $training->title = $request->input('title');

        $count_training = Training::where('title', $request->input('title') )
                    ->where('company_id', $company->id)
                    ->get();
        $count = count($count_training);
        if($count == 0)
        {
            $training->slug = $this->string_slug( $request->input('title') );
        }
        else
        {
            $training->slug = $this->string_slug( $request->input('title') ).'-'.($count+1);
        }

        $training->owner = $user->id;
        $training->type = $request->input('type');
        $training->access_everyone =  !empty( $request->input('access_everyone') ) ? $request->input('access_everyone') : 0;
        $training->access_roles = ( !empty( $request->input('role_visible') ) && !empty( $request->input('roles') ) ) ? json_encode($request->input('roles')) : '[]';
        $training->access_teams = ( !empty( $request->input('team_visible') ) && !empty( $request->input('teams') ) ) ? json_encode($request->input('teams')) : '[]';
        $training->save();

        return redirect('/topic/'.$training->id)->with(['success' => 'Training Successfully Added !']);
    }

    private function string_slug( $string )
    {
        //Lower case everything
        $string = strtolower($string);
        //Make alphanumeric (removes all other characters)
        $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
        //Clean up multiple dashes or whitespaces
        $string = preg_replace("/[\s-]+/", " ", $string);
        //Convert whitespaces and underscore to dash
        $string = preg_replace("/[\s_]/", "-", $string);
        return $string;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $topic = Training::find($id);
        $company_session = session('current_company');
        
        if( $topic->company_id != $company_session->id )
            App::abort(403, 'Forbidden');
        else
            return view('dashboard.trainings.show', compact('topic'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company_session = session('current_company');
        $topic = Training::find($id);
        $company_roles = $company_session->roles()->get();
        $company_teams = $company_session->teams()->get();
        return view('dashboard.trainings.edit', compact('topic', 'company_roles', 'company_teams'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        $company = session('current_company');
        $training = Training::find($id);
        $training->title = $request->input('title');
        $training->type = $request->input('type');

        $count_training = Training::where('title', $request->input('title') )
                    ->where('company_id', $company->id)
                    ->get();
        $count = count($count_training);
        if($count == 0)
        {
            $training->slug = $this->string_slug( $request->input('title') );
        }
        else
        {
            $training->slug = $this->string_slug( $request->input('title') ).'-'.($count+1);
        }

        $training->access_everyone =  !empty( $request->input('access_everyone') ) ? $request->input('access_everyone') : 0;;
        $training->access_roles = ( !empty( $request->input('role_visible') ) && !empty( $request->input('roles') ) ) ? json_encode($request->input('roles')) : '[]';
        $training->access_teams = ( !empty( $request->input('team_visible') ) && !empty( $request->input('teams') ) ) ? json_encode($request->input('teams')) : '[]';
        $training->update();

        return redirect()->back()->with(['success' => 'Training Successfully Updated !']);
    }

    public function delete($id)
    {
        $topic = Training::find($id);
        return view('dashboard.trainings.delete', compact('topic'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('topic_pages')->where('topic_id', '=', $id)->delete();
        DB::table('topic_pages_tracker')->where('topic_id', '=', $id)->delete();
        $module = Training::find($id);
        $module->delete();
        return redirect()->back()->with(['success' => 'Training Successfully Deleted !']);
    }

    public function tutorial($company_id)
    {
        $topics = Training::where('type', 'external')->where('company_id', $company_id)->orderBy('order')->get();
        $lang = 'en';
        return view('page_tutorials', compact('topics', 'lang', 'company_id'));
    }

    public function topic_order(Request $request)
    {
        $collection = rtrim( $request->input('collection') , "-");
        $topics = explode("-", $collection);

        $count = 0;
        foreach ($topics as $key => $id) {
            $topic = Training::find($id);
            $topic->order = $count;
            $topic->update();
            $count++;
        }

        return $topics;
    }

    public function statistics( $id )
    {
        $topic = Training::find( $id );
        $company = Company::find(session('current_company')->id);
        return view('dashboard.trainings.statistics', compact('topic', 'company') );
    }
}
