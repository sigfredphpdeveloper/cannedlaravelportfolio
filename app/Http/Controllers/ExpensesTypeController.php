<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Company;
use App\Company_user;
use App\Roles;
use App\User;
use App\Expenses;
use App\Expenses_type;
use App\Expenses_lines;
use App\Expenses_fields;

use Auth;
use Input;
use Redirect;

use Illuminate\Mail\Mailer;
use Mail;

use Intervention\Image\Facades\Image;

use DB;
use AWS;
use Storage;

use Response;
use JavaScript;
use Validator;

class ExpensesTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.expenses.modal.add-expense-type');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $company = session('current_company');
        $validation = Validator::make(Input::all(), array( 'expense-type-title' => 'required' ));

        if( $validation->fails() )
        {
            return Redirect::back()->with([ 'error' => 'Some inputs are invalid !' ]);
        }
        else {
            /* Maximum Enxpenses type 
             * of a Company is upto 10 
             * only
             */
            $count_expenses_type = Expenses_type::where( 'company_id', $company->id )->get();
            if( count( $count_expenses_type ) >=10 )
            {
                return Redirect::back()->with([ 'max-error' => 'You have reach the maximum number of Expenses Type !' ]);
            }
            else
            {
                $expense_type = new Expenses_type;
                $expense_type->company_id = $company->id;
                $expense_type->title = Input::get('expense-type-title');
                $expense_type->save();

                return Redirect::back()->with([ 'success' => 'Expenses Type Added !' ]);
            }
        }
        
        return;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $expenses_type = Expenses_type::find($id);
        return view('dashboard.expenses.modal.edit-expense-type', [ 'expenses_type' => $expenses_type ] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {

        $validation = Validator::make(Input::all(), array( 'expense-type-title' => 'required' ));

        if( $validation->fails() )
        {
            return Redirect::back()->with([ 'error' => 'Some inputs are invalid !' ]);
        }
        else {
            $expense_type = Expenses_type::find($id);
            $expense_type->title = Input::get('expense-type-title');
            $expense_type->save();
            return Redirect::back()->with([ 'success' => 'Expenses Type Updated !' ]);
        }
    }

    public function managefields($id)
    {
         $expenses_type = Expenses_type::find($id);
         $default_fields = $expenses_type->expenses_fields()->where('default', 1)->get();
         $regular_fields = $expenses_type->expenses_fields()->where('default', 0)->get();
         return view('dashboard.expenses.modal.manage-fields', [ 'default_fields' => $default_fields, 'regular_fields' => $regular_fields, 'expenses_type' => $expenses_type ] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id){
        $expense_type = Expenses_type::find($id);
        return view('dashboard.expenses.modal.delete-expense-type', [ 'expense_type' => $expense_type ] );
    }

    public function destroy($id)
    {
        $expense_type = Expenses_type::find($id);
        if( $expense_type )
        {
            $expenses = Expenses_lines::where( 'expense_type_id', $expense_type->id )->get();
            if( count($expenses) != 0 )
            {
                return Redirect::back()->with([ 'error' => 'Can\'t be deleted because it\'s associated already with expenses !' ]);
            }
            else
            {
                $deletedRows = Expenses_fields::where('expense_type_id', $id )->delete();
                $expense_type->delete();
                return Redirect::back()->with([ 'success' => 'Expenses Type Deleted !' ]);
            }
        }
    }
}
