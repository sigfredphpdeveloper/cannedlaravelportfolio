<?php

namespace App\Http\Controllers;

use App\Company;
use App\Company_user as CompanyUser;
use App\User;
use Excel;
use Illuminate\Http\Request;
use JavaScript;
use DB;
use Carbon\Carbon;

class AdminCompanyController extends Controller
{

    /**
     * @var CompanyUser
     */
    protected $company_user;

    /**
     * @var Company
     */
    protected $company;

    /**
     * @var User
     */
    protected $user;

    /**
     * AdminCompanyController constructor.
     *
     * @param Company     $company
     * @param CompanyUser $company_user
     * @param User        $user
     *
     * @author Bertrand Kintanar <bertrand.kintanar@gmail.com>
     */
    public function __construct(Company $company, CompanyUser $company_user, User $user)
    {
        $this->company = $company;
        $this->company_user = $company_user;
        $this->user = $user;
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\View\View
     *
     * @author Luigi Gaviola <luigi.acuna.gaviola@gmail.com>
     * @author Bertrand Kintanar <bertrand.kintanar@gmail.com>
     */
    function manage_companies(Request $request)
    {
        $input = $request->all();

        $search = (isset($input['search'])) ? $input['search'] : '';

        $companies = $this->company
            ->groupBy('company.id')
            ->search($search)->get();

        $companies = $this->prepareDetails($companies);

        $companies = paginateCollection($companies, config('app.page_count'));

        $companies->appends(['search' => $search])->render();

        return view('admin.dashboard.company.company_manage', compact('companies', 'search'));
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     *
     * @author Luigi Gaviola <luigi.acuna.gaviola@gmail.com>
     */
    function admin_company_add(Request $request)
    {
        $this->validate($request, ['url' => 'required|min:3|unique:company,url']);

        $request = $request->all();

        if ($request) {
            $request['file_sharing'] = $request['announcements'] = 1;

            $this->company->create($request);
        }

        return redirect()->to('manage_companies');
    }

    /**
     * @param Request $request
     * @param         $id
     *
     * @return \Illuminate\Http\RedirectResponse
     *
     * @author Luigi Gaviola <luigi.acuna.gaviola@gmail.com>
     */
    public function company_edit(Request $request, $id)
    {
        if (!$id) {
            return response('No ID Received', 404);
        }

        $post = $request->all();

        if ($request->isMethod('post')) {
            $company = $this->company->find($id);

            if (!$company) {
                return response('No Company Found', 404);
            }

            $company->update($post);

            return redirect()->to('manage_companies')->withInput()->with('success', 'Successfully Edited Company!');
        }

        $company = $this->company->find($id);

        return view('admin.dashboard.company.company_edit', compact('company'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     *
     * @author Luigi Gaviola <luigi.acuna.gaviola@gmail.com>
     */
    function company_delete($id)
    {
        if (!$id) {
            return response('ID not found', 404);
        }

        $company = $this->company->find($id);

        if ($company) {
            $company->delete();

            return redirect()->to('manage_companies')->withInput()->with('success', 'Successfully Deleted Company!');
        }

        return redirect()->to('manage_companies')->withInput()->with('error', 'Error Occurred!');
    }


    /**
     * @author Luigi Gaviola <luigi.acuna.gaviola@gmail.com>
     * @author Bertrand Kintanar <bertrand.kintanar@gmail.com>
     */
    function export_companies()
    {
        Excel::create('Zenintra Companies '.date('m/d/Y'), function ($excel) {

            $excel->sheet('companies', function ($sheet) {

                $companies = $this->company
                    ->groupBy('company.id')
                    ->get();

                $companies = $this->prepareDetails($companies);

                $sheet->fromArray($companies, null, 'A1', false, TRUE);
            });
        })->download('csv');
    }

    /**
     * @param $companies
     *
     * @return mixed
     *
     * @author Bertrand Kintanar <bertrand.kintanar@gmail.com>
     */
    public function prepareDetails($companies)
    {
        foreach ($companies as $k => $company) {
            $companies[$k]->no_employees = $company->getNumberOfEmployees();
            $companies[$k]->facilities = $company->getFacilities();
            $companies[$k]->master_email = $company->getMasterEmail();
        }

        return $companies;
    }

    public function reports()
    {
        return view('admin.dashboard.company.reports');
    }

    public function graph_report(Request $request)
    {
        $range = $this->get_from_to($request->input('days'));

        $start = $range['start'];
        $to = $range['to'];


        $stats = DB::table('company');

        if($start !='' AND $to != ''){
            $stats = $stats->whereBetween('created_at', array( $start, $to));
        }

        $stats = $stats
            ->groupBy('date')
            ->orderBy('date', 'ASC')
            ->get([
                DB::raw('Date(created_at) as date'),
                DB::raw('COUNT(*) as value')
            ]);

        return json_encode($stats);
    }

    public function graph_report_cumulative(Request $request)
    {

        $range = $this->get_from_to($request->input('days'));

        $start = $range['start'];
        $to = $range['to'];

        $stats = DB::table('history_users_companies')
            ->select(DB::raw('DATE(date) AS date'),
                DB::raw('total_company AS value'));

        if($start !='' AND $to != ''){
            $stats = $stats->whereBetween('date', array( $start, $to));
        }

        $stats = $stats->orderBy('date', 'ASC')
                ->get();

        return json_encode($stats);
    }

    function get_from_to($days){

        $range = [];
        $start = new Carbon(date("Y-m-d", strtotime("-30 day")));
        $to = Carbon::now();

        if( $days == '6-months-ago' )
        {
            $start = new Carbon(date("Y-m-d", strtotime("-6 months")));
            $to = Carbon::now();
        }
        elseif( $days == '12-months-ago' )
        {
            $start = new Carbon(date("Y-m-d", strtotime("-12 months")));
            $to = Carbon::now();
        }
        elseif( $days == 'all-time' )
        {
            $start = '';
            $to = '';
        }
        else
        {
            $start = new Carbon(date("Y-m-d", strtotime("-$days day")));
            $to = Carbon::now();
        }

        $range['start'] = $start;
        $range['to'] = $to;

        return $range;

    }


    /**
     * @param Request $request
     *
     * @return \Illuminate\View\View
     *
     * @author Luigi Gaviola <luigi.acuna.gaviola@gmail.com>
     * @author Bertrand Kintanar <bertrand.kintanar@gmail.com>
     */
    function company_stats(Request $request)
    {
        $input = $request->all();

        $companies = DB::table('company')->get();

        foreach($companies as $i=>$item){

            $company = Company::find($item->id);

            $companies[$i]->number_users = $company->users->count();

            $companies[$i]->last_login = $company->users()->orderBy('last_login','DESC')->first()->last_login;

        }

        return view('admin.dashboard.company.company_stats', compact('companies'));
    }

}
