<?php

namespace App\Http\Controllers;

use App\Company_user;
use App\Tasks;
use App\Tasks_categories;
use App\Tasks_list;
use App\Board;
use App\Company;
use App\User;
use App\Task_files;
use App\Board_teams;
use App\Teams;
use App\Task_check_list;

use FontLib\Table\Type\loca;
use Illuminate\Http\Request;

use Excel;
use Auth;
use DB;
use Illuminate\Support\Facades\Input;
use Response;
use Redirect;
use View;
use Storage;
use Config;


use App\Http\Requests;
use App\Http\Controllers\Controller;

class TaskController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index($board_id=0)
    {
        $post = Input::all();

        $company = session('current_company');

        $check_company = Board::where('id',$board_id)->where('company_id',$company->id)->count();

        if(empty($board_id) || $check_company <= 0){
            return Redirect::to('/boards');
        }

        $tasks = Tasks::select('*','tasks.id AS task_id', 'tasks.start_date AS start_date', 'tasks.due_date AS due_date')
                ->where('tasks.board_id',$board_id)
                ->where('tasks.status','!=','archived')
                ->where('tasks.status','!=','deleted');

        if(!empty($post['column_name']) && !empty($post['column_value'])){
            switch($post['column_name']){
                case'task_title':
                    $tasks = $tasks->where('tasks.task_title','like','%'.$post['column_value'].'%');
                    break;
                case'assigned_name':
                    $tasks = $tasks->where('users.first_name','like','%'.$post['column_value'].'%')->orWhere('users.last_name', 'like', '%'.$post['column_value'].'%');
                    break;
                case'category':
                    $tasks = $tasks->where('tasks_categories.category_name','like','%'.$post['column_value'].'%');
                    break;
            }
        }

        $tasks = $tasks->orderBy('tasks.id','ASC')->get()->toArray();
//dd($tasks);
        $company_users = Company_user::select('*')
            ->join('users', 'users.id','=','company_user.user_id')
            ->where('company_user.company_id', session('current_company')->id)
            ->get()->toArray();

        $comp_users = array();
        foreach($company_users as $company_user){
            $comp_users[$company_user['user_id']] = $company_user;
        }

        foreach($tasks as $key => $task){
            $tasks[$key]['first_name'] = (!empty($comp_users[$task['assigned_user_id']])?$comp_users[$task['assigned_user_id']]['first_name']:'---');
            $tasks[$key]['last_name'] = (!empty($comp_users[$task['assigned_user_id']])?$comp_users[$task['assigned_user_id']]['last_name']:'---');
        }



        $lists = Tasks_list::select('*')
                ->where('company_id',$company->id)
                ->where('board_id', $board_id)
                ->orderBy('position', 'ASC')
                ->get()->toArray();

        $list_array = array();
        foreach($lists as $list){
            $list_array [] = $list['id'];
        }

        // for old record that has not match column
        $tasks = $this->assign_task_list($tasks, $list_array);

        $boards = Board::select('*')
                ->where('company_id',$company->id)
                ->get()->toArray();

        $teams = Teams::where('company_id',session('current_company')->id)->get()->toArray();

        $page = 'task_view';
        $url = url('list-view/'.$board_id);
        return view('dashboard.task.task_view', compact('tasks', 'lists', 'board_id', 'boards', 'page', 'list_array','teams','url'));
    }

    public function assign_task_list($tasks, $list_array){
        foreach($tasks as $k => $task){
            if(!empty($list_array) && !in_array($task['list_id'], $list_array)){
                $tasks[$k]['list_id'] = $list_array[0];
                $change = Tasks::where('id',$task['task_id'])->update(['list_id'=>$list_array[0]]);
            }
        }
        return $tasks;
    }

    public function setting()
    {
        $user = Auth::user()->toArray();
        $company = session('current_company');

        $task_lists = Tasks_list::where('company_id',$company->id)->orderBy('position', 'ASC')->get()->toArray();

        $task_cats = Tasks_categories::where('company_id',$company->id)->get()->toArray();
        $count = Tasks_list::count();

        return view('dashboard.task.setting',compact('user', 'task_lists', 'task_cats', 'company', 'count'));
    }

    public function user_list()
    {
        $post = Input::all();
//        dd($post);

        $user = Auth::user()->toArray();
        $company = session('current_company');


        $users = Company_user::select('users.id', DB::raw('CONCAT(users.first_name, " ", users.last_name) AS label'), DB::raw('CONCAT(users.first_name, " ", users.last_name) AS value'))
                ->join('users','users.id','=','company_user.user_id')
                ->where('company_user.company_id',$company->id)
                ->where(DB::raw('CONCAT(users.first_name, " ", users.last_name)'), 'LIKE', '%'.$post['q'].'%')
                ->get()->toArray();

        // return as json all company users
        return Response::json($users);
    }

    public function create_task(Request $request,$board_id)
    {
        $post = Input::all();
        $company = session('current_company');

        $task['board_id'] = $board_id;
        $task['list_id'] = (array_key_exists('list_id',$post))?$post['list_id']:0;
        $task['task_title'] = $post['title'];
        $task['task_details'] = $post['details'];
        $task['assigned_user_id'] = $post['assignto_id'];
        $task['category'] = $post['category'];
        $task['due_date'] = $post['due_date_submit'];
        $task['create_date'] = date('Y-m-d H:i:s');
        $task['last_update_date'] = date('Y-m-d H:i:s');
        $task['status'] = 'active';
        $task['start_date'] = $post['start_date_submit'];
        $created_task = Tasks::create($task);

        if(!empty($post['chklst_title'])){
            foreach($post['chklst_title'] as $k => $list){
                $chk_list['task_id'] = $created_task->id;
                $chk_list['check_status'] = (!empty($post['check_list'][$k])?1:0);
                $chk_list['check_list_title'] = $list;
                Task_check_list::create($chk_list);
            }
        }

        if($request->file('file_upload') && !empty($_FILES['file_upload']['name'][0])) {

            foreach($request->file('file_upload') as $file){

                $fileName = 'Task-File-' . $company->id . '-' . time() . '.' . $file->getClientOriginalExtension(); // renaming file
                $zen_url = config('app.bucket_base_url'); // zen file url
                $company_special_folder = '/company-' . $company['id'] . '/task-file'; // company directory

                if(!in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1','::1'))){

                    $s3 = Storage::disk('s3');
                    $s3->makeDirectory($company_special_folder); // create directory if not exist
                    $s3_upload_path = $company_special_folder . '/' . $fileName;
                    $s3->put($s3_upload_path, file_get_contents($file), 'public');
                }else{
                    $zen_url = 'sample.com';
                    $s3_upload_path = $company_special_folder . '/' . $fileName;
                }

                $upload['task_id'] = $created_task->id;
                $upload['file_name'] = $fileName;
                $upload['file_path'] = $zen_url.$s3_upload_path;
                $upload['created_at'] = date('Y-m-d H:i:s');
                Task_files::create($upload);
            }
        }

        if(!empty($post['board_id'])){
            return Redirect::to('projects/day/'.$board_id)->withInput()->with('success', 'Successfully Added Task');
        }else{
            return Redirect::to('tasks/'.$board_id)->withInput()->with('success', 'Successfully Added Task');

        }


    }

    public function create_task_people(Request $request)
    {
        $post = Input::all();
        $company = session('current_company');

        $tasks_lists = Tasks_list::where('company_id',$company->id)
            ->orderBy('position', 'ASC')
            ->first()->toArray();

        $board_id = $post['board_id'];

        $task['board_id'] = $board_id; // temporarily default
        $task['list_id'] = $tasks_lists['id']; // temporarily default
        $task['task_title'] = $post['title'];
        $task['task_details'] = $post['details'];
        $task['assigned_user_id'] = $post['assignto_id'];
        $task['category'] = $post['category'];
        $task['due_date'] = $post['due_date_submit'];
        $task['create_date'] = date('Y-m-d H:i:s');
        $task['last_update_date'] = date('Y-m-d H:i:s');

        $task['start_date'] = $post['start_date_submit'];
        $created_task = Tasks::create($task);

        if($request->file('file_upload') && !empty($_FILES['file_upload']['name'][0])) {

            foreach($request->file('file_upload') as $file){

                $fileName = 'Task-File-' . $company->id . '-' . time() . '.' . $file->getClientOriginalExtension(); // renaming file
                $zen_url = config('app.bucket_base_url'); // zen file url
                $company_special_folder = '/company-' . $company['id'] . '/task-file'; // company directory

                if(!in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1','::1'))){

                    $s3 = Storage::disk('s3');
                    $s3->makeDirectory($company_special_folder); // create directory if not exist
                    $s3_upload_path = $company_special_folder . '/' . $fileName;
                    $s3->put($s3_upload_path, file_get_contents($file), 'public');
                }else{
                    $zen_url = 'sample.com';
                    $s3_upload_path = $company_special_folder . '/' . $fileName;
                }

                $upload['task_id'] = $created_task->id;
                $upload['file_name'] = $fileName;
                $upload['file_path'] = $zen_url.$s3_upload_path;
                $upload['created_at'] = date('Y-m-d H:i:s');
                Task_files::create($upload);
            }
        }

        return Redirect::to('people-view')->withInput()->with('success', 'Successfully delete row');

    }


    /***
     * tasks_list section
     ***/
    public function create_list(Request $request)
    {
        $post = Input::all();

        if(!empty($post['id'])){
            $record = Tasks_list::find($post['id']);
            $record->list_title = $post['list_title'];
            $record->save();

            return Response::json(true);exit;
        }

        $count = Tasks_list::count();

        $list['company_id'] = session('current_company')->id;
        $list['list_title'] = $post['list_title'];
        $list['create_date'] = date('Y-m-d');
        $list['position'] = $count+1;
        Tasks_list::create($list);

        return Response::json(true);

    }

    public function create_category()
    {
        $post = Input::all();

        if(!empty($post['id'])){
            $record = Tasks_categories::find($post['id']);
            $record->category_name = $post['category_name'];
            $record->save();

            return Response::json(true);exit;
        }

        $cat['company_id'] = session('current_company')->id;
        $cat['category_name'] = $post['category_name'];
        Tasks_categories::create($cat);

        return Response::json(true);
    }

    public function setting_delete()
    {
        $post = Input::all();
//        dd($post);

        switch($post['table']){
            case 'list':
                Tasks_list::where('id',$post['id'])->where('company_id',session('current_company')->id)->delete();
                break;
            case 'category':
                Tasks_categories::where('id',$post['id'])->where('company_id',session('current_company')->id)->delete();
                break;
            default:
                return Redirect::to('task-setting')->withInput()->with('error', 'Unknown ID');
                break;
        }

        return Redirect::to('task-setting')->withInput()->with('success', 'Successfully delete row');
    }

    public function save_list_position()
    {
        $post = Input::all();
        $company = session('current_company');

        foreach ($post['id'] as $key => $position_id) {

            $position = Tasks_list::where('company_id', $company->id)->find($position_id);

            $position['position'] = $key + 1;
            $position->save();
        }

        return response()->json(true);
    }
    public function change_task_list()
    {
        $post = Input::all();
        $company = session('current_company');

        $tasks_lists = Tasks_list::where('id',$post['group'])
                    ->where('company_id',$company->id)
                    ->get()->toArray();

        if(empty($tasks_lists)){
                return response()->json(false);
        }

        $task = Tasks::where('board_id',$post['board_id'])
                ->find($post['id']);
        $task->list_id = $post['group'];
        $task->last_update_date = date('Y-m-d');
        $task->save();

        return response()->json(true);
    }

    public function category_list()
    {
        $post = Input::all();

        $user = Auth::user()->toArray();
        $company = session('current_company');

        $category = Tasks_categories::select('id', 'category_name as label', 'category_name as value')
                    ->where('company_id', $company->id)
                    ->where('category_name', 'LIKE', '%'.$post['q'].'%')
                    ->get()->toArray();


        // return as json all company users
        return Response::json($category);
    }

    public function create_board()
    {
        $post = Input::all();

        if(empty($post['columns']) || count($post['columns'])<= 0){
            return Redirect::to('boards')->withInput()->with('error', 'Column is required');
        }

        $user = Auth::user()->toArray();
        $company = session('current_company');

        $board['company_id'] = $company->id;
        $board['owner_id'] = $user['id'];
        $board['created_date'] = date('Y-m-d H:i:s');
        $board['last_update_date'] = '0000-00-00 00:00:00';
        $board['board_title'] = $post['name'];
        $board['visibility'] = $post['visibility'];
        $board = Board::create($board);
        $board_id = $board->id;

        if($board['visibility'] == 'team'){
            foreach($post['teams'] as $v){
                $team[] = new Board_teams(['board_id' => $board->id, 'team_id' => $v]);
            }
            $board->teams()->saveMany($team);
        }

        foreach($post['columns'] as $k => $column){
            $array = array('board_id' => $board_id, 'list_title' => $column, 'company_id'=>$company->id, 'position'=>($k+1));
            Tasks_list::create($array);
        }

        return Redirect::to('tasks/'.$board->id)->withInput()->with('success', 'Successfully Created Board');

    }

    public function update_board()
    {
        $post = Input::all();
//dd($post);
        $company = session('current_company');

        if(empty($post['columns']) || count($post['columns'])<= 0){
            return Redirect::to('boards')->withInput()->with('error', 'Column is required');
        }

        $board = Board::where('id', $post['id'])->where('company_id',$company->id)->first()->toArray();

        if(empty($board)){
            return Response::json('Undefined board ID');
        }

        $board = Board::find($post['id']);
        $board->board_title = $post['name'];
        $board->visibility = $post['visibility'];
        $board->save();

        Board_teams::where('board_id', $post['id'])->delete();
        if($post['visibility'] == 'team'){
            foreach($post['teams'] as $team){
                $board_team['board_id'] = $post['id'];
                $board_team['team_id'] = $team;
                Board_teams::create($board_team);
            }
        }

        // get all columns and check if deleted
        $columns = Tasks_list::where('board_id',$post['id']);
        foreach($post['columns'] as $k => $column){
            $columns = $columns->where('id','!=',$k);
        }
        $columns->delete();


        $numbering = 1;
        foreach($post['columns'] as $k => $column){

            $list = Tasks_list::where('board_id',$post['id'])->where('id',$k);
            $check_existing =$list ->count();

            if($check_existing == 0 ){

                $array = array('board_id' => $post['id'], 'list_title' => $column, 'company_id'=>$company->id, 'position'=>$numbering);
                Tasks_list::create($array);

            }else{

                $list->update(['position' =>$numbering]);

            }

            $numbering++;

        }

        return Redirect::to('tasks'.'/'.$post['id'])->withInput()->with('success', 'Successfully updated Board');
    }

    public function open_board()
    {
        $post = Input::all();

        $board = Board::where('id',$post['id'])->first()->toArray();
        $board_teams = Board_teams::join('teams','teams.id','=','board_teams.team_id')
                ->where('board_id',$post['id'])->get()->toArray();

        foreach($board_teams as $k => $v){
            $board_teams[$k] = $v['team_id'];
        }

        $teams = Teams::where('company_id',session('current_company')->id)->get()->toArray();

        $task_lists = Tasks_list::where('company_id',session('current_company')->id)->where('board_id', $post['id'])->orderBy('position','ASC')->get()->toArray();

        echo view('dashboard.board.update_form',compact('board', 'board_teams', 'teams', 'task_lists'));
    }

    public function get_task_info()
    {
        $post = Input::all();

        $company = session('current_company');

        $task = Tasks::select('*','tasks.id AS task_id','tasks.start_date as start_date', 'tasks.due_date as due_date')
//            ->join('tasks_lists','tasks_lists.id','=','tasks.list_id')
//            ->join('tasks_categories','tasks_categories.id','=','tasks.category')
//            ->join('users','users.id','=','tasks.assigned_user_id')
//            ->where('tasks_lists.company_id',$company->id)
            ->where('tasks.id',$post['id'])
            ->get()->first();

        $task['first_name'] = '';
        $task['last_name'] = '';
        if(!empty($task['assigned_user_id'])){
            $user = User::where('id',$task['assigned_user_id'])->first();
            $task['first_name'] = $user->first_name;
            $task['last_name'] = $user->last_name;
        }

        $task['category_name'] = '';
        if(!empty($task['category'])){
            $category = Tasks_categories::where('id',$task['category'])->first();
            $task['category_name'] = $category->category_name;
        }


        $lists = Tasks_list::select('*')
            ->where('company_id',$company->id)
            ->orderBy('position', 'ASC')
            ->get()->toArray();

        $files = Task_files::where('task_id', $post['id'])->get()->toArray();

        $check_lists = Task_check_list::where('task_id',$post['id'])->get()->toArray();

        $view = View::make('dashboard.task.view_task', compact('task','lists','files','check_lists'));
        $contents = $view->render();

        return Response::json((string)$view);
    }

    public function save_view_task(Request $request)
    {
        $post = Input::all();
        $company = session('current_company');

        $task = Tasks::find($post['id']);

        $task['task_title'] = $post['task_title'];
        $task['task_details'] = $post['task_details'];
        $task['assigned_user_id'] = $post['assignto_id'];
        $task['category'] = $post['category'];
        $task['start_date'] = date('Y-m-d', strtotime($post['start_date_submit'])).' '.date('H:i:s', strtotime($post['start_date_time']));
        $task['due_date'] = date('Y-m-d', strtotime($post['due_date_submit'])).' '.date('H:i:s', strtotime($post['due_date_time']));
        $task['last_update_date'] = date('Y-m-d H:i:s');
        $task->save();


        if(!empty($post['title'])){
            foreach($post['title'] as $k => $list){
                if(is_numeric($k)){
                    Task_check_list::where('task_id',$post['id'])->where('id',$k)
                        ->update([
                                    'check_status'=>(!empty($post['check_list'][$k])?1:0),
                                    'check_list_title'=>$list
                                ]);
                }else{
                    $chk_list['task_id'] = $post['id'];
                    $chk_list['check_status'] = (!empty($post['check_list'][$k])?1:0);
                    $chk_list['check_list_title'] = $list;
                    Task_check_list::create($chk_list);
                }
            }
        }



        if($request->file('file_upload') && !empty($_FILES['file_upload']['name'][0])) {

            foreach($request->file('file_upload') as $file){

                $fileName = 'Task-File-' . $company->id . '-' . time() . '.' . $file->getClientOriginalExtension(); // renaming file
                $zen_url = config('app.bucket_base_url'); // zen file url
                $company_special_folder = '/company-' . $company['id'] . '/task-file'; // company directory

                if(!in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1','::1'))){

                    $s3 = Storage::disk('s3');
                    $s3->makeDirectory($company_special_folder); // create directory if not exist
                    $s3_upload_path = $company_special_folder . '/' . $fileName;
                    $s3->put($s3_upload_path, file_get_contents($file), 'public');
                }else{
                    $zen_url = 'sample.com';
                    $s3_upload_path = $company_special_folder . '/' . $fileName;
                }

                $upload['task_id'] = $post['id'];
                $upload['file_name'] = $fileName;
                $upload['file_path'] = $zen_url.$s3_upload_path;
                $upload['created_at'] = date('Y-m-d H:i:s');
                Task_files::create($upload);
            }
        }

        $page = !empty($post['return_page'])?'list-view':'tasks';

        return Redirect::to($page.'/'.$task->board_id)->withInput()->with('success', 'Successfully updated Task');
//        return Response::json(true);die();
    }

    public function delete_task()
    {
        $post = Input::all();
        $user = Auth::user();
        $is_admin = $user->isAdmin();
        $user = $user->toArray();

        if(!$is_admin){
            return Response::json(false);die();
        }

        $task = Tasks::find($post['id']);
        $task->status = 'deleted';
        $task->save();

        return Response::json(true);die();
    }

    public function list_view($board_id)
    {
        if(empty($board_id)){
            return Redirect::to('/tasks');
        }

        $post = Input::all();

        $user = Auth::user();
        $is_admin = $user->isAdmin();

        $company = session('current_company');

        $tasks = Tasks::select('*','tasks.id AS task_id','tasks.last_update_date as task_last_update_date')
            ->join('tasks_lists','tasks_lists.id','=','tasks.list_id')
            ->where('tasks.board_id',$board_id)
            ->where('tasks.status','!=','archived')
            ->where('tasks.status','!=','deleted');

        if(!empty($post['column_name']) && !empty($post['column_value'])){
            switch($post['column_name']){
                case'task_title':
                    $tasks = $tasks->where('tasks.task_title','like','%'.$post['column_value'].'%');
                    break;
                case'assigned_name':
                    $tasks = $tasks->where('users.first_name','like','%'.$post['column_value'].'%')->orWhere('users.last_name', 'like', '%'.$post['column_value'].'%');
                    break;
                case'category':
                    $tasks = $tasks->where('tasks_categories.category_name','like','%'.$post['column_value'].'%');
                    break;
            }
        }

        $tasks = $tasks->orderBy('tasks.id','ASC')->get()->toArray();

        $company_users = Company_user::select('*')
            ->join('users', 'users.id','=','company_user.user_id')
            ->where('company_user.company_id', session('current_company')->id)
            ->get()->toArray();

        $comp_users = array();
        foreach($company_users as $company_user){
            $comp_users[$company_user['user_id']] = $company_user;
        }

        foreach($tasks as $key => $task){
            $tasks[$key]['first_name'] = (!empty($comp_users[$task['assigned_user_id']])?$comp_users[$task['assigned_user_id']]['first_name']:'---');
            $tasks[$key]['last_name'] = (!empty($comp_users[$task['assigned_user_id']])?$comp_users[$task['assigned_user_id']]['last_name']:'---');

            $tasks[$key]['category_name'] = '';
            if(!empty($task['category'])){
                $category = Tasks_categories::where('id',$task['category'])->first();
                $tasks[$key]['category_name'] = $category->category_name;
            }
        }

        $lists = Tasks_list::select('*')
            ->where('company_id',$company->id)
            ->where('board_id', $board_id)
            ->orderBy('position', 'ASC')
            ->get()->toArray();

        $list_array = array();
        foreach($lists as $list){
            $list_array [] = $list['id'];
        }

        // for old record that has not match column
        $tasks = $this->assign_task_list($tasks, $list_array);

        $lists = Tasks_list::select('*')
            ->where('company_id',$company->id)
            ->orderBy('position', 'ASC')
            ->get()->toArray();

        $boards = Board::select('*')
            ->where('company_id',$company->id)
            ->get()->toArray();

        $teams = Teams::where('company_id',session('current_company')->id)->get()->toArray();

        $page = 'list_view';
        $url = url('list-view/'.$board_id);
        return view('dashboard.task.list_view', compact('tasks', 'lists', 'board_id', 'boards','page', 'teams', 'url', 'is_admin'));
    }

    public function save_pm_module()
    {
        $post = Input::all();
        $company = session('current_company');

        $my_company = Company::find($company->id);

        if (!$post['status'] || $post['status']=='false') {
            $my_company['pm_setting'] = 0;
            $my_company['task_setting'] = 0;
        } else {
            $my_company['pm_setting'] = 1;
        }
        $my_company->save();

        return response()->json(true);
    }
    public function save_task_module()
    {
        $post = Input::all();
        $company = session('current_company');
        $post['status'] = $post['status'] == "false"?false:true;

        $my_company = Company::find($company->id);
        if($post['status']){
            $my_company['pm_setting'] = 1;
            $my_company['task_setting'] = 1;

            $task_cats = Tasks_categories::where('company_id',$company->id)->count();

            if($task_cats == 0){

                for($i = 1; $i < 4 ;$i++){
                    $cat['company_id'] = $company->id;
                    $cat['category_name'] = 'Category '.$i;
                    Tasks_categories::create($cat);
                }

            }

        }else{
            $my_company['task_setting'] = 0;
        }
        $my_company->save();

        return response()->json(true);
    }

    public function update_task(Request $request)
    {
        $post = Input::all();
        $company = session('current_company');

        $task = Tasks::find($post['id']);

        $task['task_title'] = $post['task_title'];
        $task['task_details'] = $post['task_details'];
        $task['assigned_user_id'] = $post['assignto_id'];
        $task['category'] = $post['category'];
        $task['start_date'] = date('Y-m-d', strtotime($post['start_date_submit'])).' '.date('H:i:s', strtotime($post['start_date_time']));
        $task['due_date'] = date('Y-m-d', strtotime($post['due_date_submit'])).' '.date('H:i:s', strtotime($post['due_date_time']));
        $task['last_update_date'] = date('Y-m-d H:i:s');
        $task->save();

        if($request->file('file_upload') && !empty($_FILES['file_upload']['name'][0])) {

            foreach($request->file('file_upload') as $file){

                $fileName = 'Task-File-' . $company->id . '-' . time() . '.' . $file->getClientOriginalExtension(); // renaming file
                $zen_url = config('app.bucket_base_url'); // zen file url
                $company_special_folder = '/company-' . $company['id'] . '/task-file'; // company directory

                if(!in_array($_SERVER['REMOTE_ADDR'], array('127.0.0.1','::1'))){

                    $s3 = Storage::disk('s3');
                    $s3->makeDirectory($company_special_folder); // create directory if not exist
                    $s3_upload_path = $company_special_folder . '/' . $fileName;
                    $s3->put($s3_upload_path, file_get_contents($file), 'public');
                }else{
                    $zen_url = 'sample.com';
                    $s3_upload_path = $company_special_folder . '/' . $fileName;
                }

                $upload['task_id'] = $post['id'];
                $upload['file_name'] = $fileName;
                $upload['file_path'] = $zen_url.$s3_upload_path;
                $upload['created_at'] = date('Y-m-d H:i:s');
                Task_files::create($upload);
            }
        }

        return Response::json(true);die();
    }

    function export_task_data($type='csv'){

        if($type=='xls'){
            $type = 'xls';
        }else{
            $type = 'csv';
        }


//        $tasks = Tasks::select('*','boards.board_title','tasks_categories.category_name','tasks.id AS task_id', 'tasks.start_date AS start_date', 'tasks.due_date AS due_date')
//                ->join('tasks_categories','tasks_categories.id','=','tasks.category')
//                ->join('users','users.id','=','tasks.assigned_user_id')
//                ->join('boards','boards.id','=','tasks.board_id')
//                ->where('boards.company_id',$company->id)
//                ->get()->toArray();

        Excel::create('Zenintra Tasks Data Export '.date('d-M-Y'), function ($excel) {

            $excel->sheet('tasks', function ($sheet) {
                $company = session('current_company');

                $tasks = Tasks::select('tasks.task_title as Title','tasks.task_details as Detail',
                    DB::raw("boards.board_title as 'Board title'"),DB::raw("tasks_lists.list_title as 'List Title'"),
                    DB::raw("tasks.assigned_user_id as 'Assigned Person'"),'tasks.category as Category',
                    DB::raw("tasks.start_date as 'Start Date'"),DB::raw("tasks.due_date as 'Due Date'"))
                    ->join('boards','boards.id','=','tasks.board_id')
                    ->join('tasks_lists','tasks_lists.id','=','tasks.list_id')
                    ->where('boards.company_id',$company->id)
                    ->get()->toArray();

                $company_users = Company_user::select('*')
                    ->join('users', 'users.id','=','company_user.user_id')
                    ->where('company_user.company_id', $company->id)
                    ->get()->toArray();

                $comp_users = array();
                foreach($company_users as $company_user){
                    $comp_users[$company_user['user_id']] = $company_user;
                }

                foreach($tasks as $key => $task) {
                    $tasks[$key]['Assigned Person'] = (!empty($comp_users[$task['Assigned Person']]) ? $comp_users[$task['Assigned Person']]['first_name'] . ' ' . $comp_users[$task['Assigned Person']]['last_name'] : '');

                    if(!empty($tasks[$key]['Category'])){
                        $cat = Tasks_categories::where('id',$tasks[$key]['Category'])->first();
                        $tasks[$key]['Category'] = $cat->category_name;
                    }
                }

                $sheet->fromArray($tasks, null, 'A1', false, TRUE);
            });
        })->download($type);

    }

    function set_archived()
    {
        $post = Input::all();

        $task = Tasks::find($post['id']);
        $task->status = 'archived';
        $task->last_update_date = date('Y-m-d H:i:s');
        $task->save();

        $page = ($post['page'] == 'list_view')?'list-view':'tasks';
        return Redirect::to($page.'/'.$post['board_id'])->withInput()->with('success', 'Successfully set to archived');
    }

    public function delete_board()
    {
        $post = Input::all();

        $board = Board::find($post['id']);
        $board->visibility = 'deleted';
        $board->save();

        $task = Tasks::where('board_id', $post['id'])
                ->update(['status'=>'deleted']);


        return Redirect::to('boards')->withInput()->with('success', 'Successfully deleted board');

    }


}
