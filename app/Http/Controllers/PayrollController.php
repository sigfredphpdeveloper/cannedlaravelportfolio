<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\PayrollAddition;
use App\TaxCategory;
use App\PayrollDeduction;
use App\User;
use App\SGEmployeeData;
use App\CpfFunds;
use App\SGGenerateCpf;

use App\Company;
use App\Sg_cpf;

use DB;
use Auth;
use Illuminate\Support\Facades\App;
use Input;
use Redirect;
use Response;
use App\Payroll;
use App\PayrollItem;
use App\PayrollItemAdditions;
use App\PayrollItemDeductions;
use App\Permissions;
use App\Services\PayrollService;
use Storage;
use App\SGIrasForm;

use App\Classes\Helper;


class PayrollController extends Controller
{

    protected $payrollService;

    public function __construct()
    {
        $company = Company::find(session('current_company')->id);

        if($company->country != 'SG'){
            return Redirect::to('dashboard')->send();
        }

        $this->payrollService = new PayrollService();
    }

    //$company = Company::find(session('current_company')->id);
    public function getSettings()
    {
        $user = Auth::user();

        $company = Company::find(session('current_company')->id);

        $roles = $company->roles()->with('role_permissions')->get();

        $permissions = Permissions::whereIn('key',['manage_payroll'])->get()->toArray();

        $permissions = Helper::super_unique($permissions,'feature');

        return view('dashboard.payroll.settings.settings', compact('permissions','roles') );
    }

    public function postSettings(Request $request)
    {
        $company = session('current_company');
        $company->payroll = $request->input('payroll');
        $company->save();

        return redirect()->back()->with('success', 'Payroll Settings Updated!');
    }

    public function newPayrollAddition()
    {
        $tax_categories = TaxCategory::all();
        return view('dashboard.payroll.settings.create-payroll-addition', compact('tax_categories'));
    }

    public function savePayrollAddition(Request $request)
    {
        $payroll_addition = new PayrollAddition;
        $payroll_addition->company_id = session('current_company')->id;
        $payroll_addition->title = $request->input('title');
        $payroll_addition->cpf_payable = empty($request->input('cpf_payable')) ? 0 : $request->input('cpf_payable');
        $payroll_addition->taxable = empty($request->input('taxable')) ? 0 : $request->input('taxable');
        $payroll_addition->other_allowance = empty($request->input('other_allowance')) ? 0 : $request->input('other_allowance');
        $payroll_addition->category_id = $request->input('tax_category');
        $payroll_addition->save();

        return redirect()->back()->with('success', 'Payroll Addition saved!');
    }

    public function editPayrollAddition($id)
    {
        $payroll_addition = PayrollAddition::find($id);
        $tax_categories = TaxCategory::all();
        return view('dashboard.payroll.settings.update-payroll-addition', compact('payroll_addition', 'tax_categories'));
    }

    public function updatePayrollAddition(Request $request, $id)
    {
        $payroll_addition = PayrollAddition::find($id);
        $payroll_addition->title = $request->input('title');
        $payroll_addition->cpf_payable = empty($request->input('cpf_payable')) ? 0 : $request->input('cpf_payable');
        $payroll_addition->taxable = empty($request->input('taxable')) ? 0 : $request->input('taxable');
        $payroll_addition->other_allowance = empty($request->input('other_allowance')) ? 0 : $request->input('other_allowance');
        $payroll_addition->category_id = $request->input('tax_category');
        $payroll_addition->save();
        return redirect()->back()->with('success', 'Payroll Addition Updated!');
    }

    public function newPayrollDeduction()
    {
        return view('dashboard.payroll.settings.create-payroll-deduction');
    }

    public function savePayrollDeduction(Request $request)
    {
        $payroll_deduction = new PayrollDeduction;
        $payroll_deduction->company_id = session('current_company')->id;
        $payroll_deduction->title = $request->input('title');
        $payroll_deduction->cpf_deductible = empty($request->input('cpf_deductible')) ? 0 : $request->input('cpf_deductible');
        $payroll_deduction->save();

        return redirect()->back()->with('success', 'Payroll Deduction saved!');
    }

    public function editPayrollDeduction($id)
    {
        $payroll_deduction = PayrollDeduction::find($id);
        return view('dashboard.payroll.settings.update-payroll-deduction', compact('payroll_deduction'));
    }

    public function updatePayrollDeduction(Request $request, $id)
    {
        $payroll_deduction = PayrollDeduction::find($id);
        $payroll_deduction->title = $request->input('title');
        $payroll_deduction->cpf_deductible = empty($request->input('cpf_deductible')) ? 0 : $request->input('cpf_deductible');
        $payroll_deduction->save();

        return redirect()->back()->with('success', 'Payroll Deduction Updated!');
    }

    public function process_payroll()
    {

        $company = session('current_company');

        $company = Company::find(session('current_company')->id);

        // add cpf employee if not exist
        $company->has_employee_cpf;

        $employees = User::select('users.*', 'company_user.is_master', 'company_user.is_guest', 'users.id AS user_id')
            ->join('company_user', 'users.id', '=', 'company_user.user_id')
            ->join('company', 'company_user.company_id', '=', 'company.id')
            ->where('company_user.company_id', $company->id)->get();

        $payroll_employees_user_ids = [];

        $payroll_employees = $company->payroll_employees;

        $employees_info = array();
        foreach ($payroll_employees as $pr) {
            $payroll_employees_user_ids[] = $pr->user_id;
            $employees_info[$pr->user_id] = $pr;
        }

        $additions = $company->payroll_additions;
        $deductions = $company->payroll_deductions;
        return view('dashboard.payroll.process.process_payroll', compact('company', 'payroll_employees_user_ids', 'payroll_employees', 'employees', 'additions', 'deductions', 'employees_info'));


    }



    public function process_payroll_save(Request $request)
    {

        $company = session('current_company');
        $user = Auth::user();

        $post = $request->input();

        if($post){

            $duplicated_count = 0;

            //check if there is duplicated at the payroll level
            $duplicated_payrolls = Payroll::where('status','saved')->where('year',$post['year'])->where('month',$post['month'])->where('user_id',$user->id)->where('company_id',$company->id)->get();

            if($duplicated_payrolls){

                foreach($duplicated_payrolls as $PD){

                    $item = PayrollItem::where('payroll_id',$PD->id)->where('month',$post['month'])->where('year',$post['year'])->count();

                    if($item > 0){
                        $duplicated_count++;
                    }

                }

            }

            if($duplicated_count > 0){

               return redirect()->back()->with('error', 'Payroll has already been generated for one or more of the employee selected for this month and year, please remove those employees to proceed');

            }


        }


        $payroll_id = FALSE;

        if(!empty($post['payroll_id']) AND $post['payroll_id'] != NULL){
            $payroll_id = $post['payroll_id'];
        }

        $draft = (!empty($post['draft'])) ? 1 : 0;

        if($payroll_id){ //if updating

            $payroll = Payroll::find($payroll_id);

            $payroll->month = $post['month'];
            $payroll->year = $post['year'];
            $payroll->update();

            //clear item additions / deductions

            foreach($payroll->items as $item){

                $item_additions = $item->item_additions()->delete();
                $item_deductions = $item->item_deductions()->delete();

            }

            //clear payroll items
            $payroll->items()->delete();

        }else{
            $payroll = Payroll::create([
                'user_id' => $user->id,
                'company_id' => $company->id,
               # 'status' => ($draft == 1) ? 'draft' : 'saved',
                 'status' => 'draft' ,
                'month' => $post['month'],
                'year' => $post['year']
            ]);

        }


        $salary_rate = 0;
        $no_frequency = 0;
        $total_paid_salary = 0;

        if (!empty($post['employees']) AND count($post['employees']) > 0) {

            foreach ($post['employees'] as $employee_id) {

                if ($post['salary_frequency'][$employee_id] == 'monthly') {

                    $salary_rate = $post['basic_pay'][$employee_id];
                    $no_frequency = 1;

                } elseif ($post['salary_frequency'][$employee_id] == 'hourly') {

                    $salary_rate = $post['hourly_rate'][$employee_id];
                    $no_frequency = $post['hours'][$employee_id];

                } elseif ($post['salary_frequency'][$employee_id] == 'daily') {

                    $salary_rate = $post['daily_rate'][$employee_id];
                    $no_frequency = $post['days'][$employee_id];

                }

                $cpf_employee['CPF_EMPLOYEE'] = 0;
                $cpf_employee['CPF_EMPLOYER'] = 0;
                $cpf_employee['OW'] = 0;
                $cpf_employee['AW'] = 0;

                if ( $post['cpf_entitlement'][$employee_id] ==  '1' ) {
                    $SGPayrollController = new SGPayrollController;
                    $cpf_employee = $SGPayrollController->cpf_computation($post, $employee_id);
                }

                $payroll_item = PayrollItem::create([
                    'user_id' => $employee_id,
                    'month' => $post['month'],
                    'year' => $post['year'],
                    'salary_rate' => $salary_rate,
                    'salary_frequency' => $post['salary_frequency'][$employee_id],
                    'no_frequency' => $no_frequency,
                    'overtime1_rate' => $post['overtime_1_rate'][$employee_id],
                    'overtime1_hours' => (!empty($post['overtime1'][$employee_id])) ? $post['overtime1'][$employee_id] : 0,
                    'overtime2_rate' => $post['overtime_2_rate'][$employee_id],
                    'overtime2_hours' => (!empty($post['overtime2'][$employee_id])) ? $post['overtime2'][$employee_id] : 0,
                    'total_paid_salary' => $total_paid_salary,
                    'aw' => number_format($cpf_employee['AW'], 2, '.', ''),
                    'ow' => number_format($cpf_employee['OW'], 2, '.', ''),
                    'fund_donation_total' => 0,
                    'payment_date' => '0000-00-00 00:00:00',
                    'cpf_employee' => $cpf_employee['CPF_EMPLOYEE'],
                    'cpf_employer' => $cpf_employee['CPF_EMPLOYER'],
                    'basic_pay' => $post['basic_pay'][$employee_id],
                    'net_pay' => $post['basic_pay'][$employee_id],
                    'overtime_from' => (!empty($post['overtime_from'][$employee_id])) ? date('Y-m-d',strtotime($post['overtime_from'][$employee_id])) : 0,
                    'overtime_to' => (!empty($post['overtime_to'][$employee_id])) ? date('Y-m-d',strtotime($post['overtime_to'][$employee_id])) : 0,
                    'payroll_id' => $payroll->id
                ]);


                // get cpf employees id
                $employees_cpf = PayrollDeduction::where('company_id', $company->id)->where('title','Employee\'s CPF deduction')->first()->toArray();

                // cpf deductions
                PayrollItemDeductions::create([
                    'deduction_id' => $employees_cpf['id'],
                    'item_id' => $payroll_item->id,
                    'user_id' => $employee_id,
                    'amount' => $cpf_employee['CPF_EMPLOYEE'],
                    'title' => 'employee cpf'
                ]);

                $additions = 0;
                $tax_additions = 0;
                $deductions = 0;

                if(!empty($cpf_employee['CPF_EMPLOYEE'])){

                    $deductions = $deductions + $cpf_employee['CPF_EMPLOYEE'];

                }


                //handle additions
                if (!empty($post['addition_title']) AND count($post['addition_title']) > 0) {

                    //loop additions
                    foreach ($post['addition_title'] as $i => $addition_id) {

                        if ($addition_id != '') {

                            //loop all employees applicable
                            $Ekey = 'employees_additions_' . $i;
                            $Akey = 'addition_amount_' . $i;

                            //employees loop
                            foreach ($post[$Ekey] as $i2 => $add_employee_id) {

                                //make sure same employee ID as selected one
                                if ($add_employee_id == $employee_id AND $post[$Akey] != 0) {

                                    $addition_record = PayrollAddition::find($addition_id);

                                    if($addition_record->taxable == 1 AND $addition_record->category_id ==4){
                                        $tax_additions = $tax_additions + $post[$Akey];
                                    }

                                    $additions = $additions + $post[$Akey];

                                    $payroll_item_addition = PayrollItemAdditions::create([
                                        'addition_id' => $addition_id,
                                        'item_id' => $payroll_item->id,
                                        'user_id' => $add_employee_id,
                                        'amount' => $post[$Akey],
                                        'title' => $addition_record->title
                                    ]);
                                }

                            }

                        }
                    }

                }

                //handle deductions
                if (!empty($post['deduction_title']) AND count($post['deduction_title']) > 0) {

                    //loop deductions
                    foreach ($post['deduction_title'] as $i => $deduction_id) {

                        if ($deduction_id != '') {
                            //loop all employees applicable
                            $Ekey = 'employees_deductions_' . $i;
                            $Dkey = 'deduction_amount_' . $i;

                            //employees loop employees_deductions_0[]
                            foreach ($post[$Ekey] as $i2 => $dec_employee_id) {

                                //make sure same employee ID as selected one
                                if ($dec_employee_id == $employee_id AND $post[$Dkey] != 0) {

                                    $deduction_record = PayrollDeduction::find($deduction_id);

                                    $deductions = $deductions + $post[$Dkey];

                                    $payroll_item_deduction = PayrollItemDeductions::create([
                                        'deduction_id' => $deduction_id,
                                        'item_id' => $payroll_item->id,
                                        'user_id' => $dec_employee_id,
                                        'amount' => $post[$Dkey],
                                        'title' => $deduction_record->title
                                    ]);
                                }

                            }
                        }
                    }

                }

                //FUNDS HANDLING

                //IF Employee has CPF Entitlement
                if ( $post['cpf_entitlement'][$employee_id] ==  '1' ) {

                    //GROSS SALARY = OW + AW
                    $total_wage = $cpf_employee['OW'] + $cpf_employee['AW'];

                    //GET EMPLOYEE DATA
                    $emp_data = SGEmployeeData::where('user_id',$employee_id)->first();

                    //CHECK FUNDS SELECTIONS
                    $funds_selection = (json_decode($emp_data->funds_selection) != NULL) ? json_decode($emp_data->funds_selection) : array();
                    $funds_selection = is_array($funds_selection)?$funds_selection:array($funds_selection);

                    if(count($funds_selection) > 0){

                            //IF HAS EMPLOYEE CDAC CHECK on cpf_funds table for the monthly contribution
                            if(in_array('CDAC', $funds_selection)){

                                //get cpf_funds category and get MONTHLY CONTRIBUTION
                                $cpf_fund = CpfFunds::where('min_wage','<',$total_wage)->where('max_wage','>',$total_wage)->where('type','CDAC')->first();

                                if($cpf_fund){

                                    $deductions = $deductions + $cpf_fund->monthly_contribution;

                                    $deduction_item = PayrollDeduction::where('company_id',$emp_data->company_id)->where('title','CDAC')->first();

                                    if($deduction_item){
                                        $payroll_item_deduction = PayrollItemDeductions::create([
                                            'deduction_id' => $deduction_item->id,
                                            'item_id' => $payroll_item->id,
                                            'user_id' => $employee_id,
                                            'amount' => $cpf_fund->monthly_contribution,
                                            'title' => 'CDAC'
                                        ]);
                                    }
                                }

                            }

                             //IF HAS EMPLOYEE MBMF CHECK on cpf_funds table for the monthly contribution
                            if(in_array('MBMF', $funds_selection)){

                                //get cpf_funds category and get MONTHLY CONTRIBUTION
                                $cpf_fund = CpfFunds::where('min_wage','<',$total_wage)->where('max_wage','>',$total_wage)->where('type','MBMF')->first();
                                if($cpf_fund){

                                    $deductions = $deductions + $cpf_fund->monthly_contribution;

                                    $deduction_item = PayrollDeduction::where('company_id',$emp_data->company_id)->where('title','MBMF')->first();


                                    if($deduction_item){
                                        $payroll_item_deduction = PayrollItemDeductions::create([
                                            'deduction_id' => $deduction_item->id,
                                            'item_id' => $payroll_item->id,
                                            'user_id' => $employee_id,
                                            'amount' => $cpf_fund->monthly_contribution,
                                            'title' => 'MBMF'
                                        ]);
                                    }
                                }

                            }

                             //IF HAS EMPLOYEE ECF CHECK on cpf_funds table for the monthly contribution
                            if(in_array('ECF', $funds_selection)){

                                //get cpf_funds category and get MONTHLY CONTRIBUTION
                                $cpf_fund = CpfFunds::where('min_wage','<',$total_wage)->where('max_wage','>',$total_wage)->where('type','ECF')->first();

                                if($cpf_fund){

                                    $deductions = $deductions + $cpf_fund->monthly_contribution;

                                    $deduction_item = PayrollDeduction::where('company_id',$emp_data->company_id)->where('title','ECF')->first();


                                    if($deduction_item){
                                        $payroll_item_deduction = PayrollItemDeductions::create([
                                            'deduction_id' => $deduction_item->id,
                                            'item_id' => $payroll_item->id,
                                            'user_id' => $employee_id,
                                            'amount' => $cpf_fund->monthly_contribution,
                                            'title' => 'ECF'
                                        ]);
                                    }
                                }
                            }

                             //IF HAS EMPLOYEE SINDA CHECK on cpf_funds table for the monthly contribution
                            if(in_array('SINDA', $funds_selection)){

                                //get cpf_funds category and get MONTHLY CONTRIBUTION
                                $cpf_fund = CpfFunds::where('min_wage','<',$total_wage)->where('max_wage','>',$total_wage)->where('type','SINDA')->first();

                                if($cpf_fund){

                                    $deductions = $deductions + $cpf_fund->monthly_contribution;

                                    $deduction_item = PayrollDeduction::where('company_id',$emp_data->company_id)->where('title','SINDA')->first();

                                    if($deduction_item){
                                        $payroll_item_deduction = PayrollItemDeductions::create([
                                            'deduction_id' => $deduction_item->id,
                                            'item_id' => $payroll_item->id,
                                            'user_id' => $employee_id,
                                            'amount' => $cpf_fund->monthly_contribution,
                                            'title' => 'SINDA'
                                        ]);
                                    }
                                }
                            }


                    }

                }

                // END FUND HANDLING

                //calculate total paid salary
                $total_paid_salary = ($salary_rate * $no_frequency) + ($additions - $deductions);

                $total_paid_salary = $total_paid_salary + ($payroll_item->overtime1_rate * $payroll_item->overtime1_hours) + ($payroll_item->overtime2_rate * $payroll_item->overtime2_hours);


                $payroll_item->total_paid_salary = $total_paid_salary;
                $payroll_item->net_pay = $total_paid_salary;
                $payroll_item->taxable_salary = ($salary_rate * $no_frequency) + ($payroll_item->overtime1_rate * $payroll_item->overtime1_hours) + ($payroll_item->overtime2_rate * $payroll_item->overtime2_hours) + $tax_additions;

                $payroll_item->save();

                /*
                 * ASA DEDUCTIONS HANDLING NAKO!
                 *
               'user_id',
                'month',
                'year',
                'salary_rate',
                'salary_frequency',
                'no_frequency',
                'overtime1_rate',
                'overtime1_hours',
                'overtime2_rate',
                'overtme2_hours',
                'total_paid_salary',
                'fund_donation_total',
                'payment_date',
                'cpf_employee',
                'cpf_employer',
                'net_pay',
                'basic_pay'
*/

            }

        }

        $payroll_id = $payroll->id;

        return redirect()->to('review_process_payroll/' . $payroll_id);

    }

    public function load_payroll($payroll_id = NULL)
    {

        $company = session('current_company');

        $company = Company::find(session('current_company')->id);

        $employees = User::select('users.*', 'company_user.is_master', 'company_user.is_guest', 'users.id AS user_id')
            ->join('company_user', 'users.id', '=', 'company_user.user_id')
            ->join('company', 'company_user.company_id', '=', 'company.id')
            ->where('company_user.company_id', $company->id)->get();

        $payroll_employees_user_ids = [];

        $payroll_employees = $company->payroll_employees;

        $employees_info = array();
        foreach ($payroll_employees as $pr) {
            $payroll_employees_user_ids[] = $pr->user_id;
            $employees_info[$pr->user_id] = $pr;

            $payroll_item = PayrollItem::where('user_id',$pr->user_id)->where('payroll_id',$payroll_id)->first();

            $pr->payroll_item = $payroll_item;

        }

        $additions = $company->payroll_additions;
        $deductions = $company->payroll_deductions;

        //load the payroll

        $payroll = Payroll::find($payroll_id);

        $data['payroll'] = $payroll;
        $data['company'] = $company;
        $data['payroll_employees_user_ids'] = $payroll_employees_user_ids;
        $data['payroll_employees'] = $payroll_employees;
        $data['employees'] = $employees;
        $data['additions'] = $additions;
        $data['deductions'] = $deductions;
        $data['employees_info'] = $employees_info;

        $payroll_items_users = [];

        $additionals_selects = [];
        $deductions_selects = [];

        foreach($payroll->items as $item){

            $payroll_items_users[] = $item->user_id;

            $item_additions = $item->item_additions;

            foreach($item_additions as $addition){

                if(!array_key_exists($addition->addition_id,$additionals_selects)){
                    $additionals_selects[$addition->addition_id]['amount'] = $addition->amount;
                    $additionals_selects[$addition->addition_id]['users'][] = $addition->user_id;
                }else{
                    $additionals_selects[$addition->addition_id]['users'][] = $addition->user_id;
                }
            }

             $item_deductions= $item->item_deductions;

            foreach($item_deductions as $deduction){

                if(!array_key_exists($deduction->deduction_id,$deductions_selects)){
                    $deductions_selects[$deduction->deduction_id]['amount'] = $deduction->amount;
                    $deductions_selects[$deduction->deduction_id]['users'][] = $deduction->user_id;
                }else{
                    $deductions_selects[$deduction->deduction_id]['users'][] = $deduction->user_id;
                }
            }


        }

        $additions_array = [];

        if($additions != NULL AND !empty($additions)){

            foreach($additions as $AD){
                $additions_array[] = $AD->id;
            }

        }

        $deductions_array = [];

        if($deductions != NULL AND !empty($deductions)){

            foreach($deductions as $D){
                $deductions_array[] = $D->id;
            }

        }


        $data['payroll_items_users'] = $payroll_items_users;

        //special array for handling item additions and item deductions
        $data['additionals_selects'] = $additionals_selects;
        $data['deductions_selects'] = $deductions_selects;

        //normal additions deductions from table of additions/deductions
        $data['additions_array'] = $additions_array;
        $data['deductions_array'] = $deductions_array;

        return view('dashboard.payroll.process.process_payroll_load', $data);

    }



    public function get_total_paid_salary($post)
    {


        if ($post['salary_frequency'][$employee_id] == 'monthly') {

            $salary_rate = $post['basic_pay'][$employee_id];

        } elseif ($post['salary_frequency'][$employee_id] == 'hourly') {

            $salary_rate = $post['hours'][$employee_id];

        } elseif ($post['salary_frequency'][$employee_id] == 'daily') {

            $salary_rate = $post['days'][$employee_id];

        }
    }

    public function review_process_payroll($id)
    {

        $company = session('current_company');
        $user = Auth::user();

        $payroll = Payroll::find($id);

        if (!$payroll) {
            return redirect()->to('process_payroll');
        }

        $data['payroll'] = $payroll;
        $data['items'] = $payroll->items;

        return view('dashboard.payroll.process.review_payroll', $data);

    }

    public function process_payroll_save_finalize(Request $request)
    {

        $company = session('current_company');
        $user = Auth::user();

        $post = $request->input();

        $payroll = Payroll::find($post['payroll_id']);

        if ($payroll) {

            $payroll->status = 'saved';
            $payroll->payment_date = $post['payment_date']!= ''?date('Y-m-d', strtotime($post['payment_date'])):'0000-00-00';
            $payroll->update();

            foreach($payroll->items as $item){

                $item->payment_date = date('Y-m-d', strtotime($post['payment_date']));
                $item->update();

            }

            return redirect()->to('review_process_payroll/' . $post['payroll_id'])->with('success', 'Payroll saved!');

        }
    }

    public function history()
    {
        $company = session('current_company');
        $payrolls = Payroll::whereCompanyId($company->id)
        ->orderBy('year', 'DESC')
        ->orderBy('month', 'ASC')
        ->get();
        
        return view('dashboard.payroll.history.index', compact('payrolls'));
    }

    public function show($id)
    {
        $company = session('current_company');
        $user = Auth::user();

        $payroll = Payroll::find($id);

        if (!$payroll) {
            return redirect()->to('process_payroll');
        }

        $data['id'] = $id;
        $data['payroll'] = $payroll;
        $data['items'] = $payroll->items;

        return view('dashboard.payroll.history.show-payroll', $data);
    }

    public function showEmployeePayroll($id)
    {
        $employee = PayrollItem::findOrFail($id);
        $payroll = Payroll::findOrFail($employee->payroll_id);
        $company = Company::findOrFail($payroll->company_id);
//dd($employee->other_allowance->where('other_allowance',0));

        return view('dashboard.payroll.history.show-employee-payroll', compact('company', 'payroll', 'employee'));
    }

    public function download($id)
    {

        $employee = PayrollItem::findOrFail($id);
        $payroll = Payroll::findOrFail($employee->payroll_id);
        $company = Company::findOrFail($payroll->company_id);
        $pdf_name = str_replace(' ', '-', $employee->user->employee_data->full_name_1.'-'.$payroll->month.'-'.$payroll->year).'.pdf';

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('dashboard.payroll.history.partials.payslip_download', compact('company', 'payroll', 'employee'));
        return $pdf->download($pdf_name);
    }

    public function bulk_download($id)
    {
        $company = session('current_company');
        $user = Auth::user();

        $payroll = Payroll::find($id);

        if (!$payroll) {
            return redirect()->to('process_payroll');
        }

        $data['items'] = $payroll->items;

        $records = array();

        foreach($data['items'] as $k => $item){

            $records[$k]['employee'] = PayrollItem::findOrFail($item->id);
            $records[$k]['payroll'] = Payroll::findOrFail($records[$k]['employee']->payroll_id);
            $records[$k]['company'] = Company::findOrFail($records[$k]['payroll']->company_id);

        }

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('dashboard.payroll.history.partials.bulk_download', compact('company', 'payroll', 'employee', 'records'));
        return $pdf->download();

    }

    public function myPayslips()
    {
        $user = Auth::user();
        $payslips = PayrollItem::whereUserId($user->id)->get();

        return view('dashboard.payroll.my-payslips.index', compact('payslips'));
    }

    public function showPayslipItem($id)
    {
        $employee = PayrollItem::findOrFail($id);
        $payroll = Payroll::findOrFail($employee->payroll_id);
        $company = Company::findOrFail($payroll->company_id);
        return view('dashboard.payroll.my-payslips.show-payslip', compact('company','payroll','employee'));
    }


    public function generate_cpf()
    {
        $company = session('current_company');
        $user = Auth::user();
        $is_admin = $user->isAdmin();

        $payrolls = Payroll::where('company_id', $company->id)->orderBy('year','DESC')->get();
        $years = array();

        foreach($payrolls as $payroll){
            $years[$payroll->year][] = $payroll->month;
        }


        return view('dashboard.payroll.company.generate_cpf2', compact('years'));
    }

    public function cpf_download_report($report_id) {
        $report = SGGenerateCpf::find($report_id);

        $headers = ['Content-type'=>'text/plain', "charset"=>"utf-8", 'test'=>'YoYo', 'Content-Disposition'=>sprintf('attachment; filename="%s"', $report->file_name),'X-BooYAH'=>'cpf','Content-Length'=>sizeof($report->file)];
        $fileText = $report->file;
        //redirect to history cpf
        return Response::make($fileText, 200, $headers);
    }

    public function generate_cpf_save()
    {
        $post = Input::all();
        $company = session('current_company');

        $header = $this->cpf_header();
        $employees = $this->cpf_employee_contribution($post['year'], $post['month']); // with dash ex. '2018-4'
        $employee_info = $this->line_each_employee($post['year'], $post['month']);
        $fileText = $header;
        $fileText .= "\r\n";
        foreach($employees as $key => $employee){
            if($key != 'first'){
                $fileText .= $employees['first'].$employee;
                $fileText .= "\r\n";
            }

        }

        foreach($employee_info as $key => $employee) {
            if($key !== 'first') {
                $fileText .= $employees['first'].$employee;
                $fileText .= "\r\n";
            }
        }
        $length= substr_count($fileText,"\r\n");
        $fileText .= $this->trailer_record(array_merge($employee_info,$employees),$length);

        $filename = "cpf-". $post['year'] . $post['month'] . ".txt";

        // check if already generated
        $check = SGGenerateCpf::where('company_id',$company->id)->where('month',$post['month'])->where('year',$post['year'])->first();

        if(empty($check)){
            SGGenerateCpf::insert([
                'company_id' => $company->id,
                'month' => $post['month'],
                'year' => $post['year'],
                'file_name' => $filename,
                'file' => utf8_encode($fileText)
            ]);
        }

        $headers = ['Content-type'=>'text/plain', 'test'=>'YoYo', 'Content-Disposition'=>sprintf('attachment; filename="%s"', $filename),'X-BooYAH'=>'cpf','Content-Length'=>sizeof($fileText)];
        //redirect to history cpf
        //return Response::make($fileText, 200, $headers);
        $months = [
            1 => 'January',
            2 => 'February',
            3 => 'March',
            4 => 'April',
            5 => 'May',
            6 => 'June',
            7 => 'July',
            8 => 'August',
            9 => 'September',
            10 => 'October',
            11 => 'November',
            12 => 'December'
        ];
        return redirect()->to('cpf_reports')->with('success',"CPF report for the month of ".$months[$post['month']]." ".$post['year']." has been generated, you can now download it.");
        dd($post, $header, $empoyee);
    }

    public function cpf_reports()
    {
        $company = session('current_company');
        $reports = SGGenerateCpf::where('company_id',$company->id)->orderBy('year','desc')->orderBy('month','desc')->get();
        return view('dashboard.payroll.company.cpf_reports',compact('reports'));
    }

    public function get_cpf_employees()
    {
        $post = Input::all();
        $company = session('current_company');

        $payrolls = Payroll::where('company_id', $company->id)->where('year',$post['year'])->where('month',$post['month'])->first();

        $employees = array();
        foreach($payrolls->items as $user){
            $employees[] = $user->user->employee_data;
        }
        return Response::json($employees);

    }

    public function cpf_header_record()
    {

        return view('dashboard.payroll.company.header_record');
    }

    public function cpf_header()
    {
        $company = session('current_company');

        $header = 'F ';
        $header .= $company->sg_company_details->org_id;

        if(strlen($company->sg_company_details->org_id)< 10){
            $x = strlen($company->sg_company_details->org_id);

            while($x < 10){
                $header .= ' ';
                $x++;
            }

        }

        $header .= 'PTE';
        $header .= '01 01';
        $header .= date('Ymd');
        $header .= date('His');
        $header .= 'FTP.DTL';

        $space = 1;
        while($space < 110){
            $header .= ' ';
            $space++;
        }

        return $header;
    }

    public function cpf_employee_contribution($year, $month)
    {
        $post = Input::all();
        $payroll = date('Ym',strtotime($year.'-'.$month)); // set month with zero ex. 2018-4 to 201804
        $company = session('current_company');

        $data = array();

        $data['first'] = 'F';
        $data['first'] .= '0';
        $data['first'] .= $company->sg_company_details->org_id;

        if(strlen($company->sg_company_details->org_id)< 10){
            $x = strlen($company->sg_company_details->org_id);
            while($x < 10){
                $data['first'] .= ' ';
                $x++;
            }
        }

        $data['first'] .= 'PTE';
        $data['first'] .= '01 01';
        $data['first'] .= $payroll;

        // total cpf contribution
        // get payroll item
        $payroll = Payroll::where('company_id', $company->id)->where('month', $month)->where('year', $year)->first();

        $total_contribution = 0;
        foreach($payroll->items as $item){
            $total_contribution += $item->cpf_employee + $item->cpf_employer;
        }

        $data['01'] = '01';
        $total_contribution = number_format($total_contribution, 2, '.', '');
        $total_contribution = str_replace(".","",$total_contribution);
        $data['01'] .= $this->get_space((int)$total_contribution);

        for($x=1;$x<=7;$x++){
            $data['01'] .= '0';
        }

        $space = '';
        $s = 0;
        while($s<103){
            $space .= ' ';
            $s++;
        }

        $data['01'] .= $space;


        // other contribution

        $mbmf = 0;
        $sinda = 0;
        $cdac = 0;
        $ecf = 0;
        $sdl = 0;
        $contri['mbmf'] = 0;
        $contri['sinda'] = 0;
        $contri['cdac'] = 0;
        $contri['ecf'] = 0;

        foreach($payroll->items as $item){

            if($item->user->employee_data->funds_selection != "null"){ // check if included in FUNDS SELECTIONS

                if(!in_array($item->user_id, $post['employees'])){
                    continue;
                }

                $decode = json_decode($item->user->employee_data->funds_selection);

                if(!empty($decode) && in_array('MBMF', $decode)){ // check if included in FUNDS SELECTIONS
                    $MBMF = $item->item_deductions->where('title','MBMF')->first();
                    if(!empty($MBMF) && ($MBMF->amount)>0){
                        $mbmf += $MBMF->amount;
                        $contri['mbmf']++;
                    }
                }

                if(!empty($decode) && in_array('SINDA', $decode)){ // check if included in FUNDS SELECTIONS
                    $SINDA = $item->item_deductions->where('title','SINDA')->first();
                    if(!empty($SINDA) && ($SINDA->amount)>0){
                        $sinda += $SINDA->amount;
                        $contri['sinda']++;
                    }
                }

                if(!empty($decode) && in_array('CDAC', $decode)){ // check if included in FUNDS SELECTIONS
                    $CDAC = $item->item_deductions->where('title','CDAC')->first();
                    if(!empty($CDAC) && ($CDAC->amount)>0){
                        $cdac += $CDAC->amount;
                        $contri['cdac']++;
                    }
                }

                if(!empty($decode) && in_array('ECF', $decode)){ // check if included in FUNDS SELECTIONS
                    $ECF = $item->item_deductions->where('title','ECF')->first();
                    if(!empty($ECF) && ($ECF->amount)>0){
                        $ecf += $ECF->amount;
                        $contri['ecf']++;
                    }
                }

            }

            // sdl
            $total_amount = 0;
            if($item->employee_data->cpf_entitlement == 1){
                $total_amount += $item->ow + $item->aw;
            }else{
                $total_amount = $item->salary_rate * $item->no_frequency;
            }

            if($total_amount<=800){
                $sdl += 2;
            }elseif($total_amount >= 4500){
                $sdl += 11.25;
            }else{
                $sdl += $total_amount*0.0025;
            }

        }
        $mbmf = number_format($mbmf, 2, '.', '');
        $mbmf = str_replace(".","",$mbmf);
        $sinda = number_format($sinda, 2, '.', '');
        $sinda = str_replace(".","",$sinda);
        $cdac = number_format($cdac, 2, '.', '');
        $cdac = str_replace(".","",$cdac);
        $ecf = number_format($ecf, 2, '.', '');
        $ecf = str_replace(".","",$ecf);
        $data['02'] = '02' . $this->get_space($mbmf) . $this->get_space($contri['mbmf'], 7) . $space;
        $data['03'] = '03' . $this->get_space($sinda) . $this->get_space($contri['sinda'], 7) . $space;
        $data['04'] = '04' . $this->get_space($cdac) . $this->get_space($contri['cdac'], 7) . $space;
        $data['05'] = '05' . $this->get_space($ecf) . $this->get_space($contri['ecf'], 7) . $space;
        $sdl = number_format((int)$sdl, 2, '.', '');
        $sdl = str_replace(".","",$sdl);
        $data['11'] = '11' . $this->get_space($sdl);
        for($x=1;$x<=7;$x++){
            $data['11'] .= '0';
        }

        $data['11'] .= $space;

        return $data;
    }

    public function get_space($string, $space = 12)
    {
        $return = $string;
        $len = strlen($string);

        while($len<$space){
            $return = '0'.$return;
            $len++;
        }

        return $return;
    }

    public function line_each_employee($year, $month)
    {
        $post = Input::all();

        $payroll = date('Ym',strtotime($year.'-'.$month)); // set month with zero ex. 2018-4 to 201804
        $company = session('current_company');

        $data = array();

        $data['first'] = 'F';
        $data['first'] .= '0';
        $data['first'] .= $company->sg_company_details->org_id;

        if(strlen($company->sg_company_details->org_id)< 10){
            $x = strlen($company->sg_company_details->org_id);
            while($x < 10){
                $data['first'] .= ' ';
                $x++;
            }
        }

        $data['first'] .= 'PTE';
        $data['first'] .= '01 01';
        $data['first'] .= $payroll;


        // total cpf contribution
        // get payroll item
        $payroll = Payroll::where('company_id', $company->id)->where('month', $month)->where('year', $year)->first();

        $cc = 1;
        foreach($payroll->items_orderId as $key => $item){

            if(!in_array($item->user_id, $post['employees'])){
                continue;
            }

            $data[$key] = '';

//            if(strlen($key)<2){
//                $data[$key] = '0';
//            }

            $data[$key] .= '01';
            $data[$key] .= $this->put_zero($item->user->employee_data->id_number, 9); // get zero
            $data[$key] .= substr($item->user->employee_data->id_number, 0, 9);

            $total_cpf = number_format($item->cpf_employer + $item->cpf_employee, 2, '.', '');
            $data[$key] .= $this->put_zero(str_replace(".","",$total_cpf), 12); // get zero
            $data[$key] .= str_replace(".","",$total_cpf);

//            $wage = $item->aw + $item->ow;
            $wage = $item->ow;
            $wage = str_replace(".","",number_format($wage, 2, '.', ''));
            $data[$key] .= $this->put_zero($wage, 10); // get zero
            $data[$key] .= $wage;

            $wage2 = $item->aw;
            $wage2 = str_replace(".","",number_format($wage2, 2, '.', ''));
            $data[$key] .= $this->put_zero($wage2, 10); // get zero
            $data[$key] .= $wage2;

            $data[$key] .= 'E';

            $fullname = $item->user->employee_data->full_name_1.$item->user->employee_data->full_name_2;
            $data[$key] .= substr($fullname,0,22);

            $fullname_len = strlen($fullname);
            if($fullname_len<22){
                while($fullname_len < 22){
                    $data[$key] .= ' ';
                    $fullname_len++;
                }
            }

            // 58 space
            for($x=1;$x<=58;$x++){
                $data[$key] .= ' ';
            }

            $return = $this->get_fund_section($item);

            if(!empty($return)){
                foreach($return as $k => $val){

                    $data[$key] .= "\r\n";
                    $data[$key] .= $data['first'];
                    $data[$key] .= $val;

                    $fullname = $item->user->employee_data->full_name_1.$item->user->employee_data->full_name_2;
                    $data[$key] .= substr($fullname,0,22);

                    $fullname_len = strlen($fullname);
                    if($fullname_len<22){
                        while($fullname_len < 22){
                            $data[$key] .= ' ';
                            $fullname_len++;
                        }
                    }

                    // 58 space
                    for($x=1;$x<=58;$x++){
                        $data[$key] .= ' ';
                    }

                }
            }

            $cc++;
        }

       return $data;
    }

    public function get_fund_section($item)
    {
        // check fund section if null
        if(empty($item->user->employee_data->funds_selection)){
            return false;
        }

        $funds = json_decode($item->user->employee_data->funds_selection);

        $array = array();

        if(!empty($funds) && in_array('MBMF', $funds)){
            $array['02'] = '02';
            $array['02'] .= $this->put_zero($item->user->employee_data->id_number, 9); // get zero
            $array['02'] .= substr($item->user->employee_data->id_number, 0, 9);

            $MBMF = $item->item_deductions->where('title','MBMF')->first();
            $mbmf = 0;
            if(!empty($MBMF) && ($MBMF->amount)>0){
                $mbmf = $MBMF->amount;
            }
            $mbmf = number_format($mbmf, 2, '.', '');
            $mbmf = str_replace(".","",$mbmf);
            $array['02'] .= $this->put_zero($mbmf, 12) . $mbmf;
            $array['02'] .= $this->put_zero('', 10);
            $array['02'] .= $this->put_zero('', 10);
            $array['02'] .= ' ';

        }

        if(!empty($funds) && in_array('SINDA', $funds)){
            $array['03'] = '03';
            $array['03'] .= $this->put_zero($item->user->employee_data->id_number, 9); // get zero
            $array['03'] .= substr($item->user->employee_data->id_number, 0, 9);

            $SINDA = $item->item_deductions->where('title','SINDA')->first();
            $sinda = 0;
            if(!empty($SINDA) && ($SINDA->amount)>0){
                $sinda = $SINDA->amount;
            }
            $sinda = number_format($sinda, 2, '.', '');
            $sinda = str_replace(".","",$sinda);
            $array['03'] .= $this->put_zero($sinda, 12) . $sinda;
            $array['03'] .= $this->put_zero('', 10);
            $array['03'] .= $this->put_zero('', 10);
            $array['03'] .= ' ';

        }

        if(!empty($funds) && in_array('CDAC', $funds)){
            $array['04'] = '04';
            $array['04'] .= $this->put_zero($item->user->employee_data->id_number, 9); // get zero
            $array['04'] .= substr($item->user->employee_data->id_number, 0, 9);

            $CDAC = $item->item_deductions->where('title','CDAC')->first();
            $cdac = 0;
            if(!empty($CDAC) && ($CDAC->amount)>0){
                $cdac = $CDAC->amount;
            }
            $cdac = number_format($cdac, 2, '.', '');
            $cdac = str_replace(".","",$cdac);
            $array['04'] .= $this->put_zero($cdac, 12) . $cdac;
            $array['04'] .= $this->put_zero('', 10);
            $array['04'] .= $this->put_zero('', 10);
            $array['04'] .= ' ';

        }

        if(!empty($funds) && in_array('ECF', $funds)){
            $array['05'] = '05';
            $array['05'] .= $this->put_zero($item->user->employee_data->id_number, 9); // get zero
            $array['05'] .= substr($item->user->employee_data->id_number, 0, 9);

            $ECF = $item->item_deductions->where('title','ECF')->first();
            $ecf = 0;
            if(!empty($ECF) && ($ECF->amount)>0){
                $ecf = $ECF->amount;
            }
            $ecf = number_format($ecf, 2, '.', '');
            $ecf = str_replace(".","",$ecf);
            $array['05'] .= $this->put_zero($ecf, 12) . $ecf;
            $array['05'] .= $this->put_zero('', 10);
            $array['05'] .= $this->put_zero('', 10);
            $array['05'] .= ' ';
        }

        return $array;
    }

    public function put_zero($char, $len = 9)
    {
        $return = '';
        $c = strlen($char);

        while($c < $len){
            $return .= '0';
            $c++;
        }

        return $return;
    }

    public function trailer_record($total_lines,$length)
    {
        $total = $length + 1; // +2 for header ahd trailer record

        $post = Input::all();
        $payroll = date('Ym',strtotime($post['year'].'-'.$post['month'])); // set month with zero ex. 2018-4 to 201804
        $company = session('current_company');

        $data = 'F';
        $data .= '9';
        $data .= $company->sg_company_details->org_id;

        if(strlen($company->sg_company_details->org_id)< 10){
            echo 'pasok';
            $x = strlen($company->sg_company_details->org_id);
            while($x < 10){
                $data .= ' ';
                $x++;
            }
        }

        $data .= 'PTE';
        $data .= '01 01';
        $data .= $this->put_zero($total,7).$total;

        $payroll = Payroll::where('company_id', $company->id)->where('month', $post['month'])->where('year', $post['year'])->first();

        $total_amount = 0;
        foreach($payroll->items_orderId as $key => $item) {

            $mbmf = 0;
            $sinda = 0;
            $cdac = 0;
            $ecf = 0;

            if($item->user->employee_data->funds_selection != "null") { // check if included in FUNDS SELECTIONS

                if(!in_array($item->user_id, $post['employees'])) {
                    continue;
                }

                $decode = json_decode($item->user->employee_data->funds_selection);

                if(!empty($decode) && in_array('MBMF', $decode)) { // check if included in FUNDS SELECTIONS
                    $MBMF = $item->item_deductions->where('title','MBMF')->first();
                    if(!empty($MBMF) && ($MBMF->amount)>0){
                        $mbmf += $MBMF->amount;
                    }
                }

                if(!empty($decode) && in_array('SINDA', $decode)) { // check if included in FUNDS SELECTIONS
                    $SINDA = $item->item_deductions->where('title','SINDA')->first();
                    if(!empty($SINDA) && ($SINDA->amount)>0){
                        $sinda += $SINDA->amount;
                    }
                }

                if(!empty($decode) && in_array('CDAC', $decode)) { // check if included in FUNDS SELECTIONS
                    $CDAC = $item->item_deductions->where('title','CDAC')->first();
                    if(!empty($CDAC) && ($CDAC->amount)>0){
                        $cdac += $CDAC->amount;
                    }
                }

                if(!empty($decode) && in_array('ECF', $decode)) { // check if included in FUNDS SELECTIONS
                    $ECF = $item->item_deductions->where('title','ECF')->first();
                    if(!empty($ECF) && ($ECF->amount)>0){
                        $ecf += $ECF->amount;
                    }
                }

            }

            $total_amount += ($item->cpf_employee + $item->cpf_employer) + $sinda + $cdac + $ecf + $mbmf ;

            if($item->employee_data->cpf_entitlement == 1){
                $sdl = $item->ow + $item->aw;
            }else{
                $sdl = $item->salary_rate * $item->no_frequency;
            }

            if($sdl<=800){
                $total_amount += 2;
            }elseif($sdl >= 4500){
                $total_amount += 11.25;
            }else{
                $total_amount += $sdl*0.0025;
            }

        }
        $total_amount = (int)$total_amount;
        $total_amount = number_format($total_amount, 2, '.', '');
        $total_amount = str_replace(".","",$total_amount);

        $data .= $this->put_zero($total_amount,15).$total_amount;

        // 108 space
        for($x=1;$x<=108;$x++){
            $data .= ' ';
        }

        return $data;

    }


    public function getCompanyIras()
    {
        $company = session('current_company');

         return view('dashboard.payroll.iras.index', compact('company'));
    }

    public function getGenerateIras()
    {
        $company = session('current_company');
        $employees = $company->payroll_employees;
        $years = array();
        $years = Payroll::whereCompanyId($company->id)->addSelect('year')->distinct()->get();

        return view('dashboard.payroll.iras.generate', compact('employees', 'years'));
    }

    public function postGenerateIras(Request $request)
    {
        $company = session('current_company');
        $final_iras8A = $this->payrollService->generateIR8AForm($request->input());

        $fileText = $final_iras8A;
        $fileName = time()."-iras8A.txt";
        file_put_contents( base_path() . '/public/uploads/'.$fileName,$fileText);

        $zen_url = config('app.bucket_base_url');
        $company_special_folder = '/company-'. $company->id;

        $s3 = Storage::disk('s3');
        $s3->makeDirectory($company_special_folder);
        $filePath =  base_path() . '/public/uploads/'.$fileName;
        $s3_upload_path = $company_special_folder.'/'.$fileName;
        $image = $s3->put($s3_upload_path, file_get_contents($filePath),'public');
        $s3_file_url = $zen_url.$s3_upload_path;
        unlink($filePath);

        $sg_iras_form = new SGIrasForm;
        $sg_iras_form->company_id = $company->id;
        $sg_iras_form->year = $request->input('year');
        $sg_iras_form->type = 'company'; //company or individual
        $sg_iras_form->file_url = $s3_file_url;
        $sg_iras_form->save();

        return view('dashboard.payroll.iras.result', compact('sg_iras_form'));
    }

    public function getEmployeesFromYear(Request $request)
    {
        $year = $request->input('year');
        $sgemployees = SGEmployeeData::all();

        return view('dashboard.payroll.iras.partials.employee-from-year', compact('sgemployees'));
    }

    public function getIrasPerEmployee(Request $request)
    {
        $employees = $request->input('employees');
        $sgemployees = SGEmployeeData::whereIn('id', $employees)->get();

        return view('dashboard.payroll.iras.partials.iras-per-employee', compact('sgemployees'));
    }


}
