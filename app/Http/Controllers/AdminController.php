<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use App\Deal;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Config;
use Input;
use Intervention\Image\Facades\Image;
use Redirect;
use App\User;
use App\Classes\CustomFields;

use DB;
use AWS;
use Storage;
use Excel;
//use Request;

use Auth;
use App\Admin;
use App\Company;
//use App\Http\Requests;
use App\Http\Requests\AdminRequest;
use App\Stages as Stage;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    /**
     * @var Admin
     */
    protected $admin;

    /**
     * @var Stage
     */
    protected $stage;

    /**
     * @var Company
     */
    protected $company;

    /**
     * AdminController constructor.
     *
     * @param Admin   $admin
     * @param Company $company
     * @param Stage   $stage
     *
     * @author Bertrand Kintanar <bertrand.kintanar@gmail.com>
     */
    public function __construct(Admin $admin, Company $company, Stage $stage)
    {
        $this->admin = $admin;
        $this->company = $company;
        $this->stage = $stage;
    }

    /**
     * @return \Illuminate\View\View
     *
     * @author Luigi Gaviola <luigi.acuna.gaviola@gmail.com>
     */
    public function admin_dashboard()
    {
        return view('admin.dashboard.dashboard');
    }

    /**
     * @return \Illuminate\View\View
     *
     * @author Luigi Gaviola <luigi.acuna.gaviola@gmail.com>
     */
    public function admin_login()
    {
        return view('admin.login');
    }

    /**
     * @param AdminRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     *
     * @author Luigi Gaviola <luigi.acuna.gaviola@gmail.com>
     * @author Bertrand Kintanar <bertrand.kintanar@gmail.com>
     */
    public function process_admin_login(AdminRequest $request)
    {
        $request = $request->all();

        $admin = $this->admin->whereUsername($request['username'])->first();

        if (!$admin || !password_verify($request['password'], $admin->password)) {
            return redirect()->to('admin_login')->withInput()->with('error', 'Username/Password Wrong!');
        }

        session()->put('admin', $admin);

        return redirect()->to('admin_dashboard');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     *
     * @author Luigi Gaviola <luigi.acuna.gaviola@gmail.com>
     */
    function admin_logout()
    {
        session()->flush();
        return redirect()->to('admin_login');
    }

    /**
     * @return \Illuminate\View\View
     *
     * @author Ginndee Claramount <ginndee.p.claramount@gmail.com>
     */
    public function crm_setting()
    {
        $user = Auth::user();
        $is_admin = $user->isAdmin();

        $company = session('current_company');

        $stages = $this->stage->where('company_id', $company->id)
            ->orderBy('stage_position', 'ASC')
            ->get()->toArray();

        return view('dashboard.crm.setting', compact('stages', 'company', 'is_admin'));
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     *
     * @author Ginndee Claramount <ginndee.p.claramount@gmail.com>
     */
    public function create_stages(Request $request)
    {
        $request = $request->all();
        $company = session('current_company');

        $request['company_id'] = $company->id;

        $this->stage->create($request);

        return redirect()->to('crm_setting')->withInput()->with('success', 'Successfully added new stages');
    }

    /**
     * @param Request $request
     * @param         $id
     * @return \Illuminate\Http\RedirectResponse
     *
     * @author Ginndee Claramount <ginndee.p.claramount@gmail.com>
     */
    public function stages_field_edit(Request $request, $id)
    {
        if (!$id) return response('No ID Received', 404);

        $post = Input::all();

        if ($request->isMethod('post')) {

            $company = session('current_company');
            if (!$company) return response('No ID Received', 404);

            $stage = $this->stage->find($id);
            $stage['stage_label'] = $post['stage_label'];
            $stage['stage_desc'] = $post['stage_desc'];
            $stage['stage_position'] = $post['stage_position'];
            $stage->save();

            return redirect()->to('crm_setting')->withInput()->with('success', 'Successfully Edited Stage!');
        }

        $stage = $this->stage->find($id)->toArray();

        return view('dashboard.crm.crm_field_edit', compact('stage'));
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     *
     * @author Ginndee Claramount <ginndee.p.claramount@gmail.com>
     */
    public function stage_field_delete($id)
    {
        $company = session('current_company');
        if (!$id) {
            return response('Stage field not found', 404);
        }

        $Stage = $this->stage->find($id);

        if (!$Stage) {
            return response('Stage not found', 404);
        }

        if ($Stage) {
            $Stage->delete();

            $get_first = Stage::where('company_id', $company->id)->orderBy('stage_position', 'ASC')->first();

            $deals = Deal::where('stage',$Stage->id)->get()->toArray();
            foreach($deals as $k => $deal){
                $d = Deal::find($deal['id']);
                $d->stage = $get_first->id;
                $d->save();
            }

            return redirect()->to('crm_setting')->withInput()->with('success', 'Successfully Deleted Stage!');
        } else {
            return redirect()->to('crm_setting')->withInput()->with('error', 'Error Occured!');
        }
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @author Ginndee Claramount <ginndee.p.claramount@gmail.com>
     */
    public function update_stage_rank(Request $request)
    {
        $post = $request->all();
        $company = session('current_company');

        foreach ($post['id'] as $key => $stage_id) {

            $stage = $this->stage->where('company_id', $company->id)->find($stage_id);

            $stage['stage_position'] = $key + 1;
            $stage->save();
        }

        return response()->json(true);
    }

    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @author Ginndee Claramount <ginndee.p.claramount@gmail.com>
     */
    public function save_crm_setting(Request $request)
    {
        $post = $request->all();
        $company = session('current_company');

        $my_company = $this->company->find($company->id);

        if ($post['status'] == 1) {
            $my_company['contact_setting'] = 1;
            $my_company['crm_setting'] = 1;
        } else {
            $my_company['crm_setting'] = 0;
        }
        $my_company->save();

        return response()->json(true);
    }

    public function crm_export($type = 'csv')
    {
        $user = Auth::user();
        $is_admin = $user->isAdmin();

        $company = session('current_company');

        if(!$is_admin){
            return Redirect::to('crm_setting');
        }

        // to make sure file type
        if($type == 'xls'){
            $type = 'xls';
        }else{
            $type = 'csv';
        }

        $crm = Deal::select(DB::raw('deals.deal_title as "Deal Title", contacts.contact_name as "Contact Name", organization.organization_name as "Organization Name"'),
            DB::raw('deals.deal_value as "Deal Value", stages.stage_label as "Stage", deals.status as "Status"'),
            DB::raw('IF(deals.won_date = "0000:00:00 00:00:00","",DATE_FORMAT(deals.won_date, "%b %e %Y %T")) as "Won Date"'),
            DB::raw('IF(deals.lost_date = "0000:00:00 00:00:00","",DATE_FORMAT(deals.lost_date, "%b %e %Y %T")) as "Lost Date"'),
            DB::raw('deals.lost_reason as "Lost Reason"'))
            ->join('contacts','contacts.id','=','deals.contact_id')
            ->join('organization', 'contacts.organization_id','=','organization.id')
            ->join('stages', 'stages.id','=','deals.stage')
            ->where('organization.company_id',$company->id)
            ->where('deals.status','!=', 'DELETE')
            ->orderBy('deals.id', 'DESC')
            ->get()->toArray();

        Excel::create($company->company_name.' CRM '.date('d-M-Y'), function($excel) use($crm){

            $excel->sheet('CRM', function($sheet) use($crm){

                $sheet->setOrientation('landscape');
                $sheet->fromArray($crm);

            });

        })->export($type);

        return Redirect::to('crm_setting');

    }



}

