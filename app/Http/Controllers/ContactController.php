<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
//use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Company_user;
use App\Company;
use App\Organization;
use App\Contact;

use App\User;
use Mail;
use Request;
use Auth;
use DB;
use Redirect;
use Input;
use Response;

use App\EmailTemplates;
use App\Classes\Helper;

class ContactController extends Controller
{
	public function __construct(){
        $this->middleware('auth');
    }
    public function receive_message(Request $req)
    {	
    	//TODO forward message to contact us email
    	$email = $req->input("contact_email");
    	$name = $req->input("contact_name");
    	$text = $req->input("contact_message");
    	$lang = $req->input("lang");
    	if(empty($lang)){
    		$lang = "en";
    	}
    	if(empty($email) || empty($name) || empty($text)){
    		return response()->json(['message'=>trans('landingpage.Please fill out all fields.',[],null,$lang)], Response::HTTP_BAD_REQUEST);
    	}

        $email_template = EmailTemplates::where('slug','contact_us')->first();

        $subject = $email_template->subject;

        $for_parsing = [
            'name' => $name,
            'email' => $email,
            'message' => $text
        ];

        $body = Helper::parse_email($email_template->body,$for_parsing);

        Mail::send('emails.default_email', compact('body'), function($message) use ($email,$name,$subject){
        	$message->from($email, $name);
        	$message->to(config('mail.contact_us'), "Contact");
        	$message->subject($subject);
        });
    }

    public function index()
    {
        $user = Auth::user();
        $page = Input::get('page');

        $is_admin = $user->isAdmin();

        $contacts = DB::table('contacts')->select('*','contacts.id AS contact_id')
                        ->leftjoin('organization', 'contacts.organization_id','=','organization.id')
                        ->join('users', 'contacts.owner_id','=','users.id')
                        ->where('organization.company_id', session('current_company')->id)
                        ->whereNotNull('contacts.id')
                        ->paginate(25);

        $json_contacts = $contacts->toArray()['data'];

        $total = DB::table('contacts')->select('*','contacts.id AS contact_id')
            ->leftjoin('organization', 'contacts.organization_id','=','organization.id')
            ->join('users', 'contacts.owner_id','=','users.id')
            ->where('organization.company_id', session('current_company')->id)
            ->whereNotNull('contacts.id')
            ->count();

        return view('dashboard.contact.index', compact('contacts','json_contacts','total', 'page', 'is_admin'));
    }

    public function contact_search(){
        $post = Input::all();
        $user = Auth::user()->toArray();

        $contacts = Contact::select('*','contacts.id AS contact_id')
            ->join('organization AS organization2', 'contacts.organization_id','=','organization2.id')
            ->join('users AS users2', 'contacts.owner_id','=','users2.id')
            ->where('organization2.company_id', session('current_company')->id)
            ->where($post['column_name'], 'LIKE', '%'.$post['search'].'%')->get();
//            ->search($post['search'])->get();

        $json_contacts = $contacts->toArray();
        $contacts = paginateCollection($contacts, 25);


        $total = Contact::select('*','contacts.id AS contact_id')
            ->join('organization AS organization2', 'contacts.organization_id','=','organization2.id')
            ->join('users AS users2', 'contacts.owner_id','=','users2.id')
            ->where('organization2.company_id', session('current_company')->id)
            ->where($post['column_name'], 'LIKE', '%'.$post['search'].'%')
            ->count();
//        $contacts->appends(['search' => $post['search']])->render();

        return view('dashboard.contact.index', compact('contacts','json_contacts','post', 'total'));
    }

    public function create()
    {
        $user = Auth::user();
        $company = session('current_company');

        $request = Request::all();

        // i don't remove this for query reference
        $contact = Contact::select('*','contacts.id AS id')
                    ->join('organization','organization.id','=','contacts.organization_id')
                    ->where('contacts.contact_email','=',$request['contact_email'])
                    ->where('organization.company_id',$company->id)
                    ->get()->toArray();
//        $contact = Contact::with('organization','organization.contact')->get()->toArray();

        if(!empty($contact)){
            return Redirect::to('contact')->withInput()->with('errors', 'A contact with the same email already exists, please update your contact or create one with a different email.');
        }

		$request['owner_id']=$user->id;
        $request['created_date']=date('Y-m-d H:i:s');
        Contact::create($request);
		
		$organization = Organization::find($request['organization_id'])->toArray();
		$nb_of_contacts = !empty($organization['nb_of_contacts'])?$organization['nb_of_contacts']:0;
		
		$update_org = Organization::find($request['organization_id']);
		$update_org->nb_of_contacts = $nb_of_contacts+1;
		$update_org->save();
		
        return redirect('contact');

    }
    public function create_json()
    {
        $user = Auth::user();
        $company = session('current_company');

        $request = Request::all();


        // i don't remove this for query reference
        $contact = Contact::select('*','contacts.id AS id')
                    ->join('organization','organization.id','=','contacts.organization_id')
                    ->where('contacts.contact_email','=',$request['contact_email'])
                    ->where('organization.company_id',$company->id)
                    ->where('contacts.organization_id',$request['contact_organization_id'])
                    ->get()->toArray();

        if(!empty($contact)){
            return Response::json(array('error','A contact with the same email already exists, please update your contact or create one with a different email.'));
        }

		$request['organization_id']=$request['contact_organization_id'];
		$request['owner_id']=$user->id;
        $request['created_date']=date('Y-m-d H:i:s');
        $mycontact = Contact::create($request);

		$organization = Organization::find($request['organization_id'])->toArray();
		$nb_of_contacts = !empty($organization['nb_of_contacts'])?$organization['nb_of_contacts']:0;

		$update_org = Organization::find($request['organization_id']);
		$update_org->nb_of_contacts = $nb_of_contacts+1;
		$update_org->save();

        return Response::json($mycontact);

    }

    public function contact_delete($id)
    {
        if(!$id) return response('Contact not found',404);

        $organization = Contact::find($id);

        if(!$organization) return response('Contact not found',404);

        if($organization){
            $organization->delete();
            return Redirect::to('contact')->withInput()->with('success', 'Successfully Deleted Contact!');
        }else{
            return Redirect::to('contact')->withInput()->with('error', 'Error Occured!');
        }

    }

    public function contact_edit($id,Request $request)
    {
        if(!$id) return response('No ID Received',404);

        $post = Input::all();

        if (Request::isMethod('post')){

            $contact = Contact::find($id);

            $contact->contact_name = $post['contact_name'];
            $contact->contact_email = $post['contact_email'];
            $contact->contact_phone = $post['contact_phone'];
            $contact->organization_id = $post['organization_id'];
            $contact->position = $post['position'];
            $contact->address = $post['address'];
            $contact->contact_notes = $post['contact_notes'];

            $contact->save();

            return redirect('contact')->withInput()->with('success', 'Successfully Edited Organization!');

        }

        $contact = Contact::select('*','contacts.id AS contact_id')
            ->leftjoin('organization', 'contacts.organization_id','=','organization.id')
            ->where('contacts.id',$id)->get()->toArray();
        $contact = $contact[0];

        echo view('dashboard.contact.form_edit',compact('contact'));
    }

    public function contact_module_status()
    {
        $post = Input::all();

        if (Request::isMethod('post')){

            $user = Auth::user()->toArray();
            $company = Company_user::where('user_id',$user['id'])->get()->toArray();

            $contact = Company::find($company[0]['company_id']);

            $contact->contact_setting = $post['value'];

            $contact->save();
        }
    }

    public function contact_list()
    {
        $post = Input::all();

        $user = Auth::user()->toArray();
        $company = session('current_company');

        $contact = Contact::select('contacts.id AS id','contact_name AS label','contact_name AS value')
            ->join('organization','organization.id','=','contacts.organization_id')
            ->where('contacts.contact_name', 'LIKE', '%'.$post['q'].'%')
            ->where('organization.company_id',$company->id);

        if(array_key_exists('organization_id',$post)){
            $contact = $contact->where('contacts.organization_id',$post['organization_id']);
        }
        $contact = $contact->get()->toArray();

        return Response::json($contact);
    }

    public function create_organization_in_contact()
    {
        $post = Input::all();

        $user = Auth::user();

        $request['organization_name']=$post['organization_name'];
        $request['company_id']=session('current_company')->id;
        $request['owner_id']=$user['attributes']['id'];
        $request['created_date']=date('Y-m-d H:i:s');
        $request['nb_of_contacts']=1;

        $request['extra_fields'] = json_encode(array());
        $id = Organization::create($request);

        return Response::json(array('message'=>'Successfully Added Organization!', 'id'=>$id->id));

    }






}
