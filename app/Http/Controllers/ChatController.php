<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Chat;
use App\ChatLine;
use App\User;
use App\User_endpoints; 
use App\User_notification; 
use App\Company;
use Redis;
use Auth;
use DB;
use Log;
use Storage;
use App\Classes\StatusChangeNotification;
use App\Classes\ChatMessageNotification;
use App\Classes\NewChatNotification;
use App\Classes\ChatsResponse;
use App\Classes\ElasticFacade;
use \Firebase\JWT\JWT;
use \Carbon\Carbon;
use \Ramsey\Uuid\Uuid;

class ChatController extends Controller
{
    
    public function searchelastic(Request $request) {
        if(!$request['query']){
            return response('Missing query',Response::HTTP_BAD_REQUEST);
        }
        //validated
        $elastic = app(ElasticFacade::class);
        $user_chats= $request->user()->chats->pluck('id')->all();
        
        $parameters = [
            'index' => 'chat',
            'type' => 'chatline',
            'body' => [
                "query" => [
                    "bool" => [
                      "must" => [
                        [ "match" => [ "text_content" =>   $request['query'] ]]
                      ],
                      "filter" => [ 
                        [ "terms" =>  [ "chat_id" => $user_chats ]]
                      ]
                    ]
                ]
            ]
        ]; 
    
        $response = $elastic->search($parameters);
        //dd(($response["hits"]["hits"]));
        $result= array();
        foreach ($response["hits"]["hits"] as $key => $hit) {
            $chat=Chat::where("id","=",$hit['_source']["chat_id"])->with("users")->first();
            $user=User::find($hit['_source']["user_id"]);
            $chat_line=$hit['_source']["text_content"];
            $chat_line=ChatLine::where("id","=",$hit['_source']['id'])->with("user","chat")->first();
            if ($chat_line->chat->chat_type=="group") {
                $chat_line->reciever=$chat_line->chat->chat_name;
            } else {
                if ($chat_line->chat->users->sortBy("id")->first()->id==$chat_line->user->id) {
                    $chat_line->reciever= $chat_line->chat->users->sortByDesc("id")->first();
                } else {
                    $chat_line->reciever=$chat_line->chat->users->sortBy("id")->first();
                }
            }
            array_push($result,$chat_line);
        }
        return $result;
        
    }
    public function searchelasticChat(Request $request) {
        if(!$request['query']){
            return response('Missing query',Response::HTTP_BAD_REQUEST);
        }
        if(!$request['chat_id']){
            return response('Missing chat',Response::HTTP_BAD_REQUEST);
        }
        //validated
        $elastic = app(ElasticFacade::class);
        
        $parameters = [
            'index' => 'chat',
            'type' => 'chatline',
            'body' => [
                "query" => [
                    "bool" => [
                      "must" => [
                        [ "match" => [ "text_content" =>   $request['query'] ]],
                        [ "match" => [ "chat_id" =>   $request['chat_id'] ]]
                      ]
                    ]
                ]
            ]
        ]; 
    
        $response = $elastic->search($parameters);
        //dd(($response["hits"]["hits"]));
        $result= array();
        foreach ($response["hits"]["hits"] as $key => $hit) {
            $chat=Chat::where("id","=",$hit['_source']["chat_id"])->with("users")->first();
            $user=User::find($hit['_source']["user_id"]);
            $chat_line=$hit['_source']["text_content"];
            $chat_line=ChatLine::where("id","=",$hit['_source']['id'])->with("user","chat")->first();
            if ($chat_line->chat->chat_type=="group") {
                $chat_line->reciever=$chat_line->chat->chat_name;
            } else {
                if ($chat_line->chat->users->sortBy("id")->first()->id==$chat_line->user->id) {
                    $chat_line->reciever= $chat_line->chat->users->sortByDesc("id")->first();
                } else {
                    $chat_line->reciever=$chat_line->chat->users->sortBy("id")->first();
                }
            }
            array_push($result,$chat_line);
        }
        return $result;
        
    }
    public function get_user_chats_view(Request $req){

        $user = Auth::user();

        $chats = $this->get_user_chats($user)->chats;

        return view('dashboard.chat.chats',compact('chats'));
    }

    public function get_user_chats_data(Request $req){

        $chats = $this->get_user_chats($req->user());

        return response($chats->toJson());
    }
    
    private function get_user_chats($user){

        //find all other users within company
        //TODO update to find all users within currently selected

        if($user->guest_company()){
            $other_users = [];
        }else{
            $other_users = $user->default_company()->users()->where("users.id","<>",$user->id)->get();
        }



        $user_arr = array();

        foreach($other_users as $other_user){
            $user_arr[$other_user->id] = $other_user;
        }

        //find all one-to-one chats for a user
        $one_to_one_chats = $user->chats()->with('users')->where('chat_type', '=', 'one-to-one')->get();
        // dd($one_to_one_chats);
        $group_chats = $user->chats()->with('users')->where('chat_type', '=', 'group')->get();
        // dd($group_chats);

        //remove any users who are currently in a chat with logged in user from array
        foreach ($one_to_one_chats as $chat) {
            foreach($chat->users as $chat_user){
                if(isset($user_arr[$chat_user->id])){
                    unset($user_arr[$chat_user->id]);
                }
            }
        }
        //create array of chats
        //3 types --> group,one-to-one,empty
        $chats = new ChatsResponse($group_chats,$one_to_one_chats,$user_arr,$user);
        return $chats;
    }

    public function send_message_new(Request $req){
        //validate
        $upload = $req->file('file');
        $active_chat = $req->input('active_chat');
        if(empty($active_chat['id'])){
            $active_chat = json_decode($req->input('active_chat'),true);
        }
        $chat;
        //validation 
        //check file is valid
        if($upload){
            if(!$upload->isValid()){
                return response('File is invalid',Response::HTTP_BAD_REQUEST);
            }
            //check filesize is not too large
            if($upload->getClientSize() > config('filesystems.chat_max_file_size_b')){
                return response('File size is too large',Response::HTTP_BAD_REQUEST);
            }
        }

        if(!$active_chat){
            return response('Please supply chat to send message to', Response::HTTP_BAD_REQUEST);
        }
        //if chat_type empty --> intent to create new chat
        if($active_chat['chat_type'] === 'empty'){
            //users must be in same company
            $user_ids = collect($active_chat['users'])->pluck('id')->all();
            if(!User::belongsToSameCompany($user_ids)){
                return response("Users are not in the same company", Response::HTTP_BAD_REQUEST);
            }
            if ($req->user()->guest_company()) {
                return response("Guests cannot intiate chats", Response::HTTP_BAD_REQUEST);
            }
            //if one-to-one
            if($active_chat['chat_intent'] === 'one-to-one'){
                //check if chat does not already exist between users
                if(Chat::existingOneToOne($user_ids[0],$user_ids[1])){
                    return response('There is already a one-to-one chat between these users, please use the existing chat',Response::HTTP_BAD_REQUEST);
                }

                if (count($user_ids)>2) {
                    return response('Please create group chat to join multiple users',Response::HTTP_BAD_REQUEST);
                }
            }
        }else{
            $chat = Chat::find($active_chat['id']);
            //chat exists
            if(!$chat){
                return response('Chat does not exist', Response::HTTP_BAD_REQUEST);
            }

            //user is in chat
            $user = $chat->users()->where('user_id', $req->user()->id)->get();
            if(!count($user)){
                return response('User not in chat', Response::HTTP_BAD_REQUEST);   
            }
        }

        //validation passed

        //create a chat if it hasn't been done yet
        if($active_chat['chat_type'] === 'empty'){
            $chat = new Chat([
                'chat_type' => $active_chat['chat_intent']
            ]);
            //add the current user to the active chat if type == group
            if($active_chat['chat_intent'] === 'group'){
                $active_chat['users'][] = array('id' => $req->user()->id);
                $chat->chat_name = $active_chat['chat_name'];
            }
            $chat->save();
            $chat->users()->sync(collect($active_chat['users'])->pluck('id')->all());
        }
        
        //create chat_line
        $chat_line = new ChatLine([
            'chat_id' => $chat->id,
            'user_id' => $req->user()->id,
            'text_content' => $req->input('text'),
            'time_sent' => Carbon::now()
        ]);

        //upload file if one is sent
        if($upload){

            $disk = Storage::disk('s3');

            $file_name = $upload->getClientOriginalName();
            $file_path = 'chats/'.$chat->id.'/'.Uuid::uuid4().'/'.$file_name;

            $aws_result = $disk->put($file_path, file_get_contents($upload));
            $aws_url = $disk->getAdapter()->getClient()->getObjectUrl($disk->getAdapter()->getBucket(), $file_path);

            $chat_line->media_content = $aws_url;
            $chat_line->media_size_in_bytes = $upload->getClientSize();
            $chat_line->media_name = $file_name;
            $chat_line->media_type = 'file';
        }

        $chat_line->save();
        $chat->users()->updateExistingPivot(Auth::user()->id,['last_read_message' => $chat_line->id]);
        $notif;
        if($active_chat['chat_type'] === 'empty'){
            $notif = new NewChatNotification($chat,$req->user());
        }else{
            $notif = ChatMessageNotification::createFromChat($chat_line, $chat);
        }
        
        $json = $notif->toJson();
        $sender= User::find($chat_line->user_id);
        foreach ($chat->users as $key => $user) {
            if ($user->id!=Auth::user()->id) {
                $endpoints= User_endpoints::where("user_id","=",$user->id)->get();
                // Message to be sent
                $message = ["title" => $sender->first_name." ".$sender->last_name,"message" => $chat_line->text_content, "icon" => $sender->photo];
                $message=json_encode($message);
                foreach ($endpoints as $endpoint) {
                    $reg= explode("/",$endpoint->endpoint);
                    $reg= $reg[count($reg)-1];
                    $apiKey = "AIzaSyB5X3WtyLVzpLC0cd95O69gKiUtewDFEMM";

                    $registrationIDs = array($reg);

                    
                    // Set POST variables
                    $url = 'https://android.googleapis.com/gcm/send';

                    $fields = array(
                                    'registration_ids'  => $registrationIDs,
                                    'data'              => array( "notification" => $message ),
                                    );

                    $headers = array( 
                                        'Authorization: key=' . $apiKey,
                                        'Content-Type: application/json'
                                    );

                    // Open connection
                    $ch = curl_init();

                    // Set the url, number of POST vars, POST data
                    curl_setopt( $ch, CURLOPT_URL, $url );

                    curl_setopt( $ch, CURLOPT_POST, true );
                    curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

                    curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );

                    // Execute post
                    $result = curl_exec($ch);
                    }
                    $notification= User_notification::create(["user_id" => $user->id, "notification" => $message, "consumed" =>0]);
                
            }
        }
        //publish message
        Redis::publish('user_notifications', $json);

        return $json;
    }
    public function get_notif(Request $request) {
        $notifications= User_notification::where("user_id","=",$request['uid'])->orderby('id',"DESC")->first();
        if (empty($notifications)) {
            return response()->json(['message'=>"No notification found"],Response::HTTP_BAD_REQUEST);
        }
        $notif= json_decode($notifications->notification);
        $notifications->consumed=$notifications->consumed+1;
        
        $endpoints= User_endpoints::where("user_id","=",$request["uid"])->get();
        if ($notifications->consumed==count($endpoints)) {
            $notifications->delete();
        } else {
            $notifications->save();
        }
        return response()->json(['title' => $notif->title , 'message' => $notif->message, 'icon' => $notif->icon]);
    }
    public function get_chat_messages(Request $req, $chat_id){
        $chat = $req->user->chats()
                            ->with(['users','chat_lines' => function($query){
                                $query->orderBy('created_at','asc');
                            },'chat_lines.user'])
                            ->where('chat.id', $chat_id)
                            ->first();

        if($chat === null){
            return response()->json(['message'=>"This chat either does not exist, or does not include this user"],Response::HTTP_BAD_REQUEST);
        }

        $last=$chat->chat_lines->sortByDesc('time_sent')->first()->id;
        $chat->users()->updateExistingPivot($req->user()->id,['last_read_message' => $last]);
        
        $chat->chat_name = Chat::name($chat,$req->user());

        
        $seen= [];
        if ((($chat->chat_lines->sortByDesc('time_sent')->first()->user_id==$req->user()->id) && ($chat->chat_type=="one-to-one")) || ($chat->chat_type == "group")) {
            foreach ($chat->users as $key => $user) {
                $last_read_message=$chat->users()->where('user_id', $user->id)->first()->pivot->last_read_message;
                if (($last_read_message== $last) && ($user->id!=$req->user()->id) ) {
                    $seen[]= $user->first_name;
                } 
            }
        } 
        $chat->status = "";
        if ((($chat->chat_lines->sortByDesc('time_sent')->first()->user_id==$req->user()->id) && ($chat->chat_type=="one-to-one")) && (count($seen)==1)) {
            $chat->status = "seen";
        } else if (($chat->chat_type=="group") &&  (count($seen)>0)) {
            if (count($seen)==(count($chat->users)-1)) {
                 $chat->status = "seen by all";
             } else {
                $chat->status = "seen by ". implode(",", $seen);
             }  
        }
        
        return $chat->toJson();
    }
    public function get_chat_messages_part(Request $req, $chat_id,$part){
       
        $chat = Chat::with(['users','chat_lines' => function($query) use($part){
                                $query->orderBy('created_at','desc')->skip($part*10)->take(10)->orderBy('created_at','desc');
                            },'chat_lines.user'])
                            ->where('chat.id', $chat_id)
                            ->first();

        if($chat === null){
            return response()->json(['message'=>"This chat either does not exist, or does not include this user"],Response::HTTP_BAD_REQUEST);
        }
        if($chat->chat_lines->isEmpty()){
            return response()->json(['message'=>"No more messages"],Response::HTTP_BAD_REQUEST);
        }
        $last=$chat->chat_lines->sortByDesc('time_sent')->first()->id;
        $chat->users()->updateExistingPivot($req->user()->id,['last_read_message' => $last]);
        
        $chat->chat_name = Chat::name($chat,$req->user());

        
        $seen= [];
        if ((($chat->chat_lines->sortByDesc('time_sent')->first()->user_id==$req->user()->id) && ($chat->chat_type=="one-to-one")) || ($chat->chat_type == "group")) {
            foreach ($chat->users as $key => $user) {
                $last_read_message=$chat->users()->where('user_id', $user->id)->first()->pivot->last_read_message;
                if (($last_read_message== $last) && ($user->id!=$req->user()->id) ) {
                    $seen[]= $user->first_name;
                } 
            }
        } 
        $chat->status = "";
        if ((($chat->chat_lines->sortByDesc('time_sent')->first()->user_id==$req->user()->id) && ($chat->chat_type=="one-to-one")) && (count($seen)==1)) {
            $chat->status = "seen";
        } else if (($chat->chat_type=="group") &&  (count($seen)>0)) {
            if (count($seen)==(count($chat->users)-1)) {
                 $chat->status = "seen by all";
             } else {
                $chat->status = "seen by ". implode(",", $seen);
             }  
        }
        $lines= $chat->chat_lines->sortBy("created_at");
        $lines=$lines->values()->all();
        
        $chat = $chat->toArray();
        $chat['chat_lines']= $lines;
        return json_encode($chat);
    }
    public function mark_read_chat(Request $req,$chat_id){
        $chat = $req->user()->chats()
                            ->with(['users','chat_lines' => function($query){
                                $query->orderBy('created_at','asc');
                            },'chat_lines.user'])
                            ->where('chat.id', $chat_id)
                            ->first();

        if($chat === null){
            return response()->json(['message'=>"This chat either does not exist, or does not include this user"],Response::HTTP_BAD_REQUEST);
        }
        $last=$chat->chat_lines->sortByDesc('time_sent')->first()->id;
        $chat->users()->updateExistingPivot($req->user()->id,['last_read_message' => $last]);
        $notif = new StatusChangeNotification($chat,$req->user());
        $json= $notif->toJson();
        Redis::publish('user_notifications', $json);

        return "true";
    }

    

    public function create_chat(Request $req){
        $user_id = $req->input('user_id');
        $user = Auth::user();
        //VALIDATION
        //Check that current user is Authorized
        if(!Auth::check()){
            return response()->json(['message'=>"User not authorized"],Response::HTTP_UNAUTHORIZED);
        }
        //Check that user_id is in same company as current user
        if(!User::belongsToSameCompany([$user->id,$user_id])){
             return response()->json(['message'=>"Users not in same company or some do not exist"],Response::HTTP_BAD_REQUEST);
        }

        $chat = Chat::create();
        $chat->users()->sync([$user->id,$user_id]);


        $chat->chat_name = Chat::determineGroupName($chat,$req->user());

        return $chat->toJson();
    }

    public function getUsersByCompany(Request $req, $company_id){
        //Validation
        //company exists
        $company = Company::find($company_id);
        if($company === null){
            return response("Company does not exist", 400);
        }
        $users = $company->users()->get();
        $current_user_in_company = $users->contains('id', $req->user()->id);
        //user from same company as requested
        if(!$current_user_in_company){
            return response("User not in this company", 400);
        }

        return $users->toJson();
    }
}
