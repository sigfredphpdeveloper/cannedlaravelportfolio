<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use DB;
use App\Sg_cpf;
use App\SGEmployeeData;
use App\PayrollDeduction;
use App\PayrollAddition;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class SGPayrollController extends Controller
{

    public function payslips()
    {
        $company = session('current_company');

//        dd($company);

        return view('dashboard.payroll.payslips.payslip_calculation', compact('company'));
    }

    public function my_payroll()
    {
        $user = Auth::user();
        $is_admin = $user->isAdmin();

        return view('dashboard.payroll.payslips.my_payroll');
    }

    public function my_payslips()
    {
        $user = Auth::user();
        $is_admin = $user->isAdmin();

        return view('dashboard.payroll.payslips.my_payslips');
    }

    public function my_irasForms()
    {
        $user = Auth::user();
        $is_admin = $user->isAdmin();

        return view('dashboard.payroll.payslips.my_irasforms');
    }


    public function cpf_computation($data, $employee_id)
    {
        // OW = Ordinary Wage
        // AW = Additional Wage
        // TW = Total Wage
        $ow = 0;
        $aw = 0;
        $tw = 0;

        $emp_data = SGEmployeeData::where('user_id',$employee_id)->first()->toArray();


        // if id type is not NRICc and nationality is not singaporean
        if($emp_data['id_type'] != 'NRIC' && $emp_data['Nationality'] != '301'){

            $cpf_employee['CPF_EMPLOYEE'] = 0;
            $cpf_employee['CPF_EMPLOYER'] = 0;
            $cpf_employee['OW'] = 0;
            $cpf_employee['AW'] = 0;
            return $cpf_employee;
        }

        $PR_YEAR = date('Y-m', strtotime($data['year'].'-'.$data['month'])) - date('Y-m',strtotime($emp_data['pr_date']));

        $post_payroll = new \DateTime($data['year'].'-'.$data['month']);
        $employee_pr_temp = new \DateTime($emp_data['pr_date']);
        $employee_pr= new \DateTime(date_format($employee_pr_temp, 'Y-m'));
        $interval = $post_payroll->diff($employee_pr);
        $m_pr = $interval->y * 12 + $interval->m;
        if ($m_pr <= 12 ) {
                $year_pr= 1;
        } else if ($m_pr <= 24) {
                $year_pr= 2;
        } else {
                $year_pr= 3;
        }

        if($emp_data['Nationality'] == '301' || $year_pr >= 3){

            $employee_category = 'Singaporean';

        }elseif($year_pr <= 1){

            $employee_category = '1st Year PR';

        }else{

            $employee_category = '2nd Year PR';
        }

        //explode the date to get month, day and year
        $emp_data['dob'] = date('m',strtotime($emp_data['dob'])).'/01/'.date('Y',strtotime($emp_data['dob']));
        $birthDate = explode("/", $emp_data['dob']);

        //get age from date or birthdate
        $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
            ? ((date("Y") - $birthDate[2]) - 1)
            : (date("Y") - $birthDate[2]));

        // get cpf rates from age
        $sg_cpf = array();
        $query_sg_cpf = Sg_cpf::where('min_age','<',$age)->where('max_age','>',$age)
            ->where('employee_category',$employee_category)->get();

        foreach($query_sg_cpf as $cpf){
            $payroll_date = date('Y-m',strtotime($data['year'].'-'.$data['month']));
            $cpf_first_date = date('Y-m',strtotime($cpf['first_year'].'-'.$cpf['first_month']));
            $cpf_last_date = date('Y-m',strtotime($cpf['last_year'].'-'.$cpf['last_month']));

            if($payroll_date >= $cpf_first_date && $payroll_date <= $cpf_last_date){
                $sg_cpf = $cpf;
                break;
            }
        }

        // if not updated the data yet, get the last one.
        $sg_cpf = empty($sg_cpf) ? $query_sg_cpf[count($query_sg_cpf)-1] : $sg_cpf;

        // handle deductions
        $data['total_deductions'] = 0;

        //check if there is any deductions chosen
        if (!empty($data['deduction_title']) AND count($data['deduction_title']) > 0) {

            foreach($data['deduction_title'] as $k => $deduction_id){

                //if deduction_id meaning not cloned

                if ($deduction_id != '') {

                    //need to check if employee is in the CHOSEN LIST OF Employees DEDUCTIONS for this batch of employee titles
                    if(in_array ($employee_id, $data['employees_deductions_'.$k])){

                        //get deduction record
                        $deduction_record = PayrollDeduction::find($deduction_id);
                        //WE ONLY CONSIDER AND ADD CPF DEDUCTABLE DEDUCTION RECORD
                        if($deduction_record->cpf_deductible == 1){

                            //INCREMENT TOTAL DEDUCTIONS NOW
                            $data['total_deductions'] += !empty($data['deduction_amount_'.$k])?$data['deduction_amount_'.$k]:0;
                        }
                    }

                }
            }

        }


        // handle additions
        $data['total_additions'] = 0;
        $data['total_additions_other_allowance_no'] = 0;
        $data['total_additions_other_allowance_yes'] = 0;

        //check if there is any additions chosen
        if (!empty($data['addition_title']) AND count($data['addition_title']) > 0) {

            //loop on chosen additions
            foreach($data['addition_title'] as $k => $addition_id){

                //if addition_id meaning not cloned
                if ($addition_id != '') {

                    //need to check if employee is in the CHOSEN LIST OF Employees addition for this batch of employee titles
                    if(in_array ($employee_id, $data['employees_additions_'.$k])){

                        //get addition record
                        $addition_record = PayrollAddition::find($addition_id);

                        //we only add CPF PAYABLE ones
                        if($addition_record->cpf_payable == 1 ){

                            //INCREMENT TOTAL ADDITIONS NOW
                            $data['total_additions'] += !empty($data['addition_amount_'.$k])?$data['addition_amount_'.$k]:0;


                                if($addition_record->other_allowance == 1){

                                    $data['total_additions_other_allowance_yes'] += !empty($data['addition_amount_'.$k])?$data['addition_amount_'.$k]:0;

                                }elseif($addition_record->other_allowance == 0){

                                    $data['total_additions_other_allowance_no'] += !empty($data['addition_amount_'.$k])?$data['addition_amount_'.$k]:0;

                                }

                        }
                    }
                }
            }

        }


        $overtime1 = !empty($data['overtime1'][$employee_id])?$data['overtime1'][$employee_id]:0;
        $overtime2 = !empty($data['overtime2'][$employee_id])?$data['overtime2'][$employee_id]:0;

        #OW == basic pay (or daily x nb days or hourly x nb of hours) + all overtime pay
        #+ [all additions that have "other allowance" = No (in admin settings)] - deductions

        #AW = other additions (where “other allowance” = Yes in admin settings)


        $basic_pay_comp = 0;
        $overtime_comp = 0;

        if($emp_data['fee_frequency'] == 'monthly'){

            // OW = BASIC PAY + OVERTIME + ADDITION OTHER ALLOWANCE == NO - DEDUCTIONS

            $basic_pay_comp = ($emp_data['basic_pay']*1);

            $overtime_comp = ($emp_data['overtime_1_rate']*$overtime1) + ($emp_data['overtime_2_rate']*$overtime2);

            $ow =  $basic_pay_comp + $overtime_comp + $data['total_additions_other_allowance_no'] - $data['total_deductions'];

            // AW = TOTAL_ADDITIONS eligible to CPF AND OTHER ALLOWANCE == YES
            $aw = $data['total_additions_other_allowance_yes'];

        }elseif($emp_data['fee_frequency'] == 'daily'){

            $basic_pay_comp = ($data['daily_rate'][$employee_id]*$data['days'][$employee_id]);

            $overtime_comp = ($emp_data['overtime_1_rate']*$overtime1) + ($emp_data['overtime_2_rate']*$overtime2);

            // OW = BASIC PAY + OVERTIME + ADDITION OTHER ALLOWANCE == NO - DEDUCTIONS
            $ow =  $basic_pay_comp + $data['total_additions_other_allowance_no'] + $overtime_comp - $data['total_deductions']; // need to add days worked if daily rate

            // AW = TOTAL_ADDITIONS eligible to CPF AND OTHER ALLOWANCE == YES
            $aw = $data['total_additions_other_allowance_yes'];

        }elseif($emp_data['fee_frequency'] == 'hourly'){

            $basic_pay_comp = ($data['hourly_rate'][$employee_id]* $data['hours'][$employee_id]);

            $overtime_comp = ($emp_data['overtime_1_rate']*$overtime1) + ($emp_data['overtime_2_rate']*$overtime2);

            // OW = BASIC PAY + OVERTIME + ADDITION OTHER ALLOWANCE == NO - DEDUCTIONS
            $ow =  $basic_pay_comp + $overtime_comp + $data['total_additions_other_allowance_no'] - $data['total_deductions'];

            // AW = TOTAL_ADDITIONS eligible to CPF AND OTHER ALLOWANCE == YES
            $aw = $data['total_additions_other_allowance_yes'];

        }

        // in CPF computation, OW is capped (G in the DB)
        if($ow > $sg_cpf['ow_ceiling'] ){
            $CAPPED_OW = $sg_cpf['ow_ceiling'];
        }else{
            $CAPPED_OW = $ow;
        }

        // computation of total wage
        $tw = $ow+$aw;

        $return['OW'] = $ow;
        $return['AW'] = $aw;

        // CPF calculation
        if($emp_data['cpf_entitlement'] == 1){

            if($tw <= $sg_cpf['threshold1']){

                $return['CPF_EMPLOYEE'] = 0;
                $return['TOTAL_CPF'] = 0;

            }elseif($tw <= $sg_cpf['threshold2']){

                $percent = str_replace('%', '', $sg_cpf['employer_rate']) / 100;
                $return['CPF_EMPLOYEE'] = 0;
                $return['TOTAL_CPF'] = $percent * $tw;

            }elseif($tw <= $sg_cpf['threshold3']){

                $discount_rate = floatval($sg_cpf['discount_rate']);
                $employer_rate = str_replace('%', '', $sg_cpf['employer_rate']) / 100;
                $return['CPF_EMPLOYEE'] = $discount_rate * ($tw - $sg_cpf['threshold2']);
                $return['TOTAL_CPF'] = $employer_rate * $tw + $discount_rate * ($tw - $sg_cpf['threshold2']);

            }else{

                $employer_rate = str_replace('%', '', $sg_cpf['employer_rate']) / 100;
                $employee_rate = str_replace('%', '', $sg_cpf['employee_rate']) / 100;

                $return['CPF_EMPLOYEE'] = $employee_rate * $CAPPED_OW + $employee_rate * $aw;
                $return['TOTAL_CPF'] = ($employer_rate + $employee_rate) * $CAPPED_OW + ($employer_rate + $employee_rate) * $aw;

            }

            $return['CPF_EMPLOYEE'] = floor($return['CPF_EMPLOYEE']);
            $return['TOTAL_CPF'] = round($return['TOTAL_CPF']);
            $return['CPF_EMPLOYER'] = $return['TOTAL_CPF'] - $return['CPF_EMPLOYEE'];

        }else{
            $return['CPF_EMPLOYEE'] = 0;
            $return['CPF_EMPLOYER'] = 0;
            $return['TOTAL_CPF'] = 0;
        }

        return $return;

    }


    /*
     * formula reference
     *
    public function cpf_computation()
    {

//        $post = Input::all();
        $post['ordinary_wages'] = 4000;
        $post['additional_wages'] = 500;

        $total_wage = $post['ordinary_wages']+$post['additional_wages'];

        // Calculation of CPF contributions

        // Total CPF Contribution
        $data['cpf_contribution'] = $total_wage*.37;

        // Employee�s Share of CPF Contribution
        $data['employee_share'] = floor($total_wage*.20);

        $data['contribution'] = $data['cpf_contribution']-$data['employee_share'];

        // Medisave Account
        $data['medisave_account'] = round(0.2162*$data['cpf_contribution'], 2);

        // Special Account
        $data['special_account'] = round(0.1621*$data['cpf_contribution'], 2);

        // Ordinary Account

        $data['ordernary_account'] = $data['cpf_contribution']-($data['special_account']+$data['medisave_account']);

        return $data;
    }
    **/



}
