<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Listeners\PushNotificationEventListener;


use App\User;
use App\Company;
use App\Company_user;
use App\Roles;
use App\User_roles;
use App\User_teams;


use Auth;
use Input;
use Redirect;

use Illuminate\Mail\Mailer;
use Mail;
use Config;

use Excel;

class DirectoryController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $input = Input::all();

        $user = Auth::user();

        $company = Company::find( session('current_company')->id );

        $admins = $company->administrator_users();

        $roles = $company->roles;

        $teams = $company->teams;

        $search = (isset($post['search']))?$post['search']:'';

        $employees = User::select('users.*','company_user.is_master','company_user.is_guest')
            ->join('company_user', 'users.id','=','company_user.user_id')
            ->join('company', 'company_user.company_id','=','company.id')
           # ->where('company_user.is_master',0)
            ->where('company_user.company_id',$company->id);

        #Model::where('column', 'LIKE', '%value%')->get();

        if(!empty($input['type']) AND !empty($input['key'])){

            $key = $input['key'];
            $type = $input['type'];

            switch($type){

                case 'name':
                    $employees = $employees->whereRaw("concat(first_name, ' ', last_name) like '%$key%' ");

                break;
                case 'role':

                    $employees = $employees
                    ->join('user_roles', 'users.id','=','user_roles.user_id')
                    ->join('roles', 'user_roles.role_id','=','roles.id')
                    ->whereRaw("roles.role_name like '%$key%' ");

                break;
                case 'team':

                    $employees = $employees
                        ->join('user_teams', 'users.id','=','user_teams.user_id')
                        ->join('teams', 'user_teams.team_id','=','teams.id')
                        ->whereRaw("teams.team_name like '%$key%' ");

                break;
                case 'title':
                    $employees = $employees->whereRaw("users.title like '%$key%' ");
                break;

            }
        }

        $employees = $employees->search($search)->get();

        $employees = paginateCollection($employees, config('app.page_count'));

        $employees->appends(['search' => $search])->render();

        $completed = FALSE;
        if(!empty($input['completed'])){
            $completed = TRUE;
        }


//        return view('dashboard.directory.directory_manager2',compact('employees','roles','teams','search'));
        return view('dashboard.directory.directory_manager',compact('employees','roles','teams','search','completed'));

    }

    function employee_assign_roles_save()
    {
        $all_array = [];

        $post = Input::all();

        if($post){

            $user_ids = explode(',',rtrim($post['user_ids'],','));

            if(count($user_ids) AND array_key_exists('roles',$post) AND count($post['roles'])){

                foreach($user_ids as $user_id){

                    if($user_id){

                        $user_rooms_arr = [];

                        $company_id = DB::table('company_user')->where('user_id', $user_id)->value('company_id');

                        DB::table('user_roles')->where('user_id', $user_id)->delete();

                        foreach($post['roles'] as $role_id){

                            $user_role = User_roles::create([
                                'user_id' => $user_id,
                                'role_id' => $role_id
                            ]);
                            
                            array_push($user_rooms_arr, 'company_'.$company_id.'_role_'.$role_id);
                            
                        }

                        // Push Roles Assignment To Node Server
                        $array = array(
                            'user_id'        => $user_id,
                            'event'          => Config::get('constants.EVENT_DIRECTORY.ASSIGN_ROLE'),
                            'role_id'        => $post['roles'],
                            'assigned_rooms' => $user_rooms_arr
                        );

                        array_push($all_array, $array);
                    }
                }

                if($_SERVER['SERVER_NAME'] != 'zenintra.local'){

                    $push_notification = new PushNotificationEventListener();
                    $push_notification->pushNotification($all_array);

                }

                return Redirect::to('directory')->withInput()->with('success', 'Successfully Assigned Role!');

            }
        }
    }

    function employee_assign_teams_save(){

        $all_array = [];

        $post = Input::all();

        if($post){

            $user_ids = explode(',',rtrim($post['user_ids'],','));
            if(count($user_ids) AND array_key_exists('teams',$post) AND count($post['teams'])){

                foreach($user_ids as $user_id){

                    if($user_id){

                        $user_rooms_arr = [];

                        $company_id = DB::table('company_user')->where('user_id', $user_id)->value('company_id');

                        DB::table('user_teams')->where('user_id', $user_id)->delete();

                        foreach($post['teams'] as $team_id){

                            $user_team = User_teams::create([
                                'user_id' => $user_id,
                                'team_id' => $team_id
                            ]);

                            array_push($user_rooms_arr, 'company_'.$company_id.'_team_'.$team_id);
                        }

                        $array = array(
                            'user_id'        => $user_id,
                            'event'          => Config::get('constants.EVENT_DIRECTORY.ASSIGN_TEAM'),
                            'team_id'        => $post['teams'],
                            'assigned_rooms' => $user_rooms_arr
                        );

                        array_push($all_array, $array);
                    }
                }


                if($_SERVER['SERVER_NAME'] != 'zenintra.local'){
                    $push_notification = new PushNotificationEventListener();
                    $push_notification->pushNotification($all_array);

                }




                return Redirect::to('directory')->withInput()->with('success', 'Successfully Assigned Team!');

            }
        }
    }


    function resend_invitation($user_id = NULL){

        if(!$user_id) return response('User ID not found',404);

        $user = User::find($user_id);

        if(!$user) return response('User no found', 404);

        $company = $user->company_user->first();

        $master_user = $company->master_user();

        //userid current + bcript of time
        $hash = $user->invite_hash;

        $url = url('/accept_invite/'.$hash);
        $link = '<a href="'.$url.'" >'.$url.'</a>';

        $username = $user->username;
        $company_name = $company['company_name'];
        $your_username = $user->email;
        $invitee_name = $user->first_name.' '.$user->last_name;
        $invitee_first_name = $user->first_name;
        $invitor_name = $master_user->first_name .' '.$master_user->last_name;

        $subject = 'Invitation to join '.$company_name.' on Zenintra';

        $to_name = $your_username;

        Mail::send('emails.invite_user', compact('username','company_name','your_username','link','invitor_name','invitee_first_name'), function($message)
        use($subject,$invitee_name,$user)
        {
            $message->subject($subject);
            $message->to($user->email, $invitee_name);
        });

        return Redirect::to('directory')->withInput()->with('success', 'Successfully Invited User!');

    }

    function user_view($user_id = NULL){

        if(!$user_id) return response('User ID not found',404);

        $view_user = User::find($user_id);

        if(!$view_user) return response('User no found', 404);

        $company = $view_user->company_user->first();

        $user_fields = $company->user_fields->toArray();

        $custom_keypairs_array = [];

        if($view_user['custom_keypairs']!=''){
            $custom_keypairs_array = json_decode($view_user['custom_keypairs'],TRUE);
        }

        return view('dashboard.directory.view_user',compact('view_user','user_fields','custom_keypairs_array'));

    }

    function user_delete_front($user_id){

        if(!$user_id) return response('User ID not found',404);

        $user = User::find($user_id);

        if(!$user) return response('User no found', 404);

        // Push Notifications to Node Server Before Deleting
        $rooms_arr = [];

        $company_ids = DB::table('company_user')->where('user_id', $user_id)->lists('company_id');
        $user_teams = DB::table('user_teams')->where('user_id', $user_id)->lists('team_id');
        $user_roles = DB::table('user_roles')->where('user_id', $user_id)->lists('role_id');

        foreach($company_ids as $comp_id) {
            foreach($user_teams as $team) {
                array_push($rooms_arr, 'company_'.$comp_id.'_team_'.$team);
            }

            foreach($user_roles as $role) {
                array_push($rooms_arr, 'company_'.$comp_id.'_role_'.$role);
            }
        }

        $array = array([
            'user_id'       => $user_id,
            'event'         => Config::get('constants.EVENT_DIRECTORY.DELETE_DIRECTORY'),
            'deleted_rooms' => $rooms_arr
        ]);

        if($_SERVER['SERVER_NAME'] != 'zenintra.local') {
            $push_notification = new PushNotificationEventListener();
            $push_notification->pushNotification($array);
        }

        //Delete Main Record
        $user->delete();

        //REMOVE necesarry records to be deleted

        //user teams/user roles
        DB::table('user_teams')->where('user_id',$user_id)->delete();
        DB::table('user_roles')->where('user_id',$user_id)->delete();

        //company_user
        DB::table('company_user')->where('user_id',$user_id)->delete();

        return Redirect::to('directory')->withInput()->with('success', 'Successfully Deleted User!');
    }

    public function user_import(){
        return view('dashboard.directory.user_import');
    }

    public function user_import_process()
    {
        $datas = Excel::load($_FILES['upload']['tmp_name'])->get()->toArray();

        if(!$datas) {

         return Redirect::to('directory')->withInput()->with('error', 'Error on Import!');

        }

        $user = Auth::user();

        $company = $user->company_user->first();

        $all_push_arr = [];
        $push_imported_rooms = []; 
        $roles_arr   = [];
        $teams_arr   = [];

        if($datas){

            $imported=0;

            foreach($datas as $data){

                if($data['email'] != ''){

                    $check_user = DB::table('users')->where('email',$data['email'])->first();

                    if(!($check_user)){

                        $imported++;

                        //userid current + bcript of time
                        $hash = $user['id'].'-'.md5(time());

                        $invitee_name = $data['first_name'] .' ' . $data['last_name'];

                        $new_user = User::create([
                            'email' => $data['email'],
                            'first_name' => $data['first_name'],
                            'last_name' => $data['last_name'],
                            'user_level' => 'employee',
                            'position' => $data['position'],
                            'phone_number' => $data['phone_number'],
                            'invite_hash' => $hash,
                            'invited_by' => $user['id'],
                            'invited_on' => date('Y-m-d H:i:s')
                        ]);

                        $user_company_mapping = Company_user::create(
                            [
                                'company_id' => $company->id,
                                'user_id' => $new_user->id,
                                'is_master'=> 0
                            ]
                        );

                        $roles = DB::table('roles')->where('company_id',$company->id)->get();

                        $role_id = '';

                        foreach($roles as $role) {
                            if($role->role_name == 'Employee') { 
                                $role_id = $role->id;

                                array_push($roles_arr, $role_id);
                                array_push($push_imported_rooms, 'company_'.$company_id.'_role_'.$role_id);
                            }
                        }

                        if($role_id){
                            $user_role = User_roles::create([
                                'user_id' => $new_user->id,
                                'role_id' => $role_id
                            ]);
                        }


                        $teams = DB::table('teams')->where('company_id',$company->id)->get();

                        $team_id = '';

                        foreach($teams as $team) {
                            $team_id = $team->id;

                            array_push($teams_arr, $team_id);
                            array_push($push_imported_rooms, 'company_'.$company_id.'_team_'.$team_id);
                        }

                        if($team_id){
                            $user_role = User_teams::create([
                                'user_id' => $new_user->id,
                                'role_id' => $team_id
                            ]);
                        }

                        $array = array(
                            'user_id'       => $new_user->id,
                            'event'         => Config::get('constants.EVENT_DIRECTORY.ADD_DIRECTORY'),
                            'invitee_name'  => $new_user->first_name .' '. $new_user->last_name,
                            'invitee_email' => $new_user->email,
                            'invitee_roles' => $rooms_roles_arr,
                            'invitee_teams' => $rooms_teams_arr,
                            'rooms'         => $push_imported_rooms
                        );

                        array_push($all_push_arr, $array);


                        $url = url('/accept_invite/'.$hash);
                        $link = '<a href="'.$url.'" >'.$url.'</a>';

                        $username = $user->username;
                        $company_name = $company->company_name;
                        $your_username = $new_user->email;
                        $invitor_name = $user->first_name.' '.$user->last_name;
                        $invitee_first_name = $new_user->first_name;

                        $subject = 'Invitation to join '.$company_name.' on Zenintra';

                        $to_name = $your_username;

                        Mail::send('emails.invite_user', compact('username','company_name','your_username','link','invitor_name','invitee_first_name'), function($message)
                        use($subject,$invitee_name,$new_user)
                        {
                            $message->subject($subject);
                            $message->to($new_user->email, $invitee_name);
                        });

                    }
                }
            }

            

            if($imported==0){
                return Redirect::to('directory')->withInput()->with('error', 'No Imported Records!');
            } else {
                // Push Imported Directories To Node Server

                if ($_SERVER['SERVER_NAME'] != 'zenintra.local') {

                    $push_notification = new PushNotificationEventListener();
                    $push_notification->pushNotification($all_push_arr);

                }

                return Redirect::to('directory')->withInput()->with('success', 'Successfully Imported '.$imported.' User(s)!');
            }

        }

    }

    public function edit_user($id,Request $request)
    {
        $edited_roles = []; $edited_teams = []; $rooms_arr =[];

        $edit_user = User::find($id);

        if(!$edit_user) return response('User not found', 404);

        $company = $edit_user->company_user->first();

        $roles = $company->roles;

        $teams = $company->teams;

        $post = Input::all();

        if($post){

            $this->validate($request, ['email' => 'required']);

            if(!empty($post['roles']) AND count($post['roles']) > 0){

                DB::table('user_roles')->where('user_id', $edit_user->id)->delete();

                foreach($post['roles'] as $role_id){

                    $user_role = User_roles::create([
                        'user_id' => $edit_user->id,
                        'role_id' => $role_id
                    ]);

                    array_push($edited_roles, $role_id);
                    array_push($rooms_arr, 'company_'.$company->id.'_role_'.$role_id);

                }

            } 

            if(!empty($post['teams']) AND count($post['teams']) > 0){
                DB::table('user_teams')->where('user_id', $edit_user->id)->delete();

                foreach($post['teams'] as $team_id){

                    $user_team = User_teams::create([
                        'user_id' => $edit_user->id,
                        'team_id' => $team_id
                    ]);

                    array_push($edited_teams, $team_id);
                    array_push($rooms_arr, 'company_'.$company->id.'_team_'.$team_id);

                }
            }

            $update = [
                'email' => $post['email'],
                'title' => $post['title'],
                'first_name' => $post['first_name'],
                'last_name' => $post['last_name'],
                'notes'=>$post['notes']
            ];

            DB::table('users')
                ->where('id',$edit_user->id)
                ->update($update);

            // Push Notifications To Node Server
            $array = array([
                'user_id'      => $edit_user->id,
                'event'        => Config::get('constants.EVENT_DIRECTORY.EDIT_DIRECTORY'),
                'email'        => $post['email'],
                'title'        => $post['title'],
                'user_name'    => $post['first_name'] .' '. $post['last_name'],
                'roles'        => $edited_roles,
                'teams'        => $edited_teams,
                'edited_rooms' => $rooms_arr
            ]);


            if($_SERVER['SERVER_NAME'] != 'zenintra.local') {

                $push_notification = new PushNotificationEventListener();
                $push_notification->pushNotification($array);

            }

            return Redirect::to('directory_edit_user/'.$edit_user->id)->withInput()->with('success', 'Successfully Updated!');

        }

        return view('dashboard.directory.edit_user',compact('roles','teams','edit_user'));

    }

    function export_directory(){

         Excel::create('Zenintra Directory Data Export '.date('m/d/Y'), function ($excel) {

            $excel->sheet('companies', function ($sheet) {

                $company = Company::find( session('current_company')->id );

                $employees = User::select('users.first_name','users.last_name','users.title','users.position','users.phone_number','users.notes')
                ->join('company_user', 'users.id','=','company_user.user_id')
                ->join('company', 'company_user.company_id','=','company.id')
                ->where('company_user.company_id',$company->id)
                    ->get();

                $sheet->fromArray($employees, null, 'A1', false, TRUE);
            });
        })->download('csv');



    }

}
