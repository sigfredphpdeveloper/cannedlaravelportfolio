<?php

namespace App\Http\Controllers;

use App\Company_user;
use App\Tasks;
use App\Tasks_categories;
use App\Tasks_list;
use App\Teams;
use App\User;
use Illuminate\Http\Request;


use Auth;
use DB;
use Illuminate\Support\Facades\Input;
use Response;
use Redirect;
use App\Board;
use App\Board_teams;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class BoardsController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $is_admin = $user->isAdmin();
        $company = session('current_company');

        $boards = Board::where('boards.company_id', $company->id)->where('visibility','!=', 'deleted')->get()->toArray();

        if(!$is_admin){
            $boards = $this->board_visibility($boards);
        }

        $teams = Teams::where('company_id',session('current_company')->id)->get()->toArray();

        return view('dashboard.board.index', compact('boards', 'teams'));
    }

    private function board_visibility($boards){
        $user_id = Auth::user()->id;
        $teams = Auth::user()->team($user_id);
        $team_member = array();
        foreach($teams as $team){
            $team_member[] = $team['team_id'];
        }

        foreach($boards as $key => $board){
            if($board['visibility'] == 'team'){
                $board_team = Board_teams::where('board_id',$board['id'])->get()->toArray();

                if(count($board_team)>1){
                    $btm = array();
                    foreach($board_team as $bt){
                        $btm[] = $bt['team_id'];
                    }
                    if(!array_intersect($team_member,$btm)){ unset($boards[$key]); }

                }elseif(empty($board_team['team_id']) || !in_array($board_team['team_id'],$team_member)){
                    unset($boards[$key]);
                }
            }elseif($board['visibility'] == 'me'){
                if($board['owner_id'] != $user_id){
                    unset($boards[$key]);
                }
            }
        }

        return $boards;
    }
}
