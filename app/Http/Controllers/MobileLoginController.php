<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use DB;
use Hash;
use Validator;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Listeners\MobileAuthLoginEventListener;
use Config;

use App\MobileUser;
use App\User;
use App\User_roles;
use App\User_teams;
use App\Company_user;

use App\Announcement_roles;
use App\Announcement_teams;
use App\Announcements;


class MobileLoginController extends Controller
{
    /**
     * Login function for different platforms
     * 
     * @param Request $request
     * @return \Illuminate\Http\Response
     */

    public function login(Request $request)
    {
        // Get request sent from App
        $method = $request->method();

        if($request->isMethod('post')) {
            // Input validation
            $validator = Validator::make($request->all(), [
                'username' => 'required|email',
                'password' => 'required',
                'platform' => 'required'
            ])->fails();

            // Check validator
            if($validator) {
                return response()->json([
                    'error' => 'Missing required field(s)'
                ]);
            } else {

                //Find user matching input
                $user = User::where('email', $request->input("username"))->first();
                $company = Company_user::where('user_id', $user->id)->first();

                if(empty($user)){
                    return response()->json([
                        'error' => 'User not found'
                    ]);
                }

                if (!Hash::check($request->input('password'), $user->password)) {
                    return response()->json([
                        'error' => 'Incorrect password'
                    ]);
                }
                //Check if user has any active tokens matching the platform
                //If no tokens, issue new one
                $token = $user->findOrCreateToken($request->input('platform'));
                $rooms = $this->updateUserRoom($user);

                return response()->json([
                    'token' => $token->toJWT($user),
                    'user_id' => $user->id,
                    'company_id' => $company->company_id,
                    'rooms' => $rooms,
                    'event' => array_merge(Config::get('constants.EVENT_ANNOUNCEMENT'),Config::get('constants.EVENT_DIRECTORY'))
                ]);
            }
        }
        else {
            return response()->json([
                'error' => 'POST request is required'
            ]);
        }
    }


    /**
     * Get all the user's roles & teams
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */

    public function getAllAnnouncementsAndDirectories(Request $request) 
    {
        $user_role_announcements = []; 
        $user_team_announcements = [];
        $all_announcements       = array();
        $all_ann_ids             = array();
        $all_directories         = array();
        
        $user = $request->user;
        $role_announcements = $user->user_roles()->with('announcements')->get();
        $team_announcements = $user->user_teams()->with('announcements')->get();

        $all_directories = array_add($all_directories, 'user_roles', $role_announcements->pluck('id'));
        $all_directories = array_add($all_directories, 'user_teams', $team_announcements->pluck('id'));
        $all_directories = array_add($all_directories, 'user_id', $user->id);

        foreach ($role_announcements as $role) {
            $all_ann_ids = array_merge($all_ann_ids, $role->announcements->pluck('id')->all());
        }

        foreach ($team_announcements as $ream) {
            $all_ann_ids = array_merge($all_ann_ids, $ream->announcements->pluck('id')->all());
        }

        $all_announcements = $this->retrieveAnnouncementsEach(array_unique($all_ann_ids));

        return response()->json([
            'announcements_array' => $all_announcements,
            'directories_array'   => $all_directories
        ]);
    }

    /**
     * Announcements Retrival
     *
     * @param array
     * @return array
     */

    function retrieveAnnouncementsEach($param_arr) 
    {
        $all_announcements = array();

        foreach($param_arr as $ann_id) {
            $ann_row = Announcements::find($ann_id);
            $creator_id = $ann_row->user_id;

            $creator_row = User::find($creator_id);
            $creator_fullname = $creator_row->first_name.' '.$creator_row->last_name;

            $array = array(
                'announcement' => $ann_row->message,
                'creator' => $creator_fullname,
                'title' => $ann_row->title,
                'updated_date' => $ann_row->updated_at
            );

            array_push($all_announcements, $array);
        }

        return $all_announcements;
    }


    /**
     *  Update user's room for everytime login
     * 
     *  @param MobileUser $user_object
     *  @return array
     */

    function updateUserRoom(User $user) 
    {
        $rooms_array = [];
        foreach ($user->user_roles as $role) {
            $room = 'company_'.$role->company_id.'_role_'.$role->id;
            array_push($rooms_array, $room);
        }

        foreach($user->user_teams as $team) {
            $room = 'company_'.$team->company_id.'_team_'.$team->id;
            array_push($rooms_array, $room);
        }
        return $rooms_array;
    }
}