<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Filesystem\Filesystem;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Config;
use Auth;
use Input;
use Redirect;

use App\User;
use App\Company;
use App\Roles;
use App\Content;
use App\Content_permissions;
use DB;
use AWS;
use Storage;
use App\Classes\Helper;

use Intervention\Image\Facades\Image;

class FilesController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function manager($parent_content_id = 0){

        $input = Input::all();

        $user = Auth::user();

        $company = $user->company_user->first();

        $roles = $user->user_roles->toArray();

        $is_admin = FALSE;

        if(isset($company) AND !empty($company['pivot']['is_master']) AND $company['pivot']['is_master'] ==1){
            $is_admin = TRUE;
        }

        $user_roles_id = [];

        foreach($roles as $role){
            array_push($user_roles_id,$role['id']);
        }

        //Contents Based on Folder ID
        $contents = Content::select('content.*','users.email','users.first_name','users.last_name')
            ->where('company_id',$company['id'])
            ->where('parent_content_id',$parent_content_id)
            ->join('users', 'users.id','=','content.owner_id')
            ->orderBy('content_type','ASC');

        $search_key = '';

        if(!empty($input['key']) ){

            $key = $input['key'];

            $contents = $contents->whereRaw("content_name like '%$key%' ");

            $search_key = '&key='.$input['key'];

        }

        $caret_name = 'down';
        $caret_size = 'down';
        $caret_date = 'down';

        $order = 'desc';

        if(!empty($input['order']) ){

            if($input['order'] == 'desc'){
                $order = 'asc';
                $caret_name = 'up';

            }else{
                $order = 'desc';
                $caret_name = 'down';
            }

            if(!empty($input['sort'])){

                switch($input['sort']){

                    case 'name':
                        $contents = $contents->orderBy('content_name',strtoupper($input['order']));
                        break;

                    case 'size':
                        $contents = $contents->orderBy('file_size',strtoupper($input['order']));
                        break;

                    case 'date':
                        $contents = $contents->orderBy('created_at',strtoupper($input['order']));
                        break;

                }

            }


        }else{

            $contents = $contents->orderBy('content_name','ASC');

        }

        $next = $parent_content_id==0?'':$parent_content_id;
        $sort_name = url('/files_manager').'/'.$next.'?sort=name&order='.$order.$search_key;
        $sort_size = url('/files_manager').'/'.$next.'?sort=size&order='.$order.$search_key;
        $sort_date = url('/files_manager').'/'.$next.'?sort=date&order='.$order.$search_key;
        $action_url = url('/files_manager').'/'.$next.'?'.http_build_query($_GET);

        $is_root = FALSE;

        if($parent_content_id == '' OR $parent_content_id == 0 ){
            $is_root = TRUE;
        }

        $contents = $contents->get()->toArray();

        foreach($contents as $i=>$content){

            //size logic
            $size = 0;
            $files_nodes = Content::where('parent_content_id',$content['id'])->get()->toArray();
            foreach($files_nodes as $node){
                $size = $size + $node['file_size'];
            }

            if($content['content_type']==0){//folders
                $contents[$i]['size'] = $size;
            }else{
                $contents[$i]['size'] = $content['file_size'];
            }

            //FILES
            //processing of files whether root or non-root
            if($is_root){ //means on root folder

                if($content['content_type'] == '1'){ //FILES

                    //FILES need to be different cause on root files cant be deleted when you're not ownr
                    //if root only admin and owner can edit
                    if($user['id'] == $content['owner_id'] OR $is_admin == TRUE){
                        $contents[$i]['read_access'] = 1;
                        $contents[$i]['write_access'] = 1;
                        $contents[$i]['no_access'] = 0;
                    }else{ // not your file so no delete but can read
                        $contents[$i]['read_access'] = 1;
                        $contents[$i]['write_access'] = 0;
                        $contents[$i]['no_access'] = 0;
                    }
                }

            }else{ //non root process files

                if($content['content_type'] == '1'){

                    //get folder permission of current File under the folder cause we are not on the root already
                    $folder_permission = Content_permissions::where('content_id',$parent_content_id)->get()->toArray();

                    $read = 0;
                    $write = 0;
                    $no_access = 0;

                    foreach($folder_permission as $permission){

                        if(in_array($permission['role_id'],$user_roles_id) AND $permission['read_access']=='1'){
                            $read = 1;
                        }
                        if(in_array($permission['role_id'],$user_roles_id) AND $permission['write_access']=='1'){
                            $write = 1;
                        }
                        if(in_array($permission['role_id'],$user_roles_id) AND $permission['no_access']=='1'){
                            $no_access = 1;
                        }

                    }

                    if($user['id'] == $content['owner_id'] OR $is_admin == TRUE){// if admin and if folder
                        $contents[$i]['read_access'] = 1;
                        $contents[$i]['write_access'] = 1;
                        $contents[$i]['no_access'] = 0;
                    }else{
                        $contents[$i]['read_access'] = $read;
                        $contents[$i]['write_access'] = $write;
                        $contents[$i]['no_access'] = $no_access;
                    }
                }
            }


            // FOLDERS
            if($content['content_type'] == '0'){

                //if root show all files but not all folders
                //Check if content folders should be
                $folder_permission = Content_permissions::where('content_id',$content['id'])->get()->toArray();

                $read = 0;
                $write = 0;
                $no_access = 0;

                foreach($folder_permission as $permission){

                    if(in_array($permission['role_id'],$user_roles_id) AND $permission['read_access']=='1'){
                        $read = 1;
                    }
                    if(in_array($permission['role_id'],$user_roles_id) AND $permission['write_access']=='1'){
                        $write = 1;
                    }
                    if(in_array($permission['role_id'],$user_roles_id) AND $permission['no_access']=='1'){
                        $no_access = 1;
                    }

                }

                if($user['id'] == $content['owner_id'] OR $is_admin == TRUE){// if OWNER or is ADMIN
                    $contents[$i]['read_access'] = 1;
                    $contents[$i]['write_access'] = 1;
                    $contents[$i]['no_access'] = 0;
                }else{
                    $contents[$i]['read_access'] = $read;
                    $contents[$i]['write_access'] = $write;
                    $contents[$i]['no_access'] = $no_access;
                }

            }

        }

        $breadcrumb = '';

        $roles = Roles::all()->where('company_id',$company['id'])->toArray();

        $current_content = [];

        if($parent_content_id==0){
            $current_content['content_name'] = '';
        }else{
            $current_content = Content::find($parent_content_id);
        }

        $role = $user->user_roles->first()->toArray();

        $team = $user->user_teams->first();

        $file_breadcrumb = $this->extract_breadcrumb($parent_content_id);

        
        return view('dashboard.file_sharing.file_manager',
        compact('roles','contents','parent_content_id','current_content','file_breadcrumb','role','team','current_folder',
        'sort_name','caret_name','action_url','caret_size','sort_size','sort_date','caret_date','is_root'
        ));

    }

    public function settings(){

        $user = Auth::user();

        $company = $user->company_user->first();

        $total_size = 0;

        $company_files = Content::where('content_type',1)->where('company_id',$company['id'])->get()->toArray();

        foreach($company_files as $file){

            $total_size = $total_size + $file['file_size'];

        }

        return view('dashboard.file_sharing.settings',compact('total_size'));

    }

    public function settings_save(){

        $user = Auth::user();

        $company = $user->company_user->first()->toArray();

        $post = Input::all();

        if ($post) {

            $save_company = Company::find($company['id']);

            $save_company->file_sharing = (array_key_exists('file_sharing',$post) AND $post['file_sharing'] == '1')?1:0;

            $save_company->save();

            return Redirect::to('file_settings')->withInput()->with('success', 'Successfully Updated!');

        }

    }

    function edit_folder($parent_content_id = NULL,$content_id = 0){


        if($content_id!=0){
            $current_content = Content::find($content_id);
        }

        $user = Auth::user();

        $company = $user->company_user->first();

        $content_permissions = DB::table('content_permissions')->where('content_id',$content_id)->get();

        $roles = Roles::all()->where('company_id',$company['id'])->toArray();
        foreach($roles as $i => $role){

            //roles_read roles_write roles_no_access

            $where = ['content_id'=>$content_id,'role_id'=>$role['id']];

            $content_permissions = DB::table('content_permissions')->where($where)->first();

            if($content_permissions){

                $permission_array = array(
                    'read_access' => $content_permissions->read_access,
                    'write_access' => $content_permissions->write_access,
                    'no_access' => $content_permissions->no_access
                );
            }else{
                $permission_array = array(
                    'read_access' => 0,
                    'write_access' => 0,
                    'no_access' => 0
                );

            }


            $roles[$i]['permission'] = $permission_array;

        }


        echo view('dashboard.file_sharing.edit_folder',compact('roles','current_content','content_id','parent_content_id'));

    }




    function edit_folder_save($parent_content_id = NULL,$content_id = NULL){

        $post = Input::all();

        if ($post) {
            $user = Auth::user();

            $team = $user->user_teams->first();

            $team_id = 0;

            if($team){
                $team_id = $team['pivot']['team_id'];
            }

            $content = Content::find($content_id);

            $content->content_name = $post['content_name'];

            $content->save();

            $roles_ids = $post['roles_ids'];

            foreach($roles_ids as $key =>  $role){

                $read = 0;
                $write = 0;
                $no_access = 0;

                if(array_key_exists('read_access',$post) AND in_array($role,$post['read_access'])){
                    $read = 1;
                }
                if(array_key_exists('write_access',$post) AND in_array($role,$post['write_access'])){
                    $write = 1;
                }
                if(array_key_exists('no_access',$post) AND in_array($role,$post['no_access'])){
                    $no_access = 1;
                }

                $where = ['content_id'=>$content_id,'role_id'=>$role];


                $content_permission = DB::table('content_permissions')->where($where)->first();

                //no record on content_permissions yet so this must be because the role was added later on after content was created
                if(!($content_permission)){

                    $team = $user->user_teams->first();


                    $content_permissions = Content_permissions::create([
                        'content_id' => $post['content_id'],
                        'role_id' => $role,
                        'team_id' => $team_id,
                        'read_access' => $read,
                        'write_access'=>$write,
                        'no_access'=>$no_access
                    ]);
                }else{ //content permission found then update

                    $updates = ['read_access'=>$read,'write_access'=>$write,'no_access'=>$no_access];
                    DB::table('content_permissions')
                        ->where($where)
                        ->update($updates);
                }

            }

            return Redirect::to('files_manager/'.$post['parent_content_id'])->withInput()->with('success', 'Successfully Saved Folder!');

        }

    }


    function add_folder_save(Request $request){

        $user = Auth::user();

        $team = $user->user_teams->first();

        $team_id = 0;

        if($team){
            $team_id = $team['pivot']['team_id'];
        }

        $this->validate($request, ['content_name' => 'required']);

        $company = $user->company_user->first()->toArray();

        $post = Input::all();

        if ($post) {

            $content = Content::create([
                'company_id'=>$company['id'],
                'content_type'=>0,
                'content_name'=>$post['content_name'],
                'owner_id'=>$user['id'],
                'parent_content_id' => $post['parent_content_id']
            ]);

            $roles_ids = $post['roles_ids'];

            foreach($roles_ids as $key =>  $role){

                $read = 0;
                $write = 0;
                $no_access = 0;

                if(array_key_exists('read_access',$post) AND in_array($role,$post['read_access'])){
                    $read = 1;
                }

                if(array_key_exists('write_access',$post) AND in_array($role,$post['write_access'])){
                    $write = 1;
                }

                if(array_key_exists('no_access',$post) AND in_array($role,$post['no_access'])){
                    $no_access = 1;
                }

                $content_permissions = Content_permissions::create([
                    'content_id' => $content->id,
                    'role_id' => $role,
                    'team_id' => $team_id,
                    'read_access' => $read,
                    'write_access'=>$write,
                    'no_access'=>$no_access
                ]);

            }

            return Redirect::to('files_manager/'.$post['parent_content_id'])->withInput()->with('success', 'Successfully Saved Folder!');

        }

    }


    function upload_file($parent_content_id = 0){

        $current_folder = Content::find($parent_content_id);

        $user = Auth::user();

        $company = $user->company_user->first()->toArray();

        $role = $user->user_roles->first()->toArray();

        $team = $user->user_teams->first();

        return view('dashboard.file_sharing.upload_file',compact('role','team','current_folder','parent_content_id'));

    }

    function upload_file_process(Request $request){
       // $this->validate($request, ['file' => 'required|mimes:png,jpg,jpeg,gif,bmp,pdf,docx,doc,csv,xls,xlsx']);

        $user = Auth::user();

        $company = $user->company_user->first()->toArray();

        $post = Input::all();

        if ($post) {

            if($request->file('file') AND $request->file('file')->isValid()){

                $total_size = 0;

                $company_files = Content::where('content_type',1)->where('company_id',$company['id'])->get()->toArray();

                foreach($company_files as $file){
                    $total_size = $total_size + $file['file_size'];
                }

                $file_size = $_FILES['file']['size'];
                $original_file_name = $_FILES['file']['name'];

                $predicted = $total_size + $file_size;

                $formated_limit = Helper::formatBytes($company['file_capacity']);

                if($company['file_capacity'] < $predicted){ //means limit reached already

                    header("HTTP/1.0 400 Bad Request");

                    echo  'Limit of '.$formated_limit.' reached already!';
                    die();
                }

                $type = $_FILES['file']['type'];

                $file = $request->file('file');

                $fileName = $user->id . '-File-' . time() . '-'. rand(5, 99999) . '.'.
                    $request->file('file')->getClientOriginalExtension();

                //https://zenintra-staging.s3.amazonaws.com/test.jpg

                $zen_url = config('app.bucket_base_url');
                $company_special_folder = '/company-'.$company['id'];

                $s3 = Storage::disk('s3');

                $s3->makeDirectory($company_special_folder);

                $s3_upload_path = $company_special_folder.'/'.$fileName;

                $file_uploaded = $s3->put($s3_upload_path, file_get_contents($file),'public');

                $size = $s3->size($s3_upload_path);

                $bucket = Config::get('app.bucket');

                $s3_file_url = $zen_url.$s3_upload_path;

              //  if(!empty($post['filename']) AND $post['filename'] != ''){
              //      $content_name = $post['filename'].'.'.$request->file('file')->getClientOriginalExtension();
              //  }else{
                    $content_name = $original_file_name;
              //  }


                $content = Content::create([
                    's3_link' => $s3_file_url,
                    'company_id'=> $post['company_id'],
                    'content_type'=> 1,
                    'content_name'=> $content_name,
                    'owner_id'=> $user['id'],
                    'parent_content_id' => $post['parent_content_id'],
                    'meta_type'=>$type,
                    'bucket'=>$bucket,
                    'key_name'=>$fileName,
                    'file_size' => $size
                ]);

                $is_admin = FALSE;

                if(isset($company) AND !empty($company['pivot']['is_master']) AND $company['pivot']['is_master'] ==1){
                    $is_admin = TRUE;
                }

                $parent_content_id = $post['parent_content_id'];

                $is_root = FALSE;

                if($parent_content_id == '' OR $parent_content_id == 0 ){
                    $is_root = TRUE;
                }

                $roles = $user->user_roles->toArray();

                $is_admin = FALSE;

                if(isset($company) AND !empty($company['pivot']['is_master']) AND $company['pivot']['is_master'] ==1){
                    $is_admin = TRUE;
                }

                $user_roles_id = [];

                foreach($roles as $role){
                    array_push($user_roles_id,$role['id']);
                }


                $content['size'] = $content['file_size'];

                //FILES
                //processing of files whether root or non-root
                if($is_root){ //means on root folder

                    if($content['content_type'] == '1'){ //FILES

                        //FILES need to be different cause on root files cant be deleted when you're not ownr
                        //if root only admin and owner can edit
                        if($user['id'] == $content['owner_id'] OR $is_admin == TRUE){
                            $content['read_access'] = 1;
                            $content['write_access'] = 1;
                            $content['no_access'] = 0;
                        }else{ // not your file so no delete but can read
                            $content['read_access'] = 1;
                            $content['write_access'] = 0;
                            $content['no_access'] = 0;
                        }
                    }

                }else{ //non root process files

                    if($content['content_type'] == '1'){

                        //get folder permission of current File under the folder cause we are not on the root already
                        $folder_permission = Content_permissions::where('content_id',$parent_content_id)->get()->toArray();

                        $read = 0;
                        $write = 0;
                        $no_access = 0;

                        foreach($folder_permission as $permission){

                            if(in_array($permission['role_id'],$user_roles_id) AND $permission['read_access']=='1'){
                                $read = 1;
                            }
                            if(in_array($permission['role_id'],$user_roles_id) AND $permission['write_access']=='1'){
                                $write = 1;
                            }
                            if(in_array($permission['role_id'],$user_roles_id) AND $permission['no_access']=='1'){
                                $no_access = 1;
                            }

                        }

                        if($user['id'] == $content['owner_id'] OR $is_admin == TRUE){// if admin and if folder
                            $content['read_access'] = 1;
                            $content['write_access'] = 1;
                            $content['no_access'] = 0;
                        }else{
                            $content['read_access'] = $read;
                            $content['write_access'] = $write;
                            $content['no_access'] = $no_access;
                        }
                    }
                }

                $owner = User::find($content->owner_id);

                $content['first_name'] = $owner->first_name;
                $content['last_name'] = $owner->last_name;

                return view('dashboard.file_sharing.file_row',compact('content','is_admin','parent_content_id'));

            }

        }

    }


    function extract_breadcrumb($id = NULL){

        if( ! $id){
            return "<li class='active'><a href='". url('/files_manager') ."'> <i class='fa fa-home'></i> Home </a></li>";
        }



        $current_content = Content::find($id);

        $breadcrumb = '';
        if( ! $current_content['parent_content_id']) {
            $breadcrumb .= "<li class='active breads'  data-content-id='0' data-content-name='Home'><a href='". url('/files_manager') ."'> <i class='fa fa-home'></i> Home </a></li>";

            $breadcrumb .= "<li class='active breads' data-content-id='".$current_content['id']."' data-content-name='".$current_content['content_name']."'><a href='". url('/files_manager') ."/".$current_content['id']."'> <i class='fa fa-folder-open'></i> ". $current_content['content_name'] ."</a></li>";

            return $breadcrumb;
        }
        else{ //Do a recursive call
            return $this->extract_breadcrumb($current_content['parent_content_id']). "<li class='active' data-content-type='folder' data-content-id='".$current_content['id']."' data-content-name='".$current_content['content_name']."' ><a href='". url('/files_manager') ."/".$current_content['id']."'> <i class='fa fa-folder-open'></i> ".$current_content['content_name']."</a></li>";
        }

    }

    function delete_file($id = NULL){

        if($id){

            $content = Content::find($id);

            if($content){

                $parent_content_id = $content['parent_content_id'];

                $s3 = AWS::createClient('s3');

                $bucket = Config::get('app.bucket');

                $result = $s3->deleteObject(array(
                    'Bucket' => $content['bucket'],
                    'Key'    => $content['content_name']
                ));

                $content->delete();

                return Redirect::to('files_manager/'.$parent_content_id)->withInput()->with('success', 'Successfully Deleted File!');

            }

        }

    }

    function delete_folder($content_id = NULL){

        if($content_id){

            $content = Content::find($content_id);

            if($content){

                $parent_content_id = $content['parent_content_id'];

                $content->delete();

                //delete folder permissions
                $where = ['content_id'=>$content_id];
                DB::table('content_permissions')->where($where)->delete();

                //get all contents under them
                $where = ['parent_content_id' => $content_id];
                $contents = DB::table('content')->where($where)->get();

                if($contents ){

                    foreach($contents as $content){

                        //delete folder permissions of these childs
                        $where = ['content_id'=>$content->id];
                        DB::table('content_permissions')->where($where)->delete();

                        $where = ['id'=>$content->id];
                        //delete these childs
                        DB::table('content')->where($where)->delete();


                    }
                }

                return Redirect::to('files_manager/'.$parent_content_id)->withInput()->with('success', 'Successfully Deleted Folder!');

            }

        }

    }

    function delete_file_all(){


        $post = Input::all();

        if ($post) {

            if(!empty($post['ids'])){

                $ids = explode('##',$post['ids']);

                foreach($ids as $content_id){

                   if($content_id != ''){

                       $content = Content::find($content_id);

                       if($content){

                            if($content['content_type'] == 0){ //folder

                                $parent_content_id = $content['parent_content_id'];

                                $content->delete();

                                //delete folder permissions
                                $where = ['content_id'=>$content_id];
                                DB::table('content_permissions')->where($where)->delete();

                                //get all contents under them
                                $where = ['parent_content_id' => $content_id];
                                $contents = DB::table('content')->where($where)->get();

                                if($contents ){

                                    foreach($contents as $content){

                                        //delete folder permissions of these childs
                                        $where = ['content_id'=>$content->id];
                                        DB::table('content_permissions')->where($where)->delete();

                                        $where = ['id'=>$content->id];
                                        //delete these childs
                                        DB::table('content')->where($where)->delete();


                                    }
                                }

                            }else{ //Files

                                $parent_content_id = $content['parent_content_id'];

                                $s3 = AWS::createClient('s3');

                                $bucket = Config::get('app.bucket');

                                $result = $s3->deleteObject(array(
                                    'Bucket' => $content['bucket'],
                                    'Key'    => $content['content_name']
                                ));

                                $content->delete();


                            }
                       }

                   }
                }

            }

        }


    }




    function download_file($id = NULL){

        if($id){

            $content = Content::find($id);

            if($content){

                $parent_content_id = $content['parent_content_id'];

                $s3 = AWS::createClient('s3');

                try {
                    // Get the object
                    $result = $s3->getObject(array(
                        'Bucket' => $content['bucket'],
                        'Key'    => str_replace(config('app.bucket_base_url').'/','',$content['s3_link'])
                    ));


                    // Display the object in the browser
                    header("Content-Type: {$result['ContentType']}");
                    header('Content-Description: File Transfer');
                    //this assumes content type is set when uploading the file.
                    header("Content-Type: {$result['ContentType']}");
                    header('Content-Disposition: attachment; filename=' . $content['content_name']);
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate');
                    header('Pragma: public');

                    //send file to browser for download.
                    echo $result['Body'];
                } catch (S3Exception $e) {
                    echo $e->getMessage() . "\n";
                }

                }

            }

    }

    function initialize_folder_move(){


        $post = Input::all();

        if ($post) {

            $parent_id = $post['parent_id'];
            $content_id = $post['content_id'];

            $content = Content::find($content_id);
            $content->parent_content_id = $parent_id;

            $content->save();


        }

    }




}





