<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Config;
use Auth;
use Input;
use Redirect;
use Response;

use App\Http\Requests;
use App\Http\Controllers\Controller;
//use Illuminate\Support\Facades\Input;

use Vansteen\Sendinblue\Facades\SendinblueWrapper;
use Sendinblue\Mailin;

use Illuminate\Mail\Mailer;
use Mail;

use App\EmailTemplates;
use App\Classes\Helper;


class PublicController extends Controller
{

    public function contact_us(Request $request)
    {
        $post = Input::all();

        $this->validate($request, ['email' => 'required|email']);

        $name = $post['name'];
        $email = $post['email'];
        $message_body = $post['question'];

        $email_template = EmailTemplates::where('slug','contact_us')->first();

        $subject = $email_template->subject;

        $for_parsing = [
            'name' => $name,
            'email' => $email,
            'message' => $message_body
        ];

        $body = Helper::parse_email($email_template->body,$for_parsing);

        Mail::send('emails.default_email', compact('body'), function($message)
        use($subject,$name,$email)
        {
            $message->from($email, $name);
            $message->subject($subject);
            $message->to(config('mail.contact_us'));
        });

        return Response::json(true);

    }

    public function support_form()
    {
        return view('dashboard.layouts.groove-email');
    }

    public function support_email(Request $request)
    {
        $name = $request->input('name');
        $email = $request->input('email');
        $request_type = $request->input('request_type');
        $title = $request->input('title');
        $message_text = $request->input('message');

        $filepath = '';
        if( $request->hasFile('file') ):
            $filename = time() . '.' .$request->file('file')->getClientOriginalExtension();

            $request->file('file')->move(
                base_path() . '/public/uploads/', $filename
            );

            $filepath = base_path() . '/public/uploads/'.$filename;
        endif;

        $email_template = EmailTemplates::where('slug','support')->first();

        $for_parsing = ['title'=>$title];

        $subject = Helper::parse_email($email_template->subject,$for_parsing);

        $for_parsing = [
            'email' => $email,
            'request_type' => $request_type,
            'message_text' => $message_text
        ];

        $body = Helper::parse_email($email_template->body,$for_parsing);

        Mail::send('emails.default_email',compact('body'), function( $mails ) use ( $subject, $name, $filepath ) {
                $mails->subject($subject);
                $mails->to('zenintra@inbox.groovehq.com', $name);
                if( !empty($filepath) ) {
                    $mails->attach($filepath);
                }
        });

        if( !empty($filepath) ) {
            unlink( base_path() . '/public/uploads/'.$filename );
        }
        
        return redirect()->back()->with(['success_mail' => 'sent']);
    }

}
