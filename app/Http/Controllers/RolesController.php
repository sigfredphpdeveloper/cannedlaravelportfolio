<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use Input;
use Redirect;

use App\Company;
use App\Company_user;
use App\Roles;
use App\RolePermission;
use App\User;
use App\User_roles;

class RolesController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $input = Input::all();

        $user = Auth::user();

        $company = Company::find(session('current_company')->id);

        $search = (isset($input['search']))?$input['search']:'';

        $roles = Roles::select('roles.*')
            ->where('roles.company_id',$company->id)
            ->groupBy('roles.id')
            ->search($search)->get();


        $roles = paginateCollection($roles, config('app.page_count'));

        $roles->appends(['search' => $search])->render();

        return view('dashboard.roles.manage_roles',compact('roles','search'));
    }

    public function roles_add(Request $request)
    {

        return view('dashboard.roles.add_roles');
    }

    public function roles_add_save(Request $request)
    {

        $this->validate($request, ['role_name' => 'required','role_description'=>'required']);

        $user = Auth::user();

        $company = $user->company_user->first()->toArray();

        $post = Input::all();

        if ($post)
        {
            $check_role = Roles::where('company_id',$company['id'])->where('role_name',$post['role_name'])->count();

            if($check_role == 0){
                $roles = Roles::create([
                    'role_name' => $post['role_name'],
                    'role_description' => $post['role_description'],
                    'company_id' => $post['company_id']
                ]);
            }else{
                return Redirect::to('roles')->withInput()->with('error', 'A role with the same name already exists, please create roles with different names so you can differentiate them later');
            }


        }

        return Redirect::to('roles')->withInput()->with('success', 'Successfully Added Role!');
    }

    public function roles_edit($id){

        $post = Input::all();

        if ($post)
        {


            $role = Roles::find($id);

            $role->role_name = $post['role_name'];
            $role->role_description = $post['role_description'];

            $role->save();

            return Redirect::to('roles_edit/'.$id)->withInput()->with('success', 'Successfully Edited Role!');

        }

        $role = Roles::find($id);

        return view('dashboard.roles.roles_edit',compact('role'));

    }


    public function roles_delete($id){
        if(!$id){
            return Redirect::to('roles')->withInput()->with('error', 'Error Occured!');
        }

        $user = Auth::user();

        $role = Roles::find($id);

        if($role){

            $role->delete();

            RolePermission::where('role_id',$id)->delete();

            return Redirect::to('roles')->withInput()->with('success', 'Successfully Deleted Role!');
        }else{
            return Redirect::to('roles')->withInput()->with('error', 'Error Occured!');
        }

    }

    public function roles_create_process()
    {

        return view('dashboard.roles.roles_create_process');

    }

    function roles_create_process_save(Request $request){

        $this->validate($request, ['role_name' => 'required','role_description'=>'required']);

        $company = Company::find(session('current_company')->id);

        $post = Input::all();

        if ($post)
        {
            foreach($post['role_name'] as $key=>$val){
                $this->process_roles_save($post,$key,$company);
            }
        }

        //    return Redirect::to('teams?step2=yes')->withInput()->with('success', 'Successfully Added Role!');
        return Redirect::to('step4')->withInput()->with('success', 'Successfully Added Role!');

    }

    function process_roles_save($post,$key,$company){

        $roles = Roles::create([
            'role_name' => $post['role_name'][$key],
            'role_description' => $post['role_description'][$key],
            'company_id' => $company->id
        ]);

    }


}
