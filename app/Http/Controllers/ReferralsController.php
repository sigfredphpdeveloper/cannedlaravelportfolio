<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use Input;
use Redirect;

use App\Company;
use App\Company_user;
use App\Roles;
use App\User;
use App\User_roles;
use App\Referrals;


use Illuminate\Mail\Mailer;
use Mail;

use Excel;
use App\EmailTemplates;
use App\Classes\Helper;


class ReferralsController extends Controller
{

     public function __construct(){
        $this->middleware('auth',['except'=>['accept_referral_invite']]);

    }

    public function index()
    {

        $user = Auth::user();

        $input = Input::all();

        $search = (isset($input['search']))?$input['search']:'';

        $referrals = Referrals::select('referrals.*')
            ->where('referrals.user_id',$user->id)
            ->search($search)->get();

        $referrals = paginateCollection($referrals, config('app.page_count'));

        $referrals->appends(['search' => $search])->render();

        return view('dashboard.referrals.referrals_manager',compact('referrals','search'));

    }



    public function referrals_add()
    {

        return view('dashboard.referrals.referrals_add');
    }

    public function referrals_add_save(Request $request)
    {

        $user = Auth::user();

        $company = $user->company_user->first()->toArray();

        $post = Input::all();

        if ($post)
        {
            if(array_key_exists('email_address',$post) AND count($post['email_address'])){

                $referred = 0;
                foreach($post['email_address'] as $email){

                    if($email != ''){

                        $check_referral = Referrals::where('invitee_email',$email)->where('user_id',$post['user_id'])->first();

                        if(!$check_referral){ //we should make sure it isnt existing yet to AVOID Duplicate

                             $referral_code = md5($email).'-'.time();

                             $referral = Referrals::create([
                                 'sender_company_id'=>$post['company_id'],
                                 'user_id'=>$post['user_id'],
                                 'send_date'=>date('Y-m-d H:i:s'),
                                 'referral_status'=>'pending',
                                 'invitee_email'=>$email,
                                 'referral_code'=>$referral_code
                             ]);

                            $url = url('/accept_referral_invite/'.$referral_code);
                            $link = '<a href="'.$url.'" >'.$url.'</a>';

                            #$invite_message = $post['invite_message'];
                            $invite_message = '';

                            $invitee_name = $email;
                            $referrer_name = $user->first_name .' '.$user->last_name;
                            $referrer_company = $company['company_name'];

                            $email_template = EmailTemplates::where('slug','referral_request')->first();

                            $for_parsing = [
                                'referrer_name' => $referrer_name,
                            ];

                            $subject = Helper::parse_email($email_template->subject,$for_parsing);

                            $for_parsing = [
                                'referrer_name' => $referrer_name,
                                'referrer_company' => $referrer_company,
                                'link' => $link
                            ];

                            $body = Helper::parse_email($email_template->body,$for_parsing);

                            Mail::send('emails.default_email',
                            compact('body'), function($message)
                            use($subject,$invitee_name)
                            {
                                $message->subject($subject);
                                $message->to($invitee_name, $invitee_name);
                            });

                            $referred++;
                        }

                    }

                }

            }

        }

        if($referred>0)
        return Redirect::to('referrals')->withInput()->with('success', 'Successfully Added Referral!');
        else
        return Redirect::to('referrals');


    }

    function accept_referral_invite($hash){

        return Redirect::to('register?referrer='.$hash);

    }

    public function referrals_import()
    {

        $user = Auth::user();

        $referrals = $user->referrals->toArray();

        return view('dashboard.referrals.referrals_import',compact('referrals'));

    }

    public function referrals_import_process()
    {

        $datas = Excel::load($_FILES['upload']['tmp_name'])->get()->toArray();

        if(!$datas) {

         return Redirect::to('directory')->withInput()->with('error', 'Error on Import!');

        }

        $user = Auth::user();

        $company = $user->company_user->first()->toArray();

        if($datas) {

            $imported = 0;

            foreach ($datas as $data) {

                if ($data['email'] != '') {

                    $email = $data['email'];

                    $referral_code = md5($email).'-'.time();

                     $referral = Referrals::create([
                         'sender_company_id'=>$company['id'],
                         'user_id'=>$user->id,
                         'send_date'=>date('Y-m-d H:i:s'),
                         'referral_status'=>'pending',
                         'invitee_email'=>$email,
                         'referral_code'=>$referral_code
                     ]);

                    $url = url('/accept_referral_invite/'.$referral_code);
                    $link = '<a href="'.$url.'" >'.$url.'</a>';

                    $invite_message = FALSE;

                    $invitee_name = $email;
                    $referrer_name = $user->first_name .' '.$user->last_name;
                    $referrer_company = $company['company_name'];

                    $email_template = EmailTemplates::where('slug','referral_request')->first();

                    $for_parsing = [
                        'referrer_name' => $referrer_name,
                    ];

                    $subject = Helper::parse_email($email_template->subject,$for_parsing);

                    $for_parsing = [
                        'referrer_name' => $referrer_name,
                        'referrer_company' => $referrer_company,
                        'link' => $link
                    ];

                    $body = Helper::parse_email($email_template->body,$for_parsing);

                    Mail::send('emails.default_email',
                        compact('body'), function($message)
                        use($subject,$invitee_name)
                        {
                            $message->subject($subject);
                            $message->to($invitee_name, $invitee_name);
                        });


                }
            }

        }

         if($imported==0){
            return Redirect::to('referrals')->withInput()->with('error', 'No Imported Records!');
        }else{
            return Redirect::to('referrals')->withInput()->with('success', 'Successfully Imported '.$imported.' Referrals(s)!');
        }

    }

    function view_referral_details($id = NULL){

        if(!$id) return response('Id is required',404);

        $referral = Referrals::select('company.url','referrals.*','users.email')
            ->join('company', 'company.id','=','referrals.created_company_id')
            ->join('users', 'users.id','=','referrals.created_user_id')
            ->where('referrals.id',$id)
            ->first();

        return view('dashboard.referrals.referrals_view',compact('referral'));

    }


}
