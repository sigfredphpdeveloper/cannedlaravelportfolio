<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\EmailTemplates;
use Mail;
use App\Classes\Helper;

use App\Company;
use App\Company_user as CompanyUser;
use App\User;
use Excel;
use App\Company_user;
use App\Roles;
use App\Teams;
use App\User_teams;
use App\Users_leaves;
use App\Permissions;
use App\Leave_types;
use App\LeaveTypesRoles;
use App\UserLeaveAllowance;

use Auth;
use Input;
use Redirect;


class AdminLeavesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function admin_approved_leaves()
    {

        $leaves = Users_leaves::with('user','Leave_types')->where('status','approved');

        $leaves = $leaves->get();

        $title = trans("leaves.Approved Leaves");

        return view('admin.dashboard.leaves.leaves_table', compact('leaves', 'title'));

    }

    public function admin_pending_leaves()
    {

        $leaves = Users_leaves::with('user','Leave_types')->where('status','pending');

        $leaves = $leaves->get();

        $title = trans("leaves.Pending Leaves");

        return view('admin.dashboard.leaves.leaves_table', compact('leaves', 'title'));
    }

    public function admin_rejected_leaves()
    {

        $leaves = Users_leaves::with('user','Leave_types')->where('status','rejected');

        $leaves = $leaves->get();

        $title = trans("leaves.Rejected Leaves");

        return view('admin.dashboard.leaves.leaves_table', compact('leaves', 'title'));

    }

    public function admin_add_leave($user_id = NULL){

        $user = User::find($user_id);

        $leave_types_set = $user->leave_types_set();

        return view('admin.dashboard.leaves.add_leave', compact('user', 'leave_types_set'));

    }

}
