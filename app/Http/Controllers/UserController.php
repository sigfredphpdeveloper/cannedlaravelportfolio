<?php

namespace App\Http\Controllers;

use App\Board;
use App\Company;
use App\Crm_board;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Mail\Mailer;

use Config;
use Auth;
use Input;
use Intervention\Image\Facades\Image;
use Redirect;
use App\User;
use App\Classes\CustomFields;

use App\Roles;
use App\Teams;
use App\User_endpoints;
use App\Expenses_roles;
use App\Expenses;

use DB;
use AWS;
use Storage;
use Mail;

use Vansteen\Sendinblue\Facades\SendinblueWrapper;
use Sendinblue\Mailin;
use JavaScript;


use App\PayrollDeduction;
use App\PayrollAddition;


class UserController extends Controller
{

    public function __construct(){
        $this->middleware('auth', ['except' => [
            'get_access_now_subscribe'
        ]]);

    }

    public function upgrade()
    {
        $user = Auth::user();

        // Should I calculate which plan is possible here instead of the view?

        return view('user.upgrade', compact('user'));
    }

    public function do_upgrade()
    {
        // TODO: Update this with is ever subscribed check! :D
        if (Input::get('stripeToken')) {
            Auth::user()->subscription(Input::get('plan'))->create(Input::get('stripeToken'));
        } else if (Input::get('plan') == 'cancel') {
            Auth::user()->subscription()->cancel();
        } else if (Input::get('action') == 'switch') {
            Auth::user()->subscription(Input::get('plan'))->swap();
        }

        return Redirect::to('upgrade');
    }

    public function setup_after_registration(){

    }

    public function step1(){

        $user = Auth::user();

        $company = Company::find(session('current_company')->id);

        $user_fields = $company->user_fields->toArray();

        $custom_keypairs_array = [];

        if($user['custom_keypairs']!=''){

            $custom_keypairs_array = json_decode($user['custom_keypairs'],TRUE);

        }

        return view('dashboard.user.complete_profile',compact('company','user_fields','custom_keypairs_array'));

    }

    public function step2(){

        $user = Auth::user();

        $company = Company::find(session('current_company')->id);

        $user_fields = $company->user_fields->toArray();

        $custom_keypairs_array = [];

        if($user['custom_keypairs']!=''){

            $custom_keypairs_array = json_decode($user['custom_keypairs'],TRUE);

        }

        return view('dashboard.user.complete_company',compact('company','user_fields','custom_keypairs_array'));

    }

    public function step3(){

        $input = Input::all();

        $company = Company::find(session('current_company')->id);

        $search = (isset($input['search']))?$input['search']:'';

        $roles = $company->roles;

        $roles = paginateCollection($roles, config('app.page_count'));

        $roles->appends(['search' => $search])->render();

        return view('dashboard.user.step3',compact('roles','search'));

    }

    public function step4(){

        $input = Input::all();

        $user = Auth::user();

        $teams = $user->user_teams;

        $search = (isset($input['search']))?$input['search']:'';

        $teams = paginateCollection($teams, config('app.page_count'));

        $teams->appends(['search' => $search])->render();

        return view('dashboard.user.step4',compact('teams','search'));

    }

    public function step5(){

        $input = Input::all();

        $company = Company::find(session('current_company')->id);

        $roles = $company->roles;

        $teams = $company->teams;

        $search = (isset($input['search']))?$input['search']:'';

        $employees = $company->users();

        if(!empty($input['type']) AND !empty($input['key'])){

            $key = $input['key'];
            $type = $input['type'];

            switch($type){

                case 'name':
                    $employees = $employees->whereRaw("concat(first_name, ' ', last_name) like '%$key%' ");

                    break;
                case 'role':

                    $employees = $employees
                        ->join('user_roles', 'users.id','=','user_roles.user_id')
                        ->join('roles', 'user_roles.role_id','=','roles.id')
                        ->whereRaw("roles.role_name like '%$key%' ");

                    break;
                case 'team':

                    $employees = $employees
                        ->join('user_teams', 'users.id','=','user_teams.user_id')
                        ->join('teams', 'user_teams.team_id','=','teams.id')
                        ->whereRaw("teams.team_name like '%$key%' ");

                    break;
                case 'title':
                    $employees = $employees->whereRaw("users.title like '%$key%' ");
                    break;

            }
        }

        $employees = $employees->search($search)->get();

        $employees = paginateCollection($employees, config('app.page_count'));

        $employees->appends(['search' => $search])->render();

        return view('dashboard.user.step5',compact('employees','roles','teams','search'));

    }


    public function dashboard(){

        $user = Auth::user();

        if($user['completed_profile'] == 0){
            return Redirect::to('step1');
        }elseif($user['completed_profile'] == 1){
            return Redirect::to('step2');
        }

        $company = Company::find(session('current_company')->id);

        $user_fields = $company->user_fields->toArray();

        $custom_keypairs_array = [];

        if($user['custom_keypairs']!=''){

            $custom_keypairs_array = json_decode($user['custom_keypairs'],TRUE);

        }

        $roles = $user->user_roles->toArray();

        $teams = $user->user_teams->toArray();

        $user_roles= [];

        foreach($roles as $role){
            array_push($user_roles,$role['id']);
        }

        $user_teams = [];

        foreach($teams as $team){
            array_push($user_teams,$team['id']);
        }

        $company = $user->company_user->first();

        $announcement_array = array();

        $announcements_count = DB::table('announcements')
            ->select('announcements.*','users.photo','users.first_name','users.last_name')
            ->join('users', 'users.id','=','announcements.user_id')
            ->where('company_id',$company->id)
            ->count();

        $announcements = DB::table('announcements')
            ->select('announcements.*','users.photo','users.first_name','users.last_name')
            ->join('users', 'users.id','=','announcements.user_id')
            ->where('company_id',$company->id)
//            ->limit(4)
            ->orderBy('created_at','DESC')
            ->get();


        // Company_id is for both owner & employee.
        $recognition_board_arr = DB::table('employee_recognition_board as board')
            ->select('board.*', 'employee.photo as employee_photo',
                    'employee.first_name as employee_first_name', 'employee.last_name as employee_last_name', 
                    'owner.first_name as owner_first_name', 'owner.last_name as owner_last_name')
            ->join('users as employee', 'employee.id', '=', 'board.employee_id')
            ->join('users as owner', 'owner.id', '=', 'board.owner_id')
            ->where('board.company_id', $company->id)
            ->where('board.status', '=', 'visible')
            ->limit(4)
            ->orderBy('created_at', 'DESC')
            ->get();


        foreach($announcements as $announcement){

            if($announcement->permission=='all'){

                $announcement_array[] = (array)$announcement;

            }elseif($announcement->permission == 'roles'){ //roles

                $announcement_roles = DB::table('announcement_roles')
                    ->where('announcement_id',$announcement->id)
                    ->get();
                $all_roles_allowed=[];

                foreach($announcement_roles as $role_record){
                    $all_roles_allowed[] = $role_record->role_id;
                }

                foreach($all_roles_allowed as $allowed_role){

                    if(in_array($allowed_role,$user_roles)){
                        $announcement_array[] =  (array)$announcement;
                    }

                }

            }elseif($announcement->permission == 'teams'){ //teams

                $announcement_teams = DB::table('announcement_teams')
                    ->where('announcement_id',$announcement->id)
                    ->get();

                $all_teams_allowed=[];

                foreach($announcement_teams as $team_record){
                    $all_teams_allowed[] = $team_record->team_id;
                }

                foreach($all_teams_allowed as $allowed_team){

                    if(in_array($allowed_team,$user_teams)){
                        $announcement_array[] =  (array)$announcement;
                    }

                }

            }

        }

        $announcement_array = array_map('unserialize', array_unique(array_map('serialize', $announcement_array)));
        $announcements_count = count($announcement_array);
        $recognitions_count = count($recognition_board_arr);
        $loop = 0;


        // get leaves
        $leave_types = $company->leave_types();

        $leave_types = $leave_types->join('leave_types_roles', 'leave_types_roles.leave_type_id','=','leave_types.id')
            ->whereIn('role_id',$user_roles)->get();

        $LeavesController = new LeavesController;
        $total_leaves = 0;
        foreach($leave_types as $item) {
            $total_leaves += $LeavesController->get_leave_type_user_balance($user, $item['leave_type_id']);
        }

        // get claims
        $total_claims = Expenses::where( 'user_id', $user->id )
            ->where( 'company_id', $company->id )->count();


        // get deals
        $total_deals = 0;
        $crm_boards = Crm_board::where('company_id', $company->id)->get();
        foreach($crm_boards as $crm){
            $total_deals += $crm->deals()->count();
        }

        // get on going projects
        $total_projects = Board::where('company_id', $company->id)->count();


        return view('dashboard.dashboard',compact('roles','teams','announcement_array','user_fields',
            'custom_keypairs_array','announcements_count', 'recognition_board_arr', 'recognitions_count', 'loop',
            'total_leaves', 'total_claims', 'total_deals', 'total_projects'));

    }






    public function complete_profile(Request $request)
    {

        $user = Auth::user();

        $company = $user->company_user->toArray();

        $post = Input::all();

        if ($post) {

            if (array_key_exists('user_id', $post) AND !array_key_exists('company_id', $post)) { //user profile

                $validate=[];

                $validate['first_name'] = 'required';
                $validate['last_name'] ='required';

                $this->validate($request, $validate);

                $user = User::find($post['user_id']);

                $company = $user->company_user->first();

                $user_fields = $company->user_fields->toArray();

                $company_fields = [];

                foreach($user_fields as $field){
                    array_push($company_fields,str_replace(' ','_',$field['field_name']));
                }

                $custom_keypairs = [];

                foreach($post as $key => $p){
                    if(in_array($key,$company_fields)){
                        $custom_keypairs[$key] = $post[$key];
                    }
                }

                if(count($custom_keypairs)){
                    $user->custom_keypairs = json_encode($custom_keypairs);
                }

                $user->position = $post['position'];
                $user->phone_number = $post['phone_number'];
                $user->first_name = $post['first_name'];
                $user->last_name = $post['last_name'];
                $user->title = $post['title'];

                //meaning just a plain user so no more updating of company profile
                if (isset($company) AND !empty($company['pivot']['is_master']) == 0) {
                    $user->completed_profile = 2;
                } else {
                    $user->completed_profile = 1;
                }

                if (array_key_exists('password', $post) AND $post['password'] != '') {
                    $user->password = bcrypt($post['password']);
                }

                $user->save();

                if($request->file('image') AND $request->file('image')->isValid()){

                    $imageName = 'user-'.$user->id . '.' .
                        $request->file('image')->getClientOriginalExtension();

                    $request->file('image')->move(
                        base_path() . '/public/uploads/', $imageName
                    );

                    $img = Image::make(base_path() . '/public/uploads/'.$imageName);

                    // prevent possible upsizing
                    $img->resize(null, 150, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });

                    $img->save(base_path() . '/public/uploads/'.$imageName);

                    //https://zenintra-staging.s3.amazonaws.com/test.jpg
                    $zen_url = config('app.bucket_base_url');
                    $company_special_folder = '/company-'.$company['id'];

                    $s3 = Storage::disk('s3');

                    $s3->makeDirectory($company_special_folder);

                    $filePath =  './uploads/'.$imageName;
                    $s3_upload_path = $company_special_folder.'/'.$imageName;

                    $image = $s3->put($s3_upload_path, file_get_contents($filePath),'public');

                    $bucket = Config::get('app.bucket');

                    $s3_file_url = $zen_url.$s3_upload_path;

                    $user->photo = $s3_file_url;

                    $user->save();

                }


                return Redirect::to('dashboard')->withInput()->with('success', 'Successfully Updated Your Profile!');

            } elseif (array_key_exists('company_id', $post)) {

                $this->validate($request, ['industry' => 'required','address'=>'required','country'=>'required']);

                $user = User::find($post['user_id']);

                $user->completed_profile = 2;

                $user->save();

                $company = Company::find($post['company_id']);


                $company->company_name = $post['company_name'];
                $company->industry = $post['industry'];
                $company->size = $post['size'];
                $company->address = $post['address'];
                $company->zip = $post['zip'];
                $company->city = $post['city'];
                $company->country = $post['country'];
                $company->state = $post['state'];
                $company->phone_number = $post['phone_number'];


                if($post['country'] == 'SG'){

                    $company->payroll = 1 ;

                    //add default deductions

                    $default_deductions_count = PayrollDeduction::where('company_id',$company->id)->where('default_deduction',1)->count();
                    $default_additions_count = PayrollAddition::where('company_id',$company->id)->where('default_addition',1)->count();

                    //create only the deductions once so when > 1 dont create it only when zero
                    if($default_deductions_count == 0){

                        $payroll_deduction = new PayrollDeduction;
                        $payroll_deduction->company_id = $company->id;
                        $payroll_deduction->title = 'CDAC';
                        $payroll_deduction->cpf_deductible = 0;
                        $payroll_deduction->default_deduction = 1;
                        $payroll_deduction->save();


                        $payroll_deduction = new PayrollDeduction;
                        $payroll_deduction->company_id = $company->id;
                        $payroll_deduction->title = 'MBMF';
                        $payroll_deduction->cpf_deductible = 0;
                        $payroll_deduction->default_deduction = 1;
                        $payroll_deduction->save();


                        $payroll_deduction = new PayrollDeduction;
                        $payroll_deduction->company_id = $company->id;
                        $payroll_deduction->title = 'ECF';
                        $payroll_deduction->cpf_deductible = 0;
                        $payroll_deduction->default_deduction = 1;
                        $payroll_deduction->save();


                        $payroll_deduction = new PayrollDeduction;
                        $payroll_deduction->company_id = $company->id;
                        $payroll_deduction->title = 'SINDA';
                        $payroll_deduction->cpf_deductible = 0;
                        $payroll_deduction->default_deduction = 1;
                        $payroll_deduction->save();

                    }

                    if($default_additions_count == 0){

                        $payroll_addition = new PayrollAddition;
                        $payroll_addition->company_id = $company->id;
                        $payroll_addition->title = 'Transport Allowance';
                        $payroll_addition->cpf_payable = 1;
                        $payroll_addition->taxable = 1;
                        $payroll_addition->other_allowance = 0;
                        $payroll_addition->category_id = 5;
                        $payroll_addition->default_addition = 1;
                        $payroll_addition->save();

                        $payroll_addition = new PayrollAddition;
                        $payroll_addition->company_id = $company->id;
                        $payroll_addition->title = 'Entertainment Allowance';
                        $payroll_addition->cpf_payable = 1;
                        $payroll_addition->taxable = 1;
                        $payroll_addition->other_allowance = 0;
                        $payroll_addition->category_id = 5;
                        $payroll_addition->default_addition = 1;
                        $payroll_addition->save();


                        $payroll_addition = new PayrollAddition;
                        $payroll_addition->company_id = $company->id;
                        $payroll_addition->title = 'Other Allowances';
                        $payroll_addition->cpf_payable = 1;
                        $payroll_addition->taxable = 1;
                        $payroll_addition->other_allowance = 0;
                        $payroll_addition->category_id = 5;
                        $payroll_addition->default_addition = 1;
                        $payroll_addition->save();

                        $payroll_addition = new PayrollAddition;
                        $payroll_addition->company_id = $company->id;
                        $payroll_addition->title = 'Sales Commissions';
                        $payroll_addition->cpf_payable = 1;
                        $payroll_addition->taxable = 1;
                        $payroll_addition->other_allowance = 0;
                        $payroll_addition->category_id = 5;
                        $payroll_addition->default_addition = 1;
                        $payroll_addition->save();

                        $payroll_addition = new PayrollAddition;
                        $payroll_addition->company_id = $company->id;
                        $payroll_addition->title = 'Bonus';
                        $payroll_addition->cpf_payable = 1;
                        $payroll_addition->taxable = 1;
                        $payroll_addition->other_allowance = 1;
                        $payroll_addition->category_id = 1;
                        $payroll_addition->default_addition = 1;
                        $payroll_addition->save();

                        $payroll_addition = new PayrollAddition;
                        $payroll_addition->company_id = $company->id;
                        $payroll_addition->title = 'Director\'s fees';
                        $payroll_addition->cpf_payable = 1;
                        $payroll_addition->taxable = 1;
                        $payroll_addition->other_allowance = 1;
                        $payroll_addition->category_id = 2;
                        $payroll_addition->default_addition = 1;
                        $payroll_addition->save();

                        $payroll_addition = new PayrollAddition;
                        $payroll_addition->company_id = $company->id;
                        $payroll_addition->title = 'Leave Pay';
                        $payroll_addition->cpf_payable = 1;
                        $payroll_addition->taxable = 1;
                        $payroll_addition->other_allowance = 1;
                        $payroll_addition->category_id = 4;
                        $payroll_addition->default_addition = 1;
                        $payroll_addition->save();

                        $payroll_addition = new PayrollAddition;
                        $payroll_addition->company_id = $company->id;
                        $payroll_addition->title = 'Notice Pay';
                        $payroll_addition->cpf_payable = 0;
                        $payroll_addition->taxable = 1;
                        $payroll_addition->other_allowance = 1;
                        $payroll_addition->category_id = 5;
                        $payroll_addition->default_addition = 1;
                        $payroll_addition->save();

                        $payroll_addition = new PayrollAddition;
                        $payroll_addition->company_id = $company->id;
                        $payroll_addition->title = 'Pension';
                        $payroll_addition->cpf_payable = 0;
                        $payroll_addition->taxable = 1;
                        $payroll_addition->other_allowance = 1;
                        $payroll_addition->category_id = 3;
                        $payroll_addition->default_addition = 1;
                        $payroll_addition->save();

                    }

                }


                $company->save();

                if ($request->file('logo') AND $request->file('logo')->isValid()) {

                    $imageName = 'Company-Logo-' . $company->id . '.' .
                        $request->file('logo')->getClientOriginalExtension();

                    $request->file('logo')->move(
                        base_path() . '/public/uploads/', $imageName
                    );

                    $img = Image::make(base_path() . '/public/uploads/' . $imageName);

                    // prevent possible upsizing
                    $img->resize(null, 150, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });

                    $img->save(base_path() . '/public/uploads/' . $imageName);

                    //https://zenintra-staging.s3.amazonaws.com/test.jpg
                    $zen_url = config('app.bucket_base_url');
                    $company_special_folder = '/company-'.$company['id'];

                    $s3 = Storage::disk('s3');

                    $s3->makeDirectory($company_special_folder);

                    $filePath =  './uploads/'.$imageName;
                    $s3_upload_path = $company_special_folder.'/'.$imageName;

                    $image = $s3->put($s3_upload_path, file_get_contents($filePath),'public');

                    $bucket = Config::get('app.bucket');

                    $s3_file_url = $zen_url.$s3_upload_path;

                    $company->logo = $s3_file_url;

                    $company->save();


                }

                return Redirect::to('step3')->withInput()->with('success', 'Successfully Updated Your Company!');

                //redirect to Directory
            }



        }

        return Redirect::to('dashboard');

    }

    public function my_profile(){

        $user = Auth::user();

        $company = $user->company_user->first();

        $user_fields = $company->user_fields->toArray();

        $custom_keypairs_array = [];

        if($user['custom_keypairs']!=''){

            $custom_keypairs_array = json_decode($user['custom_keypairs'],TRUE);

        }

        $ec = $user->emergency_contact;

        if($ec){
            $ec = $ec->toArray();
        }else{
            $ec['contact_name'] = '';
            $ec['contact_phone'] = '';
            $ec['contact_address'] = '';
            $ec['relationship'] = '';
            $ec['relationship_other'] = '';
            $ec['id'] = '';
        }

        $user_notes = $user->leave_notes;

        $user_notes = paginateCollection($user_notes, config('app.page_count'));

        $search = '';


        return view('dashboard.user.my_profile',compact('user_fields','custom_keypairs_array','ec'));

    }

    public function my_profile_save(Request $request){

        $user = Auth::user();

        $post = Input::all();

        $validate=[];

        $validate['first_name'] = 'required';
        $validate['last_name'] ='required';

        if(isset($_FILES['image']['name']) AND $_FILES['image']['name'] != '')
        $validate['image']='required|mimes:png,jpg,jpeg,gif,bmp';

        if($post){

            if($post['password'] != ''){
                $validate['password']='required|confirmed|min:6';
            }

        }

        $this->validate($request, $validate);

        if ($post) {

            if (array_key_exists('user_id', $post)) {

                $user = User::find($post['user_id']);

                $company = $user->company_user->first();

                $user_fields = $company->user_fields->toArray();

                $company_fields = [];

                foreach($user_fields as $field){
                    array_push($company_fields,str_replace(' ','_',$field['field_name']));
                }

                $custom_keypairs = [];

                foreach($post as $key => $p){
                    if(in_array($key,$company_fields)){
                        $custom_keypairs[$key] = $post[$key];
                    }
                }

                if(count($custom_keypairs)){
                    $user->custom_keypairs = json_encode($custom_keypairs);
                }

                $user->completed_profile = 2;
                $user->position = $post['position'];
                $user->phone_number = $post['phone_number'];
                $user->first_name = $post['first_name'];
                $user->last_name = $post['last_name'];
                $user->title = $post['title'];

                if (array_key_exists('password', $post) AND $post['password'] != '') {
                    $user->password = bcrypt($post['password']);
                }

                if(!empty($post['photo']))
                $user->photo = $post['photo'];

                $user->save();

                return Redirect::to('my_profile')->withInput()->with('success', 'Successfully Updated Your Profile!');

            }

        }
    }

    function upload_profile_pic(Request $request){

        $post = Input::all();

        if ($post) {
            if($request->file('Filedata') AND $request->file('Filedata')->isValid()){

                $user = User::find($post['user_id']);

                $company = $user->company_user->first();

                $imageName = 'user-'.$user->id . '-'.time() . '.' .
                    $request->file('Filedata')->getClientOriginalExtension();

                $request->file('Filedata')->move(
                    base_path() . '/public/uploads/', $imageName
                );

                $img = Image::make(base_path() . '/public/uploads/'.$imageName);

                // prevent possible upsizing
                $img->resize(null, 150, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });

                $img->fit(150, 150);

                #$img->resizeCanvas(150, 150, 'center', false, array(255, 255, 255, 0));

                $img->save(base_path() . '/public/uploads/'.$imageName);

                //https://zenintra-staging.s3.amazonaws.com/test.jpg
                $zen_url = config('app.bucket_base_url');
                $company_special_folder = '/company-'.$company['id'];

                $s3 = Storage::disk('s3');

                $s3->makeDirectory($company_special_folder);

                $filePath =  './uploads/'.$imageName;
                $s3_upload_path = $company_special_folder.'/'.$imageName;

                $image = $s3->put($s3_upload_path, file_get_contents($filePath),'public');

                $bucket = Config::get('app.bucket');

                $s3_file_url = $zen_url.$s3_upload_path;

                echo $s3_file_url;

            }
        }



    }

    public function changeLang(Request $req){
        $code = $req->input('code');

        if(!$code){
            return response("Missing a language code.",400);
        }
        $user = Auth::user();
        $user->preferred_lang = $code;
        $success = $user->save();
        if(!$success){
            return response("Could not update language. Try again in a few minutes.", 500);
        }
    }

    public function getUsersByCompanyExceptSelf($company_id){
        //Validation
        //company exists
        $company = Company::find($company_id);
        if($company === null){
            return response("Company does not exist", 400);
        }
        $users = $company->users()->get();
        $current_user_in_company = $users->contains('id', Auth::user()->id);
        //user from same company as requested
        if(!$current_user_in_company){
            return response("User not in this company", 400);
        }
        $users= $company->users()->get()->reject(function ($item) {
            return $item->id == Auth::user()->id;
        })->values();
      
        return $users->toJson();
    }

    function sb_test(){

        $list_id = config('app.sendinblue_list_prelaunch');

        $data = array( "email" => "jay.alvarez.developer@gmail.com",
            "attributes" => array("NAME"=>"Test Jay", "SURNAME"=>""),
            "listid" => array($list_id)
        );

        dd(SendinblueWrapper::create_update_user($data));

    }

    function get_access_now_subscribe(){

        $post = Input::all();

        if($post){

            $list_id = config('app.sendinblue_list_prelaunch');

            $data = array( "email" => $post['email'],
                "attributes" => array("NAME"=>$post['name'], "SURNAME"=>""),
                "listid" => array($list_id)
            );

            $response = (SendinblueWrapper::create_update_user($data));

            if(!empty($response['code'])){
                echo $response['code'];
            }else{
                echo 'error';
            }

        }
    }

    public function dismiss_first()
    {

        $user = Auth::user();

        $post = Input::all();

        if($post){

            $user->first_visit = 0;
            $user->save();

        }

    }

    public function add_endpoint(Request $request){
        $endpoint = User_endpoints::where("endpoint", "=", $request['endpoint'])->get();
        if ($endpoint->isEmpty()) {
            $endpoint = User_endpoints::create(["endpoint" => $request['endpoint'], "user_id" => $request->user()->id]);
        }
        
        return "true";
    }

    public function delete_endpoint(Request $request){
        $endpoint = User_endpoints::where("endpoint", "=", $request['endpoint'])->first();
        if (!$endpoint) {
            return response('Endpoint not found',Response::HTTP_BAD_REQUEST);
        } else {
            $endpoint->delete();
        }
        
        return "true";
    }

}
