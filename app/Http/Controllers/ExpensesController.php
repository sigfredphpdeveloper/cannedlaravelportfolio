<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Company;
use App\Company_user;
use App\Roles;
use App\User;
use App\Expenses;
use App\Expenses_type;
use App\Expenses_fields;
use App\Expenses_lines;
use App\User_roles;
use App\Expenses_roles;

use Auth;
use Input;
use Redirect;

use Illuminate\Mail\Mailer;
use Mail;

use Intervention\Image\Facades\Image;

use DB;
use AWS;
use Storage;

use Response;
use JavaScript;
use Validator;
use Carbon\Carbon;
use App;

use App\Permissions;

use App\EmailTemplates;
use App\Classes\Helper;

class ExpensesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    public function myexpenses()
    {
        $user = Auth::user();
        $company = session('current_company');

//        $roles = $user->user_roles->toArray();
        $roles = $user->user_roles()->where('company_id', $company->id)->get();

        $expense_roles = Expenses_roles::whereIn('role_id', $roles->pluck('id')->all())->get()->toArray();

        $expense_permission = array('approve'=>0,'delete'=>0,'pay'=>0,'view_all'=>0,);
        foreach($expense_roles as $role){
            $expense_permission['approve'] = ($role['approve']==1)?1:$expense_permission['approve'];
            $expense_permission['delete'] = ($role['delete']==1)?1:$expense_permission['approve'];
            $expense_permission['pay'] = ($role['pay']==1)?1:$expense_permission['approve'];
            $expense_permission['view_all'] = ($role['view_all']==1)?1:$expense_permission['approve'];
        }

        $my_expenses = Expenses::where( 'user_id', $user->id )
                ->where( 'company_id', $company->id )
                ->get();

       return view('dashboard.expenses.index',compact('my_expenses', 'expense_roles','expense_permission'));
    }

    public function approve_expense()
    {
        $post = Input::all();

        $expenses = Expenses::find($post['id']);
        $expenses['approval_date'] = date('Y-m-d');
        $expenses->save();

        echo json_encode(true);die();
    }


    public function settings()
    {
        $user = Auth::user();
        $company = Company::find(session('current_company')->id);

        $expenses_types = Expenses_type::where( 'company_id', $company->id )->get();
        $user_role = User_roles::where('user_id', $user->id)->first();
        $company_roles = Roles::where( 'company_id', $company->id )
                ->where('id', '!=', $user_role->role_id)
                ->get();

        $roles = $company->roles()->with('role_permissions')->get();

        $permissions = Permissions::whereIn('key',['view_expenses'])->get()->toArray();

        return view('dashboard.expenses.settings', compact('expenses_types', 'company_roles','permissions','roles' ) );
    }

    public function settings_save(){

        $user = Auth::user();
        $company = session('current_company');
        $post = Input::all();

        if ($post) {

            $save_company = Company::find( $company->id );
            $save_company->expenses = (array_key_exists('expenses',$post) AND $post['expenses'] == '1')?1:0;
            $save_company->expenses_currency = $post['curreny'];
            $save_company->save();
            $save_company->update_expenses_roles( $user->id );

            // not empty check if exist, false and 0
            if(!empty($post['expenses'])){
                $types = array('travel expense','Office Supplies');

                foreach($types as $type){

                    $check = $company->expenses_types()->where('title',$type)->get()->toArray();

                    if(empty($check)){
                        $expense_type = new Expenses_type;
                        $expense_type->company_id = $company->id;
                        $expense_type->title = $type;
                        $expense_type->created_at = date('Y-m-d H:i:s');
                        $expense_type->save();
                    }
                }
            }

            return Redirect::to('expenses-setting')->withInput()->with('success', 'Successfully Updated!');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.expenses.modal.add-expense');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'expense-title' => 'required',
        ]);

        $user = Auth::user();
        $company = session('current_company');

        if( $validation->fails() )
        {
            return Redirect::back()->with('error', 'Some input are invalid!');
        }
        else
        {
            $expenses = new Expenses;
            $expenses->company_id = $company->id;
            $expenses->user_id = $user->id;
            $expenses->title = $request->input('expense-title');
            $expenses->creation_date = Carbon::now();
            $expenses->total_amount = 0;
            $expenses->status = 'pending';
            $expenses->description = $request->input('details');
            $expenses->save();
            return Redirect::to('expense-item/'.$expenses->id)->with('success','Claim Added !');
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Auth::user();
        $company = session('current_company');
        $expense_item = Expenses::find($id);

        // $expense_type = Expenses_type::find($expense_item->expense_type_id);
        // $expense_fields = Expenses_fields::where('expense_type_id', $expense_type->id )->get();
        $expenses_line = Expenses_lines::where('expense_id', $id)->get();

        return view('dashboard.expenses.show', compact('expense_item', 'expenses_line', 'id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $expense = Expenses::find($id);
        return view('dashboard.expenses.modal.edit-expense', compact('expense'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'expense-title' => 'required',
        ]);

        $user = Auth::user();
        $company = session('current_company');

        if( $validation->fails() )
        {
            return Redirect::back()->with('success', 'Some input are invalid!');
        }
        else
        {
            $expenses = Expenses::find( $request->input('expense-id') );
            $expenses->company_id = $company->id;
            $expenses->user_id = $user->id;
            $expenses->title = $request->input('expense-title');
            $expenses->description = $request->input('details');
            $expenses->update();
            return Redirect::back()->with('success','Expense Updated !');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function delete($id){
        $expense = Expenses::find($id);
        return view('dashboard.expenses.modal.delete-expense', compact('expense'));
    }

    public function destroy($id)
    {
        $expense = Expenses::find($id);
        if( $expense )
        {
            DB::table('expenses_lines')->where('expense_id', '=', $expense->id )->delete();
            $expense->delete();
            return Redirect::to('my-expenses')->with('success','Expense Deleted !');
        }
    }

    public function approve_expenses()
    {
        $user = Auth::user();
        $company = session('current_company');
        $expenses = $company->expense()->where('status', 'submitted')->orderBy('updated_at', 'DESC')->get();

       return view('dashboard.expenses.approve.index',compact('expenses'));
    }

    public function approve_expenses_show($id)
    {
        $expense_item = Expenses::find($id);
        $expenses_line = Expenses_lines::where('expense_id', $id)->get();
        return view('dashboard.expenses.approve.show', compact('expense_item', 'expenses_line', 'id'));
    }

    public function approve_expenses_update(Request $request, $id)
    {
        $this->validate($request, [
            'status' => 'required',
        ]);

        if(empty($id)){
            return Redirect::back()->with( array( 'error' => 'Undefine expense') );
        }

        $expense = Expenses::find($id);
        $expense->status = $request->input('status');
        if($request->input('status') == 'approved'){
            $expense->approval_date = Carbon::now();
        }
        $expense->request_details = $request->input('request_details_content');
        $expense->update();

        $user = User::find($expense->user_id);

        $details = $request->input('request_details_content');
        $details = !empty($details)?$details:'-';
        $to = $user->first_name.' '.$user->last_name;

        $email_template = EmailTemplates::where('slug','expenses')->first();

        $for_parsing = [
            'name' => $to,
            'title' => $expense->title,
            'status' => $expense->status,
            'contact' => '<a href="mailto:'.config('mail.contact').'">'.config('mail.contact').'</a>'
        ];

        $body = Helper::parse_email($email_template->body,$for_parsing);
        $subject = $email_template->subject;

        Mail::send('emails.default_email', compact('body') , function($message)
        use($subject, $to, $user)
        {
            $message->subject($subject);
            $message->to( $user->email, $to);
        });

        return Redirect::back()->with( array( 'success' => 'Status Updated !') );
    }

    public function pay_expenses()
    {
        $user = Auth::user();
        $company = session('current_company');
        $expenses = Expenses::where( 'company_id', $company->id )
                ->where('status', 'approved')
                ->orderBy('creation_date', 'DESC')
                ->get();

       return view('dashboard.expenses.pay.index',compact('expenses'));
    }

    public function show_pay_expenses_item($id)
    {
        $expense_item = Expenses::find($id);
        $expenses_line = Expenses_lines::where('expense_id', $id)->get();
        return view('dashboard.expenses.pay.show', compact('expense_item', 'expenses_line', 'id'));
    }

    public function pay_expense_item($id)
    {
        $expense = Expenses::find($id);
        return view('dashboard.expenses.modal.pay-expense',compact('expense'));
    }

    public function pay_expenses_update(Request $request, $id)
    {
        $this->validate($request, [
            'pay_date' => 'required',
        ]);
        $expense = Expenses::find($id);
        $user = User::withTrashed()->find($expense->user_id);
        $expense->paid_date = $request->input('pay_date');
        $expense->status = 'paid';
        $expense->update();

        // send email here
        $email = $user->email;
        $name = $user->first_name.' '.$user->last_name;
        $date = 'Date: '.$request->input('pay_date');
        $title = $expense->title;
        $status = $expense->status;



        Mail::send('emails.expenses', compact('email','name','date','title','status'), function($message)
        use ($email,$name,$date,$title, $status){
            $message->to($email, $name);
            $message->subject("Expenses paid");
        });

        return Redirect::back()->with( array( 'success' => 'Expense '.$expense->title.' Successfully Paid !') );
    }

    public function history()
    {
        $user = Auth::user();
        $company = session('current_company');
        $expenses = Expenses::where('company_id', $company->id)
            ->whereIn('status', array('paid', 'rejected', 'cancelled'))
            ->get();

         return view('dashboard.expenses.history', compact('expenses'));
    }

    public function report()
    {
        $user = Auth::user();
        $company = session('current_company');
        $expenses = Expenses::where('company_id', $company->id)->get();
      
        // get number of expenses base on status
        $expenses_status = $expenses->toArray();
        $count_status = array_count_values(
            array_map(function($var)
                {
                    return $var['status'];
                }, $expenses_status) );

        $status = array();
        foreach ( $count_status as $key => $value) {
            $status_array = array();
            $status_array['y'] = ucfirst($key);
            $status_array['x'] = $value;
            array_push( $status,  $status_array );
        }


        // get number of expenses base on Expense type
        $expenses_types_object = Expenses::where('company_id', $company->id)->whereNotIn('status', ['pending','cancelled'])->get();
        $total_expenses_lines = array();
        foreach ( $expenses_types_object as $expense ) {
            $expenses_lines = $expense->expenses_lines->toArray();
            foreach ($expenses_lines as $key => $value) {
                  array_push( $total_expenses_lines, $value );
              }  
        }
        $count_total_lines = array_count_values(
            array_map(function($var)
                {
                    return $var['expense_type_id'];
                }, $total_expenses_lines) );
        
        $expense_types = array();
        foreach ($count_total_lines as $key => $value) {
            $temp_array = array();
            $temp_array['y'] = Expenses_type::find($key)->title;
            $temp_array['x'] = $value;
            array_push( $expense_types,  $temp_array );
        }

        // prepare an array of expenses for datatable
        $expenses_array = array();
        foreach ( $expenses as $expense ) {
            $expense_array = array();
            $expense_array['id'] = $expense->id;
            $expense_array['submitted_date'] = $expense->creation_date;
            $expense_array['employee'] = $expense->user->first_name.' '. $expense->user->last_name;
            $expense_array['title'] = $expense->title;
            $expense_array['status'] = $expense->status;
            $expense_array['amount'] = $company->expenses_currency.' '.$expense->total_amount;
            $expense_array['approval_date'] = $expense->approval_date;
            $expense_array['paid_date'] = $expense->paid_date;
            array_push( $expenses_array,  $expense_array );
        }
        JavaScript::put([
        'expenses_array' => $expenses_array,
        'status' => json_encode($status),
        'expense_types' => json_encode($expense_types)
        ]);

        return view('dashboard.expenses.reports');
    }

    public function expenseStatusDataByDate(Request $request)
    {
        $dates = (!empty($request->input('expense_dates'))) ? explode('-', $request->input('expense_dates')) : null;
        $date_from  = (!is_null($dates)) ? date('Y-m-d', strtotime(trim($dates[0]))) : null;
        $date_to    = (!is_null($dates)) ? date('Y-m-d', strtotime(trim($dates[1]))) : null;

        $user = Auth::user();
        $company = session('current_company');
        $expenses = Expenses::where('company_id', $company->id)
                    ->whereBetween('created_at', [$date_from, $date_to])
                    ->get();

        // get number of expenses base on status
        $expenses_status = $expenses->toArray();
        $count_status = array_count_values(
            array_map(function($var)
                {
                    return $var['status'];
                }, $expenses_status) );

        $status = array();
        foreach ( $count_status as $key => $value) {
            $status_array = array();
            $status_array['y'] = ucfirst($key);
            $status_array['x'] = $value;
            array_push( $status,  $status_array );
        }

        return $status;
    }

    public function expenseTypeDataByDate(Request $request)
    {
        $dates = (!empty($request->input('expense_dates'))) ? explode('-', $request->input('expense_dates')) : null;
        $date_from  = (!is_null($dates)) ? date('Y-m-d', strtotime(trim($dates[0]))) : null;
        $date_to    = (!is_null($dates)) ? date('Y-m-d', strtotime(trim($dates[1]))) : null;

        $user = Auth::user();
        $company = session('current_company');
        // get number of expenses base on Expense type
        $expenses_types_object = Expenses::where('company_id', $company->id)
            ->whereBetween('created_at', [$date_from, $date_to])
            ->whereNotIn('status', ['pending','cancelled'])
            ->get();

        $total_expenses_lines = array();
        foreach ( $expenses_types_object as $expense ) {
            $expenses_lines = $expense->expenses_lines->toArray();
            foreach ($expenses_lines as $key => $value) {
                  array_push( $total_expenses_lines, $value );
              }  
        }
        $count_total_lines = array_count_values(
            array_map(function($var)
                {
                    return $var['expense_type_id'];
                }, $total_expenses_lines) );
        
        $expense_types = array();
        foreach ($count_total_lines as $key => $value) {
            $temp_array = array();
            $temp_array['y'] = Expenses_type::find($key)->title;
            $temp_array['x'] = $value;
            array_push( $expense_types,  $temp_array );
        }

        return $expense_types;
    }

    public function resubmit_details(Request $request, $id)
    {

        $expense = Expenses::find($id);
        $expense->status = 'pending';
        $expense->update();

        $details = $request->input('request_details_content');
        $details = !empty($details)?$details:'';

        $email_template = EmailTemplates::where('slug','expenses_status')->first();

        $user = User::find($expense->user_id);

        $to = $user->first_name.' '.$user->last_name;

        $for_parsing = [
            'user' => $to,
            'title' => $expense->title,
            'status' => $expense->status,
            'details' => $details
        ];

        $body = Helper::parse_email($email_template->body,$for_parsing);
        $subject = $email_template->subject;


        Mail::send('emails.default_email',compact('body'), function($message) use($subject, $to, $user)
        {
            $message->subject($subject);
            $message->to( $user->email, $to);
        });



        echo json_encode(true);exit;

    }

    public function send_to_approver(Request $request){
        $post = Input::all();
        $this->validate($request, [
            'id' => 'required',
        ]);

        $expense = Expenses::find($post['id']);
        $expense->status = 'submitted';
        $expense->update();

        return Redirect::back()->with( array( 'success' => 'Status Updated !') );
    }

    public function approve_this_expense(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $expense = Expenses::find($request->id);
        $expense->status = 'approved';
        $expense->approval_date = Carbon::now();
        $expense->update();

        $email_template = EmailTemplates::where('slug','expenses_status')->first();

        $user = User::withTrashed()->find($expense->user_id);

        $to = $user->first_name.' '.$user->last_name;

        $for_parsing = [
            'user' => $to,
            'title' => $expense->title,
            'status' => $expense->status,
            'details' => !empty($details)?$details:''
        ];

        $body = Helper::parse_email($email_template->body,$for_parsing);
        $subject = $email_template->subject;


        Mail::send('emails.default_email',compact('body'), function($message) use($subject, $to, $user)
        {
            $message->subject($subject);
            $message->to( $user->email, $to);
        });

        return Redirect::back()->with( array( 'success' => 'Status Updated !') );
    }


}
