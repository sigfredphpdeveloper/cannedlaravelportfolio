<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use DB;
use Redirect;
use Carbon\Carbon;

use App\Personal_note;

class NotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        $notes_list = Personal_note::where('owner_id', $user->id)->get();

        return view('dashboard.notes.index', ['notes_list' => $notes_list]);
    }

    public function notes()
    {
        $user = Auth::user();

        $notes = Personal_note::where('owner_id', $user->id)->get();

        return view('dashboard.notes.notes', compact('notes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updatePersonalNote(Request $request) 
    {
        // Check table and reset id to 0 if table is empty
        $all_rows = DB::table('personal_note')->get();

        if(count($all_rows) < 1) {
            DB::table('personal_note')->truncate();
        }


        $event_arr = ['create_note', 'update_title', 'update_content', 'delete_note',
                    'update_dimension', 'update_position', 'update_theme', 'update_z_index'];

        $user = Auth::user();

        $event = $request->input('event');

        if($event == $event_arr[0]) {

            DB::table('personal_note')->insert([
                'owner_id' => $user->id,
                'top_position' => $request->input('top'),
                'left_position' => $request->input('left'),
                'z_index' => $request->input('z_index'),
                'height_note' => $request->input('height'),
                'width_note' => $request->input('width'),
                'theme_note' => $request->input('theme'),
                'title' => $request->input('title'),
                'content' => $request->input('content')
            ]);

        } else if($event == $event_arr[1]){

            DB::table('personal_note')->where('id', $request->input('note_id'))->update([
                'title' => $request->input('title')
            ]);

        } else if($event == $event_arr[2]){

            DB::table('personal_note')->where('id', $request->input('note_id'))->update([
                'content' => $request->input('content')
            ]);

        } else if($event == $event_arr[3]){

            DB::table('personal_note')->where('id', $request->input('note_id'))->delete();

        } else if($event == $event_arr[4]){

            DB::table('personal_note')->where('id', $request->input('note_id'))->update([
                'width_note' => $request->input('width'),
                'height_note' => $request->input('height')
            ]);

        } else if($event == $event_arr[5]){
            
            DB::table('personal_note')->where('id', $request->input('note_id'))->update([
                'top_position' => $request->input('top'),
                'left_position' => $request->input('left')
            ]);

        } else if($event == $event_arr[6]){
            
            DB::table('personal_note')->where('id', $request->input('note_id'))->update([
                'theme_note' => $request->input('theme')
            ]);

        } else if($event == $event_arr[7]){
            
            DB::table('personal_note')->where('id', $request->input('note_id'))->update([
                'z_index' => $request->input('z_index')
            ]);

        }

        return json_encode(['request' => $request->all()]);
    }

    public function update(Request $request, $id)
    {
        $note = Personal_note::findOrFail($id);
        $note->title = $request->input('title');
        $note->content = $request->input('content');
        $note->save();

        return redirect()->back()->with('success', 'Note Successfully Updated!');
    }

    public function show($id)
    {
        $note = Personal_note::find($id);
        return view('dashboard.notes.show', compact('note'));
    }

    public function destroy(Request $request)
    {
        $note = Personal_note::find($request->input('note_id'));
        $note->delete();
        return redirect()->back()->with('success', 'Note Successfully Deleted!');
    }
}
