<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\SGCompanyDetail;
use App\Company;
use Redirect;

class SGCompanyDetailController extends Controller
{

    public function __construct()
    {
        $company = Company::find(session('current_company')->id);

        if($company->country != 'SG'){
            return Redirect::to('dashboard')->send();
        }
    }

    public function editCompanyDetails()
    {
        $company = session('current_company');
        $sg_company = SGCompanyDetail::whereCompanyId($company->id)->first();

        if(!empty($sg_company))
            return view('dashboard.payroll.company.edit-sg-company', compact('sg_company'));
        else
            return view('dashboard.payroll.company.create-sg-company');
    }

    public function updateCompanyDetails(Request $request)
    {
        $company = session('current_company');
        $sg_company = SGCompanyDetail::findOrNew($request->input('sg_company_id'));
        $sg_company->company_id                     = $company->id;
        $sg_company->org_id                         = $request->input('org_id');
        $sg_company->org_type                       = $request->input('org_type');
        $sg_company->authorized_person              = $request->input('authorized_person');
        $sg_company->designation_authorized_person  = $request->input('designation_authorized_person');
        $sg_company->phone_number                   = $request->input('phone_number');
        $sg_company->email                          = $request->input('email');
        $sg_company->save();

        return redirect()->back()->with('success', 'Company Details Updated!');
    }
}
