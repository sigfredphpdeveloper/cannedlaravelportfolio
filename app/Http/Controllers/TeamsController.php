<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Company;
use App\Company_user;
use App\Roles;
use App\User;
use App\Teams;
use App\User_teams;


use Auth;
use Input;
use Redirect;

class TeamsController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        $input = Input::all();

        $user = Auth::user();

        $company = $user->company_user->first()->toArray();

        $teams = $user->user_teams->toArray();

        $search = (isset($input['search']))?$input['search']:'';

        $teams = Teams::select('teams.*')
            ->join('user_teams', 'user_teams.team_id','=','teams.id')
            ->join('users', 'user_teams.user_id','=','users.id')
            ->where('users.id',$user->id)
            ->groupBy('teams.id')
            ->search($search)->get();

        $teams = paginateCollection($teams, config('app.page_count'));

        $teams->appends(['search' => $search])->render();

        return view('dashboard.teams.manage_teams',compact('teams','search'));
    }

    public function teams_add()
    {

        return view('dashboard.teams.teams_add');
    }

    public function teams_add_save(Request $request)
    {

        $this->validate($request, ['team_name' => 'required','team_description'=>'required']);

        $user = Auth::user();

        $company = $user->company_user->first()->toArray();

        $post = Input::all();

        if ($post)
        {
            $teams = Teams::create([
                'team_name' => $post['team_name'],
                'team_description' => $post['team_description'],
                'company_id'=>$post['company_id']
            ]);

            $user_teams = User_teams::create(
                [
                    'user_id' => $user['id'],
                    'team_id' => $teams['id']
                ]
            );

        }



        return Redirect::to('teams')->withInput()->with('success', 'Successfully Added Team!');
    }


    public function teams_edit($id){

        $post = Input::all();

        if ($post)
        {
            $team = Teams::find($id);

            $team->team_name = $post['team_name'];
            $team->team_description = $post['team_description'];

            $team->save();

            return Redirect::to('teams_edit/'.$id)->withInput()->with('success', 'Successfully Edited Team!');

        }

        $team = Teams::find($id);


        return view('dashboard.teams.teams_edit',compact('team'));

    }


    public function teams_delete($id){
        if(!$id){
            return Redirect::to('teams')->withInput()->with('error', 'Error Occured!');
        }

        $team = Teams::find($id);

        if($team){
            $team->delete();
            return Redirect::to('teams')->withInput()->with('success', 'Successfully Deleted Team!');
        }else{
            return Redirect::to('teams')->withInput()->with('error', 'Error Occured!');
        }

    }

    public function teams_create_process()
    {

        return view('dashboard.teams.teams_create_process');

    }

    function teams_create_process_save(Request $request){

        $this->validate($request, ['team_name' => 'required','team_description'=>'required']);

        $user = Auth::user();

        $company = Company::find(session('current_company')->id);

        $post = Input::all();

        if ($post)
        {

            foreach($post['team_name'] as $key=>$val){
                $this->process_teams_save($post,$key,$company,$user);
            }
        }

        //return Redirect::to('directory?completed=1')->withInput()->with('success', 'Successfully Added Role!');
        return Redirect::to('step5')->withInput()->with('success', 'Successfully Added Role!');


    }


    function process_teams_save($post,$key,$company,$user){

        $teams = Teams::create([
            'team_name' => $post['team_name'][$key],
            'team_description' => $post['team_description'][$key],
            'company_id'=>$company['id']
        ]);

        $user_teams = User_teams::create(
            [
                'user_id' => $user->id,
                'team_id' => $teams['id']
            ]
        );

    }


}
