<?php

namespace App\Http\Controllers;

use App\Milestone;
use Illuminate\Support\Facades\Input;
use Response;
use App\Company_user;
use App\Tasks;
use App\Tasks_categories;
use App\Tasks_list;
use App\Board;
use App\User;
use App\Teams;
use App\Company;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use DB;
use View;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($calendar_type='day', $project_id = null)
    {
        $company = session('current_company');

        $boards = Board::select('*')
            ->where('company_id', $company->id)
            ->get()->toArray();

        $project_id = !empty($project_id) ? $project_id : $boards[0]['id'];


        $tasks = Tasks::select('*', 'tasks.id AS task_id', 'tasks.start_date as start_date','tasks.parent_task','tasks.due_date as due_date')
            ->join('tasks_lists', 'tasks_lists.id', '=', 'tasks.list_id')
            ->join('users', 'users.id', '=', 'tasks.assigned_user_id')
            ->where('tasks_lists.company_id', $company->id)
            ->where('tasks.board_id', $project_id)
            ->orderBy('tasks.id')
            ->get()->toArray();

        $lists = Tasks_list::select('*')
            ->where('company_id', $company->id)
            ->orderBy('position', 'ASC')
            ->get()->toArray();

        $all_data = array();

        // set milestone in the top of the chart
        $milestone = Milestone::where('board_id',$project_id)->first();

        if(!empty($milestone['milestone'])){
            $all_data['data'][] = array('id'=>'ml'.$milestone->id, 'text'=>$milestone->note, 'type'=>'milestone', 'employee_name'=>'', 'start_date'=>date('d-m-Y', strtotime($milestone->milestone)), 'end_date'=>date('d-m-Y', strtotime($milestone->milestone)));
        }

        $oldest_date = date('Y-m-d');

        if(!empty($milestone->milestone)){
            $newest_date = (date('Y-m-d')<$milestone->milestone)?$milestone->milestone:date('Y-m-d');
        }else{
            $newest_date = date('Y-m-d');
        }
        foreach ($tasks as $k => $task) {
            $time = time();

            $task_name = (strlen($task['task_title'])>15)?substr($task['task_title'],0,15).'...':$task['task_title'];
            $employee_name = (strlen($task['first_name'] . ' ' . $task['last_name'])>15)?substr($task['first_name'] . ' ' . $task['last_name'],0,15).'...':$task['first_name'] . ' ' . $task['last_name'];
            $oldest_date = ($oldest_date>$task['start_date'])?$task['start_date']:$oldest_date;

            $all_data['data'][] = array(
                'id' => $task['task_id'],
                'text' => $task_name,
                'type' => "task",
                'employee_name' => $employee_name,
                'start_date' => date('d-m-Y', strtotime($task['start_date'])),
                'end_date' => date('d-m-Y', strtotime($task['due_date'])),
                'duration' => $task['duration'],
//                'parent' => $task['board_id'],
                'progress' => $task['progress'],
                'open' => true,
            );

            if(!empty($task['parent_task'])){
                $all_data['links'][] = array("id" => $task['parent_task'], "source" => $task['parent_task'], "target" => $task['task_id'], "type" => '1');
            }
        }

        // addjust two months ahead
        $newest_date = date('Y-m-d', strtotime("+2 months", strtotime($newest_date)));
        $oldest_date = date('Y-m-d', strtotime("-3 months", strtotime($oldest_date)));

        $page = 'task_view';

        $board_id = $project_id;

        return view('dashboard.project.calendar_view_'.$calendar_type, compact('board_id','all_data', 'boards', 'project_id','calendar_type', 'milestone', 'newest_date', 'oldest_date'));
    }

    public function people_view($calendar_type='day', $user_id = null)
    {


        $company = session('current_company');

        $employees = User::join('company_user', 'users.id', '=', 'company_user.user_id')->where('company_user.company_id', $company->id)->get();

        $selected_employee = !empty($user_id) ? $user_id : $employees[0]->user_id;

        $company = Company::find(session('current_company')->id);

        $teams = $company->teams;

        $tasks = Tasks::select('*', 'tasks.id AS task_id', 'tasks.create_date as create_date','tasks.task_details')
            ->join('boards', 'boards.id', '=', 'tasks.board_id')
            ->join('tasks_lists', 'tasks_lists.id', '=', 'tasks.list_id')
            ->join('users', 'users.id', '=', 'tasks.assigned_user_id')
            ->where('tasks_lists.company_id', $company->id);

        $where_user_ids = [];

        if($user_id){

            $user_ids = explode(',',$user_id);

            foreach($user_ids as $IDS){

                if($IDS != ''){
                    $where_user_ids[] =  $IDS;
                }

            }

        }

        if(!empty($_GET['team_id'])){

            $team = Teams::find($_GET['team_id']);

            $team_users = $team->team_users();

            foreach($team_users as $IDS){

                if($IDS->id != ''){
                    $where_user_ids[] =  $IDS->id;
                }

            }
        }

        if(count($where_user_ids) > 0){

           $tasks = $tasks->whereIn('tasks.assigned_user_id', $where_user_ids);

        }

        $tasks = $tasks->orderBy('tasks.id')->get()->toArray();

        $newest = date('Y-m-d');
        $oldest = date('Y-m-d');
        $all_data = array();
        foreach ($tasks as $k => $task) {

            $task_name = (strlen($task['task_title'])>20)?substr($task['task_title'],0,20).'...':$task['task_title'];
            $project_name = (strlen($task['board_title'])>20)?substr($task['board_title'] . ' ' . $task['board_title'],0,20).'...':$task['board_title'];
            $oldest = ($oldest>$task['start_date'])?$task['start_date']:$oldest;

            $time = time();
            $all_data['data'][] = array(
                'id' => $task['task_id'],
                'text' => $task_name,
                'type' => "task",
                'project_name' => $project_name,
                'start_date' => date('d-m-Y', strtotime($task['create_date'])),
                'duration' => $task['duration'],
                'progress' => $task['progress'],
                'open' => true,
            );
            $all_data['links'][] = array("id" => $task['parent_task'], "source" => $task['parent_task'], "target" => $task['task_id'], "type" => '1');
        }

        $newest = date('Y-m-d', strtotime("+2 months", strtotime($newest)));
        $oldest = date('Y-m-d', strtotime("-3 months", strtotime($oldest)));

        $boards = Board::select('*')
            ->where('company_id', $company->id)
            ->get();
        $team_id = !empty($_GET['team_id'])?$_GET['team_id']:'';

        return view('dashboard.project.people_view_'.$calendar_type, compact('boards','employees', 'tasks', 'all_data', 'user_id', 'calendar_type','teams','where_user_ids', 'oldest', 'newest', 'team_id'));
    }

    public function save_project()
    {
        $post = Input::all();
//        dd($post);

        if($post['data']['type'] == 'milestone'){
            $post['data']['id'] = str_replace('ml','',$post['data']['id']);

            $milestone = Milestone::find($post['data']['id']);
            if(!empty($milestone)){
                $milestone->note = $post['data']['text'];
                $milestone->milestone = $post['data']['end_date'];
                $milestone->save();
            }
        }else{
            $task = Tasks::find($post['data']['id']);
            $task->start_date = date('Y-m-d H:i:s', strtotime($post['data']['start_date']));
            $task->due_date = date('Y-m-d H:i:s', strtotime($post['data']['end_date']));
            $task->duration = $post['data']['duration'];
            $task->progress = $post['data']['progress'];
            $task->save();
        }

    }

    public function milestone()
    {
        $post = Input::all();

        // check milestone if exist
        $mileston = Milestone::where('board_id',$post['board_id'])->first();

        if(empty($mileston)){
            if(!empty($post['date_submit']) && !empty($post['note'])){
                $ms['board_id'] = $post['board_id'];
                $ms['note'] = $post['note'];
                $ms['milestone'] = $post['date_submit'];
                Milestone::create($ms);
            }
        }else{
            $mileston['note'] = $post['note'];
            $mileston['milestone'] = $post['date_submit'];
            $mileston->save();
        }

        return redirect('projects')->withInput()->with('success', 'Successfully set milestone');
    }
}