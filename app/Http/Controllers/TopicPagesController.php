<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App;
use App\Pages;

class TopicPagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function page_build($id)
    {
        $company_session = session('current_company');
        $page = Pages::find($id);
        if( $page->topic->company_id != $company_session->id )
            App::abort(403, 'Forbidden');
        else
            return view('dashboard.trainings.pages.page-builder', compact('page'));
    }

    public function save_build(Request $request, $id)
    {
        $page = Pages::find($id);
        $page->title = $request->input('page_name');
        $page->content = $request->input('html'); // Input::get('html');
        $page->raw_html = $request->input('raw_html'); //Input::get('raw_html');
        $page->update();
        return 1;
    }

    public function position(Request $request)
    {
        $collection = rtrim( $request->input('collection') , "-");
        $pages = explode("-", $collection);

        $count = 0;
        foreach ($pages as $key => $id) {
            $page = Pages::find($id);
            $page->order = $count;
            $page->update();
            $count++;
        }

        return $pages;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function delete($id)
    {
        $page = Pages::find($id);
        return view('dashboard.trainings.pages.delete', compact('page'));
    }

    public function destroy($id)
    {
        $page = Pages::find($id);
        $page->delete();
        return redirect()->back()->with(['success' => 'Page Successfully Deleted !']);
    }
}
