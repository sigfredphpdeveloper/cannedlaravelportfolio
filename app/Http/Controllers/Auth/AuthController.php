<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Company;
use App\Expenses_type;
use App\Expenses_fields;
use App\Company_user;
use App\Roles;
use App\User_roles;
use App\Referrals;
use App\Stages;
use App\Expenses_roles;
use Auth;
use App\EmailTemplates;
use App\Leave_types;
use App\LeaveTypesRoles;
use App\UserLeaveAllowance;
use App\CompanyWorkWeek;


use Illuminate\Support\Facades\Input;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use Illuminate\Mail\Mailer;
use Mail;

use Vansteen\Sendinblue\Facades\SendinblueWrapper;
use Sendinblue\Mailin;

use App\Announcements;
use App\Classes\Helper;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectPath = '/dashboard';
    protected $loginPath = '/login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'email' => 'required|email|not_gmail|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'agree_terms' => 'required'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {

        $extracted_domain_from_email = substr(strrchr($data['email'], "@"), 1);

        $company = Company::create([
            'file_sharing'=>1,
            'announcements'=>1,
            'crm_setting'=>1,
            'contact_setting'=>1,
            'url' => $extracted_domain_from_email,
            'expenses'=>1,
            'leaves'=>1,
            'training'=>1,
            'file_capacity'=>1073741824.0006000,
            'task_setting'=>1,
            'recognition_board'=>1,
            'asset_management'=>1

        ]);


        $this->create_default_leave_days($company);

        session()->flash('created_company_id',$company->id);

        $role_admin = Roles::create([
            'role_name' => 'Administrator',
            'role_description' => 'Administrator of the company',
            'default_role' => '1',
            'company_id' => $company->id
        ]);

        Roles::create([
            'role_name' => 'Employee',
            'role_description' => 'Employee',
            'default_role' => '1',
            'company_id' => $company->id
        ]);

        $this->create_default_leave_types($company);

        // dd($data);
        $user = User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'title' => $data['title'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'user_level' => 'master',
            'status' => 'unverified',
            'first_visit'=>1
        ]);

        $user_role = User_roles::create([
            'user_id' => $user->id,
            'role_id' => $role_admin->id
        ]);

        // update expenses roles
        $company->update_expenses_roles( $user->id );

        $hash = $user->id.'-'.md5(time());
        $user->activate_hash = $hash;

        $user->save();

        $user_company_mapping = Company_user::create(
            [
                'company_id' =>  $company->id,
                'user_id' => $user->id,
                'is_master'=> 1
            ]
        );



        //Create Default Announcement for company
        $announcement_title = 'Welcome to Zenintranet';
        $annnouncement_body = 'Congratulations on creating your company space on Zenintra.net. Please watch the tutorials to help you configure the platform and take out the best of it. Any question ? Contact our friendly support !';

        $new_announcement = Announcements::create([
            'user_id' => $user->id,
            'company_id' => $company->id,
            'title' => $announcement_title,
            'message' => $annnouncement_body,
            'permission'=>'all'
        ]);

        $new_announcement->save();

        //Welcome OPT IN Email
        $url = url('/activate_user/'.$hash);
        $link = '<a href="'.$url.'" >'.$url.'</a>';


        $email_template = EmailTemplates::where('slug','registration')->first();

        $for_parsing = [
            'link' => $link,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name
        ];

        $body = Helper::parse_email($email_template->body,$for_parsing);
        $subject = $email_template->subject;

        Mail::send('emails.default_email', compact('body'), function($message)
        use($subject,$user)
        {
            $message->subject($subject);
            $message->to($user->email, $user->first_name.' '.$user->last_name);
        });

        //handle Referral
        if(array_key_exists('referrer',$data) AND isset($data['referrer']) AND $data['referrer'] != ''){

            $referral_code = $data['referrer'];

            $referral = Referrals::where('referral_code',$referral_code)->where('referral_status','pending')->first();

            $referrer_user = User::find($referral->user_id);

            //Referral Record Found -> update now..
            if($referral ) {

                //Update Status of Referral Record
                $referral->referral_status='success';
                $referral->referral_credit_date = date('Y-m-d H:i:s');
                $referral->created_company_id = $company->id;
                $referral->created_user_id = $user->id;
                $referral->bonus_file_capacity = 209715200;

                $referral->save();

                //Update Company Bonus //5mb = 524288000
                $update_company = Company::find($referral->sender_company_id);

                $update_company->file_capacity = $update_company->file_capacity + 209715200;

                $update_company->save();

                //Find Referrer USER

                $referrer = $user->first_name.' '.$user->last_name;

                $to = $referrer_user->email;

                $company_referred = $company->url;

                $email_template = EmailTemplates::where('slug','referral_success')->first();

                $for_parsing = [
                    'referrer' => $referrer,
                    'company_referred' => $company_referred
                ];

                $body = Helper::parse_email($email_template->body,$for_parsing);

                $for_parsing = ['company_referred'=>$company_referred];

                $subject = Helper::parse_email($email_template->subject,$for_parsing);;

                Mail::send('emails.default_email', compact('body'), function($message)
                use($subject,$to,$referrer)
                {
                    $message->subject($subject);
                    $message->to($to, $referrer);
                });


            }
        }

        Stages::create([
            'company_id' => $company->id,
            'stage_label' => 'Leads ', //'Meeting Schedule',
            'stage_desc' => '',
            'stage_position' => 2
        ]);
        Stages::create([
            'company_id' => $company->id,
            'stage_label' => 'Contact Made', //'Proposal Sent',
            'stage_desc' => '',
            'stage_position' => 3
        ]);
        Stages::create([
            'company_id' => $company->id,
            'stage_label' => 'Proposal Sent', //'Contact Sent',
            'stage_desc' => '',
            'stage_position' => 4
        ]);

        $work_week = CompanyWorkWeek::create([
                    'company_id'=> $company->id,
                    'title'=>'Normal Work Week',
                    'monday'=>1,
                    'tuesday'=>1,
                    'wednesday'=>1,
                    'thursday'=>1,
                    'friday'=>1,
                    'saturday'=>0,
                    'sunday'=>0
                ]);

        $user->work_week_id = $work_week->id;
        $user->update();

        //Sendin blue Admin Add
        $list_id = config('app.sendinblue_list_admins');

        $data = array( "email" => $user['email'],
            "attributes" => array("NAME"=>$user['first_name'], "SURNAME"=>$user['last_name']),
            "listid" => array($list_id)
        );

        if($_SERVER['SERVER_NAME'] != 'zenintra.local'){
            $response = (SendinblueWrapper::create_update_user($data));
        }

        return $user;

    }

    function create_default_leave_types($company){

        $leave_type = $company->leave_types()->create([
            'type' => 'Paid Leave',
            'description' => 'Paid Leave',
            'default_allowance'=> 10,
            'color_code'=>'#2891de',
            'start_date_allowance' => date('Y-m-d')
        ]);

        $company_roles = $company->roles;

        $leave_types_roles_collection = [];

        foreach($company_roles as $role){

            $role_id = $role->id;

            $leave_types_roles_collection[] = [
                'leave_type_id'=>$leave_type->id,
                'role_id'=>$role_id
            ];

        }

        LeaveTypesRoles::insert($leave_types_roles_collection);

        $leave_type = $company->leave_types()->create([
            'type' => 'Medical Leave',
            'description' => 'Medical Leave',
            'default_allowance'=> 10,
            'color_code'=>'#e07e24',
            'start_date_allowance' => date('Y-m-d')
        ]);

        $leave_types_roles_collection = [];

        foreach($company_roles as $role){

            $role_id = $role->id;

            $leave_types_roles_collection[] = [
                'leave_type_id'=>$leave_type->id,
                'role_id'=>$role_id
            ];

        }

        LeaveTypesRoles::insert($leave_types_roles_collection);

    }

    function create_default_leave_days($company){

        $days = [];

        $weekdays = Helper::week_days();

        foreach($weekdays as $day){

            if($day == 'sunday' || $day == 'saturday'){
                $days[$day] = 0;
            }else{
                $days[$day] = 1;
            }

        }

        $company->leave_days = json_encode($days);
        $company->save();

        return $days;
    }

    function email_signup()
    {
        $post = Input::All();

        //Sendin blue Admin Add
        $list_id = config('app.sendinblue_list_sg_payroll');

        $data = array( "email" => $post['email'],
            "attributes" => array("NAME"=>$post['first_name'], "SURNAME"=>$post['last_name']),
            "listid" => array($list_id)
        );

        if($_SERVER['SERVER_NAME'] != 'zenintra.local'){
            $response = (SendinblueWrapper::create_update_user($data));
        }

        return redirect('free-payroll-sg');
    }

}
