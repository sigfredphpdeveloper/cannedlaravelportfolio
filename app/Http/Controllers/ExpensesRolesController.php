<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Company;
use App\Company_user;
use App\Roles;
use App\User;
use App\Expenses_roles;

use Auth;
use Input;
use Redirect;

class ExpensesRolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function updaterole()
    {
        $user = Auth::user();
        $company = session('current_company');
        $action = Input::get('action');
        $role = Expenses_roles::where( 'company_id', $company->id )
            ->where( 'role_id', Input::get('role_id') )
            ->first();

        if( !$role )
        {
            $expenses_roles = new Expenses_roles;
            if( $action == 'approve' )
            {
                $expenses_roles->approve = 1;
            }
            elseif( $action == 'delete' )
            {
                $expenses_roles->delete = 1;
            }
            elseif( $action == 'pay' )
            {
                $expenses_roles->pay = 1;
            }
            elseif( $action == 'view-all' )
            {
                $expenses_roles->view_all = 1;
            }

            $expenses_roles->company_id = $company['id'];
            $expenses_roles->role_id = Input::get('role_id');
            $expenses_roles->save();
        }
        else
        {
            if( $action == 'approve' )
            {
                $role->approve = Input::get('value');
            }
            elseif( $action == 'delete' )
            {
                $role->delete = Input::get('value');
            }
            elseif( $action == 'pay' )
            {
                $role->pay = Input::get('value');
            }
            elseif( $action == 'view-all' )
            {
                $role->view_all = Input::get('value');
            }

            $role->update();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
