<?php

namespace App\Http\Controllers;

//use Illuminate\Http\Request;

use App\Company_user;
use App\Company;
use App\Contact;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Organization;
use App\Organization_fields;
use App\User;
//use Faker\Provider\fr_FR\Company;
use Request;
use Auth;

use App\Permissions;

use Input;
use Redirect;
use DB;
use Response;
use Excel;


class OrganizationController extends Controller
{
    public function index(){

        $user = Auth::user()->toArray();
//dd(session('current_company')->id);
        $company = Company_user::where('user_id',$user['id'])->get()->toArray();

        $organizations = Organization::getOrganizationContacts(session('current_company')->id)->paginate(25);

        $total_org = Organization::getOrganizationContacts(session('current_company')->id)->get()->count();

        $custom_fields = Organization_fields::Where('company_id',$company[0]['company_id'])->get()->toArray();

        return view('dashboard.organization.manage_organization', compact('organizations','custom_fields', 'user', 'is_admin', 'total_org'));
    }
    public function search(){

        $post = Input::all();
        $user = Auth::user()->toArray();

        $company = Company_user::where('user_id',$user['id'])->get()->toArray();

        $custom_fields = Organization_fields::Where('company_id',$company[0]['company_id'])->get()->toArray();

        $organizations = Organization::getOrganizationContacts(session('current_company')->id, $post['search'])->get();
        $total_org = $organizations->count();

        $organizations = paginateCollection($organizations, 25);
        $organizations->appends(['search' => $post['search']])->render();

        return view('dashboard.organization.manage_organization', compact('organizations','custom_fields','post', 'total_org'));
    }
    
    public function create(){

        $user = Auth::user();
        $request = Request::all();

        $organization = Organization::where('organization_name', $request['organization_name'])
                        ->where('company_id', session('current_company')->id)->get()->toArray();

        if(!empty($organization)){
            return Redirect::to('organization')->withInput()->with('errors', 'Organization name is already exist.');
        }

        $request['company_id']=session('current_company')->id;
        $request['owner_id']=$user['attributes']['id'];
        $request['created_date']=date('Y-m-d H:i:s');
        $request['nb_of_contacts']=0;

        $array_to_json = array();
        // get array to identify custom
        foreach($request as $key => $value){
            if(is_array($value)){
                $array_to_json[$key] = $value[0];
            }
        }

        $request['extra_fields'] = json_encode($array_to_json);
        $id = Organization::create($request);

        return Redirect::to('organization')->withInput()->with('success', 'Successfully Added Organization!');

    }

    public function organization_edit($id,Request $request){

        if(!$id) return response('No ID Received',404);

        $post = Input::all();

        if (Request::isMethod('post')){

            $organization = Organization::find($id);

            $organization->organization_name = $post['organization_name'];
            $organization->organization_address = $post['organization_address'];
            $organization->visibility = $post['visibility'];

            $array_to_json = array();
            // get array to identify custom
            foreach($post as $key => $value){
                if(is_array($value)){
                    $array_to_json[$key] = $value[0];
                }
            }
            $organization->extra_fields = json_encode($array_to_json);
            $organization->last_update_date = date('Y-m-d H:i:s');

            $organization->save();

            return Redirect::to('organization')->withInput()->with('success', 'Successfully Edited Organization!');

        }

        $organization = Organization::find($id);

        $user = Auth::user()->toArray();
        $company = Company_user::where('user_id',$user['id'])->get()->toArray();
        $custom_fields = Organization_fields::Where('company_id',$company[0]['company_id'])->get()->toArray();


        echo view('dashboard.organization.organization_edit',compact('organization','custom_fields'));

    }

    public function organization_delete($id){

        if(!$id) return response('Organization not found',404);

        $organization = Organization::find($id);

        if(!$organization) return response('Organization not found',404);

        if($organization){
            $organization->delete();
            return Redirect::to('organization')->withInput()->with('success', 'Successfully Deleted Organization!');
        }else{
            return Redirect::to('organization')->withInput()->with('error', 'Error Occured!');
        }

    }

    public function organization_list()
    {
        $post = Input::all();
        $user = Auth::user()->toArray();

        $organizations = Organization::select('id','organization_name AS label','organization_name AS value')
            ->where('organization_name', 'LIKE', '%'.$post['q'].'%')
            ->where('organization.company_id', session('current_company')->id)
            ->get()->toArray();

        return Response::json($organizations);

    }

	public function settings(){
		
        $user = Auth::user();

        $is_admin = $user->isAdmin();

        $organization_fields = Organization_fields::where('company_id',session('current_company')->id)
						->get()->toArray();


        $company = Company::find(session('current_company')->id);

        $roles = $company->roles()->with('role_permissions')->get();

        $permissions = Permissions::whereIn('key',['manage_contacts'])->get()->toArray();

        $roles_array = $roles->toArray();

		return view('dashboard.organization.setting', compact('organization_fields','company','permissions','roles','is_admin'));
	}

    public function create_organization_field(){

        $post = Input::all();

        $user = Auth::user();

        $request = Request::all();
        $request['company_id']=session('current_company')->id;
        $request['organization_id']=0;

        $id = Organization_fields::create($request);

        return Redirect::to('organization_setting')->withInput()->with('success', 'Successfully Added Organization!');

    }

    public function organization_field_edit($id){

        if(!$id) return response('No ID Received',404);

        $post = Input::all();

        if (Request::isMethod('post')){

            $user = Auth::user()->toArray();
            // check company if exist and its owner
            $company = Company_user::where('company_id',$post['company_id'])->where('user_id',$user['id'])
                        ->get()->toArray();

            if(!$company) return response('No ID Received',404);

            $organization = Organization_fields::find($id);
            $organization['company_id'] = $post['company_id'];
            $organization['field_title'] = $post['field_title'];
            $organization->save();

            return Redirect::to('organization_setting')->withInput()->with('success', 'Successfully Edited Organization!');

        }

        $organization_field = Organization_fields::find($id)->toArray();
//dd($organization_field);
        echo view('dashboard.organization.organization_field_edit',compact('organization_field'));
    }

    public function organization_field_delete($id){

        if(!$id) return response('Organization field not found',404);

        $organization_field = Organization_fields::find($id);

        if(!$organization_field) return response('Organization not found',404);

        if($organization_field){
            $organization_field->delete();
            return Redirect::to('organization_setting')->withInput()->with('success', 'Successfully Deleted Organization!');
        }else{
            return Redirect::to('organization_setting')->withInput()->with('error', 'Error Occured!');
        }

    }

    public function get_contact()
    {
        $post = Input::all();

        $organization = Organization::where('organization_name',$post['organization'])
            ->where('organization.company_id', session('current_company')->id)->get()->toArray();

        if(!empty($organization)){
            $organization = $organization[0]; //get first instead of ->first()
        }

        if(empty($organization) || empty($post['organization']))
            return Response::json(false);

        return Response::json($organization);
    }

    public function export_contacts($type = 'csv')
    {
        $user = Auth::user();
        $is_admin = $user->isAdmin();

        $company = session('current_company');


        if(!$is_admin){
            return Redirect::to('organization_setting');
        }

        // to make sure file type
        if($type == 'xls'){
            $type = 'xls';
        }else{
            $type = 'csv';
        }

        $contacts = Contact::select(DB::raw("contacts.contact_name as 'Contact Name', contacts.contact_email as 'Contact Email'"),'contacts.position as Position', 'contacts.contact_phone as Phone','organization.organization_name as Organization',
            DB::raw("contacts.address as 'Contact Address'"), DB::raw("organization.organization_address as 'Organization Address'"))
            ->join('organization','organization.id','=','contacts.organization_id')->where('organization.company_id', $company->id)->get()->toArray();

        Excel::create($company->company_name.' Contacts '.date('d-M-Y'), function($excel) use($contacts){

            $excel->sheet('Contacts', function($sheet) use($contacts){

                $sheet->setOrientation('landscape');
                $sheet->fromArray($contacts);

            });

            // Our second sheet
            $excel->sheet('Second sheet', function($sheet) {

            });

        })->export($type);

        return Redirect::to('organization_setting');

    }



}
