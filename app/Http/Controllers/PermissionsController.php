<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Company;
use App\Company_user;
use App\Roles;
use App\User_roles;
use App\User_teams;
use App\Permissions;
use App\RolePermission;

use Auth;
use Input;
use Redirect;
use DB;

class PermissionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = Auth::user();

        $company = Company::find(session('current_company')->id);

        $roles = $company->roles()->with('role_permissions')->get();

        $keys[] = 'edit_company_role_team';
        $keys[] = 'view_company_role_team';
        $keys[] = 'assign_roles';
        $keys[] = 'assign_teams';
        $keys[] = 'edit_employees';
        $keys[] = 'edit_general_permissions';

        $permissions = Permissions::whereIn('key',$keys)->get()->toArray();

        return view('dashboard.permissions.permissions_manager',compact('user','company','permissions','roles'));

    }


    function save_role_permission(){

        $user = Auth::user();

        $company = $user->company_user->first()->toArray();

        $post = Input::all();

        if ($post){

            if(array_key_exists('user_roles_permissions',$post) AND count($post['user_roles_permissions']) > 0){

                $roles = Company::find($company['id'])->roles;

                $keys[] = 'edit_company_role_team';
                $keys[] = 'view_company_role_team';
                $keys[] = 'assign_roles';
                $keys[] = 'assign_teams';
                $keys[] = 'edit_employees';
                $keys[] = 'edit_general_permissions';

                $permissions = Permissions::whereIn('key',$keys)->get()->toArray();
                $roles_array = $roles->toArray();

                $roles_all= [];
                $keys_all = [];

                foreach($roles_array as $ra){
                    $roles_all[] = $ra['id'];
                }

                foreach($permissions as $pa){
                    $keys_all[] = $pa['id'];
                }


                DB::table('role_permission')->whereIn('role_id', $roles_all)->whereIn('permission_id',$keys_all)->delete();

                //Inserters
                foreach($post['user_roles_permissions'] as $RP){

                    if($RP != ''){

                        $RP_array = explode('-',$RP);

                        if(count($RP_array)==2){ /*must be 2 */

                            //role_id = 0 key
                            $role_id = $RP_array['0'];

                            //permission_id = 1 key
                            $permission_id = $RP_array['1'];

                            DB::table('role_permission')->insert(
                                ['role_id' => $role_id,'permission_id' => $permission_id]
                            );

                        }
                    }
                }

                //end inserters

            }

            return Redirect::to('permissions')->withInput()->with('success', 'Successfully Saved Permissions!');

        }

    }




    //expenses settings permissions
    function save_role_permission_expense(){

        $user = Auth::user();

        $company = session('current_company');

        $specific_permissions = Permissions::whereIn('key',['view_expenses'])->get()->toArray();

        $roles = Company::find($company['id'])->roles;

        $post = Input::all();

        if ($post){

            //if posted then no role permissions combination meaning no checks at all
            //loop this type of permission specifically for this module
            //clear for now
            foreach($specific_permissions as $permission){

                foreach($roles->toArray() as $role){
                    //clear role_permissions for all roles and this permission
                    DB::table('role_permission')->where('role_id', $role['id'])->where('permission_id',$permission['id'])->delete();
                }
            }

            if(array_key_exists('user_roles_permissions',$post) AND count($post['user_roles_permissions']) > 0){

                //Inserters
                foreach($post['user_roles_permissions'] as $RP){

                    if($RP != ''){

                        $RP_array = explode('-',$RP);

                        if(count($RP_array)==2){ /*must be 2 */

                            //role_id = 0 key
                            $role_id = $RP_array['0'];

                            //permission_id = 1 key
                            $permission_id = $RP_array['1'];

                            DB::table('role_permission')->insert(
                                ['role_id' => $role_id,'permission_id' => $permission_id]
                            );

                        }
                    }
                }

                //end inserters

            }

            return Redirect::to('expenses-setting')->withInput()->with('success', 'Successfully Saved Permissions!');

        }
    }

    function save_role_permission_trainings(){

        $user = Auth::user();

        $company = session('current_company');

        $specific_permissions = Permissions::whereIn('key',['manage_trainings'])->get()->toArray();

        $roles = Company::find($company['id'])->roles;

        $post = Input::all();

        if ($post){

            //if posted then no role permissions combination meaning no checks at all
            //loop this type of permission specifically for this module
            //clear for now
            foreach($specific_permissions as $permission){

                foreach($roles->toArray() as $role){
                    //clear role_permissions for all roles and this permission
                    DB::table('role_permission')->where('role_id', $role['id'])->where('permission_id',$permission['id'])->delete();
                }
            }

            if(array_key_exists('user_roles_permissions',$post) AND count($post['user_roles_permissions']) > 0){

                //Inserters
                foreach($post['user_roles_permissions'] as $RP){

                    if($RP != ''){

                        $RP_array = explode('-',$RP);

                        if(count($RP_array)==2){ /*must be 2 */

                            //role_id = 0 key
                            $role_id = $RP_array['0'];

                            //permission_id = 1 key
                            $permission_id = $RP_array['1'];

                            DB::table('role_permission')->insert(
                                ['role_id' => $role_id,'permission_id' => $permission_id]
                            );

                        }
                    }
                }

                //end inserters

            }

            return Redirect::back()->withInput()->with('success', 'Successfully Saved Permissions!');

        }
    }

    //emergency contact permissions
    function save_role_permission_emergency_contact(){

        $user = Auth::user();

        $company = session('current_company');

        $specific_permissions = Permissions::whereIn('key',['view_emergency_contacts','edit_emergency_contacts'])->get()->toArray();

        $roles = Company::find($company['id'])->roles;

        $post = Input::all();

        if ($post){

            //if posted then no role permissions combination meaning no checks at all
            //loop this type of permission specifically for this module
            //clear for now
            foreach($specific_permissions as $permission){

                foreach($roles->toArray() as $role){
                    //clear role_permissions for all roles and this permission
                    DB::table('role_permission')->where('role_id', $role['id'])->where('permission_id',$permission['id'])->delete();
                }
            }

            if(array_key_exists('user_roles_permissions',$post) AND count($post['user_roles_permissions']) > 0){

                //Inserters
                foreach($post['user_roles_permissions'] as $RP){

                    if($RP != ''){

                        $RP_array = explode('-',$RP);

                        if(count($RP_array)==2){ /*must be 2 */

                            //role_id = 0 key
                            $role_id = $RP_array['0'];

                            //permission_id = 1 key
                            $permission_id = $RP_array['1'];

                            DB::table('role_permission')->insert(
                                ['role_id' => $role_id,'permission_id' => $permission_id]
                            );

                        }
                    }
                }
                //end inserters

            }

            return Redirect::to('emergency_contacts_settings')->withInput()->with('success', 'Successfully Saved Permissions!');

        }

    }

    //organization permissions
    function save_role_permission_organization(){

        $user = Auth::user();

        $company = session('current_company');

        $specific_permissions = Permissions::whereIn('key',['manage_contacts'])->get()->toArray();

        $roles = Company::find($company['id'])->roles;

        $post = Input::all();

        if ($post){

            //if posted then no role permissions combination meaning no checks at all
            //loop this type of permission specifically for this module
            //clear for now
            foreach($specific_permissions as $permission){

                foreach($roles->toArray() as $role){
                    //clear role_permissions for all roles and this permission
                    DB::table('role_permission')->where('role_id', $role['id'])->where('permission_id',$permission['id'])->delete();
                }
            }

            if(array_key_exists('user_roles_permissions',$post) AND count($post['user_roles_permissions']) > 0){

                //Inserters
                foreach($post['user_roles_permissions'] as $RP){

                    if($RP != ''){

                        $RP_array = explode('-',$RP);

                        if(count($RP_array)==2){ /*must be 2 */

                            //role_id = 0 key
                            $role_id = $RP_array['0'];

                            //permission_id = 1 key
                            $permission_id = $RP_array['1'];

                            DB::table('role_permission')->insert(
                                ['role_id' => $role_id,'permission_id' => $permission_id]
                            );

                        }
                    }
                }

                //end inserters

            }

            return Redirect::to('organization_setting')->withInput()->with('success', 'Successfully Saved Permissions!');

        }

    }


    //recognition board permissions
    function save_role_permission_recognition_board(Request $request) 
    {
        $company = session('current_company');

        $specific_permissions = Permissions::whereIn('key', ['view_recognition_board'])->get()->toArray();

        $save_company = Company::find($company['id']);

        $roles = $save_company->roles;

        $post = Input::all();

        if($post) {
            foreach($specific_permissions as $permission){

                foreach($roles->toArray() as $role){
                    //clear role_permissions for all roles and this permission
                    DB::table('role_permission')->where('role_id', $role['id'])->where('permission_id',$permission['id'])->delete();
                }
            }

            if(array_key_exists('recognition_permissions_arr',$post) AND count($post['recognition_permissions_arr']) > 0){

                //Inserters
                foreach($post['recognition_permissions_arr'] as $RP){

                    if($RP != ''){

                        $RP_array = explode('-',$RP);

                        if(count($RP_array)==2){ /*must be 2 */

                            //role_id = 0 key
                            $role_id = $RP_array['0'];

                            //permission_id = 1 key
                            $permission_id = $RP_array['1'];

                            DB::table('role_permission')->insert(
                                ['role_id' => $role_id,'permission_id' => $permission_id]
                            );

                        }
                    }
                }

                //end inserters

            }

            // ON / OFF toggle button
            $recognition_input = (Input::has('recognition_board') && (int) ($post['recognition_board']) == 1)?1:0;

            DB::table('company')->where('id', $company['id'])->update(['recognition_board' => $recognition_input]);

            return Redirect::to('employee_recognition_board_settings')->withInput()->with('success', 'Successfully Saved Permissions!');
        }
    }


    // asset management permission
    function save_role_permission_asset_management(Request $request) 
    {
        $company = session('current_company');

        $specific_permissions = Permissions::whereIn('key', ['view_asset_management', 'edit_asset_management'])->get()->toArray();

        $save_company = Company::find($company['id']);

        $roles = $save_company->roles;

        $post = Input::all();

        if($post) {
            foreach($specific_permissions as $permission){

                foreach($roles->toArray() as $role){
                    //clear role_permissions for all roles and this permission
                    DB::table('role_permission')->where('role_id', $role['id'])->where('permission_id',$permission['id'])->delete();
                }
            }

            if(array_key_exists('asset_permissions_arr',$post) AND count($post['asset_permissions_arr']) > 0){

                //Inserters
                foreach($post['asset_permissions_arr'] as $RP){

                    if($RP != ''){

                        $RP_array = explode('-',$RP);

                        if(count($RP_array)==2){ /*must be 2 */

                            //role_id = 0 key
                            $role_id = $RP_array['0'];

                            //permission_id = 1 key
                            $permission_id = $RP_array['1'];

                            DB::table('role_permission')->insert(
                                ['role_id' => $role_id,'permission_id' => $permission_id]
                            );

                        }
                    }
                }

                //end inserters

            }

            // ON / OFF toggle button
            $asset_management = (Input::has('asset_management') && (int) ($post['asset_management']) == 1)?1:0;

            DB::table('company')->where('id', $company['id'])->update(['asset_management' => $asset_management]);

            return Redirect::to('asset_management_settings')->withInput()->with('success', 'Successfully Saved Permissions!');
        }
    }


    //announcements permissions
    function save_role_permission_announcement(){

        $user = Auth::user();

        $company = session('current_company');

        $specific_permissions = Permissions::whereIn('key',['manage_announcement'])->get()->toArray();

        $roles = Company::find($company['id'])->roles;

        $post = Input::all();

        if ($post){

            //if posted then no role permissions combination meaning no checks at all
            //loop this type of permission specifically for this module
            //clear for now
            foreach($specific_permissions as $permission){

                foreach($roles->toArray() as $role){
                    //clear role_permissions for all roles and this permission
                    DB::table('role_permission')->where('role_id', $role['id'])->where('permission_id',$permission['id'])->delete();
                }
            }

            if(array_key_exists('user_roles_permissions',$post) AND count($post['user_roles_permissions']) > 0){

                //Inserters
                foreach($post['user_roles_permissions'] as $RP){

                    if($RP != ''){

                        $RP_array = explode('-',$RP);

                        if(count($RP_array)==2){ /*must be 2 */

                            //role_id = 0 key
                            $role_id = $RP_array['0'];

                            //permission_id = 1 key
                            $permission_id = $RP_array['1'];

                            DB::table('role_permission')->insert(
                                ['role_id' => $role_id,'permission_id' => $permission_id]
                            );

                        }
                    }
                }

                //end inserters

            }

            return Redirect::to('announcement_setting')->withInput()->with('success', 'Successfully Saved Permissions!');

        }

    }

    //leaves settings permissions
    function save_role_permission_leaves(){

        $user = Auth::user();

        $company = session('current_company');

        $specific_permissions = Permissions::whereIn('key',['manage_leaves','approve_leaves','view_calendar'])->get()->toArray();

        $roles = Company::find($company['id'])->roles;

        $post = Input::all();

        if ($post){

            //if posted then no role permissions combination meaning no checks at all
            //loop this type of permission specifically for this module
            //clear for now
            foreach($specific_permissions as $permission){

                $role_ids = [];
                foreach($roles->toArray() as $role){
                    //clear role_permissions for all roles and this permission
                    $role_ids[] = $role['id'];

                }

                RolePermission::whereIn('role_id', $role_ids)->where('permission_id',$permission['id'])->delete();

            }

            if(array_key_exists('user_roles_permissions',$post) AND count($post['user_roles_permissions']) > 0){

                //Inserters
                $role_permission_collection = [];

                foreach($post['user_roles_permissions'] as $RP){

                    if($RP != ''){

                        $RP_array = explode('-',$RP);

                        if(count($RP_array)==2){ /*must be 2 */

                            //role_id = 0 key
                            $role_id = $RP_array['0'];

                            //permission_id = 1 key
                            $permission_id = $RP_array['1'];

                            $role_permission_collection[] =
                                ['role_id' => $role_id,'permission_id' => $permission_id]
                            ;



                        }
                    }
                }

                if($role_permission_collection){

                    RolePermission::insert($role_permission_collection);
                }

                //end inserters

            }

            return Redirect::to('leaves-setting')->withInput()->with('success', 'Successfully Saved Permissions!');

        }
    }



    //PAYROLL settings permissions
    function save_role_permission_payroll(){

        $user = Auth::user();

        $company = session('current_company');

        $specific_permissions = Permissions::whereIn('key',['manage_payroll'])->get()->toArray();

        $roles = Company::find($company['id'])->roles;

        $post = Input::all();

        if ($post){

            //if posted then no role permissions combination meaning no checks at all
            //loop this type of permission specifically for this module
            //clear for now
            foreach($specific_permissions as $permission){

                $role_ids = [];
                foreach($roles->toArray() as $role){
                    //clear role_permissions for all roles and this permission
                    $role_ids[] = $role['id'];

                }

                RolePermission::whereIn('role_id', $role_ids)->where('permission_id',$permission['id'])->delete();

            }

            if(array_key_exists('user_roles_permissions',$post) AND count($post['user_roles_permissions']) > 0){

                //Inserters
                $role_permission_collection = [];

                foreach($post['user_roles_permissions'] as $RP){

                    if($RP != ''){

                        $RP_array = explode('-',$RP);

                        if(count($RP_array)==2){ /*must be 2 */

                            //role_id = 0 key
                            $role_id = $RP_array['0'];

                            //permission_id = 1 key
                            $permission_id = $RP_array['1'];

                            $role_permission_collection[] =
                                ['role_id' => $role_id,'permission_id' => $permission_id]
                            ;



                        }
                    }
                }

                if($role_permission_collection){

                    RolePermission::insert($role_permission_collection);
                }

                //end inserters

            }

            return Redirect::to('payroll-settings')->withInput()->with('success', 'Successfully Saved Permissions!');

        }
    }

}
