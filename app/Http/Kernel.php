<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \App\Http\Middleware\VerifyCsrfToken::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'auth.ajax' => \App\Http\Middleware\AjaxAuthenticate::class,
        'auth.token' => \App\Http\Middleware\TokenAuthenticate::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'registration' => \App\Http\Middleware\Registration::class,
        'login_checks' => \App\Http\Middleware\Login::class,
        'localize' => \App\Http\Middleware\SetLocale::class,
        'admin_auth' => \App\Http\Middleware\Admin::class,
        'expenses' => \App\Http\Middleware\ExpensesMiddleware::class,
        'expensesroles' => \App\Http\Middleware\ExpensesRoleMiddleware::class,
        'permissions' => \App\Http\Middleware\Permissions::class,
        'company_admin' => \App\Http\Middleware\CompanyAdmin::class,
        'trainings' => \App\Http\Middleware\TrainingsMiddleware::class,
        'hasPermissions' => \App\Http\Middleware\HasPermissionMiddleware::class,
    ];
}
