<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expenses extends Model
{
    protected $table = 'expenses';

    // treated as carbon dates
    protected $dates = [];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id')->withTrashed();
    }

    public function expenses_lines()
    {
    	return $this->hasMany('App\Expenses_lines', 'expense_id');
    }
}
