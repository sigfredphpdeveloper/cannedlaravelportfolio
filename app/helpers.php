<?php

/**
 * Paginate the given collection.
 *
 * @param \Illuminate\Support\Collection $collection
 * @param int                            $perPage
 * @param string                         $pageName
 * @param int|null                       $page
 *
 * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
 */
function paginateCollection($collection, $perPage = 15, $pageName = 'page', $page = null)
{
    $page = $page ?: \Illuminate\Pagination\Paginator::resolveCurrentPage($pageName);
    $page = (int) max(1, $page); // Handle pageResolver returning null and negative values
    $path = \Illuminate\Pagination\Paginator::resolveCurrentPath();

    return new \Illuminate\Pagination\LengthAwarePaginator(
        $collection->forPage($page, $perPage),
        count($collection),
        $perPage,
        $page,
        compact('path', 'pageName')
    );
}


?>