<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Classes\ElasticFacade;
use Elasticsearch\ClientBuilder;
use App\ChatLine;
class ElasticServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $elastic = $this->app->make(ElasticFacade::class);
 
        ChatLine::saved(function ($post) use ($elastic) {
            $elastic->index([
                'index' => 'chat',
                'type' => 'chatline',
                'id' => $post->id,
                'body' => $post->toArray()
            ]);
        });
     
        ChatLine::deleted(function ($post) use ($elastic) {
            $elastic->delete([
                'index' => 'chat',
                'type' => 'chatline',
                'id' => $post->id,
            ]);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ElasticFacade::class, function ($app) {
            $hosts = [
                config('app.elasticsearch_endpoint')
            ];
            return new ElasticFacade(
                ClientBuilder::create()
                    ->setHosts($hosts) 
                    ->setLogger(ClientBuilder::defaultLogger(storage_path('logs/elastic.log')))
                    ->build()
            );
    });
    }
}
