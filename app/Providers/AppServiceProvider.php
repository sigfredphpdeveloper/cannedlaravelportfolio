<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Expenses_type;
use Validator;
use Input;
use Auth;
use App\User;
use App\Company;
use App\Leave_types;
use App\LeaveTypesRoles;
use App\UserLeaveAllowance;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('not_gmail', function($field,$value,$parameters){
            //return true if field value is foo

            $return = TRUE;

            if(strpos($value, 'gmail.com') !== false) {
                $return = FALSE;
            }

            if(strpos($value, 'hotmail.') !== false) {
                $return = FALSE;
            }

            if(strpos($value, 'yahoo.') !== false) {
                $return = FALSE;
            }

            if(Input::get('address2')) {
                // Do something!
            }

            return $return;

        });

        Validator::replacer('not_gmail', function($message, $attribute, $rule, $parameters) {
            return 'invalid email (gmail,yahoo or hotmail emails) not allowed';
        });

        Validator::extend('email_match_company_url', function($field,$value,$parameters){
            //return true if field value is foo
            $inputed_email = Input::get('email');
            $extracted_domain_from_email = substr(strrchr($inputed_email, "@"), 1);

            $company = $value;

            $company_url_arr = parse_url($company);

            if(array_key_exists('host',$company_url_arr) AND !empty($company_url_arr['host'])){

                $company = $company_url_arr['host'];

                $company = str_replace('www.','',$company);
            }
            $return = FALSE;

            #$store = (strpos($company, $extracted_domain_from_email)) ? 'true':'false';

            // MUST MATCH exactly
            if(strtolower($company) == strtolower($extracted_domain_from_email)) {
                $return = TRUE;
            }

            return $return;
        });

        Validator::replacer('email_match_company_url', function($message, $attribute, $rule, $parameters) {
            return 'Email address must match the company url';
        });


        Validator::extend('email_match_saved_saved_company', function($field,$value,$parameters){
            //return true if field value is foo
            $inputed_email = $value;
            $extracted_domain = substr(strrchr($inputed_email, "@"), 1);

            $user = Auth::user();

            $company = $user->company_user->first();

            $return = TRUE;

            if(strtolower($company['url']) == strtolower($extracted_domain)) {
                $return = FALSE;
            }

            return $return;
        });

        Validator::replacer('email_match_saved_saved_company', function($message, $attribute, $rule, $parameters) {
            return 'Email address must match your company website';
        });

        Expenses_type::created(function ($expense_type) {
            $expense_type->addDefaultFields();
        });

        User::created(function ($user) {

            if(session()->get('created_company_id')){

                $company = Company::find(session('created_company_id'));

                $this->create_default_user_leave_allowance($user,$company);

            }elseif(session('current_company')->id){

                $company = Company::find(session('current_company')->id);

                $this->create_default_user_leave_allowance($user,$company);

            }

        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    function create_default_user_leave_allowance($user,$company){

        $leave_types = $company->leave_types;

        foreach($leave_types as $leave_type){

            $user_leave_allowance = UserLeaveAllowance::where('user_id',$user->id)->where('leave_types_id','=',$leave_type->id)->first();

            if(!$user_leave_allowance){

                $user_leave_allowance = $user->leave_allowances()->create([
                    'user_id'=>$user['id'],
                    'allowance'=>$leave_type->default_allowance,
                    'remaining'=>$leave_type->default_allowance,
                    'leave_types_id'=>$leave_type->id
                ]);

            }

        }
    }
}