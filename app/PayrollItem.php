<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Crypt;

class PayrollItem extends Model
{
    protected $table = 'sg_payroll_item';

    protected $fillable = [
        'user_id',
        'month',
        'year',
        'salary_rate',
        'salary_frequency',
        'no_frequency',
        'overtime1_rate',
        'overtime1_hours',
        'overtime2_rate',
        'overtime2_hours',
        'total_paid_salary',
        'aw',
        'ow',
        'fund_donation_total',
        'payment_date',
        'cpf_employee',
        'cpf_employer',
        'net_pay',
        'basic_pay',
        'payroll_id',
        'overtime_from',
        'overtime_to',
        'taxable_salary'

    ];

    protected $encryptable = [
        'salary_rate',
        'overtime1_rate',
        'overtime2_rate',
        'total_paid_salary',
        'aw',
        'ow',
        'fund_donation_total',
        'cpf_employee',
        'cpf_employer',
        'net_pay',
        'basic_pay',
        'taxable_salary'
    ];

    public function getAttributeValue($key)
    {
        $value = parent::getAttributeValue($key);

        if (in_array($key, $this->encryptable) && ( ! is_null($value)) && (! is_numeric($value)))
        {
            $value = Crypt::decrypt($value);
        }
 
        return $value;
    }

    public static function boot()
    {
        parent::boot();

        PayrollItem::saving(function($object)
        {
            if ( $object->salary_rate)  $object->salary_rate= Crypt::encrypt($object->salary_rate);
            if ( $object->overtime1_rate) $object->overtime1_rate= Crypt::encrypt($object->overtime1_rate);
            if ( $object->overtime2_rate) $object->overtime2_rate= Crypt::encrypt($object->overtime2_rate);
            if ( $object->total_paid_salary) $object->total_paid_salary= Crypt::encrypt($object->total_paid_salary);
            if ( $object->fund_donation_total) $object->fund_donation_total= Crypt::encrypt($object->fund_donation_total);
            if ( $object->cpf_employee) $object->cpf_employee= Crypt::encrypt($object->cpf_employee);
            if ( $object->cpf_employer) $object->cpf_employer= Crypt::encrypt($object->cpf_employer);
            if ( $object->net_pay) $object->net_pay= Crypt::encrypt($object->net_pay);
            if ( $object->basic_pay) $object->basic_pay= Crypt::encrypt($object->basic_pay);
            if ( $object->aw) $object->aw= Crypt::encrypt($object->aw);
            if ( $object->ow) $object->ow= Crypt::encrypt($object->ow);
            if ( $object->taxable_salary) $object->taxable_salary= Crypt::encrypt($object->taxable_salary);
            return $object;
        });
    }

    public function payroll()
    {
        return $this->belongsTo('App\Payroll');
    }

    public function item_additions()
    {
        return $this->hasMany('App\PayrollItemAdditions','item_id','id');
    }
    public function other_allowance()
    {
        return $this->hasMany('App\PayrollItemAdditions','item_id','id')
            ->join('payroll_additions','payroll_additions.id','=', 'sg_payroll_item_addition.addition_id')
            ->select('*','sg_payroll_item_addition.id');
//            ->where('other_allowance', 0);
    }

    public function item_deductions()
    {
        return $this->hasMany('App\PayrollItemDeductions','item_id','id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function employee_data()
    {
        return $this->hasOne('App\SGEmployeeData', 'user_id', 'user_id');
    }

}
