<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;
use App\User_roles;
use App\Expenses_roles;
use App\PayrollDeduction;

class Company extends Model
{

    use SoftDeletes, SearchableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'company';

    /**
     * @var array
     */
    protected $searchable = [
        'columns' => [
            'company.company_username' => 10,
            'company.company_name'     => 10,
            'company.url'              => 10,
            'company.address'          => 10,
            'company.phone_number'     => 10
        ],
        'joins'   => [],
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_username',
        'company_name',
        'url',
        'industry',
        'size',
        'address',
        'zip',
        'city',
        'country',
        'state',
        'phone_number',
        'file_sharing',
        'announcements',
        'contact_setting',
        'expenses',
        'leaves',
        'training',
        'crm_setting',
        'file_capacity',
        'leave_days',
        'recognition_board',
        'payroll',
        'leaves_halfday'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The fields that should be treated as Carbon date instances
     *
     * @var array
     */
    protected $dates = [ 'deleted_at'];


    /**
     * @return $this
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'company_user')->withPivot('is_master', 'is_guest');
    }

    /**
     * @return mixed
     */
    public function employees()
    {
        $employees = $this->belongsToMany('App\User', 'company_user')->withPivot('is_master', 'is_guest')->where('is_master', 0);

        if (array_key_exists('keyword', $_GET)) {
            $employees->orWhere('email', 'like', '%'.$_GET['keyword'].'%');
            $employees->orWhere('first_name', 'like', '%'.$_GET['keyword'].'%');
            $employees->orWhere('last_name', 'like', '%'.$_GET['keyword'].'%');
        }

        return $employees;

    }



    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function roles()
    {
        return $this->hasMany('App\Roles');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function teams()
    {
        return $this->hasMany('App\Teams');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user_fields()
    {
        return $this->hasMany('App\User_fields');
    }


    /**
     * @return mixed
     */
    public function master_user()
    {
        return $this->belongsToMany('App\User', 'company_user')
            ->withPivot('is_master', 'is_guest')
            ->where(function ($query) {
                $query->where('is_master', 1);
            })
            ->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function announcements()
    {
        return $this->hasMany('App\Announcements');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function expenses_types()
    {
        return $this->hasMany('App\Expenses_type');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function expense()
    {
        return $this->hasMany('App\Expenses');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function expenses_roles()
    {
        return $this->hasMany('App\Expenses_roles');
    }

    /**
     * @return mixed
     *
     * @author Bertrand Kintanar <bertrand.kintanar@gmail.com>
     */
    public function getNumberOfEmployees()
    {
        return $this->users->count();
    }

    /**
     * @return string
     *
     * @author Bertrand Kintanar <bertrand.kintanar@gmail.com>
     */
    public function getFacilities()
    {
        $enabled_modules = [];

        $modules = [
            'File Sharing'  => $this->file_sharing,
            'Announcements' => $this->announcements,
            'Contacts'      => $this->contact_setting,
        ];

        foreach ($modules as $module => $enabled) {

            if ($enabled) {
                $enabled_modules[] = $module;
            }
        }

        return $enabled_modules ? implode(', ', $enabled_modules) : 'None';
    }

    /**
     * @return string
     *
     * @author Bertrand Kintanar <bertrand.kintanar@gmail.com>
     */
    public function getMasterEmail()
    {
        return isset($this->master_user()->email) ? $this->master_user()->email : '';
    }

    public function trainings()
    {
        return $this->hasMany('App\Training', 'company_id');
    }

    public function isMasterUser($user_id){
        return ($this->master_user()->id === $user_id);
    }

    public function leave_types()
    {
        return $this->hasMany('App\Leave_types');
    }

    function users_userid(){

        $user_ids = $this->users()->get()->pluck('id');

        return $user_ids;
    }

    public function update_expenses_roles( $user_id )
    {
        $user_role = User_roles::where('user_id', $user_id)->first();
        $role = Expenses_roles::where( 'company_id', $this->id )
            ->where( 'role_id', $user_role->role_id )
            ->first();

        if( !$role )
        {
            $expenses_roles = new Expenses_roles;
            $expenses_roles->approve = 1;
            $expenses_roles->delete = 1;
            $expenses_roles->pay = 1;
            $expenses_roles->view_all = 1;
            $expenses_roles->company_id = $this->id;
            $expenses_roles->role_id = $user_role->role_id;
            $expenses_roles->save();
        }
        else
        {
            $role->approve = 1;
            $role->delete = 1;
            $role->pay = 1;
            $role->view_all = 1;
            $role->update();
        }
    }

    function administrator_users(){

        $admins = $this->users()->get();


        foreach($admins as $i=>$user){

            if(!$user->isAdmin()){

                unset($admins[$i]);

            }

        }


        return $admins;

    }

    public function sg_company_details()
    {
        return $this->hasOne('App\SGCompanyDetail');
    }

    public function payroll_additions()
    {
        return $this->hasMany('App\PayrollAddition');
    }

    public function payroll_deductions()
    {
        return $this->hasMany('App\PayrollDeduction')->where('default_deduction',0);
    }

    public function payroll_employees()
    {
        return $this->hasMany('App\SGEmployeeData');
    }

    public function has_employee_cpf()
    {
        $result = $this->hasOne('App\PayrollDeduction')->where('title', 'Employee\'s CPF deduction');

        if(empty($result->first())){

            PayrollDeduction::insert([
                'company_id' => $this->id, // company_id
                'title' => 'Employee\'s CPF deduction',
                'cpf_deductible' => 0,
                'created_at' => date('Y-m-d H:i:s'),
                'default_deduction' => 1
            ]);

        }

        return $result;
    }

    public function sgIrasForm()
    {
        return $this->hasMany('App\SGIrasForm');
    }

    public function company_work_week()
    {
        return $this->hasMany('App\CompanyWorkWeek');
    }

}
