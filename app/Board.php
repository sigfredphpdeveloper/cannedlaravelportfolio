<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    protected $table = 'boards';

    protected $fillable = [
        'company_id',
        'owner_id',
        'created_date',
        'last_update_date',
        'board_title',
        'visibility'
    ];
    public $timestamps = false;

    public function teams()
    {
        return $this->hasMany('App\Teams');
    }

    public function board_teams()
    {
        return $this->hasMany('App\Board_teams');
    }
}
