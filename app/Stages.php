<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stages extends Model
{
    protected $table = 'stages';

    protected $fillable = [
        'crm_board_id',
        'company_id',
        'stage_label',
        'stage_desc',
        'stage_position'
    ];
}
