<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Nicolaslopezj\Searchable\SearchableTrait;

class Teams extends Model
{

    public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'teams';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'team_name',
        'team_description',
        'company_id'
    ];

    use SearchableTrait;

    protected $searchable = [
        'columns' => [
            'teams.team_name' => 10,
            'teams.team_description' => 10,
        ],
        'joins' => [ ],
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The fields that should be treated as Carbon date instances
     *
     * @var array
     */
    protected $dates = [];

    public function users()
    {
        return $this->belongsToMany('App\User','user_teams','team_id','user_id');
    }

    public function team_users()
    {
        return $this->belongsToMany('App\User','user_teams','team_id','user_id')->get();


    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function announcements(){
        return $this->belongsToMany('App\Announcements', 'announcement_teams', 'team_id', 'announcement_id');
    }

    public function crm_teams()
    {
        return $this->hasMany('App\Crm_board_team');
    }

}
