<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveNotes extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'leave_notes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     *
     *
     */
    protected $fillable = [
        'user_id',
        'from_user_id',
        'date',
        'note'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The fields that should be treated as Carbon date instances
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];


    public function User()
    {
        return $this->belongsTo('App\User');
    }


    public function From_user()
    {
        return $this->hasOne('App\User','id','from_user_id');
    }

}
