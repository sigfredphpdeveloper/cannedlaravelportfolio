<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Crm_board_team extends Model
{
    protected $table = 'crm_board_team';

    protected $fillable = [
        'crm_board_id',
        'team_id'
    ];

    public function board()
    {
        return $this->belongsTo('App\Crm_board');
    }

    public function team()
    {
        return $this->belongsTo('App\Teams');
    }

}
