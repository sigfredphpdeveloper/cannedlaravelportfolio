<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Sg_cpf;
use Crypt;

class SGEmployeeData extends Model
{

    protected $table = 's_g_employee_datas';

    protected $encryptable = [
        'basic_pay',
        'daily_rate',
        'hourly_rate',
        'overtime1_rate',
        'overtime2_rate'    
    ];

    public function getAttributeValue($key)
    {
        $value = parent::getAttributeValue($key);

        if (in_array($key, $this->encryptable) && ( ! is_null($value)) && (! is_numeric($value)))
        {
            $value = Crypt::decrypt($value);
        }
 
        return $value;
    }

    public static function boot()
    {
        parent::boot();

        SGEmployeeData::saving(function($object)
        {
            if ( $object->basic_pay)  $object->basic_pay= Crypt::encrypt($object->basic_pay);
            if ( $object->daily_rate) $object->daily_rate= Crypt::encrypt($object->daily_rate);
            if ( $object->hourly_rate) $object->hourly_rate= Crypt::encrypt($object->hourly_rate);
            if ( $object->overtime1_rate) $object->overtime1_rate= Crypt::encrypt($object->overtime1_rate);
            if ( $object->overtime2_rate) $object->overtime2_rate= Crypt::encrypt($object->overtime2_rate);
            return $object;
        });
    }

    public function company()
    {
    	return $this->belongsTo('App\Company');
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function cpf_computation($data)
    {
        // OW = Ordinary Wage
        // AW = Additional Wage
        // TW = Total Wage

        $ow = 0;
        $aw = 0;
        $tw = 0;

        //301 = singaporean
        if($data['Nationality'] == '301' || ( (date('Y-m-d')-date('Y-m-d',strtotime($data['dob']))) >= 2 )){

            $employee_category = 'Singaporean';

        }elseif((date('Y-m-d')-date('Y-m-d',strtotime($data['employment_start_date']))) >= 1){

            $employee_category = '1st Year PR';

        }else{

            $employee_category = '2nd Year PR';
        }

        // get cpf rates
        $sg_cpf = Sg_cpf::where('min_age','<',27)->where('max_age','>',27)->where('employee_category',$employee_category)->first()->toArray();


        if($data['fee_frequency'] == 'monthly'){

            $ow = ($data['basic_pay']*1);
            $aw = $data['total_additions'] - $data['total_deductions'] + ($data['overtime_1_rate']* $data['overtime1_hours'])+($data['overtime_2_rate']* $data['overtime2_hours']);

        }elseif($data['fee_frequency'] == 'daily'){

            $ow = ($data['daily_rate']*$data['days_worked']);
            $aw = $data['total_additions'] - $data['total_deductions'] + ($data['overtime_1_rate']* $data['overtime1_hours'])+($data['overtime_2_rate']* $data['overtime2_hours']);

        }elseif($data['fee_frequency'] == 'daily'){

            $ow = ($data['hourly_rate']* $data['hours_worked']);
            $aw = $data['total_additions'] - $data['total_deductions'] + ($data['overtime_1_rate']* $data['overtime1_hours'])+($data['overtime_2_rate']* $data['overtime2_hours']);

        }


        // in CPF computation, OW is capped (G in the DB)
        if($ow > $sg_cpf['ow_ceiling'] ){
            $CAPPED_OW = $sg_cpf['ow_ceiling'];
        }else{
            $CAPPED_OW = $ow;
        }

        // computation of total wage
        $tw = $ow+$aw;




        // CPF calculation

        if($data['cpf_entitlement'] == 1){

            if($tw <= $sg_cpf['threshold1']){

                $return['CPF_EMPLOYEE'] = 0;
                $return['TOTAL_CPF'] = 0;

            }elseif($tw <= $sg_cpf['threshold2']){

                $percent = str_replace('%', '', $sg_cpf['employer_rate']) / 100;
                $return['CPF_EMPLOYEE'] = 0;
        		$return['TOTAL_CPF'] = $percent * $tw;

            }elseif($tw <= $sg_cpf['threshold3']){

                $discount_rate = str_replace('%', '', $sg_cpf['discount_rate']) / 100;
                $employer_rate = str_replace('%', '', $sg_cpf['employer_rate']) / 100;
                $return['CPF_EMPLOYEE'] = $discount_rate * ($tw - $sg_cpf['threshold2']);
		        $return['TOTAL_CPF'] = $employer_rate * $tw + $discount_rate * ($tw - $sg_cpf['threshold2']);

            }else{

                $employer_rate = str_replace('%', '', $sg_cpf['employer_rate']) / 100;
                $employee_rate = str_replace('%', '', $sg_cpf['employee_rate']) / 100;

                $return['CPF_EMPLOYEE'] = $employee_rate * $CAPPED_OW + $employee_rate * $aw;
		        $return['TOTAL_CPF'] = ($employer_rate + $employee_rate) * $CAPPED_OW + ($employer_rate + $employee_rate) * $aw;

            }

            $return['CPF_EMPLOYER'] = $return['TOTAL_CPF'] - $return['CPF_EMPLOYEE'];

        }else{
            $return['CPF_EMPLOYEE'] = 0;
            $return['CPF_EMPLOYER'] = 0;
            $return['TOTAL_CPF'] = 0;
        }

        return $return;

    }

    public function employee_item()
    {
        return $this->hasMany('App\PayrollItem', 'user_id');
    }






}
