<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization_fields extends Model
{
	protected $table = 'organization_fields';
	
    protected $fillable =[
        'company_id',
        'organization_id',
        'field_title',
    ];
	
	
	public function organization()
    {
        return $this->belongsTo('App\Organization_fields');
    }

}
