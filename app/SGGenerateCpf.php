<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SGGenerateCpf extends Model
{
    protected $table = 'sg_generate_cpf';

    protected $fillable = [
        'company_id',
        'month',
        'year',
        'file_name',
        'file'
    ];



}
