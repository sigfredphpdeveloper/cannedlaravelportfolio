<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class notes extends Model
{
    protected $table = 'notes';


    protected $fillable = [
        'created_date',
        'owner_id',
        'note_details',
        'deal_id',
    ];


}
