<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcements extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'announcements';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
        'user_id',
        'title',
        'message',
        'picture',
        'roles',
        'teams',
        'permission'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The fields that should be treated as Carbon date instances
     *
     * @var array
     */
    protected $dates = [];

    public function users()
    {
        return $this->belongsTo('App\User');
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function roles()
    {
        return $this->hasMany('App\Announcement_roles','announcement_id');
    }

    public function teams()
    {
        return $this->hasMany('App\Announcement_teams','announcement_id');
    }

}
