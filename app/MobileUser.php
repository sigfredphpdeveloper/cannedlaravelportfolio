<?php
namespace App;

use App;
use Illuminate\Database\Eloquent\Model;

class MobileUser extends Model {
	protected $table = 'mobile_login';

	protected $fillable = [
		'id',
		'company_id',
		'email',
		'password',
		'platform'
	];
}