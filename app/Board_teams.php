<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Board_teams extends Model
{
    protected $table = 'board_teams';

    protected $fillable = [
        'board_id',
        'team_id'
    ];

    public function board()
    {
        return $this->belongsTo('App\Boards');
    }


}
