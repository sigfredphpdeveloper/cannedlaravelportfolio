<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxCategory extends Model
{
    protected $table = 'tax_categories';

    public function payroll_addition()
    {
    	return $this->hasMany('App\PayrollAddition');
    }
}
