<?php

namespace App\Services;

use App\SGEmployeeData;
use App\PayrollItem;
use DB;

class PayrollService
{

	public function generateIR8AForm($inputs)
	{
		$year = $inputs['year'];

        $header  = $this->iras8AHeader($year);
        $iras_details = $this->iras8ADetails($inputs);
        $details = $iras_details['details'];
        $trailer =  $this->trailerIRAS8A($year, $iras_details['trailer_details']);

        $final_iras8A = $header.$details.$trailer;
        
        return $final_iras8A;
	}

	private function iras8AHeader($year)
    {
        $company = session('current_company');

        $header  = '06'; //header & source
        $header .= str_pad($year, 4);
        $header .= '08'; //payment type
        $header .= str_pad($company->sg_company_details->org_type, 1); //Org ID type
        $header .= str_pad($company->sg_company_details->org_id, 12); //Org ID
        $header .= str_pad($company->sg_company_details->authorized_person, 30); //Name of Authorized persion
        $header .= str_pad($company->sg_company_details->designation_authorized_person, 30); //Designation of Authorized persion
        $header .= str_pad($company->company_name, 60); //Org name
        $header .= str_pad($company->phone_number, 20); //Telephone number
        $header .= str_pad($company->sg_company_details->email, 60); //Email of authorized person
        $header .= 'O'; //File type
        $header .= str_pad(date("Y").date("m").date("d"), 8); //Date generated
        $header .= str_pad('', 30); // Name Division/branch
        $header .= str_pad('IR8A', 10); //Form Type
        $header .= str_pad('', 930); //Filler

        return $header;
    }

    private function iras8ADetails($inputs)
    {
        $company = session('current_company');
        $year = $inputs['year'];
        $employees = $inputs['employees'];
        $sgemployees = SGEmployeeData::whereIn('id', $employees)->get();

        /**
         * Variables to be used later
         * on IRAS trailer
         */
        $total_cpf = 0;
        $total_salaries = 0;
        $total_bonuses = 0;
        $total_director_fee = 0;
        $total_other = 0;
        $total_insurance = 0;
        $total_mbmf = 0;
        $total_donation = 0;

        $details = '';

        foreach($sgemployees as $employee){

            // $employees                      = $inputs['employees'][$employee->id];
            $employee_id                    = $employee->id;
            $compensation_for_loss          = isset($inputs['compensation_for_loss'][$employee_id]) ? $inputs['compensation_for_loss'][$employee_id] : ''; //38a
            $apprv_obtained_by_iras         = isset($inputs['apprv_obtained_by_iras'][$employee_id]) ? $inputs['apprv_obtained_by_iras'][$employee_id] : ''; //27a
            $date_of_approval               = isset($inputs['date_of_approval'][$employee_id]) ? $inputs['date_of_approval'][$employee_id] : ''; //27b
            $retirement_accrued_1993        = isset($inputs['retirement_accrued_1993'][$employee_id]) ? $inputs['retirement_accrued_1993'][$employee_id] : ''; //40
            $name_of_fund                   = isset($inputs['name_of_fund'][$employee_id]) ? $inputs['name_of_fund'][$employee_id] : ''; //51
            $retirement_accrued_31_12_92    = isset($inputs['retirement_accrued_31_12_92'][$employee_id]) ? $inputs['retirement_accrued_31_12_92'][$employee_id] : ''; //39
            $contribution_made_by_employer  = isset($inputs['contribution_made_by_employer'][$employee_id]) ? $inputs['contribution_made_by_employer'][$employee_id] : ''; //41
            $sales_commision_from           = isset($inputs['sales_commision_from'][$employee_id]) ? $inputs['sales_commision_from'][$employee_id] : ''; //32a
            $sales_commision_to             = isset($inputs['sales_commision_to'][$employee_id]) ? $inputs['sales_commision_to'][$employee_id] : ''; //32b
            $excess_voluntary_cpf           = isset($inputs['excess_voluntary_cpf'][$employee_id]) ? $inputs['excess_voluntary_cpf'][$employee_id] : ''; //42
            $employer_contribution          = isset($inputs['employer_contribution'][$employee_id]) ? $inputs['employer_contribution'][$employee_id] : ''; 
            $employee_contribution          = isset($inputs['employee_contribution'][$employee_id]) ? $inputs['employee_contribution'][$employee_id] : '';
            $gains_profits_1_b              = isset($inputs['gains_profits_1_b'][$employee_id]) ? $inputs['gains_profits_1_b'][$employee_id] : ''; //43
            $gains_profits_1_g              = isset($inputs['gains_profits_1_g'][$employee_id]) ? $inputs['gains_profits_1_g'][$employee_id] : ''; //19a
            $life_insurance                 = isset($inputs['life_insurance'][$employee_id]) ? $inputs['life_insurance'][$employee_id] : ''; //15
            $date_of_approval_director_fee  = isset($inputs['date_of_approval_director_fee'][$employee_id]) ? $inputs['date_of_approval_director_fee'][$employee_id] : ''; //50
            $date_of_declaration_bonus      = isset($inputs['date_of_declaration_bonus'][$employee_id]) ? $inputs['date_of_declaration_bonus'][$employee_id] : ''; //49
            /**
             * Tax addition queries
             */

            //Salary
            $salary = PayrollItem::where('year', $year)
                    ->where('user_id', $employee->user->id)
                    ->addSelect(DB::raw("SUM(taxable_salary) as salary"))
                    ->first();
            $salary = !empty($salary) ? $salary->salary : 0;

            // CPF
            $cpf = PayrollItem::where('year', $year)
                    ->where('user_id', $employee->user->id)
                    ->addSelect(DB::raw("SUM(cpf_employee) as cpf"))
                    ->first();
            $cpf = !empty($cpf) ? $cpf->cpf : 0;

            //Bonuses
            $bonuses = PayrollItem::join('sg_payroll_item_addition', 'sg_payroll_item.id',  '=', 'sg_payroll_item_addition.item_id')
                    ->where('sg_payroll_item.user_id', $employee->user->id)
                    ->where('sg_payroll_item.year', $inputs['year'])
                    ->where('sg_payroll_item_addition.title', 'Bonus')
                    ->addSelect(DB::raw("SUM(sg_payroll_item_addition.amount) as bonuses"))
                    ->first();
            $bonuses = !empty($bonuses) ? $bonuses->bonuses : 0;

            // Director's Fee
            $director_fee = PayrollItem::join('sg_payroll_item_addition', 'sg_payroll_item.id',  '=', 'sg_payroll_item_addition.item_id')
                    ->where('sg_payroll_item.user_id', $employee->user->id)
                    ->where('sg_payroll_item.year', $inputs['year'])
                    ->whereIn('sg_payroll_item_addition.addition_id', function($query){
                            $query->select('id')
                                ->from('payroll_additions')
                                ->where('title', 'Director\'s fees');
                    })
                    ->addSelect(DB::raw("SUM(sg_payroll_item_addition.amount) as director_fee"))
                    ->first();
            $director_fee = !empty($director_fee) ? $director_fee->director_fee : 0;

            // Transport Allowance
            $transport_allowance = PayrollItem::join('sg_payroll_item_addition', 'sg_payroll_item.id',  '=', 'sg_payroll_item_addition.item_id')
                    ->where('sg_payroll_item.user_id', $employee->user->id)
                    ->where('sg_payroll_item.year', $inputs['year'])
                    ->whereIn('sg_payroll_item_addition.addition_id', function($query){
                            $query->select('id')
                                ->from('payroll_additions')
                                ->where('title', 'Transport Allowance');
                    })
                    ->addSelect(DB::raw("SUM(sg_payroll_item_addition.amount) as transport_allowance"))
                    ->first();
            $transport_allowance = !empty($transport_allowance) ? $transport_allowance->transport_allowance : 0;

            // Entertainment Allowance
            $entertainment_allowance = PayrollItem::join('sg_payroll_item_addition', 'sg_payroll_item.id',  '=', 'sg_payroll_item_addition.item_id')
                    ->where('sg_payroll_item.user_id', $employee->user->id)
                    ->where('sg_payroll_item.year', $inputs['year'])
                    ->whereIn('sg_payroll_item_addition.addition_id', function($query){
                            $query->select('id')
                                ->from('payroll_additions')
                                ->where('title', 'Entertainment Allowance');
                    })
                    ->addSelect(DB::raw("SUM(sg_payroll_item_addition.amount) as entertainment_allowance"))
                    ->first();
            $entertainment_allowance = !empty($entertainment_allowance) ? $entertainment_allowance->entertainment_allowance : 0;

            // Other Allowances
            $other_allowance = PayrollItem::join('sg_payroll_item_addition', 'sg_payroll_item.id',  '=', 'sg_payroll_item_addition.item_id')
                    ->where('sg_payroll_item.user_id', $employee->user->id)
                    ->where('sg_payroll_item.year', $inputs['year'])
                    ->whereIn('sg_payroll_item_addition.addition_id', function($query){
                            $query->select('id')
                                ->from('payroll_additions')
                                ->where('title', 'Other Allowances');
                    })
                    ->addSelect(DB::raw("SUM(sg_payroll_item_addition.amount) as other_allowance"))
                    ->first();
            $other_allowance = !empty($other_allowance) ? $other_allowance->other_allowance : 0;

            // Pension
            $pension = PayrollItem::join('sg_payroll_item_addition', 'sg_payroll_item.id',  '=', 'sg_payroll_item_addition.item_id')
                    ->where('sg_payroll_item.user_id', $employee->user->id)
                    ->where('sg_payroll_item.year', $inputs['year'])
                    ->whereIn('sg_payroll_item_addition.addition_id', function($query){
                            $query->select('id')
                                ->from('payroll_additions')
                                ->where('title', 'Pension');
                    })
                    ->addSelect(DB::raw("SUM(sg_payroll_item_addition.amount) as pension"))
                    ->first();
            $pension = !empty($pension) ? $pension->pension : 0;

            // Notice Pay
            $notice_pay = PayrollItem::join('sg_payroll_item_addition', 'sg_payroll_item.id',  '=', 'sg_payroll_item_addition.item_id')
                    ->where('sg_payroll_item.user_id', $employee->user->id)
                    ->where('sg_payroll_item.year', $inputs['year'])
                    ->whereIn('sg_payroll_item_addition.addition_id', function($query){
                            $query->select('id')
                                ->from('payroll_additions')
                                ->where('title', 'Notice Pay');
                    })
                    ->addSelect(DB::raw("SUM(sg_payroll_item_addition.amount) as notice_pay"))
                    ->first();
            $notice_pay = !empty($notice_pay) ? $notice_pay->notice_pay : 0;

            // Sales Commissions
            $sales_commission = PayrollItem::join('sg_payroll_item_addition', 'sg_payroll_item.id',  '=', 'sg_payroll_item_addition.item_id')
                    ->where('sg_payroll_item.user_id', $employee->user->id)
                    ->where('sg_payroll_item.year', $inputs['year'])
                    ->whereIn('sg_payroll_item_addition.addition_id', function($query){
                            $query->select('id')
                                ->from('payroll_additions')
                                ->where('title', 'Sales Commissions');
                    })
                    ->addSelect(DB::raw("SUM(sg_payroll_item_addition.amount) as sales_commission"))
                    ->first();
            $sales_commission = !empty($sales_commission) ? $sales_commission->sales_commission : 0;

            // MBMF
            $mbmf = PayrollItem::join('sg_payroll_item_deduction', 'sg_payroll_item.id',  '=', 'sg_payroll_item_deduction.item_id')
                    ->where('sg_payroll_item.user_id', $employee->user->id)
                    ->where('sg_payroll_item.year', $inputs['year'])
                    ->where('sg_payroll_item_deduction.title', 'MBMF')
                    ->addSelect(DB::raw("SUM(sg_payroll_item_deduction.amount) as mbmf"))
                    ->first();
            $mbmf = !empty($mbmf) ? $mbmf->mbmf : 0;

            // Donation
            $donation = PayrollItem::join('sg_payroll_item_deduction', 'sg_payroll_item.id',  '=', 'sg_payroll_item_deduction.item_id')
                    ->where('sg_payroll_item.user_id', $employee->user->id)
                    ->where('sg_payroll_item.year', $inputs['year'])
                    ->whereIn('sg_payroll_item_deduction.title', array('CDAC','ECF','SINDA'))
                    ->addSelect(DB::raw("SUM(sg_payroll_item_deduction.amount) as donation"))
                    ->first();
            $donation = !empty($donation) ? $donation->donation : 0;

            $others = $sales_commission + $pension + $transport_allowance + $entertainment_allowance + $other_allowance + $notice_pay + $retirement_accrued_1993 + $contribution_made_by_employer + $excess_voluntary_cpf + $gains_profits_1_g;

            $compensation_for_loss = is_numeric($compensation_for_loss) ? number_format($compensation_for_loss, 2, '', '') : '';
            $apprv_obtained_by_iras = is_numeric($apprv_obtained_by_iras) ? number_format($apprv_obtained_by_iras, 2, '', '') : '';
            $retirement_accrued_1993 = is_numeric($retirement_accrued_1993) ? number_format($retirement_accrued_1993, 2, '', '') : '';
            $retirement_accrued_31_12_92 = is_numeric($retirement_accrued_31_12_92) ? number_format($retirement_accrued_31_12_92, 2, '', '') : '';
            $contribution_made_by_employer = is_numeric($contribution_made_by_employer) ? number_format($contribution_made_by_employer, 2, '', '') : '';
            $excess_voluntary_cpf = is_numeric($excess_voluntary_cpf) ? number_format($excess_voluntary_cpf, 2, '', '') : '';
            $employer_contribution = is_numeric($employer_contribution) ? number_format($employer_contribution, 2, '', '') : '';
            $employee_contribution = is_numeric($employee_contribution) ? number_format($employee_contribution, 2, '', '') : '';
            $gains_profits_1_b = is_numeric($gains_profits_1_b) ? number_format($gains_profits_1_b, 2, '', '') : '';
            $gains_profits_1_g = is_numeric($gains_profits_1_g) ? number_format($gains_profits_1_g, 2, '', '') : '';
            $sales_commission = is_numeric($sales_commission) ? number_format($sales_commission, 2, '', '') : '';
            $pension = is_numeric($pension) ? number_format($pension, 2, '', '') : '';
            $transport_allowance = is_numeric($transport_allowance) ? number_format($transport_allowance, 2, '', '') : '';
            $entertainment_allowance = is_numeric($entertainment_allowance) ? number_format($entertainment_allowance, 2, '', '') : '';
            $other_allowance = is_numeric($other_allowance) ? number_format($other_allowance, 2, '', '') : '';
            $notice_pay = is_numeric($notice_pay) ? number_format($notice_pay, 2, '', '') : '';
            $compensation_for_loss = is_numeric($compensation_for_loss) ? number_format($compensation_for_loss, 2, '', '') : '';

            $details .= "\n";
            $details .= '1'; //Record type

            $id_type = '';
            if($employee->id_type == 'NRIC')
                $id_type = '1';
            if($employee->id_type == 'FIN')
                $id_type = '2';
            if($employee->id_type == 'IMS')
                $id_type = '3';
            if($employee->id_type == 'WP')
                $id_type = '4';
            if($employee->id_type == 'MIC')
                $id_type = '5';
            if($employee->id_type == 'Passport no')
                $id_type = '6';

            $details .= $id_type; //ID type
            $details .= str_pad($employee->id_number, 12); //ID number
            $details .= str_pad($employee->full_name_1, 40);
            $details .= str_pad($employee->full_name_2, 40);

            $address_type = '';
            if($employee->address == 'local')
                $address_type = 'L';
            if($employee->address == 'foreign')
                $address_type = 'F';
            if($employee->address == 'local c/o')
                $address_type = 'C';

            $details .= $address_type;

            //if address is local add required/optional item for local
            if($employee->address == 'local'){
                $details .= str_pad($employee->block_house, 10);
                $details .= str_pad($employee->street, 32);
                $details .= str_pad($employee->level, 3);
                $details .= str_pad($employee->unit, 5);
                $details .= str_pad($employee->postal_code, 6);
            } else {
                $details .= str_pad('', 10);
                $details .= str_pad('', 32);
                $details .= str_pad('', 3);
                $details .= str_pad('', 5);
                $details .= str_pad('', 6);
            }

            $details .= str_pad($employee->line_1, 30);
            $details .= str_pad($employee->line_2, 30);
            $details .= str_pad($employee->line_3, 30);
            $details .= str_pad('', 6); // 6i) Postal code for unformatted address

            if($employee->address != 'local')
                $details .= str_pad($employee->country, 3); // 6j) Country
            else
                $details .= str_pad('', 3); // 6j) Country

            $details .= str_pad($employee->Nationality, 3); // 7) Nationality
            $details .= str_pad($employee->sex, 1); // 8) Sex
            $details .= str_pad(str_replace('-', '', $employee->dob), 8); // 9) Date of Birth

            $amount = $salary + $others + $bonuses + $director_fee;
            $details .= str_pad(str_replace(".", "", $amount), 9); // 10) Amount
            // Period of Payment

            $payrolls = PayrollItem::where('user_id', $employee->user->id)->where('year', $year)->orderBy('created_at', 'asc')->get();

            // $payroll_start_month = ($payrolls[0]->month < 10) ? '0'.$payrolls[0]->month : $payrolls[0]->month;
            $start_date = $payrolls[0]->payroll->year.'0101';
            $details .= str_pad($start_date, 8); // 11a) Start date

            // $payroll_end_month = ($payrolls[$payrolls->count()-1]->month < 10) ? '0'.$payrolls[$payrolls->count()-1]->month : $payrolls[$payrolls->count()-1]->month;
            $to_date = $payrolls[$payrolls->count()-1]->payroll->year.'1231';
            $details .= str_pad($to_date, 8); // 11b) to date

            $total_mbmf += $mbmf;
            $details .= str_pad(intval($mbmf), 5); // 12) Mosque building fund

            $total_donation += $donation;
            $details .= str_pad(intval($donation), 5); // 13) Donation

            $total_cpf += $cpf;
            $details .= str_pad(intval($cpf), 7); // 14) CPF designation

            $total_insurance += $life_insurance;
            $details .= str_pad(intval($life_insurance), 5); // 15) Insurance

            $total_salaries += $salary;
            $details .= str_pad(intval($salary), 9); // 16) Salary

            $total_bonuses += $bonuses;
            $details .= str_pad(intval($bonuses), 9); // 17) Bonus

            $total_director_fee += $director_fee;
            $details .= str_pad(intval($director_fee), 9); // 18) Director's Fee

            $total_other += $others;
            $details .= str_pad(intval($others), 9); // 19) Others

            $details .= str_pad(intval($gains_profits_1_g), 9); // 19a) Grains and Profit share
            $details .= str_pad('', 9); // 20) Exempt Income
            $details .= str_pad('', 9); // 21) Amount of Employment income
            $details .= str_pad('', 9); // 22) Fixed amount of income tax
            $details .= str_pad('', 1); // 23) Benifits in kind
            $details .= str_pad('', 1); // 24) Section 45 indicator
            $details .= str_pad('', 1); // 25) Employee income tax

            $gratuity_notice_pay = ($compensation_for_loss != 0) ? 'Y' : '';
            $details .= str_pad($gratuity_notice_pay, 1); // 26) Gratuity/Notic pay

            $compensation_for_loss_indication = ($apprv_obtained_by_iras != 0 || $compensation_for_loss != 0) ? 'Y' : '';
            $details .= str_pad($compensation_for_loss_indication, 1); // 27) Compensession for loss
            $details .= str_pad($compensation_for_loss_indication, 1); // 27a) Approval Obtained
            $details .= str_pad($date_of_approval, 8); // 27b) Date of Approval
            $details .= str_pad('', 1); // 28) Cesistion Provision indicator
            $details .= str_pad('', 1); // 29) Form ir8s indicator
            $details .= str_pad('', 1); // 30) Remision/Exempt/Non taxable indicator
            $details .= str_pad('', 1); // 30a) Compensation and Graduity
            $details .= str_pad($sales_commission, 11); // 31) Gross commision
            // Period of gross commision
            $details .= str_pad($sales_commision_from, 8); // 32a) From Date
            $details .= str_pad($sales_commision_to, 8); // 32b) to Date
            $details .= str_pad('B', 1); // 33) Gross commision indicator
            $details .= str_pad($pension, 11); // 34) Pension
            $details .= str_pad($transport_allowance, 11); // 35) Transport Allowance
            $details .= str_pad($entertainment_allowance, 11); // 36) Entertainment Allowance
            $details .= str_pad($other_allowance, 11); // 37) Oter allowances
            $details .= str_pad($notice_pay, 11); // 38) Gratuity/Notice pay
            $details .= str_pad($compensation_for_loss, 11); // 38a) Compensasion for loss on office
            $details .= str_pad($retirement_accrued_31_12_92, 11); // 39) Retirement benefits accrued up to 31.12.92
            $details .= str_pad($retirement_accrued_1993, 11); // 40) Retirement benefits accrued from 1993
            $details .= str_pad($contribution_made_by_employer, 11); // 41) Contribution made by employer to any pension
            $details .= str_pad($excess_voluntary_cpf, 11); // 42) Excess
            $details .= str_pad($gains_profits_1_b, 11); // 43) Gains and profits from share
            $details .= str_pad('', 11); // 44) Value of benefits in kind
            $details .= str_pad('', 7); // 45) E'yees' voluntary contribution
            $details .= str_pad($employee->job_title, 30); // 46) Designation
            $details .= str_pad('', 8); // 47) Date of commencement
            $details .= str_pad('', 8); // 48) Date of Cessation
            $details .= str_pad($date_of_declaration_bonus, 8); // 49) Date of declaration bonus
            $details .= str_pad($date_of_approval_director_fee, 8); // 50) Date of approval of director's fee
            $details .= str_pad($name_of_fund, 60); // 51) Name of the fund for retirement benefits
            $pension_provident = ($cpf != 0) ? 'Central Provident Fund' : '';
            $details .= str_pad($pension_provident, 60); // 52) Name of Designated pension provident
            $details .= str_pad($employee->bank, 1); // 53) Name of the bank
            $details .= str_pad('', 8); // 54) Date of payroll
            $details .= str_pad('', 393); // 55) Fillers
            $details .= str_pad('', 50); // 56) Field Reserved
        }

        return array(
                'details'           => $details,
                'trailer_details'   => array(
                        'num_of_records' => count($sgemployees),
                        'total_cpf'    => $total_cpf,
                        'salaries'      => $total_salaries,
                        'bonuses'       => $total_bonuses,
                        'director_fee'  => $total_director_fee,
                        'others'        => $total_other,
                        'total_insurance' => $total_insurance,
                        'total_mbmf'   => $total_mbmf,
                        'total_donation' => $total_donation
                    )
            );
    }

    private function trailerIRAS8A($year, $infomation)
    {
        $company = session('current_company');
        $trailer = '';

        $payments = $infomation['salaries'] + $infomation['bonuses'] + $infomation['director_fee'] + $infomation['others'];
        $trailer .= "\n";
        $trailer .= '2'; //Record type
        $trailer .= str_pad($infomation['num_of_records'], 6); //Number of records
        $trailer .= str_pad(intval($payments), 12); //Total amount of payments
        $trailer .= str_pad(intval($infomation['salaries']), 12); // 4) Total amount of salaries
        $trailer .= str_pad(intval($infomation['bonuses']), 12); // 5) Total amount of bonus
        $trailer .= str_pad(intval($infomation['director_fee']), 12); // 6) Total amount of director's fee
        $trailer .= str_pad(intval($infomation['others']), 12); // 7) Total amount of others
        $trailer .= str_pad('', 12); //Total amount of exempt income
        $trailer .= str_pad('', 12); //Total amount of employment income
        $trailer .= str_pad('', 12); //Total amount of income tax liability
        $trailer .= str_pad(intval($infomation['total_donation']), 12); //Total amount of donation
        $trailer .= str_pad(intval($infomation['total_cpf']), 12); //Total amount of CPF
        $trailer .= str_pad(intval($infomation['total_insurance']), 12); //Total amount of Insurance
        $trailer .= str_pad(intval($infomation['total_mbmf']), 12); //Total amount of MBF
        $trailer .= str_pad('', 1049); //Fillers

        return $trailer;
    }
}