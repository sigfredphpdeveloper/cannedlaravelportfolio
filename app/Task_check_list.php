<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task_check_list extends Model
{
    protected $table = 'tasks_check_list';

    protected $fillable = [
        'task_id',
        'check_list_title'
    ];
}
