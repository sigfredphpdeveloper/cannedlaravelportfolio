<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deal_teams extends Model
{
    protected $table = 'deal_teams';

    protected $fillable = [
        'deal_id',
        'team_id'
    ];



    public function deal()
    {
        return $this->belongsTo('App\Deal');
    }

}
