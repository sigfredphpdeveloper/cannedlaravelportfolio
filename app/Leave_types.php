<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leave_types extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'leave_types';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     *
     *
     */
    protected $fillable = [
        'company_id',
        'type',
        'description',
        'default_allowance',
        'color_code',
        'start_date_allowance',
        'created_at',
        'updated_at',
        'roll_over',
        'advance_leave'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The fields that should be treated as Carbon date instances
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];


    public function Company()
    {
        return $this->belongsTo('App\Company');
    }

    public function leave_types_roles()
    {
        return $this->hasMany('App\LeaveTypesRoles','leave_type_id');
    }

    public function user_leave_allowance(){
        return $this->hasMany('App\UserLeaveAllowance','leave_types_id');
    }

    public function Users_leaves()
    {
        return $this->belongsTo('App\Users_leaves');
    }

}
