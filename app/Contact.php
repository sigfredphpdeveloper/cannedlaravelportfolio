<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use DB;

class Contact extends Model
{
    protected $table = 'contacts';

    use SearchableTrait;

    protected $fillable = [
        'owner_id',
        'organization_id',
        'contact_name',
        'contact_email',
        'contact_phone',
        'position',
        'address',
        'contact_notes',
    ];

    protected $searchable = [
        'columns' => [
            'contacts.contact_name' => 10,
            'contacts.contact_email' => 10,
            'contacts.position' => 10,
            'contacts.address' => 10,

            'organization.organization_name' => 10,
            'organization.organization_address' => 10,
            'organization.visibility' => 5,

//            'users.first_name' => 10,
//            'users.last_name' => 10,
//            'users.title' => 10,
//            'users.email' => 10
        ],
        'joins' => [
            'users' => ['contacts.owner_id','users.id'],
            'organization' => ['organization.id','contacts.organization_id'],
        ],
    ];

    public function organization()
    {
        return $this->belongsTo('App\Organization');
    }

    public static function getCrmContacts()
    {
        $company = session('current_company');
        return Contact::select('*','contacts.id as id','contacts.id as contact_id') //DB::raw('count(deals.contact_id) as deals_nb')
            ->join('organization AS org','contacts.organization_id','=','org.id')
//            ->join('teams', 'teams.company_id','=','org.company_id')
//            ->leftJoin('deals', function($join){
//                $join->on('contacts.id','=','deals.contact_id');
//                $join->where('deals.status', '!=', 'DELETE');
//            })
            ->join('users','org.owner_id','=','users.id')
            ->where('org.company_id',$company->id)
            ->groupBy('contacts.id');
    }


}
