<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payroll_roles extends Model
{
    public $timestamps = false;

    protected $table = 'payroll_roles';

    protected $fillable = [
        'role_id',
        'status',
    ];

    public function roles()
    {
        return $this->belongsTo('App\Roles');
    }
}
