<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User_fields extends Model
{

    public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_fields';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
        'field_name',
        'field_description',
        'field_type',
        'field_value',
        'field_visibility'
    ];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The fields that should be treated as Carbon date instances
     *
     * @var array
     */
    protected $dates = [];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

}
