<?php

namespace App\Classes;

use App\Classes\Notification;

class ChatMessageNotification extends Notification{

	protected $chat_line;

	function __construct($rooms,$chat_line){
		$this->chat_line = $chat_line;
		parent::__construct($rooms,'chat.message');
	}

	/**
	 * Creates a chat message notification from a chat and sender by assuming the last message is the notification you want to send
	 */
	public static function createFromChat($chat_line, $chat){

		$users_to_notify = $chat->users()->get();

		$broadcastRooms = [];
		foreach($users_to_notify as $user){
			$broadcastRooms[] = 'user:' . $user->id;
		}

		return new ChatMessageNotification($broadcastRooms, $chat_line->load('user')->toArray());
	}
}