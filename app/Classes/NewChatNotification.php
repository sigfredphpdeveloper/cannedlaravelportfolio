<?php

namespace App\Classes;

use App\Classes\Notification;
use App\Chat;

class NewChatNotification extends Notification{

	protected $chat;

	function __construct($chat,$current_user){
		$chat->load('users','chat_lines');

		$reverse = true;
		$chat->chat_name = Chat::determineGroupName($chat,$current_user,$reverse);

		$this->chat = $chat;

		$rooms = [];
		foreach ($chat->users as $user) {
			$rooms[] = 'user:'.$user->id;
		}
		parent::__construct($rooms,'chat.new');
	}
}