<?php

namespace App\Classes;

use Illuminate\Support\Collection;
use Carbon\Carbon;
use App\Chat;
use stdClass;
use Auth;

class ChatsResponse{

	public $chats;

	/**
	 * @param Collection $group_chats - collection of group chats
	 * @param Collection $one_to_one_chats - collection of one_to_one_chats
	 * @param Array $users_for_empty_chat - array of users_for_empty_chat
	 */
	function __construct($group_chats, $one_to_one_chats, $users_for_empty_chat,$current_user){

		$this->chats = collect();
		foreach ($group_chats as $group_chat) {

			if(!$group_chat->chat_name){

				$group_chat->chat_name = Chat::name($group_chat,$current_user);
			}
			$group_chat->hasGuest= Chat::hasGuest($group_chat);
			$group_chat->photo = Chat::determineGroupPhoto($group_chat,$current_user);
			$this->chats->push($group_chat);
		}

		foreach ($one_to_one_chats as $one_to_one_chat) {
			$one_to_one_chat->chat_name = Chat::name($one_to_one_chat,$current_user);
			$one_to_one_chat->hasGuest= Chat::hasGuest($one_to_one_chat);
			$one_to_one_chat->photo = Chat::determineGroupPhoto($one_to_one_chat,$current_user);
			$this->chats->push($one_to_one_chat);
		}

		foreach ($users_for_empty_chat as $user) {
			$empty_chat = new stdClass();
			$empty_chat->id = null;
			$empty_chat->chat_type = "empty";
			$empty_chat->users = [$current_user,$user];
			$empty_chat->chat_lines = [];
			$empty_chat->updated_at = null;
			$empty_chat->chat_name = Chat::name($empty_chat,$current_user);
			$empty_chat->hasGuest= Chat::hasGuest($empty_chat);
			$empty_chat->photo = Chat::determineGroupPhoto($empty_chat,$current_user);
			$this->chats->push($empty_chat);
		}

		$this->chats = $this->chats->sortByDesc(function ($chat, $key) {
			if($chat->updated_at){
				return $chat->updated_at->timestamp;
			}else{
				return -1;
			}
		});
	}

	public function toJson(){
		return $this->chats->values()->toJson();
	}
}

?>