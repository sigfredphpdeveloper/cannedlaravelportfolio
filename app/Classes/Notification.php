<?php

namespace App\Classes;

class Notification{

	protected $rooms;
	protected $event;

	/**
	 * @param Array $rooms - array of room name strings
	 */
	function __construct($rooms,$event){
		$this->rooms = $rooms;
		$this->event = $event;
	}

	function toArray(){
		$arr;
		foreach ($this as $key => $value) {
			$arr[$key] = $value;
		}
		return $arr;
	}

	function toJson(){
		return json_encode($this->toArray());
	}
}

?>