<?php

namespace App\Classes;

class CustomFields {

    function __construct(){
        parent::__construct();
    }


    function format_field_name($field_name = NULL){

        $field_name = str_replace('_',' ',$field_name);
        $field_name = ucwords($field_name);

        return $field_name;

    }

}