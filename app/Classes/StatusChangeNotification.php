<?php

namespace App\Classes;

use App\Classes\Notification;

class StatusChangeNotification extends Notification{

	protected $chat;

	function __construct($chat,$current_user){
		$this->chat = $chat;

		$rooms = [];
		foreach ($chat->users as $user) {
			if ($user->id != $current_user->id) {
				$rooms[] = 'user:'.$user->id;
			}	
		}
		parent::__construct($rooms,'chat.seen');
	}

	/**
	 * Creates a chat message notification from a chat and sender by assuming the last message is the notification you want to send
	 *//*
	public static function createFromChat($chat_line, $chat){

		$users_to_notify = $chat->users()->get();

		$broadcastRooms = [];
		foreach($users_to_notify as $user){
			$broadcastRooms[] = 'user:' . $user->id;
		}

		return new StatusChangeNotification($broadcastRooms, $chat_line->load('user')->toArray());
	}*/
}