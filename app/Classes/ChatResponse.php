<?php

namespace App\Classes;

class ChatResponse{

	public $group_chats;
	public $one_to_one_chats;
	public $empty_chats;

	/**
	 * @param Collection $group_chats - collection of group chats
	 * @param Collection $one_to_one_chats - collection of one_to_one_chats
	 * @param Array $empty_chats - array of empty_chats
	 */
	function __construct($group_chats, $one_to_one_chats, $empty_chats){
		$this->group_chats = $group_chats;
		$this->one_to_one_chats = $one_to_one_chats;
		$this->empty_chats = $empty_chats;
	}

	function toArray(){
		$arr;
		foreach ($this as $key => $value) {
			$arr[$key] = $value;
		}
		return $arr;
	}

	function toJson(){
		return json_encode($this->toArray());
	}
}

?>