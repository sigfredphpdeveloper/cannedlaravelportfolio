<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personal_note extends Model
{
	protected $table = 'personal_note';

    protected $fillable = [
        'title',
        'owner_id',
        'content',
        'top_position',
        'left_position',
        'z_index_position',
        'height_position',
        'width_position'
    ];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\User', 'owner_id');
    }
}