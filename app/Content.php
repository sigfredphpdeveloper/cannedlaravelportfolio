<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Content extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'content';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
        'content_type',
        'content_name',
        'owner_id',
        'parent_content_id',
        's3_link',
        'meta_type',
        'bucket',
        'key_name',
        'file_size'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The fields that should be treated as Carbon date instances
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    public function content_permissions()
    {
        return $this->hasMany('App\Content_permissions');
    }


}
