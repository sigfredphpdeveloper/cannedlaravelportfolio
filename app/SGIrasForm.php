<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SGIrasForm extends Model
{
    protected $table = 'sg_iras_form';

    protected $fillable = [
        'company_id',
        'type',
        'file_url',
        'status'
    ];

    public function company()
    {
    	return $this->belongsTo('App\Company');
    }
}
