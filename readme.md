## fewStones/ilathys Clean Laravel

This is the repository to start working from when starting a new project in Laravel. More information/detailed step--by-step instructions will be added here as they become available.

### If new to Laravel
1. Install Composer
2. Run Composer install in the root repository directory

### First things first

1. Change the DocumentRoot of your web server to /clean-laravel/public (or use a virtualhost entry to point to the public directory, remember to restart web server and edit the host file to point the new virtualhost url to localhost)
2. Create your mysql database, then edit the .env file to put the right credentials
3. Run migrations (this will create the base db tables) :- php artisan migrate
4. There are no seed database yet, but should you need to seed the database :- php artisan db:seed (for example, if we have reference data in the future)
5. Laravel uses the PHP dotenv file, so modify the .env file (it's on .gitignore, so go ahead and enter your own local environment settings, keeping in mind you need a different one for production)
6. You should also generate a new application key:- php artisan key:generate
7. Verify that you can see the home page on your localhost (steps differ depending on your local set-up)

### Quick pointers
If new to Laravel, we recommend that you finish watching the entire Laracast Laravel 5 fundamentals, (it's free and very informative):-
[https://laracasts.com/series/laravel-5-fundamentals]

Views are in resources/views/ and things should already be nicely ordered and aptly named

Controllers are in /Http/Controllers/

In UserController, you should be able to see in general how things work (very simple controller but demonstrate some of the most common tasks, like getting the current logged in user, and the passing it to the view, and some stripe sample code)

Register a new user at /register, when you succeed you should be redirected to /dashboard, logout at /logout, verify that you can login again with the user that you just created at /login, verify that you have been brought to /dashboard. There is a page /upgrade within the login that demonstrate Stripe integration and Laravel cashier, which should be sufficient sample code for most projects.

## Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing powerful tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
