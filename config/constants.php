<?php
	
return [
	'MOBILE' 	=> 1,
	'TABLET'	=> 2,
	'DESKTOP'   => 3,
	'EVENT_ANNOUNCEMENT' => [
		'ADD_ROLE'		=>	'add_roles_announcement',
		'ADD_TEAM'		=>	'add_teams_announcement',
		'ADD_ALL'		=>	'add_all_announcement',
		'EDIT_ROLE'		=>	'edit_roles_announcement',
		'EDIT_TEAM'		=>	'edit_teams_announcement',
		'DELETE_ANNOUNCEMENT' => 'delete_announcement'
	],
	'EVENT_DIRECTORY' => [
		'ASSIGN_ROLE' => 'assign_roles_directory',
		'ASSIGN_TEAM' => 'assign_teams_directory',
		'ADD_DIRECTORY' => 'add_user_directory',
		'ACCEPT_INVITATION' => 'accept_invitation',
		'ACTIVATE_USER' => 'activate_user',
		'EDIT_DIRECTORY' => 'edit_user_directory',
		'DELETE_DIRECTORY' => 'delete_user_directory'
	]
];