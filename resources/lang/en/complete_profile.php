<?php

return [
'Complete Your Registration' => 'Complete Your Registration',
'Please fill in the form to configure the platform for your company (Step 1)' => 'Please fill in the form to configure the platform for your company)',
'Your Profile' => 'Your Profile',
'Position' => 'Position',
'Phone Number' => 'Phone Number',
'Optional' => 'Optional',
'Title' => 'Title',
'Mr' => 'Mr',
'Mrs.' => 'Mrs.',
'Ms.' => 'Ms.',
'First Name' => 'First Name',
'First Name *' => 'First Name *',
'Last Name' => 'Last Name',
'Last Name *' => 'Last Name *',
'Website' => 'Website',
'Company URL *' => 'Company URL *',
'Email' => 'Email',
'Email *' => 'Email *',
'Used to log in' => 'Used to log in',
'Password' => 'Password',
'Change Password' => 'Change Password',
'Profile Picture' => 'Profile Picture (recommended)',
'Update' => 'Update',
'step1'=>'Step 1',
'step2'=>'Step 2'
];
