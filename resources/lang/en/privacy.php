<?php

return [
	
	'Blog' => 'Blog',
	'Blog-url' => 'http://blog.zenintra.net',
	'Features' => 'Features',
	'Pricing' => 'Pricing',
	'Sign up' => 'Sign up',
	'Log in' => 'Log in',
	'Privacy-title' => 'Privacy Policy',
	'content'	=> '
	<p><strong><em>Last updated: January 11,2016</em></strong> </p>

	<strong>iLathys Pte Ltd</strong><br />
	71 Ayer Rajah Crescent, #05­08<br />
	Singapore, 139951

	<p>iLathys Pte  Ltd ("us", "we",  or "our")  is  a  technology  company  based  in Singapore who operates the <a href="http://ilathys.com" target="_blank">http://ilathys.com</a> website and is the owner of Zenintra platform (the "Service"). </p>

	<p>This page informs you of our policies regarding the collection, use and disclosure of Personal Information when you use our Service. </p>

	<p>We will  not  use  or  share  your  information  with  anyone  except  as  described  in  this 
	Privacy Policy. </p>

	<p>We use your Personal Information for providing and improving the Service. By using the Service,  you  agree  to  the  collection  and  use  of  information  in  accordance with this policy. Unless otherwise defined in this Privacy Policy, terms used in this Privacy Policy  have  the  same  meanings  as  in  our  Terms  and Conditions,  accessible  at <a href="http://zenintra.net" target="_blank">http://zenintra.net</a></p>

	<strong><u>Information Collection And Use</u></strong>
	<p>While  using  our  Service,  we  may  ask  you  to  provide  us  with  certain  personally identifiable  information  that  can  be  used  to  contact  or  identify  you.  Personally identifiable information may include, but is not limited to, your email address, name, other information ("Personal Information").</p>

	<p>We  store  and  maintain  documents,  files,  data  stored  in  your  user  account  at  our facilities in Singapore and other locations. We also keep backup copies of your data in case of errors or system failures from Zenintra webservices. </p>

	<p>We may retain and use your Personal Information and data as necessary to comply with our legal obligations, we assure you that the contents of your user account will not  be  disclosed  to  anyone,  except  in  circumstances  specifically  mentioned  in  this Privacy Policy Statement & Terms of Services. </p>

	<strong><u>Log Data</u></strong>
	<p>We collect information that your browser sends whenever you visit our Service ("Log Data").  This  Log  Data  may  include  information  such  as  your  computer\'s  Internet Protocol ("IP") address, browser type, browser version, the pages of our Service that you  visit, the time and date of  your  visit, the time  spent on those pages and other statistics.</p>

	<p>In addition, we may use third party  services  such as Google Analytics  that  collect, monitor  and  analyze  this  type  of  information  in  order  to  increase  our  Service\'s functionality.  These  third  party  service  providers  have  their  own  privacy  policies addressing how they use such information.</p>

	<strong><u>Cookie</u></strong>
	<p>Cookies  are  files  with  small  amount  of data,  which  may  include  an  anonymous unique  identifier.  Cookies  are  sent  to  your browser  from  a  web  site  and  stored  on your computer\'s hard drive.
	We use "cookies" to collect information. You can instruct your browser to refuse all cookies  or  to  indicate when  a  cookie  is  being  sent. However,  if  you  do  not  accept cookies, you may not be able to use some portions of our Service.</p>

	<strong><u>Behavioral Remarketing</u></strong>
	<p>iLathys Ltd  uses remarketing  services  to advertise  on  third  party  web  sites  to  you after you visited our Service. We, and our third party vendors, use cookies to inform, optimize and serve ads based on your past visits to our Service. This cookie does not in any way identify you or give access to your computer.</p>

	­ <strong><u> -Google </u></strong>
	<p>Google AdWords remarketing service is provided by Google Inc. You  can  opt­out  of  Google  Analytics  for  Display  Advertising  and  customize  the Google  Display  Network  ads  by  visiting  the  Google  Ads  Settings  page: http://www.google.com/settings/ads <br />Google also recommends installing the Google Analytics Opt­out Browser Add­on ­  https://tools.google.com/dlpage/gaoptout ­  for  your  web  browser.  Google  Analytics Opt­out Browser Add­on provides  visitors with the ability to prevent their data from being collected and used by Google Analytics. <br />For  more  information  on  the  privacy  practices  of  Google,  please  visit  the  Google Privacy & Terms web page: <a href="http://www.google.com/intl/en/policies/privacy/" target="_blank">http://www.google.com/intl/en/policies/privacy/</a></p>

	<strong><u> -­ Facebook </u></strong>
	<p>You  can  opt  out  of  Facebook  tracking  at  any  time  by  visiting <a href="https://www.facebook.com/ads/website_custom_audiences/" target="_blank">https://www.facebook.com/ads/website_custom_audiences/</a></p>

	<strong><u>Service Providers</u></strong>
	<p>We  may  employ  third  party  companies  and  individuals  to  facilitate  our  Service,  to provide the Service on our behalf, to perform Service­related services or to assist us in analyzing how our Service is used. <br />
	These third parties have access to your Personal Information only to perform these tasks on our behalf and are obligated not to disclose or use it for any other purpose.</p>

	<strong><u>Communications</u></strong>
	<p>We may use your Personal Information to contact you with newsletters, marketing or promotional materials and other information that may be of interest to you. You may opt  out  of  receiving  any,  or  all,  of  these  communications  from  us  by  following  the unsubscribe link or instructions provided in any email we send.</p>

	<strong><u>Compliance With Laws</u></strong>
	<p>Our Service is in accordance with the <strong>Singaporean Personal Data Protection Act (PDPA)</strong> which stipulates that Organisations may: </p>
	1) Collect, use or disclose personal data only with the individual’s knowledge and consent <br />
	2) Collect,  use  or  disclose  personal  data  in  an  appropriate  manner  for  the circumstances, and only if they have informed the individual of purposes for the collection, use or disclosure ; and <br />
	3) Collect,  use  or  disclose  personal  data  only  for  purposes  that  would  be considered appropriate to a reasonable person in the given circumstances. 

	<p>Singapore  users  will  have  the  right  to  access  their  data  and  ask  this  data  to  be removed. </p>

	<p>Non­Singaporean  users  will  need  to  comply  with  their  own  local  regulations  in general and in particular in terms of protection of personal data including for users created or imported by them.   </p>

	<p>We  will  disclose  your  Personal  Information  where  required  to  do  so  by  law  or subpoena or if we believe that such action is necessary to comply with the law and the reasonable requests of law enforcement or to protect the security or integrity of our Service.</p>

	<strong><u>Business Transaction</u></strong>
	<p>If  iLathys Pte  Ltd  is  involved  in  a  merger,  acquisition  or  asset  sale,  your Personal Information  may  be  transferred.  We  will  provide  notice  before  your  Personal Information is transferred and becomes subject to a different Privacy Policy.</p>

	<strong><u>Security</u></strong>
	<p>The security of your Personal Information is important to us, but remember that no method  of  transmission  over  the  Internet,  or  method  of  electronic  storage  is  100% secure.  While  we  strive  to  use  commercially  acceptable  means  to  protect  your Personal Information (use of HTTPS service for example), we cannot guarantee its absolute security.</p>

	<strong><u>International Transfer</u></strong>
	<p>Your  information,  including  Personal  Information,  may  be  transferred  to  —  and maintained on — computers located outside of your state, province, country or other governmental jurisdiction where the data protection laws may differ than those from your jurisdiction. If you are located outside Singapore and choose to provide information to us, please note  that  we  transfer  the  information,  including Personal  Information,  to Singapore and process it there.</p>

	<p>Your consent to this Privacy Policy followed by your submission of such information represents your agreement to that transfer.</p>

	<strong><u>Links To Other Sites</u></strong>
	<p>Our Service may contain links to other sites that are not operated by us. If you click on a third party link, you will be directed to that third party\'s site. We strongly advise you to review the Privacy Policy of every site you visit. We  have  no  control  over,  and  assume  no  responsibility  for  the  content,  privacy policies or practices of any third party sites or services.</p>

	<strong><u>Children\'s Privacy</u></strong>
	<p>Our Service does not address anyone under the age of 13 ("Children"). We  do  not  knowingly  collect  personally  identifiable  information  from  children  under 13.  If  you  are  a  parent  or  guardian  and  you  are  aware  that  your  Children  has provided us with Personal Information, please contact us. If we become aware that we  have  collected  Personal  Information  from  a  children  under  age  13  without verification  of parental  consent, we take  steps  to remove that  information  from  our servers.</p>

	<strong><u>Investigation of illegal activity:</u></strong>
	<p>By  registering  and  using  our  Service,  you  agree  to  be  solely  responsible  for  the contents  of  your  transmissions  through  the  Service.  You  agree  not  to  use  the Services  for  illegal  purposes  or  for  the transmission  of  material  that  is  unlawful, defamatory,  harassing,  invasive  of  another’s  privacy,  abusive,  harmful,  vulgar, obscene,  promotes  racism,  or  that  which  infringes  or  may  infringe  intellectual property or other rights of another. </p>

	<p>You agree not to use the Service for the transmission of “junk mail”, “spam”, “chain letters” or unsollicited mass distribution of email. </p>

	<p>We reserve the right to terminate your access to the services if there are reasonable grounds to believe that  you have used the Services for any illegal or unauthorized activity.  </p>

	<strong><u>Changes To This Privacy Policy</u></strong>
	<p>We  may  update  our  Privacy  Policy  from  time  to  time.  We  will  notify  you  of  any changes by posting the new Privacy Policy on this page.</p>

	<p>You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.</p>

	<strong><u>Contact us</u></strong>
	<p>If  you  have  any  questions  about  this  Privacy  Policy,  please  contact  us  via 
	Zenintra.net online platform.</p>
	',
	'testi-text1' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
	'testi-title2' => 'Testimonial goes here',
	'testi-text2' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
	'Footer-Privacy-Policy' => 'Privacy Policy',
	'Footer-Terms-of-use' => 'Terms of use',
	'Footer-About-us' => 'About us',
	'Footer-Contact-us' => 'Contact us',
	'Footer-Support' => 'Support',
	'Footer-Faq' => 'FAQ',
	'Footer-Follow-us' => 'Follow us on',
	'Facebook-url' => 'https://www.facebook.com/zenintra',
	'Twitter-url' => 'https://twitter.com/zenintra',
	'Youtube-url' => 'http://www.youtube.com/c/ZenintraNetOfficial',
	'Linkedin-url' => 'https://www.linkedin.com/company/zenintra-net?trk=company_logo',
	'Contact Us' => 'Contact Us',
    'Copyright' => 'Copyright ' . date("Y"). ' Zenintra.net',
    //Contact Us Modal
	'Close' => 'Close',
    'Name'	=> 'Name',
    'Email' => 'Email',
    'Message' => 'Message',
	'John Smith' => 'John Smith',
	'sample-email' => 'jsmith@gmail.com',
	'What can we do for you?' => 'What can we do for you?',
	'Submit' => 'Submit',
    'Your message was successfully sent! We will get back to you soon\.' => 'Your message was successfully sent! We will get back to you soon\.',
    'Please fill out all fields.' => 'Please fill out all fields.',
    'Free intranet and internal communication for startups and small businesses'=> 'Free intranet and internal communication for startups and small businesses'
];