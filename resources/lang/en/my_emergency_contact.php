<?php

return [
'Emergency Contact' => 'Emergency Contact',
'Contact Name' => 'Contact Name',
'Phone Number' => 'Phone Number',
'Address' => 'Address',
'Relationship' => 'Relationship',
'Spouse' => 'Spouse',
'Parent' => 'Parent',
'Child' => 'Child',
'Friend' => 'Friend',
'Other' => 'Other',
'Other Relationship' => 'Other Relationship',
'Save' => 'Save'
];
