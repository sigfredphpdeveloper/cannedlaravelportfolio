<?php

return [
'Confirmation' => 'Confirmation',
'Warning' => 'Warning',
'This Expense type may be associated already with expenses.' => 'This Claim type may be associated already with expenses.',
'Are you sure you want to delete this Expense Type ?' => 'Are you sure you want to delete this Claim Type ?',
'No' => 'No',
'Yes' => 'Yes'
];
