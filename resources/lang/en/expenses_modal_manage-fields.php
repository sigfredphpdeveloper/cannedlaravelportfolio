<?php

return [
'Manage Fields for' => 'Manage Fields for',
'Note:' => 'Note:',
'Amount' => 'Amount',
'data-type is constant. It will be use to compute the total expenses.' => 'data-type is constant. It will be use to compute the total claims.',
'Label' => 'Label',
'Select' => 'Select',
'Text' => 'Text',
'Number' => 'Number',
'Date' => 'Date',
'Textarea' => 'Textarea',
'File' => 'File',
'Required' => 'Required',
'Save' => 'Save'
];

