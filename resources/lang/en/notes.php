<?php

return [
'Notes' => 'Notes',
'All Notes' => 'All Notes',
'ID' => 'ID',
'Title' => 'Title',
'Content' => 'Content',
'Theme' => 'Theme',
'Action' => 'Action',
'Delete' => 'Delete',
'View' => 'View',
'Filter ID' => 'Filter ID',
'Filter Name' => 'Filter Name',
'Filter Content' => 'Filter Content',
'Filter Theme' => 'Filter Theme',
'Delete Note' => 'Delete Note',
'Are you sure you want to delete this note?' => 'Are you sure you want to delete this note?',
'No' => 'No',
'Yes' => 'Yes',
];
