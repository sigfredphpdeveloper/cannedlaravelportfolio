<?php

return [
'Manage Announcements' => 'Manage Announcements',
'Add Announcement' => 'Add Announcement',
'Title' => 'Title',
'Message' => 'Message',
'Action' => 'Action',
'Edit' => 'Edit',
'Delete' => 'Delete',
'Confirmation' => 'Confirmation',
'Are you sure you want to delete this announcement ?' => 'Are you sure you want to delete this announcement ?',
'No' => 'No',
'Yes' => 'Yes',
'Posted Date' => 'Posted Date',
'View More...' => 'View More...',
];
