<?php

return [
'Approve Expenses' => 'Approve Claims',
'Update Status' => 'Update Status',
'Pending' => 'Pending',
'Approve' => 'Approve',
'Reject' => 'Reject',
'Request Details' => 'Request Details',
'Cancel' => 'Cancel',
'Total Amount' => 'Total Amount',
'Status' => 'Status',
'Creation Date' => 'Creation Date',
'Paid Date' => 'Paid Date',
'Approval Date' => 'Approval Date',
'No Expenses Line' => 'No Claims Line',
'Add Request Details Text' => 'Add Request Details Text',
'Details' => 'Details',
'Save' => 'Save',
'Update' => 'Update'
];

