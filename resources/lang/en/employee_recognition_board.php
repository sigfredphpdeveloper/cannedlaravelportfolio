<?php 

return [
	'Bulk Delete' => 'Bulk Delete',
	'Configure'	=> 'Configure',
	'Add New Recognition' => 'Add New Recognition',
	'Employee Name' => 'Employee Name',
	'Recognition Date' => 'Recognition Date',
	'Status' => 'Status',
	'Add New Recognition' => 'Add New Recognition',
	'Title' => 'Title',
	'Message' => 'Message',
	'Submit' => 'Submit',
	'Cancel' => 'Cancel',
	'Edit A Recognition' => 'Edit A Recognition',
	'Visible' => 'Visible',
	'Hidden' => 'Hidden',
	'Confirmation' => 'Confirmation',
	'Are you sure you want to delete this record ?' => 'Are you sure you want to delete this record ?',
	'Yes' => 'Yes',
	'No' => 'No',
	'Save' => 'Save',
	'Manage Permission' => 'Manage Permission',
	'Recognition Module' => 'Recognition Module',
	'Role' => 'Role',
	'Access To Recognition Module' => 'Access To Recognition Module'
];