<?php

return [
'CRM Module' => 'CRM Module',
'Company Stages' => 'Company Stages',
'Add a stage' => 'Add a stage',
'Edit Rank' => 'Edit Rank',
'Stages' => 'Stages',
'Description' => 'Description',
'Rank' => 'Rank',
'Edit' => 'Edit',
'Delete' => 'Delete'

];

