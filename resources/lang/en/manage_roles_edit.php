<?php

return [
'Back' => 'Back',
'Edit Role' => 'Edit Role',
'Warning.' => 'Warning.',
'Role Name' => 'Role Name',
'Role Description' => 'Role Description',
'Save' => 'Save'
];

