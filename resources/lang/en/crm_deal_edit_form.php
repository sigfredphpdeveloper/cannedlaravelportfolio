<?php

return [
'Deal Title' => 'Deal Title',
'Contact Name' => 'Contact Name',
'Organization Name' => 'Organization Name',
'Stage' => 'Stage',
'Stage 1' => 'Stage 1',
'Stage 2' => 'Stage 2',
'Stage 3' => 'Stage 3',
'Stage X' => 'Stage X',
'Visibility' => 'Visibility',
'Me' => 'Me',
'Team' => 'Team',
'Every One' => 'Every One',
'Expected Value' => 'Expected Value',
'Close' => 'Close',
'Save changes' => 'Save changes'
];
