<?php

return [
'Organizations' => 'Organizations',
'Add' => 'Add',
'Search' => 'Search',
'Organization Name' => 'Organization Name',
'Nb of Contact' => 'Nb of Contact',
'Owner' => 'Owner',
'Actions' => 'Actions',
'Edit' => 'Edit',
'Delete' => 'Delete',
'Search for...' => 'Search for...',
'Organization Address' => 'Organization Address',
'Visibility' => 'Visibility',
'Me' => 'Me',
'Team' => 'Team',
'Every One' => 'Everyone',
'Delete' => 'Delete',
'Close' => 'Close',
'Save changes' => 'Save changes',
'Confirmation' => 'Confirmation',
'Are you sure you want to delete this record ?' => 'Are you sure you want to delete this record ?',
'No' => 'No',
'Yes' => 'Yes',
'Add a new Organization' => 'Add a new Organization',
'Customize' => 'Customize',
'View Contacts' => 'View Contacts',
'View Organizations' => 'View Organizations',
'organization_count_info' => ':total Organization|:total Organizations',

];

