<?php

return [
"Personalized Settings for Employee's Profile" => "Personalized Settings for Employee's Profile",
'Add Field' => 'Add Field',
'Up to 10 fields' => 'Up to 10 fields',
'Field Name' => 'Field Name',
'Description' => 'Description',
'Type' => 'Type',
'Visibility' => 'Visibility',
'Action' => 'Action',
'Edit' => 'Edit',
'Delete' => 'Delete',
'Confirmation' => 'Confirmation',
'Are you sure you want to delete this record ?' => 'Are you sure you want to delete this record ?',
'No' => 'No',
'Yes' => 'Yes'


];
