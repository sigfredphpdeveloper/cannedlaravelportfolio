<?php

return [
'Customize' => 'Customize',
'No Trainings' => 'No Trainings',
'Add New Topic' => 'Add New Training',
'Topics' => 'Trainings',
'All Company Topics' => 'All Company Trainings',
'All Topics' => 'All Trainings',
'Internal Trainings' => 'Internal Trainings',
'No Intenal Trainings' => 'No Intenal Trainings',
'External Trainings' => 'External Trainings',
'No External Trainings' => 'No External Trainings',
'All Pages' => 'All Pages',
'Add New Page' => 'Add New Page',
'Add New Topic' => 'Add New Training',
'Internal' => 'Internal',
'External' => 'External',
'Are you sure you want to delete this topic' => 'Are you sure you want to delete this topic',
'Pages under this will be deleted also' => 'Pages under this will be deleted also'
];
