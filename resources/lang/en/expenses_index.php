<?php

return [
'My Expenses' => 'My Claims',
'My Expenses List' => 'My Claims List',
'Submit a New Expense' => 'Submit a New Claim',
'Submision Date' => 'Submision Date',
'Expense ID' => 'Claim ID',
'Title' => 'Title',
'Total Amount' => 'Total Amount',
'Status' => 'Status',
'Approve Date' => 'Approval Date',
'Paid Date' => 'Paid Date',
'Action' => 'Action',
'View' => 'View',
'Edit' => 'Edit',
'Delete' => 'Delete',
'No Expenses' => 'No Claims',
'Customize'	=> 'Customize',
'Expenses History' => 'Claims History',
'Expenses List' => 'Claims List'
];
