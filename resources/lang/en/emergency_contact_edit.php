<?php

return [
'Contact Name' => 'Contact Name',
'Phone Number' => 'Phone Number',
'Address' => 'Address',
'Relationship' => 'Relationship',
'Spouse' => 'Spouse',
'Parent' => 'Parent',
'Child' => 'Child',
'Friend' => 'Friend',
'Other' => 'Other',
'Other Relationship' => 'Other Relationship',
'Close' => 'Close',
'Save changes' => 'Save changes'

];
