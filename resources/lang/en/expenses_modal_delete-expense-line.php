<?php

return [
'Confirmation' => 'Confirmation',
'Are you sure you want to delete this Claim Line ?' => 'Are you sure you want to delete this Claim Line ?',
'No' => 'No',
'Yes' => 'Yes'
];
