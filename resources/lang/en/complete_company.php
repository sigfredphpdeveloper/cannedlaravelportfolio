<?php

return [

'Complete Your Company Profile' => 'Complete Your Company Profile',
'Whoops!' => 'Whoops!',
'There were some problems with your input.' => 'There were some problems with your input.',
'Please fill in the form to configure the platform for your company (Step 2)' => 'Please fill in the form to configure the platform for your company (Step 2)',
'My Company' => 'My Company',
'Company' => 'Company',
'Name of Company' => 'Name of Company',
'Website' => 'Website',
'Industry *' => 'Industry *',
'--- Select Industry ---' => 'Select Industry',
'Agency (branding/design/creative/video)' => 'Agency (branding/design/creative/video)',
'Agency (web)' => 'Agency (web)',
'Agency (SEM/SEO/social media)' => 'Agency (SEM/SEO/social media)',
'Agency (other)' => 'Agency (other)',
'Church/religious organisation' => 'Church/religious organisation',
'E-Commerce' => 'E-Commerce',
'Engineering' => 'Engineering',
'Health/beauty' => 'Health/beauty',
'Hospitality' => 'Hospitality',
'Industrials' => 'Industrials',
'IT/technology' => 'IT/technology',
'Media/publishing' => 'Media/publishing',
'Non profit' => 'Non profit',
'Other' => 'Other',
'Retail/consumer merchandise' => 'Retail/consumer merchandise',
'School/education' => 'School/education',
'Software/SaaS' => 'Software/SaaS',
'Travel/leisure' => 'Travel/leisure',
'Company Size' => 'Company Size',
'Address' => 'Address',
'Address *' => 'Address *',
'Zip Code' => 'Zip Code',
'City' => 'City',
'Country *' => 'Country',
'State' => 'State',
'Phone Number' => 'Phone Number',
'Company Logo' => 'Company Logo',
'Update' => 'Update',
'recommended'=>'(recommended)',
'Back To Settings'=>'Back To Settings',
'Create Roles for your company'=>'Create Roles for your company',
'A Role is used for a type'=>'A Role is used for a type of activity and can be for one or several people in the company, for instance ‘sales rep’ or ‘CFO’.',
'Skip'=>'Skip',
'Create New Role'=>'Create New Role',
'Create Teams for your company'=>'Create Teams for your company',
'A Team is for groups of people'=>'A Team is for groups of people across activities: for instance a team for a new product may include one person from marketing,
  one from production and one from finance.',
'Create New Team'=>'Create New Team',
'Name'=>'Name',
 'Role'=>'Role',
 'Title'=>'Title',
 'Name, Role or Team'=>'Name, Role or Team',
 'Import CSV'=>'Import CSV',
 'Emergency Contacts'=>'Emergency Contacts',
 'Now that your company is set up'=>'Now that your company is set up, invite your employees to join Zenintra.net and access the tools',
 'Welcome'=>'Welcome',
 'OK'=>'OK'












];
