<?php

return [
'Add Team' => 'Add Team',
'Team Name' => 'Team Name',
'Team Description' => 'Team Description',
'Save' => 'Save',
'Back' => 'Back',
'Next'=>'Next',
'Add More'=>'Add More',
'Remove'=>'Remove'
];
