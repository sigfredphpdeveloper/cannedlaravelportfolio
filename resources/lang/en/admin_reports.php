<?php

return [
	'Reports' => 'Reports',
    'Companies Signups' => 'Companies Signups',
    'From' => 'From',
    'Date From' => 'Date From',
    'To' => 'To',
    'Date To' => 'Date To',
    'Last 30 days' => 'Last 30 days',
    'Last 6 Months' => 'Last 6 Months',
    'Last 12 Months' => 'Last 12 Months',
    'Beginning of time' => 'Beginning of time',
    'Cumulative Number of Companies Per Day' => 'Cumulative Number of Companies Per Day',
    'Filter' => 'Filter',
    'Cumulative Number of Users Per Day'=>'Cumulative Number of Users Per Day',
    'New Companies Registration Per Day'=>'New Companies Registration Per Day',
    'User Signups'=>'User Signups',
    'Dashboard'=>'Dashboard'

];