<?php

return [
'Expenses Reports' => 'Claims Reports',
'Expenses Status Graph' => 'Claims Status Graph',
'Expenses by Expense Types' => 'Claims by Expense Types',
'All Company Expenses' => 'All Company Claims',
'ID' => 'ID',
'Submitted Date' => 'Submitted Date',
'Employee Name' => 'Employee Name',
'Title' => 'Title',
'Status' => 'Status',
'Approval Date' => 'Approval Date',
'Paid Date' => 'Paid Date',
'Customize'	=> 'Customize'
];
