<?php

return [
'Activity Title' => 'Activity Title',
'Call' => 'Call',
'Meeting' => 'Meeting',
'Email' => 'Email',
'Deadline' => 'Deadline',
'Trash' => 'Trash',
'Other' => 'Other',
'Date' => 'Date',
'Time' => 'Time',
'Duration' => 'Duration',
'Owner' => 'Owner',
'Assigned' => 'Assigned',
'Done' => 'Done',
'Close' => 'Close',
'Save changes' => 'Save changes'


];
