<?php

return [
'Edit Expense Line' => 'Edit Claim Line',
'Title' => 'Title',
'Expense Type' => 'Claim Type',
'Choose Expense Type' => 'Choose Claim Type',
'Save' => 'Save',
'Amount' => 'Amount',
'File' => 'File',
'Include some files' => 'Include some files'
];
