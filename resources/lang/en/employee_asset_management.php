<?php

return[
'Id'=>'Id',
'Title'=>'Title',
'Description'=>'Description',
'Allocated To *'=>'Allocated To *',
'Allocated To'=>'Allocated To',
'Status'=>'Status',
'Purchase Date'=>'Purchase Date',
'Value'=>'Value',
'Add New Asset'=>'Add New Asset',
'Edit Asset'=>'Edit Asset',
'Asset Title *'=>'Asset Title *',
'Asset Description'=>'Asset Description',
'Date Of Sale / Disposal'=>'Date Of Sale / Disposal',
'Active'=>'Active',
'Sale / Disposal'=>'Sale / Disposal',
'View Asset Details'=>'View Asset Details',
'Allocated Employee'=>'Allocated Employee',
'Created Date'=>'Created Date',
'Updated Date'=>'Updated Date',
    'Note'=>"Note"
];