<?php

return [
'Update Organization' => 'Update Organization',
'Organization Name' => 'Organization Name',
'Organization Address' => 'Organization Address',
'Visibility' => 'Visibility',
'Me' => 'Me',
'Team' => 'Team',
'Every One' => 'Every One',
'Delete' => 'Delete',
'Close' => 'Close',
'Save changes' => 'Save changes'
];
