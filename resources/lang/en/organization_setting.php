<?php

return [
'Enable Contact Module' => 'Enable Contact Module',
'Organization Custom Fields (Up to 5)' => 'Organization Custom Fields (Up to 5)',
'Add' => 'Add',
'Custom Fields are only up to 5. Delete of edit fields for new one' => 'Custom Fields are only up to 5. Delete of edit fields for new one',
'Custom Field Name' => 'Custom Field Name',
'Action' => 'Action',
'Edit' => 'Edit',
'Delete' => 'Delete',
'Save' => 'Save',
'Add a new Custom Field For the Organization' => 'Add a new Custom Field For the Organization',
'Title' => 'Title',
'Save changes' => 'Save changes',
'Confirmation' => 'Confirmation',
'Are you sure you want to delete this record ?' => 'Are you sure you want to delete this record ?',
'No' => 'No',
'Yes' => 'Yes',
'Organization Permissions' => 'Organization Permissions'

];
