<?php

return [
'Back' => 'Back',
'Add' => 'Add',
'Add a note' => 'Add a note',
'Note' => 'Note',
'Close' => 'Close',
'Save changes' => 'Save changes'
];
