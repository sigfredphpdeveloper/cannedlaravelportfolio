<?php
//ce fichier sert à la fois pour la page announcement_add et announcement_edit
//this file is used for both announcement_add and announcement_edit pages
return [
'Add Announcement' => 'Add Announcement',
'Edit Announcement' => 'Edit Announcement',
'Warning.' => 'Warning.',
'Title *' => 'Title *',
'Title' => 'Title',
'Message *' => 'Message *',
'Message' => 'Message',
'Upload Picture' => 'Upload Picture',
'Permissions' => 'Permissions',
'Visible By Everyone' => 'Visible By Everyone',
'Visible only to some roles' => 'Visible only to some roles',
'Visible only to some teams' => 'Visible only to some teams',
'Employee Roles' => 'Employee Roles',
'Employee Teams' => 'Employee Teams',
'Save' => 'Save',
'Back' => 'Back',
'Send notification ?' => 'Send email notification ?',
'Yes' => 'Yes',
'No' => 'No',
'Press the Ctrl key to select multiple roles'=>'(Press the Ctrl key to select multiple roles)',
'Press the Ctrl key to select multiple teams'=>'(Press the Ctrl key to select multiple teams)'




];
