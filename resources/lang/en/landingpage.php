<?php

return [

    'Contact Us' => 'Contact Us',
    'Productivity Boost for Startups' => 'Productivity Boost for Startups',
    'Coming soon! We are working on it' => 'Coming soon! We are working on it',
    'Get notified when we open' => 'Get notified when we open',
	'mailchimplistid' => '3d0d8217b0',
    'Free intranet and internal communication for startups' => 'Free intranet and internal communication for startups',
    'main-content-p-text' => 'Are you still using spreadsheets and a multitude of other tools to manage your business, handle your internal tasks and communicate with your coworkers? Zenintra.net provides all the tools you need to manage your team, your internal communication and file sharing need, for Free. Join our waiting list to be the first to get a lifetime access to Zenintra.net',
    'Copyright 2015 Zenintra.net' => 'Copyright 2015 Zenintra.net',
    'Email Address' => 'Email Address',
    'Close' => 'Close',
    'Subscribe' => 'Subscribe',
    'Name' => 'Name',
    'John Smith' => 'John Smith',
    'Email' => 'Email',
    'sample-email' => 'jsmith@gmail.com',
    'Message' => 'Message',
    'What can we do for you?' => 'What can we do for you?',
    'Submit' => 'Submit',
    'Your message was successfully sent! We will get back to you soon\.' => 'Your message was successfully sent! We will get back to you soon\.',
    'Please fill out all fields.' => 'Please fill out all fields.',
    'Free intranet and internal communication for startups and small businesses'=> 'Free intranet and internal communication for startups and small businesses',
    'All-in-one intranet' => 'All-in-one intranet'
];
