<?php

return [
	'Manage Email Templates' => 'Manage Email Templates',
    'Subject'=>'Subject',
    'Action'=>'Action',
    'Edit'=>'Edit',
    'Back'=>'Back',
    'Edit Email Template'=>'Edit Email Template',
    'Slug'=>'Slug',
    'Message Body'=>'Message Body',
    'Save changes'=>'Save changes',
    'Dashboard'=>'Dashboard',
    'Users'=>'Users',
    'Manage Users'=>'Manage Users',
    'Companies'=>'Companies',
    'Manage Companies'=>'Manage Companies',
    'Email Templates'=>'Email Templates',
    'Manage'=>'Manage',

    'Reports' => 'Reports',
    'Export Directory Data' => 'Export Directory Data',
    'Export Leaves Data' => 'Export Leaves Data',
];