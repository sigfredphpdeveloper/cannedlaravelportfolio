<?php

return [
'Add Field' => 'Add Field',
'Field Name' => 'Field Name',
'Field Name *' => 'Field Name *',
'Field Type *' => 'Field Type *',
'Text' => 'Text',
'Date' => 'Date',
'Number' => 'Number',
'List Dropdown' => 'List Dropdown',
'Dropdown Options' => 'Dropdown Options',
'Separated by new line' => 'Separated by new line',
'Field Description' => 'Field Description',
'Visibility' => 'Visibility',
'Save' => 'Save',
'Back' => 'Back'
];

