<?php

return [
'Please type in a user address you would like to invite' => 'Please type in a user address you would like to invite',
'Email must be in your same organization' => 'Email must be in your same organization',
'Warning.' => 'Warning.',
'Add Employee' => 'Add Employee',
'Email *' => 'Email *',
'Enter Email' => 'Enter Email',
'Employee First Name' => 'Employee First Name',
'Employee Last Name' => 'Employee Last Name',
'First Name' => 'First Name',
'Last Name' => 'Last Name',
'Employee Roles' => 'Employee Roles',
'Employee Teams' => 'Employee Teams',
'Send Invitation' => 'Send Invitation',
'Edit User' => 'Edit User',
'Title' => "Title",
'Update' => 'Update',
'Back' => 'Back',
'Invite your employees'=>'Invite your employees:',
'You can invite them even if they'=>'You can invite them even if they don\'t have an email address with your domain name,
                any email address works. ',
'Please use only one email per member'=>'Please use only one email per member.',
'Download Sample CSV'=>'Download Sample CSV',
'Bulk Upload'=>'Bulk Upload',
'Remove'=>'Remove',
'You can select multiple'=>'(You can select multiple teams and Roles by using the CTRL key and select multiple values)',
'Invite more users'=>'Invite more users',
'Loading please wait'=>'Loading please wait...',
'Start Date'=>'Start Date',
'Guest Last Name'=>'Guest Last Name',
'Guest First Name'=>'Guest First Name',
'Notes'=>'Notes',
    'Notes (visible by company administrators only)'=>'Notes (visible by company administrators only)'




];
