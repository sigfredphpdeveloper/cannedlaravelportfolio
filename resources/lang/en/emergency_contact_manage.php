<?php

return [
'Enter Keywords' => 'Enter Keywords',
'Search' => 'Search',
'Emergency Contacts' => 'Emergency Contacts',
'Employee' => 'Employee',
'Emergency Contact' => 'Emergency Contact',
'Emergency Phone' => 'Emergency Phone',
'View' => 'View',
'Edit' => 'Edit',
'Export CSV' => 'Export CSV',
'Confirmation' => 'Confirmation',
'Are you sure you want to delete this User ?' => 'Are you sure you want to delete this User ?',
'No' => 'No',
'Yes' => 'Yes',
'View Emergency Contact' => 'View Emergency Contact',
'OK' => 'OK',
'View Emergency Contact' => 'View Emergency Contact'
];
