<?php

return [
'Add User' => 'Add User',
'Enter Keywords' => 'Enter Keywords',
'Search' => 'Search',
'Employee Directory' => 'Employee Directory',
'Email' => 'Email',
'Name' => 'Name',
'Roles' => 'Roles',
'Teams' => 'Teams',
'Action' => 'Action',
'View' => 'View',
'Resend Invitation' => 'Resend Invitation',
'Delete User' => 'Delete User',
'Assign Roles' => 'Assign Roles',
'Assign Teams' => 'Assign Teams',
'Add Roles' => 'Add Roles',
'Select the roles for the selected employees' => 'Select the roles for the selected employees',
'Select the teams for the selected employees' => 'Select the teams for the selected employees',
'No' => 'No',
'Yes' => 'Yes',
'Save' => 'Save',
'Add Teams' => 'Add Teams',
'Select Employees' => 'Select Employees',
'Please select employees' => 'Please select employees',
'OK' => 'OK',
'Confirmation' => 'Confirmation',
'Are you sure you want to delete this User ?' => 'Are you sure you want to delete this User ?',
'Assign Employee Roles' => 'Assign Employee Roles',
'Assign Employee Teams' => 'Assign Employee Teams',
'Edit User' => 'Edit User',
'Invite Guest'=>'Invite Guest',
'Edit' => 'Edit',
'Delete' => 'Delete',



];
