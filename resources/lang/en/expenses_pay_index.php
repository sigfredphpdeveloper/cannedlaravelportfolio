<?php

return [
'Pay Expenses' => 'Pay Claims',
'All Approved Expenses' => 'All Approved Claims',
'Owner' => 'Owner',
'Submision Date' => 'Submision Date',
'Expense ID' => 'Expense ID',
'Title' => 'Title',
'Total Amount' => 'Total Amount',
'Status' => 'Status',
'Approve Date' => 'Approval Date',
'Paid Date' => 'Paid Date',
'Action' => 'Action',
'Pay' => 'Pay',
'No Expenses' => 'No Claims',
'Customize'	=> 'Customize'
];