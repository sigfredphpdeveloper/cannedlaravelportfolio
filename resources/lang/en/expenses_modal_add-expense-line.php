<?php

return [
'Add Expense Line' => 'Add Claim Line',
'Title' => 'Title',
'Expense Type' => 'Claim Type',
'Choose Expense Type' => 'Choose Claim Type',
'Amount' => 'Amount',
'File' => 'File',
'Include some files' => 'Include some files',
'Content' => 'Content'
];


