<?php

return [
'My Expenses' => 'My Claims',
'Request Details' => 'Request Details',
'Edit' => 'Edit',
'Delete' => 'Delete',
'Total Amount' => 'Total Amount',
'Status' => 'Status',
'Creation Date' => 'Creation Date',
'Paid Date' => 'Paid Date',
'Approval Date' => 'Approval Date',
'File' => 'File',
'No Expenses Line' => 'No Claims Line',
'Add New Line' => 'Add New Line',
'Customize'	=> 'Customize'
];
