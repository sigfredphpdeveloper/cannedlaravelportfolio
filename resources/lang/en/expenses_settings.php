<?php

return [
'Expenses' => 'Claims',
'Save' => 'Save',
'Add New' => 'Add New',
'Expenses Type' => 'Claims Type',
'ID' => 'ID',
'Title' => 'Title',
'Date Created' => 'Date Created',
'Action' => 'Action',
'Manage fields' => 'Manage fields',
'Edit' => 'Edit',
'Delete' => 'Delete',
'Expenses Roles Permision' => 'Claims Roles Permision',
'By default user can only see his/her expenses.' => 'By default user can only see his/her expenses.',
'Role' => 'Role',
'Approve' => 'Approve',
'Delete' => 'Delete',
'Pay' => 'Pay',
'View All' => 'View All',
'Currency Symbol'	=> 'Currency Symbol'
];
