<?php

return [
'Edit Field' => 'Edit Field',
'Back' => 'Back',
'Warning.' => 'Warning.',
'Field Name *' => 'Field Name *',
'Field Name' => 'Field Name',
'Field Type *' => 'Field Type *',
'Dropdown Options'  => 'Dropdown Options',
'Separated by new line' => 'Separated by new line',
'Field Description' => 'Field Description',
'Visibility' => 'Visibility',
'Save' => 'Save'



];

