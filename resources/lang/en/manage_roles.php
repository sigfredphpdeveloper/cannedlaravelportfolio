<?php

return [
'Manage Roles' => 'Manage Roles',
'Add Role' => 'Add Role',
'Search' => 'Search',
'Role' => 'Role',
'Role Description' => 'Role Description',
'Action' => 'Action',
'Edit' => 'Edit',
'Delete' => 'Delete',
'Confirmation' => 'Confirmation',
'Are you sure you want to delete this record ?' => 'Are you sure you want to delete this record ?',
'No' => 'No',
'Yes' => 'Yes',
'Enter Keywords' => 'Enter Keywords'



];
