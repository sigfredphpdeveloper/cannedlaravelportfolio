<?php

return [
'Manage Team' => 'Manage Teams',
'Add Team' => 'Add Team',
'Search' => 'Search',
'Team' => 'Team',
'Team Description' => 'Team Description',
'Action' => 'Action',
'Edit' => 'Edit',
'Delete' => 'Delete',
'Confirmation' => 'Confirmation',
'Are you sure you want to delete this record ?' => 'Are you sure you want to delete this record ?',
'No' => 'No',
'Yes' => 'Yes',
'Enter Keywords' => 'Enter Keywords'



];
