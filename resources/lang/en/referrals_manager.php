<?php

return [
'Manage Referrals' => 'Manage Referrals',
'Add Referral' => 'Add Referral',
'Import CSV' => 'Import CSV',
'Search' => 'Search',
'Invitation Sent To' => 'Invitation Sent To',
'Date' => 'Date',
'Status' => 'Status',
'Confirmation' => 'Confirmation',
'Are you sure you want to delete this record ?' => 'Are you sure you want to delete this record ?',
'No' => 'No',
'Yes' => 'Yes',
'Referral Details' => 'Referral Details',
'OK' => 'OK',
'more_space_invite_text' => 'You need more space? Invite other companies to join Zenintra and earn 200Mb additional Zenintra disk space for each company joining (up to 5Gb).',
];
