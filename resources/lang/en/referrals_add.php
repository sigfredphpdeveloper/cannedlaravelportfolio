<?php

return [
'Add Referral' => 'Add Referral',
'You need more space ?' => 'You need more space ?',
'You need more space? Invite other companies to join Zenintra and earn 200Mb additional Zenintra disk space for each company joining (up to 5Gb).' => 'You need more space? Invite other companies to join Zenintra and earn 200Mb additional Zenintra disk space for each company joining (up to 5Gb).',
'Email Address' => 'Email Address',
'Add more' => 'Add more',
'Invitation Email Title' => 'Invitation Email Title',
'Invitation Message' => 'Invitation Message',
'Invite' => 'Invite',
'Back' => 'Back',
'Please enter email' => 'Please enter email',
'Enter your Invitation Title (optional)' => 'Enter your Invitation Title (optional)',
'Please enter your invitational message. (optional)' => 'Please enter your invitational message. (optional)'
];
