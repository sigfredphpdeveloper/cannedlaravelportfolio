<?php

return [
'Organizations' => 'Organisations',
'Add' => 'Ajouter',
'Search' => 'Rechercher',
'Organization Name' => "Nom de l'organisation",
'Nb of Contact' => 'Nb de contacts',
'Owner' => 'Propriétaire',
'Actions' => 'Actions',
'Edit' => 'Éditer',
'Delete' => 'Supprimer',
'Search for...' => 'Rechercher...',
'Organization Address' => "Adresse de l'organisation",
'Visibility' => 'Visibilité',
'Me' => 'Moi',
'Team' => 'Équipe',
'Every One' => 'Tout le monde',
'Delete' => 'Supprimer',
'Close' => 'Fermer',
'Save changes' => 'Enregistrer les modifications',
'Confirmation' => 'Confirmation',
'Are you sure you want to delete this record ?' => 'Etes-vous sûrs de vouloir supprimer cette information ?',
'No' => 'Non',
'Yes' => 'Oui',
'Add a new Organization' => 'Ajouter une nouvelle organisation'


];

