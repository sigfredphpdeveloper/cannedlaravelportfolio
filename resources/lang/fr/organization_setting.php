<?php

return [
'Enable Contact Module' => 'Autoriser le module Contact', 
'Organization Custom Fields (Up to 5)' => "Champs personnalisés de l'organisation (jusqu'à 5)",
'Add' => 'Ajouter',
'Custom Fields are only up to 5. Delete of edit fields for new one' => 'Il ne peut y avoir que 5 champs personnalisés. Supprimer un champs existant pour en ajouter un nouveau',
'Custom Field Name' => 'Nom du champs personnalisé',
'Action' => 'Action',
'Edit' => 'Éditer',
'Delete' => 'Supprimer',
'Save' => 'Enregistrer',
'Add a new Custom Field For the Organization' => "Ajouter un nouveau champs personnalisé pour l'Organization",
'Title' => 'Titre',
'Save changes' => 'Enregistrer les modifications',
'Confirmation' => 'Confirmation',
'Are you sure you want to delete this record ?' => 'Etes vous sûr de vouloir supprimer cette information ?',
'No' => 'Non',
'Yes' => 'Oui',
'Organization Permissions' => 'Permissions sur les Organisations'

];
