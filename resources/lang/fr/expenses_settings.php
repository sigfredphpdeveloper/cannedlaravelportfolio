<?php

return [
'Expenses' => 'Dépenses',
'Save' => 'Enregistrer',
'Add New' => 'Ajouter',
'Expenses Type' => 'Type de dépense',
'ID' => 'ID',
'Title' => 'Titre',
'Date Created' => 'Date de création',
'Action' => 'Action',
'Manage fields' => 'Gérer les champs',
'Edit' => 'Éditer',
'Delete' => 'Supprimer',
'Expenses Roles Permision' => 'Permissions des rôles pour les dépenses',
'By default user can only see his/her expenses.' => 'Par défaut, un utilisateur ne peux que voir ses dépenses.',
'Role' => 'Rôle',
'Approve' => 'Approuver',
'Delete' => 'Supprimer',
'Pay' => 'Payer',
'View All' => 'Voir tout'
];
