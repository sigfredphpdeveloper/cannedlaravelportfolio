<?php

return [
'Confirmation' => 'Confirmation',
'Are you sure you want to delete this Expense ?' => 'Etes-vous sûr de vouloir supprimer cette dépense ?',
'No' => 'Non',
'Yes' => 'Oui'
];
