<?php

return [
'Complete Your Registration' => 'Compléter votre enregistrement',
'Please fill in the form to configure the platform for your company (Step 1)' => 'S\'il vous plaît remplir le formulaire pour configurer la plate-forme pour votre entreprise',
'Your Profile' => 'Votre Profil',
'Position' => 'Poste',
'Phone Number' => 'Numéro de téléphone',
'Optional' => 'Optionnel',
'Title' => 'Titre',
'Mr' => 'M.',
'Mrs.' => 'Mme.',
'Ms.' => 'Mlle.',
'First Name' => 'Prénom',
'First Name *' => 'Prénom *',
'Last Name' => 'Nom de famille',
'Last Name *' => 'Nom de famille *',
'Website' => 'Site Entreprise',
'Company URL *' => 'URL Entreprise *',
'Email' => 'Email',
'Email *' => 'Email *',
'Used to log in' => 'Utilisé pour se connecter',
'Password' => 'Mot de passe',
'Change Password' => 'Modifier le mot de passe',
'Profile Picture' => 'Profil Photo (recommandé)',
'Update' => 'Mettre à jour',
'step1'=>'Étape 1',
'step2'=>'Étape 2'
];