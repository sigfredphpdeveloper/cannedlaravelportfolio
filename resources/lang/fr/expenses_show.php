<?php

return [
'My Expenses' => 'Mes Dépenses',
'Request Details' => 'Demande de détails',
'Edit' => 'Éditer',
'Delete' => 'Supprimer',
'Total Amount' => 'Montant total',
'Status' => 'Statut',
'Creation Date' => 'Date de création',
'Paid Date' => 'Date de paiement',
'Approval Date' => 'Date d\'approbation',
'File' => 'Fichier',
'No Expenses Line' => 'Aucune ligne de dépense',
'Add New Line' => 'Add New Line'
];
