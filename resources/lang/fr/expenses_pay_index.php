<?php

return [
'Pay Expenses' => 'Payer les dépenses',
'All Approved Expenses' => 'Toutes les dépenses approuvées',
'Owner' => 'Propriétaire',
'Submision Date' => 'Date de soumission',
'Expense ID' => 'ID de la dépense',
'Title' => 'Titre',
'Total Amount' => 'Montant total',
'Status' => 'Statut',
'Approve Date' => 'Date d\'approbation',
'Paid Date' => 'Date de paiement',
'Action' => 'Action',
'Pay' => 'Payer',
'No Expenses' => 'Aucune dépense'
];