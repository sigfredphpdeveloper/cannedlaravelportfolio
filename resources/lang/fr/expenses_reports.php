<?php

return [
'Expenses Reports' => 'Récapitulaitf des Dépenses',
'Expenses Status Graph' => 'Graphique des Statut-Dépenses',
'Expenses by Expense Types' => 'Dépenses par type de dépenses',
'All Company Expenses' => 'Toutes les dépenses de l\'entreprise',
'ID' => 'ID',
'Submitted Date' => 'Date de soumission',
'Employee Name' => 'Nom de l\'employé',
'Title' => 'Titre',
'Status' => 'Statut',
'Approval Date' => 'Date d\'approbation',
'Paid Date' => 'Date de paiement'
];
