<?php

return [
'Add Role' => 'Ajouter un Rôle',
'Role Name' => 'Nom du rôle',
'Role Description' => 'Description du rôle',
'Save' => 'Enregistrer',
'Back' => 'Retour',
'Next'=>'Prochain',
'Success Do you want'=>'Success! Do you want to create another one ? or go to',
'Warning'=>'Warning',
'Add More'=>'Add More',
'Remove'=>'Remove'

];

