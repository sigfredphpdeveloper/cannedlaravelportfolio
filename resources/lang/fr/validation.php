<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute doit être accepté.',
    'active_url'           => ':attribute n\'est pas une url valide.',
    'after'                => ':attribute doit être une date après :date.',
    'alpha'                => ':attribute doit uniquement contenir des lettres.',
    'alpha_dash'           => ':attribute doit uniquement contenir des caractères alphanumériques et/ou des tirets.',
    'alpha_num'            => ':attribute doit uniquement contenir des caractères alphanumériques.',
    'array'                => ':attribute doit être un tableau.',
    'before'               => ':attribute doit être une date avant :date.',
    'between'              => [
        'numeric' => ':attribute doit être compris entre :min et :max.',
        'file'    => ':attribute doit avoir une taille comprise entre :min et :max kilobytes.',
        'string'  => ':attribute doit contenir entre :min et :max caractères.',
        'array'   => ':attribute doit avoir entre :min and :max objets.',
    ],
    'boolean'              => 'Le champ de :attribute doit être vrai ou faux.',
    'confirmed'            => 'La confirmation de :attribute ne correspond à rien.',
    'date'                 => ':attribute n\'est pas une date valide.',
    'date_format'          => ':attribute ne correspond pas au format :format.',
    'different'            => ':attribute et :other doivent être différents.',
    'digits'               => ':attribute doit être à :digits chiffres.',
    'digits_between'       => ':attribute doit contenir entre :min et :max chiffres.',
    'emails'                => ':attribute doit être une adresse email valide.',
    'exists'               => ':attribute est invalide.',
    'filled'               => 'Le champ :attribute est requis.',
    'image'                => ':attribute doit être une image.',
    'in'                   => 'Le champ :attribute sélectionné est invalide.',
    'integer'              => ':attribute doit être un entier.',
    'ip'                   => ':attribute doit être une adresse IP valide.',
    'json'                 => ':attribute doit être une chapine de caractères JSON valide.',
    'max'                  => [
        'numeric' => ':attribute ne doit pas être supérieur à :max.',
        'file'    => ':attribute ne doit pas excéder :max kilobytes.',
        'string'  => ':attribute ne doit pas contenir plus de :max caractères.',
        'array'   => ':attribute ne doit pas contenir plus de :max objets.',
    ],
    'mimes'                => ':attribute doit être un fichier de type :values.',
    'min'                  => [
        'numeric' => ':attribute  doit être au moins :min.',
        'file'    => ':attribute doit être au moins de :min kilobytes.',
        'string'  => ':attribute doit contenir au moins :min caractères.',
        'array'   => ':attribute doit avoir au moins :min objets.',
    ],
    'not_in'               => 'Le champ :attribute sélectionné est invalide.',
    'numeric'              => ':attribute doit être un nombre.',
    'regex'                => 'Le format :attribute est invalide.',
    'required'             => 'Le champ :attribute est requis.',
    'required_if'          => 'Le champ :attribute est requis lorsque :other vaut :value.',
    'required_with'        => 'Le champ :attribute est requis lorsque :values existe.',
    'required_with_all'    => 'Le champ :attribute est requis lorsque :values existe.',
    'required_without'     => 'Le champ :attribute est requis lorsque :values n\'est pas présent.',
    'required_without_all' => 'Le champ :attribute est requis lorsqu\'aucun de :values ne sont présents.',
    'same'                 => ':attribute et :other doivent correspondre.',
    'size'                 => [
        'numeric' => ':attribute doit être :size.',
        'file'    => ':attribute doit être de :size kilobytes.',
        'string'  => ':attribute doit contenir :size caractères.',
        'array'   => ':attribute doit contenir :size objets.',
    ],
    'string'               => ':attribute doit être une chaîne de caractères.',
    'timezone'             => ':attribute doit être une zone valide.',
    'unique'               => ':attribute a déjà été pris.',
    'url'                  => 'Le format de :attribute est invalide.',
    "not_gmail" => "Vous devez avoir un email valide d'une compagnie réelle.",
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'message personnalisé',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "emails". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
