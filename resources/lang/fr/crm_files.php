<?php

return [
'Back' => 'Retour',
'Files' => 'Fichiers',
'Add' => 'Ajouter',
'Download' => 'Télécharger',
'Add a File' => 'Ajouter un fichier',
'File Name' => 'Nom du fichier',
'File' => 'Fichier',
'Upload' => 'Télécharger',
'Close' => 'Fermer',
'Save changes' => 'Enregistrer les modifications'


];

