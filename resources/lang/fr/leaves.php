<?php

return [
    'Leave Application' => 'demande d\'autorisation',
    'Leave Type' => 'Type de congé',
    'Single Day' => 'Seule journée',
    'Multiple Days' => 'Plusieurs jours',
    'Date' => 'date',
    'All Day' => 'Toute la journée',
    'Morning' => 'Matin',
    'Afternoon' => 'Après midi',
    'From' => 'De',
    'To' => 'À',
    'Type of Leave' => 'Type de congé',
    'Details' => 'Détails',
    'Save' => 'sauvegarder',


    'Approve Leaves'=>'Approuver Leaves',
    'Search'=>'Chercher',
    'User'=>'Utilisateur',
    'Type'=>'Type',
    'Status'=>'statut',
    'Action'=>'action',
    'Approve'=>'Approuver',
    'Reject'=>'Rejeter',
    'View'=>'Vue',
    'Leave Details'=>'Donner des détails',
    'Close'=>'Fermer',
    'Confirmation'=>'Confirmation',
    'Are you sure you want to approve this leave'=>'Êtes-vous sûr de vouloir approuver ce congé?',
    'No'=>'Non',
    'Yes'=>'Oui',
    'Are you sure you want to reject this leave'=>'Etes-vous sûr de vouloir rejeter ce congé?',

    'Calendar'=>'Calendrier',
    'Filter By'=>'Filtrer par',
    'Employee'=>'Employé',
    'All'=>'Tout',
    'Role'=>'Rôle',
    'Filter'=>'Filtre',
    'Dashboard'=>'Tableau de bord',

    'Employees Leave Summary'=>'Employés Laisser Résumé',
    'Allowance'=>'Allocation',
    'Remaining'=>'Restant',
    'Leave Summary'=>'Laissez Résumé',
    'Edit'=>'modifier',
    'Name'=>'prénom',
    'Warning'=>'Attention',
    'Enter Keywords'=>'Entrez les mots clés',
    'Loading Please Wait'=>'Chargement, veuillez patienter',
    'Start Date'=>'Date de début',
    'Employee Start Date'=>'Employé Date de début',
    'Add Leave Type'=>'Ajouter congé de type',
    'Description'=>'La description',
    'Default Allowance'=>'Allocation par défaut',
    'Color Code'=>'Code couleur',
    'Select the Roles who'=>'Sélectionnez les rôles qui auront accès à ce nouveau type de congé; appuyez sur CTRL pour sélectionner plusieurs rôles',
    'Start Date Allowance'=>'Date de début Allocation',
    'Select a date'=>'Sélectionnez une date',
    'Back'=>'Arrière',
    'Edit Leave Type'=>'Modifier congé type',
    'Roles Available'=>'Rôles disponibles',
    'Leaves History'=>'Feuilles Histoire',
    'Cancel'=>'Annuler',
    'Are you sure you want to cancel this leave record'=>'Etes-vous sûr de vouloir annuler cet enregistrement de congé?',
    'Back To Settings'=>'Retour aux paramètres',
    'Add'=>'Ajouter',
    'Manage Leave Types'=>'Gérer les types de congé',
    'Company Leaves'=>'Société Feuilles',
    'Leaves Permission'=>'Feuilles Permission',
    'Year'=>'An',
    'Pending'=>'en attendant',
    'Approved'=>'A approuvé',
    'Balance'=>'Équilibre',
    'Leaves'=>'Feuilles',
    'ID'=>'ID',
    'Credits'=>'Crédits',
    'Rejected Leaves'=>'Feuilles rejeté',
    'Pending Leaves'=>'Feuilles en attente',
    'Approved Leaves'=>'Feuilles approuvées',
    'User Leaves'=>'Les feuilles de l\'utilisateur',
    'Customize'=>'Personnaliser',
    'Bulk Edit'=>'Modifier Bulk',
    'Please check atleast one checkbox'=>'S\'il vous plaît vérifier au moins une case',
    'Assign Leave Allowances'=>'Attribuer Indemnités de congé',
    'All Leaves'=>'Toutes les feuilles',
    'First Name'=>'Prénom',
    'Last Name'=>'Nom de famille',
    'Email'=>'Email',
    'Add Leave'=>'Ajouter congé',
'Company Days OFF'=>'Société Jours OFF',
'Export Data'=>'Exporter des données',
    'Export Leaves Data'=>'Export Feuilles de données',
'Allow Half Day'=>'Demi-journée',
'Roll over allowed at the end of the year?'=>'Roll over permis à la fin de l\'année?',
    'Leave Notes'=>'Laisser des notes',
    'Note'=>'Remarque',
    'Add Notes'=>'Ajouter des notes',
    'View Notes'=>'Voir Notes',
    'Leave Note'=>'Laisser un commentaire',
    'Add Leave Note'=>'Ajouter Leave Note',
'Allow Advanced Leaves'=>'Autoriser les feuilles avancées',
'Available'=>'Disponible',
    ];
