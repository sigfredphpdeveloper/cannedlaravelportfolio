<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Le mot de passe doit contenir au moins 6 caractères et être validé par la confirmation',
    'reset' => 'Votre mot de passe a bien été ré-initialisé!',
    'sent' => 'Nous vous avons envoyé un email avec le lien pour ré-initialiser votre mot de passe!',
    'token' => 'Ce lien pour réinitialiser le mot de passe est invalide.',
    'user' => "Nous n'avons pas trouvé d'utilisateur avec cette adresse email."

];
