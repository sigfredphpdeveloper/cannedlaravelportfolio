<?php

return [

	'Features' => 'Features',
	'Pricing' => 'Pricing',
	'Sign up' => 'Sign up',
	'Log in' => 'Log in',
	'About Us' => 'A propos',
	'paragraph-1' => 'Nous avons lancé Zenintranet car nous sommes aussi une PME et nous rencontrons certainement les mêmes problèmes que vous: essayer de coordonner les informations de différents outils pour ne pas payer trop cher. Ce n’était pas très efficace et nous avons perdu de temps et d\'argent. ',
	'paragraph-2' => 'C’est pourquoi nous avons décidé de créer une plate-forme intégrée pour nous-mêmes et pour d\'autres sans avoir besoin de payer ou de se connecter à plusieurs comptes.',
	'paragraph-3' => 'Par exemple, un des problèmes que nous avions était de changer le flux de travail lorsque quelqu’un quittait l’entreprise ou changeait de poste. Avec Zenintranet vous pouvez choisir que la personne en charge des RH reçoive les demandes de remboursement de frais: même si cette personne change vous avez juste besoin de redonner le titre de responsable RH à quelqu’un d’autre. Rien de plus simple !',
	'paragraph-4' => 'Zenintranet est un site conçu pour vous: notre mission est de créer et améliorer constamment une plate-forme ultra efficace et productive pour votre entreprise. Toutes vos suggestions d’améliorations sont évidemment les bienvenues.',
	'paragraph-5' => 'Mais si vous voulez vraiment en savoir plus sur nous, nous sommes une équipe française basée à Singapour. Singapour est un endroit idéal pour développer ce projet et vous accompagner dans le développement de votre société. ', 
	'sophie' => 'Sophie dirige l’équipe technique et l’équipe développement. En 2011 elle a co-fondé Fewstones, une agence de création et production vidéo à Singapour. Avant le début de son activité entrepreunariale elle a passé plus de 10 ans à occuper des fonctions en recherche et développement et opérations en Europe pour des compagnies télécoms. Sophie a un master d’ingénieur de l’ENST et un Master in Business Administration de l’INSEAD. ',
	'jc' => 'Jean-Christophe est en charge des partenariats et du développement de l’activité. Avant d’être un entrepreuneur, JC a passé plus de 15 ans à occuper des positions de manager et de business development dans une grande société en Europe, en Asie et au Moyen-Orient. Il était aussi le co-fondateur et partenaire d’une association pour financer les startups françaises de 1999 à 2012. Jean-Christophe a un Master in Business Administration de l’INSEAD.',
	'Legal' => 'Legal',
	'Privacy' => 'Privacy Policy',
	'Facebook-url' => 'https://www.facebook.com/Zenintrafr',
	'Twitter-url' => 'https://twitter.com/zenintrafr',
	'Linkedin-url' => 'https://www.linkedin.com/company/zenintra-net?trk=company_logo',
	'Contact Us' => 'Contact Us',
    'Copyright' => 'Copyright 2015 Zenintra.net',
];
