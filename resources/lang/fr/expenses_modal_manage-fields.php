<?php

return [
'Manage Fields for' => 'Gérer les champs pour',
'Note:' => 'Note:',
'Amount' => 'Montant',
'data-type is constant. It will be use to compute the total expenses.' => 'Le type de données est fixe. Il sera utilisé pour calculer le montant total des dépenses.',
'Label' => 'Label',
'Select' => 'Sélectionner',
'Text' => 'Texte',
'Number' => 'Nombre',
'Date' => 'Date',
'Textarea' => 'Zone de texte',
'File' => 'Fichier',
'Required' => 'Requis',
'Save' => 'Enregistrer'
];

