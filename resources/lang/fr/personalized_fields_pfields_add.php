<?php

return [
'Add Field' => 'Ajouter un Champ',
'Field Name' => 'Nom du Champ',
'Field Name *' => 'Nom du Champ *',
'Field Type *' => 'Type du Champ *',
'Text' => 'Texte',
'Date' => 'Date',
'Number' => 'Nombre',
'List Dropdown' => 'Liste Déroulante',
'Dropdown Options' => 'Options de la liste',
'Separated by new line' => 'Séparé par une nouvelle ligne',
'Field Description' => 'Description du Champ',
'Visibility' => 'Visibilité',
'Save' => 'Enregistrer',
'Back' => 'Retour'
];

