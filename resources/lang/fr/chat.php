<?php

return [
'Update Profile' => 'Mettre à jour le Profil',
'Uploaded a file' => 'Fichier mis à jour',
'This Chat is empty!' => 'Ce Chat est vide!',
'Send' => 'Envoyer',
'Select a chat' => 'Choisir un chat',
'Choose a chat from the list on the left' => 'Choisis un chat dans la liste à gauche :D',
'Send File' => 'Envoyer ce fichier',
'Choose File' => 'Choisir ce fichier',
'Message' => 'Message',
'Close' => 'Fermer',
'Send' => 'Envoyer',
'Choose or drag File' => 'Choisis ou drag un Fichier'




];
