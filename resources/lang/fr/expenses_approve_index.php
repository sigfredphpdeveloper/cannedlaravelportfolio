<?php

return [
'Approve Expenses' => 'Approuver les Dépenses',
'All Company Expenses' => 'Toutes les Dépenses de l\'Entreprise',
'Owner' => 'Propriétaire',
'Submision Date' => 'Date de soumission',
'Expense ID' => 'ID Dépense',
'Title' => 'Titre',
'Total Amount' => 'Montant Total',
'Status' => 'Statut',
'Approve Date' => 'Date d\'Approbation',
'Paid Date' => 'Date de paiement',
'Action' => 'Action',
'View' => 'Voir',
'No Expenses' => 'Aucune Dépense'

];
