<?php

return [
'Back' => 'Retour',
'Add' => 'Ajouter',
'Add a note' => 'Ajouter une note',
'Note' => 'Note',
'Close' => 'Fermer',
'Save changes' => 'Enregistrer les modifications'
];
