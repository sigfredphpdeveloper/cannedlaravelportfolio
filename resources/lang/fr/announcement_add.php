<?php

return [
'Add Announcement' => 'Publier une annonce',
'Edit Announcement' => 'Éditer l\'annonce',
'Warning.' => 'Attention.',
'Title *' => 'Titre *',
'Title' => 'Titre',
'Message *' => 'Message *',
'Message' => 'Message',
'Upload Picture' => 'Télécharger une image',
'Permissions' => 'Permissions',
'Visible By Everyone' => 'Visible par Tout le monde',
'Visible only to some roles' => 'Visible uniquement par certains rôles',
'Visible only to some teams' => 'Visible uniquement par certaines équipes',
'Employee Roles' => 'Rôles des Employés',
'Employee Teams' => 'Équipe d\'Employés',
'Save' => 'Enregistrer',
'Send notification ?' => 'Envoyer une notification ?',
'Yes' => 'Oui',
'No' => 'Non',
'Press the Ctrl key to select multiple roles'=>'(Appuyez sur la touche Ctrl pour sélectionner plusieurs rôles)',
'Press the Ctrl key to select multiple teams'=>'(Appuyez sur la touche Ctrl pour sélectionner plusieurs équipes)'


];
