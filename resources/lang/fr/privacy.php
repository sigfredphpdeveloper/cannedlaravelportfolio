<?php

return [
	
	'Features' => 'Features',
	'Pricing' => 'Pricing',
	'Sign up' => 'Sign up',
	'Log in' => 'Log in',
	'Privacy-title' => 'Privacy Policy',
	'content'	=> '
	<p><strong><em>Dernière mise à jour: 11 Janvier 2016</em></strong> </p>

	<strong>iLathys Pte Ltd</strong><br />
	71 Ayer Rajah Crescent, #05­08<br />
	Singapore, 139951

	<p> iLathys Pte Ltd (ci après dénommé "nous", "nôtre") est une entreprise spécialisée dans le digital. Le site internet est consultable depuis l\'adresse <a href="http://ilathys.com" target="_blank">http://ilathys.com</a>. L\'entreprise, basée à Singapour, est à l\'origine de la création de la plateforme Zenintra (ci après dénommée "le Service"). </p>

	<p>Les Privacy Policy vous informe sur la manière dont nous collectons, utilisons et divulguons vos informations personnelles lors de l\'utilisation de notre service. </p>

	<p>Nous utilisons uniquement vos informations personnelles dans le but d\'améliorer notre Service. En utilisant Zenintra, vous nous autorisez à collecter et utiliser l\'information vous concernant dont nous disposons, en accord avec cette clause. Les termes utilisés dans la présente Privacy Policy sont identiques à ceux utilisés dans les "Termes & Conditions", accessibles depuis <a href="http://zenintra.net" target="_blank">http://zenintra.net</a></p>

	<strong><u>Collecte de données personnelles & fins</u></strong>
	<p>Si vous souhaitez utiliser notre service, vous serez invités à renseigner un certain nombre d\'informations personnelles, qui seront utilisées par la suite en vue de vous identifier et/ ou de vous contacter. Ces informations personnelles sont généralement (liste non exhaustive) : votre nom, prénom, adresse email, autres informations personnelles.</p>

	<p>Nous conservons des documents, dossiers, données, renseignées dans votre profil utilisateur depuis nos bases de données (Singapour & autres locations). Nous gardons également des sauvegardes de vos données afin de prévenir un dysfonctionnement intrusif de notre système. </p>

	<p>Nous pouvons également conserver des sauvegardes de vos informations personnelles & données afin de satisfaire nos obligations légales. Nous vous assurons que ces données collectées ne seront jamais divulguées, et ne serons jamais accessibles, pas même aux employés de Zenintra (sauf circonstances exceptionnelles, telles que mentionnées dans les Privacy Policy & les Conditions Générales d\'Utilisation). </p>

	<strong><u>Log Data</u></strong>
	<p>Nous collectons l\'information renvoyée par votre navigateur quand vous utilisez notre service ("Log Data"). Cette Log Data peut contenir des données, comme votre adresse IP, votre navigateur, sa version actuelle, des statistiques relatives  à l\'utilisation que vous faîtes de notre service (date de la dernière visite, temps passé sur certaines pages du service, autres statistiques).</p>

	<p>Nous pouvons également recourir à des services tiers, comme Google Analytics, dont la mission est de collecter & analyser ce type d\'information dans le but d\'améliorer les fonctionnalités de notre service. Ces services tiers ont leur propres Conditions Générales d\'Utilisation & Privacy Policy: nous vous invitons à les consulter.</p>

	<strong><u>Cookie</u></strong>
	<p>Les cookies sont des dossiers comprenant des données, qui peuvent inclure un identifiant unique anonyme. Les Cookies sont envoyés sur votre navigateur depuis un site web et enregistrés sur le disque dur de votre ordinateur.</p>
	<p> Nous utilisons les cookies afin de collecter de l\'information. Vous pouvez décider de paramétrer votre navigateur afin de désactiver les Cookies. Toutefois, si vous désactivez les Cookies, vous risqueriez de ne plus être en mesure d\'utiliser une partie de nos Services. </p>

	<strong><u>Comportement remarketing</u></strong>
	<p>iLathys Ltd utilise des services de remarketing en vue de vous proposer des publicités adaptées à vos besoins. Nos services de remarketing et iLathys utilisent les cookies dans le but de vous informer en vous proposant de la publicité ciblée (en rapport avec vos dernières visites). Ces cookies ne divulguent en aucun cas votre identité et ne permettent pas à d\'autres services d\'accéder à votre ordinateur.</p>

	­ <strong><u> -Google </u></strong>
	<p>La campagne Google Adwords de remarketing est opérée par Google Inc. Vous pouvez à tout moment désactiver la fonction Google Analytics et/ou customiser votre Google Display Network ads en visitant les réglages de Google Ads: <a href="http://www.google.com/intl/en/policies/privacy/" target="_blank">http://www.google.com/intl/en/policies/privacy/</a></p>
	<p> Google recommande également d\'installer le Google Analytics Opt-Out Brower Add-on (<a href="https://tools.google.com/dlpage/gaoptout" target="_blank">https://tools.google.com/dlpage/gaoptout</a>) de votre navigateur. Google Analytics Opt-Out browser Add-on permet aux utilisateurs d\'empêcher la diffusion de leurs données internet collectées et utilisées par Google Analytics. </p>
	<p> Pour plus d\'information concernant les services opérés par Google, nous vous invitons à consulter les Conditions Générales d\'Utilisation de Google disponibles sur leur site internet: http://www.google.com/intl/en/policies/privacy/ </p>

	<strong><u> -­ Facebook </u></strong>
	<p>Vous pouvez à tout moment vous désengager du tracking Facebook en visitant le lien suivant: <a href="https://www.facebook.com/ads/website_custom_audiences/" target="_blank">https://www.facebook.com/ads/website_custom_audiences/</a></p>

	<strong><u>Prestataires</u></strong>
	<p>Nous pouvons recourir aux services de compagnies tiers/ individus en vue de travailler sur notre service et à son amélioration, ou en vue de nous aider dans l\'élaboration de notre service.</p>
	<p> Ces prestataires pourront avoir accès à vos informations personnelles uniquement en vue de répondre aux missions qui leur auront été attribuées. Ils sont dans l\'obligation la plus totale de conserver l\'anonymat des données auxquelles ils pourraient avoir accès. </p>

	<strong><u>Communications</u></strong>
	<p>Nous avons la possibilité d\'utiliser vos informations personnelles en vue de vous tenir informé de l\'état de notre service. Nous sommes donc dans la mesure de vous envoyer des newsletters, ou du contenu promotionnel/ autres informations qui pourraient être en mesure de vous intéresser.  Vous pouvez à tout moment décider de vous désinscrire de ces contenus promotionnels, en cliquant sur l\'onglet "se désabonner" ou en suivant les instructions indiquées dans chaque mail de notre part qui vous sera envoyé.</p>

	<strong><u>Partie Légale</u></strong>
	<p>Notre service respecte le Singaporean Personal Data Protection Act (PDPA) qui stipule que chaque organisation basée à Singapour ne doit pas: </p>
	1) Collecter, utiliser et divulguer des informations personnelles sur des utilisateurs sans les en informer ; <br />
	2) Collecter, utiliser et divulguer des informations personnelles sans en avoir indiquer la finalité aux utilisateurs <br />
	3) Collecter, utiliser ou divulguer des informations personnelles sur des utilisateurs à des fins inappropriées. 

	<p>Les utilisateurs basés à Singapour auront le droit de consulter les informations personnelles que nous aurons collectées, et demander à ce que ces informations soient retirées si besoin. </p>

	<p>Les utilisateurs basés en dehors de Singapour devront répondre aux régulations propres à leur pays d\'origine, en particulier concernant la protection des informations personnelles. </p>

	<p>Nous pouvons être en mesure de divulguer vos informations personnelles, si la Loi nous le demande ou si nous sommes en mesure de croire que cette action est nécessaire en vue de maintenir l\'intégrité de notre service.</p>

	<strong><u>En cas de fusion</u></strong>
	<p>Dans le cas où iLathys Pte Ltd serait engagée dans une activité de fusion/acquisition, vos informations personnelles seraient transférées. Nous vous informerons bien entendu de cette fusion avant de transférer les données personnelles vous concernant dont nous disposons.</p>

	<strong><u>Security</u></strong>
	<p>La sécurité de vos données personnelles est très important pour nous, mais nous souhaitons rappeler à nos utilisateurs qu\'aucune plateforme internet ne peut être sécurisée à 100% face aux attaques de hackers. Même si nous nous efforçons de protéger vos données (utilisation du service HTTPS par exemple), nous ne pouvons pas vous garantir son entière sécurité.</p>

	<strong><u>Transfert d\'information à l\'étranger</u></strong>
	<p>Vos données personnelles peuvent être transférées et sauvegardées sur des ordinateurs situés en dehors de votre pays, région, notamment sous une juridiction gouvernementale différente de la vôtre, où les lois en rapport avec la protection des données individuelles peuvent s\'avérer être différentes.</p>

	<p>Si vous êtes localisés en dehors de Singapour, et acceptez de nous fournir vos données personnelles, sachez que nous transférons vos données personnelles à Singapour pour les y analyser.</p>
	<p>En acceptant les présentes Privacy Policy, vous acceptez cette clause particulière. </p>

	<strong><u>Redirections vers d\'autres Sites/ Services</u></strong>
	<p>Notre service peut contenir des liens vers d\'autres sites dont nous n\'avons pas accès. Si vous décidez de cliquer sur l\'un de ces sites tiers, vous serez rediriger vers le site de ce service tiers. Nous vous recommandons fortement de lire la Privacy Policy de chaque site tiers avant de le rejoindre.</p>
	<p>Nous n\'avons aucun contrôle sur ces sites tiers, et n\'assumons aucune responsabilité des conséquences désastreuses auxquelles nos utilisateurs pourraient être confrontés, s\'ils décidaient de visiter ces sites.</p>

	<strong><u>Avertissement face aux activités illégales </u></strong>

<p>En vous inscrivant et en utilisant notre Service, vous acceptez d\'être pleinement responsable du contenu que vous diffusez à travers notre plateforme. Vous vous engagez à ne pas utiliser notre service à des fins illégales, ou dans le but de promouvoir du contenu illégale, diffamatoire, vulgaire, pornographique, ou qui pourrait atteindre la propriété intellectuelle (liste non exhaustive). </p>

<p>Vous vous engagez à ne pas utiliser notre service en vue de pratiquer du "junk mailing", "spam", ou distribution intensive d\'emails en vue de promouvoir un service/ produit. </p>

<p>Nous nous réservons le droit de cloturer votre accès à notre service si nous avons des raisons légitimes de croire que vous avez utilisé notre service à des illégales ou pour des activités non autorisées par la présente Privacy Policy. </p>

	<strong><u>Modification des présentes Privacy Policy</u></strong>

<p>Les présentes Privacy Policy sont susceptiblees d\'être modifiées par la suite, nous ne manquerons pas de vous tenir informés si tel est le cas. Nous vous conseillons fortement de relire ces Privacy Policy régulièrement afin de rester informer des éventuelles modifications. Les changements opérés dans le système sont directement reportés dans les présentes Privacy Policy.</p>

	<strong><u>Contact</u></strong>

<p>Si vous avez la moindre question, nous restons à votre disponibilité via la plateforme Zenintra.net</p>
	',
	'testi-text1' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
	'testi-title2' => 'Testimonial goes here',
	'testi-text2' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
	'About Us' => 'About Us',
	'Legal' => 'Legal',
	'Privacy' => 'Privacy Policy',
	'Facebook-url' => 'https://www.facebook.com/Zenintrafr',
	'Twitter-url' => 'https://twitter.com/zenintrafr',
	'Linkedin-url' => 'https://www.linkedin.com/company/zenintra-net?trk=company_logo',
	'Contact Us' => 'Contact Us',
    'Copyright' => 'Copyright 2015 Zenintra.net',
];