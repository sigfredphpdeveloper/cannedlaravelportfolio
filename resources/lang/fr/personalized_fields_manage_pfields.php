<?php

return [
"Personalized Settings for Employee's Profile" => "Réglages personnalisés pour les profils des Employés",
'Up to 10 fields' => "Jusqu'à 10 fichiers",
'Field Name' => 'Nom du fichier',
'Description' => 'Description',
'Type' => 'Type',
'Visibility' => 'Visibilité',
'Action' => 'Action',
'Edit' => 'Éditer',
'Delete' => 'Supprimer',
'Confirmation' => 'Confirmation',
'Are you sure you want to delete this record ?' => 'Etes-vous sûr de vouloir supprimer cette information ?',
'No' => 'Non',
'Yes' => 'Oui',
'Add Field' => 'Ajouter un Champ'


];
