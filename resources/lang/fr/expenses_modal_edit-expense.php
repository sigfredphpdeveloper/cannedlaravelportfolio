<?php

return [
'Edit Expense' => 'Editer la dépense',
'Expense Title' => 'Titre de la dépense',
'Details' => 'Détails',
'Save' => 'Enregistrer'
];
