<?php

return [
'Activity Title' => 'Titre de l\'Activité',
'Call' => 'Call',
'Meeting' => 'Meeting',
'Email' => 'Email',
'Deadline' => 'Deadline',
'Trash' => 'Trash',
'Other' => 'Other',
'Date' => 'Date',
'Time' => 'Heure',
'Duration' => 'Durée',
'Owner' => 'Propriétaire',
'Assigned' => 'Assignée',
'Done' => 'Fait',
'Close' => 'Fermer',
'Save changes' => 'Enregistrer les modifications'


];
