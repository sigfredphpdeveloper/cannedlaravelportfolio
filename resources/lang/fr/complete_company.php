<?php

return [
'Complete Your Company Profile' => 'Compléter le Profil de votre Entreprise !',
'Whoops!' => 'Whoops!',
'There were some problems with your input.' => "Il y a eu quelques problèmes avec la saisie de vos informations.",
'Please fill in the form to configure the platform for your company (Step 2)' => 'Veuillez remplir le formulaire pour configurer la plateforme pour votre Entreprise (étape 2)',
'My Company' => 'Mon Entreprise',
'Company' => 'Entreprise',
'Name of Company' => 'Nom de votre entreprise',
'Website' => 'Site Web',
'Industry *' => 'Industrie *',
'--- Select Industry ---' => 'Choisir une Industrie',
'Agency (branding/design/creative/video)' => 'Agence (branding/design/création/vidéo)',
'Agency (web)' => 'Agence (web)',
'Agency (SEM/SEO/social media)' => 'Agence (SEM/SEO/social media)', //Traduction ??
'Agency (other)' => 'Agence (autre)',
'Church/religious organisation' => 'Église/Organisation religieuse',
'E-Commerce' => 'E-Commerce',
'Engineering' => 'Ingénierie',
'Health/beauty' => 'Santé/Beauté',
'Hospitality' => 'Hospitalité',
'Industrials' => 'Industriels',
'IT/technology' => "Technologies de l'Information/technologies",
'Media/publishing' => 'Media/Édition', 
'Non profit' => 'Non lucratif',
'Other' => 'Autre',
'Retail/consumer merchandise' => 'Vente au détail/Biens de consommation',
'School/education' => 'École/Éducation',
'Software/SaaS' => 'Software/SaaS', // No translation I guess
'Travel/leisure' => 'Voyages/Loisirs',
'Company Size' => "Taille de l'entreprise",
'Address *' => 'Adresse',
'Address' => 'Adresse',
'Zip Code' => 'Code Postal',
'City' => 'Ville',
'Country *' => 'Pays',
'State' => 'Région',
'Phone Number' => 'Numéro de téléphone',
'Company Logo' => 'Logo entreprise',
'Update' => 'Mettre à jour',
'recommended'=>'(conseillé)',
'Back To Settings'=>'Retour aux paramètres',
'Create Roles for your company'=>'Créer des rôles pour votre entreprise',
'A Role is used for a type'=>'Un rôle est utilisé pour un type d\'activité et peut être pour une ou plusieurs personnes dans l\'entreprise, pour l\'assurance «représentant des ventes» ou «CFO».',
'Skip'=>'Sauter',
'Create New Role'=>'Créer un nouveau rôle',
'Create Teams for your company'=>'Create Teams for your company',
'A Team is for groups of people'=>'A Team is for groups of people across activities: for instance a team for a new product may include one person from marketing,
  one from production and one from finance.',
 'Create New Team'=>'Créer une nouvelle équipe',
 'Name'=>'prénom',
 'Role'=>'Rôle',
 'Title'=>'Titre',
 'Name, Role or Team'=>'Nom, rôle ou de l\'équipe',
 'Import CSV'=>'importation CSV',
 'Emergency Contacts'=>'Les contacts d\'urgence',
 'Now that your company is set up'=>'Maintenant que votre entreprise est mis en place, invitez vos employés à se joindre à Zenintra.net et accéder aux outils',
'Welcome'=>'Bienvenue',
'OK'=>'d\'accord'








];
