<?php

return [
'Manage Permissions' => 'Gérer les permissions',
'Save' => 'Enregistrer',
'Save Assigned Permissions' => 'Enregistrer les permissions assignées'
];
