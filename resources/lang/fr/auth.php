<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Ces informations ne correspondent pas avec nos données.',
    'throttle' => 'Trop de tentatives de connexion. Veuillez ré-essayer dans :seconds secondes.'

];
