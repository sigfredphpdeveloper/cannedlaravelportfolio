<?php

return [
    'Reports' => 'Rapports',
    'Companies Signups' => 'Entreprises Signups',
    'From' => 'De',
    'Date From' => 'Dater de',
    'To' => 'À',
    'Date To' => 'Jusqu\'à la date',
    'Last 30 days' => 'Les 30 derniers jours',
    'Last 6 Months' => '6 derniers mois',
    'Last 12 Months' => '12 derniers mois',
    'Beginning of time' => 'La nuit des temps',
    'Cumulative Number of Companies Per Day' => 'Nombre cumulatif d\'entreprises par jour',
    'Filter' => 'Filtre',
    'Cumulative Number of Users Per Day'=>'Cumulatif Nombre d\'utilisateurs par jour',
    'New Companies Registration Per Day'=>'Nouvelles entreprises Inscription par jour',
    'User Signups'=>'Signups de l\'utilisateur',
    'Dashboard'=>'Tableau de bord'
];