<?php

return [
'Add Referral' => 'Envoyer une invitation',
'You need more space ?' => 'Besoin de plus de place ?',
'You need more space? Invite other companies to join Zenintra and earn 200Mb additional Zenintra disk space for each company joining (up to 5Gb).' => 'Vous avez besoin de plus d\'espace? Inviter d\'autres entreprises à se joindre à Zenintra et gagner 200 Mb espace disque supplémentaire Zenintra pour chaque entreprise d\'assemblage (jusqu\'à 5Go).',
'Email Address' => 'Adresse Email',
'Add more' => 'Ajouter',
'Invitation Email Title' => 'Titre de l\'Email d\'invitation',
'Invitation Message' => 'Message d\'invitation',
'Invite' => 'Inviter',
'Back' => 'Retour',
'Please enter email' => 'Veuillez entrer une adresse email',
'Enter your Invitation Title (optional)' => 'Tapez votre titre d\'invitation (optionnel)',
'Please enter your invitational message. (optional)' => 'Veuillez entrer votre message d\'invitation (optionnel)'
];
