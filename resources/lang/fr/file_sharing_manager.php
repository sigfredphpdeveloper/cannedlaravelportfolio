<?php

return [
'Upload File' => 'Télécharger un fichier',
'Add Folder' => 'Ajouter un dossier',
'File Manager' => 'Gérer les fichiers',
'Folders' => 'Dossiers',
'Folder Name' => 'Nom du dossier',
'Size' => 'Taille',
'Manage Permissions' => 'Gérer les permissions',
'Remove Folder' => 'Retirer le dossier',
'Files' => 'Fichiers',
'Name' => 'Nom',
'Size' => 'Taille',
'Download File' => 'Télécharger le fichier',
'Delete File' => 'Supprimer le fichier',
'Enter Folder Name *' => 'Entrer le nom du dossier *',
'Set Permission' => 'Régler la permission',
'Role' => 'Rôle',
'Read' => 'Lecture',
'Write' => 'Écriture',
'No Access' => "Pas d'accès",
'No' => 'Non',
'Yes' => 'Oui',
'Save' => 'Enregistrer',
'Please Enter File Name' => 'Veuillez entrer un nom de fichier',
'Please enter file name..' => 'Veuillez entrer un nom de fichier..',
'Cancel' => 'Annuler',
'Start Upload' => 'Démarrer le téléchargement',
'Confirmation' => 'Confirmation',
'Are you sure you want to delete this file ?' => 'Etes-vous sûr de vouloir supprimer ce fichier ?',
'Are you sure you want to delete this folder ?' => 'Etes-vous sûr de vouloir supprimer ce dossier ?',
'Created'=>'Créé',
'Delete All'=>'Supprimer tout',
'Customize File Sharing Module'=>'Personnaliser fichier Module de partage',
'Customize'=>'Personnaliser'


];
