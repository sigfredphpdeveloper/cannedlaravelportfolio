<?php

return [
'Manage Referrals' => 'Gérer les invitations',
'Add Referral' => 'Envoyer une invitation',
'Import CSV' => 'Importer CSV',
'Search' => 'Chercher',
'Invitation Sent To' => 'Invitation envoyée à',
'Date' => 'Date',
'Status' => 'Statut',
'Confirmation' => 'Confirmation',
'Are you sure you want to delete this record ?' => 'Etes-vous sûr de vouloir supprimer cette donnée ?',
'No' => 'Non',
'Yes' => 'Oui',
'Referral Details' => 'Détails de l\'invitation',
'OK' => 'OK'
];
