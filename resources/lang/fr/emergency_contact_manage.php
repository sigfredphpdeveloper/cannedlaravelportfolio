<?php

return [
'Enter Keywords' => 'Entrer des Mots Clés',
'Search' => 'Rechercher',
'Emergency Contacts' => 'Contacts d\'Urgence',
'Employee' => 'Employés',
'Emergency Contact' => 'Contat d\'Urgence',
'Emergency Phone' => 'Numéro d\'Urgence',
'View' => 'Voir',
'Edit' => 'Éditer',
'Export CSV' => 'Exporter en CSV',
'Confirmation' => 'Confirmation',
'Are you sure you want to delete this User ?' => 'Etes-vous sûr de vouloir supprimer cet utilisateur ?',
'No' => 'Non',
'Yes' => 'Oui',
'View Emergency Contact' => 'Voir le Contact d\'Urgence',
'OK' => 'OK'
];
