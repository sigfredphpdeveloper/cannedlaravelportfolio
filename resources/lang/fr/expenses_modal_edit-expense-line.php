<?php

return [
'Add Expense Line' => 'Ajouter une ligne de dépense',
'Title' => 'Titre',
'Expense Type' => 'Type de dépense',
'Choose Expense Type' => 'Choisir un type de dépense',
'Save' => 'Enregistrer'
];
