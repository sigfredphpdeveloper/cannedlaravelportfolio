<?php

return [
'Contacts' => 'Contacts',
'Add' => 'Ajouter',
'Bulk Upload' => 'Transfert de masse',
'Search for' => 'Rechercher',
'Search' => 'Rechercher',
'Contact Name' => 'Nom du contact',
'Organization' => 'Organisation',
'Email' => 'Email',
'Owner' => 'Propriétaire',
'Actions' => 'Actions',
'Edit' => 'Éditer',
'Delete' => 'Supprimer',
'Contact Email' => 'Email du contact',
'Contact Phone' => 'n° de téléphone du contact',
'Position' => 'Poste',
'Address' => 'Adresse',
'Notes' => 'Notes',
'Delete' => 'Supprimer',
'Close' => 'Fermer',
'Save changes' => 'Enregistrer les modifications',
'Add a new Contact' => 'Ajouter un nouveau contact',
'Edit Contact' => 'Éditer le Contact',
'Confirmation' => 'Confirmation',
'Are you sure you want to delete this record ?' => 'Etes-vous sûr de vouloir supprimer cette donnée ?',
'No' => 'Non',
'Yes' => 'Oui'

];
