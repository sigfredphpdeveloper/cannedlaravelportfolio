<?php

return [
'CRM Module' => 'Module CRM',
'Company Stages' => 'Étapes de l\'Entreprise',
'Add a stage' => 'Ajouter une Étape',
'Edit Rank' => 'Éditer le rang',
'Stages' => 'Étape',
'Description' => 'Description',
'Rank' => 'Rang',
'Edit' => 'Éditer',
'Delete' => 'Supprimer'

];

