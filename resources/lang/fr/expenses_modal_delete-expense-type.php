<?php

return [
'Confirmation' => 'Confirmation',
'Warning' => 'Attention',
'This Expense type may be associated already with expenses.' => 'Ce type de dépense peut être associé avec des dépenses déjà existantes.',
'Are you sure you want to delete this Expense Type ?' => 'Etes-vous sûr de vouloir supprimer ce type de dépense ?',
'No' => 'Non',
'Yes' => 'Oui'
];
