<?php

return [
'Edit Expense Type Item' => 'Éditer le type de dépense',
'Expense Type Title' => 'Titre du type de dépense',
'Update' => 'Mettre à jour'
];
