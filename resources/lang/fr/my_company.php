<?php

return [
'Update Company' => "Mettre à jour l'entreprise",
'My Company' => 'Mon Entreprise',
'Company' => 'Entreprise',
'Name of Company' => "Nom de l'entreprise",
'Website' => 'Site Web',
'Industry' => 'Industrie',
'Select Industry' => 'Choisir une Industrie',
'Agency (branding/design/creative/video)' => 'Agence (branding/design/création/vidéo)',
'Agency (web)' => 'Agence (web)',
'Agency (SEM/SEO/social media)' => 'Agence (SEM/SEO/social media)', //Traduction ??
'Agency (other)' => 'Agence (autre)',
'Church/religious organisation' => 'Église/Organisation religieuse',
'E-Commerce' => 'E-Commerce',
'Engineering' => 'Ingénierie',
'Health/beauty' => 'Santé/Beauté',
'Hospitality' => 'Hospitalité',
'Industrials' => 'Industriels',
'IT/technology' => "Technologies de l'Information/technologies",
'Media/publishing' => 'Media/Édition', 
'Non profit' => 'Non lucratif',
'Other' => 'Autre',
'Retail/consumer merchandise' => 'Vente au détail/Biens de consommation',
'School/education' => 'École/Éducation',
'Software/SaaS' => 'Software/SaaS', // No translation I guess
'Travel/leisure' => 'Voyages/Loisirs',
'Company Size' => "Taille de l'entreprise",
'Address' => 'Adresse',
'Zip Code' => 'Code Postal',
'City' => 'Ville',
'Country' => 'Pays',
'State' => 'Région',
'Phone Number' => 'Numéro de téléphone',
'Company Logo' => 'Logo entreprise',
'Update' => 'Mettre à jour'


];