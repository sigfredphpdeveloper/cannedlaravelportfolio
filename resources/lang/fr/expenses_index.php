<?php

return [
'My Expenses' => 'Mes Dépenses',
'My Expenses List' => 'Ma liste de Dépenses',
'Submit a New Expense' => 'Soumettre une nouvelle dépense',
'Submision Date' => 'Date de soumission',
'Expense ID' => 'ID Dépense',
'Title' => 'Titre',
'Total Amount' => 'Montant total',
'Status' => 'Statut',
'Approve Date' => 'Date d\'approbation',
'Paid Date' => 'Date de paiement',
'Action' => 'Action',
'View' => 'Voir',
'Edit' => 'Éditer',
'Delete' => 'Supprimer',
'No Expenses' => 'Aucune dépense'
];
