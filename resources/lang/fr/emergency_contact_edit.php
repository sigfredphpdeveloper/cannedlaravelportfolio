<?php

return [
'Contact Name' => 'Nom du contact',
'Phone Number' => 'Numéro de téléphone',
'Address' => 'Adresse',
'Relationship' => 'Relation',
'Spouse' => 'Épouse',
'Parent' => 'Parent',
'Child' => 'Enfant',
'Friend' => 'Ami',
'Other' => 'Autre',
'Other Relationship' => 'Autre relation',
'Close' => 'Fermer',
'Save changes' => 'Enregistrer les modifications'

];


