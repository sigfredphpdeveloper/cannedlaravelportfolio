<?php

return [
	
	'Blog' => 'Blog',
	'Blog-url' => 'http://blog.zenintra.net',
	'Features' => 'Features',
	'Pricing' => 'Pricing',
	'Sign up' => 'Sign up',
	'Log in' => 'Log in',
	'Terms-title' => 'Terms of use',
	'content'	=> '
	<p><strong><em>Last updated: January 11,2016</em></strong> </p>

	<strong>iLathys Pte Ltd</strong><br />
	71 Ayer Rajah Crescent, #05­08<br />
	Singapore, 139951

	<p>THIS IS AN AGREEMENT BETWEEN YOU/ THE ENTITY THAT YOU REPRESENT AND ILATHYS PTE LTD GOVERNING YOUR USE OF OUR SERVICE.</p>

	<p>Please read these Terms of Use ("Terms", "Terms of Use") and our Privacy Policy carefully before using the <a href="http://zenintra.net">http://zenintra.net</a> website (the "Service") operated by iLathys Pte Ltd ("us", "we", or "our").</p>

	<p>Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.</p>

	<p>By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.</p>

	<p><strong>Content, Description of Service</strong></p>
	<p>Zenintra.net provides an array of tools to manage your team, your internal communication, file sharing and other features. You may use the Service for your personal and business use or for the internal business purpose in the organization that you represent. You may connect to the Services using any internet browser supported by the Services.</p>

	<p>We  store  and  maintain  documents,  files,  data  stored  in  your  user  account  at  our facilities in Singapore and other locations. We also keep backup copies of your data in case of errors or system failures from Zenintra webservices. </p>

	<p>Our Service allows you to post, link, store, share and otherwise make available certain information, text, graphics, videos, or other material ("Content"). You are responsible for the Content that you post to the Service, including its legality, reliability, and appropriateness.</p>
	
	<p>You retain any and all of your rights to any Content you submit, post or display on or through the Service and you are responsible for protecting those rights.</p>
	
	<p>You represent and warrant that: (i) the Content is yours (you own it) or you have the right to use it and grant us the rights and license as provided in these Terms, and (ii) the posting of your Content on or through the Service does not violate the privacy rights, publicity rights, copyrights, contract rights or any other rights of any person.</p>

	<p><strong>Accounts</strong></p>
	<p>When you create an account with Zenintra, you must provide us information that is accurate, complete, and current at all times. Failure to do so constitutes a breach of the Terms, which may result in immediate termination of your account on our Service.</p>

	<p>You are responsible for safeguarding the password that you use to access the Service and for any activities or actions under your password, whether your password is with our Service or a third-party service.</p>
	
	<p>You agree not to disclose your password to any third party. You must notify us immediately upon becoming aware of any breach of security or unauthorized use of your account. </p>
	
	<p>You may not use as a username the name of another person or entity or that is not lawfully available for use, a name or trade mark that is subject to any rights of another person or entity other than you without appropriate authorization, or a name that is otherwise offensive, vulgar or obscene.</p>

	<p><strong>Intellectual Property</strong></p>
	<p>The Service and its original content (excluding Content provided by users), features and functionality are and will remain the exclusive property of iLathys Pte Ltd and its licensors. The Service is protected by copyright, trademark, and other laws of both the Singapore and foreign countries. Our trademarks and trade dress may not be used in connection with any product or service without the prior written consent of iLathys Pte Ltd.</p>

	<p><strong>Links To Other Web Sites</strong></p>
	<p>Our Service may contain links to third-party web sites or services that are not owned or controlled by iLathys Pte Ltd.</p>
	<p>iLathys Pte Ltd has no control over, and assumes no responsibility for, the content, privacy policies, or practices of any third party web sites or services. You further acknowledge and agree that iLathys Pte Ltd shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such web sites or services.</p>
	<p>We strongly advise you to read the terms and conditions and privacy policies of any third-party web sites or services that you visit.</p>

	<p><strong>Termination</strong></p>
	<p>We may terminate or suspend your account immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms.</p>
	<p>Upon termination, your right to use the Service will immediately cease. If you wish to terminate your account, you may simply discontinue using the Service.</p>

	<p><strong>Limitation Of Liability</strong></p>
	<p>In no event shall iLathys Pte Ltd, nor its directors, employees, partners, agents, suppliers, or affiliates, be liable for any indirect, incidental, special, consequential or punitive damages, including without limitation, loss of profits, data, use, goodwill, or other intangible losses, resulting from (i) your access to or use of or inability to access or use the Service; (ii) any conduct or content of any third party on the Service; (iii) any content obtained from the Service; and (iv) unauthorized access, use or alteration of your transmissions or content, whether based on warranty, contract, tort (including negligence) or any other legal theory, whether or not we have been informed of the possibility of such damage, and even if a remedy set forth herein is found to have failed of its essential purpose.</p>

	<p><strong>Disclaimer</strong></p>
	<p>Your use of the Service is at your sole risk. The Service is provided on an "AS IS" and "AS AVAILABLE" basis. The Service is provided without warranties of any kind, whether express or implied, including, but not limited to, implied warranties of merchantability, fitness for a particular purpose, non-infringement or course of performance.</p>
	<p>iLathys Pte Ltd its subsidiaries, affiliates, and its licensors do not warrant that</p>
	<p>a) the Service will function uninterrupted, secure or available at any particular time or location; <br/>
	b) any errors or defects will be corrected; c) the Service is free of viruses or other harmful components; or <br/>
	d) the results of using the Service will meet your requirements.</p>

	<p><strong>Governing Law</strong></p>
	<p>These Terms shall be governed and construed in accordance with the laws of Singapore, without regard to its conflict of law provisions.</p>
	<p>Our failure to enforce any right or provision of these Terms will not be considered a waiver of those rights. If any provision of these Terms is held to be invalid or unenforceable by a court, the remaining provisions of these Terms will remain in effect. These Terms constitute the entire agreement between us regarding our Service, and supersede and replace any prior agreements we might have between us regarding the Service.</p>

	<p><strong>Changes</strong></p>
	<p>We reserve the right, at our sole discretion, to modify or replace these Terms at any time. What constitutes a material change will be determined at our sole discretion. By continuing to access or use our Service after those revisions become effective, you agree to be bound by the revised terms. If you do not agree to the new terms, please stop using the Service.</p> 
	
	<p><strong>Contact Us</strong></p>
	<p>If you have any questions about these Terms, please contact us via the Zenintra.net online platform.</p>
	',
	'testi-text1' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
	'testi-title2' => 'Testimonial goes here',
	'testi-text2' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
	'Footer-Privacy-Policy' => 'Privacy Policy',
	'Footer-Terms-of-use' => 'Terms of use',
	'Footer-About-us' => 'About us',
	'Footer-Contact-us' => 'Contact us',
	'Footer-Support' => 'Support',
	'Footer-Faq' => 'FAQ',
	'Footer-Follow-us' => 'Follow us on',
	'Facebook-url' => 'https://www.facebook.com/zenintra',
	'Twitter-url' => 'https://twitter.com/zenintra',
	'Youtube-url' => 'http://www.youtube.com/c/ZenintraNetOfficial',
	'Linkedin-url' => 'https://www.linkedin.com/company/zenintra-net?trk=company_logo',
	'Contact Us' => 'Contact Us',
    'Copyright' => 'Copyright ' . date("Y"). ' Zenintra.net',
    //Contact Us Modal
	'Close' => 'Close',
    'Name'	=> 'Name',
    'Email' => 'Email',
    'Message' => 'Message',
	'John Smith' => 'John Smith',
	'sample-email' => 'jsmith@gmail.com',
	'What can we do for you?' => 'What can we do for you?',
	'Submit' => 'Submit',
    'Your message was successfully sent! We will get back to you soon\.' => 'Your message was successfully sent! We will get back to you soon\.',
    'Please fill out all fields.' => 'Please fill out all fields.',
    'Free intranet and internal communication for startups and small businesses'=> 'Free intranet and internal communication for startups and small businesses'
];