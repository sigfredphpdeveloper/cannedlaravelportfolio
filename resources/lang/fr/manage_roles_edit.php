<?php

return [
'Back' => 'Retour',
'Edit Role' => 'Éditer le Rôle',
'Warning.' => 'Attention.',
'Role Name' => 'Nom du Rôle',
'Role Description' => 'Description du Rôle',
'Save' => 'Enregistrer'
];

