<?php

return [

    'Contact Us' => 'Contactez nous',
    'Productivity Boost for Startups' => 'Intranet gratuit et communication interne pour startups et PMEs',
    'Coming soon! We are working on it' => 'Bientôt Disponible!',
    'Get notified when we open' => 'Inscrivez-vous sur la liste d\'attente',
	'mailchimplistid' => '157996126f',
    'Free intranet and internal communication for startups' => 'Intranet gratuit et communication interne pour startups et PMEs',
    'main-content-p-text' => 'Améliorez la productivité de votre startups ou société. Réduisez le volume d’emails internes grâce au tchat d’entreprise de Zenintra.net, et boostez la productivité de votre équipe avec tous les autres outils de Zenintra.net. Inscrivez-vous sur notre liste d\'attente pour être dans les premiers à avoir un accès gratuit à vie pour votre entreprise.',
    'Copyright 2015 Zenintra.net' => 'Tous droits réservés 2015 Zenintra.net',
    'Email Address' => 'Adresse Email',
    'Close' => 'Fermer',
    'Subscribe' => 'S\'inscrire',
    'Name' => 'Nom',
    'John Smith' => 'John Smith',
    'Email' => 'Email',
    'sample-email' => 'jsmith@gmail.com',
    'Message' => 'Message',
    'What can we do for you?' => 'En quoi peut-on vous aider?',
    'Submit' => 'Envoyer',
    'Your message was successfully sent! We will get back to you soon\.' => 'Votre message a bien été envoyé! Nous vous contacterons très prochainement\.',
    'Please fill out all fields.' => 'Merci de remplir tous les champs demandés.',
    'Free intranet and internal communication for startups and small businesses'=> 'Intranet gratuit et communication interne pour startups et PMEs',
    'Facebook-url' => 'https://www.facebook.com/Zenintrafr',
    'Twitter-url' => 'https://twitter.com/zenintrafr',
    'Linkedin-url' => 'https://www.linkedin.com/company/zenintra-net?trk=company_logo',
];
