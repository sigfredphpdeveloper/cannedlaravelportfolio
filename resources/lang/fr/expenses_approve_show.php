<?php

return [
'Approve Expenses' => 'Approuver les Dépenses',
'Update Status' => 'Mettre à jour le Statut',
'Pending' => 'En Attente',
'Approve' => 'Approuver',
'Reject' => 'Rejeter',
'Request Details' => 'Demander Détails',
'Cancel' => 'Annuler',
'Total Amount' => 'Montant Total',
'Status' => 'Statut',
'Creation Date' => 'Date de création',
'Paid Date' => 'Date de paimenet',
'Approval Date' => 'Date d\'Approbation',
'No Expenses Line' => 'Aucune ligne de dépense',
'Add Request Details Text' => 'Ajouter un text de demande de détails',
'Details' => 'Détails',
'Save' => 'Enregistrer',
'Update' => 'Mettre à jour'
];

