<?php

return [
'Manage Team' => 'Gérer les équipes',
'Add Team' => 'Ajouter une équipe',
'Search' => 'Chercher',
'Team' => 'Équipe',
'Team Description' => "Description de l'équipe",
'Action' => 'Action',
'Edit' => 'Éditer',
'Delete' => 'Supprimer',
'Confirmation' => 'Confirmation',
'Are you sure you want to delete this record ?' => 'Etes vous sûr de vouloir supprimer cette information ?',
'No' => 'Non',
'Yes' => 'Oui',
'Enter Keywords' => 'Entrer des mots clés'
];
