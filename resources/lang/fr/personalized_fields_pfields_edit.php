<?php

return [
'Edit Field' => 'Éditer le Champ',
'Back' => 'Retour',
'Warning.' => 'Attention.',
'Field Name *' => 'Nom du Champ *',
'Field Name' => 'Nom de Champ',
'Field Type *' => 'Type du Champ *',
'Dropdown Options'  => 'Options de liste',
'Separated by new line' => 'Séparé par une nouvelle ligne',
'Field Description' => 'Description du Champ',
'Visibility' => 'Visibilité',
'Save' => 'Enregistrer'



];

