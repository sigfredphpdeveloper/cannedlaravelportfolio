<?php

return [
'Emergency Contact' => 'Contact d\'Urgence',
'Contact Name' => 'Nom du Contact',
'Phone Number' => 'Numéro de téléphone',
'Address' => 'Adresse',
'Relationship' => 'Relation',
'Spouse' => 'Épouse',
'Parent' => 'Parent',
'Child' => 'Enfant',
'Friend' => 'Ami',
'Other' => 'Autre',
'Other Relationship' => 'Autre Relation',
'Save' => 'Enregistrer'
];
