<?php

return [
'Manage Announcements' => 'Gérer les Annonces',
'Add Announcement' => 'Publier une Annonce',
'Title' => 'Titre',
'Message' => 'Message',
'Action' => 'Action',
'Edit' => 'Éditer',
'Delete' => 'Supprimer',
'Confirmation' => 'Confirmation',
'Are you sure you want to delete this announcement ?' => 'Etes-vous sûr de vouloir supprimer cette annonce ?',
'No' => 'Non',
'Yes' => 'Oui',
'Posted Date' => 'Date de publication',
];
