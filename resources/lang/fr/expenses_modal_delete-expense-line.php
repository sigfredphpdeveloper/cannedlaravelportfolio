<?php

return [
'Confirmation' => 'Confirmation',
'Are you sure you want to delete this Expense Line ?' => 'Etes-vous sûr de vouloir supprimer cette ligne de dépense ?',
'No' => 'Non',
'Yes' => 'Oui'
];
