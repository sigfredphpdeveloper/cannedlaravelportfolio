<?php

return [
    'Manage Email Templates' => 'Gérer Email Templates',
    'Subject'=>'Assujettir',
    'Slug'=>'Limace',
    'Action'=>'action',
    'Edit'=>'modifier',
    'Back'=>'Arrière',
    'Edit Email Template'=>'Modifier Email Template',
    'Message Body'=>'Corps du message',
    'Save changes'=>'Sauvegarder les modifications',
    'Dashboard'=>'Tableau de bord',
    'Users'=>'Utilisateurs',
    'Manage Users'=>'gérer les utilisateurs',
    'Companies'=>'Entreprises',
    'Manage Companies'=>'Gérer entreprises',
    'Email Templates'=>'Email Templates',
    'Manage'=>'Gérer',

    'Reports' => 'Rapports',
    'Export Directory Data' => 'Directory Data Export',
    'Export Leaves Data' => 'Export Feuilles de données',


];