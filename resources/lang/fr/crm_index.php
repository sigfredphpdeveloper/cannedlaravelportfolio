<?php

return [
'Add a Contact' => 'Ajouter un contact',
'Add a deal' => 'Ajouter un deal',
'Stage' => 'Étape',
'Stage 1' => 'Étape 1',
'Stage 2' => 'Étape 2',
'Stage 3' => 'Étape 3',
'Stage X' => 'Étape X',
'Stage x' => 'Étape x',
'Add a Deal' => 'Ajouter un Deal',
'Deal Title' => 'Titre du Deal',
'Contact Name' => 'Nom du Contact',
'Organization Name' => 'Nom Organisation',
'Visibility' => 'Visibilité',
'Me' => 'Moi',
'Team' => 'Équipe',
'Every One' => 'Tout le monde',
'Expected Value' => 'Valeur attendue',
'Close' => 'Fermer',
'Save changes' => 'Enregistrer les modifications',
'Edit a Deal' => 'Éditer le Deal'


];
