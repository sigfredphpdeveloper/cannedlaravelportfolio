<?php

return [
'Manage Roles' => 'Gérer les Rôles',
'Add Role' => 'Ajouter un Rôle',
'Search' => 'Chercher',
'Role' => 'Rôle',
'Role Description' => 'Description du Rôle',
'Action' => 'Action',
'Edit' => 'Éditer',
'Delete' => 'Supprimer',
'Confirmation' => 'Confirmation',
'Are you sure you want to delete this record ?' => 'Etes vous sûr de vouloir supprimer cette information ?',
'No' => 'Non',
'Yes' => 'Oui',
'Enter Keywords' => 'Entrer des mots clés'
];
