<?php

return [
'Update Organization' => 'Mettre à jour l\'Organisation',
'Organization Name' => 'Nom de l\'Organisation',
'Organization Address' => 'Adresse de l\'Organisation',
'Visibility' => 'Visibilité',
'Me' => 'Moi',
'Team' => 'Équipe',
'Every One' => 'Tout le monde',
'Delete' => 'Supprimer',
'Close' => 'Fermer',
'Save changes' => 'Enregistrer les modifications'
];
