<?php

return [
'Employee' => 'Employé',
'Emergency Contact Name' => 'Nom du Contact d\'Urgence',
'Emergency Contact Phone' => 'Téléphone du Contact d\'Urgence',
'Emergency Contact Address' => 'Adresse du Contact d\'Urgence',
'Emergency Contact Relationship' => 'Relation avec le Contact d\'Urgence'


];
