<?php

return [
'Add Team' => 'Ajouter une Équipe',
'Team Name' => 'Nom de l\'Équipe',
'Team Description' => 'Description de l\'Équipe',
'Save' => 'Entregistrer',
'Back' => 'Retour',
'Next'=>'Prochain',
'Add More'=>'Ajouter plus',
'Remove'=>'Retirer'

];
