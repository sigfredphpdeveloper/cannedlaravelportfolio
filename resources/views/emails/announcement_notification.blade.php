<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>New annoucement from {{$company->company_name}}</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>


<body style="margin: 0; padding: 0;">

 <div style="padding:30px;">
 	@include('emails.logo-header')
    <p>Dear {{$name}},</p>

    <p>This is a new announcement from {{$company->company_name}}</p>


    <p>{{$announcement_title}}</p>

    <p>{!! nl2br($announcement_message) !!}</p>

 	<br />
 	@include('emails.signature')

 </div>

</body>


</html>