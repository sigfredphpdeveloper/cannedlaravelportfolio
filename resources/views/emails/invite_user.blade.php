<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Join our <?=!empty($company_name)?$company_name:''?> intranet </title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>


<body style="margin: 0; padding: 0;">

 <div style="padding:30px;">
 @include('emails.logo-header')
     <p>Dear {{ $invitee_first_name }}, </p>
     <br />
     <p>
     Welcome to Zenintranet </p>
     <p>
     {{ $invitor_name  }} has invited you to join the company {{ $company_name }}
     </p>
     <p>
     Please click on the link below to create a password for your account (username {{ $your_username }})
     </p>
    <p>{!! $link !!}</p>


 <br />

 @include('emails.signature')
 </div>

</body>


</html>