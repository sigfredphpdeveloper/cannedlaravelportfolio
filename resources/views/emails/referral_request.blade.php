<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Join {{$invitee_name}} from {{$referrer_company}} on Zenintranet ! </title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>


<body style="margin: 0; padding: 0;">

 <div style="padding:30px;">
 	@include('emails.logo-header')
    <p>Hello, </p>
	<br />
	
	<p>
		My name is {{$referrer_name}} from {{$referrer_company}}.
		I am using Zenintra.net, an all-in-one FREE intranet to manage my team and my business, which I recommend to you.
	</p>

	<br>
	<p>
		To create an account, just click here or copy the link below:
		<br>
		{!! $link !!}
	</p>

 	<br />
 	@include('emails.signature')


 </div>
</body>
</html>