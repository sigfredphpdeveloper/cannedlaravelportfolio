<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Your expenses status has been updated</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>


<body style="margin: 0; padding: 0;">

 <div style="padding:30px;">
 	@include('emails.logo-header')
    <p>Dear {{$user}},</p>

    <p>This is to inform you the Expense request "{{$expense_title}}"" status is updated to <span style="color:#57889c; text-transform:uppercase">{{$status}}</span><br /></p>
    <p><?=!empty($details)?$details:''?></p>
     <br />

     @include('emails.signature')
 </div>

</body>


</html>