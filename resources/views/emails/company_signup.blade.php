   <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Welcome to Zenintra.net</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>


<body style="margin: 0; padding: 0;">

 <div style="padding:30px;">
    @include('emails.logo-header')

    <p>Dear {{$user->first_name}} {{$user->last_name}},</p>

    <p>Thank you for joining Zenintra.net. You will soon be able to manage your company from a single platform for FREE! </p>

    <p>Please click on the link below to activate your account:</p>

     <p>{!! $link !!}</p>

     <br />

	@include('emails.signature')
 </div>

</body>


</html>