<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Your intranet activity (first week)</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>


<body style="margin: 0; padding: 0;">

 <div style="padding:30px;">
 	@include('emails.logo-header')
    <p>Dear {{$user}},</p>

	<p>
		It has been a week since you created your Zenintra.net account. 
		If you need any help or have suggestions for us, please contact us at https://zenintra.zendesk.com/hc/en-us/requests/new.
	</p>
	
    <br />
	<p>You can also invite your friends or business partners through our referral program:</p>
	<p><?=!empty($link)?$link:''?></p>
	<p>and get 250MB more of storage space per referred company.</p>

	<br />

	@include('emails.signature')
 </div>

</body>


</html>