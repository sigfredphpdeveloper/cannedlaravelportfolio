   <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>You’re account is now activated!</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>


<body style="margin: 0; padding: 0;">

 <div style="padding:30px;">
 	@include('emails.logo-header')
    <p>Congratulations and welcome to the Zenintra.net community!</p>

	<p>In order to get the most out of Zenintra.net, we have prepared some short video tutorials for you:</p>
	<p>{!! $link !!}</p>

	<br />
	<p>Have a nice day!</p>

	<br />

	@include('emails.signature')

 </div>

</body>


</html>