<!doctype html>
<?php
function page_finalhome_trans($key,$lang = null){
    if(!$lang){
        $lang = 'en';
    }
    return trans($key,[],null,$lang);
}
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    @include('meta')

    @include('frontend.includes.favicon')


    <link rel="stylesheet" href="{{url('/')}}/assets/public_assets/css/bootstrap/bootstrap.css">
    <link href="{{url('/')}}/assets/public_assets/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{url('/')}}/assets/public_assets/css/jquery.mmenu.all.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>

    <script type="text/javascript" src="{{url('/')}}/assets/public_assets/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/assets/public_assets/js/jquery.mmenu.min.all.js"></script>

    <script type="text/javascript" src="{{url('/')}}/assets/public_assets/js/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/assets/public_assets/js/script.js"></script>
    <style>
        ul.payroll_list_paragraph li{ font-size: 28px;margin: 10px 0; text-align: center; line-height: 30px; }
    </style>
</head>

<body style="padding-top:79px;">

@include('frontend.includes.google_tag_manager')

@include('page_head')


<div id="content"><!--content-->
    <section class="container-fluid custom-bg1">
        <div class="container">
            <h1 align="center" style="font-size:48px;margin-bottom:0;color:#fff;"><span style="color:#ff6600;">Free</span> Payroll Platform in Singapore</h1>
            <!--<h2 align="center" style="font-size:34px;margin-top:0;color:#fff;">Available in November 2016</h2>-->
            <p align="center"><a href="{{url('register')}}" class="btn btn-default btn-md ">Sign Up</a></p>
        </div>
    </section>
    <section class="padded">
        <div class="container">

            <ul class="payroll_list_paragraph" style="margin-left:30px;">
                <li>
                    Are you struggling with expensive or complicated payroll software?
                    <br>
                    Are you still preparing your payslips manually?
                </li>
                <li> &nbsp; <!--We are launching the first Free Payroll platform in Singapore.--></li>
                <li>
                    <div class="col-xs-12">
                        <!--
                        Zenintranet Payroll module will provide a comprehensive payroll solution <br>
                        including Payslips, IRAS and CPF files.
                        -->
                        <p>Zenintranet Payroll module provides a comprehensive payroll solution.</p>

                        <p style="font-size:21px;">Available for Singapore only (for now)</p>
                        <p style="font-size:21px;">Not in Singapore? You can access all the other features!</p>
                        <br>
                    </div>
                    <div class=" col-xs-12">
                        <ul class="check-mark" style="display:block;margin:0 auto;width:150px;">
                            <li style="text-align:left;">Payslips</li>
                            <li style="text-align:left;">IRAS forms</li>
                            <li style="text-align:left;">CPF files</li>
                        </ul>
                    </div>
                    <div class="col-xs-12" style="margin-top:50px;">
                        <span style="color:#ff6600;font-weight: bold">
                        Zenintranet is totally free to use:
                        <br>
                        unlimited employees, unlimited payslips and free forever.
                        </span>
                    </div>
                </li>
                <li> &nbsp; <!--Free Payroll Singapore will be available in November 2016.--></li>
                <!--<li>Join our mailing list and we will inform you as soon as this feature is available.</li>-->
            </ul>
            <!--
            <br>
            <form action="{{url('email_signup')}}" method="POST" id="sendinblue_form">
                {!! csrf_field() !!}
                <div class="form-group col-sm-12 col-md-3 col-md-offset-1">
                    <label >Email address</label>
                    <input type="email" name="email" value="" id="blue_email" class="form-control" required placeholder="sample@gmail.com" />
                </div>
                <div class="form-group col-sm-12 col-md-3">
                    <label >First Name</label>
                    <input type="text" name="first_name" value="" id="blue_first_name" class="form-control" required placeholder="" />
                </div>
                <div class="form-group col-sm-12 col-md-3">
                    <label >Last Name</label>
                    <input type="text" name="last_name" value="" id="blue_last_name" class="form-control" required placeholder="" />
                </div>
                <div class="form-group col-sm-12 col-md-1">
                    <label >&nbsp;</label>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
            -->
        </div>
    </section>
    <section class="padded" style="border-top:1px solid #f5f5f5;border-bottom:1px solid #f5f5f5;">
        <div class="container">
            <p style="text-align: center;">
                <!--Free Payroll Singapore will be a new feature of our Zenintranet platform-->
                Free Payroll (Singapore only for now) is a new feature of our Zenintranet platform, a Business Toolbox of <br>free online tools to help small businesses manage and grow their businesses.
            </p>
            <p style="text-align: center;">Create your Zenintranet account and access all our other free features right away: <br>CRM, Leave Management, eTraining, eClaims and more!</p>

            <p>&nbsp;</p>
            <p align="center"><a href="{{url('register')}}" class="btn btn-primary btn-lg ">Register Now</a></p>
        </div>
    </section>


</div><!--content-->
<div id="footer"><!--footer-->
    <section class="container-fluid text-white padded" style="background:#f26521;">
        <div class="container text-white">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="col-md-3">
                    <h3>Using Zenintra.net</h3>
                    <ul>
                        <li><a href="{{url('product')}}">Features</a></li>
                        <li><a href="{{url('pricing')}}">Pricing</a></li>
                        <li><a href="{{url('/faq')}}">FAQ</a></li>
                        <!--<li><a href="#" data-toggle="modal" data-target="#groove_support" >Support</a></li>-->
                        <li><a href="http://support.zenintra.net" target="_blank">Support</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h3>Company</h3>
                    <ul>
                        <li><a href="{{ url('about-us') }}">About us</a></li>
                        <li><a href="javascript:;" data-toggle="modal" data-target="#contactModal">Contact us</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h3>Legal</h3>
                    <ul>
                        <li><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
                        <li><a href="{{ url('term-of-use') }}">Terms of Use</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <p>
                        <a href="{{ page_finalhome_trans('finalhome.Facebook-url',$lang) }}"><img src="{{url('/')}}/assets/public_assets/images/icon-fb.png" alt="Facebook" /></a>
                        <a href="{{ page_finalhome_trans('finalhome.Twitter-url',$lang) }}"><img src="{{url('/')}}/assets/public_assets/images/icon-twitter.png" alt="Twitter" /></a>
                        <a href="{{ page_finalhome_trans('finalhome.Linkedin-url',$lang) }}"><img src="{{url('/')}}/assets/public_assets/images/icon-linked.png" alt="Linked" /></a>
                    </p>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </section>
    <section class="container-fluid text-white" style="background:#000;">
        <div class="container">
            <p align="center">&copy; Copyright 2016 <span style="color:#f26521;">Zenintra.net</span></p>
        </div>
    </section>
</div><!--footer-->




<div id="formPop" class="modal fade" role="dialog"><!-- Modal -->
    <div class="modal-dialog">
        <div class="modal-content"><!-- Modal content-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Get notified when we open</h4>
            </div>
            <div class="modal-body">
                <!-- Begin MailChimp Signup Form -->
                <link href="//cdn-images.mailchimp.com/embedcode/classic-081711.css" rel="stylesheet" type="text/css">
                <style type="text/css">
                    #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
                </style>
                <div id="mc_embed_signup">
                    <form action="//ilathys.us12.list-manage.com/subscribe/post?u=9590bc219f3343472332769c3&amp;id=3d0d8217b0" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                        <div id="mc_embed_signup_scroll">
                            <div class="mc-field-group">
                                <label for="mce-EMAIL">Email Address <span class="asterisk">*</span>
                                </label>
                                <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
                            </div>
                            <div id="mce-responses" class="clear">
                                <div class="response" id="mce-error-response" style="display:none"></div>
                                <div class="response" id="mce-success-response" style="display:none"></div>
                            </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_9590bc219f3343472332769c3_3d0d8217b0" tabindex="-1" value=""></div>
                            <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn button btn-default"></div>
                        </div>
                    </form>
                </div>
                <!--End mc_embed_signup-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- Modal content-->
    </div>
</div><!-- Modal -->


<script>
$(document).ready(function(){

    $('#subscribe_now').click(function(){
        $('#formPop').modal('show');
    })

    $('#sendinblue_form').submit(function(e){
        var email = $('#blue_email').val();
        var first_name = $('#blue_first_name').val();
        var last_name = $('#blue_last_name').val();

        var send = true;
        if(email == ''){
            $('#blue_email').parent().addClass('has-error');
            send = false;
        }
        if(first_name == ''){
            $('#blue_first_name').parent().addClass('has-error');
            send = false;
        }
        if(last_name == ''){
            $('#blue_last_name').parent().addClass('has-error');
            send = false;
        }

        if(send == false){
            e.preventDefault();
        }


    })


})
</script>







@include('public_support')
@include('public_contact')
@include('get_access_now')

<script>
    $(document).ready(function(){
        $('#create_account_btn').on('click',function(){
            $('#get_access_now_popup').modal('show');
            // window.location.href = "{{url('register')}}";
        })
    })
</script>


</body>
</html>
