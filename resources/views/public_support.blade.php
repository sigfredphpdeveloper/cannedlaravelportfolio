
<style>
fieldset section.form-group label{
    display: inline-block;
    max-width: 100%;
    margin-bottom: 5px;
    font-weight: 700;
    font-size: 14px;
}
.btn-special{
    background: -webkit-linear-gradient(top, #F2611F, #F2611F);
    padding:6px 12px !important;
    margin: 10px 0 0 5px;
    font: 300 15px/29px 'Open Sans',Helvetica,Arial,sans-serif !important;
    height:auto !important;
}
</style>

<!-- Add Groove Support Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="groove_support">
    <div class="modal-dialog">
        <div class="modal-content">
            <div role="content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"> Submit a request </h4>
                </div>

                <div class="modal-body">
                    <div class="message-field"></div>
                    <button type="button" class="btn btn-default dismiss-support" data-dismiss="modal" style="display: none;">OK</button>
                    <form id="support-email" action="{{ url('/support-form') }}" method="POST" class="smart-form" enctype="multipart/form-data" accept-charset="UTF-8">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <input type="hidden" name="base_url" id="base_url" value="{{ url('/') }}">
                        <fieldset>
                            <section class="form-group">
                                <label for="name">Name</label>
                                <input required type="text" name="name" class="form-control" id="" />
                            </section>
                            <section class="form-group">
                                <label for="email"> Email </label>
                                <input required type="email" name="email" class="form-control" id="" />
                            </section>
                            <section class="form-group">
                                <label for="request_type">Request Type</label>
                                    <select name="request_type" class="form-control">
                                        <option value="my account">My Account</option>
                                        <option value="technical">Technical</option>
                                        <option value="other">Other</option>
                                    </select>                            </section>
                            <section class="form-group">
                                <label for="title"> Title </label>
                                <input required type="text" name="title" class="form-control" id="" />
                            </section>
                            <section class="form-group">
                                <label for="title"> Include File </label>
                                <input type="file" name="file" class="" placeholder="Include some files" />
                            </section>
                            <section class="form-group">
                                <label for="message">Message</label>
                                <textarea required name="message" class="form-control" rows="4"></textarea>
                            </section>
                        </fieldset>
                        <footer>
                            <input type="submit" class="btn btn-warning btn-special" alt="" value="Submit" />
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Groove support Modal -->
