<!doctype html>
<?php
function page_finalhome_trans($key,$lang = null){
    if(!$lang){
        $lang = 'en';
    }
    return trans($key,[],null,$lang);
}
?>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    @include('meta')

    @include('frontend.includes.favicon')


    <link rel="stylesheet" href="{{url('/')}}/assets/public_assets/css/bootstrap/bootstrap.css">
    <link href="{{url('/')}}/assets/public_assets/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{url('/')}}/assets/public_assets/css/jquery.mmenu.all.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>

    <script type="text/javascript" src="{{url('/')}}/assets/public_assets/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/assets/public_assets/js/jquery.mmenu.min.all.js"></script>
    <script type="text/javascript" src="{{url('/')}}/assets/public_assets/js/script.js"></script>
</head>

<body style="padding-top:79px;">

@include('frontend.includes.google_tag_manager')

@include('page_head')



<div id="content"><!--content-->
    <section class="container-fluid custom-bg1">
        <div class="container">
            <!--<h1 align="center" style="font-size:48px;margin-bottom:0;color:#fff;">Boost your Business</h1>-->
            <h1 align="center" style="font-size:48px;margin-bottom:0;color:#fff;">Your HR Toolbox</h1>
            <h2 align="center" style="font-size:34px;margin-top:0;color:#fff;">
                Human Resources Made Easy
            </h2>
            <p align="center"><a href="{{url('register')}}" class="btn btn-default btn-md " style="padding:10px 10px;">Create Free Account</a></p>
        </div>
    </section>

    <section class="padded">
        <div class="container">
            <p style="text-align: center">Zenintranet will help you save time and improve productivity.</p>
            <!--
            &#8226; Admin & HR &nbsp; &#8226; CRM &nbsp; &#8226; Tasks Management
            -->
            <br>
            <br>
            <br>
            <h2 style="color:#f26521;margin-top:30px;text-align: center">Why is Zenintranet different?</h2>

            <ul class="check-mark" style="max-width:640px;margin:0 auto;padding-left: 60px;">
                <li>One login per user for all tools.</li>
                <li>Easily add and remove employees.</li>
                <li>Activate only the tools you need, whenever you want.</li>
                <li>Extensive granularity by team and role.</li>
                <li>Very easy to use, even for admin users.</li>
                <li>Fully secured on Amazon Web Services.</li>
                <li>Export your data anytime, we don’t lock you in.</li>
                <li>We keep adding tools.</li>
                <li>Affordable.</li>
                <li>Great support.</li>
                <li>We listen to your feedback.</li>
            </ul>

            <!--
            <ul class="check-mark" style="width: 539px;margin: 0 auto;display: block;">
                <li style="float:left;margin-right: 50px;">Admin & HR</li>
                <li style="float:left;margin-right: 50px;">CRM</li>
                <li style="float:left;margin-right: 50px;">Tasks Management</li>
            </ul>
            -->
        </div>
    </section>

    <section class="padded">
        <div class="container">
            <div class="col-md-4 col-xs-12 pull-right"><img src="{{url('/')}}/assets/public_assets/images/service-icon1.png" alt="Collaborative tool" class="img-responsive" /></div>
            <div class="col-md-8 col-xs-12">
                <h2 style="color:#f26521;margin-top:30px;">HR made easy</h2>
                <p>
                    Save time for yourself and your employees! Our suite of admin & HR tools include:
                    <ul class="check-mark" style="margin-left:30px;">
                        <li>Company directory and emergency contacts.</li>
                        <li>Leave management and tracking.</li>
                        <li>Learning management system.</li>
                        <li>Payroll (SG). </li>
                        <li>Announcements and recognitions.  </li>
                        <li>Expenses claims, approval and tracking. </li>
                        <li>Document sharing.</li>

                        <!--
                        <li>Leave management and other HR tools </li>
                        <li>CRM</li>
                        <li>Internal trainings</li>
                        <li>Project management (coming soon)</li>
                        -->
                    </ul>
                </p>
            </div>
        </div>
    </section>
    <section class="padded" style="border-top:1px solid #f5f5f5;border-bottom:1px solid #f5f5f5;">
        <div class="container">
            <div class="col-md-4"><img src="{{url('/')}}/assets/public_assets/images/service-icon2.png" alt="Leave Management" class="img-responsive" /></div>
            <div class="col-md-8">
                <h2 style="color:#f26521;margin-top:60px;">Leave Management</h2>
                <p>What are your currently using for leave requests, to approve them and track outstanding days?  Zenintranet will make your life easier and your whole team will love you for it!</p>
            </div>
        </div>
    </section>
    <section class="padded">
        <div class="container">
            <div class="col-md-4 col-xs-12 pull-right"><img src="{{url('/')}}/assets/public_assets/images/service-icon3.png" alt="Easy to use" class="img-responsive" /></div>
            <div class="col-md-8 col-xs-12">
                <h2 style="color:#f26521;margin-top:60px;">Learning Management System</h2>
                <p>Easily create training program for your employees, with text, images, audio and video. Track who has completed which training program.</p>
            </div>
        </div>
    </section>
    <section class="container-fluid text-white padded" style="background:#f26521;">
        <div class="container">
            <div class="col-md-12 text-center">
                <h2  style="font-size:26px;">Start using Zenintra for free today
                    <form style="display:inline;">
                        <input id="create_account_btn" class="submit" type="button" value="Create Account" style="height:auto;" />
                    </form>
                </h2>
            </div>
        </div>
    </section>
    <section class="padded">

<style>
@media (max-width: 991px){
.hide-me{display:none;}
.show-me{display:inline !important; }
}
</style>


        <div class="container hide-me">
            <h2 class="" align="center" style="color:#f26521;">One login for all features</h2>
            <p class="" align="center">With only one password each of your employees will be able to access the tools and information they are allowed to.</p>
            <p class=""><img src="{{url('/')}}/assets/public_assets/images/features-icon.png" class="img-responsive center-block" /></p>
        </div>
        <div class="container show-me" style="display:none">
            <p class=""><img src="{{url('/')}}/assets/public_assets/images/features-icon.png" class="img-responsive center-block" /></p>
            <h2 class="" align="center" style="color:#f26521;">One login for all features</h2>
            <p class="" align="center">With only one password each of your employees will be able to access the tools and information they are allowed to.</p>
        </div>
    </section>
    <section class="padded">
        <div class="container">
            <p align="center"><a href="{{url('register')}}" class="btn btn-primary btn-lg ">Sign Up</a></p>
        </div>
    </section>
    <section class="padded">
        <div class="container">
            <img style="width:100%" src="{{url('/')}}/img/press_logo.png">
        </div>
    </section>
</div><!--content-->
<div id="footer"><!--footer-->
    <section class="container-fluid text-white padded" style="background:#f26521;">
        <div class="container text-white">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="col-md-3">
                    <h3>Using Zenintra.net</h3>
                    <ul>
                        <li><a href="{{url('product')}}">Features</a></li>
                        <li><a href="{{url('pricing')}}">Pricing</a></li>
                        <li><a href="{{url('/faq')}}">FAQ</a></li>
                        <!--<li><a href="#" data-toggle="modal" data-target="#groove_support" >Support</a></li>-->
                        <li><a href="http://support.zenintra.net" target="_blank">Support</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h3>Company</h3>
                    <ul>
                        <li><a href="{{ url('about-us') }}">About us</a></li>
                        <li><a href="javascript:;" data-toggle="modal" data-target="#contactModal">Contact us</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h3>Legal</h3>
                    <ul>
                        <li><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
                        <li><a href="{{ url('term-of-use') }}">Terms of Use</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <p>
                        <a href="{{ page_finalhome_trans('finalhome.Facebook-url',$lang) }}"><img src="{{url('/')}}/assets/public_assets/images/icon-fb.png" alt="Facebook" /></a>
                        <a href="{{ page_finalhome_trans('finalhome.Twitter-url',$lang) }}"><img src="{{url('/')}}/assets/public_assets/images/icon-twitter.png" alt="Twitter" /></a>
                        <a href="{{ page_finalhome_trans('finalhome.Linkedin-url',$lang) }}"><img src="{{url('/')}}/assets/public_assets/images/icon-linked.png" alt="Linked" /></a>
                    </p>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </section>
    <section class="container-fluid text-white" style="background:#000;">
        <div class="container">
            <p align="center">&copy; Copyright 2016 <span style="color:#f26521;">Zenintra.net</span></p>
        </div>
    </section>
</div><!--footer-->

@include('public_support')
@include('public_contact')
@include('get_access_now')

<script type="text/javascript" src="{{url('/')}}/assets/public_assets/js/bootstrap/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $('#create_account_btn').on('click',function(){
        $('#get_access_now_popup').modal('show');
        // window.location.href = "{{url('register')}}";
    })


})
</script>


</body>
</html>
