
<style>
    .modal-body div.form-group label{
        display: inline-block;
        max-width: 100%;
        margin-bottom: 5px;
        font-weight: 700;
        font-size: 14px;
    }
    .btn-special{
        background: -webkit-linear-gradient(top, #F2611F, #F2611F);
        padding:6px 12px !important;
        margin: 10px 0 0 5px;
        font: 300 15px/29px 'Open Sans',Helvetica,Arial,sans-serif !important;
        height:auto !important;
    }
</style>

<!-- Modal -->
<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="contactModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="contactModalLabel">Contact Us</h4>
            </div>
            <form action="javascript:;" method="POST" id="contact_us_form">
                {!! csrf_field() !!}

                <div class="modal-body">

                    <div id="contact_us_error_msg" class="alert alert-danger" role="alert" style="display:none">
                      <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                      <span class="sr-only">Error:</span>
                      Enter a valid email address
                    </div>
                    <div id="contact_us_success_msg" class="alert alert-success" role="alert" style="display:none" >
                      <span class="glyphicon glyphicon-send" aria-hidden="true"></span>
                      Thanks for contacting us, we will get back to you as soon as possible.
                    </div>

                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control" placeholder="Name" required>
                    </div>
                    <div class="form-group">
                        <label>Email address</label>
                        <input type="email" name="email" class="form-control" placeholder="Email" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Your Message</label>
                        <textarea name="question" class="form-control" style="min-height:100px;" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="contact_us_btn" class="btn btn-warning btn-special">Send</button>
                </div>
            </form>
        </div>
    </div>
</div>