<!doctype html>
<?php
    function page_finalhome_trans($key,$lang = null){
        if(!$lang){
            $lang = 'en';
        }
        return trans($key,[],null,$lang);
    }
?>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width" />
  @include('meta')
  <meta name="description" content="{{page_finalhome_trans('finalhome.Free intranet and internal communication for startups and small businesses')}}">


<meta property="og:image" content="{{url('/')}}/img/logo.png" />
<meta property="og:description" content="Boost your Business, FREE collaboration tools for smart businesses" />
<meta property="og:url"content="{{url('/')}}" />
<meta property="og:title" content="Zen intranet: free collaboration tools for smart businesses" />

<!-- #FAVICONS -->
    @include('frontend.includes.favicon')
  <!--[if lt IE 9]>
  <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/font-awesome.min.css') }}">

  <link rel="stylesheet" href="//bxslider.com/lib/jquery.bxslider.css" type="text/css" />

  <link rel="stylesheet" href="{{ asset('css/custom.css') }}" type="text/css" rel="stylesheet" />

  <link rel="stylesheet" href="{{ asset('css/jquery.mmenu.all.css') }}">

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src= "{{ asset('js/jquery.mmenu.min.all.js') }}"></script>
  <script src= "{{ asset('js/jquery.bxslider.min.js') }}"></script>
  <script src= "{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>

<style>
@media (max-width: 1260px) {
    .when-mobile{
        margin-top:200px;
    }
}
</style>
</head>

<body>
    @include('frontend.includes.google_tag_manager')


    <div class="mobile-menu-container hidden-md hidden-lg">
                <div id="mobile-menu">
                    <ul>
                    	<li><a href="{{url('/product')}}">Features</a></li>
                        <li><a href="{{url('/pricing')}}">Pricing</a></li>
                        <li><a href="{{url('/faq')}}">FAQ</a></li>
                        <!--<li><a href="#" data-toggle="modal" data-target="#groove_support">Support</a></li>-->
                        <li><a href="http://support.zenintra.net">Support</a></li>
                        <li><a href="{{url('/login')}}">Login</a></li>
                    </ul>
                </div>
            </div>
            <div id="header" class="container-fluid"><!--header-->
            	<div class="container container1300">
                    <div id="logo" class="col-md-4 col-xs-10">
                        <a href="{{url('/')}}"><img src="<?php echo url('/');?>/img/logo.png" class="img-responsive" alt="Zenintra" /></a>
                    </div>
                    <div id="menu" class="col-md-8 visible-md visible-lg">
                        <ul>
                            <li><a href="{{url('/product')}}">Features</a></li>
                            <li><a href="{{url('/pricing')}}">Pricing</a></li>
                            <li><a href="{{url('/faq')}}">FAQ</a></li>
                            <li><a href="http://support.zenintra.net">Support</a></li>
                            <!--<li><a href="#" data-toggle="modal" data-target="#groove_support">Support</a></li>-->
                            <li><a href="{{url('/login')}}">Login</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-2 hidden-md hidden-lg">
                        <a class="mobile-menu-trigger" style="padding-top:0;font-size: 35px;"><span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span></a>
                    </div>
                </div>
            </div><!--header-->
            <div id="content"><!--content-->
            	<div class="container-fluid custom-bg1">
                	<div class="col-md-5"></div>
                    <div class="col-md-7 when-mobile">
                    	<h1 align="center" id="main-quote" style="font-size:72px;margin-bottom:0;">
                    	<strong>Boost your Business</strong></h1>
                        <h2 align="center" id="sub-quote" style="font-size:48px;">

                        <em><span style="color:#F2611F;font-weight:bold;">FREE</span> collaboration tools for smart businesses

                        </em></h2>
                        <p>&nbsp;</p>
                        <p align="center"><a href="{{ url('/register') }}"
                         class="btn btn-default btn-lg get-access"
                         >Sign up</a></p>
                    </div>
                </div>
            </div><!--content-->

   @include('page_footer')

<div id="formPop" class="modal fade" role="dialog"><!-- formPopModal -->
    <div class="modal-dialog">
        <div class="modal-content"><!-- Modal content-->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">{{page_finalhome_trans('finalhome.Get notified when we open',$lang)}}</h4>
        </div>
        <div class="modal-body">
            <!-- Begin MailChimp Signup Form -->
            <link href="//cdn-images.mailchimp.com/embedcode/classic-081711.css" rel="stylesheet" type="text/css">
            <style type="text/css">
            #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
            /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
            We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
            </style>
            <div id="mc_embed_signup">
            <form onSubmit=window.location="/zenintra/public/thankyou" action="//ilathys.us12.list-manage.com/subscribe/post?u=9590bc219f3343472332769c3&amp;id={{page_finalhome_trans('finalhome.mailchimplistid',$lang)}}" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
            <div id="mc_embed_signup_scroll">
            <div class="mc-field-group">
            <label for="mce-EMAIL">{{page_finalhome_trans('finalhome.Email Address',$lang)}} <span class="asterisk">*</span>
            </label>
            <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
            </div>
            <div id="mce-responses" class="clear">
            <div class="response" id="mce-error-response" style="display:none"></div>
            <div class="response" id="mce-success-response" style="display:none"></div>
            </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_9590bc219f3343472332769c3_3d0d8217b0" tabindex="-1" value=""></div>
            <div class="clear"><input type="submit" value="{{page_finalhome_trans('finalhome.Subscribe',$lang)}}" name="subscribe" id="mc-embedded-subscribe" class="btn btn-primary"></div>
            </div>
            </form>
            </div>
            <!--End mc_embed_signup-->
        </div>
        <div class="modal-footer">
        	<button type="button" class="btn btn-default" data-dismiss="modal">{{page_finalhome_trans('finalhome.Close',$lang)}}</button>
        </div>
        </div><!-- Modal content-->
    </div>
</div><!-- formPopModal -->

<!-- Modal -->
<div class="modal fade" id="contact-us-modal" tabindex="-1" role="dialog" aria-labelledby="contact-us-modal-label">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="contact-us-modal-label">{{page_finalhome_trans('finalhome.Contact Us',$lang)}}</h4>
      </div>
      <div class="modal-body">
        <form id="contact-us-form">
            <div class="form-group">
                <label for="contact_name">{{page_finalhome_trans('finalhome.Name',$lang)}}</label>
                <input type="text" class="form-control" id="contact_name" name="contact_name" placeholder="{{page_finalhome_trans('finalhome.John Smith',$lang)}}">
            </div>
            <div class="form-group">
                <label for="contact_email">{{page_finalhome_trans('finalhome.Email',$lang)}}</label>
                <input type="email" class="form-control" id="contact_email" name="contact_email" placeholder="{{page_finalhome_trans('finalhome.sample-email',$lang)}}">
            </div>
            <div class="form-group">
                <label for="contact_message">{{page_finalhome_trans('finalhome.Message',$lang)}}</label>
                <textarea class="form-control" id="contact_message" name="contact_message" rows="3" placeholder="{{page_finalhome_trans('finalhome.What can we do for you?',$lang)}}"></textarea>
            </div>
        </form>
        <div id="contact-return" class="bg-success" style="display:none;">
            <p id="contact-return-message"></p>
        </div>
      </div>
      <div class="modal-footer">
        <button for="contact-us-form" type="submit" class="btn btn-default" onclick="submit_contact_us()">{{page_finalhome_trans('finalhome.Submit',$lang)}}</button>
      </div>
    </div>
  </div>
</div>

<div id="get_access_now_popup" class="modal fade" role="dialog"><!-- formPopModal -->
    <div class="modal-dialog">
        <div class="modal-content"><!-- Modal content-->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Get Access Now</h4>
        </div>



        <div class="modal-body">

        <form id="access_form">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
             <div class="form-group">
                <label for="exampleInputPassword1">Name</label>
                <input type="text" class="form-control" name="name" id="exampleInputPassword1" placeholder="Name">
              </div>

              <div class="form-group">
                <label for="exampleInputEmail1">Email address *</label>
                <input type="email" class="form-control" name="email" required id="exampleInputEmail1" placeholder="Email">
              </div>

              <button type="submit" class="btn btn-default" id="access_form_submit">Submit</button>

              <div id="msg_holder" class="alert" style="display:none;">

              </div>

            </form>
        </div>




        </div><!-- Modal content-->
    </div>
</div><!-- formPopModal -->


<script type="text/javascript">
var sent = false;
function submit_contact_us(){

    if(!sent){
        console.log("sending");
        var data = $("#contact-us-form").serializeArray();
        data.push({name:'lang',value:"{{$lang}}"});
        $.ajax({
            url:'{{route("contact_us")}}',
            method:"POST",
            data:data
        })
        .done(function(){
            sent = true;
            $("#contact-us-form").hide();
            $("#contact-return").show();
            $("#contact-return").addClass("bg-success");
            $("#contact-return").removeClass("bg-danger");
            $("#contact-return-message").text("{!! page_finalhome_trans('finalhome.Your message was successfully sent! We will get back to you soon\.',$lang) !!}");
        })
        .fail(function(err){
            var data = err.responseJSON;
            $("#contact-return").show();
            $("#contact-return").removeClass("bg-success");
            $("#contact-return").addClass("bg-danger");
            $("#contact-return-message").text(data.message);
        });
    }
}
</script>

<script type="text/javascript">
	//Floating Menu
	$(document).scroll(function() {
	  var y = $(this).scrollTop();
	  if (y > 140) {
		$('#floating-header').fadeIn();
	  } else {
		$('#floating-header').fadeOut();
	  }
	});

	$('.testi-slide').bxSlider({
		mode: 'horizontal',
		auto: true,
		pager: false,
		controls: true,
		pause: 7000,
		speed: 1000
	});

	// MOBILE MENU
	$('.mobile-menu-container').mmenu({
		classes: 'mm-light',
		counters: true,
		offCanvas: {
			position  : 'left',
			zposition :	'front',
		}
	});

	$('a.mobile-menu-trigger').click(function() {
		$('.mobile-menu-container').trigger('open.mm');
	});

	$(window).resize(function() {
		$('.mobile-menu-container').trigger("close.mm");
	});

    $(document).ready(function($){
        $('.lang-select').on('change', function(){
            var lang =  $(this).val();
            if( lang == 'fr' )
            {
                window.location = '{{ url("fr/finalhome") }}';
            }
            else
            {
                window.location = '{{ url("finalhome") }}';
            }
        });

        $('#access_form').submit(function(e){

            $('#access_form_submit').html('Loading Please Wait...');

            e.preventDefault();
            var dt = $(this).serializeArray();

            $.ajax({
                url : '{{url('/get_access_now_subscribe')}}',
                type : 'POST',
                data:dt,
                success : function(code){

                    $('#access_form_submit').html('Submit');

                    $('#msg_holder').fadeIn();

                    if(code=='success'){

                        setTimeout(function() {
                        $('#msg_holder').hide();
                        $('#get_access_now_popup').modal('hide');
                        }, 2000);

                        $('#msg_holder').removeClass('alert-danger').addClass('alert-success').html('Successfully Subscribed!');
                    }else{
                        $('#msg_holder').removeClass('alert-success').addClass('alert-danger').html('Error Occured!');
                    }
                }
            });

        });




    });

    $(window).scroll(function(){
        var dh = $(document).height();
        var sh = $(this).scrollTop();

        {{--if(sh>20){--}}
            {{--$('#header').attr('style', 'background: #F2611F !important');--}}
            {{--$('#logo a img').attr('src','{{url('img/zenitra-logo-white.png')}}');--}}
            {{--$('#menu ul li a').css('color','#fff');--}}
        {{--}else{--}}
            {{--$('#header').attr('style', 'background: #fff !important');--}}
            {{--$('#logo a img').attr('src','{{url('img/logo.png')}}');--}}
            {{--$('#menu ul li a').css('color','#000');--}}
        {{--}--}}
    });

</script>
</body>
</html>
