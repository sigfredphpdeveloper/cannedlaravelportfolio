<!doctype html>
<?php
function page_finalhome_trans($key,$lang = null){
    if(!$lang){
        $lang = 'en';
    }
    return trans($key,[],null,$lang);
}
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>


    @include('meta')

@include('frontend.includes.favicon')




    <link rel="stylesheet" href="{{url('/')}}/assets/public_assets/css/bootstrap/bootstrap.css">
    <link href="{{url('/')}}/assets/public_assets/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{url('/')}}/assets/public_assets/css/jquery.mmenu.all.css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>

    <script type="text/javascript" src="{{url('/')}}/assets/public_assets/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/assets/public_assets/js/jquery.mmenu.min.all.js"></script>
    <script type="text/javascript" src="{{url('/')}}/assets/public_assets/js/script.js"></script>
</head>

<body style="padding-top:79px;">

@include('frontend.includes.google_tag_manager')

@include('page_head')



<div id="content"><!--content-->
    <section class="container-fluid custom-bg2">
        <div class="container">
            <h1 align="center" style="font-size:48px;margin-bottom:0;color:#fff;">Features</h1>
        </div>
    </section>
    <section class="padded text-white" style="background:#f26521;">
        <div class="container">
            <div class="col-md-6 col-xs-12 pull-right"><img src="{{url('/')}}/assets/public_assets/images/features-img8.png" alt="Leave Management" class="img-responsive" /></div>
            <div class="col-md-6 col-xs-12">
                <h2 style="margin-top:40px;">Leave Management</h2>
                <ul class="check-mark">
                    <li>Simple leave application process</li>
                    <li>Track outstanding leave days left</li>
                    <li>Customize types of leaves</li>
                </ul>
            </div>
        </div>
    </section>
    <section class="padded">
        <div class="container">
            <div class="col-md-6"><img src="{{url('/')}}/assets/public_assets/images/features-img5.png" alt="Knowledge Sharing" class="img-responsive" /></div>
            <div class="col-md-6">


                <h2 style="color:#f26521;">Learning Management System:</h2>
                <ul class="check-mark">
                    <li>Easily create internal trainings</li>
                    <li>Facilitate onboarding and knowledge sharing</li>
                    <li>Customize access rights and track completion</li>
                </ul>
            </div>
        </div>
    </section>

    <section class="padded text-white" style="background:#f26521;">
        <div class="container">
            <div class="col-md-6 col-xs-12 pull-right"><img src="{{url('/')}}/assets/public_assets/images/features-img6.png" alt="Claims" class="img-responsive" /></div>
            <div class="col-md-6 col-xs-12">
                <h2 style="margin-top:90px;">Claims</h2>
                <ul class="check-mark">
                    <li>Expenses tracking made easy</li>
                    <li>Customize approval process</li>
                    <li>Expenses Reporting</li>
                </ul>
            </div>
        </div>
    </section>
    <section class="padded">
        <div class="container">
            <div class="col-md-6 col-xs-12 "><img src="{{url('/')}}/assets/public_assets/images/features-img2.png" alt="Directory" class="img-responsive" /></div>
            <div class="col-md-6">
                <h2 style="color:#f26521;">Directory</h2>
                <ul class="check-mark">
                    <li>List all your employees contact details</li>
                    <li>Emergency contact details with custom access rights</li>
                </ul>
            </div>
        </div>
    </section>
    
    <section class="padded text-white" style="background:#f26521;">
        <div class="container">
            <div class="col-md-6 col-xs-12 pull-right"><img src="{{url('/')}}/assets/public_assets/images/features-img4.png" alt="Announcements" class="img-responsive" /></div>
            <div class="col-md-6 col-xs-12">
                <h2>Announcements</h2>
                <ul class="check-mark">
                    <li>Improve your internal communication</li>
                    <li>Share the latest news of your business with all employees or specific teams</li>
                </ul>
            </div>
        </div>
    </section>
    
    <section class="padded">
        <div class="container">
            <div class="col-md-6"><img src="{{url('/')}}/assets/public_assets/images/features-img3.png" alt="CRM" class="img-responsive" /></div>
            <div class="col-md-6">
                <h2 style="margin-top:60px;">Employee self service</h2>
                <ul class="check-mark">
                    <li>Easy-to-use interface</li>
                    <li>Access rights as defined by Manager</li>
                    <li>Approval process for leaves and claims</li>
                </ul>
            </div>
        </div>
    </section>

    <section class="padded text-white" style="background:#f26521;">
        <div class="container">
            <div class="col-md-6 col-xs-12 pull-right"><img src="{{url('/')}}/assets/public_assets/images/task_management.png" alt="Directory" class="img-responsive" /></div>
            <div class="col-md-6">
                <h2 style="">Tasks Management</h2>
                <ul class="check-mark" >
                    <li>Create and manage tasks</li>
                    <li>Assign tasks to team members</li>
                    <li>Add checklists and more</li>
                </ul>
            </div>
        </div>
    </section>

    <section class="padded">
        <div class="container">
            <div class="col-md-6"><img src="{{url('/')}}/assets/public_assets/images/features-img1.png" alt="File Sharing" class="img-responsive" /></div>
            <div class="col-md-6">
                <h2 style="color:#f26521;margin-top:60px;">Document Sharing</h2>
                <ul class="check-mark">
                    <li>Upload and share any file</li>
                    <li>Customize access rights</li>
                    <li>1 GB Free Storage Space (Get more with our referral program)</li>
                </ul>
            </div>
        </div>
    </section>

    <section class="padded text-white" style="background:#f26521;">
        <div class="container">
            <div class="col-md-6 pull-right"><img src="{{url('/')}}/assets/public_assets/images/features-img7.png" alt="Granularity in Access Rights" class="img-responsive" /></div>
            <div class="col-md-6">
                <h2 style="margin-top:90px;">Granularity in Access Rights</h2>
                <ul class="check-mark">
                    <li>Decide who can access what in your intranet</li>
                    <li>Access rights can be defined by role and by team</li>
                </ul>
            </div>
        </div>
    </section>
    <section class="container-fluid custom-bg3 text-white">
        <div class="container">
            <h2 align="center" style="color:#f26521;">Payroll (Singapore only)</h2>
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <h2>Project Management</h2>
                <ul class="check-mark">
                    <li>Easily manage simple or complex projects</li>
                    <li>Allocate tasks to team members</li>
                    <li>Multiple views (task, calendar, team member)</li>
                </ul>
            </div>
            <div class="col-md-3"></div>
        </div>
    </section>
    <section class="padded">
        <div class="container">
            <h2 align="center" style="color:#f26521;">The Essentials</h2>
            <p><img src="{{url('/')}}/assets/public_assets/images/essentials.png" alt="The Essentials" class="img-responsive" /></p>
            <p>&nbsp;</p>
            <p align="center"><a href="{{url('register')}}" class="btn btn-primary btn-lg ">Sign Up</a></p>
        </div>
    </section>

</div><!--content-->
<div id="footer"><!--footer-->
    <section class="container-fluid text-white padded" style="background:#f26521;">
        <div class="container text-white">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="col-md-3">
                    <h3>Using Zenintra.net</h3>
                    <ul>
                        <li><a href="{{url('product')}}">Features</a></li>
                        <li><a href="{{url('pricing')}}">Pricing</a></li>
                        <li><a href="{{url('/faq')}}">FAQ</a></li>
                        <!--<li><a href="#" data-toggle="modal" data-target="#groove_support" >Support</a></li>-->
                        <li><a href="http://support.zenintra.net" target="_blank">Support</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h3>Company</h3>
                    <ul>
                        <li><a href="{{ url('about-us') }}">About us</a></li>
                        <li><a href="javascript:;" data-toggle="modal" data-target="#contactModal">Contact us</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h3>Legal</h3>
                    <ul>
                        <li><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
                        <li><a href="{{ url('term-of-use') }}">Terms of Use</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <p>
                        <a href="{{ page_finalhome_trans('finalhome.Facebook-url',$lang) }}"><img src="{{url('/')}}/assets/public_assets/images/icon-fb.png" alt="Facebook" /></a>
                        <a href="{{ page_finalhome_trans('finalhome.Twitter-url',$lang) }}"><img src="{{url('/')}}/assets/public_assets/images/icon-twitter.png" alt="Twitter" /></a>
                        <a href="{{ page_finalhome_trans('finalhome.Linkedin-url',$lang) }}"><img src="{{url('/')}}/assets/public_assets/images/icon-linked.png" alt="Linked" /></a>
                    </p>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </section>
    <section class="container-fluid text-white" style="background:#000;">
        <div class="container">
            <p align="center">&copy; Copyright 2016 <span style="color:#f26521;">Zenintra.net</span></p>
        </div>
    </section>
</div><!--footer-->

<div class="modal fade bs-example-modal-lg" id="yt_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Watch the tour</h4>
            </div>
            <div class="modal-body" style="padding:0;">
                <div id="popupVid">

                </div>
            </div>
        </div>
    </div>
</div>


@include('public_support')
@include('public_contact')
@include('get_access_now')

<script type="text/javascript" src="{{url('/')}}/assets/public_assets/js/bootstrap/bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $('#start_yt_modal').click(function(){

        var iframe = '<iframe width="100%" height="340px" id="yt_frame" src="https://www.youtube.com/embed/o-vcRz0RXL8?rel=0&autoplay=true&showinfo=0&controls=0"></iframe>';

        $('#popupVid').html(iframe);
        $('#yt_modal').modal('show');
    });

    $('#yt_modal').on('hidden.bs.modal', function() {
        $('#popupVid').html('');
    });

    $('#create_account_btn').on('click',function(){
        $('#get_access_now_popup').modal('show');
        // window.location.href = "{{url('register')}}";
    })

    $('.get_access_to_register').on('click', function(){
        $('#get_access_now_popup').modal('show');
    })

})
</script>


</body>
</html>
