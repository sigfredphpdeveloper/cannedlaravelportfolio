<!doctype html>
<?php 
    function page_feature_trans($key,$lang = null){
        if(!$lang){
            $lang = 'en';
        }
        return trans($key,[],null,$lang);
    }

    function page_finalhome_trans($key,$lang = null){
        if(!$lang){
            $lang = 'en';
        }
        return trans($key,[],null,$lang);
    }
?>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width" />
  @include('meta')
  <meta name="description" content="{{page_feature_trans('landingpage.Free intranet and internal communication for startups and small businesses')}}">
<!-- #FAVICONS -->
    @include('frontend.includes.favicon')
  <!--[if lt IE 9]>
  <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/font-awesome.min.css') }}">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  
  <link rel="stylesheet" href="{{ asset('css/custom.css') }}" type="text/css" rel="stylesheet" />
  
  <link rel="stylesheet" href="{{ asset('css/jquery.mmenu.all.css') }}">

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src= "{{ asset('js/jquery.mmenu.min.all.js') }}"></script>
  <script src= "{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
</head>

<body>
    @include('frontend.includes.google_tag_manager')
    <div class="mobile-menu-container hidden-lg">
		<div id="mobile-menu">
			<ul>
                <li><a href="{{ url('features') }}">{{page_feature_trans('feature.Features',$lang)}}</a></li>
                <li><a href="{{ url('pricing') }}">{{page_feature_trans('feature.Pricing',$lang)}}</a></li>
                <li><a href="">{{page_feature_trans('feature.Sign up',$lang)}}</a></li>
                <li><a href="">{{page_feature_trans('feature.Log in',$lang)}}</a></li>
            </ul>
		</div>
	</div>
    
    <div id="floating-header"><!--floating-header-->
    	<div class="container">
        	<div id="logo" class="col-md-3 col-sm-11 col-xs-11"><a href="{{ url('finalhome') }}"><img src="{{ asset('img/zenitra-logo.png') }}" alt="Zenintra" class="img-responsive" /></a></div>
            <div id="menu" class="col-md-9 visible-lg visible-md">
            	<ul>
                	<li><a href="{{page_feature_trans('feature.Blog-url',$lang)}}" target="_blank">{{page_feature_trans('feature.Blog',$lang)}}</a></li>
                    <li><a href="{{ url('pricing') }}">{{page_feature_trans('feature.Pricing',$lang)}}</a></li>
                    <li>
                    <form>
                    	<select class="lang-select">
                            <option value="en" <?php echo ( $lang == 'en') ? 'selected' : ''; ?> >EN</option>
                            <option value="fr" <?php echo ( $lang == 'fr') ? 'selected' : ''; ?> >FR</option>
                        </select>
                    </form>
                    </li>
                </ul>
            </div>
            <div class="col-sm-1 col-xs-1 hidden-lg hidden-md">
                <a class="mobile-menu-trigger"></a>
            </div>
            <div class="clear"></div>
            
        </div>
    </div><!--floating-header-->

	<div id="header" style="background:#ff8521;"><!--header-->
    	<div class="container">
        	<div id="logo" class="col-md-3 col-sm-11 col-xs-11"><a href="{{ url('finalhome') }}"><img src="{{ asset('img/zenitra-logo-white.png') }}" alt="Zenintra" class="img-responsive" /></a></div>
            <div id="menu" class="col-md-9 visible-lg visible-md">
            	<ul>
                	<li><a href="{{page_feature_trans('feature.Blog-url',$lang)}}" target="_blank">{{page_feature_trans('feature.Blog',$lang)}}</a></li>
                    <li><a href="{{ url('pricing') }}">{{page_feature_trans('feature.Pricing',$lang)}}</a></li>
                    <li>
                    <form>
                    	<select class="lang-select">
                            <option value="en" <?php echo ( $lang == 'en') ? 'selected' : ''; ?> >EN</option>
                            <option value="fr" <?php echo ( $lang == 'fr') ? 'selected' : ''; ?> >FR</option>
                        </select>
                    </form>
                    </li>
                </ul>
            </div>
            <div class="col-sm-1 col-xs-1 hidden-lg hidden-md">
                <a class="mobile-menu-trigger"></a>
            </div>
            <div class="clear"></div>
            
        </div>
    </div><!--header-->
    <div id="content"><!--content-->
        <div id="main-content"><!--main-content-->
        	<div class="container-fluid" style="background:#f4f4f4;padding:40px 0;">
            	<div class="container">
                	<h1 align="center" style="color:#555555;">{{page_feature_trans('feature.Feature-title',$lang)}}</h1>
                </div>
            </div>
            <div class="row padded">
            	<div class="container">
                	<div class="col-md-6">
                    	<h2><img src="{{ asset('img/hr-icon.png') }}" alt="HR" class="img-responsive alignleft" />{{page_feature_trans('feature.Communication-title',$lang)}}</h2>
                        <ul>
                        	<li>{{page_feature_trans('feature.Communication-feat1',$lang)}}</li>
                            <li>{{page_feature_trans('feature.Communication-feat2',$lang)}}</li>
                            <li>{{page_feature_trans('feature.Communication-feat3',$lang)}}</li>
                            <li>{{page_feature_trans('feature.Communication-feat4',$lang)}}</li>
                        </ul>
                    </div>
                    <div class="col-md-6"><img src="{{ asset('img/hr-img.png') }}" alt="HR" class="img-responsive center-block" /></div>
                </div>
            </div>
            <div class="row padded">
            	<div class="container">
                	<div class="col-md-6">
                    	<h2><img style="margin-top:-20px;" src="{{ asset('img/icon_FS-c.png') }}" width="115" alt="File Sharing" class="img-responsive alignleft" />{{page_feature_trans('feature.FSharing-title',$lang)}}</h2>
                        <ul>
                        	<li>{{page_feature_trans('feature.FSharing-feat1',$lang)}}</li>
                            <li>{{page_feature_trans('feature.FSharing-feat2',$lang)}}</li>
                            <li>{{page_feature_trans('feature.FSharing-feat3',$lang)}}</li>
                            <li>{{page_feature_trans('feature.FSharing-feat4',$lang)}}</li>
                        </ul>
                    </div>
                    <div class="col-md-6"><img src="{{ asset('img/fs-img.png') }}" alt="File Sharing" class="img-responsive center-block" /></div>
                </div>
            </div>
            <div class="row padded">
            	<div class="container">
                	<div class="col-md-6">
                    	<h2><img style="margin-top:-20px;" src="{{ asset('img/icon_Essentials-b.png') }}" width="115" alt="CRM" class="img-responsive alignleft" />{{page_feature_trans('feature.TEssentials-title',$lang)}}<br/>&nbsp;</h2>
                        <ul>
                        	<li>{{page_feature_trans('feature.TEssentials-feat1',$lang)}}</li>
                            <li>{{page_feature_trans('feature.TEssentials-feat2',$lang)}}</li>
                            <li>{{page_feature_trans('feature.TEssentials-feat3',$lang)}}</li>
                            <li>{{page_feature_trans('feature.TEssentials-feat4',$lang)}}</li>
                        </ul>
                    </div>
                    <div class="col-md-6"><img src="{{ asset('img/crm-img.png') }}" alt="CRM" class="img-responsive center-block" /></div>
                </div>
            </div>
            <div class="row padded">
            	<div class="container">
                	<div class="col-md-6">
                    	<h2><img style="margin-top:-20px;" src="{{ asset('img/icon_ComingSoon.png') }}" width="115" alt="Coming Soon" class="img-responsive alignleft" />{{page_feature_trans('feature.CMSoon-title',$lang)}}</h2>
                        <ul>
                        	<li>{{page_feature_trans('feature.CMSoon-feat1',$lang)}}</li>
                            <li>{{page_feature_trans('feature.CMSoon-feat2',$lang)}}</li>
                            <li>{{page_feature_trans('feature.CMSoon-feat3',$lang)}}</li>
                            <li>{{page_feature_trans('feature.CMSoon-feat4',$lang)}}</li>
                        </ul>
                    </div>
                    <div class="col-md-6"><img src="{{ asset('img/pm-img.png') }}" alt="Project Management" class="img-responsive center-block" /></div>
                </div>
            </div>
        </div><!--main-content-->
    </div><!--content-->

    @include('page_footer')

 <!-- Modal -->
<div class="modal fade" id="contact-us-modal" tabindex="-1" role="dialog" aria-labelledby="contact-us-modal-label">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="contact-us-modal-label">{{page_feature_trans('feature.Contact Us',$lang)}}</h4>
      </div>
      <div class="modal-body">
        <form id="contact-us-form">
            <div class="form-group">
                <label for="contact_name">{{page_feature_trans('feature.Name',$lang)}}</label>
                <input type="text" class="form-control" id="contact_name" name="contact_name" placeholder="{{page_feature_trans('landingpage.John Smith',$lang)}}">
            </div>
            <div class="form-group">
                <label for="contact_email">{{page_feature_trans('feature.Email',$lang)}}</label>
                <input type="email" class="form-control" id="contact_email" name="contact_email" placeholder="{{page_feature_trans('landingpage.sample-email',$lang)}}">
            </div>
            <div class="form-group">
                <label for="contact_message">{{page_feature_trans('feature.Message',$lang)}}</label>
                <textarea class="form-control" id="contact_message" name="contact_message" rows="3" placeholder="{{page_feature_trans('landingpage.What can we do for you?',$lang)}}"></textarea>
            </div>
        </form>
        <div id="contact-return" class="bg-success" style="display:none;">
            <p id="contact-return-message"></p>
        </div>
      </div>
      <div class="modal-footer">
        <button for="contact-us-form" type="submit" class="btn btn-default" onclick="submit_contact_us()">{{page_feature_trans('landingpage.Submit',$lang)}}</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
var sent = false;
function submit_contact_us(){
    
    if(!sent){
        console.log("sending");
        var data = $("#contact-us-form").serializeArray();
        data.push({name:'lang',value:"{{$lang}}"});
        $.ajax({
            url:'{{route("contact_us")}}',
            method:"POST",
            data:data
        })
        .done(function(){
            sent = true;
            $("#contact-us-form").hide();
            $("#contact-return").show();
            $("#contact-return").addClass("bg-success");
            $("#contact-return").removeClass("bg-danger");
            $("#contact-return-message").text("{!! page_feature_trans('landingpage.Your message was successfully sent! We will get back to you soon\.',$lang) !!}");
        })
        .fail(function(err){
            var data = err.responseJSON;
            $("#contact-return").show();
            $("#contact-return").removeClass("bg-success");
            $("#contact-return").addClass("bg-danger");
            $("#contact-return-message").text(data.message);
        });
    }
}
</script>
   
<script type="text/javascript">
	//Floating Menu
	$(document).scroll(function() {
	  var y = $(this).scrollTop();
	  if (y > 140) {
		$('#floating-header').fadeIn();
	  } else {
		$('#floating-header').fadeOut();
	  }
	});
		
	// MOBILE MENU
	$('.mobile-menu-container').mmenu({
		classes: 'mm-light',
		counters: true,
		offCanvas: {
			position  : 'left',
			zposition :	'front',
		}
	});

	$('a.mobile-menu-trigger').click(function() {
		$('.mobile-menu-container').trigger('open.mm');
	});
	
	$(window).resize(function() {
		$('.mobile-menu-container').trigger("close.mm");
	});

    $(document).ready(function($){
        $('.lang-select').on('change', function(){
            var lang =  $(this).val();
            if( lang == 'fr' )
            {
                window.location = '{{ url("fr/features") }}';
            }
            else
            {
                window.location = '{{ url("features") }}';
            }
        });
    });

</script>
</body>
</html>