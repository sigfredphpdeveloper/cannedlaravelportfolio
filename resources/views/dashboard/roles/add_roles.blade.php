@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-sm-12 col-md-12">
        <h1>{{trans("add_roles.Add Role")}}</h1>
        </div>

        <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>{{trans("add_roles.Warning")}}.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif



        <article class="col-sm-6 col-md-6 col-lg-6">

            <form  id="smart-form-register" class="smart-form client-form" role="form" method="POST" action="{{ url('/roles_add_save') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="company_id" value="{{ $company['id'] }}">


                <fieldset>

                     <section>
                      <label class="label">{{trans("add_roles.Role Name")}}</label>
                        <label class="input">
                        <input type="text" name="role_name" placeholder="{{trans("add_roles.Role Name")}}" value="" />

                        </label>
                    </section>

                    <section>
                    <label class="label">{{trans("add_roles.Role Description")}}</label>
                        <label class="input">
                        <input type="text" name="role_description" placeholder="{{trans("add_roles.Role Description")}}" value="" />

                        </label>
                    </section>

                </fieldset>

                <footer>
                    <button type="submit" class="btn btn-success">
                        {{trans("add_roles.Save")}}
                    </button>
                    <a href="{{ url('/roles') }}" class="btn btn-primary">{{trans("add_roles.Back")}}</a>
                </footer>

            </form>

        </article>
    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->


@endsection