@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-sm-12 col-md-12">
        <a href="{{ url('roles') }}" class="btn btn-primary">{{trans("manage_roles_edit.Back")}}</a>
        <h1>{{trans("manage_roles_edit.Edit Role")}}</h1>
        </div>

        <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>{{trans("manage_roles_edit.Warning.")}}<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <article class="col-sm-6 col-md-6 col-lg-6">

            <form  id="smart-form-register" class="smart-form client-form" role="form" method="POST" action="{{ url('/roles_edit') }}/{{$role['id']}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="company_id" value="{{ $company['id'] }}">

                <fieldset>

                     <section>
                     <label class="label">{{trans("manage_roles_edit.Role Name")}}</label>
                        <label class="input">
                        <input type="text" name="role_name" {{ ($role['role_name'] == 'Administrator')?'disabled':'' }} placeholder="{{trans("manage_roles_edit.Role Name")}}" value="{{ $role['role_name'] }}" />

                        </label>
                    </section>

                    <section>
                    <label class="label">{{trans("manage_roles_edit.Role Description")}}</label>
                        <label class="input">
                        <input type="text" {{ ($role['role_name'] == 'Administrator')?'disabled':'' }} name="role_description" placeholder="{{trans("manage_roles_edit.Role Description")}}" value="{{ $role['role_description'] }}" />

                        </label>
                    </section>

                </fieldset>

                <footer>
                    <button type="submit" class="btn btn-success" {{ ($role['role_name'] == 'Administrator')?'disabled':'' }}>
                        {{trans("manage_roles_edit.Save")}}
                    </button>
                </footer>

                {!! ($role['role_name'] == 'Administrator')?'<p style=color:red;text-align:center>Administrator Role is not allowed to be updated</p>':'' !!}

            </form>

        </article>
    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->


@endsection