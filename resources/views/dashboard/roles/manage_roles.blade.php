@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->

    <div class="row">

        <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        </div>


        @if(Session::has('error'))
        <div class="col-md-12">
            <div class="alert alert-danger">
                <p>{{ Session::get('error') }}</p>
            </div>
        </div>
        @endif

        <div class="col-md-12">

            <a href="{{ url('/admin_settings') }}" class="btn btn-xs btn-default"><i class="fa fa-caret-left"></i> Back To Settings</a>

            <h1>{{trans("manage_roles.Manage Roles")}}</h1>

        </div>
    </div>

    <div class="row">

        <div class="col-md-3">

            <?php if($edit_crt==TRUE):?>
            <a href="{{ url('/roles_add') }}" class="btn btn-primary">{{trans("manage_roles.Add Role")}}</a>
            <?php endif;?>

        </div>

        <div class="col-md-9 pull-right">
            <div class="input-group pull-right">

                <form class="form-inline" method="GET">
                    <div class="form-group">
                        <div class="input-group">
                           <input type="text" name="search" id="search_box" class="form-control"
                                                          value="<?php echo (isset($search))?$search:'';?>" placeholder="{{trans("manage_roles.Enter Keywords")}}">
                            <span class="input-group-btn"> <button class="btn btn-default" id="search_btn" type="submit">{{trans("manage_roles.Search")}}</button></span>
                        </div>
                    </div>
                </form>


            </div>

        </div>

    </div>
    <br />

    <div class="row">
        <div class="col-md-12">

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>{{trans("manage_roles.Role")}}</th>
                        <th>{{trans("manage_roles.Role Description")}}</th>
                        <?php if($edit_crt==TRUE):?>
                        <th width="200"></th>
                        <?php endif;?>
                    </tr>
                </thead>

                <tbody>

                    @foreach($roles as $i=>$item)
                    <tr>
                        <td>{{$item->role_name}}</td>
                        <td>{{$item->role_description}}</td>


                        <?php if($edit_crt==TRUE):?>
                        <td>
                            <a href="{{ url("roles_edit/") }}/{{ $item->id }}">
                                <i class="icon edit-icon"></i>
                            </a>
                           <a href="javascript:void(0);" data-id="{{ $item->id }}" class="deleter"><i class="icon trash-icon"></i></a>

                        </td>

                        <?php endif;?>

                    </tr>
                    @endforeach
                </tbody>

                <tfooot>
                    <tr>
                        <td colspan="3"> {!! $roles->render() !!}</td>
                    </tr>
                </tfooot>


            </table>

        </div>

    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->

<script type="text/javascript">

    $(document).ready(function(){

        $('.deleter').click(function(){

            var id = $(this).data('id');
            $('#delete_proceed').attr('alt',id);
            $('#confirm').modal('show');

        });

        $('#delete_proceed').click(function(){

            var id = $(this).attr('alt');

            $('#confirm').modal('hide');

            window.location.href='{{ url('roles_delete') }}/'+id;

        });

    });

</script>

<div class="modal fade" tabindex="-1" role="dialog" id="confirm">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{trans("manage_roles.Confirmation")}}</h4>
      </div>
      <div class="modal-body">
        <p>{{trans("manage_roles.Are you sure you want to delete this record ?")}}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("manage_roles.No")}}</button>
       <button type="button" class="btn btn-primary" id="delete_proceed" alt="">{{trans("manage_roles.Yes")}}</button>

      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




@endsection