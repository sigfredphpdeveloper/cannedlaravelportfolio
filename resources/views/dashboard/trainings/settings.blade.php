@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-md-12">
            @if(Session::has('success'))
                <div class="alert alert-block alert-success">
                    <a class="close" data-dismiss="alert" href="#">×</a>
                    <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('success') }}</h4>
                </div>
            @elseif(Session::has('error'))
                 <div class="alert alert-block alert-danger">
                    <a class="close" data-dismiss="alert" href="#">×</a>
                    <h4 class="alert-heading"><i class="fa-fw fa fa-times"></i> {{ Session::get('error') }}</h4>
                </div>
            @endif
        </div>

        <article class="col-sm-6 col-md-6 col-lg-6">

            <form  id="smart-form-register" class="smart-form client-form" role="form" 
            method="POST" action="{{ url('/trainings-setting') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="user_id" value="{{ $user['id'] }}">

					<fieldset>
		            <div class="row">
		            
			    		<section class="col col-5">
        			         <label class="toggle">
        			               <input type="checkbox" name="trainings"
        			               value="1" <?php echo ($current_company->training =='1')?'checked="checked"':'';?> >
        			               <i data-swchon-text="ON" data-swchoff-text="OFF"></i> Trainings
                                </label>
			             </section>
			             
		            </div>

		           </fieldset>
		           
                <footer>
                    <button type="submit" class="btn btn-primary">
                        {{trans("expenses_settings.Save")}}
                    </button>
                </footer>


            </form>

        </article>

    </div>
    <!-- end row -->
    <br />

    @if( $company['training']==1 )
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-6">
            <div class="well">
                <form action="{{ url('training-permission') }}" method="POST" >
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <h1>Manage Trainings</h1>

                        <?php
                            $count = count($permissions);
                        ?>

                        @include('dashboard.layouts.permission_select')


                        <input type="submit" value="{{trans("manage_permissions.Save")}}" class="btn btn-primary" data-toggle="tooltip" title="Save" />

                </form>
            </div>
        </div>
    </div>
    @endif
    
</section>

@endsection

@section('page-script')



@endsection