<!-- Add Page Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="add_expense_type">
    <div class="modal-dialog">
        <div class="modal-content">
            <div role="content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"> Add New Page </h4>
                </div>

                <div class="modal-body">
                    <div class="message-field"></div>
                    <form id="add-page-form" action="{{ url('/add-page') }}" method="POST" class="smart-form add-page-form">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <input type="hidden" name="topic_id" id="token" value="{{ $topic_id }}">
                        <fieldset>
                            <section>
                                <label class="label">Title</label>
                                <label class="input ">
                                      <input type="text" name="title"/>
                                </label>
                            </section>
                        </fieldset>
                        <footer>
                            <input type="submit" class="btn btn-primary btn-add-expense-type" alt="" value="Save" />
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Add page Modal -->

<script type="text/javascript">
    $(document).ready(function(){
        var $addpageForm = $('#add-page-form').validate({
            rules : {
                title : "required",
            }
        });
    });
</script>