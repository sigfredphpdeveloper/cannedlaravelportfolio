<div class="alert alert-info fade in">
                                <button class="close" data-dismiss="alert">
                                    ×
                                </button>
                                <i class="fa-fw fa fa-info"></i>
                                Select a picture or file in the Media Gallery. If you upload a new picture or file, please select it after upload.
                            </div>
<div id="contenutoimmagini"></div>

<br />
<form enctype="multipart/form-data" id="form-id">
    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
    <input name="nomefile" type="file" />
    <input class="button" type="button" value="Upload" />
</form>
<br />
<progress value="0"></progress>

<script>
    $(document).ready(function(){

    //html5 uploader
    $('.button').click(function(){
        var formx = document.getElementById('form-id');
        var formData = new FormData(formx);
        base_url = $('#base_url').val();

        $.ajax({
            url: base_url+'/upload-media',  //Server script to process data
            type: 'POST',
            xhr: function() {  // Custom XMLHttpRequest
                var myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){ // Check if upload property exists
                    myXhr.upload.addEventListener('progress',progressHandlingFunction, false); // For handling the progress of the upload
                }
                return myXhr;
            },
            //Ajax events
            success: completeHandler,
            error: errorHandler,
            // Form data
            data: formData,
            //Options to tell jQuery not to process data or worry about content-type.
            cache: false,
            contentType: false,
            processData: false
        });

        function completeHandler(){
            loadimages();
        }
        function errorHandler(){
            alert('Upload Error');
        }
        function progressHandlingFunction(e){
            if(e.lengthComputable){
                $('progress').attr({value:e.loaded,max:e.total});
            }
        }
    });

    loadimages();

    });

    function inserisci(elemento){
        var link = $(elemento);
        var image = link.data('image');
        var type = link.data('type');
        var name = link.data('name');
        var extension = link.data('extension');

        if( type == 'image' ){
            $('#img-url').val(image);
            $('#imgContent').children('img').attr('src', image);
            $('#mediagallery').slideUp();
            $('#thepref').slideDown();
        } 
        else if ( type == 'audio' ) {
            $('#audio-url').val(image);
            $('#audio-extension').val(extension);
            $('#audioContent').children('audio').remove();
            $('#audioContent').prepend(' <audio controls style="width: 100%;"><source src="'+image+'" type="audio/'+extension+'" title="">Your browser does not support the audio element.</audio>');
            // $('#audioContent').children('audio').find('source').attr('src', image);
            $('#mediagallery').slideUp();
            $('#thepref').slideDown();
        }
        else if( type == 'file' || type == 'image' ) {
            $('#file-url').val(image);
            $('#fileContent').find('a').attr('href', image);
            $('#fileContent').find('.file-title').html(name);
            $('#file-title').val(name);

            $('#mediagallery').slideUp();
            $('#thepref').slideDown();
        }

    }

    function loadimages(){

        base_url = $('#base_url').val();
        var request = $.ajax({
            url: base_url+"/load-images",
            method: "GET",
            data: { nome : '' },
            dataType: "html"
        });

        request.done(function( msg ) {
            $( "#contenutoimmagini" ).html( msg );
        });

    }
</script>