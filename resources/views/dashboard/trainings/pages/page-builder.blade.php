<!DOCTYPE html>
<html>
<head>
	<title>Build {{$page->title}}</title>
	<script src="{{ asset('/js/libs/jquery-2.1.1.min.js') }}"></script>
	<script src="{{ asset('/js/libs/jquery-ui-1.10.3.min.js') }}"></script>
	<script src="{{ asset('/js/bootstrap/bootstrap.min.js') }}"></script>

	<!-- Basic Styles -->
	<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/font-awesome.min.css') }}">

	<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/page-builder/theme.css') }}">
	<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/page-builder/bootstrap-colorselector.css') }}">

	<script src="https://cdn.jsdelivr.net/ace/1.2.0/min/ace.js"></script>
    <script src="//tinymce.cachefly.net/4.2/tinymce.min.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
    <script src="{{ asset('/js/page-builder/bootstrap-colorselector.js') }}"></script>

	 <style>
	    .lyrow{
	        margin-bottom:10px;
	    }
    </style>
</head>
<body class="edit">
    <input type="hidden" value="{{ url('/') }}" id="base_url">
	<div class="navbar navbar-inverse navbar-fixed-top navbar-htmleditor">
            <div class="navbar-header">
                <button data-target="navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="glyphicon-bar"></span>
                    <span class="glyphicon-bar"></span>
                    <span class="glyphicon-bar"></span>
                </button>
                <a class="navbar-brand" href='{{ url("/topic", $page->topic_id) }}'>Back To Topic <span style="margin-left: 8px">-</span></a>
                <div style="display: inline-block;margin-top: 8px;">
                    <input type="text" id="page-name" value="{{ $page->title }}" style="border: 1px solid #BDBDBD;padding: 3px 10px;width: 200px;font-size: 18px;">
                </div>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav" id="menu-htmleditor">
                    <li>
                        <div class="btn-group" data-toggle="buttons-radio">
                            <button type="button" id="edit" class="active btn btn-primary"><i class="glyphicon glyphicon-edit "></i> Edit</button>
                            <button type="button" class="btn btn-primary" id="sourcepreview"><i class="glyphicon-eye-open glyphicon"></i> Preview</button>
                            <button type="button" id="save" class="btn btn-warning float-right"><i class="fa fa-save"></i>&nbsp;Save</button>
                        </div>
                        &nbsp;
                       <!--  <div class="btn-group" data-toggle="buttons-radio" id='add' style='display: none;'>
                            <button type="button" class="active btn btn-default" id="pc"><i class="fa fa-laptop"></i> Desktop</button>
                            <button type="button" class="btn btn-default" id="tablet"><i class="fa fa-tablet"></i> Tablet</button>
                            <button type="button" class="btn btn-default" id="mobile"><i class="fa fa-mobile"></i> Mobile</button>
                        </div> -->
                    </li>



                </ul>

            </div><!--/.navbar-collapse -->

        </div><!--/.navbar-fixed-top -->

        <div class="container">
            <div class="row">
                <div class="">
                    <div class="sidebar-nav">

                        <ul class="nav nav-list ">
                            <li class="nav-header">
                                <i class="fa fa fa-th"> </i>&nbsp;    Layout Options
                            </li>
                            <li class="rows" id="estRows">
                                <div class="lyrow">
                                    <a href="#close" class="remove btn btn-danger btn-xs"><i class="glyphicon-remove glyphicon"></i></a>
                                    <a class="drag btn btn-default btn-xs"><i class="glyphicon glyphicon-move"></i></a>
                                    <a href="#" class="btn btn-info btn-xs clone"><i class="fa fa-clone"></i></a>

                                    <div class="preview"><input type="text" value="1 column" class="form-control"></div>
                                    <div class="view">
                                        <div class="row clearfix">
                                            <div class="col-md-12 column"></div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="lyrow">
                                    <a href="#close" class="remove btn btn-danger btn-xs"><i class="glyphicon-remove glyphicon"></i></a>
                                    <a class="drag btn btn-default btn-xs"><i class="glyphicon glyphicon-move"></i></a>
                                    <a href="#" class="btn btn-info btn-xs clone"><i class="fa fa-clone"></i></a>
                                    <div class="preview"><input type="text" value="2 columns" class="form-control"></div>
                                    <div class="view">
                                        <div class="row clearfix">
                                            <div class="col-md-6 column"></div>
                                            <div class="col-md-6 column"></div>
                                        </div>
                                    </div>
                                </div>

                            </li>
                        </ul>
                        <br>
                        <ul class="nav nav-list">
                            <li class="nav-header"><i class="fa fa-html5"></i>&nbsp;  Html Elements

                            </li>
                            <li class="boxes" id="elmBase">

                                <!-- elemento paragraph -->
                                <div class="box box-element" data-type="paragraph">
                                    <a href="#close" class="remove btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove"></i></a>
                                    <a class="drag btn btn-default btn-xs"><i class="glyphicon glyphicon-move"></i></a>
                                    <span class="configuration">
                                        <a class="btn btn-xs btn-warning settings"  href="#" ><i class="fa fa-gear"></i></a>
                                    </span>

                                    <div class="preview">
                                        <i class="fa fa-font fa-2x"></i>
                                        <div class="element-desc">Paragraph</div>
                                    </div>
                                    <div class="view">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisci elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrum exercitationem ullam
                                           corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident,
                                           sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                    </div>
                                </div>

                                <!-- Image -->
                                <div class="box box-element" data-type="image">
                                    <a href="#close" class="remove btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove"></i></a>
                                    <a class="drag btn btn-default btn-xs"><i class="glyphicon glyphicon-move"></i></a>
                                    <span class="configuration">
                                        <a class="btn btn-xs btn-warning settings"  href="#" ><i class="fa fa-gear"></i></a>
                                    </span>
                                    <div class="preview">
                                        <i class="fa fa-picture-o fa-2x"></i>
                                        <div class="element-desc">Image</div>
                                    </div>
                                    <div class="view">
                                        <img src="http://placehold.it/350x150" class="img-responsive"  title="default title"/>
                                    </div>
                                </div>

                                <!-- Youtube -->
                                <div class="box box-element" data-type="youtube">
                                    <a href="#close" class="remove btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove"></i></a>
                                    <a class="drag btn btn-default btn-xs"><i class="glyphicon glyphicon-move"></i></a>
                                    <span class="configuration">
                                        <a class="btn btn-xs btn-warning settings"  href="#" ><i class="fa fa-gear"></i></a>
                                    </span>

                                    <div class="preview">
                                        <i class="fa  fa fa-youtube-play  fa-2x"></i>
                                        <div class="element-desc">Youtube</div>
                                    </div>
                                    <div class="view">
                                        <div class="video-holder">
                                             <i class="fa  fa fa-youtube-play  fa-2x"></i>
                                        </div>
                                        <iframe style="height: 0" class="img-responsive" src="https://www.youtube.com/embed/WIJaD623dy0" frameborder="0" allowfullscreen data-url=""></iframe>
                                    </div>
                                </div>
                                <!-- Vimeo -->
                                <div class="box box-element" data-type="vimeo">
                                    <a href="#close" class="remove btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove"></i></a>
                                    <a class="drag btn btn-default btn-xs"><i class="glyphicon glyphicon-move"></i></a>
                                    <span class="configuration">
                                        <a class="btn btn-xs btn-warning settings"  href="#" ><i class="fa fa-gear"></i></a>
                                    </span>

                                    <div class="preview">
                                        <i class="fa  fa-vimeo-square fa-2x"></i>
                                        <div class="element-desc">Vimeo</div>
                                    </div>
                                    <div class="view">
                                        <div class="video-holder">
                                             <i class="fa  fa fa-vimeo-square  fa-2x"></i>
                                        </div>
                                        <iframe style="height: 0" class="img-responsive" src="https://player.vimeo.com/video/137463767?byline=0&portrait=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                    </div>
                                </div>
        
                                <!-- audio element -->
                                <div class="box box-element" data-type="audio">
                                    <a href="#close" class="remove btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove"></i></a>
                                    <a class="drag btn btn-default btn-xs"><i class="glyphicon glyphicon-move"></i></a>
                                    <span class="configuration">
                                        <a class="btn btn-xs btn-warning settings" href="#" ><i class="fa fa-gear"></i></a>
                                    </span>
                                    <div class="preview">
                                         <i class="fa  fa-file-audio-o fa-2x"></i>
                                        <div class="element-desc">Audio</div>
                                    </div>
                                    <div class="view">
                                        <div class="audio-cotrols">
                                            <audio controls>
                                              <source src="{{ asset('/files/london.mp3') }}" type="audio/mp3" title="">
                                            Your browser does not support the audio element.
                                            </audio>
                                            <a href="{{ asset('/files/london.mp3') }}" class="audio-download" download><i class="fa  fa-download fa-2x"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <!-- file element -->
                                <div class="box box-element" data-type="file">
                                    <a href="#close" class="remove btn btn-danger btn-xs"><i class="glyphicon glyphicon-remove"></i></a>
                                    <a class="drag btn btn-default btn-xs"><i class="glyphicon glyphicon-move"></i></a>
                                    <span class="configuration">
                                        <a class="btn btn-xs btn-warning settings" href="#" ><i class="fa fa-gear"></i></a>
                                    </span>
                                    <div class="preview">
                                         <i class="fa  fa-file-text-o fa-2x"></i>
                                        <div class="element-desc">File</div>
                                    </div>
                                    <div class="view">
                                        <div class="file-cotrols">
                                            <a href="#" target="_blank"><i class="fa fa-file-text-o fa-2x"></i> <span class="file-title">a-sample-file-name.pdf</span> </a>
                                        </div>
                                    </div>
                                </div>

                            </li>
                        </ul>
                    </div>
                </div>
                <!--/span-->
                <div class="htmlpage">
					{!! $page->raw_html !!}
                </div>

                <!--/row-->

            </div><!--/.fluid-container-->



            <div class="modal fade" id="download" tabindex="-1" role="dialog" aria-labelledby="download" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title"><i class='fa fa-save'></i>&nbsp;Save as </h4>
                        </div>

                        <div class="modal-body" id='sourceCode'>
                        	<div class="message-area" style="display: none">
                        		<div class="alert alert-success fade in">
					                <button class="close" data-dismiss="alert">
					                    ×
					                </button>
					                <i class="fa-fw fa fa-check"></i>
					                <strong>Success</strong> The page has been Updated.
					            </div>
                        	</div>
                            <span> Please confirm you want to save the page. </span>
                            <textarea id="src" rows="10" style="display: none;"></textarea>
                            <textarea id="model" rows="10" class="form-control" style="display: none;"></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class='fa fa-close'></i>&nbsp;Close</button>
                            <button type="button" class="btn btn-success" data-content="{{$page->id}}" data-url="{{ url('/') }}" data-token="{{ csrf_token() }}" id="srcSave"><i class='fa fa-save'></i>&nbsp;Save</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="preferences" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="preferencesTitle"></h4>
                        </div>
                        <div class="modal-body" id="preferencesContent">
                           <!--<iframe src="media-popup.php" style="width:100%; height:300px ; display:none;" frameborder="0" ></iframe>-->
                           <div  id="mediagallery"  style="overflow:auto;height:400px; display:none" >
                            @include('dashboard.trainings.pages.media-popup')
                            <div style="display: block;margin: 10px 0; text-align: right;">
                                <a class="btn btn-info" href="javascript:;" onclick="$('#mediagallery').hide();$('#thepref').show();">Return to settings</a>
                            </div>
                           </div>
                            <div id="thepref">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#Settings" aria-controls="Settings" role="tab" data-toggle="tab">Settings</a></li>
                                    <!-- <li role="presentation"><a href="#CellSettings" aria-controls="profile" role="tab" data-toggle="tab">Cell settings</a></li>
                                    <li role="presentation"><a href="#RowSettings" aria-controls="messages" role="tab" data-toggle="tab">Row settings</a></li> -->
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="Settings">
                                        <!-- header -->
                                        <div class="panel panel-body">
                                            <div id="ht" style="display: none;">
                                                <textarea id="html5editorLite"></textarea>
                                            </div>
                                            <!-- fine header -->
                                            <!-- text -->
                                            <div id="text" style="display: none;">
                                                <textarea id="html5editor"></textarea>
                                            </div>
                                            <!-- end text -->
                                            <div id="vimeo"  style="display:none">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div id="vimeo-video">

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="alert alert-info fade in" style="padding: 10px; margin-top: 10px;">
                                                            <button class="close" data-dismiss="alert">
                                                                ×
                                                            </button>
                                                            <i class="fa-fw fa fa-info"></i>
                                                            Please copy here the Vimeo video link.
                                                        </div>
                                                        <form>
                                                            <div class="form-group">
                                                                <label for="vm-video-url">Video URL :</label>
                                                                <input type="text" value="" id="vm-video-url" class="form-control" />
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="vm-alignment">Alignment :</label>
                                                                <select id="vm-alignment" class="form-control">
                                                                    <option value="pull-left">Left</option>
                                                                    <option value="pull-right">Right</option>
                                                                    <option value="center-block">Center</option>
                                                                </select>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="vm-video-width">Width :</label>
                                                                        <input type="text" value="" id="vm-video-width" class="form-control"/>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="vm-video-height">Height :</label>
                                                                        <input type="text" value="" id="vm-video-height" class="form-control"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- settaggio youtube -->
                                            <div id="youtube"  style="display:none">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div id="youtube-video">

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="alert alert-info fade in" style="padding: 10px; margin-top: 10px;">
                                                            <button class="close" data-dismiss="alert">
                                                                ×
                                                            </button>
                                                            <i class="fa-fw fa fa-info"></i>
                                                            Please copy here the YouTube video link.
                                                        </div>
                                                        <form>
                                                            <div class="form-group">
                                                                <label for="video-url">Video URL :</label>
                                                                <input type="text" value="" id="video-url" class="form-control" />
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="video-alignment">Alignment :</label>
                                                                <select id="yt-alignment" class="form-control">
                                                                    <option value="pull-left">Left</option>
                                                                    <option value="pull-right">Right</option>
                                                                    <option value="center-block">Center</option>
                                                                </select>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="video-width">Width :</label>
                                                                        <input type="text" value="" id="video-width" class="form-control"/>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="video-height">Height :</label>
                                                                        <input type="text" value="" id="video-height" class="form-control"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- fine settagio youtube -->

                                            <!-- settaggio immagine -->
                                            <div id="image" style="display:none">
                                                <div class="row">
                                                    <div class="col-md-5" >
                                                        <div id="imgContent">

                                                        </div>
                                                        <a class="btn btn-default form-control gallery" href="#" id="gallery"><i class="icon-upload-alt"></i>&nbsp;Browse ...</a>
                                                    </div>
                                                    <div class="col-md-7">
                                                        <div class="form-group">
                                                            <label for="img-url">Url :</label>
                                                            <input type="text" value="" id="img-url" class="form-control" />
                                                        </div>
                                                        <!--   <div class="form-group">
                                                               <label for="img-url">Click Url:</label>
                                                               <input type="text" value="" id="img-clickurl" class="form-control" />
                                                           </div>
                                                        -->
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="img-width">Width :</label>
                                                                    <input type="text" value="" id="img-width" class="form-control"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label for="img-height">Height :</label>
                                                                    <input type="text" value="" id="img-height" class="form-control"/>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="img-title">Title : </label>
                                                            <input type="text" value="" id="img-title" class="form-control"/>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="img-alignment">Alignment :</label>
                                                            <select id="img-alignment" class="form-control">
                                                                <option value="pull-left">Left</option>
                                                                <option value="pull-right">Right</option>
                                                                <option value="center-block">Center</option>
                                                            </select>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- fine settaggi immagine -->

                                            <!-- settaggio mappa -->
                                            <div id="map" style="display:none">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div id="map-content">

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <form>
                                                            <div class="form-group">
                                                                <label for="address">Latitude :</label>
                                                                <input type="text" value="" id="latitude" class="form-control" />
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="address">Longitude :</label>
                                                                <input type="text" value="" id="longitude" class="form-control" />
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="address">Zoom :</label>
                                                                <input type="text" value="" id="zoom" class="form-control" />
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="img-width">Width :</label>
                                                                        <input type="text" value="" id="map-width" class="form-control"/>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label for="img-height">Height :</label>
                                                                        <input type="text" value="" id="map-height" class="form-control"/>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- settaggio bottone -->
                                            <div id="buttons" style="display:none">
                                                <div id="buttonContainer"></div>
                                                <br>
                                                <div class="form-group">
                                                    <label> Label :  </label>
                                                    <input type="text" class="form-control" id="buttonLabel"/>
                                                </div>

                                                <div class="form-group">
                                                    <label> Href :  </label>
                                                    <input type="text" class="form-control" id="buttonHref"/>
                                                </div>
                                                <span class="btn-group btn-group-xs">
                                                    <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="#">Styles <span class="caret"></span></a>
                                                    <ul class="dropdown-menu">
                                                        <li class="" ><a href="#" class="btnpropa" rel="btn-default">Default</a></li>
                                                        <li class="" ><a href="#" class="btnpropa" rel="btn-primary">Primary</a></li>
                                                        <li class="" ><a href="#" class="btnpropa" rel="btn-success">Success</a></li>
                                                        <li class="" ><a href="#" class="btnpropa" rel="btn-info">Info</a></li>
                                                        <li class="" ><a href="#" class="btnpropa" rel="btn-warning">Warning</a></li>
                                                        <li class="" ><a href="#" class="btnpropa" rel="btn-danger">Danger</a></li>
                                                        <li class="" ><a href="#" class="btnpropa" rel="btn-link">Link</a></li>
                                                    </ul>
                                                </span>
                                                <span class="btn-group btn-group-xs">
                                                    <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="#">Size <span class="caret"></span></a>
                                                    <ul class="dropdown-menu">
                                                        <li class="" ><a href="#" class="btnpropb" rel="btn-lg">Large</a></li>
                                                        <li class="" ><a href="#" class="btnpropb" rel="btn-default">Default</a></li>
                                                        <li class="" ><a href="#" class="btnpropb" rel="btn-sm">Small</a></li>
                                                        <li class="" ><a href="#" class="btnpropb" rel="btn-xs">Mini</a></li>
                                                    </ul>
                                                </span>
                                                <span class="btn-group btn-group-xs">
                                                    <a class="btn btn-xs btn-default btnprop" href="#" rel="btn-block">Block</a>
                                                    <a class="btn btn-xs btn-default btnprop" href="#" rel="active">Active</a>
                                                    <a class="btn btn-xs btn-default btnprop" href="#" rel="disabled">Disabled</a>
                                                </span>


                                                <br><br>
                                                <div class="form-group">
                                                    <label> Custom width / height / font-size / padding top :  </label>
                                                    <br>
                                                    <span class="btn-group">
                                                        <input type="text"  id="custombtnwidth"      style="width:20%"/>
                                                        <input type="text"  id="custombtnheight"     style="width:20%"/>
                                                        <input type="text"  id="custombtnfont"       style="width:20%"/>
                                                        <input type="text"  id="custombtnpaddingtop" style="width:20%"/>
                                                    </span>
                                                </div>
                                                <!--
                                                <div class="form-group">
                                                    <label> Align:  </label>
                                                    <br>
                                                    <span class="btn-group">
                                                        <select id="btnalign">
                                                            <option value="center">center</option>
                                                            <option value="left">left</option>
                                                            <option value="right">right</option>
                                                        </select>
                                                    </span>
                                                </div>
                                                -->

                                                <div class="form-group">
                                                    <label>Custom background color :</label>
                                                    <input type="text" class="form-control" id="colbtn" />

                                                    <select id="colorselectorbtn">
                                                        <option value="1" data-value="1" data-color="#A0522D">sienna</option>
                                                        <option value="2" data-value="2" data-color="#CD5C5C">indianred</option>
                                                        <option value="3" data-value="3" data-color="#FF4500">orangered</option>
                                                        <option value="4" data-value="4" data-color="#008B8B">darkcyan</option>
                                                        <option value="5" data-value="5" data-color="#B8860B">darkgoldenrod</option>
                                                        <option value="6" data-value="6" data-color="#32CD32">limegreen</option>
                                                        <option value="7" data-value="7" data-color="#FFD700">gold</option>
                                                        <option value="8" data-value="8" data-color="#48D1CC">mediumturquoise</option>
                                                        <option value="9" data-value="9" data-color="#87CEEB">skyblue</option>
                                                        <option value="10" data-value="10" data-color="#FF69B4">hotpink</option>
                                                        <option value="11" data-value="11" data-color="#87CEFA">lightskyblue</option>
                                                        <option value="12" data-value="12" data-color="#6495ED">cornflowerblue</option>
                                                        <option value="13" data-value="13" data-color="#DC143C">crimson</option>
                                                        <option value="14" data-value="14" data-color="#FF8C00">darkorange</option>
                                                        <option value="15" data-value="15" data-color="#C71585">mediumvioletred</option>
                                                        <option value="16" data-value="16" data-color="#000000">black</option>

                                                        <option value="17" data-value="17" data-color="#575757">grigio scuro</option>
                                                        <option value="18" data-value="18" data-color="#f2f2f2">grigio chiaro</option>
                                                        <option value="19" data-value="19" data-color="#efefef">marroncino</option>
                                                        <option value="20" data-value="20" data-color="#e7e0d8">marrone</option>
                                                        <option value="21" data-value="21" data-color="#d7d0c6">marrone scuro</option>
                                                        <option value="22" data-value="22" data-color="#263459">blu scuro</option>
                                                        <option value="23" data-value="23" data-color="#ffffff">bianco</option>

                                                    </select>
                                                    <script>
                                                    $('#colorselectorbtn').colorselector({
                                                              callback: function (value, color, title) {
                                                                  $("#colbtn").val(color);
                                                              }
                                                        });
                                                    </script>
                                                </div>

                                                <div class="form-group">
                                                    <label>Custom text color :</label>
                                                    <input type="text" class="form-control" id="colbtncol" />

                                                    <select id="colorselectorbtncol">
                                                        <option value="1" data-value="1" data-color="#A0522D">sienna</option>
                                                        <option value="2" data-value="2" data-color="#CD5C5C">indianred</option>
                                                        <option value="3" data-value="3" data-color="#FF4500">orangered</option>
                                                        <option value="4" data-value="4" data-color="#008B8B">darkcyan</option>
                                                        <option value="5" data-value="5" data-color="#B8860B">darkgoldenrod</option>
                                                        <option value="6" data-value="6" data-color="#32CD32">limegreen</option>
                                                        <option value="7" data-value="7" data-color="#FFD700">gold</option>
                                                        <option value="8" data-value="8" data-color="#48D1CC">mediumturquoise</option>
                                                        <option value="9" data-value="9" data-color="#87CEEB">skyblue</option>
                                                        <option value="10" data-value="10" data-color="#FF69B4">hotpink</option>
                                                        <option value="11" data-value="11" data-color="#87CEFA">lightskyblue</option>
                                                        <option value="12" data-value="12" data-color="#6495ED">cornflowerblue</option>
                                                        <option value="13" data-value="13" data-color="#DC143C">crimson</option>
                                                        <option value="14" data-value="14" data-color="#FF8C00">darkorange</option>
                                                        <option value="15" data-value="15" data-color="#C71585">mediumvioletred</option>
                                                        <option value="16" data-value="16" data-color="#000000">black</option>
                                                        <option value="17" data-value="17" data-color="#575757">grigio scuro</option>
                                                        <option value="18" data-value="18" data-color="#f2f2f2">grigio chiaro</option>
                                                        <option value="19" data-value="19" data-color="#efefef">marroncino</option>
                                                        <option value="20" data-value="20" data-color="#e7e0d8">marrone</option>
                                                        <option value="21" data-value="21" data-color="#d7d0c6">marrone scuro</option>
                                                        <option value="22" data-value="22" data-color="#263459">blu scuro</option>
                                                        <option value="23" data-value="23" data-color="#ffffff">bianco</option>

                                                    </select>
                                                    <script>
                                                    $('#colorselectorbtncol').colorselector({
                                                              callback: function (value, color, title) {
                                                                  $("#colbtncol").val(color);
                                                              }
                                                        });
                                                    </script>
                                                </div>


                                            </div>
                                            <!-- fine bottone-->
                                            <!-- settaggio code -->
                                            <div id="code"  style="display:none">
                                            </div>
                                            <!-- fine code -->

                                            <!-- settings audio -->
                                            <div id="audio" style="display:none">
                                                <div class="row">
                                                    <div class="col-md-5" >
                                                        <div id="audioContent">

                                                        </div>
                                                        <a class="btn btn-default form-control gallery" href="#" id="gallery"><i class="icon-upload-alt"></i>&nbsp;Browse ...</a>
                                                    </div>
                                                    <div class="col-md-7">
                                                        <div class="form-group">
                                                            <label for="audio-url">Url :</label>
                                                            <input type="text" value="" id="audio-url" class="form-control" />
                                                        </div>
                                                        <input type="hidden" value="" id="audio-extension" class="form-control" />

                                                        <div class="form-group">
                                                            <label for="audio-title">Title : </label>
                                                            <input type="text" value="" id="audio-title" class="form-control"/>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- end settings audio -->

                                            <!-- settings file -->
                                            <div id="file" style="display:none">
                                                <div class="row">
                                                    <div class="col-md-5" >
                                                        <div id="fileContent">

                                                        </div>
                                                        <a class="btn btn-default form-control gallery" href="#" id="gallery"><i class="icon-upload-alt"></i>&nbsp;Browse ...</a>
                                                    </div>
                                                    <div class="col-md-7">
                                                        <div class="form-group">
                                                            <label for="file-url">Url :</label>
                                                            <input type="text" value="" id="file-url" class="form-control" />
                                                        </div>
                                                        <!-- <input type="hidden" value="" id="audio-extension" class="form-control" /> -->

                                                        <div class="form-group">
                                                            <label for="file-title">Title : </label>
                                                            <input type="text" value="" id="file-title" class="form-control"/>
                                                        </div>

                                                        <!-- <div class="form-group">
                                                            <label for="file-alt">Alt text : </label>
                                                            <input type="text" value="" id="file-alt" class="form-control"/>
                                                        </div> -->

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- end settings audio -->
                                            
                                            <div class="row" style="display: none;">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label> ID :  </label>
                                                        <input type="text" readonly class="form-control" id="id"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">

                                                        <label for="class"> Css class :  </label>
                                                        <input type="text" name="class" id="class" class="form-control" />

                                                    </div>
                                                </div>
                                            </div>
                                             
                                        </div>
                                    </div>
                                    

                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal"><i class='fa fa-close'></i>&nbsp;Close</button>
                                <button type="button" class="btn btn-primary" id="applyChanges"><i class='fa fa-check'></i>&nbsp;Apply changes</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Small modal for alert-->

                <!--/span-->
                <div id="download-layout"><div class="container"></div></div>
            </div>
            <script src="{{ asset('/js/page-builder/app.js') }}"></script>
</body>
<style type="text/css">
    #imgContent img {
        height: auto !important;
    }
</style>
</html>
