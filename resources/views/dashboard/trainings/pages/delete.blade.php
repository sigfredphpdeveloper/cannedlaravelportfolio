<!-- delete page Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="delete_expense_type">
    <div class="modal-dialog">
        <div class="modal-content">
            <div role="content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"> Delete {{ $page->title }} </h4>
                </div>

                <div class="modal-body">
                    <div class="message-field"></div>
                    <form id="delete-page-form" action="{{ url('/delete-page', $page->id) }}" method="POST" class="smart-form delete-page-form">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <fieldset>
                            <p>Are you sure you want to delete this page? <br />
                            </p>

                        </fieldset>
                        <footer>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <input type="submit" class="btn btn-primary btn-delete-expense-type" alt="" value="Delete" />
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End delete page Modal -->
