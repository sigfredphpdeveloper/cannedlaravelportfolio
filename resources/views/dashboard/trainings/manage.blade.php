@extends('dashboard.layouts.master')

@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
			<h1 class="page-title txt-color-blueDark">
				<i class="fa fa-graduation-cap fa-fw "></i> 
					{{trans("knowledge-module.Topics")}}
			</h1>
		</div>
		@include('dashboard.trainings.customize-link')
	</div>
	@if(Session::has('success'))
		<div class="alert alert-block alert-success">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('success') }}</h4>
		</div>
	@elseif(Session::has('error'))
		<div class="alert alert-block alert-danger">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('error') }}</h4>
		</div>
	@endif

	<!-- widget grid -->
	<section id="widget-grid" class="">
		<!-- row -->
		<div class="row">
			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-togglebutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2> {{trans("knowledge-module.All Company Topics")}} </h2>
					</header>
					<div>
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<div class="widget-body no-padding">
							<p style="text-align:right; padding: 10px 10px 0 10px">
                                <a href="javascript:void(0);" data-toggle="tooltip" title="" id="btn_add_topic" class="btn btn-primary" data-original-title="Add Expense"> {{trans("knowledge-module.Add New Topic")}} </a>
                            </p>
                            @if( count($trainings) != 0 )
									<table class="table table-hover">
										<thead>
											<tr>
												<th>Title</th>
												<th>Type</th>
												<th>Date Created</th>
												<th>Date Updated</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody class="page-sortable">
											@foreach($trainings as $topic)
												<tr data-content="{{ $topic->id }}">
													<td>{{ $topic->title }}</td>
													<td>{{ $topic->type }}</td>
													<td>{{ $topic->created_at }}</td>
													<td>{{ $topic->updated_at }}</td>
													<td>
														<a href="{{ url('topic').'/'.$topic->id }}" data-content="{{ $topic->id }}" class="view-topic-modal btn btn-primary">Manage</a>
														<a href="{{ url('topic-statistics', $topic->id) }}" data-content="{{ $topic->id }}" class="view-topic-modal btn btn-primary">Statistics</a>
														<a href="javascript:void(0);" data-content="{{ $topic->id }}" class="edit-topic-modal btn btn-primary">Edit</a>
														<a href="javascript:void(0);" data-content="{{ $topic->id }}" class="delete-topic-modal btn btn-primary">Delete</a>
													</td>
												</tr>
											@endforeach
										</tbody>
									</table>
							@else
								<p style="text-align:center">{{trans("knowledge-module.No Trainings")}}</p>
							@endif
						</div>
					</div>
				</div>
			</article>
		</div>
	</section>

@endsection

@section('page-script')

<script type="text/javascript" src="{{ url('/js/all-trainings.js') }} "></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.page-sortable').sortable({
			 stop: function(event, ui) { 
			 	var collection = '';
			 	$(this).children('tr').each(function(e){
			 		var id = $(this).attr('data-content');
			 		collection += id+'-';
			 	});

			 	var token = '{{ csrf_token() }}';

			 	zen_ajax({
					type : 'POST',
					data: { _token:token, collection:collection},
					url : base_url+'/topic-order',
				}).done(function(data){
					
				});

			  }
		});
	});
</script>

@endsection