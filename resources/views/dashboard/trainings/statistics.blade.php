@extends('dashboard.layouts.master')

@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
			<h1 class="page-title txt-color-blueDark">
				<i class="fa fa-graduation-cap fa-fw "></i> 
					<a href="{{ url('/all-topics') }}">{{trans("knowledge-module.Topics")}}</a> > {{ $topic->title }}
			</h1>
		</div>
		@include('dashboard.trainings.customize-link')
	</div>
	@if(Session::has('success'))
		<div class="alert alert-block alert-success">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('success') }}</h4>
		</div>
	@elseif(Session::has('error'))
		<div class="alert alert-block alert-danger">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('error') }}</h4>
		</div>
	@endif

	<!-- widget grid -->
	<section id="widget-grid" class="">
		<!-- row -->
		<div class="row">
			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-togglebutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2 style="text-transform:capitalize"> {{ $topic->title }} Statistics </h2>
					</header>
					<div>
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<div class="widget-body no-padding">
							<table class="table table-hover">
								<thead>
									<tr>
										<th>User</th>
										<th>Progress</th>
									</tr>
								</thead>
								<tbody class="page-sortable">
									@foreach( $company->users as $user )
										@if( $topic->user_hasAccess( $user ) )
											<tr>
												<td>
													{{$user->first_name}} {{ $user->last_name }}
												</td>
												<td>
													<div class="topic-progress" style="margin-left:0">
														<div class="progress-bar" aria-valuetransitiongoal="{{ $topic->user_progress( $user->id ) }}" aria-valuenow="{{ $topic->user_progress( $user->id ) }}" style="width: {{ $topic->user_progress( $user->id ) }}%;">
															<?= ( $topic->user_progress( $user->id  ) != 0 ) ? $topic->user_progress( $user->id  ).'%' : '' ?>
														</div>
													</div>
												</td>
											</tr>
										@endif
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</article>
		</div>
	</section>

@endsection

@section('page-script')


@endsection