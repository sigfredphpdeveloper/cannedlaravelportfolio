@extends('dashboard.layouts.master')

@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
			<h1 class="page-title txt-color-blueDark">
				<i class="fa fa-graduation-cap fa-fw "></i> 
					<a href="{{ url('trainings') }}">Trainings</a> > {{ $topic->title }}
			</h1>
		</div>
		@include('dashboard.trainings.customize-link')
	</div>

	@foreach( $pages as $page)
		<div class="row">
			<div class="container-fluid">
				<h2 style="text-transform: capitalize;">{{ $page->title }}</h2>
				{!! $page->content !!}
			</div>
		</div>
	@endforeach

	<div class="row">
		<div class="container-fluid text-align-right">
			<?php echo $pages->render(); ?>
		</div>
	</div>

@endsection

@section('page-script')

@endsection