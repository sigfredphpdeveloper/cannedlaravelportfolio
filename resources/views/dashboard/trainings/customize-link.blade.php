@if( $is_admin )
	<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4 pull-right text-right">
		<h3 class="page-title txt-color-blueDark">
				<a href="{{ url('/trainings-setting') }}" class="btn btn-primary">{{trans("knowledge-module.Customize")}}</a>
		</h3>
	</div>
@endif