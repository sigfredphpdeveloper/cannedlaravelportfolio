<!-- delete Topic Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="delete_expense_type">
    <div class="modal-dialog">
        <div class="modal-content">
            <div role="content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"> Delete {{ $topic->title }} </h4>
                </div>

                <div class="modal-body">
                    <div class="message-field"></div>
                    <form id="delete-topic-form" action="{{ url('/delete-topic', $topic->id) }}" method="POST" class="smart-form delete-topic-form">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <fieldset>
                            <p>{{trans("knowledge-module.Are you sure you want to delete this topic")}}? <br />
                            <strong><em>Note:</em></strong> {{trans("knowledge-module.Pages under this will be deleted also")}}.
                            </p>

                        </fieldset>
                        <footer>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <input type="submit" class="btn btn-primary btn-delete-expense-type" alt="" value="Delete" />
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End delete Topic Modal -->
