<!-- Add Topic Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="add_expense_type">
    <div class="modal-dialog">
        <div class="modal-content">
            <div role="content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"> {{trans("knowledge-module.Add New Topic")}} </h4>
                </div>

                <div class="modal-body">
                    <div class="message-field"></div>
                    <form id="add-topic-form" action="{{ url('/add-topic') }}" method="POST" class="smart-form add-topic-form">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <fieldset>
                            <section>
                                <label class="label">Title</label>
                                <label class="input ">
                                      <input type="text" name="title"/>
                                </label>
                            </section>
                            <section>
                                <div class="row">
                                    <div class="col col-4">
                                        <input type="radio" name="type" value="internal" checked>
                                        <i></i>
                                        {{trans("knowledge-module.Internal")}}
                                    </div>
                                    @if( strcasecmp( $company->company_name , 'ZenintraNet') == 0 || strcasecmp( $company->company_name , 'zenintranet') == 0)
                                    <div class="col col-4">
                                        <input type="radio" name="type" value="external">
                                        <i></i>
                                        {{trans("knowledge-module.External")}}
                                    </div>
                                    @endif
                                </div>
                                <!-- end radio button -->
                            </section>
                            <section class="internal-views">
                                <label class="checkbox">
                                    <input type="checkbox" name="role_visible" value="1">
                                <i></i>Roles</label>
                                    <div class="row role-choices" style="padding: 0 15px; display: none">
                                    @foreach($company_roles->chunk(3) as $chunk)
                                        <div class="col col-4">
                                            @foreach( $chunk as $role )
                                                <label class="checkbox">
                                                    <input type="checkbox" name="roles[]" value="{{ $role->id }}">
                                                    <i></i>{{ $role->role_name }}</label>
                                            @endforeach
                                        </div>
                                    @endforeach
                                    </div>
                                <!-- <br />  -->  
                                <label class="checkbox">
                                    <input type="checkbox" name="team_visible" value="1">
                                <i></i>Teams</label>
                                    <div class="row team-choices" style="padding: 0 15px; display: none">
                                    @foreach($company_teams->chunk(3) as $chunk)
                                        <div class="col col-4">
                                            @foreach( $chunk as $team )
                                                <label class="checkbox">
                                                    <input type="checkbox" name="teams[]" value="{{ $team->id }}">
                                                    <i></i>{{ $team->team_name }}</label>
                                            @endforeach
                                        </div>
                                    @endforeach
                                    </div>
                                <!-- <br /> -->
                                <label class="checkbox">
                                    <input type="checkbox" name="access_everyone" value="1" checked="">
                                 <i></i>Everyone</label>    
                            </section>
                        </fieldset>
                        <footer>
                            <input type="submit" class="btn btn-primary btn-add-expense-type" alt="" value="Save" />
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Add Topic Modal -->

<script type="text/javascript">
    $(document).ready(function(){
        var $addTopicForm = $('#add-topic-form').validate({
            rules : {
                title : "required",
            }
        });

        $('input[name=type]').on('click', function(e){
            if( $(this).val() == 'external' ){
                $('.internal-views').hide();
            }
            else{
                 $('.internal-views').show();
            }
        });

        $('input[name=role_visible]').on('click', function(e){
            $('.role-choices').toggle();
        });

        $('input[name=team_visible]').on('click', function(e){
            $('.team-choices').toggle();
        });
    });
</script>