@extends('dashboard.layouts.master')

@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
			<h1 class="page-title txt-color-blueDark">
				<i class="fa fa-graduation-cap fa-fw "></i> 
					<a href="{{ url('/all-topics') }}">{{trans("knowledge-module.Topics")}}</a> > {{ $topic->title }}
			</h1>
		</div>
		@include('dashboard.trainings.customize-link')
	</div>
	@if(Session::has('success'))
		<div class="alert alert-block alert-success">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('success') }}</h4>
		</div>
	@elseif(Session::has('error'))
		<div class="alert alert-block alert-danger">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('error') }}</h4>
		</div>
	@endif

	<!-- widget grid -->
	<section id="widget-grid" class="">
		<!-- row -->
		<div class="row">
			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-togglebutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2> {{trans("knowledge-module.All Pages")}} </h2>
					</header>
					<div>
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<div class="widget-body no-padding">
							<p style="text-align:right; padding: 10px 10px 0 10px">
                                <a href="javascript:void(0);" data-toggle="tooltip" title="" id="btn_add_page" class="btn btn-primary" data-original-title="Add Expense" data-content="{{ $topic->id }}"> {{trans("knowledge-module.Add New Page")}} </a>
                            </p>
                            @if( count($topic->pages) != 0 )
                            	<table class="table table-hover">
                            		<thead>
                            			<tr>
                            				<th>Title</th>
                            				<th>Action</th>
                            			</tr>
                            		</thead>
                            		<tbody class="page-sortable">
                            			@foreach( $topic->pages as $key => $page)
                            				<tr data-content="{{ $page->id }}">
                            					<td>{{ $page->title }}</td>
                            					<td>
                            						@if( $page->topic->type == 'internal' )
                            							<a href="{{ url('/training', $page->topic->id).'?page='.($key+1) }}" data-content="{{ $page->id }}" class="view-topic-modal btn btn-primary">View</a>
                            						@elseif( $page->topic->type == 'external')
                            							<a href="{{ url('/tutorials', $company->id).'/'.$page->topic->slug.'?page='.($key+1) }}" target="_blank" data-content="{{ $page->id }}" class="view-topic-modal btn btn-primary">View</a>
                            						@endif
                            						<a href="{{ url('/page-builder', $page->id) }}" data-content="{{ $topic->id }}" class="edit-page-modal btn btn-primary">Edit</a>
                            						<a href="javascript:void(0);" data-content="{{ $page->id }}" class="delete-page-modal btn btn-primary">Delete</a>
                            					</td>
                            				</tr>
                            			@endforeach
                            		</tbody>
                            	</table>
                            @endif
						</div>
					</div>
				</div>
			</article>
		</div>
	</section>

@endsection

@section('page-script')

<script type="text/javascript" src="{{ url('/js/pages.js') }} "></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.page-sortable').sortable({
			 stop: function(event, ui) { 
			 	var collection = '';
			 	$(this).children('tr').each(function(e){
			 		var id = $(this).attr('data-content');
			 		collection += id+'-';
			 	});

			 	var token = '{{ csrf_token() }}';

			 	$.ajax({
					type : 'POST',
					data: { _token:token, collection:collection},
					url : base_url+'/page-position',
				}).done(function(data){
					// console.log(data);
				});

			  }
		});
	});
</script>

@endsection