@extends('dashboard.layouts.master')

@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
			<h1 class="page-title txt-color-blueDark">
				<i class="fa fa-graduation-cap fa-fw "></i> 
					{{trans("knowledge-module.All Topics")}}
			</h1>
		</div>
		@include('dashboard.trainings.customize-link')
	</div>
	@if(Session::has('success'))
		<div class="alert alert-block alert-success">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('success') }}</h4>
		</div>
	@elseif(Session::has('error'))
		<div class="alert alert-block alert-danger">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('error') }}</h4>
		</div>
	@endif

	<!-- widget grid -->
	<section id="widget-grid" class="">
		<!-- row -->
		<div class="row">
			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-togglebutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2> {{trans("knowledge-module.All Company Topics")}} </h2>
					</header>
					<div>
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<div class="widget-body">
                            @if( count($trainings_internal) != 0 )
                            	<h3>{{trans("knowledge-module.Internal Trainings")}}</h3>
									<table class="table table-hover">
										<thead>
											<tr>
												<th>Title</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											@foreach($trainings_internal as $topic)
												@if( $topic->user_hasAccess( Auth::user() ) )
													<tr>
														<td width="500"><a href="{{ url('training').'/'.$topic->id }}" data-content="{{ $topic->id }}" class="view-topic-modal">{{ $topic->title }}</a></td>
														<td>
															<a href="{{ url('training').'/'.$topic->id }}" data-content="{{ $topic->id }}" class="view-topic-modal">View</a>
															<div class="topic-progress" style="width: 150px;height: 17px;vertical-align: middle;">
																<div class="progress-bar" aria-valuetransitiongoal="{{ $topic->user_progress( Auth::user()->id ) }}" aria-valuenow="{{ $topic->user_progress( Auth::user()->id ) }}" style="width: {{ $topic->user_progress( Auth::user()->id ) }}%;">
																	<?= ( $topic->user_progress( Auth::user()->id  ) != 0 ) ? $topic->user_progress( Auth::user()->id  ).'%' : '' ?>
																</div>
															</div>
														</td>
													</tr>
												@endif
											@endforeach
										</tbody>
									</table>
							@else
								<p style="text-align:center">{{trans("knowledge-module.No Intenal Trainings")}}</p>
							@endif
							
							@if( strcasecmp( $company->company_name , 'ZenintraNet') == 0 || strcasecmp( $company->company_name , 'zenintranet') == 0)
							<br />
								@if( count($trainings_external) != 0 )
	                            	<h3>{{trans("knowledge-module.External Trainings")}}</h3>
										<table class="table table-hover">
											<thead>
												<tr>
													<th>Title</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@foreach($trainings_external as $topic)
													<tr>
														<td width="500"><a href="{{ url('tutorials').'/'.$topic->company_id.'/'.$topic->slug }}" data-content="{{ $topic->id }}" target="_blank" class="view-topic-modal">{{ $topic->title }}</a></td>
														<td>
															<a href="{{ url('tutorials').'/'.$topic->company_id.'/'.$topic->slug }}"  class="view-topic-modal" target="_blank">View</a>
														</td>
													</tr>
												@endforeach
											</tbody>
										</table>
								@else
									<p style="text-align:center">{{trans("knowledge-module.No External Trainings")}}</p>
								@endif
							@endif
						</div>
					</div>
				</div>
			</article>
		</div>
	</section>

@endsection

@section('page-script')

<script type="text/javascript" src="{{ url('/js/all-trainings.js') }} "></script>

@endsection