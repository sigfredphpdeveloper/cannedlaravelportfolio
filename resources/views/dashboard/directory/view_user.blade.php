@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<div class="content">
    <!-- row -->
    <div class="row">

        <div class="col-sm-12 col-md-12">
           <a href="{{ url('/directory') }}" class="btn btn-xs btn-default"><i class="fa fa-caret-left"></i> Back To Directory</a>

        </div>

        <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Warning.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <article class="col-sm-6 col-md-6 col-lg-6">

            <form  id="smart-form-register" class="smart-form client-form" role="form" method="POST" action="{{ url('/add_folder_save') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="company_id" value="{{ $company['id'] }}">

                <fieldset>
                    <h2>User Profile</h2>

                        <div class="col-md-4">
                            <b>First Name</b>
                        </div>

                        <div class="col-md-8">
                           {{$view_user->title}} {{$view_user->first_name}} &nbsp;
                        </div>

                        <div class="col-md-4">
                            <b>Last Name</b>
                        </div>

                        <div class="col-md-8">
                            {{$view_user->last_name}} &nbsp;
                        </div>

                        <div class="col-md-4">
                            <b>Email</b>
                        </div>

                        <div class="col-md-8">
                            {{$view_user->email}} &nbsp;
                        </div>

                        <div class="col-md-4">
                            <b>Phone</b>
                        </div>

                        <div class="col-md-8">
                            {{$view_user->phone_number}} &nbsp;
                        </div>

                        <?php if($is_admin):?>
                        <div class="col-md-4">
                            <b>Note</b>
                        </div>

                        <div class="col-md-8">
                            {{ $view_user->notes }} &nbsp;
                        </div>
                        <?php endif;?>

                       @foreach($user_fields as $field)

                            <div class="col-md-4">
                                <b>{{ ucwords(str_replace('_',' ',$field['field_name']))}}</b>
                            </div>

                            <?php
                                    $value = '';
                                    $field_name_key = str_replace(' ','_',$field['field_name']);

                                    if(array_key_exists($field_name_key,$custom_keypairs_array)){
                                        $value = $custom_keypairs_array[$field_name_key];
                                    }
                            ?>

                            <div class="col-md-8">
                                {{$value}}
                                &nbsp;
                            </div>

                        @endforeach


                </fieldset>

                <footer>

                    <a href="{{url('/directory')}}" class="btn btn-primary">Back</a>
                </footer>

            </form>

        </article>
    </div>
</div>
    <!-- end row -->

<!-- end widget grid -->


@endsection