@extends('dashboard.layouts.master')

@section('content')
<style type="text/css">
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
</style>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-md-12">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ Session::get('success') }}</p>
                </div>
            @endif
        </div>

        <div class="col-md-6">

            <h1>Import Users</h1>

        </div>
        <div class="col-md-6">



            <form action="{{ url('user_import_process') }}" method="POST" id="file_upload_form" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="user_id" value="{{ $user['id'] }}">

                <a href="{{url('/download_csv_users')}}" class="btn btn-primary">Download Sample CSV</a>

                <span class="btn btn-info btn-file" id="bulkupload" data-toggle="tooltip" data-placement="top" title="">
                    <input type="file" name="upload" id="csv_upload" multiple >Bulk Upload
                </span>
            </form>

        </div>
    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->

<script type="text/javascript">

    $(document).ready(function(){

        $('#csv_upload').on('change', function(){
            $('#file_upload_form').submit();
        })

    });

</script>


@endsection