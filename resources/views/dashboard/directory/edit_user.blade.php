@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->

    <div class="row">

            <div class="col-md-12">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ Session::get('success') }}</p>
                </div>
            @endif
            </div>

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>{{trans("directory_invite_user.Warning.")}}<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <article class="col-sm-6 col-md-6 col-lg-6">

                <form  id="smart-form-register" class="smart-form client-form" role="form" method="POST" action="{{ url('/directory_edit_user/'.$edit_user->id) }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <header>

                         {{trans("directory_invite_user.Edit User")}}

                    </header>

                    <fieldset>

                        <section>
                            <label class="label">{{trans("directory_invite_user.Email *")}}</label>
                            <label class="input">
                                <input type="email" name="email" value="{{$edit_user['email']}}" placeholder="{{trans("directory_invite_user.Enter Email")}}" />
                            </label>
                        </section>

                        <section>
                            <label class="label">{{trans("directory_invite_user.Title")}}</label>
                            <label class="input">
                                <input type="text" name="title" value="{{$edit_user['title']}}" placeholder="{{trans("directory_invite_user.Title")}}" />
                            </label>
                        </section>

                        <section>
                            <label class="label">{{trans("directory_invite_user.Employee First Name")}}</label>
                            <label class="input">
                                <input type="text" name="first_name" value="{{$edit_user['first_name']}}" />
                            </label>
                        </section>

                        <section>
                            <label class="label">{{trans("directory_invite_user.Employee Last Name")}}</label>
                            <label class="input">
                                <input type="text" name="last_name"  value="{{$edit_user['last_name']}}"/>
                            </label>
                        </section>


                        <?php if($assign_roles):?>
                        <section>
                            <label class="label">{{trans("directory_invite_user.Employee Roles")}}</label>
                            <label class="input">

                                <?php

                                    $user_roles = $edit_user->user_roles->toArray();
                                    $user_roles_arr = [];
                                    foreach($user_roles as $user_role){
                                        $user_roles_arr[] = $user_role['id'];
                                    }
                                ?>
                                <select name="roles[]" multiple class="form-control">
                                    @foreach($roles as $role)
                                        <option value="{{$role['id']}}" <?php echo in_array($role['id'],$user_roles_arr)?'selected':'';?>>
                                            {{$role['role_name']}}
                                        </option>
                                    @endforeach
                                </select>

                            </label>
                        </section>
                        <?php endif;?>

                        <?php if($assign_roles):?>
                        <section>
                            <label class="label">{{trans("directory_invite_user.Employee Teams")}}</label>
                            <label class="input">

                                <?php
                                    $user_teams = $edit_user->user_teams->toArray();
                                    $user_teams_arr = [];
                                    foreach($user_teams as $user_team){
                                        $user_teams_arr[] = $user_team['id'];
                                    }
                                ?>

                                <select name="teams[]" multiple class="form-control">

                                    @foreach($teams as $team)
                                        <option value="{{$team['id']}}" <?php echo in_array($team['id'],$user_teams_arr)?'selected':'';?>>
                                            {{$team['team_name']}}
                                        </option>
                                    @endforeach

                                </select>
                            </label>
                        </section>

                        <?php endif;?>

                        <?php if($is_admin):?>
                        <section>
                            <label class="label">{{ trans("directory_invite_user.Notes (visible by company administrators only)") }}
                            </label>
                            <label class="input">
                            <textarea name="notes" style="height:100px;" class="form-control">{{ $edit_user['notes'] }}</textarea>
                            </label>
                        </section>
                        <?php endif;?>

                    </fieldset>

                    <footer>
                        <button type="submit" class="btn btn-success">
                            {{trans("directory_invite_user.Update")}}
                        </button>
                        <a href="{{url('/directory')}}" class="btn btn-default">{{trans("directory_invite_user.Back")}}</a>
                    </footer>


                </form>

            </article>

    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->


@endsection