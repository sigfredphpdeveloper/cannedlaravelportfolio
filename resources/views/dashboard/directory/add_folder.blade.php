@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<div class="content">
    <!-- row -->
    <div class="row">

        <div class="col-sm-12 col-md-12">
            <h1>Add Folder</h1>
        </div>

        <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Warning.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <article class="col-sm-6 col-md-6 col-lg-6">

            <form  id="smart-form-register" class="smart-form client-form" role="form" method="POST" action="{{ url('/add_folder_save') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="company_id" value="{{ $company['id'] }}">
            <input type="hidden" name="parent_content_id" value="{{ $parent_content_id }}">


                <div class="row">
                    <div class="col-md-4">Folder Name</div>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="content_name">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">Set Permission</div>
                </div>

                <div class="row">
                    <div class="col-md-12">

                        <table class="table table-striped">
                            <tr>
                                <th>Role</th>
                                <th>Read</th>
                                <th>Write</th>
                                <th>Access</th>
                            </tr>

                            @foreach($roles as $role)

                                <tr>
                                <td>{{$role['role_name']}}</td>
                                    <td>
                                        <input type="checkbox" name="roles_read[]" value="{{ $role['id'] }}" />
                                    </td>
                                    <td>
                                        <input type="checkbox" name="roles_write[]"  value="{{ $role['id'] }}" />
                                    </td>
                                    <td>
                                        <input type="checkbox" name="roles_no_access[]" checked value="{{ $role['id'] }}" />
                                    </td>
                                </tr>

                            @endforeach
                        </table>

                    </div>
                </div>


                <footer>
                    <button type="submit" class="btn btn-success">
                        Save
                    </button>
                </footer>

            </form>

        </article>
    </div>
</div>
    <!-- end row -->

<!-- end widget grid -->


@endsection