@extends('dashboard.layouts.master')

@section('content')
<style>
.zero-padding{
    padding:0 !important;
}
</style>
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">
        <div class="col-md-12">
            <h1>{{trans("directory_manager.Employee Directory")}}</h1>
        </div>
        <div class="col-md-8">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ Session::get('success') }}</p>
                </div>
            @endif
        </div>
        <div class="col-xs-8 zero-padding">
            <div class="col-md-2">
                 <a href="{{url('/invite_user')}}" class="btn btn-primary" >{{trans("directory_manager.Add User")}}</a>
            </div>

            <div class="col-sm-2 pull-right">
                 <a href="{{url('/user_import')}}" class="btn btn-primary" >Import CSV</a>
            </div>
        </div>



    </div>

    <div class="row">
        <div class="col-md-8">

            <table class="table table-striped">
                <tbody><tr>
                    <th>Field Name</th>
                    <th>Description</th>
                    <th>Type</th>
                    <th>Visibility</th>
                    <th width="200"></th>
                </tr>

                            </tbody></table>

        </div>

    </div>

    <!-- end row -->

</section>




@endsection
