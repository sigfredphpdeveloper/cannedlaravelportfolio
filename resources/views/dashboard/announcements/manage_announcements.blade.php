@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-md-12">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ Session::get('success') }}</p>
                </div>
            @endif
        </div>

        <div class="col-md-6">
            <h1>{{trans("manage_announcements.Manage Announcements")}}</h1>
        </div>

        <div class="col-md-6">
            <a href="{{ url('/add_announcement') }}" class="btn btn-primary pull-right">{{trans("manage_announcements.Add Announcement")}}</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            <table class="table table-striped">
                <tr>
                    <th>{{trans("manage_announcements.Title")}}</th>
                    <th>{{trans("manage_announcements.Message")}}</th>
                    <th width="180">{{trans("manage_announcements.Posted Date")}}</th>
                    <th width="200"></th>
                </tr>

                @foreach($announcements as $i=>$item)
                <tr>
                    <td>{{$item->title}}</td>
                    <td>{{substr($item->message,0,50)}}...</td>
                    <td>{{$item->first_name}} {{$item->last_name}} - {{date('F d',strtotime($item->created_at))}}</td>
                    <td>
                        <a href="{{ url("announcement_edit/") }}/{{ $item->id }}" class="icon edit-icon">{{trans("manage_announcements.Edit")}}</a>
                        <a href="javascript:void(0);" data-id="{{ $item->id }}" class="deleter icon trash-icon">{{trans("manage_announcements.Delete")}}</a>
                    </td>
                </tr>
                @endforeach
            </table>

        </div>

    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->

<script type="text/javascript">

    $(document).ready(function(){

        $('.deleter').click(function(){

            var id = $(this).data('id');
            $('#delete_proceed').attr('alt',id);
            $('#confirm').modal('show');

        });

        $('#delete_proceed').click(function(){

            var id = $(this).attr('alt');

            $('#confirm').modal('hide');

            window.location.href='{{ url('announcement_delete') }}/'+id;

        });

    });

</script>

<div class="modal fade" tabindex="-1" role="dialog" id="confirm">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{trans("manage_announcements.Confirmation")}}</h4>
      </div>
      <div class="modal-body">
        <p>{{trans("manage_announcements.Are you sure you want to delete this announcement ?")}}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("manage_announcements.No")}}</button>
       <button type="button" class="btn btn-primary" id="delete_proceed" alt="">{{trans("manage_announcements.Yes")}}</button>

      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@endsection