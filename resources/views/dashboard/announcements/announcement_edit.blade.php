@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-sm-12 col-md-12">
        <h1>{{trans("announcement_add.Edit Announcement")}}</h1>
        </div>

   <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>{{trans("announcement_add.Warning.")}}<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <article class="col-sm-6 col-md-6 col-lg-6">


            <form method="POST" action="{{url('/announcement_edit/')}}/{{$announcement['id']}}" accept-charset="UTF-8" id="smart-form-register"
             class="smart-form client-form" enctype="multipart/form-data">


            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="user_id" value="{{ $user['id'] }}">
            <input type="hidden" name="company_id" value="{{ $company['id'] }}">

                <fieldset>

                     <section>
                        <label class="label">{{trans("announcement_add.Title *")}}</label>
                        <label class="input">
                        <input type="text" name="title" placeholder="{{trans("announcement_add.Title")}}" value="{{$announcement['title']}}" required maxlength="50"/>

                        </label>
                    </section>

                    <section>
                        <label class="label">{{trans("announcement_add.Message *")}}</label>
                        <label class="input">
                            <textarea  name="message" class="form-control" placeholder="{{trans("announcement_add.Message")}}" required style="height:230px;" >{{$announcement['message']}}</textarea>
                        </label>
                    </section>

                     <section>
                        <label class="label">{{trans("announcement_add.Upload Picture")}}</label>

                        <br />

                        <!--

                        <label class="input">

                            <img src="{{$announcement['picture']}}" />

                            <br/>

                            {!! Form::file('image', null) !!}

                        </label>
                    -->


                    </section>

                    <section>
                        <label class="label">{{trans("announcement_add.Permissions")}}</label>


                        <label>
                            <input type="radio" name="permission" class="radio_permissions" <?php echo ($announcement['permission']=='all')?'checked':'';?> value="all" /> {{trans("announcement_add.Visible By Everyone")}} <br />
                        </label>
                        <br />

                        <label>
                            <input type="radio" name="permission" class="radio_permissions" <?php echo ($announcement['permission']=='roles')?'checked':'';?> value="roles"> {{trans("announcement_add.Visible only to some roles")}}<br />
                        </label>
                        <br />

                         <label>
                            <input type="radio" name="permission" class="radio_permissions" <?php echo ($announcement['permission']=='teams')?'checked':'';?> value="teams"> {{trans("announcement_add.Visible only to some teams")}}
                         </label>
                    </section>

                    <section style="display:none;" id="employee_roles">
                        <label class="label">{{trans("announcement_add.Employee Roles")}}</label>
                        <label class="input">
                            <?php $saved_roles = $announcement['roles'];?>

                            <select name="roles[]" multiple class="form-control">

                                @foreach($roles as $role)
                                    <option value="{{$role['id']}}" <?php echo (in_array($role['id'],$saved_roles))?'selected':'';?>>{{$role['role_name']}}</option>
                                @endforeach

                            </select>

                        </label>
                    </section>

                    <section style="display:none;" id="employee_teams">
                        <label class="label">{{trans("announcement_add.Employee Teams")}}</label>
                        <label class="input">
                            <select name="teams[]" multiple class="form-control">
                                <?php $saved_teams = $announcement['teams'];?>
                                @foreach($teams as $team)
                                    <option value="{{$team['id']}}" <?php echo (in_array($team['id'],$saved_teams))?'selected':'';?>>{{$team['team_name']}}</option>
                                @endforeach
                            </select>
                        </label>
                    </section>

                </fieldset>

                <footer>

                    <button type="submit" class="btn btn-primary">
                        {{trans("announcement_add.Save")}}
                    </button>
                    <a href="{{url('/announcements')}}" class="btn btn-default">{{trans("announcement_add.Back")}}</a>
                </footer>

            </form>

        </article>
    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->

<script type="text/javascript">
$(document).ready(function(){

    $('.radio_permissions').change(function(){

            var value = $(this).val();

            if(value=='all'){
                $('#employee_roles').hide();
                $('#employee_teams').hide();
            }
            else if(value=='roles'){
                $('#employee_roles').show();
                $('#employee_teams').hide();
            }else if(value=='teams'){
                $('#employee_roles').hide();
                $('#employee_teams').show();
            }


    });

        <?php if ($announcement['permission']=='roles') :?>
            $('#employee_roles').show();
        <?php endif;?>

        <?php if ($announcement['permission']=='teams') :?>
            $('#employee_teams').show();
        <?php endif;?>

});
</script>

@endsection