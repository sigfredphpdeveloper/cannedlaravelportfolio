@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->

    <!-- Announcements -->
    <div class="row announcements-row">

        <div class="col-md-6">
          <h1>{{trans("dashboard_layouts_nav.Announcements")}}</h1>
        </div>

        <div class="col-md-6 pull-right">

          @if($manage_announcement)
            <a href="{{ url('/add_announcement') }}" class="btn btn-primary pull-right">Add Announcement</a>
            <a href="{{ url('/announcements') }}" class="btn btn-primary pull-right" style="margin-right:5px;">Manage Announcements</a>
          @endif
        </div>

        <br style="clear:both;">


        @foreach($announcement_array as $announcement)

        <div class="row">
            <div class="col-md-1 ">

                <?php if($announcement['title'] == 'Welcome to Zenintra'):?>

<a href="{{ url('/announcement') }}/{{$announcement['id']}}" style="margin:4px;float:right;">
                        <div class="circle-div" style="
                        background:url('{{ url('/img') }}/zen.jpg') no-repeat;
                        background-position: center;
                        background-size:47px 24px;
                        width:50px;height:50px;
                        ">
                        </div>
                    </a>
                <?php elseif($announcement['photo'] != ''):?>
<a href="{{ url('/announcement') }}/{{$announcement['id']}}" style="margin:4px;float:right;">
                        <div class="circle-div" style="
                        background:url('{{$announcement['photo']}}') no-repeat;
                        background-position: center;
                        background-size:50px 50px;
                        width:50px;height:50px;
                        ">
                        </div>
                    </a>

                <?php endif; ?>



            </div>
            <div class="col-md-11">
                <h2 style="margin:0;font-weight:bold;">{{$announcement['title']}}</h2>
                <i style="color:#827D7D;">
                {{$announcement['first_name']}} {{$announcement['last_name']}} -
                {{ date('M d',strtotime($announcement['created_at']))}}</i>

                <p>
                    <?php if(strlen($announcement['message'])>100):?>
                    <p>{!!substr(nl2br($announcement['message']),0,100)!!}...</p>
                    <p style="display:none;">{!!(nl2br($announcement['message']))!!}...</p>
                    <a style="clear:both;margin-top:12px;" href="javascript:void(0);" class="view_more">{{trans("manage_announcements.View More...")}} </a>
                    <?php else:?>
                    {!!nl2br($announcement['message'])!!}
                    <?php endif;?>
                </p>
            </div>
        </div>
        <hr />
        @endforeach


    </div>
    <!-- end row -->

</section>
<!-- end widget grid -->
<script type="text/javascript">
    $(document).ready(function(){

        $('.view_more').click(function(){

            $(this).hide();
            $(this).prev().prev().hide();
            $(this).prev().fadeIn();

        });

    });
</script>

@endsection