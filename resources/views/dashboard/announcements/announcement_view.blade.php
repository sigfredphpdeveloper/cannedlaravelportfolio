@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-sm-12 col-md-12">
        <h1>{{$announcement['title']}}</h1>
        <i>{{ date('M d - H:i',strtotime($announcement['created_at']))}}</i>

        <br />
        </div>

        @if($announcement['picture']!='')
        <!--
        <div class="col-sm-12 col-md-12">
            <img src="{{$announcement['picture']}}"  class="img-responsive"/>
        </div>
        -->
        @endif

        <div class="col-sm-12 col-md-12">
        <br />
            <p>
            {!!nl2br($announcement['message'])!!}
            </p>
        </div>

</div>

<!-- end row -->

</section>
<!-- end widget grid -->

<script type="text/javascript">
$(document).ready(function(){

    $('.radio_permissions').change(function(){

            var value = $(this).val();

            if(value=='all'){
                $('#employee_roles').hide();
                $('#employee_teams').hide();
            }
            else if(value=='roles'){
                $('#employee_roles').show();
                $('#employee_teams').hide();
            }else if(value=='teams'){
                $('#employee_roles').hide();
                $('#employee_teams').show();
            }


    });

        <?php if ($announcement['permission']=='roles') :?>
            $('#employee_roles').show();
        <?php endif;?>

        <?php if ($announcement['permission']=='teams') :?>
            $('#employee_teams').show();
        <?php endif;?>

});
</script>

@endsection