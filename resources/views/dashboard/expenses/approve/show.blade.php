@extends('dashboard.layouts.master')

@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
			<h1 class="page-title txt-color-blueDark">
				<i class="fa fa-money fa-fw "></i> 
					<a href="{{ url('/approve-expenses') }}"> {{trans("expenses_approve_show.Approve Expenses")}} </a> <span> > {{ $expense_item->title }}</span>
			</h1>
		</div>
	</div>

	@if(Session::has('success'))
		<div class="alert alert-block alert-success">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('success') }}</h4>
		</div>
	@elseif(Session::has('error'))
		 <div class="alert alert-block alert-danger">
             <a class="close" data-dismiss="alert" href="#">×</a>
             <h4 class="alert-heading"><i class="fa-fw fa fa-times"></i> {{ Session::get('error') }}</h4>
         </div>
	@endif
	<div class="row">
		<div class="col-sm-12">
			<div class="well well-sm">
				<div class="container-fluid">
					<div class="col-sm-12 col-md-12 col-lg-6">
						<h1>{{ $expense_item->title }}</h1>
						<p>{{ $expense_item->description }}</p>
						@if( $expense_item->request_details != '' )
							<hr/>
							<div>
								<h4>{{trans("expenses_approve_show.Request Details")}}</h4>
								<p>{{$expense_item->request_details}}</p>
							</div>
						@endif
					</div>
					<div class="col-sm-12 col-md-12 col-lg-6">
						<div class="row" style="text-align:right">
							<form id="status-form" class="form-inline" action="{{ url('approve-expenses-item').'/'.$expense_item->id }}" method="POST">
								<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
								<fieldset>
									<div class="form-group">
										<select class="form-control status-dropdown" name="status" style="margin-right:5px;" <?=($expense_item->status=="approved")?'disabled="disabled"':'';?> >
											<option value=""> {{trans("expenses_approve_show.Update Status")}} </option>
											<option value="pending" <?php echo ($expense_item->status == 'pending') ? 'selected' : ''; ?>>{{trans("expenses_approve_show.Pending")}}</option>
											<option value="approved" <?php echo ($expense_item->status == 'approved') ? 'selected' : ''; ?>>{{trans("expenses_approve_show.Approve")}}</option>
											<option value="rejected" <?php echo ($expense_item->status == 'rejected') ? 'selected' : ''; ?>>{{trans("expenses_approve_show.Reject")}}</option>
											<option value="request details" <?php echo ($expense_item->status == 'request details') ? 'selected' : ''; ?>>{{trans("expenses_approve_show.Request Details")}}</option>
											<option value="cancelled" <?php echo ($expense_item->status == 'cancelled') ? 'selected' : ''; ?>>{{trans("expenses_approve_show.Cancel")}}</option>
										</select> 
									</div>
									<div class="form-group">
										<input type="hidden" name="request_details_content" value="" class="request-details-value" />
									</div>
									<button type="submit" style="margin-left:5px" class="btn btn-primary" value="Update"> {{trans("expenses_approve_show.Update")}}</button>
								</fieldset>
							</form>
						</div>
						<hr />
						<div class="row">
							<div class="col-xs-6 col-sm-4 col-md-4">
								<h5> {{trans("expenses_approve_show.Total Amount")}} <br />
									<span class="txt-color-blue">{{$company['expenses_currency']." ".$expense_item->total_amount}}</span>
								</h5>
							</div>
							<div class="col-xs-6 col-sm-4 col-md-4">
								<h5> {{trans("expenses_approve_show.Status")}} <br />
									<span class="txt-color-blue" style="text-transform:capitalize">{{$expense_item->status}}</span>
								</h5>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-6 col-sm-4 col-md-4">
								<h5> {{trans("expenses_approve_show.Creation Date")}} <br />
									<span class="txt-color-blue" style="text-transform:capitalize">{{$expense_item->creation_date}}</span>
								</h5>
							</div>
							<div class="col-xs-6 col-sm-4 col-md-4">
								<h5> {{trans("expenses_approve_show.Paid Date")}} <br />
									<span class="txt-color-blue" style="text-transform:capitalize">{{$expense_item->paid_date}}</span>
								</h5>
							</div>
							<div class="col-xs-6 col-sm-4 col-md-4">
								<h5> {{trans("expenses_approve_show.Approval Date")}} <br />
									<span class="txt-color-blue" style="text-transform:capitalize">{{$expense_item->approval_date}}</span>
								</h5>
							</div>
						</div>
					</div>				
				</div>
				<hr />
				<div class="container-fluid">
                     @if( count($expenses_line) != 0 )
						@foreach( $expenses_line as $expenses_line)
							<div class="well">
								<div class="row">
									<div class="container-fluid">
										<h3>#{{$expenses_line->id}} - {{$expenses_line->title}} / Expense Type - {{ $expenses_line->expenses_types->title }}</h3>
										<?php
											$values = unserialize($expenses_line->value);
										?>
										@foreach( $values as $key => $value )
											<div>
												@if(filter_var( str_replace( ' ', '%', $value), FILTER_VALIDATE_URL))
													<span style="text-transform:capitalize">{{ $key }}</span> : <a href="{{ $value }}" target="_blank" download><i class="fa fa-file-text-o" aria-hidden="true"></i></a>
												@else
													<span style="text-transform:capitalize">{{ $key }}</span> : {{ $value }}
												@endif
											</div>
										@endforeach
									</div>
								</div>
							</div>
						@endforeach
					@else
						<p style="text-align:center">{{trans("expenses_approve_show.No Expenses Line")}}</p>
					@endif
				</div>
			</div>
		</div>
	</div>

<div class="modal fade" tabindex="-1" role="dialog" id="request-details-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{trans("expenses_approve_show.Add Request Details Text")}}</h4>
      </div>
      <div class="modal-body">
        <form class="smart-form">
        	<section>
				<label class="label">{{trans("expenses_approve_show.Details")}}</label>
				<label class="textarea">
					<textarea rows="6" name="request-details-value" id="request_details_textarea" class="custom-scroll details-content"></textarea>
				</label>
			</section>
        </form>
      </div>
      <div class="modal-footer">
       <button type="button" id="send_request_details" class="btn btn-primary save-details" alt="">{{trans("expenses_approve_show.Save")}}</button>

      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@section('page-script')

<script type="text/javascript" src="{{ url('/js/show-expense.js') }}"></script>

<script type="text/javascript">
	$(document).ready(function(){
		var $statusForm = $('#status-form').validate({
			rules : {
				status : {
					required : true
				}
			}
		});

		$('.status-dropdown').on('change', function(){
			var value = $(this).val();
			if( value == 'request details')
			{
				$('#request-details-modal').modal('show');
			}
		});

		$('.save-details').on('click', function(){
			var content = $('.details-content').val();
			$('.request-details-value').val(content);
			$('#request-details-modal').modal('hide');

		})

        {{--$('#send_request_details').on('click', function(){--}}
            {{--var request_details_textarea = $('#request_details_textarea').val();--}}

            {{--$.ajax({--}}
                {{--url : '{{url('approve-expenses-item')}}/{{$id}}',--}}
                {{--type : 'POST',--}}
                {{--data : {'request_details_content':request_details_textarea, 'status':'request details',  '_token':'{{ csrf_token() }}'},--}}
                {{--dataType:'json',--}}
                {{--success:function(response){--}}
                    {{--location.reload();--}}
                {{--}--}}
            {{--})--}}
        {{--})--}}


	});
</script>

@endsection