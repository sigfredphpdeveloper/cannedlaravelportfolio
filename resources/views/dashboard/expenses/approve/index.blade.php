@extends('dashboard.layouts.master')

@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
			<h1 class="page-title txt-color-blueDark">
				<i class="fa fa-money fa-fw "></i> 
					{{trans("expenses_approve_index.Approve Expenses")}} 
			</h1>
		</div>
		@if( $is_admin )
		   <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4 pull-right text-right">
				<h3 class="page-title txt-color-blueDark">
						<a href="{{ url('/expenses-setting') }}" class="btn btn-primary">{{trans("expenses_approve_index.Customize")}}</a>
				</h3>
			</div>
	    @endif
	</div>
	@if(Session::has('success'))
		<div class="alert alert-block alert-success">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('success') }}</h4>
		</div>
	@elseif(Session::has('error'))
		<div class="alert alert-block alert-danger">
            <a class="close" data-dismiss="alert" href="#">×</a>
            <h4 class="alert-heading"><i class="fa-fw fa fa-times"></i> {{ Session::get('error') }}</h4>
        </div>
	@endif
	<!-- widget grid -->
	<section id="widget-grid" class="">
		<!-- row -->
		<div class="row">
			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-togglebutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2> {{trans("expenses_approve_index.All Company Expenses")}} </h2>
					</header>
					<div>
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<div class="widget-body no-padding">
							<table id="approve-expenses-table" class="table table-striped table-bordered" width="100%">
							<thead>
								<tr style="background-color: transparent !important; background-image: none !important;">
									<th class="hasinput" style="">
										<input type="text" class="form-control" placeholder="Filter ID" />
									</th>
									<th class="hasinput hidden-xs">
										<input id="submitted_date" type="text" placeholder="Filter Date" class="form-control datepicker" data-dateformat="yy-mm-dd">
									</th>
									<th class="hasinput hidden-xs" style="">
										<input type="text" class="form-control" placeholder="Filter Name" />
									</th>
									<th class="hasinput" style="">
										<input type="text" class="form-control" placeholder="Filter Title" />
									</th>
									<th class="hasinput" style="">
										<input type="text" class="form-control" placeholder="Filter Amount" />
									</th>
									<th class="hasinput">
										<input type="text" class="form-control" placeholder="Filter Status" />
									</th>
									<th></th>
								</tr>
								<tr>
									<td>{{trans("expenses_approve_index.Owner")}}</td>
									<td class="hidden-xs">{{trans("expenses_approve_index.Submision Date")}}</td>
									<td class="hidden-xs">{{trans("expenses_approve_index.Expense ID")}}</td>
									<td>{{trans("expenses_approve_index.Title")}}</td>
									<td>{{trans("expenses_approve_index.Total Amount")}}</td>
									<td>{{trans("expenses_approve_index.Status")}}</td>
									<td>{{trans("expenses_approve_index.Action")}}</td>
								</tr>
							</thead>
							<tbody>
								@foreach( $expenses as $expense )
									<tr>
										<td>{{$expense->user->first_name}} {{$expense->user->last_name}}</td>
										<td class="hidden-xs"> {{$expense->creation_date}} </td>
										<td class="hidden-xs"> {{$expense->id}} </td>
										<td> {{$expense->title}} </td>
										<td> {{$company['expenses_currency']." ".$expense->total_amount}} </td>
										<td> {{$expense->status}}</td>
										<td width="100">
												  <button type="button" data-id="{{$expense->id}}" class="btn btn-success btn-sm approve_expense">Approve</button>
												  <a href="{{ url('/approve-expenses-item') }}/{{ $expense->id }}" class="view-expense-modal"><i class="icon view-icon"></i></a>
                                              </td>
									</tr>
								@endforeach
							</tbod
						</table>
					</div>
				</div>
			</article>
		</div>
	</section>

<div class="modal fade confirmation_modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="mySmallModalLabel">Approve expenses</h4>
            </div>
            <form id="confirmation_form" action="{{url('approve-this-expense')}}" method="POST">
                <div class="modal-body">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" id="approve-expense-id" value="">
                    Please confirm you approve this Claim.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Yes</button>
                </div>
            </form>
        </div>
    </div>
</div>
	
@endsection

@section('page-script')

<script type="text/javascript" src="{{ url('/js/my-expenses.js') }}"></script>
<script type="text/javascript" src="{{ url('/js/my-expenses.js') }}"></script>
<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{ url('js/plugin/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('js/plugin/datatables/dataTables.colVis.min.js') }}"></script>
<script src="{{ url('js/plugin/datatables/dataTables.tableTools.min.js') }}"></script>
<script src="{{ url('js/plugin/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ url('js/plugin/datatable-responsive/datatables.responsive.min.js') }}"></script>


<script>
$(document).ready(function(){
	pageSetUp();

    $('.approve_expense').on('click', function(){
        var id = $(this).data('id');
        $('#approve-expense-id').val(id);
        $('.confirmation_modal').modal('show');
    });

    var responsiveHelper_datatable_fixed_column = undefined;
	var breakpointDefinition = {
				tablet : 1024,
				phone : 480
			};
	var otable = $('#approve-expenses-table').DataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
					"t"+
					"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"oTableTools": {
		        "aButtons": [
		           "copy",
		           "csv",
		           "xls",
		              {
		                  "sExtends": "pdf",
		                  "sTitle": "Zenitranet_PDF",
		                  "sPdfMessage": "Zenintranet PDF Export",
		                  "sPdfSize": "letter"
		              },
		            {
	                   "sExtends": "print",
	                   "sMessage": "Generated by Zenintranet <i>(press Esc to close)</i>"
	               }
		           ],
		          "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
		      },
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_fixed_column) {
				responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#approve-expenses-table'),breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
					responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_fixed_column.respond();
		}
	});

	// Apply the filter
	$("#approve-expenses-table thead th input[type=text]").on( 'keyup change', function () {
		   otable
		       .column( $(this).parent().index()+':visible' )
		       .search( this.value )
		       .draw();
	} );


})
</script>

@endsection
