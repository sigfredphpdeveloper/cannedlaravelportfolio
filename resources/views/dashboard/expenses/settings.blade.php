@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-md-12">
        <a href="{{ url('/admin_settings') }}" class="btn btn-xs btn-default"><i class="fa fa-caret-left"></i> Back To Settings</a>

            @if(Session::has('success'))
                <div class="alert alert-block alert-success">
                    <a class="close" data-dismiss="alert" href="#">×</a>
                    <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('success') }}</h4>
                </div>
            @elseif(Session::has('error'))
                 <div class="alert alert-block alert-danger">
                    <a class="close" data-dismiss="alert" href="#">×</a>
                    <h4 class="alert-heading"><i class="fa-fw fa fa-times"></i> {{ Session::get('error') }}</h4>
                </div>
            @elseif(Session::has('max-error'))
                <div class="alert alert-block alert-danger">
                    <a class="close" data-dismiss="alert" href="#">×</a>
                    <h4 class="alert-heading"><i class="fa-fw fa fa-times"></i> {{ Session::get('max-error') }}</h4>
                </div>
            @endif
        </div>

        <article class="col-sm-6 col-md-6 col-lg-6">

            <form  id="smart-form-register" class="smart-form client-form" role="form" 
            method="POST" action="{{ url('/expenses-settings-save') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="user_id" value="{{ $user['id'] }}">

                <fieldset>
                    <section>
                        <label class="label">{{trans("expenses_settings.Currency Symbol")}}</label>
                        <label class="input">
                            <input type="text" name="curreny" value="{{ $company['expenses_currency'] }}">
                        </label>
                    </section>
		            <section> 
			    		<section class="col col-5">
        			         <label class="toggle">
        			               <input type="checkbox" name="expenses"
        			               value="1" <?php echo ($company['expenses']=='1')?'checked="checked"':'';?> >
        			               <i data-swchon-text="ON" data-swchoff-text="OFF"></i> {{trans("expenses_settings.Expenses")}}
                                </label>
			             </section>    
		            </section>
                </fieldset>
		           
                <footer>
                    <button type="submit" class="btn btn-primary">
                        {{trans("expenses_settings.Save")}}
                    </button>
                </footer>


            </form>

        </article>

    </div>
    <!-- end row -->
    <br />
    @if( $manage_expenses AND isset($company) AND $company['expenses']==1 )

    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-6">
            <div class="well">
                <h3>{{trans("expenses_settings.Expenses Type")}}</h3>
                <p style="text-align:right">
                    <a href="javascript:void(0);" data-toggle="tooltip" title="Add Expense Type" id="btn_add_expense_type" class="btn btn-primary"> {{trans("expenses_settings.Add New")}}</a>
                </p>

                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th> {{trans("expenses_settings.ID")}} </th>
                            <th> {{trans("expenses_settings.Title")}} </th>
                            <th> {{trans("expenses_settings.Date Created")}} </th>
                                <th> {{trans("expenses_settings.Action")}} </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach( $expenses_types as $expense_type )
                        <tr>
                            <td>{{ $expense_type->id }}</td>
                            <td>{{ $expense_type->title }}</td>
                            <td>{{ $expense_type->created_at }}</td>
                            <td>
                                <div class="input-group-btn" style="width: auto;">
                                    <button type="button" class="btn btn-default" tabindex="-1">{{trans("expenses_settings.Action")}}</button>
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1" aria-expanded="false">
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        {{--<li><a href="javascript:void(0);" data-content="{{ $expense_type->id }}" class="manage-fields-modal">Approve</a></li>--}}
                                        {{--<li class="divider"></li>--}}

                                        <li><a href="javascript:void(0);" data-content="{{ $expense_type->id }}" class="manage-fields-modal">{{trans("expenses_settings.Manage fields")}}</a></li>
                                        <li class="divider"></li>
                                        <li><a href="javascript:void(0);" data-content="{{ $expense_type->id }}" class="edit-expense-modal">{{trans("expenses_settings.Edit")}}</a></li>
                                        <li class="divider"></li>
                                        <li><a href="javascript:void(0);" data-content="{{ $expense_type->id }}" class="delete-expense-modal">{{trans("expenses_settings.Delete")}}</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-6">
            <div class="well">
                <h3>{{trans("expenses_settings.Expenses Roles Permision")}}</h3>
                <div class="alert alert-info no-margin fade in">
                    <button class="close" data-dismiss="alert">
                                ×
                    </button>
                    <i class="fa-fw fa fa-info"></i>
                    {{trans("expenses_settings.By default user can only see his/her expenses.")}}
                </div>
                <!-- <br /> -->
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th> {{trans("expenses_settings.Role")}} </th>
                            <th> {{trans("expenses_settings.Approve")}} </th>
                            <th> {{trans("expenses_settings.Delete")}} </th>
                            <th> {{trans("expenses_settings.Pay")}} </th>
                            <th> {{trans("expenses_settings.View All")}} </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach( $company_roles as $role )
                            <tr>
                                <?php
                                    $approve = '';
                                    $delete = '';
                                    $pay = '';
                                    $view_all = '';
                                ?>
                                @foreach( $role->expenses_roles as $expenses_role )
                                    <?php
                                    $approve = $expenses_role->approve;
                                    $delete = $expenses_role->delete;
                                    $pay = $expenses_role->pay;
                                    $view_all = $expenses_role->view_all;
                                    ?>
                                    @endforeach 
                                <td>{{ $role->role_name }}</td>
                                <td> <input type="checkbox" data-action="approve" data-id="{{ $role->id }}" name="approve" id="approve-check" class="permision-toggle" 
                                    <?php echo ( $approve == 1 ) ? 'checked' : ''; ?> > 
                                </td>
                                <td> <input type="checkbox" data-action="delete" data-id="{{ $role->id }}" name="approve" id="delete-check" class="permision-toggle"
                                    <?php echo ( $delete == 1 ) ? 'checked' : ''; ?> > 
                                </td>
                                <td> <input type="checkbox" data-action="pay" data-id="{{ $role->id }}" name="approve" id="pay-check" class="permision-toggle"
                                    <?php echo ( $pay == 1 ) ? 'checked' : ''; ?> > 
                                </td>
                                <td> <input type="checkbox" data-action="view-all" data-id="{{ $role->id }}" name="approve" id="view-all-check" class="permision-toggle"
                                    <?php echo ( $view_all == 1 ) ? 'checked' : ''; ?> > 
                                </td>       
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>



    </div>
    
    @endif


    <div class="row">
        <div class="col-md-6">

        <div class="well">

            <form action="{{url('/save_role_permission_expense')}}" method="POST" >
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <h1>Manage Expenses</h1>

                        <?php
                            $count = count($permissions);
                        ?>

                        @include('dashboard.layouts.permission_select')


                        <input type="submit" value="{{trans("manage_permissions.Save")}}" class="btn btn-primary" data-toggle="tooltip" title="Save" />

                </form>
            </div>
        </div>
    </div>
</section>

@endsection

@section('page-script')

<script type="text/javascript" src="{{ url('/js/expenses-settings.js') }}"></script>

@endsection