@extends('dashboard.layouts.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('js/plugin/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
			<h1 class="page-title txt-color-blueDark">
				<i class="fa fa-money fa-fw "></i> 
					{{trans("expenses_reports.Expenses Reports")}} 
			</h1>
		</div>
		@if( $is_admin )
		   <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4 pull-right text-right">
				<h3 class="page-title txt-color-blueDark">
						<a href="{{ url('/expenses-setting') }}" class="btn btn-primary">{{trans("expenses_reports.Customize")}}</a>
				</h3>
			</div>
	    @endif
	</div>
	@if(Session::has('success'))
		<div class="alert alert-block alert-success">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('success') }}</h4>
		</div>
	@elseif(Session::has('error'))
		<div class="alert alert-block alert-danger">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('error') }}</h4>
		</div>
	@endif

	<section id="widget-grid" class="">
		<div class="row">
			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget" id="wid-id-1" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-togglebutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
					<h2>{{trans("expenses_reports.Expenses Status Graph")}}</h2>
					</header>
					<div>
						<div class="jarviswidget-editbox"></div>
						<div class="widget-body no-padding">
							<div class="" style="padding: 16px 28px 0;">
								<div class="col-md-4"></div>
								<div class="col-md-4"></div>
								<div class="input-group date col-md-4">
	                                <input type="text" id="expense_dates" name="expense_dates" class="form-control" value="" placeholder="Date Range">
	                                <span class="input-group-addon">
	                                    <span class="fa fa-calendar"></span>
	                                </span>
	                            </div>
							</div>
							<div id="bar-graph" class="chart no-padding"></div>
							<div id="bar-graph-no-data" class="alert alert-info" style="margin: 27px;display:none"></div>
						</div>
					</div>
				</div>
			</article>

			<article class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget" id="wid-id-2" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-togglebutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-bar-chart-o"></i> </span>
					<h2>{{trans("expenses_reports.Expenses by Expense Types")}}</h2>
					</header>
					<div>
						<div class="jarviswidget-editbox"></div>
						<div class="widget-body no-padding">
							<div class="" style="padding: 16px 28px 0;">
								<div class="col-md-4"></div>
								<div class="col-md-4"></div>
								<div class="input-group date col-md-4">
	                                <input type="text" id="expense_type_dates" name="expense_type_dates" class="form-control" value="" placeholder="Date Range">
	                                <span class="input-group-addon">
	                                    <span class="fa fa-calendar"></span>
	                                </span>
	                            </div>
							</div>
							<div id="expenses_total_graph" class="chart no-padding"></div>
							<div id="expenses_total_graph-no-data" class="alert alert-info" style="margin: 27px;display:none"></div>
						</div>
					</div>
				</div>
			</article>
		</div>
	</section>

	<!-- widget grid -->
	<section id="widget-grid" class="">
		<!-- row -->
		<div class="row">
			<div class="col-sm-12">
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget" id="wid-id-3" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-togglebutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>{{trans("expenses_reports.All Company Expenses")}}</h2>
					</header>
					<div>
						<div class="jarviswidget-editbox"></div>
						<div class="widget-body no-padding">
							<table id="all_expenses_table" class="table table-striped table-bordered" width="100%">
							<thead>
								<tr style="background-color: transparent !important; background-image: none !important;">
									<th class="hasinput" style="">
										<input type="text" class="form-control" placeholder="Filter ID" />
									</th>
									<th class="hasinput">
										<input id="submitted_date" type="text" placeholder="Filter Date" class="form-control datepicker" data-dateformat="yy-mm-dd">
									</th>
									<th class="hasinput" style="">
										<input type="text" class="form-control" placeholder="Filter Name" />
									</th>
									<th class="hasinput" style="">
										<input type="text" class="form-control" placeholder="Filter Title" />
									</th>
									<th class="hasinput" style="">
										<input type="text" class="form-control" placeholder="Filter Status" />
									</th>
									<th class="hasinput" style="">
										<input type="text" class="form-control" placeholder="Filter Amount" />
									</th>
									<th class="hasinput">
										<input id="approval_date" type="text" placeholder="Filter Date" class="form-control datepicker" data-dateformat="yy-mm-dd">
									</th>
									<th class="hasinput">
										<input id="paid_date" type="text" placeholder="Filter Date" class="form-control datepicker" data-dateformat="yy-mm-dd">
									</th>
								</tr>
								<tr>
									<th>{{trans("expenses_reports.ID")}}</th>
									<th>{{trans("expenses_reports.Submitted Date")}}</th>
									<th>{{trans("expenses_reports.Employee Name")}}</th>
									<th>{{trans("expenses_reports.Title")}}</th>
									<th>{{trans("expenses_reports.Status")}}</th>
									<th>Amount</th>
									<th>{{trans("expenses_reports.Approval Date")}}</th>
									<th>{{trans("expenses_reports.Paid Date")}}</th>
								</tr>
							</thead>
						</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
@endsection

@section('page-script')

<script type="text/javascript" src="{{ url('/js/my-expenses.js') }}"></script>
<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{ url('js/plugin/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('js/plugin/datatables/dataTables.colVis.min.js') }}"></script>
<script src="{{ url('js/plugin/datatables/dataTables.tableTools.min.js') }}"></script>
<script src="{{ url('js/plugin/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ url('js/plugin/datatable-responsive/datatables.responsive.min.js') }}"></script>

<!-- JARVIS WIDGETS -->
<script src="{{url('js/smartwidgets/jarvis.widget.min.js')}}"></script>

<!-- Morris Chart Dependencies -->
<script src="{{ url('js/plugin/morris/raphael.min.js') }}"></script>
<script src="{{ url('js/plugin/morris/morris.min.js') }}"></script>
<script src="{{ url('js/plugin/morris/morris.min.js') }}"></script>
<script src="{{ asset('js/plugin/bootstrap-daterangepicker/daterangepicker.js') }}"></script>

<script type="text/javascript">
	$(document).ready(function(){
		pageSetUp();

		var data = JSON.parse(status);
		expensesStatusChart(data);

		var expenses_types_data = JSON.parse(expense_types);
		expensesTypesChart(expenses_types_data);
		
		function expensesStatusChart(data){
			var chart =	Morris.Bar({
			element : 'bar-graph',
			data : data,
			barColors: function(row, series, type) {
				  if(row.label == 'Pending'){ return '#dfb56c' }
				  else if(row.label == 'Approved'){ return '#8ac38b' }
				  else if(row.label == 'Rejected'){ return '#953b39' }
				  else if(row.label == 'Request details'){ return '#9cb4c5' }
				  else if(row.label == 'Cancelled'){ return '#c26565' }
				  else if(row.label == 'Paid'){ return '#356635' }	  	
				  else { return "#57889C" }
				},
				xkey : 'y',
				ykeys: ['x'],
	        	labels: ['Total'],
	        	xLabelMargin: 10,
	        	resize: true
			});
		}

		function expensesTypesChart(data){
			var expenses_type_graph =	Morris.Bar({
				element : 'expenses_total_graph',
				data : expenses_types_data,
				xkey : 'y',
				ykeys: ['x'],
	        	labels: ['Total'],
	        	xLabelMargin: 10
			});
		}

		var responsiveHelper_datatable_fixed_column = undefined;
		var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
		var otable = $('#all_expenses_table').DataTable({
			data: expenses_array,
			columns: [
		        { data: 'id' },
		        { data: 'submitted_date' },
		        { data: 'employee' },
		        { data: 'title' },
		        { data: 'status' },
		        { data: 'amount' },
		        { data: 'approval_date' },
		        { data: 'paid_date' }
		    ],
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
			"oTableTools": {
		        	 "aButtons": [
		             "copy",
		             	{
		                    "sExtends": "csv",
		                    "sTitle": "{{ $company->company_name }}-expenses-{{ time() }}",
		                    "sPdfMessage": "Zenintranet CSV Export",
		                    // "sPdfSize": "letter"
		                },
		             	{
		                    "sExtends": "xls",
		                    "sTitle": "{{ $company->company_name }}-expenses-{{ time() }}",
		                    "sPdfMessage": "Zenintranet XLS Export",
		                    // "sPdfSize": "letter"
		                },
		                {
		                    "sExtends": "pdf",
		                    "sTitle": "{{ $company->company_name }}-expenses-{{ time() }}",
		                    "sPdfMessage": "Zenintranet PDF Export",
		                    "sPdfSize": "letter"
		                },
		             	{
	                    	"sExtends": "print",
	                    	"sMessage": "Generated by Zenintranet <i>(press Esc to close)</i>"
	                	}
		             ],
		            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
		        },
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_datatable_fixed_column) {
					responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#all_expenses_table'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
					responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_datatable_fixed_column.respond();
			}
		});

		// Apply the filter
		$("#all_expenses_table thead th input[type=text]").on( 'keyup change', function () {
		    otable
		        .column( $(this).parent().index()+':visible' )
		        .search( this.value )
		        .draw();
		} );

		$('#expense_dates').daterangepicker();
		$('#expense_dates').on('apply.daterangepicker', function(ev, picker) {
		     	zen_ajax({
		            type:'GET',
		            url : base_url+'/get-expense-data-status',
		            data: 'expense_dates='+ $(this).val()
	        	}).done(function(data){
	                if(!data.length) {
	                	$('#bar-graph-no-data').show();
	                	$('#bar-graph-no-data').html('No data to show.');
	                	$('#bar-graph').hide();
	                } else {
	                	$('#bar-graph-no-data').hide();
	                	$('#bar-graph').show();

	                	$('#bar-graph').html('');
	                	expensesStatusChart(data);
	                }
	            }).fail(function(xhr) {
	                console.log('Something went wrong. Please try again later.');
	            });
		});

		$('#expense_type_dates').daterangepicker();
		$('#expense_type_dates').on('apply.daterangepicker', function(ev, picker) {
	     	zen_ajax({
	            type:'GET',
	            url : base_url+'/get-expense-data-types',
	            data: 'expense_dates='+ $(this).val()
        	}).done(function(data){
        		if(!data.length) {
        			$('#expenses_total_graph-no-data').show();
                	$('#expenses_total_graph-no-data').html('No data to show.');
                	$('#expenses_total_graph').hide();
                } else {
                	$('#expenses_total_graph-no-data').hide();
                	$('#expenses_total_graph').show();
                	// expenses_type_graph.setData(data);
                	expensesTypesChart(data);
                }
            }).fail(function(xhr) {
                console.log('Something went wrong. Please try again later.');
            });
	  });

	});
</script>

@endsection