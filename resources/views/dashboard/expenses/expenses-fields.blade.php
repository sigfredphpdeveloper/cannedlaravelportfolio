@include('dashboard.expenses.generated-fields', [ 'expense_fields' => $expense_fields, 'values' => $values ])

<script type="text/javascript">
	$(document).ready(function(e){
		$('.datepicker').datepicker({
            // dateFormat : 'dd.mm.yy',
            prevText : '<i class="fa fa-chevron-left"></i>',
            nextText : '<i class="fa fa-chevron-right"></i>',
            onSelect : function(selectedDate) {
                $('#finishdate').datepicker('option', 'minDate', selectedDate);
            }
        });
	});
</script>

@if( $file_required == 1 )
<script type="text/javascript">
    $(document).ready(function(e){
        if( $('.file-field-default').next('input').length < 1 ) {
            $('.file-field-default').addClass('required');
        }
    });
</script>
@else
<script type="text/javascript">
    $(document).ready(function(e){
        $('.file-field-default').removeClass('required');
    });
</script>
@endif