@if( !empty($expense_fields) )
    <?php
        $fields = array();
        $required = '';
        $data = '';
    ?>
    @foreach ( $expense_fields as $field ) 
            <?php
            if( $field->default == 1 )
             continue; 

            if( $field->required == 1 )
            {
                $required = 'required';
            }
            else
            {
                $required = '';
            }
            $value = '';
            if( array_key_exists( Helper::string_slug($field->label) , $values) ){
                $value = $values[Helper::string_slug($field->label)];
            }
            ?>
            <section class="field-group">
                <label class="label">{{ $field->label }}</label>
                @if( $field->type == 'number' )
                    <label class="input number-field">
                        <input type="text" class="number-input-field number {{$required}}" name="{{ Helper::string_slug($field->label) }}" id="{{ Helper::string_slug($field->label) }}" value="{{$value}}" >
                    </label>
                @elseif( $field->type == 'text' )
                    <label class="input text-field">
                        <input type="text" class="text-input-field {{$required}}" name="{{ Helper::string_slug($field->label) }}" id="{{ Helper::string_slug($field->label) }}" value="{{ $value }}" >
                    </label> 
                @elseif( $field->type == 'date' )
                    <label class="input">
                        <i class="icon-append fa fa-calendar"></i>
                        <input type="text" name="{{ Helper::string_slug($field->label) }}" placeholder="{{ $field->label }}" class="datepicker {{ $required }}" data-dateformat="dd/mm/yy" value="{{ $value }}" readonly="">
                    </label>
                @elseif ( $field->type == 'textarea') 
                    <label class="textarea">
                        <textarea rows="3" name="{{ Helper::string_slug($field->label) }}" class="custom-scroll textarea-input-field {{ $required }}">{{ $value }}</textarea>
                    </label>
                @elseif( $field->type == 'file' )
                    <?php
                    if( ! empty( $value ) ) {
                        $required = '';
                    }
                    ?>
                    <div class="input input-file">
                        <input type="file" name="{{ Helper::string_slug($field->label) }}" class="file-input-field {{ $required }}" placeholder="{{trans("expenses_modal_add-expense-line.Include some files")}}" value="{{ $value }}" readonly="">
                        @if( ! empty( $value ) )
                        <?php
                            $file = explode('/', $value );
                            $filename = $file[ count($file)-1 ];
                        ?> 
                            <input type="hidden" name="{{ Helper::string_slug($field->label) }}" value="{{  $value }}">
                            <span style="display:block;margin-top:3px;"><em>{{trans("expenses_modal_add-expense-line.Content")}}: </em><a href="{{ $value }}" target="_blank">{{ $filename }}</a></span>
                        @endif
                    </div>
                @endif
            </section>
    @endforeach
@endif