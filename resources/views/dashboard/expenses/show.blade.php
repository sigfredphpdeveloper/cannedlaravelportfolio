@extends('dashboard.layouts.master')

@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
			<h1 class="page-title txt-color-blueDark">
				<i class="fa fa-money fa-fw "></i> 
					{{ $expense_item->title }}
			</h1>
		</div>
		@if( $is_admin )
		   <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4 pull-right text-right">
				<h3 class="page-title txt-color-blueDark">
						<a href="{{ url('/expenses-setting') }}" class="btn btn-primary">{{trans("expenses_show.Customize")}}</a>
				</h3>
			</div>
	    @endif
	</div>

	@if(Session::has('success'))
		<div class="alert alert-block alert-success">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('success') }}</h4>
		</div>
	@elseif(Session::has('error'))
		 <div class="alert alert-block alert-danger">
             <a class="close" data-dismiss="alert" href="#">×</a>
             <h4 class="alert-heading"><i class="fa-fw fa fa-times"></i> {{ Session::get('error') }}</h4>
         </div>
	@endif
	<div class="row">
		<div class="col-sm-12">
			<div class="well well-sm">
				<div class="container-fluid">
					<div class="col-sm-12 col-md-12 col-lg-6">
						<h1>{{ $expense_item->title }}</h1>
						<p>{{ $expense_item->description }}</p>
						@if( $expense_item->request_details != '' )
							<hr/>
							<div>
								<h4>{{trans("expenses_show.Request Details")}}</h4>
								<p>{{$expense_item->request_details}}</p>
							</div>
						@endif
					</div>
					<div class="col-sm-12 col-md-12 col-lg-6">
						<div class="row">
						    @if($expense_item->paid_date == '0000-00-00')
                                <ul id="sparks" class="">
                                    <li>
                                        <a href="javascript:void(0)" class="edit-expense-modal" data-content="{{ $expense_item->id}}" style="color:#356635">
                                            <i class="icon edit-icon"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)" class="delete-expense-modal" data-content="{{ $expense_item->id}}" style="color:#953b39">
                                            <i class="icon trash-icon"></i>
                                        </a>
                                    </li>
                                </ul>
							@endif
						</div>
						<div class="row">
							<div class="col-xs-6 col-sm-4 col-md-4">
								<h5> {{trans("expenses_show.Total Amount")}} <br />
									<span class="txt-color-blue">{{ $company['expenses_currency']." ".$expense_item->total_amount}}</span>
								</h5>
							</div>
							<div class="col-xs-6 col-sm-4 col-md-4">
								<h5> {{trans("expenses_show.Status")}} <br />
									<span class="txt-color-blue" style="text-transform:capitalize">{{$expense_item->status}}</span>
								</h5>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-6 col-sm-4 col-md-4">
								<h5> {{trans("expenses_show.Creation Date")}} <br />
									<span class="txt-color-blue" style="text-transform:capitalize">{{$expense_item->creation_date}}</span>
								</h5>
							</div>
							<div class="col-xs-6 col-sm-4 col-md-4">
								<h5> {{trans("expenses_show.Paid Date")}} <br />
									<span class="txt-color-blue" style="text-transform:capitalize">{{$expense_item->paid_date}}</span>
								</h5>
							</div>
							<div class="col-xs-6 col-sm-4 col-md-4">
								<h5> {{trans("expenses_show.Approval Date")}} <br />
									<span class="txt-color-blue" style="text-transform:capitalize">{{$expense_item->approval_date}}</span>
								</h5>
							</div>
						</div>
					</div>				
				</div>
				<hr />
				<div class="container-fluid">
					<p style="text-align:right; padding: 10px 10px 0 10px">
					    @if($expense_item->paid_date == '0000-00-00')
                            @if($expense_item->status == 'pending')
                                <a href="javascript:void(0);" id="btn_submit_to_approver" class="btn btn-primary " data-toggle="tooltip" data-content="{{$expense_item->id}}" title="Submit to approver">Submit to Approver</a>
                            @endif

                            <a href="javascript:void(0);" data-toggle="tooltip" data-content="{{$expense_item->id}}" title="Add new line expense" id="btn_add_new_line" class="btn btn-primary"> {{trans("Add a new claim item")}} </a>
                        @endif
                    </p>
                     @if( count($expenses_line) != 0 )
						@foreach( $expenses_line as $expenses_line)
							<div class="well">
								<div class="row">
									<div class="col-sm-12 col-md-12 col-lg-6">
										<h3>{{$expenses_line->id}} - {{$expenses_line->title}} / Expense Type - {{ $expenses_line->expenses_types->title }}</h3>
									</div>
									<div class="col-sm-12 col-md-12 col-lg-6">
									    @if($expense_item->paid_date == '0000-00-00')
											<ul id="sparks" class="">
												<li>
													<a href="javascript:void(0);" data-content="{{$expenses_line->id}}" class="edit-expense-line-modal" style="color:#356635">
														<i class="icon edit-icon"></i>
													</a>
												</li>
												<li>
													<a href="javascript:void(0)" class="delete-expense-line-modal" data-content="{{$expenses_line->id}}" style="color:#953b39">
														<i class="icon trash-icon"></i>
													</a>
												</li>
											</ul>
                                        @endif
									</div>
								</div>
								<div class="row">
									<div class="container-fluid">
										<?php
											$values = unserialize($expenses_line->value);
										?>
										@foreach( $values as $key => $value )
											<div>
												@if(filter_var( str_replace( ' ', '%', $value), FILTER_VALIDATE_URL))
													<span style="text-transform:capitalize">{{ $key }}</span> : <a href="{{ $value }}" target="_blank" download><i class="fa fa-file-text-o" aria-hidden="true"></i></a>
												@else
													<span style="text-transform:capitalize">{{ $key }}</span> : {{ $value }}
												@endif
											</div>
										@endforeach
									</div>
								</div>
							</div>
						@endforeach
					@else
						<p style="text-align:center">{{trans("No Claims Item")}}</p>
					@endif
				</div>
			</div>
			@if($expense_item->status == 'request details')
                <p style="text-align:right; padding:10px 30px 0 10px">
                    <button type="button" id="re_submit" class="btn btn-primary">Re-Submit</button>
                </p>
			@endif
		</div>
	</div>


<!-- Small modal -->
<div class="modal fade" id="confirmation_modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="mySmallModalLabel">Submit my claim for Approval</h4>
            </div>
            <div class="modal-body">
               Please confirm you want to submit your claim for approval?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                <form action="{{url('send_to_approver')}}" method="POST" class="pull-right" style="margin-left:10px;">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" id="expense_id" value="">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('page-script')

<script type="text/javascript" src="{{ url('/js/show-expense.js') }}"></script>
<script>
$(document).ready(function(){

    $('#re_submit').on('click', function(){
//        $('#resend_editmodal').modal('show');
        $(this).html('loading...');
        $.ajax({
            url : '{{url('resubmit_details')}}/{{$id}}',
            type : 'POST',
            data : { '_token':'{{ csrf_token() }}'},
            dataType:'json',
            success:function(response){
                location.reload();
            }
        })


    })

    $('#btn_submit_to_approver').on('click', function(){
        var id = $(this).data('content');
        $('#expense_id').val(id);
        $('#confirmation_modal').modal('show');
    })

    $('#btn_submit_to_approver').on('hidden', function () {
        $('#expense_id').val('');
    })

})
</script>

@endsection