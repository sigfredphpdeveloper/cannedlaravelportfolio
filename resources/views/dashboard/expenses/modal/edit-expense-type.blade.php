<!-- Edit Expense Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="edit_expense_type">
    <div class="modal-dialog">
        <div class="modal-content">
            <div role="content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"> {{trans("expenses_modal_edit-expense-type.Edit Expense Type Item")}} </h4>
                </div>

                <div class="modal-body">
                    <div class="message-field"></div>
                    <form id="edit-expense-type-form" action="{{ url('edit-expense-type').'/'.$expenses_type->id }}" method="POST" class="smart-form edit-expense-type-form expense-type-form">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <input type="hidden" name="user_ids" value="" id="user_ids" />
                        <input type="hidden" name="expense-item-id" value="{{ $expenses_type->id }}" id="expense-item-id" />

                        <fieldset>
                            <section>
                                <label class="label">{{trans("expenses_modal_edit-expense-type.Expense Type Title")}}</label>
                                <label class="input expense-type-title-label">
                                      <input type="text" name="expense-type-title" class="expensetypetitle" id="expense-type-title" value="{{ $expenses_type->title }}" />
                                </label>
                            </section>
                        </fieldset>
                        <footer>
                             <button type="submit" class="btn btn-primary btn-edit-expense-type" alt="" value="Update" > {{trans("expenses_modal_edit-expense-type.Update")}}</button>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Edit Expense Modal -->

<script type="text/javascript">
    $(document).ready(function(){
        var $editExpenseLine = $('#edit-expense-type-form').validate();

        $.validator.addClassRules('expensetypetitle',{
            required : true
        });
    });
</script>