<!-- Pay Expense Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="add_expense_type">
    <div class="modal-dialog">
        <div class="modal-content">
            <div role="content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"> {{trans("expenses_modal_pay-expense.Pay Expense")}} {{ $expense->title }}</h4>
                </div>

                <div class="modal-body">
                    <div class="message-field"></div>
                    <form id="pay-expense-form" action="{{ url('/pay-expenses-item').'/'.$expense->id }}" method="POST" class="smart-form add-expense-line-form expense-type-form">
                         <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <fieldset>
                            <section class="field-group">
                                <label class="label">{{trans("expenses_modal_pay-expense.Date")}}</label>
                                <label class="input">
                                    <i class="icon-append fa fa-calendar"></i>
                                    <input type="text" name="pay_date" placeholder="Date" class="datepicker" data-dateformat="dd/mm/yy" value="" readonly="">
                                </label>
                            </section>
                        </fieldset>

                        <footer style="background: transparent;border-top: 0;">
                            <button type="submit" class="btn btn-primary" value="Save">{{trans("expenses_modal_pay-expense.Save")}} </button>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Pay Expense Modal -->

<script type="text/javascript">
    $(document).ready(function(){
        var $addExpenseLine = $('#pay-expense-form').validate();

        $.validator.addClassRules('datepicker',{
            required : true
        });

        $('.datepicker').datepicker({
            dateFormat : 'yy-mm-dd',
            prevText : '<i class="fa fa-chevron-left"></i>',
            nextText : '<i class="fa fa-chevron-right"></i>',
            onSelect : function(selectedDate) {
                $('#finishdate').datepicker('option', 'minDate', selectedDate);
            }
        });
    });
</script>