<!-- Add Expense Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="add_expense_type">
    <div class="modal-dialog">
        <div class="modal-content">
            <div role="content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"> {{trans("expenses_modal_edit-expense.Edit Expense")}} {{ $expense->title }} </h4>
                </div>

                <div class="modal-body">
                    <div class="message-field"></div>
                    <form id="edit-expense-form" action="{{ url('/edit-expense').'/'.$expense->id }}" method="POST" class="smart-form add-expense-form expense-type-form">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <input type="hidden" name="expense-id" value="{{ $expense->id }}">
                        <fieldset>
                            <section>
                                <label class="label">{{trans("expenses_modal_edit-expense.Expense Title")}}</label>
                                <label class="input expense-type-title-label">
                                      <input type="text" name="expense-title" class="expense_title" value="{{ $expense->title }}" id="expense-type-title" />
                                </label>
                            </section>
                            <section>
                                <label class="label"> {{trans("expenses_modal_edit-expense.Details")}} </label>
                                <label class="textarea textarea-resizable">                                         
                                    <textarea rows="3" class="custom-scroll" name="details">{{ $expense->description }}</textarea> 
                                </label>
                            </section>
                        </fieldset>
                        <footer>
                            <button type="submit" class="btn btn-primary btn-add-expense-type" alt="" value="Save" > {{trans("expenses_modal_edit-expense.Save")}}</button>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Add Expense Modal -->

<script type="text/javascript">
    $(document).ready(function(){
        var $addExpenseLine = $('#edit-expense-form').validate();

        $.validator.addClassRules('expense_title',{
            required : true
        });
    });
</script>