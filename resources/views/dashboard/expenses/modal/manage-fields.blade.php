<!--Manage Fields Expense Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="manage_fields_expense_type">
    <div class="modal-dialog">
        <div class="modal-content">
            <div role="content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"> {{trans("expenses_modal_manage-fields.Manage Fields for")}} {{ $expenses_type->title }} </h4>
                </div>

                <div class="modal-body">
                    <div class="alert alert-info" role="alert">
                      {{trans("expenses_modal_manage-fields.Note:")}}" <strong>{{trans("expenses_modal_manage-fields.Amount")}}</strong> {{trans("expenses_modal_manage-fields.data-type is constant. It will be use to compute the total expenses.")}}
                    </div>
                    <div class="message-field"></div>
                    <form id="manage-fields-form" action="{{ url('/save-expenses-type-fields') }}" method="POST" class="form-inline manage-fields-expense-type-form expense-type-form" role="form">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <input type="hidden" name="user_ids" value="" id="user_ids" />

                        <?php $count = 0; ?>
                        @foreach( $default_fields as $default_field )
                            <fieldset class="type-group" id="amount-fieldshet">
                                <input type="hidden" class="form-control expense-id active-id" name="fields[{{ $count }}][expense_type_id]" value="{{ $default_field->expenses_type->id }}" id="expense-item-id" />
                                <input type="hidden" class="form-control default-id active-default" name="fields[{{ $count }}][default]" value="1" id="default-item-id" />
                                <div class="form-group">
                                    <label class="sr-only"> {{ $default_field->label }} </label>
                                    <input type="text" class="form-control label-input active-label" value="{{ $default_field->label }}" name="fields[{{ $count }}][label]">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="type-select">{{trans("expenses_modal_manage-fields.Select")}}</label>
                                    <select class="form-control type-input active-type" id="type-select" name="fields[{{ $count }}][type]" >
                                        <option value="text" <?php echo ($default_field->type == 'text') ? 'selected' : ''; ?> >{{trans("expenses_modal_manage-fields.Text")}}</option>
                                        <option value="number" <?php echo ($default_field->type == 'number') ? 'selected' : ''; ?> >{{trans("expenses_modal_manage-fields.Number")}}</option>
                                        <option value="date" <?php echo ($default_field->type == 'date') ? 'selected' : ''; ?> >{{trans("expenses_modal_manage-fields.Date")}}</option>
                                        <option value="textarea" <?php echo ($default_field->type == 'textarea') ? 'selected' : ''; ?>>{{trans("expenses_modal_manage-fields.Textarea")}}</option>
                                        <option value="file" <?php echo ($default_field->type == 'file') ? 'selected' : ''; ?>>{{trans("expenses_modal_manage-fields.File")}}</option>
                                    </select> 
                                </div>
                                <div class="checkbox">
                                    <input type="hidden" class="checkbox required-input active-required" name="fields[{{ $count }}][required]" value="0">
                                    <label>
                                        <input type="checkbox" class="checkbox <?php echo ( $default_field->label == 'file') ? '' : 'required-input'; ?> active-required" value="1" name="fields[{{ $count }}][required]" <?php echo ($default_field->required == 1) ? 'checked' : ''; ?>>
                                            <span> {{trans("expenses_modal_manage-fields.Required")}} </span>
                                    </label>
                                </div>
                                @if( $count == count( $default_fields )-1 )
                                    <div class="form-group">
                                        <button type="button" onclick="ExpensesSettings.addFields(event)" class="btn btn-default addButton">
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                @endif
                            </fieldset>
                            <?php $count++; ?>
                        @endforeach

                        @foreach( $regular_fields as $regular_field )
                            <fieldset class="type-group">
                                <input type="hidden" class="form-control expense-id active-id" name="fields[{{ $count }}][expense_type_id]" value="{{ $regular_field->expenses_type->id }}" id="expense-item-id" />
                                <input type="hidden" class="form-control default-id active-default" name="fields[{{ $count }}][default]" value="0" id="default-item-id" />
                                <div class="form-group">
                                    <label class="sr-only"> {{ $regular_field->label }} </label>
                                    <input type="text" class="form-control label-input active-label" value="{{ $regular_field->label }}" name="fields[{{ $count }}][label]">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="type-select">{{trans("expenses_modal_manage-fields.Select")}}</label>
                                    <select class="form-control type-input active-type" id="type-select" name="fields[{{ $count }}][type]" >
                                        <option value="text" <?php echo ($regular_field->type == 'text') ? 'selected' : ''; ?> >{{trans("expenses_modal_manage-fields.Text")}}</option>
                                        <option value="number" <?php echo ($regular_field->type == 'number') ? 'selected' : ''; ?> >{{trans("expenses_modal_manage-fields.Number")}}</option>
                                        <option value="date" <?php echo ($regular_field->type == 'date') ? 'selected' : ''; ?> >{{trans("expenses_modal_manage-fields.Date")}}</option>
                                        <option value="textarea" <?php echo ($regular_field->type == 'textarea') ? 'selected' : ''; ?>>{{trans("expenses_modal_manage-fields.Textarea")}}</option>
                                        <option value="file" <?php echo ($regular_field->type == 'file') ? 'selected' : ''; ?>>{{trans("expenses_modal_manage-fields.File")}}</option>
                                    </select> 
                                </div>
                                <div class="checkbox">
                                    <input type="hidden" class="checkbox required-input active-required" name="fields[{{ $count }}][required]" value="0">
                                    <label>
                                        <input type="checkbox" class="checkbox <?php echo ( $regular_field->label == 'file') ? '' : 'required-input'; ?> active-required" value="1" name="fields[{{ $count }}][required]" <?php echo ($regular_field->required == 1) ? 'checked' : ''; ?>>
                                            <span> {{trans("expenses_modal_manage-fields.Required")}} </span>
                                    </label>
                                </div>
                                <div class="form-group">
                                            <button type="button" onclick="ExpensesSettings.removeFields(event)" class="btn btn-default removeButton">
                                                <i class="fa fa-minus"></i>
                                            </button>
                                        </div>
                            </fieldset>
                            <?php $count++; ?>
                        @endforeach

                        <fieldset class="hide type-group" id="option-template">
                            <input type="hidden" class="form-control expense-id active-label" name="expense_type_id" value="{{ $expenses_type->id }}" id="expense-item-id" />
                            <input type="hidden" class="form-control default-id" name="default" value="0" id="default-item-id" />
                            <div class="form-group">
                                <label class="sr-only"> {{trans("expenses_modal_manage-fields.Label")}} </label>
                                <input type="text" class="form-control label-input" name="label">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="type-select">{{trans("expenses_modal_manage-fields.Select")}}</label>
                                    <select class="form-control type-input" id="type-select" name="type">
                                        <option value="text" >{{trans("expenses_modal_manage-fields.Text")}}</option>
                                        <option value="number">{{trans("expenses_modal_manage-fields.Number")}}</option>
                                        <option value="date">{{trans("expenses_modal_manage-fields.Date")}}</option>
                                        <option value="textarea">{{trans("expenses_modal_manage-fields.Textarea")}}</option>
                                        <option value="file">{{trans("expenses_modal_manage-fields.File")}}</option>
                                    </select> 
                            </div>
                            <div class="checkbox">
                                <input type="hidden" class="checkbox required-input" name="required" value="0">
                                <label>
                                    <input type="checkbox" class="checkbox required-input" value="1" name="required">
                                    <span> {{trans("expenses_modal_manage-fields.Required")}} </span>
                                </label>
                            </div>
                            <div class="form-group">
                                <button type="button" onclick="ExpensesSettings.removeFields(event)" class="btn btn-default removeButton">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </fieldset>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit"  class="btn btn-primary btn-manage-fields-expense-type" alt="" value="Save" >{{trans("expenses_modal_manage-fields.Save")}} </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Manage Fields Expense Modal-->
<script type="text/javascript">
    $(document).ready(function(){
         var num_fields = $('fieldset.type-group').length;
         if( num_fields == 11 ){
            $('#manage-fields-form').find('.addButton').attr('disabled', 'disabled');
         }

         $('#amount-fieldshet .label-input').attr('readonly', 'readonly');
         $('#amount-fieldshet .required-input').on('click', function(){
            return false;
         });
         $('#amount-fieldshet .type-input option:not(:selected)').attr('disabled', true);

          var $manageFieldsForm = $('#manage-fields-form').validate();

            $.validator.addClassRules('active-label',{
                required : true
            });
    });
</script>
<style type="text/css">
    .invalid
    {
        display: block;
    }
</style>