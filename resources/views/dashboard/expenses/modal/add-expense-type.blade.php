<!-- Add Expense Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="add_expense_type">
    <div class="modal-dialog">
        <div class="modal-content">
            <div role="content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"> {{trans("expenses_modal_add-expense-type.Add Expense Type")}} </h4>
                </div>

                <div class="modal-body">
                    <div class="message-field"></div>
                    <form id="add-expense-type-form" action="{{ url('add-expense-type') }}" method="POST" class="smart-form add-expense-type-form expense-type-form">
                         <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <input type="hidden" name="user_ids" value="" id="user_ids" />
                        <fieldset>
                            <section>
                                <label class="label">{{trans("expenses_modal_add-expense-type.Expense Type Title")}}</label>
                                <label class="input expense-type-title-label">
                                      <input type="text" name="expense-type-title" class="expensetypetitle" id="expense-type-title" />
                                </label>
                            </section>
                        </fieldset>
                        <footer>
                             <input type="submit" class="btn btn-primary btn-add-expense-type" alt="" value="Save" />
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Add Expense Modal -->

<script type="text/javascript">
    $(document).ready(function(){
        var $addExpenseLine = $('#add-expense-type-form').validate();

        $.validator.addClassRules('expensetypetitle',{
            required : true
        });
    });
</script>