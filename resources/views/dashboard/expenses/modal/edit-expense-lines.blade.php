<!-- Add Expense Line Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="add_expense_type">
    <div class="modal-dialog">
        <div class="modal-content">
            <div role="content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"> {{trans("expenses_modal_edit-expense-line.Edit Expense Line")}} </h4>
                </div>

                <div class="modal-body">
                    <div class="message-field"></div>
                    <form id="add-expense-line-form" action="{{ url('/edit-expense-lines').'/'.$expense_line->id }}" method="POST" class="smart-form add-expense-line-form expense-type-form" enctype="multipart/form-data" accept-charset="UTF-8">
                         <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                         <input type="hidden" name="expense-id" id="expense-id" value="{{ $expense_line->expense_id }}">
                        <fieldset>
                            <section>
                                <label class="label"> {{trans("expenses_modal_edit-expense-line.Title")}} </label>
                                <label class="input text-field">
                                    <input type="text" class="text-input-field required" name="title" id="title" value="{{ $expense_line->title }}">
                                </label>
                            </section>
                            <section>
                                <label class="label"> {{trans("expenses_modal_edit-expense-line.Amount")}} </label>
                                <label class="input text-field">
                                    <input type="text" class="number-input-field number required" name="amount" id="title" value="{{ $expense_line->amount }}">
                                </label>
                            </section>
                            <section class="field-group">
                                <label class="label">{{trans("expenses_modal_add-expense-line.File")}}</label>
                                <div class="input input-file">
                                    <input type="file" name="file" class="file-input-field file-field-default" placeholder="{{trans("expenses_modal_add-expense-line.Include some files")}}" value="{{ $expense_line->file }}" readonly="">
                                    @if( !empty($expense_line->file) )
                                    <?php
                                        $file = explode('/', $expense_line->file );
                                        $filename = $file[ count($file)-1 ];
                                    ?>
                                    <input type="hidden" name="file" value="{{ $expense_line->file }}">
                                    <span style="display:block;margin-top:3px;"><em>Content: </em><a href="{{ $expense_line->file }}" target="_blank">{{ $filename }}</a></span>
                                    @endif
                                </div>
                            </section>
                            <section>
                                <label class="label">{{trans("expenses_modal_edit-expense-line.Expense Type")}}</label>
                                <label class="select">
                                    <select name="expenses_types" id="expense-type-select" class="expense-type-select required">
                                        <option value="">{{trans("expenses_modal_edit-expense-line.Choose Expense Type")}}</option>
                                        @foreach( $expenses_types as $expense_type )
                                            <option value="{{ $expense_type->id }}" <?php echo ( $expense_type->id == $expense_line->expense_type_id ) ? 'selected' : ''; ?> >{{ $expense_type->title }}</option>
                                        @endforeach
                                    </select> <i></i> 
                                </label>
                            </section>
                            <div id="fields">
                                @include('dashboard.expenses.generated-fields', [ 'expense_fields' => $expense_fields, 'values' => $values ])
                            </div>
                        </fieldset>

                        <footer style="background: transparent;border-top: 0;">
                            <button type="submit" class="btn btn-primary" value="Save"> {{trans("expenses_modal_edit-expense-line.Save")}} </button>
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Add Expense line Modal -->

<script type="text/javascript">
    $(document).ready(function(){
        var $addExpenseLine = $('#add-expense-line-form').validate();

        $.validator.addClassRules('required',{
            required : true
        });

        $.validator.addClassRules('numer',{
            digits : true
        });

        $('.datepicker').datepicker({
            // dateFormat : 'dd.mm.yy',
            prevText : '<i class="fa fa-chevron-left"></i>',
            nextText : '<i class="fa fa-chevron-right"></i>',
            onSelect : function(selectedDate) {
                $('#finishdate').datepicker('option', 'minDate', selectedDate);
            }
        });

        $('.expense-type-select').on('change', function(e){
            var id = $(this).val();
            if( id == '' )
            {
                $('#fields').html('');
            }
            else
            {
                $.ajax({
                type:'GET',
                url: base_url+'/get-fields/'+id+'/'+{{ $expense_line->id }}
                }).done(function(data){
                    $('#fields').html(data);
                }).fail(function(xhr) {
                    alert('error1');
                });
            }
        });
    });
</script>