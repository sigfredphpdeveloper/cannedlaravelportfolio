<!-- end widget grid -->
<div class="modal fade" tabindex="-1" role="dialog" id="confirm">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{trans("expenses_modal_delete-expense-type.Confirmation")}}</h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-warning fade in">
                <button class="close" data-dismiss="alert">
                  ×
                </button>
                <i class="fa-fw fa fa-warning"></i>
                <strong>{{trans("expenses_modal_delete-expense-type.Warning")}}</strong> {{trans("expenses_modal_delete-expense-type.This Expense type may be associated already with expenses.")}}
              </div>
        <form action="{{ url('destroy-expense-type').'/'.$expense_type->id }}" method="post">
          <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
          <p>{{trans("expenses_modal_delete-expense-type.Are you sure you want to delete this Expense Type ?")}} </p>
          <footer>
            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("expenses_modal_delete-expense-type.No")}}</button>
            <button type="submit" class="btn btn-primary" id="delete_proceed" alt="">{{trans("expenses_modal_delete-expense-type.Yes")}}</button>
          </footer>
        </form>
        
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->