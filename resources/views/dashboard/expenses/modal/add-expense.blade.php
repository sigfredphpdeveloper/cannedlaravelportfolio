<!-- Add Expense Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="add_expense_type">
    <div class="modal-dialog">
        <div class="modal-content">
            <div role="content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"> {{trans("Create a new claim report")}} </h4>
                </div>

                <div class="modal-body">
                    <div class="message-field"></div>
                    <form id="add-expense-form" action="{{ url('/add-expense') }}" method="POST" class="smart-form add-expense-form expense-type-form">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <fieldset>
                            <section>
                                <label class="label">{{trans("Claim Report Title")}}</label>
                                <label class="input expense-type-title-label">
                                      <input type="text" name="expense-title" class="expense_title" id="expense-type-title" />
                                </label>
                            </section>
                            <section>
                                <label class="label"> {{trans("expenses_modal_add-expense.Details")}} </label>
                                <label class="textarea textarea-resizable">                                         
                                    <textarea rows="3" class="custom-scroll" name="details"></textarea> 
                                </label>
                            </section>
                        </fieldset>
                        <footer>
                            <input type="submit" class="btn btn-primary btn-add-expense-type" alt="" value="Save" />
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Add Expense Modal -->

<script type="text/javascript">
    $(document).ready(function(){
        var $addExpenseLine = $('#add-expense-form').validate();

        $.validator.addClassRules('expense_title',{
            required : true
        });

        $.validator.addClassRules('expense-type-select',{
            required : true
        });
    });
</script>