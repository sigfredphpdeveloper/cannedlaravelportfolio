<!-- end widget grid -->
<div class="modal fade" tabindex="-1" role="dialog" id="confirm">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{trans("expenses_modal_delete-expense-line.Confirmation")}}</h4>
      </div>
      <div class="modal-body">
        <form action="{{ url('destroy-expense-line').'/'.$expense_line->id }}" method="POST">
            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
            <fieldset>
              <section>
                   <p>{{trans("expenses_modal_delete-expense-line.Are you sure you want to delete this Expense Line ?")}}" </p>
              </section>
          </fieldset>
          <footer>
            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("expenses_modal_delete-expense-line.No")}}</button>
            <button type="submit" class="btn btn-primary" id="" alt="">{{trans("expenses_modal_delete-expense-line.Yes")}}</button>
          </footer>
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->