@extends('dashboard.layouts.master')

@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
			<h1 class="page-title txt-color-blueDark">
				<i class="fa fa-money fa-fw "></i> 
					{{trans("expenses_index.My Expenses")}} 
			</h1>
		</div>
		@if( $is_admin )
		   <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4 pull-right text-right">
				<h3 class="page-title txt-color-blueDark">
						<a href="{{ url('/expenses-setting') }}" class="btn btn-primary">{{trans("expenses_index.Customize")}}</a>
				</h3>
			</div>
	    @endif
	</div>
	@if(Session::has('success'))
		<div class="alert alert-block alert-success">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('success') }}</h4>
		</div>
	@elseif(Session::has('error'))
		<div class="alert alert-block alert-danger">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('error') }}</h4>
		</div>
	@endif
	<!-- widget grid -->
	<section id="widget-grid" class="">
		<!-- row -->
		<div class="row">
			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-togglebutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2> {{trans("expenses_index.My Expenses List")}} </h2>
					</header>
					<div>
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<div class="widget-body no-padding">
							<p style="text-align:right; padding: 10px 10px 0 10px">
                                <a href="javascript:void(0);" data-toggle="tooltip" title="" id="btn_add_expensed" class="btn btn-primary" data-original-title="Add Expense"> {{trans("Submit a New Claim Report")}} </a>
                            </p>
                            @if( count($my_expenses) != 0 )
								<table class="table table-hover">
									<thead>
										<tr>
											<td class="hidden-xs">{{trans("expenses_index.Submision Date")}}</td>
											<td class="hidden-xs">{{trans("expenses_index.Expense ID")}}</td>
											<td>{{trans("expenses_index.Title")}}</td>
											<td>{{trans("expenses_index.Total Amount")}}</td>
											<td>{{trans("expenses_index.Status")}}</td>
											<td class="hidden-xs">{{trans("expenses_index.Approve Date")}}</td>
											<td class="hidden-xs">{{trans("expenses_index.Paid Date")}}</td>
											<td>{{trans("expenses_index.Action")}}</td>
										</tr>
									</thead>
									<tbody>
										@foreach( $my_expenses as $my_expense )
											<tr>
												<td class="hidden-xs"> {{$my_expense->creation_date}} </td>
												<td class="hidden-xs"> {{$my_expense->id}} </td>
												<td> {{$my_expense->title}} </td>
												<td> {{ $company['expenses_currency']." ".$my_expense->total_amount}} </td>
												<td> {{$my_expense->status}}</td>
												<td class="hidden-xs"> {{$my_expense->approval_date}} </td>
												<td class="hidden-xs"> {{$my_expense->paid_date}} </td>
												<td> 
													<div class="input-group-btn" style="width: auto;">
		                                                <button type="button" class="btn btn-default" tabindex="-1">{{trans("expenses_index.Action")}}</button>
		                                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1" aria-expanded="false">
		                                                    <span class="caret"></span>
		                                                </button>
		                                                <ul class="dropdown-menu pull-right" role="menu">
		                                                {{--@if(!empty($expense_roles['approve']) && $my_expense->approval_date == '0000-00-00')--}}
		                                                	{{--<li><a href="javascript:;" class="approve-expense" data-id="{{ $my_expense->id }}">Approve</a></li>--}}
		                                                	{{--<li class="divider"></li>--}}
		                                                {{--@endif--}}
		                                                {{--@if(!empty($expense_roles['approve']))--}}
		                                                	{{--<li><a href="{{ url('/expense-item') }}/{{ $my_expense->id }}" class="reject-expense">Reject</a></li>--}}
		                                                	{{--<li class="divider"></li>--}}
                                                        {{--@endif--}}

		                                                	<li><a href="{{ url('/expense-item') }}/{{ $my_expense->id }}" class="view-expense-modal">{{trans("expenses_index.View")}}</a></li>
		                                                	<li class="divider"></li>
                                                        @if((!empty($expense_permission['approve']) && $my_expense->approval_date == '0000-00-00') || $user->user_level == 1)
		                                                    <li><a href="javascript:void(0);" data-content="{{ $my_expense->id }}" class="edit-expense-modal">{{trans("expenses_index.Edit")}}</a></li>
		                                                    <li class="divider"></li>
                                                        @endif
                                                         @if((!empty($expense_permission['approve']) && $my_expense->approval_date == '0000-00-00') || $user->user_level == 1)
		                                                    <li><a href="javascript:void(0);" data-content="{{ $my_expense->id }}" class="delete-expense-modal">{{trans("expenses_index.Delete")}}</a></li>
		                                                @endif
		                                                </ul>
		                                            </div>
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							@else
								<p style="text-align:center">{{trans("expenses_index.No Expenses")}}</p>
							@endif
						</div>
					</div>
				</div>
			</article>
		</div>
	</section>
	
@endsection

@section('page-script')

<script type="text/javascript" src="{{ url('/js/my-expenses.js') }} "></script>


<script>
$(document).ready(function(){

    $('.approve-expense').on('click', function(){
        var id = $(this).data('id');

        $.ajax({
            url : '{{url('/approve_expense')}}',
            type : 'POST',
            data : {'id':id, '_token':'{{ csrf_token() }}'},
            dataType:'json',
            success:function(response){
                if(response){
                    location.reload();
                }
            }
        })

    })

})
</script>


@endsection