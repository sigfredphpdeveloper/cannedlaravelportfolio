@extends('dashboard.layouts.master')

@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
			<h1 class="page-title txt-color-blueDark">
				<i class="fa fa-money fa-fw "></i> 
					{{trans("expenses_index.Expenses History")}} 
			</h1>
		</div>
		@if( $is_admin )
		   <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4 pull-right text-right">
				<h3 class="page-title txt-color-blueDark">
						<a href="{{ url('/expenses-setting') }}" class="btn btn-primary">{{trans("expenses_index.Customize")}}</a>
				</h3>
			</div>
	    @endif
	</div>
	@if(Session::has('success'))
		<div class="alert alert-block alert-success">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('success') }}</h4>
		</div>
	@elseif(Session::has('error'))
		<div class="alert alert-block alert-danger">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('error') }}</h4>
		</div>
	@endif
	<!-- widget grid -->
	<section id="widget-grid" class="">
		<!-- row -->
		<div class="row">
			<!-- NEW WIDGET START -->
			<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-togglebutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
						<h2> {{trans("expenses_index.Expenses List")}} </h2>
					</header>
					<div>
						<!-- widget edit box -->
						<div class="jarviswidget-editbox">
							<!-- This area used as dropdown edit box -->
						</div>
						<div class="widget-body no-padding">
							<table id="all_expenses_table" class="table table-striped table-bordered" width="100%">
								<thead>
									<tr style="background-color: transparent !important; background-image: none !important;">
										<th class="hasinput" style="">
											<input type="text" class="form-control" placeholder="Filter ID" />
										</th>
										<th class="hasinput">
											<input id="submitted_date" type="text" placeholder="Filter Date" class="form-control datepicker" data-dateformat="yy-mm-dd">
										</th>
										<th class="hasinput" style="">
											<input type="text" class="form-control" placeholder="Filter Name" />
										</th>
										<th class="hasinput" style="">
											<input type="text" class="form-control" placeholder="Filter Title" />
										</th>
										<th class="hasinput" style="">
											<input type="text" class="form-control" placeholder="Filter Status" />
										</th>
										<th class="hasinput" style="">
											<input type="text" class="form-control" placeholder="Filter Amount" />
										</th>
										<th class="hasinput">
											<input id="approval_date" type="text" placeholder="Filter Date" class="form-control datepicker" data-dateformat="yy-mm-dd">
										</th>
										<th class="hasinput">
											<input id="paid_date" type="text" placeholder="Filter Date" class="form-control datepicker" data-dateformat="yy-mm-dd">
										</th>
										<th></th>
									</tr>
									<tr>
										<th>{{trans("expenses_reports.ID")}}</th>
										<th>{{trans("expenses_reports.Submitted Date")}}</th>
										<th>{{trans("expenses_reports.Employee Name")}}</th>
										<th>{{trans("expenses_reports.Title")}}</th>
										<th>{{trans("expenses_reports.Status")}}</th>
										<th>Amount</th>
										<th>{{trans("expenses_reports.Approval Date")}}</th>
										<th>{{trans("expenses_reports.Paid Date")}}</th>
										<th>View</th>
									</tr>
								</thead>
								<tbody>
									@foreach( $expenses as $expense )
										<tr>
											<td>{{ $expense->id }}</td>
											<td>{{ $expense->created_at }}</td>
											<td>{{$expense->user->first_name}} {{$expense->user->last_name}}</td>
											<td>{{ $expense->title }}</td>
											<td>{{ $expense->status }}</td>
											<td>{{ $company->expenses_currency }} {{ $expense->total_amount }}</td>
											<td>{{ $expense->approval_date }}</td>
											<td>{{ $expense->paid_date }}</td>
											<td>
												 <a href="{{ url('/expense-item') }}/{{ $expense->id }}" class="view-expense-modal"><i class="icon view-icon"></i></a>
											</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</article>
		</div>
	</section>
	
@endsection

@section('page-script')
<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{ url('js/plugin/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('js/plugin/datatables/dataTables.colVis.min.js') }}"></script>
<script src="{{ url('js/plugin/datatables/dataTables.tableTools.min.js') }}"></script>
<script src="{{ url('js/plugin/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ url('js/plugin/datatable-responsive/datatables.responsive.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		pageSetUp();
		

		var responsiveHelper_datatable_fixed_column = undefined;
		var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
		var otable = $('#all_expenses_table').DataTable({
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
			"oTableTools": {
		        	 "aButtons": [
		             "copy",
			             {
		                    "sExtends": "csv",
		                    "sTitle": "{{ $company->company_name }}-expenses-{{ time() }}",
		                    "sPdfMessage": "Zenintranet CSV Export",
		                    // "sPdfSize": "letter"
		                },
		             	{
		                    "sExtends": "xls",
		                    "sTitle": "{{ $company->company_name }}-expenses-{{ time() }}",
		                    "sPdfMessage": "Zenintranet XLS Export",
		                    // "sPdfSize": "letter"
		                },
		                {
		                    "sExtends": "pdf",
		                    "sTitle": "{{ $company->company_name }}-expenses-{{ time() }}",
		                    "sPdfMessage": "Zenintranet PDF Export",
		                    "sPdfSize": "letter"
		                },
		             	{
	                    	"sExtends": "print",
	                    	"sMessage": "Generated by Zenintranet <i>(press Esc to close)</i>"
	                	}
		             ],
		            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
		        },
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_datatable_fixed_column) {
					responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#all_expenses_table'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
					responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_datatable_fixed_column.respond();
			}
		});

		// Apply the filter
		$("#all_expenses_table thead th input[type=text]").on( 'keyup change', function () {
		    otable
		        .column( $(this).parent().index()+':visible' )
		        .search( this.value )
		        .draw();
		} );
	});
</script>
@endsection