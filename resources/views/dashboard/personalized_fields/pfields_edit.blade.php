@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-sm-12 col-md-12">
        <h1>{{trans("personalized_fields_pfields_edit.Edit Field")}}</h1>
        <a href="{{url('/personalized_fields')}}" class="btn btn-primary">{{trans("personalized_fields_pfields_edit.Back")}}</a>
        <br />
        </div>

        <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>{{trans("personalized_fields_pfields_edit.Warning.")}}<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <article class="col-sm-6 col-md-6 col-lg-6">

            <form  id="smart-form-register" class="smart-form client-form" role="form" method="POST" action="{{ url('/pfields_edit') }}/{{$user_field['id']}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="user_id" value="{{ $user['id'] }}">
            <input type="hidden" name="company_id" value="{{ $company['id'] }}">

                <fieldset>

                     <section>
                        <label class="label">{{trans("personalized_fields_pfields_edit.Field Name *")}}</label>
                        <label class="input">
                        <input type="text" name="field_name" placeholder="{{trans("personalized_fields_pfields_edit.Field Name")}}" value="{{$user_field['field_name']}}" />

                        </label>
                    </section>

                    <section>
                        <label class="label">{{trans("personalized_fields_pfields_edit.Field Type *")}}</label>
                        <label class="input">

                            {!!
                              Form::select('field_type', $field_types, $user_field['field_type'],['id' => 'field_type','class'=>'form-control']);
                            !!}

                        </label>
                    </section>

                    <section id="section_field_value" <?php echo ($user_field['field_type']!='list_dropdown')?'style="display:none;"':'';?>>
                        <label class="label">{{trans("personalized_fields_pfields_edit.Dropdown Options")}}</label>
                        <label class="input">
                        <textarea name="field_value" class="form-control" style="height:150px;">{{$user_field['field_value']}}</textarea>
                        <p><i>{{trans("personalized_fields_pfields_edit.Separated by new line")}}</i></p>
                        </label>
                    </section>

                    <section>
                        <label class="label">{{trans("personalized_fields_pfields_edit.Field Description")}}</label>
                        <label class="input">

                        <input type="text" name="field_description" placeholder="{{trans("personalized_fields_pfields_edit.Field Description")}}" value="{{$user_field['field_description']}}" />

                        </label>
                    </section>


                    <section style="width:50%;">

                        <label class="toggle">
                        <input type="checkbox" name="field_visibility"
                        value="1"  <?php echo ($user_field['field_visibility']=='1')?'checked="checked"':'';?>>
                        <i data-swchon-text="ON" data-swchoff-text="OFF"></i>{{trans("personalized_fields_pfields_edit.Visibility")}}</label>

                    </section>


                </fieldset>

                <footer>
                    <button type="submit" class="btn btn-success">
                        {{trans("personalized_fields_pfields_edit.Save")}}
                    </button>
                </footer>

            </form>

        </article>
    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->

<script type="text/javascript">
    $(document).ready(function(){


        $('#field_type').change(function(){
            var val = $(this).val();

            if(val == 'list_dropdown'){
                $('#section_field_value').fadeIn();
            }else{
                $('#section_field_value').hide();
            }

        });

    });
</script>

@endsection
