@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-md-12">

         <a href="{{ url('/admin_settings') }}" class="btn btn-xs btn-default"><i class="fa fa-caret-left"></i> Back To Settings</a>


            @if(Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ Session::get('success') }}</p>
                </div>
            @endif
        </div>

        <div class="col-md-12">

            @if(Session::has('error'))
                <div class="alert alert-danger">
                    <p>{{ Session::get('error') }}</p>
                </div>
            @endif
        </div>

        <div class="col-md-6">

            <h1>{{trans("personalized_fields_manage_pfields.Personalized Settings for Employee's Profile")}}</h1>
            <p>{{trans("personalized_fields_manage_pfields.Up to 10 fields")}}</p>
        </div>

        <div class="col-md-6">
            <a href="{{ url('/pfields_add') }}" class="btn btn-primary">{{trans("personalized_fields_manage_pfields.Add Field")}}</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">

            <table class="table table-striped">
                <tr>
                    <th>{{trans("personalized_fields_manage_pfields.Field Name")}}</th>
                    <th>{{trans("personalized_fields_manage_pfields.Description")}}</th>
                    <th>{{trans("personalized_fields_manage_pfields.Type")}}</th>
                    <th>{{trans("personalized_fields_manage_pfields.Visibility")}}</th>
                    <th width="200"></th>
                </tr>

                @foreach($user_fields as $i=>$item)
                <tr>
                    <td>{{$item['field_name']}}</td>
                    <td>{{$item['field_description']}}</td>
                    <td>{{$item['field_type']}}</td>
                    <td><?php echo ($item['field_visibility'] == 0)?'Hidden':'Visible';?>
                    </td>
                    <td>
                        <a href="{{ url("pfields_edit/") }}/{{ $item['id'] }}"><i class="icon edit-icon"></i></a>
                        <a href="javascript:void(0);" data-id="{{ $item['id'] }}" class="deleter"><i class="icon trash-icon"></i></a>
                    </td>
                </tr>
                @endforeach
            </table>

        </div>

    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->

<script type="text/javascript">

    $(document).ready(function(){

        $('.deleter').click(function(){

            var id = $(this).data('id');
            $('#delete_proceed').attr('alt',id);
            $('#confirm').modal('show');

        });

        $('#delete_proceed').click(function(){

            var id = $(this).attr('alt');

            $('#confirm').modal('hide');

            window.location.href='{{ url('pfields_delete') }}/'+id;

        });

    });

</script>

<div class="modal fade" tabindex="-1" role="dialog" id="confirm">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{trans("personalized_fields_manage_pfields.Confirmation")}}</h4>
      </div>
      <div class="modal-body">
        <p>{{trans("personalized_fields_manage_pfields.Are you sure you want to delete this record ?")}}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("personalized_fields_manage_pfields.No")}}</button>
       <button type="button" class="btn btn-primary" id="delete_proceed" alt="">{{trans("personalized_fields_manage_pfields.Yes")}}</button>

      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@endsection