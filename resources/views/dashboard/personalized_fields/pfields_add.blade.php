@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-sm-12 col-md-12">
        <h1>{{trans("personalized_fields_pfields_add.Add Field")}}</h1>
        </div>

        <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Warning.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <article class="col-sm-6 col-md-6 col-lg-6">

            <form  id="smart-form-register" class="smart-form client-form" role="form" method="POST" action="{{ url('/pfields_add_save') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="user_id" value="{{ $user['id'] }}">
            <input type="hidden" name="company_id" value="{{ $company['id'] }}">

                <fieldset>

                     <section>
                        <label class="label">{{trans("personalized_fields_pfields_add.Field Name *")}}</label>
                        <label class="input">
                        <input type="text" name="field_name" placeholder="{{trans("personalized_fields_pfields_add.Field Name")}}" value="" />

                        </label>
                    </section>

                    <section>
                        <label class="label">{{trans("personalized_fields_pfields_add.Field Type *")}}</label>
                        <label class="input">

                            <select name="field_type" id="field_type" class="form-control">
                                <option value="text">{{trans("personalized_fields_pfields_add.Text")}}</option>
                                <option value="date">{{trans("personalized_fields_pfields_add.Date")}}</option>
                                <option value="number">{{trans("personalized_fields_pfields_add.Number")}}</option>
                                <option value="list_dropdown">{{trans("personalized_fields_pfields_add.List Dropdown")}}</option>
                            </select>

                        </label>
                    </section>

                    <section id="section_field_value" style="display:none;">
                        <label class="label">{{trans("personalized_fields_pfields_add.Dropdown Options")}}</label>
                        <label class="input">
                        <textarea name="field_value" class="form-control" style="height:150px;"></textarea>
                        <p><i>{{trans("personalized_fields_pfields_add.Separated by new line")}}</i></p>
                        </label>
                    </section>

                    <section>
                        <label class="label">{{trans("personalized_fields_pfields_add.Field Description")}}</label>
                        <label class="input">

                        <input type="text" name="field_description" placeholder="{{trans("personalized_fields_pfields_add.Field Description")}}" value="" />

                        </label>
                    </section>


                    <section style="width:50%;">

                        <label class="toggle">
                        <input type="checkbox" name="field_visibility"
                        value="1"  >
                        <i data-swchon-text="ON" data-swchoff-text="OFF"></i>{{trans("personalized_fields_pfields_add.Visibility")}}</label>

                    </section>


                </fieldset>

                <footer>
                    <button type="submit" class="btn btn-success">
                        {{trans("personalized_fields_pfields_add.Save")}}
                    </button>
                    <a href="{{ url('/personalized_fields') }}" class="btn btn-primary">{{trans("personalized_fields_pfields_add.Back")}}</a>
                </footer>

            </form>

        </article>
    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->

<script type="text/javascript">
    $(document).ready(function(){


        $('#field_type').change(function(){
            var val = $(this).val();

            if(val == 'list_dropdown'){
                $('#section_field_value').fadeIn();
            }else{
                $('#section_field_value').hide();
            }

        });

    });
</script>

@endsection
