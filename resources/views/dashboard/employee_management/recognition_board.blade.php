@extends('dashboard.layouts.master')

@section('content')

<style>
	body .ui-autocomplete .ui-menu-item .ui-state-focus {
   		/* selected <a> */
   		background: grey!important;
	}

	/* Elements from Datatable JQuery plug-ins */
	.paginate_button {
		margin: 5px;
	}

	.paginate_button:hover {
		cursor: pointer;
	}

	.disabled:hover {
		color: white !important;
		cursor: default;
	}
</style>

<section id="widget-grid" class="">
	<div class="row">
		<div class="col-md-12">
			@if(session('success'))
				<div class="alert alert-success">
					<p>{{ session('success') }}</p>
				</div>
			@endif
		</div>

		<div class="col-md-6">
            <h1>{{trans("dashboard_layouts_nav.Recognition Board")}}</h1>
		</div>

		<div class="col-md-6">
			<form style="float:right; margin-left:5px"><input id="bulk-del-btn" type="submit" class="btn btn-primary" 
				value="{{trans("employee_recognition_board.Bulk Delete")}}" /></form>

			<button class="add-recog-btn btn btn-primary pull-right" style="margin-left:5px">
				{{trans("employee_recognition_board.Add New Recognition")}}
			</button>

			<?php if($is_admin):?>
				<a href="{{ url('/employee_recognition_board_settings') }}" class="btn btn-primary pull-right">
					{{trans("employee_recognition_board.Configure")}}
				</a>
			<?php endif;?>
		</div>
	</div>

	<div class="row" style="margin-top:10px">
		<div class="col-md-12">
			<table id='recognition-table' class="table table-striped">
				<thead>
					<tr>
						<th class="no-sorting-col" width="5%"><input id="del-all" type="checkbox"></th>
						<th width="30%">{{trans("employee_recognition_board.Employee Name")}}</th>
						<th width="35%">{{trans("employee_recognition_board.Recognition Date")}}</th>
						<th width="15%">{{trans("employee_recognition_board.Status")}}</th>
						<th class='no-sorting-col' width="15%"></th>
					</tr>
				</thead>

				<tbody>
					@foreach($recognition_board_arr as $item)
					<tr>
						<td><form><input type="checkbox" name="recognition_row[]" value="{{ $item->id }}" /></form></td>
						<td>{{$item->employee_first_name}} {{$item->employee_last_name}}</td>
						<td>{{$item->owner_first_name}} {{$item->owner_last_name}} - {{date('F d', strtotime($item->created_at))}}, {{date('o', strtotime($item->created_at))}}</td>
						<td>{{$item->status}}</td>
						<td>
							<a class="edit-recog-btn icon edit-icon" data-id="{{ $item->id }}" style="cursor:pointer"></a>
							<a class="del-recog-btn icon trash-icon" data-id="{{ $item->id }}" style="cursor:pointer"></a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</section>



<!-- Modal Section -->
<div id="add-new-recognition" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!--Modal Content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">{{trans("employee_recognition_board.Add New Recognition")}}</h4>
			</div>

			<form id="add-recog-form">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="modal-body">
					<div class="form-group">
						<label>{{trans("employee_recognition_board.Employee Name")}}</label>
						<input id="add-recog-autocomplete" class="form-control" required>
						<input id="add-recog-employee-id" name="employee_id" type="hidden">
					</div>
					<div class="form-group">
						<label>{{trans("employee_recognition_board.Title")}}</label>
						<input class="form-control" name="title" type="text" required maxlength="150">
					</div>
					<div class="form-group">
						<label>{{trans("employee_recognition_board.Message")}}</label>
						<input class="form-control" name="message" type="text" required maxlength="300">
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">
						{{trans("employee_recognition_board.Cancel")}}
					</button>
					<input type="submit" class="btn btn-primary submit-btn" value="{{trans('employee_recognition_board.Submit')}}">
				</div>
			</form>
		</div>
	</div>
</div>


<div id="edit-recognition" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal Content -->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">{{trans("employee_recognition_board.Edit A Recognition")}}</h4>
			</div>

			<form id="edit-recog-form">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="modal-body">
					<div class="form-group">
						<label>{{trans("employee_recognition_board.Employee Name")}} *</label>
						<input id="edit-recog-autocomplete" class="form-control" required>
						<input id="edit-recog-employee-id" name="employee_id" type="hidden">
					</div>
					<div class="form-group">
						<label>{{trans("employee_recognition_board.Title")}} *</label>
						<input id='edit-recog-title' class="form-control" name="title" type="text" required maxlength="150" required>
					</div>
					<div class="form-group">
						<label>{{trans("employee_recognition_board.Message")}} *</label>
						<input id='edit-recog-message' class="form-control" name="message" type="text" required maxlength="300" required>
					</div>
					<div class="form-group">
						<label>{{trans("employee_recognition_board.Status")}} *</label>
						<select id='edit-recog-visibility' name="status" class="form-control" required>
							<option value="visible" selected>{{trans("employee_recognition_board.Visible")}}</option>
							<option value="hidden">{{trans("employee_recognition_board.Hidden")}}</option>
						</select>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">
						{{trans("employee_recognition_board.Cancel")}}
					</button>
					<input type="submit" class="btn btn-primary submit-btn" value="{{trans("employee_recognition_board.Submit")}}">
				</div>
			</form>
		</div>
	</div>
</div>


<div id="delete-recognition" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!--Modal Content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">{{trans("employee_recognition_board.Confirmation")}}</h4>
			</div>

			<div class="modal-body">
				<div class="form-group">
					<p>{{trans("employee_recognition_board.Are you sure you want to delete this record ?")}}</p>
				</div>
			</div>

			<form>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">
						{{trans("employee_recognition_board.No")}}
					</button>
					<input type="submit" class="btn btn-primary submit-btn" data-dismiss="modal"
						value="{{trans("employee_recognition_board.Yes")}}">
				</div>
			</form>
		</div>
	</div>
</div>


<script type="text/javascript">
	(function($) {
		$('#recognition-table').DataTable({
        	"columnDefs": [
        		{ "orderable": false, "targets": [0, 4] }
        	]
    	});

		// remove the sorting class
    	$('.no-sorting-col').removeClass('sorting_asc');

		$('#bulk-del-btn').on('click', function() {
   			multipleCheckBox();
   		});

		$('#del-all').on('click',function(){
   			$('input:checkbox').not('#del-all').prop('checked', this.checked);

   			$('#bulk-del-btn').on('click', function() {
   				multipleCheckBox();
   			});
		});


		$('.add-recog-btn').on('click', function() {

			$.ajax({
				url: '{{ url("/get_employee_name_list") }}',
				type: 'GET',
				success: function(data) {
					// Auto Complete Feature
					var obj = $.parseJSON(data);

					$('#add-recog-autocomplete').autocomplete({
						minLength: 0,
						source:obj.employee_names,
						open: function(event, ui){
							var len = $('.ui-autocomplete > li').length;

							// shorten autocomplete list of items
							if(len > 4) {
								$('.ui-autocomplete').css({
									'height': '100px',
									'overflow-y': 'scroll',
									'overflow-x': 'hidden'
								});
							} else {
								$('.ui-autocomplete').css({
									'height': ''
								});
							}
						}
					}).focus(function(){     
            			$(this).autocomplete("search");
        			});


					$('#add-new-recognition').modal('show');

					// append the autocomplete list to modal form
					$('#add-recog-autocomplete').autocomplete("option", "appendTo", "#add-recog-form");

					$('#add-new-recognition .submit-btn').on('click', function(e) {

						var missingField = false;
						var validName = false;

						// Checking the empty value in required fields or not
						$('#add-recog-form').find('input').each(function(){
							if($(this).prop('required') && $(this).val() == '') {
								missingField = true;

								return;
							}
						});


						if(!missingField) {
							var selected_employee = $('#add-recog-autocomplete').val();

							for(var i = 0; i < obj.employee_names.length; i++) {
								if(obj.employee_names[i] == selected_employee) {
									$('#add-recog-employee-id').val(obj.employee_ids[i]);

									validName = true;
								}
							}

							if(!validName) {

								return false;

							} else {

								$('#add-new-recognition').modal('hide');

								$.ajax({
									url: '{{ url("/add_new_recognition") }}',
									type: 'POST',
									data: $('#add-recog-form').serialize(),
									success: function(data) {

									}
								});
							}
						}
					});
				}
			});
		});


		$('.edit-recog-btn').on('click', function() {
			
			var id = $(this).data('id');

			$.ajax({
				url: '{{ url("/get_employee_name_list") }}',
				type: 'GET',
				success: function(data) {
					// Auto Complete Feature
					var obj = $.parseJSON(data);

					$('#edit-recog-autocomplete').autocomplete({
						minLength: 0,
						source: obj.employee_names,
						open: function() {
							var len = $('.ui-autocomplete > li').length;

							// shorten autocomplete list of items
							if(len > 4) {

								$('.ui-autocomplete').css({
									'height': '100px',
									'overflow-y': 'scroll',
									'overflow-x': 'hidden'
								});
							} else {

								$('.ui-autocomplete').css({
									'height': ''
								});
							}
						}
					}).focus(function(){
						$(this).autocomplete("search");
					});

					$('#edit-recog-autocomplete').autocomplete("option", "appendTo", "#edit-recog-form");


					// get edited employee information
					$.ajax({
						url: '{{ url("/get_edited_employee_information") }}/' + id,
						type: 'GET',
						success: function(data) {
							// Fill employee information to editting form
							var employee_obj = $.parseJSON(data);

							$('#edit-recog-autocomplete').val(employee_obj.employee_name);
							$('#edit-recog-title').val(employee_obj.title);
							$('#edit-recog-message').val(employee_obj.message);
							$('#edit-recog-visibility').val(employee_obj.status);

							// show modal after filling data
							$('#edit-recognition').modal('show');

							// Submit edited form
							$('#edit-recognition .submit-btn').on('click', function() {
								
								var missingField = false;
								var validName = false;

								// Checking the empty value in required fields or not
								$('#edit-recog-form').find('input').each(function(){
									if($(this).prop('required') && $(this).val() == '') {
										missingField = true;
									}
								});

								if(!missingField) {

									// id of recognized employee
									var selected_employee = $('#edit-recog-autocomplete').val();

									for(var i = 0; i < obj.employee_names.length; i++) {
										if(obj.employee_names[i] == selected_employee) {
											$('#edit-recog-employee-id').val(obj.employee_ids[i]);
											validName = true;
											
											break;
										}
									}

									if(!validName) {
										// prevent submit the form
										return false;

									} else {

										$('#edit-recognition').modal('hide');

										$.ajax({
											url: '{{ url("/edit_recognition") }}/' + id,
											type: 'POST',
											data: $('#edit-recog-form').serialize(),
											success: function(data) {

											}
										});
									}
								}
							});
						}
					});	
				}
			});
		});


		$('.del-recog-btn').on('click', function(){

			$('#delete-recognition').modal('show');
			var id = $(this).data('id');

			$('#delete-recognition .submit-btn').on('click', function() {
				$('#delete-recognition').modal('hide');

        		$.ajax({
        			url: '{{ url("/delete_recognition") }}/' + id,
        			type: 'GET',
        			success: function(data) {
        				
        			}
        		});

			});
		});


		function multipleCheckBox() {
			var ids = [];

			$("input[name='recognition_row[]']:checked").each(function(){
   				var id = parseInt($(this).val());
   				ids.push(id);
   			});

			if(ids.length > 0) {

				$.ajax({
					url: '{{ url("/multiple_delete_recognition") }}',
					type: 'GET',
					data: {ids: ids},
					success: function(data) {

					}
				});
			}
		}
	}) (jQuery);

</script>

@endsection