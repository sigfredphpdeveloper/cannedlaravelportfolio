@extends('dashboard.layouts.master')

@section('content')

<section id="widget-grid" class="">
	<div class="row">
		<div class="col-md-12">
         	<a href="{{ url('/admin_settings') }}" class="btn btn-xs btn-default"><i class="fa fa-caret-left"></i> Back To Settings</a>

        	@if(Session::has('success'))
            	<div class="alert alert-success">
                	<p>{{ Session::get('success') }}</p>
            	</div>
        	@endif

        </div>


        <article class="col-sm-6 col-md-6 col-lg-6">
            <form  id="smart-form-register" class="smart-form client-form" role="form" 
            method="POST" action="{{ url('/save_role_permission_recognition_board') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

				<fieldset>
		            <div class="row">
		            	<h1>{{trans("employee_management.Employee Management")}}</h1>

			    		<section class="col-8">
			            	<label class="toggle">
			                <input type="checkbox" name="recognition_board" 
                           value="1" <?php echo ($company['recognition_board']=='1')?'checked="checked"':'';?> >
			                <i data-swchon-text="ON" data-swchoff-text="OFF"></i>
                            {{ trans("employee_recognition_board.Recognition Module") }}</label>
			            </section>

		            </div>

                    <div class="row">
                        <h1 style="margin: 10px 0px">{{ trans("employee_recognition_board.Manage Permission") }}</h1>

                        <table class="table table-striped">
                            <tbody>
                                <tr>
                                    <th>{{ trans("employee_recognition_board.Role") }}</th>
                                    <th>{{ trans("employee_recognition_board.Access To Recognition Module") }}</th>
                                </tr>

                                @foreach($roles as $role)
                                <tr>
                                    <td>{{ $role->role_name }}</td>

                                    @foreach($permissions as $permission)

                                    <td>
                                        <input type="checkbox" name="recognition_permissions_arr[]"

                                        <?php echo ($role['role_name'] =='Administrator')?'checked':'';?>
                                        <?php echo ($role['role_name'] =='Administrator')?'disabled':'';?>

                                        value="{{$role['id']}}-{{$permission['id']}}"
                                        <?php if(count($role->role_permissions) AND in_array($permission['id'],$role->role_permissions->pluck('id')->toArray())):?>
                                        checked="checked"
                                        <?php endif;?>

                                        />
                                    </td>
                                    @endforeach

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

		        </fieldset>
		           
                <footer>
                    <button type="submit" class="btn btn-primary">
                        {{trans("employee_recognition_board.Save")}}
                    </button>
                </footer>


            </form>

        </article>


	</div>
</section>

@endsection