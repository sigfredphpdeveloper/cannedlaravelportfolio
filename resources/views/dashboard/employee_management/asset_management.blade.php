@extends('dashboard.layouts.master')

@section('content')

<style>
	body .ui-autocomplete .ui-menu-item .ui-state-focus {
   		/* selected <a> */
   		background: grey!important;
	}

	/* Elements from Datatable JQuery plug-ins */
	.paginate_button {
		margin: 5px;
	}

	.paginate_button:hover {
		cursor: pointer;
	}

	.disabled:hover {
		color: white !important;
		cursor: default;
	}
</style>

<section id="widget-grid" class="">
	<div class="row">
		<div class="col-md-6">
		</div>
		<div class="col-md-6">
			@if($edit_permission OR $is_admin)
			<form><input id="asset-del-all-btn" class="btn btn-primary pull-right" 
				type="submit" style="margin-left:5px"
				value="{{ trans("employee_recognition_board.Bulk Delete") }}" />
			</form>
			@endif
			
			@if($edit_permission OR $is_admin)
			<button id="add-asset-btn" class="btn btn-primary pull-right" style="margin-left:5px" data-toggle="modal" data-target="#addNewAsset">
				{{ trans("employee_asset_management.Add New Asset")}}
			</button>
			@endif

			<?php if($is_admin):?>
				<a class="btn btn-primary pull-right" href="{{ url('/asset_management_settings') }}">
					{{ trans("employee_recognition_board.Configure") }}
				</a>
			<?php endif;?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<table id="asset-management-tbl" class="table table-striped">
				<thead>
					<tr>
						<th width="5%" class="no-sorting-col"><input id="del-all-asset" type="checkbox"></th>
						<th width="5%">{{ trans("employee_asset_management.Id") }}</th>
						<th width="20%">{{ trans("employee_asset_management.Title") }}</th>
						<th width="20%">{{ trans("employee_asset_management.Allocated To") }}</th>
						<th width="15%">{{ trans("employee_asset_management.Status") }}</th>
						<th width="15%">{{ trans("employee_asset_management.Purchase Date") }}</th>
						<th width="10%">{{ trans("employee_asset_management.Value") }}</th>
						<th width="10%">{{ trans("employee_asset_management.Note") }}</th>
						<th width="10%"></th>
					</tr>
				</thead>

				<tbody>
					@foreach($asset_list as $asset)
					<tr>
						<td><form><input type="checkbox" name="asset_row[]" value="{{ $asset->id }}"></form></td>
						<td>{{ $asset->id }}</td>
						<td>{{ $asset->title }}</td>
						<td>{{ $asset->e_first_name }} {{ $asset->e_last_name }}</td>
						<td>{{ $asset->status }}</td>
						@if($asset->purchase_date == NULL)
						<td>-</td>
						@else
						<td>{{ date('F d, y',strtotime($asset->purchase_date)) }}</td>
						@endif
						<td>{{ $asset->value }}</td>
						<td>{{ $asset->note }}</td>
						<td>
							@if($view_permission OR $edit_permission OR $is_admin)
							<a class="view-asset-btn icon view-icon" data-id="{{ $asset->id }}" style="cursor:pointer"></a>
							@endif

							@if($edit_permission OR $is_admin)
							<a class="edit-asset-btn icon edit-icon" data-id="{{ $asset->id }}" style="cursor:pointer"></a>
							<a class="del-asset-btn icon trash-icon" data-id="{{ $asset->id }}" style="cursor:pointer"></a>
							@endif
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</section>


<!-- Modal -->
<div id="add-new-asset-modal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">{{ trans("employee_asset_management.Add New Asset") }}</h4>
			</div>

			<form id="add-asset-form">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="modal-body">
					<div class="form-group">
						<label>{{trans("employee_asset_management.Asset Title *")}}</label>
						<input name="title" type="text" class="form-control" required>
					</div>
					<div class="form-group">
						<label>{{ trans("employee_asset_management.Asset Description") }}</label>
						<input name="description" type="text" class="form-control">
					</div>
					<div class="form-group">
						<label>{{ trans("employee_asset_management.Allocated To *") }}</label>
						<input id="add-asset-autocomplete" class="form-control" required>
						<input id="employee-asset-id" name="employee_id" type="hidden">
					</div>
					<div class="form-group">
						<label>{{ trans("employee_asset_management.Value") }}</label>
						<input name="value" type="text" class="form-control">
					</div>
					<div class="form-group">
						<label>{{ trans("employee_asset_management.Note") }}</label>
						<input name="note" type="text" class="form-control">
					</div>
					<div class="form-group">
						<label>{{ trans("employee_asset_management.Status") }}</label>
						<select name="status" class="form-control" id="select-asset-status">
							<option value="active" selected>{{ trans("employee_asset_management.Active") }}</option>
							<option value="sold / disposed">{{ trans("employee_asset_management.Sale / Disposal")}}</option>
						</select>
					</div>
					<div class="form-group" id="purchase-date-div">
						<label>{{ trans("employee_asset_management.Purchase Date") }}</label>
						<input id='purchase_date' name="purchase_date" class="form-control" type="hidden">
					</div>
					<div class="form-group" id="sale-div">
						<label>{{ trans("employee_asset_management.Date Of Sale / Disposal") }}</label>
						<input id="sale_disposal_date" name="sale_disposal_date" class="form-control" type="hidden">
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans("employee_recognition_board.Cancel")}}</button>
					<input type="submit" class="btn btn-primary submit-btn" value="{{trans('employee_recognition_board.Submit')}}">
				</div>
			</form>
		</div>
	</div>
</div>


<div id="edit-asset-modal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">{{ trans("employee_asset_management.Edit Asset") }}</h4>
			</div>

			<form id="edit-asset-form">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="modal-body">
					<div class="form-group">
						<label>{{trans("employee_asset_management.Asset Title *")}}</label>
						<input id="edit-asset-title" name="title" type="text" class="form-control" required>
					</div>
					<div class="form-group">
						<label>{{ trans("employee_asset_management.Asset Description") }}</label>
						<input id="edit-asset-des" name="description" type="text" class="form-control">
					</div>
					<div class="form-group">
						<label>{{ trans("employee_asset_management.Allocated To *") }}</label>
						<input id="edit-asset-autocomplete" class="form-control" required>
						<input id="edit-employee-asset-id" name="employee_id" type="hidden">
					</div>
					<div class="form-group">
						<label>{{ trans("employee_asset_management.Value") }}</label>
						<input id="edit-asset-val" name="value" type="text" class="form-control">
					</div>
					<div class="form-group">
						<label>{{ trans("employee_asset_management.Note") }}</label>
						<input id="edit-asset-note" name="note" type="text" class="form-control">
					</div>

					<div class="form-group">
						<label>{{ trans("employee_asset_management.Status") }}</label>
						<select name="status" class="form-control" id="edit-asset-status">
							<option value="active" selected>{{ trans("employee_asset_management.Active") }}</option>
							<option value="sold / disposed">{{ trans("employee_asset_management.Sale / Disposal")}}</option>
						</select>
					</div>
					<div class="form-group" id="edit-purchase-date-div">
						<label>{{ trans("employee_asset_management.Purchase Date") }}</label>
						<input id="edit-purchase-date" name="purchase_date" class="form-control" type="hidden">
					</div>
					<div class="form-group" id="edit-sale-div">
						<label>{{ trans("employee_asset_management.Date Of Sale / Disposal") }}</label>
						<input id="edit-sale-disposal-date" name="sale_disposal_date" class="form-control" type="hidden">
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">{{ trans("employee_recognition_board.Cancel")}}</button>
					<input type="submit" class="btn btn-primary submit-btn" value="{{trans('employee_recognition_board.Submit')}}">
				</div>
			</form>
		</div>
	</div>
</div>


<div id="delete-asset-modal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!--Modal Content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">{{trans("employee_recognition_board.Confirmation")}}</h4>
			</div>

			<div class="modal-body">
				<div class="form-group">
					<p>{{trans("employee_recognition_board.Are you sure you want to delete this record ?")}}</p>
				</div>
			</div>

			<form>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">
						{{trans("employee_recognition_board.No")}}
					</button>
					<input type="submit" class="btn btn-primary submit-btn" data-dismiss="modal"
						value="{{trans("employee_recognition_board.Yes")}}">
				</div>
			</form>
		</div>
	</div>
</div>


<div id="view-asset-modal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!--Modal Content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">{{trans("employee_asset_management.View Asset Details")}}</h4>
			</div>

			<div class="modal-body">
				<div class="form-group">
					<b>{{trans("employee_asset_management.Id")}}: </b>
					<span id="view-id-asset"></span>
				</div>
				<div class="form-group">
					<b>{{trans("employee_asset_management.Allocated Employee")}}: </b>
					<span id="view-employee-asset"></span>
				</div>
				<div class="form-group">
					<b>{{trans("employee_asset_management.Title")}}: </b>
					<span id="view-title-asset"></span>
				</div>
				<div class="form-group">
					<b>{{trans("employee_asset_management.Description")}}: </b>
					<label id="view-des-asset"></label>
				</div>
				<div class="form-group">
					<b>{{trans("employee_asset_management.Value")}}: </b>
					<label id="view-value-asset"></label>
				</div>

				<div class="form-group">
					<b>{{trans("employee_asset_management.Note")}}: </b>
					<label id="view-note-asset"></label>
				</div>



				<div class="form-group">
					<b>{{trans("employee_asset_management.Status")}}: </b>
					<label id="view-status-asset"></label>
				</div>
				<div class="form-group">
					<b>{{trans("employee_asset_management.Purchase Date")}}: </b>
					<label id="view-purchase-date-asset"></label>
				</div>
				<div class="form-group">
					<b>{{trans("employee_asset_management.Date Of Sale / Disposal")}}: </b>
					<label id="view-sale-date-asset"></label>
				</div>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">
					{{trans("employee_recognition_board.Cancel")}}
				</button>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	(function($) {
		$('#asset-management-tbl').DataTable({
			"columnDefs": [
        		{ "orderable": false, "targets": [0, 7] }
        	]
		});

		/* Date Time DropDown */
		$('#purchase_date').dateDropdowns({
			submitFormat: "yyyy-mm-dd"
		});

		$('#sale_disposal_date').dateDropdowns({
			submitFormat: "yyyy-mm-dd"
		});	
		

		// remove the sorting class
    	$('.no-sorting-col').removeClass('sorting_asc');

		$('#asset-del-all-btn').on('click', function() {
   			multipleAssetCheckBox();
   		});

		$('#del-all-asset').on('click',function(){
   			$('input:checkbox').not('#del-all-asset').prop('checked', this.checked);

   			$('#asset-del-all-btn').on('click', function() {
   				multipleAssetCheckBox();
   			});
		});


		/*------------Add New Asset------------*/
		$('#add-asset-btn').on('click', function() {
			$('#sale-div').hide();

			$.ajax({
				url: "{{ url('/get_employee_name_list') }}",
				type: "GET",
				success: function(data) {
					var obj = $.parseJSON(data);
					autocompleteDisplay($('#add-asset-autocomplete'), obj);

					// append the autocomplete list to modal form
					$('#add-asset-autocomplete').autocomplete("option", "appendTo", "#add-asset-form");

					/* Check the selected status */
					$('#select-asset-status').change(function(){

						if($('#select-asset-status :selected').text() == 'Sale / Disposal') {
							$('#sale-div').show();
						} else {
							$('#sale-div').hide();
						}
					});

					$('#add-new-asset-modal').modal('show');

					$('#add-new-asset-modal').on('submit', function(e){
						e.preventDefault();
					})

					$('#add-new-asset-modal .submit-btn').on('click', function(e) {

						var missingField = false;
						var validName = false;			
						var validDate = false;			

						// Checking the empty value in required fields or not
						$('#add-asset-form').find('input').each(function(){
							if($(this).prop('required') && $(this).val() == '') {
								missingField = true;
								return;
							}
						});

						validDate = validateDateFunction(
							$('#purchase-date-div'),
							$('#sale-div').is(':visible'),
							$('#sale-div'));

						if(!validDate) {
							return false;
						}


						if(!missingField) {
							var selected_employee = $('#add-asset-autocomplete').val();

							for(var i = 0; i < obj.employee_names.length; i++) {
								if(obj.employee_names[i] == selected_employee) {
									$('#employee-asset-id').val(obj.employee_ids[i]);

									validName = true;
								}
							}

							if(!validName) {
								return false;
							} else {

								$('#add-new-asset-modal').modal('hide');

								$.ajax({
									url: '{{ url("/add_new_asset") }}',
									type: 'POST',
									data: $('#add-asset-form').serialize(),
									success: function(data) {
										window.location.replace("{{ url('asset_management' )}}");
									}
								});
							}
						}
					});
				}
			});
		});


		/*--------------Edit Asset--------------*/
		$('.edit-asset-btn').on('click', function() {
			var id = $(this).data('id');
			$.ajax({
				url: "{{ url('/get_employee_name_list') }}",
				type: "GET",
				success: function(data) {
					var obj = $.parseJSON(data);
					autocompleteDisplay($('#edit-asset-autocomplete'), obj);

					$.ajax({
						url: "{{ url('/get_edited_asset') }}/" + id,
						type: "GET",
						success: function(data) {
							var asset_obj = $.parseJSON(data);

							$('#edit-asset-title').val(asset_obj.title);
							$('#edit-asset-des').val(asset_obj.description);
							$('#edit-asset-val').val(asset_obj.value);
							$('#edit-asset-note').val(asset_obj.note);



							$('#edit-asset-autocomplete').val(asset_obj.employee_name);
							$('#edit-employee-asset-id').val(asset_obj.employee_id);
							$('#edit-asset-status').val(asset_obj.status);

							$('#edit-purchase-date').dateDropdowns({
								defaultDate: asset_obj.purchase_date,
								submitFormat: "yyyy-mm-dd"
							});
							$('#edit-sale-disposal-date').dateDropdowns({
								defaultDate: asset_obj.sale_date,
								submitFormat: "yyyy-mm-dd"
							});

							// append the autocomplete list to modal form
							$('#edit-asset-autocomplete').autocomplete("option", "appendTo", "#edit-asset-form");

							/* Check the selected status before & during changes*/
							if($('#edit-asset-status :selected').text() == 'Sale / Disposal') {
								$('#edit-sale-div').show();
							} else {
								$('#edit-sale-div').hide();
							}

							$('#edit-asset-status').change(function(){

								if($('#edit-asset-status :selected').text() == 'Sale / Disposal') {
									$('#edit-sale-div').show();
								} else {
									$('#edit-sale-div').hide();
								}
							});
							/*--------------------------------------------------*/

							$('#edit-asset-modal').modal('show');

							$('#edit-asset-modal .submit-btn').on('click', function(e) {

								var missingField = false;
								var validName = false;
								var validDate = false;
								var validValue = false;

								// Checking the empty value in required fields or not
								$('#edit-asset-form').find('input').each(function(){
									if($(this).prop('required') && $(this).val() == '') {
										missingField = true;
										return;
									}
								});

								validDate = validateDateFunction(
									$('#edit-purchase-date-div'), 
									$('#edit-sale-div').is(':visible'),
									$('#edit-sale-div'));

								if(!validDate) {
									return false;
								}


								if(!missingField) {
									var selected_employee = $('#edit-asset-autocomplete').val();

									for(var i = 0; i < obj.employee_names.length; i++) {
										if(obj.employee_names[i] == selected_employee) {
											$('#edit-employee-asset-id').val(obj.employee_ids[i]);

											validName = true;
										}
									}

									if(!validName) {
										return false;
									} else {

										$('#edit-asset-modal').modal('hide');

										$.ajax({
											url: '{{ url("/edit_asset") }}/' + id,
											type: 'POST',
											data: $('#edit-asset-form').serialize(),
											success: function(data) {
											}
										});
									}
								}
							});
						}
					});
				}
			});
		});

		$('.del-asset-btn').on('click', function(){
			$('#delete-asset-modal').modal('show');
			var id = $(this).data('id');

			$('#delete-asset-modal .submit-btn').on('click', function(){
				$('#delete-asset-modal').modal('hide');

				$.ajax({
					url: "{{ url('/delete_asset') }}/" + id,
					type: "GET",
					success: function(data) {

					}
				});
			});
		});

		$('.view-asset-btn').on('click', function() {
			var id = $(this).data('id');

			$.ajax({
				url: "{{ url('/get_edited_asset') }}/" + id,
				type: 'GET',
				success: function(data) {
					var obj = $.parseJSON(data);

					$('#view-asset-modal').modal('show');
					$('#view-id-asset').text(obj.id);
					$('#view-title-asset').text(obj.title);
					$('#view-des-asset').text(obj.description);
					$('#view-employee-asset').text(obj.employee_name);
					$('#view-value-asset').text(obj.value);
					$('#view-note-asset').text(obj.note);
					$('#view-status-asset').text(obj.status);
					$('#view-purchase-date-asset').text(obj.purchase_date);
					$('#view-sale-date-asset').text(obj.sale_date);
				}
			});
		});

		
		function autocompleteDisplay(autocompleteField, obj) {
			autocompleteField.autocomplete({
				minLength: 0,
				source:obj.employee_names,
				open: function(event, ui){
					var len = $('.ui-autocomplete > li').length;

					// shorten autocomplete list of items
					if(len > 4) {
						$('.ui-autocomplete').css({
							'height': '100px',
							'overflow-y': 'scroll',
							'overflow-x': 'hidden'
						});
					} else {
						$('.ui-autocomplete').css({
							'height': ''
						});
					}
				}
			}).focus(function(){     
				$(this).autocomplete("search");
			});
		}


		function multipleAssetCheckBox() {
			var ids = [];

			$("input[name='asset_row[]']:checked").each(function(){
   				var id = parseInt($(this).val());
   				ids.push(id);
   			});

			if(ids.length > 0) {

				$.ajax({
					url: '{{ url("/multiple_delete_assets") }}',
					type: 'GET',
					data: {ids: ids},
					success: function(data) {

					}
				});
			}
		}


		function validateDateFunction(purchasedDate, isSaleDateVisible, saleDisposalDate) {
			// Check the valid date
			if((purchasedDate.find('.day').val() == "" &&
				purchasedDate.find('.month').val() == "" &&
				purchasedDate.find('.year').val() == "") ||

				(purchasedDate.find('.day').val() != "" &&
				purchasedDate.find('.month').val() != "" &&
				purchasedDate.find('.year').val() != "")) {

				return true;
			} else {
				alert('Invalid Purchase Date');
				return false;
			}


			if(isSaleDateVisible){
				if((saleDisposalDate.find('.day').val() == ""  &&
					saleDisposalDate.find('.month').val() == "" &&
					saleDisposalDate.find('.year').val() == "") ||

					(saleDisposalDate.find('.day').val() != ""  &&
					saleDisposalDate.find('.month').val() != "" &&
					saleDisposalDate.find('.year').val() != "")) {

					return true;
				} else {
					alert('Invalid Sale Disposal Date');				
					return false;
				}
			} else {
				return true;
			}
		}
	})(jQuery);
</script>

@endsection