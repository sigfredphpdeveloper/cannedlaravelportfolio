@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-md-6">

        <a href="{{ url('/leaves-my-account') }}" class="btn btn-white btn-xs"><i class="fa fa-caret-left"></i> My Account</a>
        @if($is_admin )
        <a href="{{ url('/manage-leaves') }}" class="btn btn-white btn-xs"><i class="fa fa-caret-left"></i> Manage Leaves</a>
        @endif

            <h1>{{trans("leaves.Leave Application")}}</h1>

            <p>{{ trans("leaves.Company Days OFF") }}:

            {{rtrim($work_weeks,' , ')}}

            </p>

        </div>

        <div class="col-md-6 pull-right">
            @if($is_admin OR $manage_leaves)
            <a href="{{ url('/leaves-setting') }}" class="btn btn-white pull-right" >
                <i class="fa fa-cog"></i> {{trans("leaves.Customize")}}</a>
            @endif
        </div>


        <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        </div>
        <div class="col-md-12">
            @if(Session::has('error'))
                <div class="alert alert-danger">
                    <p>{{ Session::get('error') }}</p>
                </div>
            @endif
            </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>{{trans("leaves.Warning")}}.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <article class="col-sm-6 col-md-6 col-lg-6">

            <form  id="submit_apply" class="smart-form client-form" role="form" method="POST" action="{{ url('/apply-leave-process') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                @include('admin.dashboard.leaves.form')

                <footer>
                    <button type="submit" class="btn btn-success" id="submit_apply_btn">
                         {{trans("leaves.Save")}}
                    </button>
                </footer>

            </form>

        </article>
    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->


@endsection


@section('page-script')
<script type="text/javascript" src="{{ url('/js/leaves.js') }} "></script>
@endsection