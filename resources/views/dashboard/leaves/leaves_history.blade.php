@extends('dashboard.layouts.master')

@section('content')

<!-- row -->
<div class="row">

    @include('errors.success')
    @include('errors.errors')

    <div class="col-md-6">
    <a href="{{ url('/leaves-my-account') }}" class="btn btn-white btn-xs"><i class="fa fa-caret-left"></i> My Account</a>
        @if($is_admin )
        <a href="{{ url('/manage-leaves') }}" class="btn btn-white btn-xs"><i class="fa fa-caret-left"></i> Manage Leaves</a>
        @endif

        <h1>{{trans("leaves.Leaves History")}}</h1>
    </div>

    <div class="col-md-6 pull-right">
        @if($is_admin OR $manage_leaves)
        <a href="{{ url('/leaves-setting') }}" class="btn btn-white pull-right" >
            <i class="fa fa-cog"></i> {{trans("leaves.Customize")}}</a>
        @endif
    </div>

</div>

<div class="row">

        <div class="col-md-3">
            <div class="input-group pull-left">
                <form class="form-inline" method="GET" id="filter_form">
                    <div class="form-group">
                        <div class="input-group">
                           <select name="status" id="filters" class="form-control">
                                <option value="">{{trans("leaves.Select Status")}}</option>
                                <option value="pending" <?=(!empty($_GET['status']) AND $_GET['status'] == 'pending')?'selected':'';?>>Pending</option>
                                <option value="approved" <?=(!empty($_GET['status']) AND $_GET['status'] == 'approved')?'selected':'';?>>Approved</option>
                                <option value="rejected" <?=(!empty($_GET['status']) AND $_GET['status'] == 'rejected')?'selected':'';?>>Rejected</option>
                           </select>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="col-md-9 pull-right">
            <div class="input-group pull-right">

                <form class="form-inline" method="GET">
                    <div class="form-group">
                        <div class="input-group">
                           <input type="text" name="search" id="search_box" class="form-control"
                              value="<?php echo (isset($search))?$search:'';?>" placeholder="{{trans("leaves.Enter Keywords")}}">
                            <span class="input-group-btn">
                            <button type="submit" class="btn btn-default">{{trans("leaves.Search")}}</button>
                            </span>
                        </div>
                    </div>
                </form>
            </div>

        </div>

    </div>
    <br />

<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>{{trans("leaves.Type")}}</th>
                    <th>{{trans("leaves.Date")}}</th>
                    <th>{{trans("leaves.Status")}}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php $passed = 0; ?>

                @foreach($user_leaves as $row)
                <tr>
                    <td>{{$row->type}}</td>
                    <td>

                    @if($row->leave_application_type=='single')

                        <?php
                            if(time() > strtotime($row->date)){
                                $passed = 1;
                            }
                        ?>
                        {{ date('M d, Y',strtotime($row->date)) }} ({{ $row->date_type }})
                    @else
                        {{ date('M d, Y',strtotime($row->date_from)) }} ({{ $row->date_from_type }}) -
                        {{ date('M d, Y',strtotime($row->date_to)) }} ({{ $row->date_to_type }})

                        <?php
                            if(time() > strtotime($row->date_to)){

                                $passed = 1;
                            }
                        ?>



                    @endif
                    </td>
                    <td>{{ $row->status }}</td>
                    <td>

                    <a href="javascript:void(0);" class="btn btn-primary btn-xs view_leave_record" data-id="{{$row->id}}">{{trans("leaves.View")}}</a>

                    @if($passed==0)
                     <a href="javascript:void(0);" class="btn btn-danger btn-xs cancel_leave_record" data-id="{{$row->id}}">{{trans("leaves.Cancel")}}</a>
                    @endif

                    </td>
                </tr>

                <?php $passed = 0; ?>

                @endforeach
            </tbody>
            <tfooot>
                <tr>
                    <td colspan="4"> {!! $user_leaves->render() !!}</td>
                </tr>
            </tfooot>

        </table>
    </div>
</div>




<!-- Modal -->
<div class="modal fade" id="view_leave" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans("leaves.Leave Details")}}</h4>
            </div>
            <div class="modal-body" id="view_leave_body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("leaves.Close")}}</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="cancel_leave_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans("leaves.Confirmation")}}</h4>
            </div>
            <div class="modal-body">
                <p>{{trans("leaves. you sure you want to cancel this leave record")}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("leaves.No")}}</button>
                <button type="button" class="btn btn-success" id="cancel_leave_proceed" >{{trans("leaves.Yes")}}</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('page-script')

<script type="text/javascript">

    $(document).ready(function(){

        $('.view_leave_record').click(function(){

            var id = $(this).data('id');

             $.ajax({
                url : '{{url('/view_leave_record/')}}/'+id,
                type : 'GET',
                data : 'id='+id,
                success:function(html){
                    $('#view_leave_body').html(html);
                    $('#view_leave').modal('show');
                }

            });


        });

        $('#filters').change(function(){
            $('#filter_form').submit();
        });


        $('.cancel_leave_record').click(function(){



            var id = $(this).data('id');

            $('#cancel_leave_proceed').attr('alt',id);
            $('#cancel_leave_modal').modal('show');

        });


        $('#cancel_leave_proceed').click(function(){

            var id = $(this).attr('alt');

            var dt={
                id : id,
                _token : '{{ csrf_token() }}'
            };

            $.ajax({
                url : '{{url('/cancel_leave_record/')}}',
                type: 'POST',
                data : dt,
                success:function(html){
                    window.location.reload();
                }
            });


        });

    });

</script>
@endsection