@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        @include('errors.success')
        @include('errors.errors')

        <div class="col-md-6">
            <a href="{{ url('/admin_settings') }}" class="btn btn-xs btn-default"><i class="fa fa-caret-left"></i>
            {{trans("leaves.Back To Settings")}} </a>

            <h1>{{trans("manage_teams.Manage Team")}}</h1>

        </div>
    </div>

    <div class="row">

        <div class="col-md-3">

            <?php if($edit_crt==TRUE):?>
                <a href="{{ url('/teams_add') }}" class="btn btn-primary">{{trans("manage_teams.Add Team")}}</a>
            <?php endif;?>

        </div>

        <div class="col-md-9 pull-right">
            <div class="input-group pull-right">

                <form class="form-inline" method="GET">
                    <div class="form-group">
                        <div class="input-group">
                           <input type="text" name="search" id="search_box" class="form-control"
                              value="<?php echo (isset($search))?$search:'';?>" placeholder="{{trans("manage_teams.Enter Keywords")}}">
                            <span class="input-group-btn">

                            <button type="submit" class="btn btn-default">{{trans("manage_teams.Search")}}</button>
                            </span>
                        </div>
                    </div>
                </form>

             
            </div>

        </div>

    </div>
    <br />
    <div class="row">
        <div class="col-md-12">

            <table class="table table-striped">

                <thead>
                    <tr>
                        <th>{{trans("manage_teams.Team")}}</th>
                        <th>{{trans("manage_teams.Team Description")}}</th>
                        <th width="200"></th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($teams as $i=>$item)
                    <tr>
                        <td>{{$item->team_name}}</td>
                        <td>{{$item->team_description}}</td>
                        <?php if($edit_crt==TRUE):?>
                        <td>
                            <a href="{{ url("teams_edit/") }}/{{ $item->id }}"><i class="icon edit-icon"></i></a>
                            <a href="javascript:void(0);" data-id="{{ $item->id }}" class="deleter"><i class="icon trash-icon"></i></a>
                        </td>
                        <?php endif;?>
                    </tr>
                    @endforeach

                </tbody>

                <tfooot>
                    <tr>
                        <td colspan="3"> {!! $teams->render() !!}</td>
                    </tr>
                </tfooot>

            </table>

        </div>

    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->

<script type="text/javascript">

    $(document).ready(function(){

        $('.deleter').click(function(){

            var id = $(this).data('id');
            $('#delete_proceed').attr('alt',id);
            $('#confirm').modal('show');

        });

        $('#delete_proceed').click(function(){

            var id = $(this).attr('alt');

            $('#confirm').modal('hide');

            window.location.href='{{ url('teams_delete') }}/'+id;

        });

    });

</script>

<div class="modal fade" tabindex="-1" role="dialog" id="confirm">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{trans("manage_teams.Confirmation")}}</h4>
      </div>
      <div class="modal-body">
        <p>{{trans("manage_teams.Are you sure you want to delete this record ?")}}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("manage_teams.No")}}</button>
       <button type="button" class="btn btn-primary" id="delete_proceed" alt="">{{trans("manage_teams.Yes")}}</button>

      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@endsection