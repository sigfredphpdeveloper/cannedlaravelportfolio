<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-sm-12 col-md-12">
        <h1>{{trans("leaves.Assign Leave Allowances")}}</h1>
        <p>( {{$user_names}} )</p>
        </div>

        <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>{{trans("leaves.Warning")}}.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <article class="col-sm-12 col-md-12 col-lg-12">

            <form  id="smart-form-register" class="smart-form client-form" role="form" method="POST" action="{{ url('/leave_summary_edit_bulk_save') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            @foreach($user_ids as $user_id)
                <input type="hidden" name="user_ids[]" value="{{$user_id}}" />
            @endforeach

                <fieldset>

                    @foreach($leave_types as $leave_type)
                    <section>
                        <label class="label">{{$leave_type['type']}} {{trans("leaves.Allowance")}}</label>
                        <label class="input">
                            <input type="text" name="leave_types[{{$leave_type['id']}}]" value="" required="required" class="form-control" />
                        </label>
                    </section>
                    @endforeach
                </fieldset>

                <footer>
                    <button type="submit" class="btn btn-success">
                         {{trans("leaves.Save")}}
                    </button>
                </footer>

            </form>

        </article>
    </div>

    <!-- end row -->

</section>
<style type="text/css">
.clsDatePicker {
    z-index: 99999  !important;
}
</style>

<script type="text/javascript">
    $(document).ready(function(){

        // START AND FINISH DATE
        $('#start_date_employee')
                .removeClass('hasDatepicker')
                .removeData('datepicker')
                .unbind()
                .datepicker({
                    dateFormat: 'dd-mm-yy',
                    prevText: '<i class="fa fa-chevron-left"></i>',
                    nextText: '<i class="fa fa-chevron-right"></i>'
                });

    });
</script>
<!-- end widget grid -->

