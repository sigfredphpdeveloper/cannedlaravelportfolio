@extends('dashboard.layouts.master')

@section('content')

<!-- row -->
<div class="row">

    @include('errors.success')
    @include('errors.errors')

</div>

<script type="text/javascript">

        function eventajaxmodal(id){

            var site_url = $('body').data('site-url');

            var fields = id.split(/-/);

            var event_id = fields[0];
            var user_id = fields[1];
            var login_id = '{{$user->id}}';

            <?php if(!$manage_leaves OR !$approve_leaves):?>
                if(user_id == login_id){

                        $.ajax({
                            url : site_url+'view_leave_record/'+event_id,
                            type : 'GET',
                            data : 'id='+id,
                            success:function(html){
                                $('#event_view_leave_body').html(html);
                                $('#event_view_leave').modal('show');
                            }
                        });

                }
            <?php else: ?>

                $.ajax({
                    url : site_url+'view_leave_record/'+event_id,
                    type : 'GET',
                    data : 'id='+id,
                    success:function(html){
                        $('#event_view_leave_body').html(html);
                        $('#event_view_leave').modal('show');
                    }

                });


            <?php endif;?>


        }


</script>
<style>
td.fc-other-month{
background-image:none !important;

}
</style>


<div class="row">

    <div class="col-md-6 ">
        <h1>Overview</h1>
    </div>

    <div class="col-md-6 pull-right">

            <a href="{{ url('/leaves-history') }}" class="btn btn-white pull-right" >REQUEST HISTORY</a>

            <a href="{{ url('/apply-leave') }}" class="btn btn-white pull-right" style="margin-right:20px;" >APPLY FOR LEAVE</a>


    </div>

</div>

<div class="row" style="margin-top:30px;">

    <div class="col-md-4">

        <div class="zen-panel text-center">
            <h1>REMAINING BALANCE</h1>

            <table class="table table-condensed">

            <tbody>
                @foreach($leave_types_collections as $item)
                <tr>
                    <td style="text-align: left">{{ $item['type'] }}</td>
                    <td>{{ $item['balance'] }}</td>
                </tr>
                @endforeach
            </tbody>

        </table>


            <a href="{{ url('/leaves-account') }}" class="btn btn-xs btn-white pull-right">View Details</a>

    <br style="clear:both;" />

        </div>

    </div>


    <div class="col-md-4">

        <div class="zen-panel text-center">
            <h4>PENDING REQUEST</h4>
            <h1>{{ $pending_user_leaves_count }}</h1>
        </div>


    </div>


    <div class="col-md-4">

        <div class="zen-panel text-center">
            <h4>UPCOMING LEAVE</h4>
            <h1>

                @if($upcoming_leave)

                {{ date('F d, Y',strtotime($upcoming_leave->date)) }}

                @endif

            </h1>
        </div>


    </div>


</div>

@if($view_calendar)
<div class="row" style="margin-top:50px;'">
    <div class="col-md-12">
        {!! $calendar->calendar() !!}
        {!! $calendar->script() !!}


    </div>

    <div class="col-md-4">

    <br />

            <h4>{{trans("leaves.Filter By")}}</h4>

            <form  id="smart-form-register" class="smart-form client-form" role="form" method="GET" action="">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <fieldset>

                <label class="label">{{trans("leaves.Employee")}}</label>
                <label class="input">
                   <select name="user_id"  class="form-control">
                        <option value="">{{trans("leaves.All")}}</option>
                       @foreach($users as $user)
                           <option value="{{$user->id}}"
                           <?=(!empty($_GET['user_id']) AND $_GET['user_id']==$user->id)?'selected':'';?>
                           >{{$user->first_name}} {{$user->last_name}}</option>
                       @endforeach
                   </select>
                </label>


                <label class="label">{{trans("leaves.Leave Type")}}</label>
                <label class="input">
                    <select name="type" class="form-control">
                        <option value="">{{trans("leaves.All")}}</option>
                        @foreach($leave_types as $lt)
                            <option value="{{$lt->id}}"
                            <?=(!empty($_GET['type']) AND $_GET['type']==$lt->id)?'selected':'';?>
                            >
                            {{ $lt->type }}
                            </option>
                        @endforeach
                     </select>
                </label>

                <label class="label">{{trans("leaves.Role")}}</label>
                <label class="input">
                   <select name="role"  class="form-control">
                        <option value="">{{trans("leaves.All")}}</option>
                       @foreach($roles as $role)
                           <option value="{{$role['id']}}"
                           <?=(!empty($_GET['role']) AND $_GET['role']==$role['id'])?'selected':'';?>
                           >{{$role['role_name']}}</option>
                       @endforeach
                   </select>
                </label>



                </fieldset>
                <br />

                <button type="submit" class="btn btn-white">{{trans("leaves.Filter")}}</button>
            </form>

        </div>


</div>
@endif

<script type="text/javascript" src="{{ asset('assets/js/fullcalendar.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/fullcalendar.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/fullcalendar.print.css') }}" />

<style type="text/css">
.fc-event-inner{
    cursor:pointer;
}
</style>

<!-- Modal -->
<div class="modal fade" id="event_view_leave" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans("leaves.Leave Details")}}</h4>
            </div>
            <div class="modal-body" id="event_view_leave_body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("leaves.Close")}}</button>
            </div>
        </div>
    </div>
</div>


@endsection