<!-- widget grid -->
<section id="widget-grid" class="">

    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>{{trans("leaves.Date")}}</th>
                        <th>{{trans("leaves.From")}}</th>
                        <th>{{trans("leaves.Note")}}</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($user_notes as $row)
                    <tr>
                        <td>{{ date('m-d-Y',strtotime($row->created_at))}}</td>
                        <td>{{$row->From_user->first_name }} {{$row->From_user->last_name }}</td>
                        <td>{{ $row->note }}</td>
                    </tr>
                    @endforeach
                </tbody>

            </table>
        </div>
    </div>

</section>
