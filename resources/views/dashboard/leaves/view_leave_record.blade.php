<table class="table">

    <tr>
        <td>{{trans("leaves.First Name")}}</td>
        <td>{{$leave[0]->first_name}}</td>
    </tr>
    <tr>
        <td>{{trans("leaves.Last Name")}}</td>
        <td>{{$leave[0]->last_name}}</td>
    </tr>

    <tr>
        <td>{{trans("leaves.Email")}}</td>
        <td>{{$leave[0]->email}}</td>
    </tr>

    <tr>
        <td>{{trans("leaves.Type")}}</td>
        <td>{{$leave[0]->type}}</td>
    </tr>

    @if($leave[0]->leave_application_type=='single')
    <tr>
        <td>{{trans("leaves.Date")}}</td>
        <td>{{ date('m-d-Y',strtotime($leave[0]->date)) }} ({{ $leave[0]->date_type }})</td>
    </tr>
    @else
    <tr>
        <td>{{trans("leaves.From")}}</td>
        <td>{{ date('m-d-Y',strtotime($leave[0]->date_from)) }} ({{ $leave[0]->date_from_type }})</td>
    </tr>

    <tr>
        <td>{{trans("leaves.To")}}</td>
        <td>{{ date('m-d-Y',strtotime($leave[0]->date_to)) }} ({{ $leave[0]->date_to_type }})</td>
    </tr>
    @endif

    <tr>
            <td>{{trans("leaves.Type")}}</td>
            <td>{{ucwords($leave[0]->leave_application_type)}}</td>
        </tr>

    <tr>
        <td>{{trans("leaves.Status")}}</td>
        <td>{{$leave[0]->status}}</td>
    </tr>
    <tr>
        <td>{{trans("leaves.Details")}}</td>
        <td>{{$leave[0]->details}}</td>
    </tr>
</table>
