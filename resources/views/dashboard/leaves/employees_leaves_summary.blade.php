@extends('dashboard.layouts.master')

@section('content')

<!-- row -->
<div class="row">

    @include('errors.success')
    @include('errors.errors')

    <div class="col-md-6">
        <a href="{{ url('/leaves-my-account') }}" class="btn btn-white btn-xs"><i class="fa fa-caret-left"></i> My Account</a>
        @if($is_admin )
        <a href="{{ url('/manage-leaves') }}" class="btn btn-white btn-xs"><i class="fa fa-caret-left"></i> Manage Leaves</a>
        @endif

        <h1>{{trans("leaves.Employees Leave Summary")}}</h1>
    </div>

    <div class="col-md-6 pull-right">
        @if($is_admin OR $manage_leaves)
        <a href="{{ url('/leaves-setting') }}" class="btn btn-white pull-right" >
            <i class="fa fa-cog"></i> {{trans("leaves.Customize")}}</a>
        @endif
    </div>

</div>


<div class="row">

        <div class="col-md-3">
            <a href='javascript:void(0);' id="bulk_edit"
            class="btn btn-success">{{trans("leaves.Bulk Edit")}}</a>

        </div>

        <div class="col-md-9 pull-right">
            <div class="input-group pull-right">

                <form class="form-inline" method="GET">
                    <div class="form-group">
                        <div class="input-group">
                           <input type="text" name="search" id="search_box" class="form-control"
                              value="<?php echo (isset($search))?$search:'';?>" placeholder="{{trans("leaves.Enter Keywords")}}">
                            <span class="input-group-btn">
                            <button type="submit" class="btn btn-default">{{trans("leaves.Search")}}</button>
                            </span>
                        </div>
                    </div>
                </form>
            </div>

        </div>

    </div>
    <br />

<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">

                <tr>
                    <th></th>
                    <th>{{trans("leaves.Name")}}</th>
                    @foreach($employees['leave_types'] as $leave_type)
                    <th style="text-align:center;">Allowance</th>
                    <th style="text-align:center;border-right:2px solid #d1d1d1;">Remaining</th>
                    @endforeach
                    <th></th>
                </tr>


            <thead>

                 <tr>
                    <th><input type="checkbox" id="check-all"></th>
                    <th></th>
                    @foreach($employees['leave_types'] as $leave_type)
                    <th colspan="2" style="text-align:center;border-right:2px solid #E67D44;" data-id="{{$leave_type['id']}}">{{$leave_type['type']}}</th>
                    @endforeach
                    <th></th>
                </tr>


            </thead>
            <tbody>

                <?php $user_roles = []; ?>
                @foreach($employees['users'] as $row)

                <tr>
                    <td><input type="checkbox" name="users[]" value="{{$row['id']}}" class="checkboxes" /></td>
                    <td>
                    {{$row['first_name']}} {{ $row['last_name'] }}
                    </td>


                    @foreach($employees['leave_types'] as $key=>$leave_type)

                    <?php $allowance = ($row['allowance-'.$leave_type['id']]);?>
                    <td style="text-align:center;"><?php echo (!empty($allowance[0]->allowance))?$allowance[0]->allowance:'-';?></td>
                    <td style="text-align:center;"><?php echo (!empty($allowance[0]->remaining))?$allowance[0]->remaining:'-';?></td>
                    @endforeach

                    <td>

                    <div class="btn-group">
                        <button type="button" class="btn btn btn-success dropdown-toggle" data-toggle="dropdown">
                        Actions <span class="caret"></span></button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href='javascript:void(0);' data-user-id="{{$row['id']}}" class="btn-edit">{{trans("leaves.Edit")}}</a></li>
                            <li><a href='javascript:void(0);' data-user-id="{{$row['id']}}" class="btn-add-note">{{trans("leaves.Add Notes")}}</a></li>
                            <li><a href='javascript:void(0);' data-user-id="{{$row['id']}}" class="btn-view-note">{{trans("leaves.View Notes")}}</a></li>
                            <li><a href='javascript:void(0);' data-name="{{$row['first_name']}} {{ $row['last_name'] }}" data-user-id="{{$row['id']}}" data-user-id="{{$row['id']}}" data-work-week-id="{{ $row['work_week_id'] }}" class="btn-edit-workweek">Edit Work Week</a></li>
                        </ul>
                      </div>


                    </td>
                </tr>
                @endforeach
            </tbody>

        </table>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="view_leave" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans("leaves.Leave Details")}}</h4>
            </div>
            <div class="modal-body" id="view_leave_body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("leaves.Close")}}</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="leave_summary_edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans("leaves.Leave Summary")}}</h4>
            </div>
            <div class="modal-body" id="leave_summary_edit_body">

            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="user_leave_notes_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans("leaves.Leave Notes")}}</h4>
            </div>
            <div class="modal-body" id="user_leave_notes_body">

            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="bulk_edit_modal_warning" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" >{{trans("leaves.Warning")}}</h4>
            </div>
            <div class="modal-body" >
                <p>{{trans("leaves.Please check atleast one checkbox")}}</p>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="leave_summary_edit_bulk_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans("leaves.Leave Summary")}}</h4>
            </div>
            <div class="modal-body" id="leave_summary_edit_bulk_body">

            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="add_leave_notes_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans("leaves.Leave Notes")}}</h4>
            </div>
            <div class="modal-body" id="add_leave_notes_body">

            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="user_work_week_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Assign Work Week to <span id="user_name_work_week" style="font-weight:bold;"></span></h4>
            </div>
            <div class="modal-body">

                <form class="form-inline" action="{{ url('/update_user_work_week') }}" method="POST">
                    <input type="text" name="user_id" style="display:none;" id="select_work_week_user_id" value="" />
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="form-group">
                        <div class="input-group">

                            <select name="work_week_id" id="select_work_week_id" class="form-control" style="width:200px;">

                                <option value="">--- Select Work Week ---</option>
                                @foreach($compay_work_week as $week)
                                    <option value="{{$week->id}}">{{$week->title}}</option>
                                @endforeach

                            </select>
                            <span class="input-group-btn">
                            <button type="submit" class="btn btn-default">{{trans("leaves.Save")}}</button>
                            </span>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>



@endsection

@section('page-script')

<script type="text/javascript">

    $(document).ready(function(){


        //leave_summary_edit btn-edituser-id
            //leave_summary_edit_modal leave_summary_edit_body

        $('.btn-edit').click(function(){

            var user_id = $(this).data('user-id');
            $.ajax({
                url : '{{url('/leave_summary_edit/')}}/'+user_id,
                type : 'GET',
                data : 'id='+user_id,
                success:function(html){
                    $('#leave_summary_edit_body').html(html);
                    $('#leave_summary_edit_modal').modal('show');
                }

            });


        });

        $('.view_leave_record').click(function(){

            var id = $(this).data('id');

             $.ajax({
                url : '{{url('/view_leave_record/')}}/'+id,
                type : 'GET',
                data : 'id='+id,
                success:function(html){
                    $('#view_leave_body').html(html);
                    $('#view_leave').modal('show');
                }

            });


        });

        $('.approver').click(function(){

            var id = $(this).data('id');

            $('#proceed_approve').attr('alt',id);
            $('#confirm_approve').modal('show');



        });

        $('#proceed_approve').click(function(){
            var id = $(this).attr('alt');

            var dt = {
                'id':id,
                'status':'approved',
                '_token': '{{ csrf_token() }}'
            };

            $.ajax({
                url : '{{url('/update_leave_status/')}}',
                type : 'POST',
                data : dt,
                success:function(html){
                    window.location.reload();
                }

            });


        });


        $('.reject').click(function(){

            var id = $(this).data('id');

            $('#proceed_reject').attr('alt',id);
            $('#confirm_reject').modal('show');
        });

        $('#proceed_reject').click(function(){
            var id = $(this).attr('alt');
            var dt = {
                'id':id,
                'status':'rejected',
               '_token': '{{ csrf_token() }}'
            };
            $.ajax({
                url : '{{url('/update_leave_status/')}}',
                type : 'POST',
                data : dt,
                success:function(html){
                    window.location.reload();
                }
            });
        });


        $('.reject').click(function(){

            var id = $(this).data('id');

        });

        // START AND FINISH DATE
        $('.startdate').datepicker({
            dateFormat: 'dd-mm-yy',
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>'
        });



        $("#check-all").change(function(){

            $('.checkboxes').not(this).prop('checked', this.checked);

        });

        $('#bulk_edit').click(function(){

            if ($(".checkboxes:checked").length == 0)
            {
                $('#bulk_edit_modal_warning').modal('show');
            }
            else
            {


                var user_array = new Array();
                $(".checkboxes:checked").each(function() {
                   user_array.push($(this).val());
                });


                var user_id = $(this).data('user-id');
                var dt= {
                    user_ids :user_array,
                   '_token': '{{ csrf_token() }}'
                };

                $.ajax({
                    url : '{{url('/leave_summary_edit_bulk')}}',
                    type : 'POST',
                    data : dt,
                    success:function(html){

                        $('#leave_summary_edit_bulk_body').html(html);
                        $('#leave_summary_edit_bulk_modal').modal('show');
                    }

                });

            }


        });



        $('.btn-view-note').click(function(){

            var user_id = $(this).data('user-id');

            $.ajax({
                url : '{{url('/user_leave_notes/')}}/'+user_id,
                type : 'GET',
                data : 'id='+user_id,
                success:function(html){
                    $('#user_leave_notes_body').html(html);
                    $('#user_leave_notes_modal').modal('show');
                }
            });

        });

        $('.btn-add-note').click(function(){

            var user_id = $(this).data('user-id');
            $.ajax({
                url : '{{url('/add_leave_note/')}}/'+user_id,
                type : 'GET',
                data : 'id='+user_id,
                success:function(html){
                    $('#add_leave_notes_body').html(html);
                    $('#add_leave_notes_modal').modal('show');
                }

            });


        });

        $('.btn-edit-workweek').click(function(){

            var user_id = $(this).data('user-id');
            var work_week_id = $(this).data('work-week-id');
            var user_name = $(this).data('name');

            $('#user_name_work_week').html(user_name);

            $('#select_work_week_user_id').val(user_id);
            $('#select_work_week_id').val(work_week_id);
            $('#user_work_week_modal').modal('show');

        });



    });

</script>
@endsection