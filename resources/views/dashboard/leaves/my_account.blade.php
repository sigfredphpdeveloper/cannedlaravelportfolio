@extends('dashboard.layouts.master')

@section('content')

<!-- row -->
<div class="row">

    @include('errors.success')
    @include('errors.errors')

</div>

<div class="row">

    <div class="col-md-6 ">
    <a href="{{ url('/leaves-my-account') }}" class="btn btn-white btn-xs"><i class="fa fa-caret-left"></i> My Account</a>
        @if($is_admin )
        <a href="{{ url('/manage-leaves') }}" class="btn btn-white btn-xs"><i class="fa fa-caret-left"></i> Manage Leaves</a>
        @endif

        <h1>{{trans("leaves.Year")}} <?=date('Y')?></h1>
    </div>

    <div class="col-md-6 pull-right">
        @if($is_admin OR $manage_leaves)
        <a href="{{ url('/leaves-setting') }}" class="btn btn-white pull-right" >
            <i class="fa fa-cog"></i> {{trans("leaves.Customize")}}</a>
        @endif
    </div>

    <div class="col-md-12">

        <table class="table table-striped">
            <thead>
                <tr>
                    <th>{{trans("leaves.Type")}}</th>
                    <th>{{trans("leaves.Allowance")}}</th>
                    <th>{{trans("leaves.Pending")}}</th>
                    <th>{{trans("leaves.Approved")}}</th>
                    <th>{{trans("leaves.Balance")}}</th>
                    <th>{{trans("leaves.Details")}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($leave_types_collections as $item)
                <tr>
                    <td>{{ $item['type'] }}</td>
                    <td>{{ $item['allowance'] }}</td>
                    <td>{{ $item['planned'] }}</td>
                    <td>{{ $item['used'] }}</td>
                    <td>{{ $item['balance'] }}</td>
                    <td>
                        <a href="{{ url('/leaves-history') }}?type={{$item['type_id']}}" class="btn btn-success">{{trans("leaves.Details")}}</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</div>


<div class="row">

    <div class="col-md-6 ">
        <h1>Leave Notes</h1>
    </div>

    <div class="col-md-6 pull-right">

    </div>

    <div class="col-md-12">



            <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>{{trans("leaves.Date")}}</th>
                                    <th>{{trans("leaves.From")}}</th>
                                    <th>{{trans("leaves.Note")}}</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($user_notes as $row)
                                <tr>
                                    <td>{{ date('m-d-Y',strtotime($row->created_at))}}</td>
                                    <td>{{$row->From_user->first_name }} {{$row->From_user->last_name }}</td>
                                    <td>{{ $row->note }}</td>
                                </tr>
                                @endforeach
                            </tbody>

                        </table>


    </div>

</div>

@endsection