@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-md-12">
        <a href="{{ url('/admin_settings') }}" class="btn btn-xs btn-default"><i class="fa fa-caret-left"></i> {{trans("leaves.Back To Settings")}}</a>

            @if(Session::has('success'))
                <div class="alert alert-block alert-success">
                    <a class="close" data-dismiss="alert" href="#">×</a>
                    <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('success') }}</h4>
                </div>
            @elseif(Session::has('error'))
                 <div class="alert alert-block alert-danger">
                    <a class="close" data-dismiss="alert" href="#">×</a>
                    <h4 class="alert-heading"><i class="fa-fw fa fa-times"></i> {{ Session::get('error') }}</h4>
                </div>
            @elseif(Session::has('max-error'))
                <div class="alert alert-block alert-danger">
                    <a class="close" data-dismiss="alert" href="#">×</a>
                    <h4 class="alert-heading"><i class="fa-fw fa fa-times"></i> {{ Session::get('max-error') }}</h4>
                </div>
            @endif

    <br />
    <br />
        </div>



    <br />
    @if( $manage_leaves AND isset($company) AND $company['leaves']==1 )

        <div class="col-md-6">


            <div class="well">
                <h3 class="pull-left">{{trans("leaves.Manage Leave Types")}}</h3>
                <a  href="{{url('/leave-types-add')}}" class="btn btn-success pull-right">{{trans("leaves.Add")}}</a>

                <table class="table">
                    <tr>
                    <th>{{trans("leaves.Leave Type")}}</th>
                    <th>{{trans("leaves.Description")}}</th>
                    <th></th>
                    </tr>
                    @foreach($leave_types as $lt)
                        <tr>
                            <td>{{$lt->type}}</td>
                            <td>{{$lt->description}}</td>
                            <td><a href="{{ url('/leave-types-edit') }}/{{$lt->id}}" class=" btn btn-xs btn-primary">{{trans("leaves.Edit")}}</a></td>
                        </tr>
                    @endforeach
                </table>

            </div>
        </div>

    @endif


        <div class="col-md-6">

                <div class="well">

                    <form action="{{url('/save_role_permission_leaves')}}" method="POST" >

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <h3 >{{trans("leaves.Leaves Permission")}}</h3>

                            <?php
                                $count = count($permissions);
                            ?>

                            @include('dashboard.layouts.permission_select')

                            <input type="submit" value="{{trans("leaves.Save")}}" class="btn btn-primary" data-toggle="tooltip" title="Save" />

                    </form>
                </div>
        </div>

        <div class="col-md-6">
            <div class="well">
                <h3 >Leave Module - General Settings</h3>
                <form  id="smart-form-register" class="smart-form client-form" role="form"
                method="POST" action="{{ url('/leaves-setting-save') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="user_id" value="{{ $user['id'] }}">

                        <fieldset>
                        <div class="row">

                            <section class="col col-8">
                                 <label class="toggle">
                                       <input type="checkbox" name="leaves"
                                       value="1" <?php echo ($company['leaves']=='1')?'checked="checked"':'';?> >
                                       <i data-swchon-text="ON" data-swchoff-text="OFF"></i> Enable the Leave Module
                                    </label>
                             </section>

                        </div>

                       </fieldset>

                       <fieldset>
                        <div class="row">

                            <section class="col col-8">
                                 <label class="toggle">
                                       <input type="checkbox" name="leaves_halfday"
                                       value="1" <?php echo ($company['leaves_halfday']=='1')?'checked="checked"':'';?> >
                                       <i data-swchon-text="ON" data-swchoff-text="OFF"></i> {{trans("leaves.Allow Half Day")}}
                                    </label>
                             </section>

                        </div>

                       </fieldset>

                        <fieldset style="display:none;">
                        <div class="row">

                            <section class="col col-8">
                                 <label class="toggle">
                                       <input type="checkbox" name="leaves_advance"
                                       value="1" <?php echo ($company['leaves_advance']=='1')?'checked="checked"':'';?> >
                                       <i data-swchon-text="ON" data-swchoff-text="OFF"></i> {{trans("leaves.Allow Advanced Leaves")}}
                                    </label>
                             </section>

                        </div>

                       </fieldset>


                        <input type="submit" value="{{trans("leaves.Save")}}" class="btn btn-primary" data-toggle="tooltip" title="Save" />

                </form>

            </div>
        </div>

            <div class="col-md-6">

                <div class="well">


                    <h3 class="pull-left">Company Work Weeks</h3>

                        <a href="javascript:void(0);" class="btn btn-primary pull-right btn-add-work-week btn-xs" >ADD</a>

                        <br style="clear:both;" />
                    <table class="table">

                        @foreach($compay_work_week as $val)

                        <tr>
                            <td>{{$val->title}}</td>
                            <td>

                                <a href="javascript:void(0);" class="btn btn-primary btn-edit btn-xs" data-id="{{ $val->id }}">EDIT</a>
                                <a href="javascript:void(0);" class="btn btn-primary btn-delete btn-xs" data-id="{{ $val->id }}">DELETE</a>

                            </td>
                        </tr>

                        @endforeach


                    </table>

                </form>

            </div>
        </div>


        @if($is_admin)
         <div class="col-md-6">

            <div class="well" style="min-height:150px;">
                <h3 class="pull-left">{{trans("leaves.Export Data")}}</h3>
                <a  href="{{url('/export_leaves_data')}}" class="btn btn-success pull-right">{{trans("leaves.Export Leaves Data")}}</a>

            </div>

        </div>

        @endif





    </div>
        <!-- end row -->
</section>

 <div class="modal fade" id="edit_company_work_week_modal" tabindex="-1" role="dialog" aria-labelledby="edit_company_work_week_modal">
    <div class="modal-dialog" role="document">
        <div id="edit_company_work_week_modal-content" class="modal-content">

        </div>
    </div>
</div>

 <div class="modal fade" id="add_company_work_week_modal" tabindex="-1" role="dialog" aria-labelledby="add_company_work_week_modal">
    <div class="modal-dialog" role="document">
        <div  class="modal-content">

            @include('dashboard.leaves.company_work_week_add')

        </div>
    </div>
</div>

 <div class="modal fade" id="delete_confirm_modal" tabindex="-1" role="dialog" aria-labelledby="add_company_work_week_modal">
    <div class="modal-dialog" role="document">
        <div  class="modal-content">

           <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{trans("manage_announcements.Confirmation")}}</h4>
          </div>
          <div class="modal-body">
            <p>Are you sure you want to delete this work week?</p>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("manage_announcements.No")}}</button>
           <button type="button" class="btn btn-primary" id="delete_proceed" alt="">{{trans("manage_announcements.Yes")}}</button>

          </div>

        </div>
    </div>
</div>

@endsection

@section('page-script')

<script type="text/javascript" src="{{ url('/js/expenses-settings.js') }}"></script>

<script type="text/javascript">

    $(document).ready(function(){

        $('.btn-edit').click(function(){

            var id =$(this).data('id');

            $.ajax({
                url:'{{ url('/company_work_week_edit') }}',
                data:'id='+id,
                type:'GET',
                success:function(response){

                    $('#edit_company_work_week_modal-content').html(response);
                    $('#edit_company_work_week_modal').modal('show');

                }

            });

        });

        $('.btn-add-work-week').click(function(){

            $('#add_company_work_week_modal').modal('show');

        });

        $('.btn-delete').click(function(){

            var id = $(this).data('id');

            $('#delete_proceed').attr('alt',id);
            $('#delete_confirm_modal').modal('show');

        });

        $('#delete_proceed').click(function(){

            var id = $(this).attr('alt');

            window.location.href='{{ url('/delete_company_work_week') }}/'+id;

        });


    });


</script>

@endsection