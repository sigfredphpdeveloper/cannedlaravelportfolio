@extends('dashboard.layouts.master')

@section('content')

<script type="text/javascript">

        function eventajaxmodal(id){

            var site_url = $('body').data('site-url');

            var fields = id.split(/-/);

            var event_id = fields[0];
            var user_id = fields[1];
            var login_id = '{{$user->id}}';

            <?php if(!$manage_leaves OR !$approve_leaves):?>
                if(user_id == login_id){

                        $.ajax({
                            url : site_url+'view_leave_record/'+event_id,
                            type : 'GET',
                            data : 'id='+id,
                            success:function(html){
                                $('#event_view_leave_body').html(html);
                                $('#event_view_leave').modal('show');
                            }
                        });

                }
            <?php else: ?>

                $.ajax({
                    url : site_url+'view_leave_record/'+event_id,
                    type : 'GET',
                    data : 'id='+id,
                    success:function(html){
                        $('#event_view_leave_body').html(html);
                        $('#event_view_leave').modal('show');
                    }

                });


            <?php endif;?>


        }


</script>
<style>
td.fc-other-month{
background-image:none !important;

}
</style>
<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        @include('errors.success')
        @include('errors.errors')

        <div class="col-md-6">
            <h1>{{trans("leaves.Calendar")}}</h1>
        </div>
        <div class="col-md-6 pull-right">
            @if($is_admin OR $manage_leaves)
            <a href="{{ url('/leaves-setting') }}" class="btn btn-primary pull-right" >
                <i class="fa fa-cog"></i> {{trans("leaves.Customize")}}</a>
            @endif
        </div>

    </div>




    <div class="row">
        <div class="col-md-12">
            {!! $calendar->calendar() !!}
            {!! $calendar->script() !!}

            <h4>{{trans("leaves.Filter By")}}</h4>


        </div>

        <div class="col-md-4">

            <form  id="smart-form-register" class="smart-form client-form" role="form" method="GET" action="">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <fieldset>

                <label class="label">{{trans("leaves.Employee")}}</label>
                <label class="input">
                   <select name="user_id"  class="form-control">
                        <option value="">{{trans("leaves.All")}}</option>
                       @foreach($users as $user)
                           <option value="{{$user->id}}"
                           <?=(!empty($_GET['user_id']) AND $_GET['user_id']==$user->id)?'selected':'';?>
                           >{{$user->first_name}} {{$user->last_name}}</option>
                       @endforeach
                   </select>
                </label>


                <label class="label">{{trans("leaves.Leave Type")}}</label>
                <label class="input">
                    <select name="type" class="form-control">
                        <option value="">{{trans("leaves.All")}}</option>
                        @foreach($leave_types as $lt)
                            <option value="{{$lt->id}}"
                            <?=(!empty($_GET['type']) AND $_GET['type']==$lt->id)?'selected':'';?>
                            >
                            {{ $lt->type }}
                            </option>
                        @endforeach
                     </select>
                </label>

                <label class="label">{{trans("leaves.Role")}}</label>
                <label class="input">
                   <select name="role"  class="form-control">
                        <option value="">{{trans("leaves.All")}}</option>
                       @foreach($roles as $role)
                           <option value="{{$role['id']}}"
                           <?=(!empty($_GET['role']) AND $_GET['role']==$role['id'])?'selected':'';?>
                           >{{$role['role_name']}}</option>
                       @endforeach
                   </select>
                </label>



                </fieldset>
                <br />

                <button type="submit" class="btn btn-success">{{trans("leaves.Filter")}}</button>
            </form>

        </div>

    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->

<script type="text/javascript" src="{{ asset('assets/js/fullcalendar.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/fullcalendar.min.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/fullcalendar.print.css') }}" />

<style type="text/css">
.fc-event-inner{
    cursor:pointer;
}
</style>



<!-- Modal -->
<div class="modal fade" id="event_view_leave" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans("leaves.Leave Details")}}</h4>
            </div>
            <div class="modal-body" id="event_view_leave_body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("leaves.Close")}}</button>
            </div>
        </div>
    </div>
</div>


@endsection



@section('page-script')

<script type="text/javascript">

    $(document).ready(function(){

        $('.fc-event-time').each(function(){
            $(this).remove();
//            var me = $(this);
//            var id = $(this).html();
//            me.html(id.slice(0,-1));
        })
    });

</script>

@endsection