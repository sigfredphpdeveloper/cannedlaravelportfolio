@extends('dashboard.layouts.master')

@section('content')

<!-- row -->
<div class="row">

    @include('errors.success')
    @include('errors.errors')

</div>



<div class="row">

    <div class="col-md-6 ">
        <h1>Overview</h1>
    </div>

    <div class="col-md-6 pull-right">

        @if($is_admin)
            <a href="{{ url('/leaves-setting') }}" class="pull-right btn btn-white">Customize</a>
        @endif
    </div>

</div>

<div class="row" style="margin-top:30px;">

    <div class="col-md-4">

        <div class="zen-panel text-center">

            <h1>PENDING REQUESTS</h1>

            <h1>{{ $company_pending_leaves }}</h1>

        </div>

    </div>


    <div class="col-md-4">



    </div>


    <div class="col-md-4">


    </div>


</div>


<div class="row" style="margin-top:30px;">

    <div class="col-md-12" >

        <a href="{{ url('approve-leaves') }}"  class="btn btn-white">Pending Requests</a>


        <a href="{{ url('all_leaves') }}" style="margin-left:20px;" class="btn btn-white">All Leave History</a>


        <a href="{{ url('leaves-summary') }}" style="margin-left:20px;" class="btn btn-white">All Leave Allowance</a>

    </div>


</div>



<div class="row" style="margin-top:30px;">

    <div class="col-md-4" >

        <h1>Select Employee</h1>

            <table class="table">

            @foreach($employees['users'] as $row)

                @if($row['first_name'].''.$row['last_name'] != '')

                <tr data-url="{{ url('/manage-leaves') }}?user_id={{$row['id']}}" class="tr_navigate"
                style="{{ (!empty($_GET['user_id']) AND $_GET['user_id'] == $row['id'])?'background:#F3F3F3;':'' }}cursor:pointer">

                    <td
                    style="color:black;{{ (!empty($_GET['user_id']) AND $_GET['user_id'] == $row['id'])?'font-weight:bold;':'' }}"
                    >{{ $row['first_name'] }} {{ $row['last_name'] }}</td>

                    </a>
                </tr>
                @endif
            @endforeach

            </table>


    </div>

    <div class="col-md-8">


        @if(!$user_leaves)

            <h1>Select an employee as the left side</h1>

        @else

        <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#overview" aria-controls="home" role="tab" data-toggle="tab">Overview</a></li>
            <li role="presentation"><a href="#leaves_history" aria-controls="profile" role="tab" data-toggle="tab">Leaves History</a></li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="overview">


                <h1>Overview  - {{ $employee->first_name }} {{ $employee->last_name }}
                <span style="margin-left:30px;font-size:15px;">Work week : {{ $employee->work_week->title }}</span></h1>

                <table class="table table-striped">

                <thead>

                     <tr>
                        <th>Leave Type</th>
                        <th style="text-align:center;">Allowance</th>
                        <th style="text-align:center;">Remaining</th>
                    </tr>

                </thead>
                <tbody>

                    <?php $user_roles = []; ?>
                    @foreach($employees['users'] as $row)

                    @if($row['id'] == $employee->id)


                        @foreach($employees['leave_types'] as $key=>$leave_type)

                        <tr>

                            <?php $allowance = ($row['allowance-'.$leave_type['id']]);?>

                            <td>{{$leave_type['type']}}</td>
                            <td style="text-align:center;"><?php echo (!empty($allowance[0]->allowance))?$allowance[0]->allowance:'-';?></td>
                            <td style="text-align:center;"><?php echo (!empty($allowance[0]->remaining))?$allowance[0]->remaining:'-';?></td>

                        </tr>

                        @endforeach

                        <tr>
                            <td colspan="3">

                                <div class="btn-group">
                                    <button type="button" class="btn btn btn-white dropdown-toggle" data-toggle="dropdown">
                                    Actions <span class="caret"></span></button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href='javascript:void(0);' data-user-id="{{$row['id']}}" class="btn-edit">{{trans("leaves.Edit")}}</a></li>
                                        <li><a href='javascript:void(0);' data-user-id="{{$row['id']}}" class="btn-add-note">{{trans("leaves.Add Notes")}}</a></li>
                                        <li><a href='javascript:void(0);' data-user-id="{{$row['id']}}" class="btn-view-note">{{trans("leaves.View Notes")}}</a></li>
                                        <li><a href='javascript:void(0);' data-name="{{$row['first_name']}} {{ $row['last_name'] }}" data-user-id="{{$row['id']}}" data-user-id="{{$row['id']}}" data-work-week-id="{{ $row['work_week_id'] }}" class="btn-edit-workweek">Edit Work Week</a></li>
                                    </ul>
                                  </div>

                      </td>
                        </tr>

                    @endif

                    @endforeach
                </tbody>

            </table>



            </div>
            <div role="tabpanel" class="tab-pane" id="leaves_history">

                <h1>Leave History  - {{ $employee->first_name }} {{ $employee->last_name }} </h1>

             <table class="table table-striped">
            <thead>
                <tr>
                    <th>{{trans("leaves.Date")}}</th>
                    <th>{{trans("leaves.Type")}}</th>
                    <th>Credits</th>
                    <th>{{trans("leaves.Status")}}</th>
                </tr>
            </thead>
            <tbody>
                <?php $passed = 0; ?>

                @foreach($user_leaves as $row)
                <tr>
                    <td>

                    @if($row->leave_application_type=='single')

                        <?php
                            if(time() > strtotime($row->date)){
                                $passed = 1;
                            }
                        ?>
                        {{ date('M d, Y',strtotime($row->date)) }} ({{ $row->date_type }})
                    @else
                        {{ date('M d, Y',strtotime($row->date_from)) }} ({{ $row->date_from_type }}) -
                        {{ date('M d, Y',strtotime($row->date_to)) }} ({{ $row->date_to_type }})

                        <?php
                            if(time() > strtotime($row->date_to)){

                                $passed = 1;
                            }
                        ?>



                    @endif
                    </td>
                    <td>{{$row->type}}</td>
                    <td>{{$row->credits}}</td>
                    <td>{{ $row->status }}</td>
                </tr>

                <?php $passed = 0; ?>

                @endforeach
            </tbody>



            </div>
          </div>




        </table>





        @endif

    </div>


</div>

<div class="modal fade" id="leave_summary_edit_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans("leaves.Leave Summary")}}</h4>
            </div>
            <div class="modal-body" id="leave_summary_edit_body">

            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="user_leave_notes_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans("leaves.Leave Notes")}}</h4>
            </div>
            <div class="modal-body" id="user_leave_notes_body">

            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="bulk_edit_modal_warning" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" >{{trans("leaves.Warning")}}</h4>
            </div>
            <div class="modal-body" >
                <p>{{trans("leaves.Please check atleast one checkbox")}}</p>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="leave_summary_edit_bulk_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans("leaves.Leave Summary")}}</h4>
            </div>
            <div class="modal-body" id="leave_summary_edit_bulk_body">

            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="add_leave_notes_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans("leaves.Leave Notes")}}</h4>
            </div>
            <div class="modal-body" id="add_leave_notes_body">

            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="user_work_week_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Assign Work Week to <span id="user_name_work_week" style="font-weight:bold;"></span></h4>
            </div>
            <div class="modal-body">

                <form class="form-inline" action="{{ url('/update_user_work_week') }}" method="POST">
                    <input type="text" name="user_id" style="display:none;" id="select_work_week_user_id" value="" />
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="form-group">
                        <div class="input-group">

                            <select name="work_week_id" id="select_work_week_id" class="form-control" style="width:200px;">

                                <option value="">--- Select Work Week ---</option>
                                @foreach($compay_work_week as $week)
                                    <option value="{{$week->id}}">{{$week->title}}</option>
                                @endforeach

                            </select>
                            <span class="input-group-btn">
                            <button type="submit" class="btn btn-default">{{trans("leaves.Save")}}</button>
                            </span>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>



<script type="text/javascript">

    $(document).ready(function(){

        $('.btn-edit').click(function(){

            var user_id = $(this).data('user-id');
            $.ajax({
                url : '{{url('/leave_summary_edit/')}}/'+user_id,
                type : 'GET',
                data : 'id='+user_id,
                success:function(html){
                    $('#leave_summary_edit_body').html(html);
                    $('#leave_summary_edit_modal').modal('show');
                }

            });


        });


        $('.view_leave_record').click(function(){

            var id = $(this).data('id');

             $.ajax({
                url : '{{url('/view_leave_record/')}}/'+id,
                type : 'GET',
                data : 'id='+id,
                success:function(html){
                    $('#view_leave_body').html(html);
                    $('#view_leave').modal('show');
                }

            });


        });

        $('.approver').click(function(){

            var id = $(this).data('id');

            $('#proceed_approve').attr('alt',id);
            $('#confirm_approve').modal('show');



        });

        $('#proceed_approve').click(function(){
            var id = $(this).attr('alt');

            var dt = {
                'id':id,
                'status':'approved',
                '_token': '{{ csrf_token() }}'
            };

            $.ajax({
                url : '{{url('/update_leave_status/')}}',
                type : 'POST',
                data : dt,
                success:function(html){
                    window.location.reload();
                }

            });


        });


        $('.reject').click(function(){

            var id = $(this).data('id');

            $('#proceed_reject').attr('alt',id);
            $('#confirm_reject').modal('show');
        });

        $('#proceed_reject').click(function(){
            var id = $(this).attr('alt');
            var dt = {
                'id':id,
                'status':'rejected',
               '_token': '{{ csrf_token() }}'
            };
            $.ajax({
                url : '{{url('/update_leave_status/')}}',
                type : 'POST',
                data : dt,
                success:function(html){
                    window.location.reload();
                }
            });
        });


        $('.reject').click(function(){

            var id = $(this).data('id');

        });

        // START AND FINISH DATE
        $('.startdate').datepicker({
            dateFormat: 'dd-mm-yy',
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>'
        });



        $("#check-all").change(function(){

            $('.checkboxes').not(this).prop('checked', this.checked);

        });

        $('#bulk_edit').click(function(){

            if ($(".checkboxes:checked").length == 0)
            {
                $('#bulk_edit_modal_warning').modal('show');
            }
            else
            {


                var user_array = new Array();
                $(".checkboxes:checked").each(function() {
                   user_array.push($(this).val());
                });


                var user_id = $(this).data('user-id');
                var dt= {
                    user_ids :user_array,
                   '_token': '{{ csrf_token() }}'
                };

                $.ajax({
                    url : '{{url('/leave_summary_edit_bulk')}}',
                    type : 'POST',
                    data : dt,
                    success:function(html){

                        $('#leave_summary_edit_bulk_body').html(html);
                        $('#leave_summary_edit_bulk_modal').modal('show');
                    }

                });

            }


        });



        $('.btn-view-note').click(function(){

            var user_id = $(this).data('user-id');

            $.ajax({
                url : '{{url('/user_leave_notes/')}}/'+user_id,
                type : 'GET',
                data : 'id='+user_id,
                success:function(html){
                    $('#user_leave_notes_body').html(html);
                    $('#user_leave_notes_modal').modal('show');
                }
            });

        });

        $('.btn-add-note').click(function(){

            var user_id = $(this).data('user-id');
            $.ajax({
                url : '{{url('/add_leave_note/')}}/'+user_id,
                type : 'GET',
                data : 'id='+user_id,
                success:function(html){
                    $('#add_leave_notes_body').html(html);
                    $('#add_leave_notes_modal').modal('show');
                }

            });


        });

        $('.btn-edit-workweek').click(function(){

            var user_id = $(this).data('user-id');
            var work_week_id = $(this).data('work-week-id');
            var user_name = $(this).data('name');

            $('#user_name_work_week').html(user_name);

            $('#select_work_week_user_id').val(user_id);
            $('#select_work_week_id').val(work_week_id);
            $('#user_work_week_modal').modal('show');

        });

        $('.tr_navigate').click(function(){

            var url = $(this).data('url');

            window.location.href= url;

        });

    });

</script>

@endsection