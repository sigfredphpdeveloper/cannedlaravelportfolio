@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-sm-12 col-md-12">
        <h1>{{trans("leaves.Add Leave Type")}}</h1>
        </div>

        <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>{{trans("leaves.Warning")}}.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <article class="col-sm-6 col-md-6 col-lg-6">

            <form  id="smart-form-register" class="smart-form client-form" role="form" method="POST" action="{{ url('/leave-types-add') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="user_id" value="{{ $user['id'] }}">
            <input type="hidden" name="company_id" value="{{ $company['id'] }}">

                <fieldset>

                    <label class="label">{{trans("leaves.Leave Type")}} *</label>
                    <label class="input">
                        <input type="text" name="type" class="form-control" value="" required/>
                    </label>

                    <label class="label">{{trans("leaves.Description")}}</label>
                    <label class="input">
                        <textarea name="description" class="form-control"></textarea>
                    </label>

                    <label class="label">{{trans("leaves.Default Allowance")}}</label>
                    <label class="input">
                        <input type="text" name="default_allowance" value="15" class="form-control" required/>
                    </label>

                    <label class="label">{{trans("leaves.Color Code")}}</label>
                    <label class="input">


                    <div id="cp3" class="input-group colorpicker-component">
                        <input type="text" name="color_code" value="#00AABB" class="form-control" />
                        <span class="input-group-addon"><i></i></span>
                    </div>


                    </label>

                    <section >

                        <label class="label">
                        {{trans("leaves.Select the Roles who")}}
                        </label>
                        <label class="input">

                            <select name="roles[]" multiple class="form-control">
                                @foreach($roles as $role)
                                    <option value="{{$role['id']}}">{{$role['role_name']}}</option>
                                @endforeach
                            </select>

                        </label>
                    </section>

                    <label class="label">{{trans("leaves.Start Date Allowance")}}</label>
                    <label class="input">
                        <div class="input-group">
                            <input type="text"
                                     value=""
                                    name="start_date_allowance" placeholder="{{trans("leaves.Select a date")}}"
                                   class="form-control datepicker "
                                   id="datepicker" required data-dateformat="mm/dd/yy" id="dp1462167530325">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>

                    </label>


                    <label class="label">{{trans("leaves.Roll over allowed at the end of the year?")}}</label>

                    <div class="radio">
                      <label><input type="radio" name="roll_over" value="1"  checked style="left:auto"/> Yes</label>
                    </div>
                    <div class="radio">
                      <label><input type="radio" name="roll_over" value="0"  style="left:auto"/> No</label>
                    </div>


                    <label class="label">{{trans("leaves.Allow Advanced Leaves")}} ?</label>

                    <div class="radio">
                      <label><input type="radio" name="advance_leave" value="1"  checked style="left:auto"/> Yes</label>
                    </div>
                    <div class="radio">
                      <label><input type="radio" name="advance_leave" value="0"  style="left:auto"/> No</label>
                    </div>


                </fieldset>

                <footer>
                    <button type="submit" class="btn btn-success">
                         {{trans("leaves.Save")}}
                    </button>
                     <a href="{{ url('/leaves-setting') }}" class="btn btn-default">
                          {{trans("leaves.Back")}}
                    </a>
                </footer>

            </form>

        </article>
    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->


@endsection


@section('page-script')

<script type='text/javascript' src="{{ url('/') }}/js/plugin/colorpicker/js/bootstrap-colorpicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{ url('/') }}/js/plugin/colorpicker/css/bootstrap-colorpicker.min.css" />

<script type="text/javascript">

    $(document).ready(function(){

        $('.leave_selector').change(function(){

            var selected = $(".leave_selector:checked").val();

            if(selected == 'single'){
                $('#single_day').show();
                $('#multiple_day').hide();
                $('#datepicker').attr('required','required');
                $('#to').attr('required',false);
                $('#from').attr('required',false);
            }else if(selected == 'multiple'){
                $('#single_day').hide();
                $('#multiple_day').show();
                $('#datepicker').attr('required',false);
                $('#to').attr('required','required');
                $('#from').attr('required','required');
            }
        });

        $('#cp3').colorpicker({
            format: 'hex'
        });



    });

</script>
@endsection