<!-- widget grid -->
<section id="widget-grid" class="content">

    <!-- row -->
    <div class="row">

        <div class="col-md-12">

        <h1>Add Work Week</h1>

                <form action="{{url('/save_add_company_work_week')}}" method="POST" >

                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <fieldset style="margin-bottom:8px;">

                             <div class="form-group" >
                                <label  class="col-sm-2 control-label">Title</label>
                                <div class="col-sm-10">
                                  <input type="text" name="title" class="form-control" value="" required placeholder="Enter Title"/>
                                </div>

                              </div>

                          </fieldset>


                        <fieldset style="margin-bottom:8px;">

                              <div class="form-group" >
                                <label  class="col-sm-2 control-label">Monday</label>
                                <div class="col-sm-10">
                                    Yes
                                    <input type="radio" name="monday" value="1" checked/>
                                    &nbsp; No
                                    <input type="radio" name="monday" value="0" />
                                </div>
                              </div>
                          </fieldset>

                        <fieldset style="margin-bottom:8px;">
                              <div class="form-group" >
                                <label  class="col-sm-2 control-label">Tuesday</label>
                                <div class="col-sm-10">
                                    Yes
                                    <input type="radio" name="tuesday" value="1" checked/>
                                     &nbsp;No
                                    <input type="radio" name="tuesday" value="0"/>
                                </div>
                              </div>
                      </fieldset>

                        <fieldset style="margin-bottom:8px;">

                              <div class="form-group" >
                                <label  class="col-sm-2 control-label">Wednesday</label>
                                <div class="col-sm-10">
                                    Yes
                                    <input type="radio" name="wednesday" value="1" checked/>
                                     &nbsp;No
                                    <input type="radio" name="wednesday" value="0" />
                                </div>
                              </div>

                          </fieldset>

                        <fieldset style="margin-bottom:8px;">
                          <div class="form-group" >
                            <label  class="col-sm-2 control-label">Thursday</label>
                            <div class="col-sm-10">
                                Yes
                                <input type="radio" name="thursday" value="1" checked/>
                                 &nbsp;No
                                <input type="radio" name="thursday" value="0" />
                            </div>
                          </div>
                          </fieldset>

                        <fieldset style="margin-bottom:8px;">
                          <div class="form-group" >
                            <label  class="col-sm-2 control-label">Friday</label>
                            <div class="col-sm-10">
                                Yes
                                <input type="radio" name="friday" value="1" checked/>
                                &nbsp; No
                                <input type="radio" name="friday" value="0" />
                            </div>
                          </div>
                          </fieldset>

                        <fieldset style="margin-bottom:8px;">
                          <div class="form-group" >
                            <label  class="col-sm-2 control-label">Saturday</label>
                            <div class="col-sm-10">
                                Yes
                                <input type="radio" name="saturday" value="1" />
                                 &nbsp; No
                                <input type="radio" name="saturday" value="0" checked/>
                            </div>
                          </div>
                          </fieldset>


                        <fieldset style="margin-bottom:8px;">
                          <div class="form-group" s>
                            <label  class="col-sm-2 control-label">Sunday</label>
                            <div class="col-sm-10">
                                Yes
                                <input type="radio" name="sunday" value="1" />
                                &nbsp; No
                                <input type="radio" name="sunday" value="0" checked/>
                            </div>
                          </div>

                       </fieldset>

                        <input type="submit" value="{{trans("leaves.Save")}}" class="btn btn-primary" data-toggle="tooltip" title="Save" />

                </form>

        </div>

</div>
<!-- end row -->

</section>

