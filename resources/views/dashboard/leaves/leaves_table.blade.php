@extends('dashboard.layouts.master')

@section('content')


<script src="{{ url('js/plugin/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('js/plugin/datatable-responsive/datatables.responsive.min.js') }}"></script>

<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/data_tables.css') }}">
<!-- widget grid -->
<style>
table thead tr th {
    background-color: #F7DCB4 !important;
}

tfoot {
    display: table-header-group !important;
}
thead {
    display: table-row-group !important;
}

</style>
<section id="widget-grid" class="">

    <div class="row">

        @include('errors.success')
        @include('errors.errors')

        <div class="col-md-6">

            <a href="{{ url('/leaves-my-account') }}" class="btn btn-white btn-xs"><i class="fa fa-caret-left"></i> My Account</a>
            @if($is_admin )
            <a href="{{ url('/manage-leaves') }}" class="btn btn-white btn-xs"><i class="fa fa-caret-left"></i> Manage Leaves</a>
            @endif

            <h1>{{$title}}</h1>
        </div>



        <div class="col-md-6 pull-right">

            @if($is_admin OR $manage_leaves)
            <a href="{{ url('/leaves-setting') }}" class="btn btn-primary pull-right" >
                <i class="fa fa-cog"></i> {{trans("leaves.Customize")}}</a>
            @endif
        </div>

    </div>


    <!-- row -->
    <div class="row">

        <div class="col-md-12">



            <table class="table table-striped datatable-dom-position">

                <thead>
                    <tr>
                        <th>{{trans("leaves.First Name")}}</th>
                        <th>{{trans("leaves.Last Name")}}</th>
                        <th>{{trans("leaves.Email")}}</th>
                        <th>{{trans("leaves.Date")}}</th>
                        <th>{{trans("leaves.Type")}}</th>
                        <th>{{trans("leaves.Credits")}}</th>
                        <th>{{trans("leaves.Status")}}</th>
                        <th style="width:100px;" ></th>
                    </tr>
                </thead>

                <tbody>

                @foreach($leaves as $leave)
                <tr>
                    <td>{{$leave->user->first_name}}</td>
                    <td>{{$leave->user->last_name}}</td>
                    <td>{{$leave->user->email}}</td>
                    <td>
                        @if($leave->leave_application_type =='single')
                            {{$leave->date}} ({{$leave->date_type}})
                        @else
                            From {{$leave->date_from}} ({{$leave->date_from_type}})
                            To {{$leave->date_to}} ({{$leave->date_to_type}})
                        @endif
                    </td>
                    <td>{{$leave->Leave_types->type}}</td>
                    <td>{{$leave->credits}}
                    <td>{{$leave->status}}</td>
                    <td>
                        <a href="javascript:void(0);" class="btn btn-primary btn-xs view_leave_record" data-id="{{$leave->id}}">{{trans("leaves.View")}}</a>
                    </td>
                </tr>
                @endforeach

                </tbody>
                <tfoot>
                    <tr>
                        <th><input type="text" placeholder="{{trans("leaves.Search")}} {{trans("leaves.First Name")}}" /></th>
                        <th><input type="text" placeholder="{{trans("leaves.Search")}} {{trans("leaves.Last Name")}}" /></th>
                        <th><input type="text" placeholder="{{trans("leaves.Search")}} {{trans("leaves.Email")}}" /></th>
                        <th><input type="text" placeholder="{{trans("leaves.Search")}} {{trans("leaves.Date")}}" /></th>
                        <th><input type="text" placeholder="{{trans("leaves.Search")}} {{trans("leaves.Type")}}" /></th>
                        <th></th>
                        <th><input type="text" placeholder="{{trans("leaves.Search")}} {{trans("leaves.Status")}}" /></th>
                        <th style="width:100px;" >



                        </th>
                    </tr>
                </tfoot>

            </table>


        </div>

    </div>
    <!-- end row -->

</section>
<!-- end widget grid -->



<!-- Modal -->
<div class="modal fade" id="view_leave" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans("leaves.Leave Details")}}</h4>
            </div>
            <div class="modal-body" id="view_leave_body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("leaves.Close")}}</button>
            </div>
        </div>
    </div>
</div>

<!-- end widget grid -->

<script type="text/javascript">

    $(document).ready(function(){

        $('.deleter').click(function(){

            var id = $(this).data('id');
            $('#delete_proceed').attr('alt',id);
            $('#confirm').modal('show');

        });

        $('#delete_proceed').click(function(){

            var id = $(this).attr('alt');

            $('#confirm').modal('hide');

            window.location.href='{{ url('company_delete') }}/'+id;

        });

        $( ".custom_auto_complete" ).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "{{ url('/companies_list') }}",
                    dataType: "json",

                    data: {
                        q: request.term
                    },
                    success: function( data ) {
                        console.log(data);
                        response(data);
                    }
                });
            },
            select: function(e,ui){
                console.log(ui);
                $('#company_id').val(ui.item.id);
            }
        });

        $( ".custom_auto_complete" ).autocomplete( "option", "appendTo", ".eventInsForm" );



     var table =  $('.datatable-dom-position').DataTable({
           dom: '<"datatable-header length-left"><"datatable-scroll"t><"datatable-footer info-right"lp>'
       });

        // Apply the search
            table.columns().every( function () {
                var that = this;

                $( 'input', this.footer() ).on( 'keyup change', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );


         $(document).on("click", ".view_leave_record", function(){
            var id = $(this).data('id');

              $.ajax({
                 url : '{{url('/view_leave_record/')}}/'+id,
                 type : 'GET',
                 data : 'id='+id,
                 success:function(html){
                     $('#view_leave_body').html(html);
                     $('#view_leave').modal('show');
                 }

             });
        });
    });

</script>


@endsection
