<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-sm-12 col-md-12">
        <h1>{{$user_find['first_name']}} {{$user_find['last_name']}}</h1>
        </div>


        <article class="col-sm-12 col-md-12 col-lg-12">

            <form  id="smart-form-register" class="smart-form client-form" role="form" method="POST" action="{{ url('/add_leave_note_save') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="user_id" value="{{ $user_find['id'] }}">

                <fieldset>

                    <section>
                        <label class="label">{{trans("leaves.Leave Note")}}</label>
                        <label class="input">
                            <textarea name="leave_note" style="height:80px;" class="form-control" placeholder="Add a leave note here"></textarea>
                        </label>
                    </section>

                </fieldset>

                <footer>
                    <button type="submit" class="btn btn-success">
                         {{trans("leaves.Save")}}
                    </button>
                </footer>

            </form>

        </article>
    </div>

    <!-- end row -->

</section>
