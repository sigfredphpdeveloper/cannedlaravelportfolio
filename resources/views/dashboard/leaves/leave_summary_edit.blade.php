<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-sm-12 col-md-12">
        <h1>{{$employee['first_name']}} {{$employee['last_name']}}</h1>
        </div>

        <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>{{trans("leaves.Warning")}}.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <article class="col-sm-12 col-md-12 col-lg-12">

            <form  id="smart-form-register" class="smart-form client-form" role="form" method="POST" action="{{ url('/leave_summary_edit_save') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="employee_id" value="{{ $employee['id'] }}">
            <input type="hidden" name="company_id" value="{{ $company['id'] }}">

                <fieldset>

                    <section>
                        <label class="label">{{trans("leaves.Employee Start Date")}}</label>
                        <label class="input">

                            <div class="form-group">
                                <div class="input-group">
                                    <input class="form-control startdate clsDatePicker" id="start_date_employee"
                                    name="start_date[{{$employee['id']}}]" type="text" placeholder="{{trans("leaves.Start Date")}}"
                                    value="{{date('d-m-Y',strtotime($employee['start_date']))}}"
                                    >
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>

                        </label>
                    </section>

                    @foreach($employee['leave_allowance'] as $EL)
                    <section>
                        <label class="label">{{$EL['type']}} {{trans("leaves.Allowance")}}</label>
                        <label class="input">
                            <input type="text" name="leave_allowance[{{$EL['allowance_id']}}]" value="{{$EL['allowance']}}" class="form-control" />
                        </label>
                    </section>
                    @endforeach

                    <section>
                        <label class="label">{{trans("leaves.Leave Note")}}</label>
                        <label class="input">
                            <textarea name="leave_note" style="height:80px;" class="form-control" placeholder="Add a leave note here"></textarea>
                        </label>
                    </section>

                </fieldset>

                <footer>
                    <button type="submit" class="btn btn-success">
                         {{trans("leaves.Save")}}
                    </button>
                </footer>

            </form>

        </article>
    </div>

    <!-- end row -->

</section>
<style type="text/css">
.clsDatePicker {
    z-index: 99999  !important;
}
</style>

<script type="text/javascript">
    $(document).ready(function(){

        // START AND FINISH DATE
        $('#start_date_employee')
                .removeClass('hasDatepicker')
                .removeData('datepicker')
                .unbind()
                .datepicker({
                    dateFormat: 'dd-mm-yy',
                    prevText: '<i class="fa fa-chevron-left"></i>',
                    nextText: '<i class="fa fa-chevron-right"></i>'
                });

    });
</script>
<!-- end widget grid -->

