@extends('dashboard.layouts.master')

@section('content')

<!-- row -->
<div class="row">

    @include('errors.success')
    @include('errors.errors')

    <div class="col-md-6">

        <a href="{{ url('/leaves-my-account') }}" class="btn btn-white btn-xs"><i class="fa fa-caret-left"></i> My Account</a>
        @if($is_admin )
        <a href="{{ url('/manage-leaves') }}" class="btn btn-white btn-xs"><i class="fa fa-caret-left"></i> Manage Leaves</a>
        @endif

        <h1>{{trans("leaves.Approve Leaves")}}</h1>
    </div>



    <div class="col-md-6 pull-right">
        @if($is_admin OR $manage_leaves)
        <a href="{{ url('/leaves-setting') }}" class="btn btn-white pull-right" >
            <i class="fa fa-cog"></i> {{trans("leaves.Customize")}}</a>
        @endif
    </div>

</div>




<div class="row">

        <div class="col-md-3">

        </div>

        <div class="col-md-9 pull-right">
            <div class="input-group pull-right">

                <form class="form-inline" method="GET">
                    <div class="form-group">
                        <div class="input-group">
                           <input type="text" name="search" id="search_box" class="form-control"
                              value="<?php echo (isset($search))?$search:'';?>" placeholder="{{trans("leaves.Enter Keywords")}}">
                            <span class="input-group-btn">
                            <button type="submit" class="btn btn-default">{{trans("leaves.Search")}}</button>
                            </span>
                        </div>
                    </div>
                </form>
            </div>

        </div>

    </div>
    <br />

<div class="row">
    <div class="col-md-12">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>{{trans("leaves.Date")}}</th>
                    <th>{{trans("leaves.User")}}</th>
                    <th>{{trans("leaves.Type")}}</th>
                    <th>{{trans("leaves.Status")}}</th>
                    <th>{{trans("leaves.Available")}}</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($user_leaves as $row)
                <tr>

                <?php $user = \App\User::find($row->user_id); ?>

                    <td>

                    @if($row->leave_application_type=='single')
                        {{ date('M d, Y',strtotime($row->date)) }} ({{ $row->date_type }})
                    @else
                        {{ date('M d, Y',strtotime($row->date_from)) }} ({{ $row->date_from_type }}) -
                        {{ date('M d, Y',strtotime($row->date_to)) }} ({{ $row->date_to_type }})
                    @endif

                    </td>
                    <td>{{$row->first_name}} {{ $row->last_name }}</td>
                    <td>{{$row->type}}</td>
                    <td>{{ $row->status }}</td>
                    <td>{{ $user->all_remaining_leaves($row->leave_types_id) }}</td>
                    <td>

                    <a href="javascript:void(0);" class="btn btn-primary approver" data-id="{{$row->id}}">{{trans("leaves.Approve")}}</a>
                    <a href="javascript:void(0);" class="btn btn-primary reject" data-id="{{$row->id}}">{{trans("leaves.Reject")}}</a>
                    <a href="javascript:void(0);" class="btn btn-primary view_leave_record" data-id="{{$row->id}}">{{trans("leaves.View")}}</a>

                    </td>
                </tr>
                @endforeach
            </tbody>
            <tfooot>
                <tr>
                    <td colspan="4"> {!! $user_leaves->render() !!}</td>
                </tr>
            </tfooot>

        </table>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="view_leave" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans("leaves.Leave Details")}}</h4>
            </div>
            <div class="modal-body" id="view_leave_body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("leaves.Close")}}</button>
            </div>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="confirm_approve" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans("leaves.Confirmation")}}</h4>
            </div>
            <div class="modal-body" id="view_leave_body">
                <p>{{trans("leaves.Are you sure you want to approve this leave")}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("leaves.No")}}</button>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="proceed_approve">{{trans("leaves.Yes")}}</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="confirm_reject" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans("leaves.Confirmation")}}</h4>
            </div>
            <div class="modal-body" id="view_leave_body">
                <p>{{trans("leaves.Are you sure you want to reject this leave")}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="proceed_reject">Yes</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('page-script')

<script type="text/javascript">

    $(document).ready(function(){

        $('.view_leave_record').click(function(){

            var id = $(this).data('id');

             $.ajax({
                url : '{{url('/view_leave_record/')}}/'+id,
                type : 'GET',
                data : 'id='+id,
                success:function(html){
                    $('#view_leave_body').html(html);
                    $('#view_leave').modal('show');
                }

            });


        });

        $('.approver').click(function(){

            var id = $(this).data('id');

            $('#proceed_approve').attr('alt',id);
            $('#confirm_approve').modal('show');



        });

        $('#proceed_approve').click(function(){

            $(this).attr('disabled','disabled');
            $(this).html('{{trans("leaves.Loading Please Wait")}}...');

            var id = $(this).attr('alt');

            var dt = {
                'id':id,
                'status':'approved',
                '_token': '{{ csrf_token() }}'
            };

            $.ajax({
                url : '{{url('/update_leave_status')}}',
                type : 'POST',
                data : dt,
                success:function(html){
                    window.location.reload();
                }

            });


        });


        $('.reject').click(function(){

            var id = $(this).data('id');

            $('#proceed_reject').attr('alt',id);
            $('#confirm_reject').modal('show');
        });

        $('#proceed_reject').click(function(){
            var id = $(this).attr('alt');
            var dt = {
                'id':id,
                'status':'rejected',
               '_token': '{{ csrf_token() }}'
            };
            $.ajax({
                url : '{{url('/update_leave_status')}}',
                type : 'POST',
                data : dt,
                success:function(html){
                    window.location.reload();
                }
            });
        });





        $('.reject').click(function(){

            var id = $(this).data('id');

        });

    });

</script>
@endsection