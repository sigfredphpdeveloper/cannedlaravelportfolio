@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->

    <div class="row">


        <div class="alert alert-primary">
            <p>{{trans("directory_invite_user.Please type in a user address you would like to invite")}}</p>
            <p>{{trans("directory_invite_user.Email must be in your same organization")}} ({{ $company['url'] }})</p>
        </div>

            <div class="col-md-12">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ Session::get('success') }}</p>
                </div>
            @endif
            </div>

            <div class="col-md-12">
            @if(Session::has('error'))
                <div class="alert alert-danger">
                    <p>{{ Session::get('error') }}</p>
                </div>
            @endif
            </div>

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>{{trans("directory_invite_user.Warning.")}}<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <article class="col-sm-6 col-md-6 col-lg-6">

                <form  id="smart-form-register" class="smart-form client-form" role="form" method="POST" action="{{ url('/send_invitation') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <header>

                         {{trans("directory_invite_user.Add Employee")}}

                    </header>

                    <div id="form_contains">

                        <fieldset >

                        <section>
                            <label class="label">{{trans("directory_invite_user.Email *")}}</label>
                            <label class="input">
                                <input type="email" name="email[]" required placeholder="{{trans("directory_invite_user.Enter Email")}}" />
                            </label>
                        </section>

                        <section>
                            <label class="label">{{trans("directory_invite_user.Employee First Name")}}</label>
                            <label class="input">
                                <input type="text" name="first_name[]" value="" />
                            </label>
                        </section>

                        <section>
                            <label class="label">{{trans("directory_invite_user.Employee Last Name")}}</label>
                            <label class="input">
                                <input type="text" name="last_name[]"  />
                            </label>
                        </section>

                        <section>
                            <label class="label">{{trans("directory_invite_user.Employee Roles")}}</label>
                            <label class="input">

                                <select name="roles[][]" multiple class="form-control">

                                    @foreach($roles as $role)
                                        <option value="{{$role['id']}}">{{$role['role_name']}}</option>
                                    @endforeach

                                </select>

                            </label>
                        </section>

                        <section>
                            <label class="label">{{trans("directory_invite_user.Employee Teams")}}</label>
                            <label class="input">
                                <select name="teams[][]" multiple class="form-control">

                                    @foreach($teams as $team)
                                        <option value="{{$team['id']}}">{{$team['team_name']}}</option>
                                    @endforeach

                                </select>
                            </label>
                        </section>






                    </fieldset>


                    <hr />

                    </div>


                    <footer>

                        <a href="javascript:void(0);" class=" pull-left" id="add_more">
                            <i class="fa fa-plus-square"></i> Add More
                        </a>

                        <button type="submit" class="btn btn-success">
                            {{trans("directory_invite_user.Send Invitation")}}
                        </button>
                        <a href="{{url('/directory')}}" class="btn btn-default">{{trans("directory_invite_user.Back")}}</a>
                    </footer>


                </form>

            </article>

    </div>


<div id="clone_from" style="display:none;">
    <hr />

    <fieldset >
        <a href="javascript:void(0);" class=" pull-right remover" style="color:red;cursor:pointer;">
            <i class="fa fa-remove"></i> Remove
        </a>
        <br style="clear:both;" />

        <section>
            <label class="label">{{trans("directory_invite_user.Email *")}}</label>
            <label class="input">
                <input type="email" name="email[]" required placeholder="{{trans("directory_invite_user.Enter Email")}}" />
            </label>
        </section>

        <section>
            <label class="label">{{trans("directory_invite_user.Employee First Name")}}</label>
            <label class="input">
                <input type="text" name="first_name[]" value="" />
            </label>
        </section>

        <section>
            <label class="label">{{trans("directory_invite_user.Employee Last Name")}}</label>
            <label class="input">
                <input type="text" name="last_name[]"  />
            </label>
        </section>

        <section>
            <label class="label">{{trans("directory_invite_user.Employee Roles")}}</label>
            <label class="input">

                <select name="roles[][]" multiple class="form-control">

                    @foreach($roles as $role)
                        <option value="{{$role['id']}}">{{$role['role_name']}}</option>
                    @endforeach

                </select>

            </label>
        </section>

        <section>
            <label class="label">{{trans("directory_invite_user.Employee Teams")}}</label>
            <label class="input">
                <select name="teams[][]" multiple class="form-control">

                    @foreach($teams as $team)
                        <option value="{{$team['id']}}">{{$team['team_name']}}</option>
                    @endforeach

                </select>
            </label>
        </section>



    </fieldset>

</div>

    <!-- end row -->

</section>
<!-- end widget grid -->

<script type="text/javascript">

    $(document).ready(function(){

        //form_contains
        //clone_from

        $('#add_more').click(function(){

            var $clone = $("#clone_from").clone();

            $('#form_contains').append($clone);

            /*
            $clone.find("input.startdate")
                .removeClass('hasDatepicker')
                .removeData('datepicker')
                .unbind()
                .datepicker({
                    dateFormat: 'dd-mm-yy',
                    prevText: '<i class="fa fa-chevron-left"></i>',
                    nextText: '<i class="fa fa-chevron-right"></i>'
                });*/


            $('#form_contains').children().show();

        });

        $(document).on('click','.remover',function(e){
            $(this).parent().parent().remove();
        });

         // START AND FINISH DATE
        $('.startdate').datepicker({
            dateFormat: 'dd-mm-yy',
            prevText: '<i class="fa fa-chevron-left"></i>',
            nextText: '<i class="fa fa-chevron-right"></i>'
        });


    });

</script>

@endsection