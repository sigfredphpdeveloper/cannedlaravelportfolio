@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">
    <div class="row">

    <!-- row -->


    <?php
        $first_step = false;
        if($user['first_name'] == '' OR $user['last_name'] == ''){
        $first_step = TRUE;
        }

    ?>



    <?php if($user['completed_profile'] == 2){ ?>

        <div class="col-sm-6">
            <?php if($company['announcements'] == 1){ ?>

                <?php if(!$is_guest){ ?>

                    <!-- Announcements -->

                    <div class="row" style="margin-left:0;">
                        <div class="col-md-12 announcements-row">

                            <div class="col-md-3">
                              <h1>{{trans("dashboard_layouts_nav.Announcements")}}</h1>
                            </div>

                            <div class="col-md-9 pull-right">

                              @if($manage_announcement)
                              <a href="#" class="dropdown-toggle no-margin" data-toggle="dropdown" style="float:right;color:#333;">
                                  <i class="icon manage-icon" style="width:25px;height:25px;margin-top:9px;"></i>
                              </a>
                              <ul class="dropdown-menu pull-right profilemenu" style="padding:0">
                                <li><a href="{{ url('/add_announcement') }}" >Add Announcement</a></li>
                                <li><a href="{{ url('/announcements') }}"  >Manage Announcements</a></li>
                              </ul>
                              <!--
                                <a href="{{ url('/add_announcement') }}" class="btn pull-right" style="color:#d1d1d1">Add Announcement</a>
                                <a href="{{ url('/announcements') }}" class="btn pull-right" style="margin-right:5px;color:#d1d1d1">Manage Announcements</a>
                              -->
                              @endif
                            </div>

                            <br style="clear:both;">


                            @foreach($announcement_array as $announcement)
                            <?php $loop++; ?>
                            <div class="row col-md-12" style="<?=($loop>=5)?'display:none':$loop?>">
                                <div class="col-md-2 ">

                                    <?php if($announcement['title'] == 'Welcome to Zenintra'){?>
                                    <a href="{{ url('/announcement') }}/{{$announcement['id']}}" style="margin:4px;float:right;">
                                            <div class="circle-div" style="
                                            background:url('{{ url('/img') }}/zen.jpg') no-repeat;
                                            background-position: center;
                                            background-size:47px 24px;
                                            width:50px;height:50px;
                                            ">
                                            </div>
                                        </a>
                                    <?php }elseif($announcement['photo'] != ''){?>
                                        <a href="{{ url('/announcement') }}/{{$announcement['id']}}" style="margin:4px;float:right;">
                                            <div class="circle-div" style="
                                            background:url('{{$announcement['photo']}}') no-repeat;
                                            background-position: center;
                                            background-size:50px 50px;
                                            width:50px;height:50px;
                                            ">
                                            </div>
                                        </a>

                                    <?php } ?>



                                </div>
                                <div class="col-md-10">
                                    <h2 style="margin:0;font-weight:bold;">{{$announcement['title']}}</h2>
                                    <i style="color:#827D7D;">
                                    {{$announcement['first_name']}} {{$announcement['last_name']}} -
                                    {{ date('M d',strtotime($announcement['created_at']))}}</i>
                                    <p>
                                        <?php if(strlen($announcement['message'])>100):?>
                                        <p>{!!substr(nl2br($announcement['message']),0,100)!!}...</p>
                                        <p style="display:none;">{!!(nl2br(App\Classes\Helper::replace_links($announcement['message']) ))!!}...</p>
                                        <a style="clear:both;margin-top:12px;" href="javascript:void(0);" class="view_more">{{trans("manage_announcements.View More...")}} </a>
                                        <?php else:?>
                                        {!!(nl2br(App\Classes\Helper::replace_links($announcement['message']) ))!!}
                                        <?php endif;?>
                                    </p>
                                </div>
                            </div>

                            @endforeach

                            <?php
                            if($announcements_count > 4 ){ ?>
                                <div class="col-md-12 text-right" style="padding: 3px 10px !important;">
                                    <a class="btn btn-orange btn-sm" href="{{url('all_announcements')}}">Read more...</a>
                                </div>
                            <?php } ?>

                        </div>

                    </div>

                <?php } ?>
            <?php } ?>
        </div>


    <div class="col-sm-6">

            <?php if($user['first_visit'] ==1){ ?>
                <?php if(!$is_guest){ ?>
                    <div class="row" style="margin-left:0;">
                        <div class="col-md-12 announcements-row" style="margin-bottom:10px;">
                            <div class="alert alert-info text-center" style="background:#000;border-color:#fff;width:100%;margin:0 auto;">
                                <a href="#" class="close" style="color:#fff;opacity: 1" data-dismiss="alert" aria-label="close" id="dismiss_first">&times;</a>

                                <h4 style="margin-bottom:0;font-weight:bold;color:#fff;">Welcome to Zenintranet: Watch this overview video first</h4>

                                <div style="margin:0 auto;width:100%;padding:20px;background:#000;padding-top:0;padding-bottom:0">
                                    <?php if($is_admin):?>
                                        <iframe width="560" style="margin:0 auto;width:100%;display:block;" height="315" src="https://www.youtube.com/embed/dWzx3Kn0tHE?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                                    <?php else: ?>
                                        <iframe width="560" style="margin:0 auto;width:100%;display:block;"  height="315" src="https://www.youtube.com/embed/YWMOdiiArjM?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
                                    <?php endif;?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>


            <!--Recognition Board-->

                @if(!$is_guest)

                    <div class="row" style="margin-left:0;
                    <?php if(!$view_recognition_board AND count($recognition_board_arr) ==0){ echo 'display:none;'; } ?>
                    " >
                        <div class="col-md-12 announcements-row">

                        <div class="col-md-9">
                          <h1>{{ trans("dashboard_layouts_nav.Recognition Board") }}</h1>
                        </div>

                        <div class="col-md-3 pull-right">

                          @if($view_recognition_board)
                          <a href="#" class="dropdown-toggle no-margin" data-toggle="dropdown" style="float:right;color:#333;">
                                <i class="icon manage-icon" style="width:25px;height:25px;margin-top:9px;"></i>
                          </a>
                          <ul class="dropdown-menu pull-right profilemenu" style="padding:0">
                              <li><a href="javascript:void(0);" class="add-recog-btn">{{trans("dashboard_layouts_nav.Add a new recognition")}}</a></li>
                              <li><a href="{{ url('/employee_recognition_board') }}">{{trans("dashboard_layouts_nav.Manage recognitions")}}</a></li>
                          </ul>
                          <!--
                            <a href="javascript:void(0);" class="add-recog-btn btn  pull-right" style="color:#d1d1d1">{{trans("dashboard_layouts_nav.Add a new recognition")}}</a>
                            <a href="{{ url('/employee_recognition_board') }}" class="btn  pull-right" style="margin-right:5px;color:#d1d1d1">{{trans("dashboard_layouts_nav.Manage recognitions")}}</a>
                          -->
                          @endif
                        </div>

                        @if(count($recognition_board_arr) > 0)

                                @foreach($recognition_board_arr as $recognition)
                                <div class="row col-md-12">
                                    <!-- Employee Photos -->
                                    <div class="col-md-2" >
                                        <?php if($recognition->employee_photo == ''):?>
                        <a href="{{ url('/employee_recognition') }}/{{$recognition->id}}" style="float:right;margin:4px;">
                                                <div class="circle-div" style="
                                                background:url('{{ url('/img') }}/zen.jpg') no-repeat;
                                                background-position: center;
                                                background-size:47px 24px;
                                                width:45px;height:45px;
                                                ">
                                                </div>
                                            </a>
                                        <?php elseif($recognition->employee_photo != ''):?>
                        <a href="{{ url('/employee_recognition') }}/{{$recognition->id}}" style="float:right;margin:4px;">
                                                <div class="circle-div" style="
                                                background:url('{{$recognition->employee_photo}}') no-repeat;
                                                background-position: center;
                                                background-size:50px 50px;
                                                width:45px;height:45px;
                                                ">
                                                </div>
                                            </a>

                                        <?php endif; ?>
                                    </div>


                                    <div class="col-md-10">
                                        <h2 style="margin:0;font-weight:bold;">{{ $recognition->employee_first_name}} {{ $recognition->employee_last_name }}</h2>
                                        <i style="color:#827D7D;">{{ $recognition->title }} - {{ date('M d',strtotime($recognition->created_at)) }}</i>

                                        <p>
                                            <?php if(strlen($recognition->message) > 40):?>
                                            <p>{!!substr(nl2br($recognition->message),0,40)!!}...</p>
                                            <p style="display:none;">{!!(nl2br($recognition->message))!!}...</p>
                                            <a style="clear:both;margin-top:12px;" href="javascript:void(0);" class="view_more">{{trans("manage_announcements.View More...")}} </a>
                                            <?php else:?>
                                            {!!nl2br($recognition->message)!!}
                                            <?php endif;?>
                                        </p>
                                    </div>
                                </div>

                                @endforeach

                                <?php
                                if($recognitions_count > 4 ):?>
                                <div class="col-md-12 text-left">
                                    <a href="{{url('all_recognitions')}}">Read more...</a>
                                </div>
                                <?php endif; ?>


                        @endif


                        </div>
                    </div>

                @endif
            <?php } ?>


        <!-- end row -->
    </div>


</div>
</section>


@if($view_recognition_board)

<style>
	body .ui-autocomplete .ui-menu-item .ui-state-focus {
   		/* selected <a> */
   		background: grey!important;
	}

	/* Elements from Datatable JQuery plug-ins */
	.paginate_button {
		margin: 5px;
	}

	.paginate_button:hover {
		cursor: pointer;
	}

	.disabled:hover {
		color: white !important;
		cursor: default;
	}
</style>


<!-- Modal Section -->
<div id="add-new-recognition" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!--Modal Content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">{{trans("employee_recognition_board.Add New Recognition")}}</h4>
			</div>

			<form id="add-recog-form">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">

				<div class="modal-body">
					<div class="form-group">
						<label>{{trans("employee_recognition_board.Employee Name")}}</label>
						<input id="add-recog-autocomplete" class="form-control" required>
						<input id="add-recog-employee-id" name="employee_id" type="hidden">
					</div>
					<div class="form-group">
						<label>{{trans("employee_recognition_board.Title")}}</label>
						<input class="form-control" name="title" type="text" required maxlength="150">
					</div>
					<div class="form-group">
						<label>{{trans("employee_recognition_board.Message")}}</label>
						<input class="form-control" name="message" type="text" required maxlength="300">
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">
						{{trans("employee_recognition_board.Cancel")}}
					</button>
					<input type="submit" class="btn btn-primary submit-btn" value="{{trans('employee_recognition_board.Submit')}}">
				</div>
			</form>
		</div>
	</div>
</div>

<script type="text/javascript">
    $(document).ready(function(){

            $('.add-recog-btn').on('click', function() {

			$.ajax({
				url: '{{ url("/get_employee_name_list") }}',
				type: 'GET',
				success: function(data) {
					// Auto Complete Feature
					var obj = $.parseJSON(data);

					$('#add-recog-autocomplete').autocomplete({
						minLength: 0,
						source:obj.employee_names,
						open: function(event, ui){
							var len = $('.ui-autocomplete > li').length;

							// shorten autocomplete list of items
							if(len > 4) {
								$('.ui-autocomplete').css({
									'height': '100px',
									'overflow-y': 'scroll',
									'overflow-x': 'hidden'
								});
							} else {
								$('.ui-autocomplete').css({
									'height': ''
								});
							}
						}
					}).focus(function(){
            			$(this).autocomplete("search");
        			});


					$('#add-new-recognition').modal('show');

					// append the autocomplete list to modal form
					$('#add-recog-autocomplete').autocomplete("option", "appendTo", "#add-recog-form");

					$('#add-recog-form').on('submit', function(e) {


                        e.preventDefault();

						var missingField = false;
						var validName = false;

						// Checking the empty value in required fields or not
						$('#add-recog-form').find('input').each(function(){
							if($(this).prop('required') && $(this).val() == '') {
								missingField = true;

								return;
							}
						});


						if(!missingField) {
							var selected_employee = $('#add-recog-autocomplete').val();

							for(var i = 0; i < obj.employee_names.length; i++) {
								if(obj.employee_names[i] == selected_employee) {
									$('#add-recog-employee-id').val(obj.employee_ids[i]);

									validName = true;
								}
							}

							if(!validName) {

								return false;

							} else {

								$('#add-new-recognition').modal('hide');

								$.ajax({
									url: '{{ url("/add_new_recognition") }}',
									type: 'POST',
									data: $('#add-recog-form').serialize(),
									success: function(data) {

									    window.location.reload();

									}
								});
							}
						}
					});
				}
			});
		});


    });
</script>

@endif
<!-- end widget grid -->
<script type="text/javascript">
    $(document).ready(function(){

        $('.view_more').click(function(){

            $(this).hide();
            $(this).prev().prev().hide();
            $(this).prev().fadeIn();

        });


        $('#dismiss_first').click(function(){

            $.ajax({
                url : '{{url('/dismiss_first')}}',
                type : 'POST',
                data : {id:<?php echo $user['id']; ?>, '_token':'{{ csrf_token() }}'},
                success:function(html){
                }
            });



        });


    });
</script>



@endsection