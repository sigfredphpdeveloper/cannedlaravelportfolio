
<script src="{{url()}}/js/dhtmlx/dhtmlxgantt.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="{{url()}}/css/dhtmlx/dhtmlxgantt.css" type="text/css" media="screen" title="no title" charset="utf-8">
<script type="text/javascript" src="{{url()}}/js/dhtmlx/testdata.js"></script>

<style>
    .gantt_row{cursor: pointer !important;}
</style>

<?php
    // set project id when change view type
    $project_url_id = !empty($project_id)?'/'.$project_id:'';
?>

<div class="row">

    <div class="col-xs-2">
        <select name="change_page" class="form-control" id="change_page">
            <option value="projects">By Projects</option>
            <option value="people-view">By People</option>
        </select>
    </div>

    <div class="col-xs-4 ">
        <select name="search" class="form-control" id="project_list">
            @foreach($boards as $board)
                <option <?=($project_id==$board['id'])?'selected':''?> value="{{$board['id']}}">{{$board['board_title']}}</option>
            @endforeach
        </select>
    </div>


    <div class="col-xs-2 ">
        <div class="btn-group" role="group" aria-label="Default button group">
            <a href="{{url('projects/day')}}{{$project_url_id}}" class="btn <?=($calendar_type=='day')?'btn-primary':'btn-default';?>">Day</a>
            <a href="{{url('projects/week')}}{{$project_url_id}}" class="btn <?=($calendar_type=='week')?'btn-primary':'btn-default';?>">Week</a>
            <a href="{{url('projects/month')}}{{$project_url_id}}" class="btn <?=($calendar_type=='month')?'btn-primary':'btn-default';?>">Month</a>
        </div>
    </div>

    <h2 class="col-xs-2 text-right pull-right" style="margin-top:0;">

        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#setMileStone">Set MileStone</button>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addtaskModal">Add Task</button>

    </h2>

</div>

<div class="modal fade" id="setMileStone" tabindex="-1" role="dialog" aria-labelledby="setMileStoneLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Set Project Milestone</h4>
            </div>
            <form action="{{url('milestone')}}" method="POST" id="milestone_form">
                <div class="modal-body">
                    {!! csrf_field() !!}
                    <input type="hidden" name="board_id" value="{{$project_id}}">
                    <div class="row">
                        <div class="form-group">
                            <div class="col-xs-2">Note</div>
                            <div class="col-xs-10">
                                <textarea name="note" class="form-control"><?=!empty($milestone['note'])?$milestone['note']:''?></textarea>
                            </div>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-xs-2">Date</div>
                            <div class="col-xs-10">
                                <input type="text" name="date" value="<?=!empty($milestone['milestone'])?$milestone['milestone']:''?>" class="milestone_dateapicker form-control" placeholder="Select date" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- view task modal -->
<div class="modal fade" id="viewTaskModal" tabindex="-1" role="dialog" aria-labelledby="viewTaskModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="paddin-top:10px;border:none !important;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="viewTaskModalLabel">View/Edit Task</h4>
            </div>
            <form action="{{url('save-view-task')}}" method="POST" class="taskEditForm" enctype="multipart/form-data">
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" id="archive_task" class="btn btn-primary">Archive</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" data-id_to_edit="">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>

function DHTMLXcustomDate(myData){

    if(myData.start_date.length == null){
        var s_day = myData.start_date.getDate();
        var s_month = myData.start_date.getMonth()+1;
        var s_year = myData.start_date.getFullYear();
        myData.start_date = s_year+'-'+s_month+'-'+s_day;
    }

    if(myData.end_date.length == null) {
        var e_day = myData.end_date.getDate();
        var e_month = myData.end_date.getMonth() + 1;
        var e_year = myData.end_date.getFullYear();
        myData.end_date = e_year + '-' + e_month + '-' + e_day;
    }

    return myData;
}

$(document).ready(function(){

    $('#project_list').on('change', function(){
        var value = $(this).val();
        location.href = "{{url('projects/'.$calendar_type)}}/"+value;
    })

    gantt.attachEvent("onAfterTaskDrag", function(id, mode, task, original){

        //gantt.getLink(id)
        console.log(gantt.getTask(id));
        var data = gantt.getTask(id);
//        var original = gantt.getTask(id);
//        data = DHTMLXcustomDate(data);

        var s_day = data.start_date.getDate();
        var s_month = data.start_date.getMonth();
        var s_year = data.start_date.getFullYear();
        var sd = s_year+'-'+(s_month+1)+'-'+s_day;

        var e_day = data.end_date.getDate();
        var e_month = data.end_date.getMonth();
        var e_year = data.end_date.getFullYear();
        var ed = e_day+'-'+(e_month+1)+'-'+e_year;
        var e_year = data.end_date.getFullYear();
        var ed = e_year+'-'+(e_month+1)+'-'+e_day;

        record = {'id':data.id, 'start_date': sd, 'end_date': ed, 'duration': data.duration, 'progress': data.progress, 'type':data.type, 'text':data.text};

        $.ajax({
            type : 'POST',
            dataType : 'JSON',
            data : {'data': record, '_token':'{{ csrf_token() }}'},
            url : '{{url('/save_project')}}',
            success:function(response){
            }
        });
    });



    $( '.milestone_dateapicker' ).pickadate({
        formatSubmit: 'yyyy-mm-dd',
        onStart: function ()
        {
            this.set('select', '<?=!empty($milestone['milestone'])?$milestone['milestone']:''?>'); // Set to current date on load
        },
        // min: [2015, 7, 14],
        //container: '#container',
        // editable: true,
        closeOnSelect: true,
        closeOnClear: true
    })

    $('#change_page').on('change',function(){
        var page = $(this).val();
        document.location.href = '{{url('/')}}/'+page;
    })

    $('.gantt_row').on('click', function(){

    })

    gantt.attachEvent("onTaskClick", function(id, mode, task, original){

        var data = gantt.getTask(id);

        if(data.type == 'task'){
            var id = data.id;
            $('#save_view_task').attr('data-id_to_edit',id); //set id to button to trigget which id is

            $.ajax({
                type : 'POST',
                dataType : 'JSON',
                data : {'id':id, '_token':'{{ csrf_token() }}'},
                url : '{{url('view-task')}}',
                success:function(response){

                    $('#viewTaskModal .modal-body').html(response);
                    $('#viewTaskModal').modal('show');

                }
            });
        }
    });



    gantt.templates.rightside_text = function(start, end, task){
        if(task.type == gantt.config.types.milestone){
            return task.text;
        }
        return "";
    };

    gantt.attachEvent("onAfterTaskDrag", function(id, mode){
        var task = gantt.getTask(id);
        if(mode == gantt.config.drag_mode.progress){
            var pr = Math.floor(task.progress * 100 * 10)/10;
            gantt.message(task.text + " is now " + pr + "% completed!");
        }else{
            var convert = gantt.date.date_to_str("%H:%i, %F %j");
            var s = convert(task.start_date);
            var e = convert(task.end_date);
            gantt.message(task.text + " starts at " + s + " and ends at " + e);
        }
    });
    gantt.attachEvent("onBeforeTaskChanged", function(id, mode, old_event){
        var task = gantt.getTask(id);
        if(mode == gantt.config.drag_mode.progress){
            if(task.progress < old_event.progress){
                gantt.message(task.text + " progress can't be undone!");
                return false;
            }
        }
        return true;
    });

    gantt.attachEvent("onBeforeTaskDrag", function(id, mode){
        var task = gantt.getTask(id);
        var message = task.text + " ";

        if(mode == gantt.config.drag_mode.progress){
            message += "progress is being updated";
        }else{
            message += "is being ";
            if(mode == gantt.config.drag_mode.move)
                message += "moved";
            else if(mode == gantt.config.drag_mode.resize)
                message += "resized";
        }

        gantt.message(message);
        return true;
    });



    gantt.attachEvent("onTaskDblClick", function(id,e){
       return false;
    });


})
</script>



