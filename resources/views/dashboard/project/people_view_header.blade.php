<script src="{{url()}}/js/dhtmlx/dhtmlxgantt.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="{{url()}}/css/dhtmlx/dhtmlxgantt.css" type="text/css" media="screen" title="no title" charset="utf-8">

<script type="text/javascript" src="{{url()}}/js/dhtmlx/testdata.js"></script>

<?php
// set project id when change view type
$user_id = !empty($user_id)?'/'.$user_id:'';
$team_id = !empty($team_id)?'?team_id='.$team_id:'';
?>

<div class="row">

    <div class="col-xs-2">
        <select name="change_page" class="form-control" id="change_page">
            <option value="projects">By Projects</option>
            <option value="people-view" selected="selected">By People</option>
        </select>
    </div>

    <div class="col-xs-6 ">

        <div class="row">
            <div class="col-md-6">
                <select name="search" multiple class="form-control" id="employee_list" style="height:50px;">
                    <option value="">Select Employee</option>
                    @foreach($employees as $employee)
                        <option <?php echo (in_array($employee->user_id,$where_user_ids))?'selected':'';?>
                        value="{{$employee->user_id}}">{{$employee->first_name}} {{$employee->last_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-6">
                <select name="team_id"  class="form-control"  id="team_id">
                    <option value="">Select Team</option>
                    @foreach($teams as $team)
                        <option <?php echo (!empty($_GET['team_id']) AND $_GET['team_id'] == $team->id)?'selected':'';?>
                        value="{{$team->id}}">{{$team->team_name}}</option>
                    @endforeach
                </select>
            </div>

        </div>

    </div>

     <div class="col-xs-2">
        <div class="btn-group" role="group" aria-label="Default button group">
            <a href="{{url('people-view/day')}}{{$user_id}}{{$team_id}}" class="btn <?=($calendar_type=='day')?'btn-primary':'btn-default';?>">Day</a>
            <a href="{{url('people-view/week')}}{{$user_id}}{{$team_id}}" class="btn <?=($calendar_type=='week')?'btn-primary':'btn-default';?>">Week</a>
            <a href="{{url('people-view/month')}}{{$user_id}}{{$team_id}}" class="btn <?=($calendar_type=='month')?'btn-primary':'btn-default';?>">Month</a>
        </div>
    </div>

    <h2 class="col-xs-2" style="margin-top:0;text-align:right;float:right">
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addtaskModal">Add Task</button>
    </h2>

</div>


<!-- view task modal -->
<div class="modal fade" id="viewTaskModal" tabindex="-1" role="dialog" aria-labelledby="viewTaskModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="paddin-top:10px;border:none !important;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="viewTaskModalLabel">View/Edit Task</h4>
            </div>
            <form action="{{url('save-view-task')}}" method="POST" class="taskEditForm" enctype="multipart/form-data">
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" id="archive_task" class="btn btn-primary">Archive</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" data-id_to_edit="">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>



<script>
$(document).ready(function(){
    $('#employee_list').on('change', function(){
        var value = $(this).val();

        if(value != '')
        location.href = "{{url('people-view/'.$calendar_type)}}/"+value;

    })

    $('#team_id').on('change', function(){
        var value = $(this).val();

        if(value != '')
        location.href = "{{url('people-view/'.(!empty($calendar_type)?$calendar_type.'/':'').'?team_id=')}}"+value;

    })




    gantt.attachEvent("onAfterTaskDrag", function(id, mode, task, original){

        //gantt.getLink(id)
        //gantt.getTask(id)
        console.log(gantt.getTask(id));
        var data = gantt.getTask(id);
//        var original = gantt.getTask(id);
//        data = DHTMLXcustomDate(data);

        var s_day = data.start_date.getDate();
        var s_month = data.start_date.getMonth();
        var s_year = data.start_date.getFullYear();
        var sd = s_year+'-'+(s_month+1)+'-'+s_day;

        var e_day = data.end_date.getDate();
        var e_month = data.end_date.getMonth();
        var e_year = data.end_date.getFullYear();
        var ed = e_day+'-'+(e_month+1)+'-'+e_year;

        record = {'id':data.id, 'start_date': sd, 'end_date': ed, 'duration': data.duration, 'progress': data.progress, 'type':data.type, 'text':data.text};

        $.ajax({
            type : 'POST',
            dataType : 'JSON',
            data : {'data': record, '_token':'{{ csrf_token() }}'},
            url : '{{url('/save_project')}}',
            success:function(response){
            }
        });
    });

    $('#change_page').on('change',function(){
        var page = $(this).val();
        document.location.href = '{{url('/')}}/'+page;
    })

    $('.gantt_tree_icon').remove();

    gantt.attachEvent("onTaskClick", function(id, mode, task, original){

        var data = gantt.getTask(id);

        if(data.type == 'task'){
            var id = data.id;

            $('#save_view_task').attr('data-id_to_edit',id); //set id to button to trigget which id is

            $.ajax({
                type : 'POST',
                dataType : 'JSON',
                data : {'id':id, '_token':'{{ csrf_token() }}'},
                url : '{{url('view-task')}}',
                success:function(response){

                    $('#viewTaskModal .modal-body').html(response);
                    $('#viewTaskModal').modal('show');

                }
            });
        }
    });

    gantt.attachEvent("onAfterTaskDrag", function(id, mode){
        var task = gantt.getTask(id);
        if(mode == gantt.config.drag_mode.progress){
            var pr = Math.floor(task.progress * 100 * 10)/10;
            gantt.message(task.text + " is now " + pr + "% completed!");
        }else{
            var convert = gantt.date.date_to_str("%H:%i, %F %j");
            var s = convert(task.start_date);
            var e = convert(task.end_date);
            gantt.message(task.text + " starts at " + s + " and ends at " + e);
        }
    });
    gantt.attachEvent("onBeforeTaskChanged", function(id, mode, old_event){
        var task = gantt.getTask(id);
        if(mode == gantt.config.drag_mode.progress){
            if(task.progress < old_event.progress){
                gantt.message(task.text + " progress can't be undone!");
                return false;
            }
        }
        return true;
    });

    gantt.attachEvent("onBeforeTaskDrag", function(id, mode){
        var task = gantt.getTask(id);
        var message = task.text + " ";

        if(mode == gantt.config.drag_mode.progress){
            message += "progress is being updated";
        }else{
            message += "is being ";
            if(mode == gantt.config.drag_mode.move)
                message += "moved";
            else if(mode == gantt.config.drag_mode.resize)
                message += "resized";
        }

        gantt.message(message);
        return true;
    });

    gantt.attachEvent("onTaskDblClick", function(id,e){
       return false;
    });

})
</script>



