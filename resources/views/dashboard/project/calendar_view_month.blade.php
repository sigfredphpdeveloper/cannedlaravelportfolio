@extends('dashboard.layouts.master')



@section('content')


@include('dashboard.project.calendar_view_header')

<div id="content" class="">

    <div class="row">
        <div id="gantt_here" style='width:100%; height:500px;'></div>
    </div>

</div>


<style>
.put-left-radius{
    -webkit-border-radius: 6px 0 0 6px !important;
    border-radius: 6px 0 0 6px !important;
}
.margi-bottom{
    margin-bottom: 10px;
}
.margi-bottom label{width:120px;}
.margi-bottom input[type="text"]{
    -webkit-border-radius: 6px 6px 6px 6px !important;
    border-radius: 6px 6px 6px 6px !important;
    width:250px;
}

/**
 * Sortable css
 **/
#sort_records { list-style-type: none; margin: 0; padding: 0;  }
#sort_records li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 12px; }
#sort_records li span { float:left;margin-left: -1.3em; margin-top:7px; }
.padding-left-none{padding-left:0 !important;}

.sort_records_edit { list-style-type: none; margin: 0; padding: 0;  }
.sort_records_edit li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 12px; }
.sort_records_edit li span { float:left;margin-left: -1.3em; margin-top:7px; }

.main-label{
    width:100px;
}

</style>



<link rel="stylesheet" type="text/css" media="screen" href="https://code.jquery.com/ui/jquery-ui-git.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.devbridge-autocomplete/1.2.24/jquery.autocomplete.js"></script>

<link rel="stylesheet" href="{{url('css')}}/classic.css">
<link rel="stylesheet" href="{{url('css')}}/classic.date.css">
<link rel="stylesheet" href="{{url('css')}}/classic.time.css">
<script src="{{url('js')}}/picker.js"></script>
<script src="{{url('js')}}/picker.date.js"></script>
<script src="{{url('js')}}/picker.time.js"></script>

      <!-- add task modal -->
<div class="modal fade" id="addtaskModal" tabindex="-1" role="dialog" aria-labelledby="addtaskModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addtaskModalLabel">Add new task</h4>
      </div>

          <form action="{{url('/create_task')}}/{{$board_id}}" method="POST" class="form-inline eventInsForm" enctype="multipart/form-data" >
              <input type="hidden" value="{{$board_id}}" name="board_id" />

              <div class="modal-body">
                <div class="alert alert-danger popup_error" style="display:none;">
                    Start and Due date is required.
                </div>
                  {!! csrf_field() !!}
                <div class="margi-bottom">
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" name="title" value="" class="form-control" placeholder="Title" required>
                    </div>
                </div>
                <div class="margi-bottom">
                    <div class="form-group">
                        <label>Details</label>
                        <textarea name="details" class="form-control" placeholder="Details" style="width:250px;" required></textarea>
                    </div>
                </div>
                <div class="margi-bottom">
                    <div class="form-group">
                        <label>Assigned to</label>
                        <input class="form-control delete_fields assignTo_autoComplete" autocomplete="off" type="text" value="" placeholder="Assigned to" required />
                        <input class="form-control delete_fields" type="hidden" name="assignto_id" id="assignto_id" value="" placeholder="" required />
                    </div>
                </div>
                <div class="margi-bottom">
                    <div class="form-group">
                        <label>Category</label>
                        <input class="form-control delete_fields category_autoComplete" autocomplete="off" type="text" value="" placeholder="Category" required />
                        <input type="hidden" name="category" id="task_category" value="" class="form-control" placeholder="Category" required>
                    </div>
                </div>
                <div class="margi-bottom">
                    <div class="form-group">
                        <label>Start Date</label>
                        <input type="text" name="start_date" value="" class="dateapicker form-control delete_fields" placeholder="Select date" required />
                    </div>
                </div>
                <div class="margi-bottom">
                    <div class="form-group">
                        <label>Due Date</label>
                        <input type="text" name="due_date" value="" class="dateapicker form-control delete_fields" placeholder="Select date" required />
                    </div>
                </div>
                <div class="margi-bottom">
                    <div class="form-group">
                        <label style="float:left;">File to Upload</label>
                        <input style="float:left;" type="file" name="file_upload[]" id="file_upload" value="" placeholder="File to Upload" multiple>
                    </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default hide-me-after" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary hide-me-after">Save changes</button>
              </div>
          </form>
        </div>
      </div>
    </div>
    </div>
  </div>
</div>



@endsection


@section('page-script')


<script>
$(document).ready(function(){
    $( ".assignTo_autoComplete" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "{{ url('/user-list') }}",
                dataType: "json",

                data: {
                    q: request.term
                },
                success: function( data ) {
//                console.log(data);
                   response(data);
                }
            });
        },
        select: function(e,ui){
//            console.log(ui);
            $('#assignto_id').val(ui.item.id);
        }
    });

    $( ".assignTo_autoComplete" ).autocomplete( "option", "appendTo", ".eventInsForm" );


    $( ".category_autoComplete" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "{{ url('/category-list') }}",
                dataType: "json",

                data: {
                    q: request.term
                },
                success: function( data ) {
                   response(data);
                }
            });
        },
        select: function(e,ui){
            $('#task_category').val(ui.item.id);
        }
    });

    $( ".category_autoComplete" ).autocomplete( "option", "appendTo", ".eventInsForm" );

    $( '.dateapicker' ).pickadate({
        formatSubmit: 'yyyy-mm-dd',
        // min: [2015, 7, 14],
        //container: '#container',
        // editable: true,
        closeOnSelect: true,
        closeOnClear: true
    })

    $('.timepicker').pickatime()

    $('.eventInsForm').submit(function(event){
        if($(this).find('.dateapicker').val() == ''){
            $('.popup_error').show();
            event.preventDefault();
        }
        $('.hide-me-after').hide();

    })

    $('.view_task').on('click', function(){
        var id = $(this).data('id');
        $('#save_view_task').attr('data-id_to_edit',id); //set id to button to trigget which id is

        $.ajax({
            type : 'POST',
            dataType : 'JSON',
            data : {'id':id, '_token':'{{ csrf_token() }}'},
            url : '{{url('view-task')}}',
            success:function(response){

                $('#viewTaskModal .modal-body').html(response);
                $('#viewTaskModal').modal('show');

            }
        });
    })

    $('#save_view_task').on('click', function(e){
        var me = $(this);

        var myfile = $('.file_here'+me.data('id_to_edit'));
        var myfile = document.getElementById('file_here'+me.data('id_to_edit'));
        file = myfile.files;

        me.html('Saving..').removeClass('btn-primary').addClass('btn-default');
        var form = $('.taskEditForm').serialize();
        $.ajax({
            type : 'POST',
            dataType : 'JSON',
            data : form,
            url : '{{url('save-view-task')}}',
            success:function(response){

                if(response == true || response == 'true'){

                    me.html('Save changes').removeClass('btn-default').addClass('btn-primary');
                    $('#view_task_title').hide();
                    $('#view_task_title').prev().html('<h2 style="margin:0">'+$('#view_task_title').val()+'</h2>').show();

                    $('#view_task_details').hide();
                    $('#view_task_details').prev().html($('#view_task_details').val()).show();

//                    $('#view_due_date').hide();
//                    $('#view_due_date').prev().html($('#view_due_date').val()).show();

                    $('.change_details').show();
                }

            }
        });

    })

    $('.delete_selected_task').on('click', function(){
        var id = $(this).data('id');
        $.ajax({
            type : 'POST',
            dataType : 'JSON',
            data : {'id':id, '_token':'{{ csrf_token() }}'},
            url : '{{url('delete-task')}}',
            success:function(response){
                if(response == true || response == 'true'){
                    location.reload();
                }
            }
        });

    })




})
</script>


<script type="text/javascript">

var tasks = <?=json_encode($all_data);?>;


gantt.config.scale_unit = "month";
gantt.config.date_scale = "%F";
gantt.config.min_column_width = 700;

gantt.config.scale_height = 50;

gantt.config.subscales = [
    {unit:"year", step:1, date:"%Y" }
];


gantt.init("gantt_here", new Date(<?=date('Y',strtotime($oldest_date))?>, <?=date('m',strtotime($oldest_date))?>, <?=date('d',strtotime($oldest_date))?>), new Date(<?=date('Y',strtotime($newest_date))?>, <?=date('m',strtotime($newest_date))?>, <?=date('d',strtotime($newest_date))?>));

gantt.config.columns = [
        {name:"text",       label:"Task name",  width:"*", tree:true,align: "left" },
        {name:"employee_name", label:"Name", align: "left" },
    ];

console.log(gantt.config);

gantt.parse(tasks);



</script>
@endsection