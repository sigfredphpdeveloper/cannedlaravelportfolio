@extends('dashboard.layouts.master')

@section('content')



<div id="content" class="">


</div>


<!-- view task modal -->
<div class="modal fade" id="viewTaskModal" tabindex="-1" role="dialog" aria-labelledby="viewTaskModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="viewTaskModalLabel">View task</h4>
      </div>
      <form method="POST" class="taskEditForm">
          <div class="modal-body">

          </div>
          <div class="modal-footer">
            <button type="button" id="archive_task" class="btn btn-primary">Archive</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" id="save_view_task" class="btn btn-primary">Save changes</button>
          </div>
      </form>
    </div>
  </div>
</div>




@endsection


@section('page-script')

<script>
$(document).ready(function(){



})
</script>
@endsection