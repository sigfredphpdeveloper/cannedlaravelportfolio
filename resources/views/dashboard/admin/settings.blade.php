@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
<style type="text/css">
    .a_settings .col-md-3{
        padding-top:16px;
        text-align:center;
        position:relative;
        height:127px;
    }
.a_settings .col-md-3 img{
margin:0 auto;
    }
    .a_settings .col-md-3 a{
            color:#333;
        }
.a_settings .col-md-3 p{
            width: 100%;
            text-align: center;
            margin-top: 16px;
    }
</style>
    <div class="row">

        <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        </div>

        <article class="col-sm-12 col-md-12 col-lg-12">

                <div class="row a_settings" >
                    <div class="col-md-3">
                        <a href="{{ url('/roles') }}">
                            <img src="{{ asset('img/mng-roles.png') }}"  class="img-responsive" />
                            <p>Manage Roles</p>
                        </a>
                    </div>
                    <div class="col-md-3">
                          <a href="{{ url('/teams') }}">
                              <img src="{{ asset('img/mng-teams.png') }}"  class="img-responsive" />
                              <p>Manage Teams</p>
                          </a>
                    </div>
                    <div class="col-md-3">
                        <a href="{{ url('/permissions') }}">
                            <img src="{{ asset('img/product-icon9.png') }}"  class="img-responsive" />
                            <p>Manage Permissions</p>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a href="{{ url('/personalized_fields') }}">
                            <img src="{{ asset('img/modify.png') }}"  class="img-responsive" />
                            <p>Company Settings</p>
                        </a>
                    </div>
                </div>


                <div class="row a_settings" >

                    <div class="col-md-3">
                          <a href="{{ url('/file_settings') }}">
                              <img src="{{ asset('img/files-b.png') }}"  class="img-responsive" />
                              <p>File Sharing</p>
                          </a>
                    </div>
                    <div class="col-md-3">
                            <a href="{{ url('/organization_setting') }}">
                                <img src="{{ asset('img/product-icon2.png') }}"  class="img-responsive" />
                                <p>Contacts</p>
                            </a>
                    </div>
                    <div class="col-md-3">
                        <a href="{{ url('/announcement_setting') }}">
                            <img src="{{ asset('img/product-icon4.png') }}"  class="img-responsive" />
                            <p>Announcements</p>
                        </a>
                    </div>
                    <div class="col-md-3">
                          <a href="{{ url('/emergency_contacts_settings') }}">
                              <img src="{{ asset('img/emergeny-contact-b.png') }}"  class="img-responsive" />
                              <p>Emergency Contacts</p>
                          </a>
                    </div>

                </div>

                <div class="row a_settings" >

                    <div class="col-md-3">
                            <a href="{{ url('/expenses-setting') }}">
                                <img src="{{ asset('img/product-icon6.png') }}"  class="img-responsive" />
                                <p>Expenses</p>
                            </a>
                    </div>
                    <div class="col-md-3">
                          <a href="{{ url('/crm_setting') }}">
                              <img src="{{ asset('img/product-icon3.png') }}"  class="img-responsive" />
                              <p>CRM</p>
                          </a>
                    </div>

                    <div class="col-md-3">
                            <a href="{{ url('/trainings-setting') }}">
                                <img src="{{ asset('img/trainings_icon.png') }}"  class="img-responsive" />
                                <p>Trainings</p>
                            </a>
                    </div>
                    <div class="col-md-3">
                        <a href="{{ url('/leaves-setting') }}">
                            <img src="{{ asset('img/leaves-icon.png') }}"  class="img-responsive" />
                            <p>Leaves</p>
                        </a>
                    </div>
                </div>

                <div class="row a_settings">
                    <div class="col-md-3">
                        <a href="{{ url('/employee_recognition_board_settings') }}">
                            <img src="{{ asset('img/recognition_board_icon.png') }}" 
                                class="img-responsive" style="height: 75px"/>
                            <p>Recognition Board</p>
                        </a>
                    </div>
                    <div class="col-md-3">
                        <a href="{{ url('/asset_management_settings') }}">
                            <img src="{{ asset('img/asset_management_icon.png') }}" 
                                class="img-responsive" style="height: 75px"/>
                            <p>Asset Management</p>

                        </a>
                    </div>
                    <div class="col-md-3">
                        <a href="{{ url('/task-setting') }}">
                            <img src="<?php echo url('/');?>/img/task.png"  class="img-responsive" style="height:63px;width:94px;" />
                            <p>Task</p>
                        </a>
                    </div>
                    @if( isset($company) AND ($company['country']=='Singapore' || $company['country']=='SG') )
                        <div class="col-md-3">
                            <a href="{{ url('/payroll-settings') }}">
                                <i class="fa fa-fw fa-tag" style="font-size: 45px;padding-top: 20px;color: #f15f1d;"></i>
                                <p>Payroll</p>
                            </a>
                        </div>
                    @endif
                </div>


        </article>

    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->


@endsection