@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->

    <div class="row">

        <div class="col-md-12">
            <h1>My Emergency Contacts</h1>
        </div>


        <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        </div>

        <article class="col-sm-6 col-md-6 col-lg-6">


        {!! Form::open(
        array(
        'url' => 'my_emergency_contacts_save',
        'files' => true,
        'id'=>'smart-form-register',
        'class'=>'smart-form client-form'
        )) !!}


        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="user_id" value="{{ $user['id'] }}">

                <fieldset>

                 <section>
                    <label class="label">Contact Name</label>
                    <label class="input">
                    	<input type="text" name="contact_name" placeholder="Contact Name" />
                    	<b class="tooltip tooltip-bottom-right">Required</b> </label>
                    </label>
                </section>

                 <section>
                    <label class="label">Contact's Phone Number</label>
                    <label class="input">
                        <input type="text" name="contact_phone_number" placeholder="Contact's Phone Number">
                        <b class="tooltip tooltip-bottom-right">Required</b> </label>
                </section>

                <section>
                	<label class="label">Address</label>
                    <textarea class="form-control delete_fields" name="address" id="" cols="30" rows="5"></textarea>
                    <b class="tooltip tooltip-bottom-right">Required</b>



            </fieldset>
			<fieldset>
                <div class="row">
                <section class="col col-5">
                    <label class="label">Relationship</label>
                    <label class="input">
                        <select name="relationship" class="form-control" id="relationship-select">
                                <option value="Spouse" >Spouse</option>
                                <option value="Parent" >Parent</option>
                                <option value="Child" >Child</option>
								<option value="Friend">Friend</option>
								<option value="Other">Other</option>                        


                        </select>
                    </label>
                </section>
			</fieldset>


			<footer>
                <button type="submit" class="btn btn-success">
                    Save
                </button>
            </footer>

        </article>

    </section>



@endsection


@section('page-script')
<script type="text/javascript">
$( "#relationship-select" ).change(function() {
  
  ///alert($(this).val());
  //var other = $(this).val();
  //if (other.equals("Other")) { 
   	//function appendText() {
   	//var textboxother = "<p><input type="text" name="other" placeholder="Please specify" /></p>";
   //}
  	//$(this).append(textboxother);}

    var textboxother = '<input type="text" name="other" placeholder="Please specify" />';
    


    if ($(this).val()==="Other") {
    //  <input type="text" name="other" placeholder="Please specify" />
      //alert($(this).val());
      $("#relationship-select").after(textboxother);
    }

    else {
           


    }
  })

</script>
@endsection
