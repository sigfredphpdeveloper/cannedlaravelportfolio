@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->

    <div class="row">

        <!--
        <div class="col-md-12">
            <h1>{{trans("my_profile.Update Profile")}}</h1>
        </div>
        -->

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>{{trans("my_profile.Warning")}}<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        </div>

        <article class="col-md-8 col-md-offset-2">

        <div>

          <!-- Nav tabs -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#my_profile" aria-controls="home" role="tab" data-toggle="tab">{{trans("my_profile.My Profile")}}</a></li>
            <li role="presentation"><a href="#emergency_contact" aria-controls="profile" role="tab" data-toggle="tab">{{trans("my_emergency_contact.Emergency Contact")}}</a></li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="my_profile">

                <form id="smart-form-register" class="smart-form client-form" action="{{url('my_profile_save')}}" method="POST" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="user_id" value="{{ $user['id'] }}">


                    <fieldset>

                         <section>
                             <label class="label">{{trans("my_profile.Position")}}</label>
                            <label class="input">
                            <input type="text" name="position" placeholder="Position" value="{{ $user['position'] }}" />

                            </label>
                        </section>

                         <section>
                            <label class="label">{{trans("my_profile.Phone Number")}}</label>
                            <label class="input">
                                <input type="text" name="phone_number" placeholder="Phone Number" value="{{ $user['phone_number'] }}">
                                <b class="tooltip tooltip-bottom-right">{{trans("my_profile.Optional")}}</b> </label>
                        </section>


                    </fieldset>

                    <fieldset>

                        <div class="row">
                        <section class="col col-2">
                            <label class="label">{{trans("my_profile.Title")}} *</label>
                            <label class="input">
                                <select name="title" class="form-control" required>
                                        <option value="Mr" <?php echo ($user['title']=='Mr')?'selected':'';?>>Mr</option>
                                        <option value="Mrs" <?php echo ($user['title']=='Mrs')?'selected':'';?>>Mrs.</option>
                                        <option value="Ms" <?php echo ($user['title']=='Ms')?'selected':'';?>>Ms.</option>
                                </select>
                            </label>
                        </section>
                            <section class="col col-5">
                                <label class="label">{{trans("my_profile.First Name")}}*</label>
                                <label class="input">
                                    <input type="text" name="first_name" placeholder="First name *" value="{{ $user['first_name'] }}" >
                                </label>
                            </section>
                            <section class="col col-5">
                                <label class="label">{{trans("my_profile.Last Name")}} *</label>
                                <label class="input">
                                    <input type="text" name="last_name" placeholder="Last name *" value="{{ $user['last_name'] }}" >
                                </label>
                            </section>
                        </div>

                        <section >
                            <label class="input">
                            <label class="label">{{trans("my_profile.Company Website")}}</label>
                                <input type="text" name="company_url" value="{{ $company['url'] }}" placeholder="Company URL *" disabled>
                            </label>
                        </section>

                        <section>
                           <label class="label">{{trans("my_profile.Email")}}</label>
                            <label class="input"> <i class="icon-append fa fa-lock"></i>
                                <input type="email" name="email" placeholder="Email *" value="{{ $user['email'] }}"
                                class="form-control" disabled/>
                                <b class="tooltip tooltip-bottom-right">{{trans("my_profile.Used to log in")}}</b> </label>
                        </section>

                        <section >
                            <label class="label">{{trans("my_profile.New Password")}}</label>
                            <label class="input">
                                <input type="password" name="password"  placeholder="{{trans("my_profile.New Password")}}" >
                            </label>
                        </section>

                        <section >
                            <label class="label">{{trans("my_profile.Confirm New Password")}}</label>
                            <label class="input">
                                <input type="password" name="password_confirmation"  placeholder="{{trans("my_profile.Confirm New Password")}}" >
                            </label>
                        </section>

                        <section>
                            <label class="label">{{trans("my_profile.Profile Picture")}}</label>

                            <div id="photoCont_logo" >

                                @if($user['photo'])
                                    <img src="{{$user['photo']}}?version=<?php echo time();?>" />
                                @endif

                            </div>

                            <label class="input">

                                <br class="clr" />

                                <input type="file" class="form-control" name="file_upload" id="pic_upload"/>

                                <input type="text" name="photo" id="photo_value" class="hide"/>

                            </label>


                        </section>

                        <?php echo view('dashboard.user.custom_fields',compact('user_fields','custom_keypairs_array')); ?>

                    </fieldset>


                    <footer>
                        <button type="submit" class="btn btn-success">
                            {{trans("my_profile.Update")}}
                        </button>
                    </footer>


                </form>


            </div>
            <div role="tabpanel" class="tab-pane" id="emergency_contact">

                     <form  id="smart-form-register" class="smart-form client-form" role="form" method="POST" action="{{ url('/emergency_contacts_save') }}">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="user_id" value="{{ $user['id'] }}">
                    <input type="hidden" name="company_id" value="{{ $company['id'] }}">
                    <input type="hidden" name="id" value="{{$ec['id']}}" />

                        <fieldset>

                             <section>
                                <label class="label">{{trans("my_emergency_contact.Contact Name")}}</label>
                                <label class="input">
                                <input type="text" name="contact_name" placeholder="{{trans("my_emergency_contact.Contact Name")}}" value="{{$ec['contact_name']}}" class="form-control"/>
                                </label>
                            </section>

                            <section>
                                <label class="label">{{trans("my_emergency_contact.Phone Number")}}</label>
                                <label class="input">
                                <input type="text" name="contact_phone" placeholder="{{trans("my_emergency_contact.Phone Number")}}" value="{{$ec['contact_phone']}}" class="form-control"/>
                                </label>
                            </section>

                            <section>
                                <label class="label">{{trans("my_emergency_contact.Address")}}</label>
                                <label class="input">
                                <textarea name="contact_address" class="form-control">{{$ec['contact_address']}}</textarea>
                                </label>
                            </section>

                            <section>
                                <label class="label">{{trans("my_emergency_contact.Relationship")}}</label>
                                <label class="input">
                                    <select name="relationship" id="relationship" class="form-control">
                                        <option value="Spouse" <?php echo $ec['relationship']=='Spouse'?'selected':'';?>>{{trans("my_emergency_contact.Spouse")}}</option>
                                        <option value="Parent" <?php echo $ec['relationship']=='Parent'?'selected':'';?>>{{trans("my_emergency_contact.Parent")}}</option>
                                        <option value="Child" <?php echo $ec['relationship']=='Child'?'selected':'';?>>{{trans("my_emergency_contact.Child")}}</option>
                                        <option value="Friend" <?php echo $ec['relationship']=='Friend'?'selected':'';?>>{{trans("my_emergency_contact.Friend")}}</option>
                                        <option value="Other" <?php echo $ec['relationship']=='Other'?'selected':'';?>>{{trans("my_emergency_contact.Other")}}</option>
                                    </select>
                                </label>
                            </section>

                             <section id="secion_other" <?php echo $ec['relationship_other']==''?'style="display:none;"':'';?>>
                                <label class="label">{{trans("my_emergency_contact.Other Relationship")}}</label>
                                <label class="input">
                                <input type="text" name="relationship_other"  value="{{$ec['relationship_other']}}" class="form-control" />
                                </label>
                            </section>

                        </fieldset>

                        <footer>
                            <button type="submit" class="btn btn-success">
                                {{trans("my_emergency_contact.Save")}}
                            </button>
                        </footer>

                    </form>



            </div>


          </div>

        </div>







    </article>

    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->

<script type="text/javascript">
    $(document).ready(function(){
        $('#relationship').change(function(){

            var v = $(this).val();

            if(v=='Other'){
                $('#secion_other').show();
            }else{
                $('#secion_other').hide();
            }

        });

        $('#pic_upload').uploadifive({
            'buttonText' : '<i class="fa fa-upload"></i> Upload Picture',
            'uploadScript' : '{{url('/upload_profile_pic')}}',
            'fileType' : 'image/*',
            'fileSizeLimit' : '5MB',
            'width' : 160,
            'multi':false,
            'onUploadComplete' : function(file, data, response) {

                $('#photo_value').val(data);

                $('#pic_upload').uploadifive('clearQueue');

                var img = '<img src="';
                img = img + data + '?version=<?php echo time();?>" />';
                $('#photoCont_logo').html(img);

            },
            'formData'         : {
                'user_id' : '{{$user['id']}}',
                '_token':'{{ csrf_token() }}'}
        });

    });
</script>

@endsection