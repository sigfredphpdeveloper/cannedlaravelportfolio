@foreach($user_fields as $field)

    <section >
        <label class="label">{{ ucwords(str_replace('_',' ',$field['field_name']))}}</label>
        <label class="input">

            <?php
                    $value = '';
                    $field_name_key = str_replace(' ','_',$field['field_name']);

                    if(array_key_exists($field_name_key,$custom_keypairs_array)){
                        $value = $custom_keypairs_array[$field_name_key];
                    }
            ?>


            <?php switch($field['field_type']){

                case 'text':

                    echo '<input type="text" name="'.$field['field_name'].'" class="form-control" value="'.$value.'"/>';

                 break;

               case 'date':

                    echo '<input type="date" name="'.$field['field_name'].'" class="form-control" value="'.$value.'"/>';

                break;

                case 'number':

                    echo '<input type="number" name="'.$field['field_name'].'" class="form-control" value="'.$value.'" />';

                break;

                case 'list_dropdown':

                    echo '<select name="'.$field['field_name'].'" class="form-control">';

                    $options = explode("\n", $field['field_value']);

                    foreach($options  as $option){

                        if($option != ''){
                            $val_option = trim(preg_replace('/\s\s+/', ' ', $option));

                            $selected = '';

                            if($value==$val_option){
                                $selected = 'selected';
                            }

                            echo '<option value="'.$val_option.'" '.$selected.'>'.$option.'</option>';
                        }

                    }

                    echo '</select>';

                break;

             }
                ?>
        </label>
    </section>

@endforeach