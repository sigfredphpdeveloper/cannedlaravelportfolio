@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->

    <div class="row">

        <div class="col-sm-12 col-md-12">
        <h1>{{trans("complete_profile.step2")}} : {{trans("complete_company.Complete Your Company Profile")}}</h1>
        </div>

        <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        </div>

        <div class="col-md-12">
            @if(Session::has('error'))
                <div class="alert alert-danger">
                    <p>{{ Session::get('error') }}</p>
                </div>
            @endif
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>{{trans("complete_company.Whoops!")}}</strong> {{trans("complete_company.There were some problems with your input.")}}<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="alert ">
            <p>{{trans("complete_company.Please fill in the form to configure the platform for your company (Step 2)")}}</p>
        </div>

        <article class="col-sm-6 col-md-6 col-lg-6">

            <form id="smart-form-register" class="smart-form client-form" method="POST" enctype="multipart/form-data" action="{{url('/complete_profile')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="company_id" value="{{ $company['id'] }}">
            <input type="hidden" name="user_id" value="{{ $user['id'] }}">

                <header>

                     {{trans("complete_company.My Company")}}

                </header>

                <fieldset>

                     <section>
                        <label class="label">{{trans("complete_company.Company")}} *</label>
                        <label class="input">
                        <input type="text" name="company_name" placeholder="{{trans("complete_company.Name of Company")}}" value="{{ $company['company_name'] }}" />

                        </label>
                    </section>

                    <section>
                        <label class="label">{{trans("complete_company.Website")}}</label>
                        <label class="input">
                        <input type="text" name="url" placeholder="{{trans("complete_company.Website")}}" value="{{ $company['url'] }}" disabled/>

                        </label>
                    </section>

                    <section>
                        <label class="label">{{trans("complete_company.Industry *")}}</label>
                        <label class="input">
                        <select name="industry" class="form-control" placeholder="{{trans("complete_company.Select Industry")}}">
                           <option value="">{{trans("complete_company.--- Select Industry ---")}}</option>
                           <option value="Agency (branding/design/creative/video)" <?php echo ($company['industry']=='Agency (branding/design/creative/video)')?'selected':'';?>>{{trans("complete_company.Agency (branding/design/creative/video)")}}</option>
                           <option value="Agency (web)" <?php echo ($company['industry']=='Agency (web)')?'selected':'';?>>{{trans("complete_company.Agency (web)")}}</option>
                           <option value="Agency (SEM/SEO/social media)" <?php echo ($company['industry']=='Agency (SEM/SEO/social media)')?'selected':'';?>>{{trans("complete_company.Agency (SEM/SEO/social media)")}}</option>
                           <option value="Agency (other)" <?php echo ($company['industry']=='Agency (other)')?'selected':'';?>>{{trans("complete_company.Agency (other)")}}</option>
                           <option value="Church/religious organisation" <?php echo ($company['industry']=='Church/religious organisation')?'selected':'';?>>{{trans("complete_company.Church/religious organisation")}}</option>
                           <option value="E-Commerce" <?php echo ($company['industry']=='E-Commerce')?'selected':'';?>>{{trans("complete_company.E-Commerce")}}</option>
                           <option value="Engineering" <?php echo ($company['industry']=='Engineering')?'selected':'';?>>{{trans("complete_company.Engineering")}}</option>
                           <option value="Health/beauty" <?php echo ($company['industry']=='Health/beauty')?'selected':'';?>>{{trans("complete_company.Health/beauty")}}</option>
                           <option value="Hospitality" <?php echo ($company['industry']=='Hospitality')?'selected':'';?>>{{trans("complete_company.Hospitality")}}</option>
                           <option value="Industrials" <?php echo ($company['industry']=='Industrials')?'selected':'';?>>{{trans("complete_company.Industrials")}}</option>
                           <option value="IT/technology" <?php echo ($company['industry']=='IT/technology')?'selected':'';?>>{{trans("complete_company.IT/technology")}}</option>
                           <option value="Media/publishing" <?php echo ($company['industry']=='Media/publishing')?'selected':'';?>>{{trans("complete_company.Media/publishing")}}</option>
                           <option value="Non profit" <?php echo ($company['industry']=='Non profit')?'selected':'';?>>{{trans("complete_company.Non profit")}}</option>
                           <option value="Retail/consumer merchandise" <?php echo ($company['industry']=='Retail/consumer merchandise')?'selected':'';?>>{{trans("complete_company.Retail/consumer merchandise")}}</option>
                           <option value="School/education" <?php echo ($company['industry']=='School/education')?'selected':'';?>>{{trans("complete_company.School/education")}}</option>
                           <option value="Software/SaaS" <?php echo ($company['industry']=='Software/SaaS')?'selected':'';?>>{{trans("complete_company.Software/SaaS")}}</option>
                           <option value="Travel/leisure" <?php echo ($company['industry']=='Travel/leisure')?'selected':'';?>>{{trans("complete_company.Travel/leisure")}}</option>
                            <option value="Other" <?php echo ($company['industry']=='Other')?'selected':'';?>>{{trans("complete_company.Other")}}</option>

                        </select>
                        </label>
                    </section>

                    <section>
                        <label class="label">{{trans("complete_company.Company Size")}}</label>
                        <label class="input">
                        <select name="size" class="form-control">
                           <option value="1-10" <?php echo ($company['size']=='1-10')?'selected':'';?>>1-10</option>
                           <option value="10-50" <?php echo ($company['size']=='10-50')?'selected':'';?>>10-50</option>
                           <option value="50-500" <?php echo ($company['size']=='50-500')?'selected':'';?>>50-500</option>
                           <option value="500+" <?php echo ($company['size']=='500+')?'selected':'';?>>500+</option>
                         </select>
                        </label>
                    </section>



                     <section>
                        <label class="label">{{trans("complete_company.Address")}} *</label>
                        <label class="input">
                        <input type="text" name="address" placeholder="{{trans("complete_company.Address *")}}" value="{{ $company['address'] }}" />

                        </label>
                    </section>


                     <section>
                        <label class="label">{{trans("complete_company.Zip Code")}} *</label>
                        <label class="input">
                        <input type="text" name="zip" placeholder="{{trans("complete_company.Zip Code")}}" value="{{ $company['zip'] }}" />

                        </label>
                    </section>

                    <div class="row">
                        <section class="col col-3">
                            <label class="label">{{trans("complete_company.City")}} *</label>
                            <label class="input">
                               <input type="text" class="form-control" name="city" placeholder="{{trans("complete_company.City")}}" value="{{ $company['city'] }}" />
                            </label>
                        </section>
                        <section class="col col-3">
                            <label class="label">{{trans("complete_company.Country *")}} *</label>
                            <label class="input">

                                @include('...common.countries', ['default' =>  $company['country'] ])

                            </label>
                        </section>
                        <section class="col col-3">
                            <label class="label">{{trans("complete_company.State")}}</label>
                            <label class="input">
                                <input type="text" class="form-control" name="state" placeholder="{{trans("complete_company.State")}}" value="{{ $company['state'] }}" />
                            </label>
                        </section>
                    </div>

                    <section>
                        <label class="label">{{trans("complete_company.Phone Number")}}</label>
                        <label class="input">
                        <input type="text" name="phone_number" placeholder="{{trans("complete_company.Phone Number")}}" value="{{ $company['phone_number'] }}" />

                        </label>
                    </section>

                    <section>
                        <label class="label">{{trans("complete_company.Company Logo")}} {{trans("complete_company.recommended")}}</label>
                        <label class="input">
                            {!! Form::file('logo', null) !!}
                        </label>
                    </section>

                </fieldset>

                <footer>
                    <button type="submit" class="btn btn-success">
                        {{trans("complete_company.Update")}}
                    </button>
                </footer>

            </form>

        </article>

    </div>

</section>


@endsection