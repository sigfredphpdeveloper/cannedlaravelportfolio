@extends('dashboard.layouts.master')

@section('content')

<style>
.color-orange{ color:#FD9940; }
.btn-default{
    background:#e7e7e7;
}

</style>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->

    <div class="row">

        <div class="col-md-8" style="height:30px;"></div>

        <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        </div>

        @if(Session::has('error'))
        <div class="col-md-12">
            <div class="alert alert-danger">
                <p>{{ Session::get('error') }}</p>
            </div>
        </div>
        @endif

        <div class="col-md-2">
             @if($edit_employees)
             <a href="{{url('/invite_user')}}" class="btn btn-primary" >{{trans("directory_manager.Add User")}}</a>
             @endif
        </div>

        <div class="col-md-4 ">


         <form class="form-inline" method="GET">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon put-left-radius" style="padding: 0;border: 0;">
                            <select name="type" class="form-control put-left-radius" id="search_filter">
                                <option value="name" <?php echo (!empty($_GET['type']) AND $_GET['type']=='name')?'selected':'';?> >{{trans("complete_company.Name")}}</option>
                                <option value="role" <?php echo (!empty($_GET['type']) AND $_GET['type']=='role')?'selected':'';?> >{{trans("complete_company.Role")}}</option>
                                <option value="team" <?php echo (!empty($_GET['type']) AND $_GET['type']=='team')?'selected':'';?> >{{trans("complete_company.Team")}}</option>
                                <option value="title" <?php echo (!empty($_GET['type']) AND $_GET['type']=='title')?'selected':'';?> >{{trans("complete_company.Title")}}</option>
                            </select>
                        </div>
                        <div id="change_here">
                            <input type="text" name="key" value="<?php echo (!empty($_GET['key']))?$_GET['key']:'';?>" class="form-control" placeholder="{{trans("complete_company.Name, Role or Team")}}">
                        </div>
                        <span class="input-group-btn"> <button class="btn btn-default" id="search_btn" type="submit">{{trans("crm.Search")}}</button></span>
                    </div>
                </div>
            </form>

        </div>

        <div class="col-sm-6 ">

            <?php if($assign_roles):?>
                <a href="javascript:void(0);" data-toggle="tooltip" title="{{trans("directory_manager.Assign Employee Roles")}}"
                id="btn_assign_role" class="btn btn-primary pull-right">{{trans("directory_manager.Assign Roles")}}</a>
            <?php endif; ?>

            <?php if($assign_teams):?>
                <a href="javascript:void(0);" data-toggle="tooltip" title="{{trans("directory_manager.Assign Employee Teams")}}" style="margin-right:10px;" id="btn_assign_team" class="btn btn-primary pull-right">{{trans("directory_manager.Assign Teams")}}</a>
            <?php endif;?>

             @if($is_admin)
             <a href="{{url('/user_import')}}" class="btn btn-primary pull-right"  style="margin-right:10px;">{{trans("complete_company.Import CSV")}}</a>
            @endif

             @if($view_emergency_contacts)
             <a href="{{url('/manage_emergency_contacts')}}" class="btn btn-primary pull-right" style="margin-right:10px;">{{trans("complete_company.Emergency Contacts")}}</a>
             @endif
        </div>


        <div class="col-md-8" style="height:20px;"></div>

        <div class="col-md-12">

            <!--<h1>{{trans("directory_manager.Employee Directory")}}</h1> -->

            <table class="table table-striped">
                <!--
                <thead>

                    <tr>
                        <?php if($assign_roles OR $assign_teams):?>
                            <th style="vertical-align:middle;"></th>
                        <?php endif ?>
                        <th style="vertical-align:middle;">{{trans("directory_manager.Email")}}</th>
                        <th style="vertical-align:middle;">{{trans("directory_manager.Name")}}</th>
                        <th style="vertical-align:middle;">{{trans("directory_manager.Roles")}}</th>
                        <th style="vertical-align:middle;">{{trans("directory_manager.Teams")}}</th>
                        <th style="vertical-align:middle;" width="160"></th>
                    </tr>

                </thead>

                -->

                <tbody>

                @foreach($employees as $employee)
                <tr>
                    <?php if($assign_roles OR $assign_teams):?>
                        <td width="25" style="vertical-align:middle;">
                            <input type="checkbox" data-user-id="{{$employee->id}}" data-email="{{$employee->email}}" class="employees"/>
                        </td>
                    <?php endif;?>
                    <td width="90" style="text-align:center;vertical-align:middle;">
                        @if($employee->photo)



                        <div class="circle-div" style="background:url('{{$employee->photo}}') no-repeat;
                        background-position: center;background-size:50px 50px;width:50px;height:50px;  ">
                        </div>
                        @else
                            <div class="circle-div" >
                                <i class="fa fa-user fa-4" style="font-size:44px;color:gray;margin-top:6px;"></i>
                            </div>
                        @endif
                    </td>
                    <td style="vertical-align:middle;width:150px;">
                        {{$employee->title}} {{$employee->first_name}} {{$employee->last_name}}
                    </td>
                    <td style="vertical-align:middle;width:200px;">
                        <?php
                            $user_roles = $employee->user_roles->toArray();
                            $user_roles_string = '';
                            foreach($user_roles as $user_role){
                                $user_roles_string .= ' '.$user_role['role_name'].' ,';
                            }
                        ?>
                        <?php
                            if($user_roles_string){
                                echo '<p>'.rtrim($user_roles_string,',').'</p>';
                            }
                        ?>
                    </td>
                    <td style="vertical-align:middle;width:200px;">
                        <?php
                            $user_teams = $employee->user_teams->toArray();
                            $user_teams_string = '';
                            foreach($user_teams as $user_team){
                                $user_teams_string .= ' '.$user_team['team_name'].' ,';
                            }
                            ?>
                         <?php
                             if($user_teams_string){
                                 echo '<p> '.rtrim($user_teams_string,',').'</p>';
                             }
                         ?>

                    </td>
                    <td width="120" style="vertical-align:middle;">
                    {{--<a href="{{url('/chats')}}"><i class="fa fa-comment-o color-orange" style="font-size:30px;"></i></a>--}}
                    <a href="mailto:{{$employee->email}}"><i class="fa fa-envelope-o color-orange" style="font-size:30px;"></i></a>
                        <?php
                            $user_teams = $employee->user_teams->toArray();
                            $user_teams_string = '';
                            foreach($user_teams as $user_team){
                                $user_teams_string .= ' '.$user_team['team_name'].' ,';
                            }

                        ?>
                    </td>
                    <td width="120" style="vertical-align:middle;">
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default" tabindex="-1">{{trans("directory_manager.Action")}}</button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1" aria-expanded="false">
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu " role="menu">
                                <li><a href="{{ url("user_view/") }}/{{ $employee->id }}" target="_blank">{{trans("directory_manager.View")}}</a></li>

                                @if($employee->completed_profile != 2)
                                <li class="divider"></li>
                                <li><a href="{{url('/resend_invitation/')}}/{{$employee->id}}">{{trans("directory_manager.Resend Invitation")}}</a></li>
                                @endif

                                @if($edit_employees AND $employee->is_master==0)
                                <li class="divider"></li>

                                    <li><a href="{{url('/directory_edit_user/'.$employee->id)}}"  >{{trans("directory_manager.Edit User")}}</a></li>
                                    <li><a href="javascript:void(0);" data-id="{{$employee->id}}" class="deleter">{{trans("directory_manager.Delete User")}}</a></li>
                                @endif

                            </ul>
                        </div>
                    </td>
                </tr>

                @endforeach

                </tbody>

                <tfooot>
                    <tr>
                        <td colspan="6"> {!! $employees->render() !!}</td>
                    </tr>
                </tfooot>

            </table>

        </div>


    </div>
    <!-- end row -->

</section>
<!-- end widget grid -->

<div class="modal fade" tabindex="-1" role="dialog" id="assign_role_modal">

  <div class="modal-dialog">

    <div class="modal-content">

        <form id="smart-form-register" class="" role="form" method="POST" action="{{ url('/employee_assign_roles_save') }}">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{trans("directory_manager.Assign Roles")}}</h4>
          </div>

          <div class="modal-body">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="user_ids" value="" id="user_ids" />

                <p>{{trans("directory_manager.Select the roles for the selected employees")}}</p>

                <section>

                    <label class="input" style="width:100%;">

                        <select name="roles[]" multiple class="form-control" required>

                            @foreach($roles as $role)
                                <option value="{{$role['id']}}">{{$role['role_name']}}</option>
                            @endforeach

                        </select>

                    </label>
                </section>
          </div>
          <div class="modal-footer">

            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("directory_manager.No")}}</button>
            <button type="submit" class="btn btn-success" alt="">{{trans("directory_manager.Save")}}</button>

          </div>
    </form>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->

</div><!-- /.modal -->


<div class="modal fade" tabindex="-1" role="dialog" id="assign_team_modal">

  <div class="modal-dialog">

    <div class="modal-content">

        <form id="smart-form-register" class="" role="form" method="POST" action="{{ url('/employee_assign_teams_save') }}">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{trans("directory_manager.Add Teams")}}</h4>
          </div>

          <div class="modal-body">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="user_ids" value="" id="user_ids_teams" />

                <p>{{trans("directory_manager.Select the teams for the selected employees")}}</p>

                <section>

                    <label class="input" style="width:100%;">

                        <select name="teams[]" multiple class="form-control" required>

                            @foreach($teams as $team)
                                <option value="{{$team['id']}}">{{$team['team_name']}}</option>
                            @endforeach

                        </select>

                    </label>

                </section>
          </div>
          <div class="modal-footer">

            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("directory_manager.No")}}</button>
            <button type="submit" class="btn btn-success"  alt="">{{trans("directory_manager.Save")}}</button>

          </div>
    </form>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="select_employees_modal">

  <div class="modal-dialog">

    <div class="modal-content">

        <form id="smart-form-register" class="" role="form" method="POST" action="{{ url('/add_folder_save') }}">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{trans("directory_manager.Select Employees")}}</h4>
          </div>

          <div class="modal-body">

            <p>{{trans("directory_manager.Please select employees")}}</p>
          </div>
          <div class="modal-footer">

            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("directory_manager.OK")}}</button>

          </div>
    </form>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="confirm">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{trans("directory_manager.Confirmation")}}</h4>
      </div>
      <div class="modal-body">
        <p>{{trans("directory_manager.Are you sure you want to delete this User ?")}} </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("directory_manager.No")}}</button>
       <button type="button" class="btn btn-primary" id="delete_proceed" alt="">{{trans("directory_manager.Yes")}}</button>

      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="welcome_modal">

  <div class="modal-dialog">

    <div class="modal-content">

        <form id="smart-form-register" class="" role="form" method="POST" action="{{ url('/employee_assign_teams_save') }}">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{trans("complete_company.Welcome")}}</h4>
          </div>

          <div class="modal-body">
            <p>Welcome

            {{trans("complete_company.Now that your company is set up")}}

            </p>
          </div>
          <div class="modal-footer">

            <a href="{{ url('/dashboard') }}" class="btn btn-default" >{{trans("complete_company.Skip")}}</a>
            <a href="{{ url('/process_invite_user') }}" class="btn btn-success" >{{trans("complete_company.OK")}}</a>

          </div>
    </form>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

<script type="text/javascript">

    $(document).ready(function(){

        $('[data-toggle="tooltip"]').tooltip();

        $('#btn_assign_role').click(function(){

            var count_checked = $('input.employees:checked').length;

            if(count_checked ==0){
                $('#select_employees_modal').modal('show');
            }else{

                var employee_ids = '';

                $('input.employees:checked').each(function(){
                   employee_ids = employee_ids + $(this).data('user-id') + ',';
                    $('#user_ids').val(employee_ids);
                });


                $('#assign_role_modal').modal('show');
            }

        });

        $('#btn_assign_team').click(function(){

            var count_checked = $('input.employees:checked').length;

            if(count_checked ==0){
                $('#select_employees_modal').modal('show');
            }else{

                var employee_ids = '';

                $('input.employees:checked').each(function(){
                   employee_ids = employee_ids + $(this).data('user-id') + ',';
                    $('#user_ids_teams').val(employee_ids);
                });

                $('#assign_team_modal').modal('show');
            }
        });

        $('.deleter').click(function(){

            var id = $(this).data('id');
            $('#delete_proceed').attr('alt',id);
            $('#confirm').modal('show');

        });

        $('#delete_proceed').click(function(){

            var id = $(this).attr('alt');

            $('#confirm').modal('hide');

            var url = '{{url('user_delete_front')}}/'+id;

            window.location.href=url;

        });

        $('#bulkupload').tooltip('toggle')

        $('#csv_upload').on('change', function(){
            $('#file_upload_form').submit();
        });

        $('#welcome_modal').modal('show');

    });

</script>
@endsection
