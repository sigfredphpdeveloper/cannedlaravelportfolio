
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="editBoardModalLabel">Edit Board</h4>
</div>

<form action="{{url('update_crm_board')}}" method="POST" class="form-inline">
    <div class="modal-body">
        {!! csrf_field() !!}
        <div class="margi-bottom">
            <div class="form-group">
                <label>Board Name</label>
                <input type="hidden" name="id" value="<?=$board['id']?>" />
                <input type="text" name="name" value="<?=$board['board_title']?>" class="form-control" placeholder="Board Name" required>
            </div>
        </div>
        <div class="margi-bottom">
            <div class="form-group">
                <label class="main-label">Visibility</label>
                <label style="width:60px"><input type="radio" name="visibility" value="me" class="form-control edit_visibility_options" <?=$board['visibility']=="me"?'checked="checked"':''?> />Me</label>
                <label style="width:60px"><input type="radio" name="visibility" value="team" class="form-control edit_visibility_options" <?=$board['visibility']=="team"?'checked="checked"':''?> />Team</label>
                <label style="width:80px"><input type="radio" name="visibility" value="everyone" class="form-control edit_visibility_options" <?=($board['visibility']=="everyone" || empty($board['visibility']))?'checked="checked"':''?> />Everyone</label>
            </div>

            <select name="teams[]" id="select_team_edit" class="form-control" <?=$board['visibility']=="team"?'required':''?> multiple style="margin-left:100px;<?=$board['visibility']!="team"?'display:none;':''?>" >
                @foreach($teams as $team)
                    <option value="{{$team['id']}}" <?=(in_array($team['id'],$board_teams))?'selected':''?> >{{$team['team_name']}}</option>
                @endforeach
            </select>

        </div>
        <div class="">
            <label>
                Columns
                <button type="button" class="btn btn-primary btn-sm add_columns_in_edit" style="padding: 3px 10px;">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </button>
            </label>
            <div id="columns_error" style="color:#f00;font-weight:bold;padding-left:5px;"></div>
            <ul id="sort_records<?=$board['id']?>" class="sort_records_edit">
                @if(empty($stages))
                    <li class="ui-state-default" data-id="">
                        <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
                        <input type="text" name="columns[]" value="To Do" required class="form-control column-name" />
                        <button type="button" class="close" data-type="column"><span aria-hidden="true">&times;</span></button>
                    </li>
                    <li class="ui-state-default" data-id="">
                        <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
                        <input type="text" name="columns[]" value="Urgent" required class="form-control column-name" />
                        <button type="button" class="close" data-type="column"><span aria-hidden="true">&times;</span></button>
                    </li>
                @else
                    @foreach($stages as $column)
                        <li class="ui-state-default" data-id="">
                            <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
                            <input type="text" name="columns[{{$column['id']}}]" value="{{$column['stage_label']}}" required class="form-control column-name" />
                            <button type="button" class="close" data-type="column"><span aria-hidden="true">&times;</span></button>
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit"    class="btn btn-primary">Save changes</button>
    </div>
</form>

<script>
    $(document).ready(function(){
        $('.edit_visibility_options').on('change', function(){
            var selected = $('.edit_visibility_options:checked').val();

            if(selected == 'team'){
                $('#select_team_edit').show();
                $('#select_team_edit').prop('required',true);
            }else{
                $('#select_team_edit').hide();
                $('#select_team_edit').prop('required',false);
            }
        })


        /**
         * Sortable js
         */
        $( "#sort_records<?=$board['id']?>" ).sortable({
            update: function(event, ui){
                if(ui.sender){

                    var group = ui.item.parent().data('group');
                    var id = ui.item.data('id');

                }
            }
        }).disableSelection();

        $('#sort_records<?=$board['id']?>').on('click', '.close', function(e){
            var type = $(this).data('type');

            if(type == 'column'){
                var li = $('#sort_records<?=$board['id']?>').find('li').length;

                if(li <= 2){
                    $('#columns_error').html('The minumun Column list is 2').focus();
                    setTimeout(function(){
                        $('#columns_error').html('');
                    },3000);
                }else{
                    $(this).parent().remove();
                }

                e.preventDefault();
            }
        })

        $('#add_column').on('click', function(){
            var column = '<li class="ui-state-default" data-id="">'+
                    '<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'+
                    '<input type="text" name="columns[]" value="" required class="form-control column-name" />'+
                    '<button type="button" class="close" data-type="column"><span aria-hidden="true">&times;</span></button>'+
                    '</li>';

            $('#sort_records<?=$board['id']?>').append(column);
        })

        var column_id = 0;
        $('.add_columns_in_edit').on('click', function(){
            var column = '<li class="ui-state-default" data-id="">'+
                    '<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'+
                    '<input type="text" name="columns[a'+column_id+']" value="" required class="form-control column-name" />'+
                    '<button type="button" class="close" data-type="column"><span aria-hidden="true">&times;</span></button>'+
                    '</li>';

            column_id++;
            $('#sort_records<?=$board['id']?>').append(column);
        })

    })
</script>
