
<style>
    .put-left-radius{
        -webkit-border-radius: 6px 0 0 6px !important;
        border-radius: 6px 0 0 6px !important;
    }
</style>

<div class="col-sm-2 col-xs-12" style="padding-left:0;">
    <select id="page_display" class="form-control">
        <option <?=($display=='list_view'?'selected':'');?> value="list_view" >{{trans("crm.List view")}}</option>
        <option <?=($display=='deal_per_contacts'?'selected':'');?> value="deal_per_contacts" >{{trans("crm.By Contact")}}</option>
        <option <?=($display=='deal_table_view'?'selected':'');?> value="deal_table_view" >{{trans("crm.All Deals")}}</option>
        <option <?=($display=='deal_per_organization'?'selected':'');?> value="deal_per_organization" >{{trans("crm.By Organization")}}</option>
    </select>
</div>
<div class="col-sm-6 col-xs-12">
    <div class="col-xs-4">
        <select id="board_name_selection" data-display="{{$display}}" class="form-control">
            @foreach($boards as $board)
                <option value="{{$board['id']}}" <?=($board['id']==$board_id)?'selected="selected"':''?> > {{$board['board_title']}} </option>
            @endforeach
        </select>
    </div>

    @if($display !== "list_view")
        <div class="col-xs-8">
            <form class="form-inline" action="{{url('crm/'.$board_id.'?display='.$display)}}" method="POST">
                <div class="form-group">
                    <label class="sr-only" for="exampleInputAmount">{{trans("crm.Amount (in dollars)")}}</label>
                    <div class="input-group">
                        <div class="input-group-addon put-left-radius" style="padding: 0;border: 0;">
                            {!! csrf_field() !!}
                            <select name="column_name" class="form-control put-left-radius" id="search_filter">
                                @foreach($search_options as $key => $options)
                                    <option <?=$post['column_name']==$key?'selected':''?> value="{{$key}}">{{$options}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div id="change_here">
                            <input type="text" name="column_value" value="{{$post['column_value']}}" class="form-control" placeholder="{{trans("crm.Search")}}">
                        </div>
                        <span class="input-group-btn"> <button class="btn btn-primary" id="search_btn" type="submit">{{trans("crm.Search")}}</button></span>
                    </div>
                </div>
            </form>
        </div>
    @endif
</div>
<div class="col-sm-4 pull-right">
    @if($is_admin)
        <a href="{{url('/crm_setting')}}" class="btn btn-primary pull-right margin-left-10">{{trans("crm.Customize")}}</a>
    @endif
    @if($display == "list_view")
        <a href="{{url('contact')}}" class="btn btn-primary pull-right margin-left-10"> {{trans("crm_index.Add a Contact")}}</a>
        <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#newAddDeal"  <?=(count($stages)<=0)?'disabled title="{{trans("crm.No stages have been set")}}"':'';?> >{{trans("crm_index.Add a deal")}}</button>
    @endif
</div>

<script>
    $(document).ready(function(){
        $('#page_display').on('change', function(){
            var display = $(this).val();
            window.location.href = "{{url('crm/'.$board_id).'?display='}}"+display;
        })

        $('#board_name_selection').on('change', function(){
            var id = $(this).val();
            var display = $(this).data('display');
            console.log(display);
            window.location.href = "{{url('crm')}}"+"/"+id+"?display="+display;
        })

        $('#search_filter').on('change', function(){
            var value = $(this).val();

            if(value == 'status'){
                var select = '<select name="column_value" class="form-control"><option value=""> -- </option><option value="won">{{trans("crm.Won")}}</option><option value="lost">{{trans("crm.Lost")}}</option><option value="on progress">{{trans("crm.In Progress")}}</option></select>';
                $('#change_here').html(select);
            }else{
                $('#change_here').html('<input type="text" name="column_value" value="{{!empty($post['column_value'])?$post['column_value']:''}}" class="form-control" placeholder="{{trans("crm.Search")}}">');
            }

        })

        // on load
        var search_filter_status = $('#search_filter').val();
        if(search_filter_status == 'status'){
            var select = '<select name="column_value" class="form-control"><option value=""> -- </option><option value="won">{{trans("crm.Won")}}</option><option value="lost">{{trans("crm.Lost")}}</option><option value="on progress">{{trans("crm.In Progress")}}</option></select>';
            $('#change_here').html(select);
        }else{
            $('#change_here').html('<input type="text" name="column_value" value="{{!empty($post['column_value'])?$post['column_value']:''}}" class="form-control" placeholder="{{trans("crm.Search")}}">');
        }

    })
</script>

<div class="col-md-12" style="height:10px;"></div>