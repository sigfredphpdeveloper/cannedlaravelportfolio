
<form action="{{url('/deal_edit_form').'/'.$deal['deal_id']}}" method="POST" class="ModaleventInsForm{{$deal['deal_id']}}">


        {!! csrf_field() !!}
        <input type="hidden" name="user_id" value="{{ $user['id'] }}">

        <div class="form-group">
            <label>{{trans("crm_deal_edit_form.Deal Title")}}</label>
            <input class="form-control delete_fields" type="text" name="deal_title" value="{{$deal['deal_title']}}" placeholder="" />
        </div>
        <div class="form-group">
            <label>{{trans("crm_deal_edit_form.Contact Name")}}</label>
            <input class="form-control delete_fields contact_auto_complete" autocomplete="off" type="text" value="{{$deal['contact_name']}}" placeholder="" />
            <input class="form-control delete_fields" type="hidden" name="contact_id" id="contact_id{{$deal['deal_id']}}" value="{{$deal['contact_id']}}" placeholder="" />
        </div>
        <div class="form-group">
            <label>{{trans("crm_deal_edit_form.Organization Name")}}</label>
            <input class="form-control delete_fields custom_auto_complete" autocomplete="off" type="text" value="{{$deal['organization_name']}}" placeholder="" />
            <input class="form-control delete_fields" type="hidden" name="organization_id" id="organization_id{{$deal['deal_id']}}" value="{{$deal['organization_id']}}" placeholder="" />
        </div>
        <div class="form-group">
            <label>{{trans("crm_deal_edit_form.Stage")}}</label>
            <select name="stage" class="form-control">
                @foreach($stages as $stage)
                    <option value="{{$stage['id']}}" {{($deal['stage']==$stage['id'])?'selected="selected"':''}} >{{$stage['stage_label']}}</option>
                @endforeach
            </select>
        </div>
        <?php
        /**
        <div class="form-group">
            <label>{{trans("crm_deal_edit_form.Visibility")}}</label>

            <label class="radio-inline">
                <input class="form-control visibility-change<?=$deal['deal_id']?>" type="radio" name="visibility" value="me" {{($deal['visibility']=='me')?'checked':''}} /> {{trans("crm_deal_edit_form.Me")}}
            </label>
            <label class="radio-inline">
                <input class="form-control visibility-change<?=$deal['deal_id']?>" type="radio" name="visibility" value="team" {{($deal['visibility']=='team')?'checked':''}} />{{trans("crm_deal_edit_form.Team")}}
            </label>
            <label class="radio-inline">
                <input class="form-control visibility-change<?=$deal['deal_id']?>" type="radio" name="visibility" value="everyone" {{($deal['visibility']=='everyone')?'checked':''}} />{{trans("crm_deal_edit_form.Every One")}}
            </label>
            <select name="teams[]" id="select_team<?=$deal['deal_id']?>" class="form-control" multiple style="<?=($deal['visibility']=='team')?'':'display:none;'?>" >
                @foreach($teams as $team)
                    <option value="{{$team['id']}}" <?=in_array($team['id'],$deal_teams)?'selected="selected"':''?>>{{$team['team_name']}}</option>
                @endforeach
            </select>
        </div>
        */
        ?>

        <div class="form-group">
            <label>{{trans("crm_deal_edit_form.Expected Value")}}</label>
            <input class="form-control delete_fields" type="text" name="deal_value" value="{{$deal['deal_value']}}" placeholder="" />
        </div>


        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("crm_deal_edit_form.Close")}}</button>
        <button type="submit" class="btn btn-primary">{{trans("crm_deal_edit_form.Save changes")}}</button>

</form>



 <script>
 $(document).ready(function(){


    $( ".contact_auto_complete" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "{{ url('/contact_list') }}",
                dataType: "json",

                data: {
                    q: request.term
                },
                success: function( data ) {
                console.log(data);
                   response(data);
                }
            });
        },
        select: function(e,ui){
            console.log(ui);
            $('#contact_id{{$deal['deal_id']}}').val(ui.item.id);
        }
    });

    $( ".contact_auto_complete" ).autocomplete( "option", "appendTo", ".ModaleventInsForm{{$deal['deal_id']}}" );

    $( ".custom_auto_complete" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "{{ url('/organization_list') }}",
                dataType: "json",

                data: {
                    q: request.term
                },
                success: function( data ) {
                console.log(data);
                   response(data);
                }
            });
        },
        select: function(e,ui){
            console.log(ui);
            $('#organization_id{{$deal['deal_id']}}').val(ui.item.id);
        }
    });

    $( ".custom_auto_complete" ).autocomplete( "option", "appendTo", ".ModaleventInsForm{{$deal['deal_id']}}" );

    $('.visibility-change<?=$deal['deal_id']?>').on('change', function(){
        var checked = $('.visibility-change<?=$deal['deal_id']?>:checked').val();

        if(checked == 'team'){
            $('#select_team<?=$deal['deal_id']?>').fadeIn();
        }else{
            $('#select_team<?=$deal['deal_id']?>').fadeOut();
        }

    })



 })
 </script>