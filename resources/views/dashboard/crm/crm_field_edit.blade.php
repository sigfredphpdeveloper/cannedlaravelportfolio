
<form action="{{ url('/stages_field_edit') }}/{{$stage['id']}}" method="POST" class="eventInsForm">
    <div class="modal-body">

        {!! csrf_field() !!}
        <input type="hidden" name="user_id" value="{{ $user['id'] }}">

        <div class="form-gourp">
            <label>Stage Label</label>
            <input class="form-control delete_fields" type="text" name="stage_label" value="{{$stage['stage_label']}}" placeholder="" />
        </div>
        <div class="form-gourp">
            <label>Stage Description</label>
            <textarea name="stage_desc" class="form-control delete_fields" maxlength="100" >{{$stage['stage_desc']}}</textarea>
        </div>
        <div class="form-gourp">
            <label>Stage Position</label>
            <input type="number" name="stage_position" class="form-control" min="1"  value="{{$stage['stage_position']}}" readonly>
        </div>


    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
    </div>
</form>
