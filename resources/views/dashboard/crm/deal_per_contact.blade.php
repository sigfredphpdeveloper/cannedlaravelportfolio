@extends('dashboard.layouts.master')


@section('content')
@include('dashboard.crm.crm_search', ['display' => $display])

<table class="table table-striped">
    <thead>
        <tr>
            <th>{{trans("crm.Contact Name")}}</th>
            <th>{{trans("crm.Organization Name")}}</th>
            <th>{{trans("crm.Deals Nb")}}</th>
            <th>{{trans("crm.Owner")}}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($crm_contacts as $crm_contact)
            <tr>
                <td>{{$crm_contact['contact_name']}}</td>
                <td>{{$crm_contact['organization_name']}}</td>
                <td>
                     <a href="javascript:;" class="deals_information" data-contact_id="{{$crm_contact['contact_id']}}" data-nb="{{$crm_contact['deals_nb']}}">{{$crm_contact['deals_nb']}}</a>
                </td>
                <td>{{$crm_contact['first_name']}} {{$crm_contact['last_name']}}</td>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td colspan="10">
                {{--{!! $crm_contacts->render() !!}--}}
                @if($total > $perpage)
                <nav>
                    <ul class="pagination">
                        <li <?=($page==1?'class="disabled"':'')?> >
                          <a href="{{($page!=1)?url('crm?display=deal_per_contacts').'&page=1':'javascript:;'}}" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                          </a>
                        </li>
                        @for($x=1;$x<=ceil($total/$perpage);$x++)
                            <li class="{!! ($page==$x?'active disabled':'') !!} ">
                                <a href="{{($page!=$x)?url('crm?display=deal_per_contacts').'&page='.$x:'javascript:;'}}">{{$x}}</a>
                            </li>
                        @endfor
                        <li <?=($page==ceil($total/$perpage)?'class="disabled"':'')?>>
                          <a href="{{($page!=ceil($total/$perpage))?url('crm?display=deal_per_contacts').'&page='.ceil($total/$perpage):'javascript:;'}}" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                          </a>
                        </li>
                    </ul>
                 </nav>
                 @endif
            </td>
        </tr>
    </tfoot>
</table>


<!-- Modal -->
<div class="modal fade" id="contactsDealsModal" tabindex="-1" role="dialog" aria-labelledby="contactsDealsModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="contactsDealsModalLabel">Deals</h4>
      </div>
      <div id="contactsDealsBody" class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

@endsection


@section('page-script')
<script>
$(document).ready(function(){
    $('ol.breadcrumb').append('<li>Crm</li>');

    $('.deals_information').on('click', function(){
        var contact_id = $(this).data('contact_id');
        var nb = $(this).data('nb');

        if(nb != 0 || nb != '0'){
            $.ajax({
                url : '{{url('/contacts_deals')}}',
                type : 'GET',
//                dataType: 'JSON',
                data : {id:contact_id, '_token':'{{ csrf_token() }}'},
                success:function(html){
                    $('#contactsDealsBody').html(html);
                    $('#contactsDealsModal').modal('show');
                }

            });



//            alert(org_id+' - '+org);
        }
    });

})
</script>
@endsection