@extends('dashboard.layouts.master')


@section('content')
@include('dashboard.crm.crm_search', ['display' => $display])

<table class="table table-striped">
    <thead>
        <tr>
            <th>{{trans("crm.Organization")}}</th>
            <th>{{trans("crm.Deals Nb")}}</th>
            <th>{{trans("crm.Nb of Contact")}}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($crm_orgs as $crm_org)
            <tr>
                <td>{{$crm_org['organization_name']}}</td>
                <td>
                    <a href="javascript:;" class="deals_information" data-org_id="{{$crm_org['org_id']}}" data-org="{{$crm_org['deals_nb']}}">{{$crm_org['deals_nb']}}</a>
                </td>
                <td>{{$crm_org['nb_contacts']}}</td>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td colspan="10">
                {{--{!! $crm_orgs->render() !!}--}}
                @if($total > $perpage)
                 <nav>
                    <ul class="pagination">
                        <li <?=($page==1?'class="disabled"':'')?> >
                          <a href="{{($page!=1)?url('crm?display=deal_per_organization').'&page=1':'javascript:;'}}" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                          </a>
                        </li>
                        @for($x=1;$x<=ceil($total/$perpage);$x++)
                            <li class="{!! ($page==$x?'active disabled':'') !!} ">
                                <a href="{{($page!=$x)?url('crm?display=deal_per_organization').'&page='.$x:'javascript:;'}}">{{$x}}</a>
                            </li>
                        @endfor
                        <li <?=($page==ceil($total/$perpage)?'class="disabled"':'')?>>
                          <a href="{{($page!=ceil($total/$perpage))?url('crm?display=deal_per_organization').'&page='.ceil($total/$perpage):'javascript:;'}}" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                          </a>
                        </li>
                    </ul>
                 </nav>
                 @endif
            </td>
        </tr>
    </tfoot>
</table>

<!-- Modal -->
<div class="modal fade" id="organizationDealsModal" tabindex="-1" role="dialog" aria-labelledby="organizationDealsModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="organizationDealsModalLabel">Deals</h4>
      </div>
      <div id="organizationDealsBody" class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

@endsection


@section('page-script')
<script>
$(document).ready(function(){
    $('ol.breadcrumb').append('<li>Crm</li>');

    $('.deals_information').on('click', function(){
        var org_id = $(this).data('org_id');
        var org = $(this).data('org');

        if(org != 0 || org != '0'){
            $.ajax({
                url : '{{url('/organization_deals')}}',
                type : 'GET',
//                dataType: 'JSON',
                data : {id:org_id, '_token':'{{ csrf_token() }}'},
                success:function(html){
                    $('#organizationDealsBody').html(html);
                    $('#organizationDealsModal').modal('show');
                }

            });



//            alert(org_id+' - '+org);
        }
    });



})
</script>
@endsection