@extends('dashboard.layouts.master')

@section('content')


<link rel="stylesheet" type="text/css" media="screen" href="https://code.jquery.com/ui/jquery-ui-git.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.devbridge-autocomplete/1.2.24/jquery.autocomplete.js"></script>

<style>
.margin-left-10{
    margin-left:10px;
}
.radio-inline{
    margin-left:10px;
}
.radio-inline input[type="radio"]{
    width:auto !important;
    height:auto;
}
#sort_records { list-style-type: none; margin: 0; padding: 0;  }
#sort_records li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; }
#sort_records li span { position: absolute; margin-left: -1.3em; }
article.col-sm-6{padding-bottom: 100px;}

.btn-primary,.btn-default{padding:6px 12px;}
.margin-left-20{margin-left:20px;}
#stage_name{font-weight: bold;}
</style>

     <a href="{{ url('/admin_settings') }}" class="btn btn-xs btn-default"><i class="fa fa-caret-left"></i> Back To Settings</a>


    @include('errors.success')
    @include('errors.errors')


<article class="col-sm-6 col-md-6 col-lg-6">

    <form  id="smart-form-register" class="smart-form client-form" role="form"  method="POST" action="#">
        {!! csrf_field() !!}
        <input type="hidden" name="user_id" value="{{ $user['id'] }}">

        <fieldset>
             <div class="row">

                <section class="col col-5">
                    <label class="toggle">
                    <input type="checkbox" name="file_sharing" class="crm_setting_checkbox"
                    value="1" <?php echo ($company['crm_setting']=='1')?'checked="checked"':'';?> >
                    <i data-swchon-text="ON" data-swchoff-text="OFF"></i>{{trans("crm_settings.CRM Module")}}</label>
                </section>

                @if($is_admin)
                     <a href="{{url('crm-export/csv')}}" class="btn btn-primary pull-right">
                         <span class="glyphicon glyphicon-export" aria-hidden="true"></span> Export (CSV)
                     </a>
                     <a href="{{url('crm-export/xls')}}" class="btn btn-primary pull-right" style="margin-right:10px;">
                         <span class="glyphicon glyphicon-export" aria-hidden="true"></span> Export (XLS)
                     </a>
                @endif

            </div>



        </fieldset>


    </form>

</article>



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="myModalLabel">Add a Stage</h4>
            </div>
            <form action="{{ url('/create_stages') }}" method="POST" class="eventInsForm">
                <div class="modal-body">

                    {!! csrf_field() !!}
                    <input type="hidden" name="user_id" value="{{ $user['id'] }}">

                    <div class="form-gourp">
                        <label>Stage Label</label>
                        <input class="form-control delete_fields" type="text" name="stage_label" value="" placeholder="" />
                    </div>
                    <div class="form-gourp">
                        <label>Stage Description</label>
                        <textarea name="stage_desc" class="form-control delete_fields" maxlength="100" ></textarea>
                    </div>
                    <div class="form-gourp">
                        <label>Stage Position</label>
                        <input type="number" name="stage_position" class="form-control" min="1" max="{{count($stages)+1}}" value="{{count($stages)+1}}" readonly="readonly">
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>




@endsection



@section('page-script')
<script>
$(document).ready(function(){

    $('.crm_setting_checkbox').on('change', function(){
        var myval = this.checked ? 1 : 0;

        $.ajax({
            url : '{{url('/save_crm_setting')}}',
            type : 'POST',
            data : {'status':myval, '_token':'{{ csrf_token() }}'},
            dataType:'json',
            success:function(response){
                if(response){
                    location.reload();
                }
            }
        })
    })

})
</script>
@endsection