@extends('dashboard.layouts.master')


@section('content')

<div class="col-xs-12" style="margin-bottom:20px;">
    <div class="col-sm-3 col-xs-12">
        <select id="page_display" class="form-control">
            <option value="list_view" >List view</option>
            <option value="deal_per_contacts" >Deals per contacts</option>
            <option value="deal_table_view" >Deals table view</option>
            <option value="organization_view" >Organization view</option>
        </select>
    </div>
    <div class="col-sm-6 col-xs-12">
        <form action="{{url('crm_search')}}" method="POST">
            <div class="input-group">
                {!! csrf_field() !!}
                <input type="hidden" name="user_id" value="{{ $user['id'] }}">
                <input type="text" name="search" id="search_box" class="form-control" value="" placeholder="{{trans("crm.Search for...")}}">
                <span class="input-group-btn"> <button class="btn btn-info" id="search_btn" type="submit">{{trans("crm.Search")}}</button></span>
            </div>
        </form>
    </div>
    <div class="col-sm-3 col-xs-12">

    </div>
</div>



<table class="table table-striped">
    <thead>
        <tr>
            <th>{{trans("crm.Contact Name")}}</th>
            <th>{{trans("crm.Organization Name")}}</th>
            <th>{{trans("crm.Deals Nb")}}</th>
            <th>{{trans("crm.Owner")}}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($crm_contacts as $crm_contact)
            <tr>
                <td>{{$crm_contact['contact_name']}}</td>
                <td>{{$crm_contact['organization_name']}}</td>
                <td>{{$crm_contact['deals_nb']}}</td>
                <td>{{$crm_contact['first_name']}} {{$crm_contact['last_name']}}</td>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td colspan="10">{!! $crm_contacts->render() !!}</td>
        </tr>
    </tfoot>
</table>


<table class="table table-striped">
    <thead>
        <tr>
            <th>{{trans("crm.Deals Title")}}</th>
            <th>{{trans("crm.Contact")}}</th>
            <th>{{trans("crm.Organization")}}</th>
            <th>{{trans("crm.Owner")}}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($crm_deals as $crm_deal)
            <tr>
                <td>{{$crm_deal['deal_title']}}</td>
                <td>{{$crm_deal['contact_name']}}</td>
                <td>{{$crm_deal['organization_name']}}</td>
                <td>{{$crm_deal['first_name']}} {{$crm_contact['last_name']}}</td>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td colspan="10">{!! $crm_deals->render() !!}</td>
        </tr>
    </tfoot>
</table>

<table class="table table-striped">
    <thead>
        <tr>
            <th>{{trans("crm.Organization")}}</th>
            <th>{{trans("crm.Deals Nb")}}</th>
            <th>{{trans("crm.Nb of Contact")}}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($crm_orgs as $crm_org)
            <tr>
                <td>{{$crm_org['organization_name']}}</td>
                <td>{{$crm_org['deals_nb']}}</td>
                <td>{{$crm_org['nb_of_contacts']}}</td>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td colspan="10">{!! $crm_orgs->render() !!}</td>
        </tr>
    </tfoot>
</table>
@endsection


@section('page-script')
<script>
$(document).ready(function(){
    $('ol.breadcrumb').append('<li>Crm</li>');
})
</script>
@endsection