
<table class="table table-striped">
    <thead>
        <tr>
            <th>{{trans("crm.Deals Title")}}</th>
            <th>Status</th>
            <th>{{trans("crm.Contact")}}</th>
            <th>{{trans("crm.Organization")}}</th>
            <th>{{trans("crm.Owner")}}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($deals as $deal)
            <tr>
                <td>
                    <a href="{{url('deal_view')}}/{{$deal['deal_id']}}">
                        {{$deal['deal_title']}}
                    </a>
                </td>
                <td>
                    @if($deal['status']=='WON')
                        <label style="color:limegreen">WON</label>
                    @elseif($deal['status']=='LOST')
                        <label style="color:#f00000">LOST</label>
                    @elseif(empty($deal['status']))
                        In Progress
                    @else
                        <?=($deal['status']=='ON PROGRESS')?'IN PROGRESS':$deal['status']?>
                    @endif
                </td>
                <td>{{$deal['contact_name']}}</td>
                <td>{{$deal['organization_name']}}</td>
                <td>{{$deal['first_name']}} {{$deal['last_name']}}</td>
            </tr>
        @endforeach
    </tbody>
</table>