@extends('dashboard.layouts.master')


@section('content')

<link rel="stylesheet" href="{{url('css')}}/classic.css">
<link rel="stylesheet" href="{{url('css')}}/classic.date.css">
<link rel="stylesheet" href="{{url('css')}}/classic.time.css">

<style>
.ml10{
    margin-left:10px;
}
.mt20{
    margin-top:20px;
}
.radio-inline input[type="radio"]{
    width:auto !important;
    height:auto;
}
div.btn-group-justified .btn{
    width:auto;
}
.date-textbox{
    width:150px;
}
.time-textbox{
    width:130px;
}
.duration-textbox{
    width:135px;
}
.one-line-label{
    padding: 5px 5px 5px 5px;
}
.unique-form-group{
    height: 35px;
    margin-top: 5px;
}
.flipped{
    -moz-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: FlipH;
    -ms-filter: "FlipH";
}
h2{
    margin:0 0 0 15px;
}
button.margin-10-0{
    margin:10px 0;
}
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
.container{ width:auto !important; }
.tab-pane{min-height: 100px !important;}
.btn-success{
    color: #fff !important;
    background-color: #739e73 !important;
    border-color: #659265 !important;
}
.form-group .btn-group .active{
    background: #ff8521 !important;
    color: #fff !important;
}
</style>

<div class="container">
    <div class="col-xs-12">
        <a href="{{url('crm/'.$deal['crm_board_id'].'?display=list_view')}}" class="btn btn-primary btn-sm pull-left">{{trans("crm_deal_view.Back")}}</a>
        <h2 class="pull-left">{{$deal['deal_title']}}</h2>

        @if($deal['status']=='ON PROGRESS')
            <form action="{{url('delete_deal')}}" method="POST" id="delete_deal" >
                {!! csrf_field() !!}
                <input type="hidden" name="deal_id" value="{{$deal_id}}"/>
                <button type="submit" class="btn btn-danger pull-right ml10 " >{{trans("crm_deal_view.Delete")}}</button>
            </form>
            <form action="{{url('archived_deal')}}" method="POST" >
                {!! csrf_field() !!}
                <input type="hidden" name="deal_id" value="{{$deal_id}}"/>
                <button type="submit" class="btn btn-primary pull-right ml10 " >{{trans("crm_deal_view.Archive")}}</button>
            </form>

            <button type="button" class="btn btn-danger pull-right ml10 " data-toggle="modal" data-target="#lostModal" >{{trans("crm_deal_view.Lost")}}</button>
            <button type="button" class="btn btn-success pull-right " data-toggle="modal" data-target="#wonModal" >{{trans("crm_deal_view.Won")}}</button>
        @endif

    </div>
    <div class="col-md-12" style="height:10px;"></div>

    @include('errors.success')
    @include('errors.errors')

    <div class="col-sm-3 ">
        <div class="panel panel-default">
            <div class="panel-heading"> <h3 class="panel-title">{{trans("crm_deal_view.Deal Info")}}</h3> </div>
            <div class="panel-body">
                {{trans("crm_deal_view.Stage:")}}

                @if($deal['status']=='WON')
                    <label style="color:limegreen">WON</label><br>
                @elseif($deal['stage']=='LOST' || $deal['stage']=='DELETE' || $deal['stage']=='ARCHIVED')
                    <label style="color:rgb(255,0,0)">{{$deal['stage']}}</label><br>
                @elseif(empty($stage['status']))
                    On Progress<br>
                @else
                    <?=($stage['stage_label']=='ON PROGRESS')?'IN PROGRESS':$stage['stage_label']?><br>
                @endif
                Contact: {{$deal['contact_name']}}<br>
                Org: {{$deal['organization_name']}}
            </div>
        </div>


        <div class="panel panel-default">
            <div class="panel-heading"> <h3 class="panel-title">{{trans("crm_deal_view.Contact Details")}}</h3> </div>
            <div class="panel-body">
                {{$deal['contact_name']}}   <br/>
                {{$deal['contact_email']}} <br/>
                {{$deal['contact_phone']}} <br/>
                {{$deal['position']}} <br/>
                {{$deal['address']}} <br/>
            </div>
        </div>

        <div class="panel panel-default">
            <div class="panel-heading"> <h3 class="panel-title">{{trans("crm_deal_view.Organization Details")}}</h3> </div>
            <div class="panel-body">
                {{$deal['organization_name']}}   <br/>
                {{$deal['organization_address']}} <br/>
                @if(!empty($deal['extra_fields']))
                    @foreach(json_decode($deal['extra_fields']) as $key => $extra_field)
                        @if(!empty($extra_field))
                            {{ucfirst(str_replace('_', ' ', $key))}} : {{$extra_field}} <br/>
                        @endif
                    @endforeach
                @endif
            </div>
        </div>

        <br/>
        <br/>
        <br/>

    </div>

    <div class="col-sm-9 ">
        <div class="panel panel-default">
            <div class="panel-heading"> <h3 class="panel-title">{{trans("crm_deal_view.Deal Stats")}}</h3> </div>
            <div class="panel-body">
                <table class="table table-bordered">
                    <tr>
                        <th>{{trans("crm_deal_view.Deal Age")}}</th>
                        <th>{{trans("crm_deal_view.Last Updated")}}</th>
                        <th>{{trans("crm_deal_view.Created Date")}}</th>
                    </tr>
                    <tr>
                        <td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$deal['create_date'])->diffForHumans()}}</td>
                        <td>{{($deal['last_update_date']!='0000-00-00 00:00:00')?\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$deal['last_update_date'])->diffForHumans():'Not Updated Yet!'}}</td>
                        <td>{{date('M d,Y (H:i:s)',strtotime($deal['create_date']))}}</td>
                    </tr>
                </table>
            </div>
        </div>


      <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" <?=empty($tab)||$tab=='activity'?'class="active"':''?> ><a href="#home" aria-controls="home" role="tab" data-toggle="tab">{{trans("crm_deal_view.Activity")}}</a></li>
            <li role="presentation" <?=($tab=='files')?'class="active"':''?> ><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">{{trans("crm_deal_view.Files")}}</a></li>
            <li role="presentation" <?=($tab=='notes')?'class="active"':''?> ><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">{{trans("crm_deal_view.Notes")}}</a></li>
        </ul>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane <?=empty($tab)||$tab=='activity'?'active':''?>" id="home">
                @if($deal['status']=='ON PROGRESS')
                    <button class="btn btn-primary pull-right margin-10-0" data-toggle="modal" data-target="#myModal" >{{trans("crm_deal_view.Add Activity")}}</button>
                @endif

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Activity type</th>
                            <th>Activity title</th>
                            <th>Time of the activity</th>
                            <th>Duration of the activity</th>
                            <th>Date of activity</th>
                            <th style="width:16%"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($activities as $activity)
                        <tr>
                            <td>{{$activity['activity_type']}}</td>
                            <td>{{$activity['activity_title']}}</td>
                            <td>{{$activity['time']}}</td>
                            <td>{{$activity['duration']}}</td>
                            <td>{{$activity['date']}}</td>
                            <td>
                                <a href="javascript:;" class="btn btn-primary btn-sm activity_info_btn" data-id="{{$activity['activity_id']}}"><i class="fa fa-info"></i></a>
                                @if($deal['status']=='ON PROGRESS')
                                    <button type="button" class="btn btn-primary btn-sm btn_edit_activity" data-id="{{$activity['activity_id']}}" data-deal_id="{{$deal_id}}" ><i class="fa fa-pencil-square-o"></i></button>
                                    <button type="button" class="btn btn-danger btn-sm btn_delete_me" data-id="{{$activity['activity_id']}}" data-table="activity" data-deal_id="{{$deal_id}}" ><i class="fa fa-times"></i></button>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="6">
                                @if($total_activity>0)
                                <nav>
                                    <ul class="pagination">
                                        <li <?=($activity_page==1?'class="disabled"':'')?> >
                                          <a href="{{($activity_page!=1)?url('deal_view/'.$deal_id).'?activity_page=1':'javascript:;'}}" aria-label="Previous">
                                            <span aria-hidden="true">&laquo;</span>
                                          </a>
                                        </li>
                                        @for($x=1;$x<=ceil($total_activity/$perpage);$x++)
                                            <li class="{!! ($activity_page==$x?'active disabled':'') !!} ">
                                                <a href="{{($activity_page!=$x)?url('deal_view/'.$deal_id).'?activity_page='.$x:'javascript:;'}}">{{$x}}</a>
                                            </li>
                                        @endfor
                                        <li <?=($activity_page==ceil($total_activity/$perpage)?'class="disabled"':'')?>>
                                          <a href="{{($activity_page!=ceil($total_activity/$perpage))?url('deal_view/'.$deal_id).'?activity_page='.ceil($total_activity/$perpage):'javascript:;'}}" aria-label="Next">
                                            <span aria-hidden="true">&raquo;</span>
                                          </a>
                                        </li>
                                    </ul>
                                </nav>
                                @endif
                                {{--{!! $files->render() !!}--}}
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div role="tabpanel" class="tab-pane <?=($tab=='files')?'active':''?>" id="profile">
                @if($deal['status']=='ON PROGRESS')
                    <button type="button" class="btn btn-primary pull-right margin-10-0" data-toggle="modal" data-target="#myActivityModal" >{{trans("crm_files.Add")}}</button>
                @endif
                <table class="table table-stripe">
                    <tbody>
                    @foreach($files as $file)
                        <tr>
                            <td>• {{$file['file_name']}} &nbsp;&nbsp;&nbsp;</td>
                            <td>
                                <a href="{{$file['file_path']}}{{$file['file_name']}}">{{trans("crm_files.Download")}}</a>
                                <button type="button" class="btn btn-danger btn-sm btn_delete_me pull-right" data-id="{{$file['file_id']}}" data-table="files" data-deal_id="{{$deal_id}}" ><i class="fa fa-times"></i></button>
                            </td>
                            <td></td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="3">
                                <nav>
                                @if($total_files>0)
                                    <ul class="pagination">
                                        <li <?=($file_page==1?'class="disabled"':'');?> >
                                          <a href="{{($file_page!=1)?url('deal_view/'.$deal_id).'?file_page=1':'javascript:;'}}" aria-label="Previous">
                                            <span aria-hidden="true">&laquo;</span>
                                          </a>
                                        </li>
                                        @for($x=1;$x<=ceil($total_files/$perpage);$x++)
                                            <li class="{!! ($file_page==$x?'active disabled':'') !!} ">
                                                <a href="{{($file_page!=$x)?url('deal_view/'.$deal_id).'?file_page='.$x:'javascript:;'}}">{{$x}}</a>
                                            </li>
                                        @endfor
                                        <li <?=($file_page==ceil($total_files/$perpage)?'class="disabled"':'');?> >
                                          <a href="{{($file_page!=ceil($total_files/$perpage))?url('deal_view/'.$deal_id).'?file_page='.ceil($total_notes/$perpage):'javascript:;'}}" aria-label="Next">
                                            <span aria-hidden="true">&raquo;</span>
                                          </a>
                                        </li>
                                    </ul>
                                </nav>
                                @endif
                                {{--{!! $files->render() !!}--}}
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div role="tabpanel" class="tab-pane <?=($tab=='notes')?'active':''?>" id="messages">
            @if($deal['status']=='ON PROGRESS')
                <div class="col-xs-12">
                    <button type="button" class="btn btn-primary pull-right margin-10-0" data-toggle="modal" data-target="#myNoteModal" >{{trans("crm_notes.Add")}}</button>
                </div>
            @endif
                <table class="table table-striped">
                   <tbody>
                        @foreach($notes as $note)
                            <tr>
                                <td>
                                    {{date('M d,Y H:i:s', strtotime($note['note_date']))}} - {{$note['title']}} {{$note['first_name']}} {{$note['last_name']}}
                                    <button type="button" class="btn btn-danger btn-sm btn_delete_me pull-right" data-id="{{$note['note_id']}}" data-table="notes" data-deal_id="{{$deal_id}}" ><i class="fa fa-times"></i></button>
                                    <br><br>
                                    {{$note['note_details']}}
                                </td>
                            </tr>
                        @endforeach
                   </tbody>
                   <tfoot>
                        <tr>
                            <td>
                                @if($total_notes>0)
                                <nav>
                                  <ul class="pagination">
                                    <li <?=($note_page==1?'class="disabled"':'');?> >

                                      <a href="{{($note_page!=1)?url('deal_view/'.$deal_id).'?note_page=1':'javascript:;'}}" aria-label="Previous">
                                        <span aria-hidden="true">&laquo;</span>
                                      </a>
                                    </li>
                                    @for($x=1;$x<=ceil($total_notes/$perpage);$x++)
                                        <li class="{!! ($note_page==$x?'active disabled':'') !!} ">
                                            <a href="{{($note_page!=$x)?url('deal_view/'.$deal_id).'?note_page='.$x:'javascript:;'}}">{{$x}}</a>
                                        </li>
                                    @endfor
                                    <li <?=($note_page==ceil($total_notes/$perpage)?'class="disabled"':'');?> >
                                      <a href="{{($note_page!=ceil($total_notes/$perpage))?url('deal_view/'.$deal_id).'?note_page='.ceil($total_notes/$perpage):'javascript:;'}}" aria-label="Next">
                                        <span aria-hidden="true">&raquo;</span>
                                      </a>
                                    </li>
                                  </ul>
                                </nav>
                                @endif
                                {{--{!! $notes->render() !!}--}}
                            </td>
                        </tr>
                   </tfoot>
                </table>
            </div>
        </div>
    </div>

</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="myModalLabel">{{trans("crm_deal_view.Add an Activity")}}</h4>
            </div>
            <form action="{{url('create_activity')}}" method="POST" class="activityForm">
                <div class="modal-body">

                    <div class="alert alert-danger" role="alert" style="display:none"> </div>

                    {!! csrf_field() !!}
                    <input type="hidden" name="user_id" value="{{ $user['id'] }}">
                    <input type="hidden" name="deal_id" value="{{ $deal_id }}">


                    <div class="form-group">
                        <label>{{trans("crm_deal_view.Activity Title")}}</label>
                        <input class="form-control delete_fields" type="text" name="activity_title" value="" placeholder="" required />
                    </div>

                    <div class="form-group">
                        <label>{{trans("Activity Type")}}</label>
                        <div class="btn-group" role="group" data-toggle="buttons" style="width:100%;">
                            <label class="btn btn-default active activity-radio-button">
                                {{trans("crm_deal_view.Call")}} <input type="radio" name="activity_type" value="call" class="" checked />
                            </label>
                            <label class="btn btn-default">
                                 {{trans("crm_deal_view.Meeting")}} <input type="radio" name="activity_type" value="meeting" >
                            </label>
                            <label class="btn btn-default activity-radio-button">
                                {{trans("crm_deal_view.Email")}} <input type="radio" name="activity_type" value="email" >
                            </label>
                            <label class="btn btn-default activity-radio-button">
                               {{trans("crm_deal_view.Deadline")}} <input type="radio" name="activity_type" value="deadline" >
                            </label>
                            <label class="btn btn-default activity-radio-button">
                                {{trans("crm_deal_view.Trash")}} <input type="radio" name="activity_type" value="trash" >
                            </label>
                            <label class="btn btn-default activity-radio-button">
                                {{trans("crm_deal_view.Other")}} <input type="radio" name="activity_type" value="other" >
                            </label>
                        </div>
                    </div>

                     <div class="form-group unique-form-group">
                        <label class="pull-left one-line-label">{{trans("crm_deal_view.Date")}}</label>
                        <input type="text" name="date" id="activity_date" class="dateapicker form-control delete_fields pull-left date-textbox" required placeholder="dd mm, yyyy"/>
                        <label class="pull-left one-line-label">{{trans("crm_deal_view.Time")}}</label>
                        <input type="text" name="time" id="activity_time" class="timepicker form-control delete_fields pull-left time-textbox" required placeholder="HH:mm" />
                        <label class="pull-left one-line-label">{{trans("crm_deal_view.Duration")}}</label>
                        {{--<input id="activity_duration" class="form-control delete_fields pull-left duration-textbox" type="text" name="duration" value="" placeholder="Hrs:mins" required />--}}
                        <select name="duration" class="form-control delete_fields pull-left duration-textbox">
                            <option value="15min">15min</option>
                            <option value="30min">30min</option>
                            <option value="45min">45min</option>
                            <option value="1h">1h</option>
                            <option value="1h30m">1h 30min</option>
                            <option value="2h">2h</option>
                            <option value="2h30m">2h 30min</option>
                            <option value="3h">3h</option>
                            <option value="3h30m">3h 30min</option>
                            <option value="4h">4h</option>
                            <option value="4h30m">4h 30min</option>
                            <option value="5h30m">5h</option>
                            <option value="5h30m">5h 30min</option>
                            <option value="6h">6h</option>
                            <option value="6h30m">6h 30min</option>
                            <option value="7h">7h</option>
                            <option value="7h30m">7h 30min</option>
                            <option value="8h">8h</option>
                            <option value="8h30m">8h 30min</option>
                        </select>
                     </div>

                    <div class="form-group">
                        <label>{{trans("crm_deal_view.Owner")}}</label>
                        <input class="form-control delete_fields user_auto_complete" type="text" name="owner" value="{{$user['title']}}. {{$user['first_name']}} {{$user['last_name']}}" placeholder="" required />
                         <input class="form-control delete_fields " type="hidden" name="owner_id" value="{{$user['id']}}" placeholder="" />
                    </div>
                    <div class="form-group">
                        <label>{{trans("crm_deal_view.Assigned")}}</label>
                        <input class="form-control delete_fields user_auto_complete" type="text" name="assigned" value="{{$user['title']}}. {{$user['first_name']}} {{$user['last_name']}}" placeholder="" required />
                        <input class="form-control delete_fields " type="hidden" name="assigned_user_id" value="{{$user['id']}}" placeholder="" />
                    </div>
                    <div class="form-group">
                        <label class="radio-inline">
                            {{trans("crm_deal_view.Done")}}
                            <input type="checkbox" name="done" value="1" class="" />
                        </label>

                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("crm_deal_view.Close")}}</button>
                    <button type="submit" class="btn btn-primary">{{trans("crm_deal_view.Save changes")}}</button>
                </div>
            </form>
        </div>
    </div>
</div>




<!-- Modal -->
<div class="modal fade bs-example-modal-sm" id="wonModal" tabindex="-1" role="dialog" aria-labelledby="wonModalLabel">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="wonModalLabel">{{trans("crm_deal_view.Deal Won")}}</h4>
            </div>

             <form action="{{url('win_lose_update')}}" method="POST" class="eventInsForm">
                <div class="modal-body">

                    {!! csrf_field() !!}
                    <input type="hidden" name="user_id" value="{{ $user['id'] }}">
                    <input type="hidden" name="deal_id" value="{{ $deal_id }}">

                    <div class="form-group">
                        <label>{{trans("crm_deal_view.On date")}}</label>
                        <input type="text" name="won_date" value="" class="dateapicker form-control delete_fields" placeholder="Select date" required />
                        <label class="popup_error" style="color:#f00;display:none;">Please select date</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("crm_deal_view.Close")}}</button>
                    <button type="submit" class="btn btn-primary">{{trans("crm_deal_view.Save changes")}}</button>
                </div>
            </form>

        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade bs-example-modal-sm" id="lostModal" tabindex="-1" role="dialog" aria-labelledby="lostModalLabel">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="lostModalLabel">{{trans("crm_deal_view.Deal Lost")}}</h4>
            </div>

             <form action="{{url('win_lose_update')}}" method="POST" class="eventInsForm">
                <div class="modal-body">

                    {!! csrf_field() !!}
                    <input type="hidden" name="user_id" value="{{ $user['id'] }}">
                    <input type="hidden" name="deal_id" value="{{ $deal_id }}">
                    <div class="form-group">
                        <label>{{trans("crm_deal_view.On date")}}</label>
                        <input type="text" name="lost_date" value="" class="dateapicker form-control delete_fields" required placeholder="Select date" />
                        <label class="popup_error" style="color:#f00;display:none;">Please select date</label>
                    </div>
                    <div class="form-group">
                        <label>{{trans("crm_deal_view.Reason")}}</label>
                        <textarea name="lost_reason" class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("crm_deal_view.Close")}}</button>
                    <button type="submit" class="btn btn-primary">{{trans("crm_deal_view.Save changes")}}</button>
                </div>
            </form>

        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="EditActivityModal" tabindex="-1" role="dialog" aria-labelledby="EditActivityModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="myModalLabel">{{trans("crm_deal_view.Add an Activity")}}</h4>
            </div>
            <div id="edit_activity_content">

            </div>
        </div>
    </div>
</div>




<!-- Modal -->
<div class="modal fade" id="ActivityInfo" tabindex="-1" role="dialog" aria-labelledby="activityInfoLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="activityInfoLabel">Activity Details   </h4>
            </div>
            <div id="activity_info_modal" class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>




{{--ACTIVITY--}}

<!-- Modal -->
<div class="modal fade" id="myActivityModal" tabindex="-1" role="dialog" aria-labelledby="myActivityModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="myActivityModalLabel">{{trans("crm_files.Add a File")}}</h4>
            </div>
            <form action="{{url('add_file')}}" method="POST" enctype="multipart/form-data">
                <div class="modal-body">

                    {!! csrf_field() !!}
                    <input type="hidden" name="user_id" value="{{ $user['id'] }}">
                    <input type="hidden" name="deal_id" value="{{ $deal_id }}">

                    <div class="form-gourp">
                        <label>{{trans("crm_files.File Name")}}</label>
                        <input type="text" name="file_name" value="" class="form-control" />
                    </div>
                    <div class="form-gourp">
                        <br/>
                        <label>{{trans("crm_files.File")}}</label>
                         <span class="btn btn-info btn-file" id="Upload" data-toggle="tooltip" data-placement="top" >
                            <input type="file" name="upload" id="upload_excel" multiple >{{trans("crm_files.Upload")}}
                        </span>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("crm_files.Close")}}</button>
                    <button type="submit" class="btn btn-primary">{{trans("crm_files.Save changes")}}</button>
                </div>
            </form>
        </div>
    </div>
</div>


{{--Note--}}
<div class="modal fade" id="myNoteModal" tabindex="-1" role="dialog" aria-labelledby="myNoteModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="myNoteModalLabel">{{trans("crm_notes.Add a note")}}</h4>
            </div>
            <form action="{{url('add_note')}}" method="POST" class="eventInsForm">
                <div class="modal-body">

                    {!! csrf_field() !!}
                    <input type="hidden" name="user_id" value="{{ $user['id'] }}">
                    <input type="hidden" name="deal_id" value="{{ $deal_id }}">

                    <div class="form-gourp">
                        <label>{{trans("crm_notes.Note")}}</label>
                        <textarea name="note_details" class="form-control"></textarea>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("crm_notes.Close")}}</button>
                    <button type="submit" class="btn btn-primary">{{trans("crm_notes.Save changes")}}</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade alertModalLabel" tabindex="-1" role="dialog" aria-labelledby="alertModalLabel">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="alertModalLabel">Delete</h4>
            </div>
            <form action="{{url('delete_sub_info')}}" method="POST" class="eventInsForm">
                <div class="modal-body">
                    <p>Are you sure that you want to delete this line?</p>
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <input type="hidden" name="id" id="delete_id" value="">
                    <input type="hidden" name="deal_id" id="delete_deal_id" value="">
                    <input type="hidden" name="table" id="delete_table" value="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade deleteModalLabel" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="deleteModalLabel">Delete Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Please confirm you want to delete the deal?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" id="confirm_delete_deal" class="btn btn-danger">Confirm</button>
            </div>
        </div>
    </div>
</div>

@endsection



@section('page-script')
<script src="{{url('js')}}/picker.js"></script>
<script src="{{url('js')}}/picker.date.js"></script>
<script src="{{url('js')}}/picker.time.js"></script>

<script>
$(document).ready(function(){

    $('button.radio_button').on('click', function(){
        var me = $(this);
        console.log(me.find('input[type="radio"]').val());
        me.find('input[type="radio"]').trigger('click');
    })

    $('.btn_edit_activity').on('click', function(){
        var id = $(this).data('id');
        var deal_id = $(this).data('deal_id');

        $.ajax({
            url : '{{url('/activity_edit_form')}}/'+id,
            type : 'GET',
            data : {'deal_id':deal_id, 'id':id},
            success:function(html){
                $('#edit_activity_content').html(html);
                $('#EditActivityModal').modal('show');
            }
        });

    })



    $( ".user_auto_complete" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "{{ url('/company_user_list') }}",
                dataType: "json",

                data: {
                    q: request.term,
                    '_token':'{{ csrf_token() }}'
                },
                type: 'POST',
                success: function( data ) {
                console.log(data);
                   response(data);
                }
            });
        },
        select: function(e,ui){
            $(this).next().attr('value',ui.item.user_id);

            console.log($(this).next().val());
        }
    });

    $( ".user_auto_complete" ).autocomplete( "option", "appendTo", ".activityForm" );

    $('.activity_info_btn').on('click', function(){
        var id = $(this).data('id');

        $.ajax({
            url : '{{url('/activity_details')}}/'+id,
            type : 'GET',
            data : {'id':id},
            success:function(html){
                $('#activity_info_modal').html(html);
                $('#ActivityInfo').modal('show');
            }
        });

    })


    $('ol.breadcrumb').append('<li>Deals</li><li>View</li>');

//     $.mask.definitions['H'] = "[0-1]";
//    $.mask.definitions['h'] = "[0-9]";
//    $.mask.definitions['M'] = "[0-5]";
//    $.mask.definitions['m'] = "[0-9]";
//    $.mask.definitions['P'] = "[AaPp]";
//    $.mask.definitions['p'] = "[Mm]";
//
//    $(".timepicker").mask("Hh:Mm");

    $( '.dateapicker' ).pickadate({
        formatSubmit: 'yyyy-mm-dd',
         min: [<?=date('Y')?>, 01, 01],
        //container: '#container',
        closeOnSelect: true,
        closeOnClear: true
    })

    $('.timepicker').pickatime()

    $('.eventInsForm').submit(function(event){

        if($(this).find('.dateapicker').val() == ''){
            $('.popup_error').show();
            event.preventDefault();
        }

    })

    $('.btn_delete_me').on('click', function(){
        var id = $(this).data('id');
        var table = $(this).data('table');
        var deal_id = $(this).data('deal_id');

        $('#delete_id').val(id);
        $('#delete_table').val(table);
        $('#delete_deal_id').val(deal_id);

        $('.alertModalLabel').modal('show');

        {{--$.ajax({--}}
            {{--url : '{{url('/activity_delete')}}',--}}
            {{--type : 'POST',--}}
            {{--dataType: 'JSON',--}}
            {{--data : {'deal_id':deal_id, 'id':id},--}}
            {{--success:function(response){--}}
                {{--if(response==true||response=='true'){--}}
                    {{--location.reload();--}}
                {{--}--}}
            {{--}--}}
        {{--});--}}
    })


//    $('.activity-radio-button').on('click', function(){
//        var me = $(this);
//
//        $('.activity-radio-button').each(function(){
//            $(this).removeClass('btn-primary').addClass('btn-default');
//        })
//
//        me.removeClass('btn-default').addClass('btn-primary');
//    })


    var confirm_delete = false;
    $("form#delete_deal").submit(function (e) {
        if(!confirm_delete){
            e.preventDefault(); // this will prevent from submitting the form.
            $('.deleteModalLabel').modal('show');
        }
    });

    $('#confirm_delete_deal').on('click',function(){
        confirm_delete = true;
        $("form#delete_deal").submit();
    })

})
</script>

@endsection
