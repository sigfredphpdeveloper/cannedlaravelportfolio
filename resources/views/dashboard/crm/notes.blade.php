@extends('dashboard.layouts.master')


@section('content')
<style>
h2{
    margin:0 0 0 15px;
}
</style>
<a href="{{url('deal_view').'/'.$deal_id}}" class="btn btn-primary btn-sm pull-left">{{trans("crm_notes.Back")}}</a>
<h2 class="pull-left">Notes</h2>

<button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#myModal" >{{trans("crm_notes.Add")}}</button>
<br>
<br>

<table class="table table-striped">
   <tbody>
        @foreach($notes as $note)
            <tr>
                <td>
                    {{date('M d,Y H:i:s', strtotime($note['note_date']))}} - {{$note['title']}} {{$note['first_name']}} {{$note['last_name']}}<br><br>
                    {{$note['note_details']}}
                </td>
            </tr>
        @endforeach
   </tbody>
   <tfoot>
        <tr>
            <td>
                {!! $notes->render() !!}
            </td>
        </tr>
   </tfoot>
</table>



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="myModalLabel">{{trans("crm_notes.Add a note")}}</h4>
            </div>
            <form action="{{url('add_note')}}" method="POST" class="eventInsForm">
                <div class="modal-body">

                    {!! csrf_field() !!}
                    <input type="hidden" name="user_id" value="{{ $user['id'] }}">
                    <input type="hidden" name="deal_id" value="{{ $deal_id }}">

                    <div class="form-gourp">
                        <label>{{trans("crm_notes.Note")}}</label>
                        <textarea name="note_details" class="form-control"></textarea>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("crm_notes.Close")}}</button>
                    <button type="submit" class="btn btn-primary">{{trans("crm_notes.Save changes")}}</button>
                </div>
            </form>
        </div>
    </div>
</div>



@endsection

@section('page-script')
<script>
$(document).ready(function(){
})
</script>
@endsection