@extends('dashboard.layouts.master')


@section('content')
@include('dashboard.crm.crm_search', ['display' => $display])

<table class="table table-striped">
    <thead>
        <tr>
            <th>{{trans("crm.Deals Title")}}</th>
            <th>{{trans("crm.Status")}}</th>
            <th>{{trans("crm.Stage")}}</th>
            <th>{{trans("crm.Contact")}}</th>
            <th>{{trans("crm.Organization")}}</th>
            <th>{{trans("crm.Owner")}}</th>
        </tr>
    </thead>
    <tbody>
        @foreach($crm_deals as $crm_deal)
            <tr data-board_id="{{$crm_deal['crm_board_id']}}">
                <td>
                    <a href="{{url('deal_view')}}/{{$crm_deal['deal_id']}}">
                        {{$crm_deal['deal_title']}}
                    </a>
                </td>
                <td>
                    @if($crm_deal['status']=='WON')
                        <label style="color:limegreen">{{trans("crm.WON")}}</label>
                    @elseif($crm_deal['status']=='LOST' || $crm_deal['status']=='ARCHIVED' || $crm_deal['status']=='DELETE')
                        <label style="color:#f00000">{{$crm_deal['status']}}</label>
                    @else
                        {{trans("crm.In Progress")}}
                    @endif
                </td>
                <td>
{{--                    @if($crm_deal['status']=='ON PROGRESS')--}}
                        {{$crm_deal['stage_label']}}
                    {{--@else--}}
                        {{------}}
                    {{--@endif--}}
                </td>
                <td>{{$crm_deal['contact_name']}}</td>
                <td>{{$crm_deal['organization_name']}}</td>
                <td>{{$crm_deal['first_name']}} {{$crm_deal['last_name']}}</td>
            </tr>
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td colspan="10">
                {{--{!! $crm_deals->render() !!}--}}
                @if($total > $perpage)
                <nav>
                    <ul class="pagination">
                        <li <?=($page==1?'class="disabled"':'')?> >
                          <a href="{{($page!=1)?url('crm?display=deal_table_view').'&page=1':'javascript:;'}}" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                          </a>
                        </li>
                        @for($x=1;$x<=ceil($total/$perpage);$x++)
                            <li class="{!! ($page==$x?'active disabled':'') !!} ">
                                <a href="{{($page!=$x)?url('crm/'.$board_id.'?display=deal_table_view').'&page='.$x:'javascript:;'}}">{{$x}}</a>
                            </li>
                        @endfor
                        <li <?=($page==ceil($total/$perpage)?'class="disabled"':'')?>>
                          <a href="{{($page!=ceil($total/$perpage))?url('crm/'.$board_id.'?display=deal_table_view').'&page='.ceil($total/$perpage):'javascript:;'}}" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                          </a>
                        </li>
                    </ul>
                 </nav>
                 @endif
            </td>
        </tr>
    </tfoot>
</table>

@endsection


@section('page-script')
<script>
$(document).ready(function(){
    $('ol.breadcrumb').append('<li>Crm</li>');
})
</script>
@endsection