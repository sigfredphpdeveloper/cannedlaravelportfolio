
    <div class="form-group">
        <label class="bold_text">Deal:</label>
        <label >{{$activity['deal_title']}}</label>
    </div>
    <div class="form-group">
        <label class="bold_text">Activity Title:</label>
        <label >{{$activity['activity_title']}}</label>
    </div>
    <div class="form-group">
        <label class="bold_text">Activity Type:</label>
        <label >{{$activity['activity_type']}}</label>
    </div>
    <div class="form-group">
        <label class="bold_text">Date:</label>
        <label >{{$activity['date']}}</label>
    </div>
    <div class="form-group">
        <label class="bold_text">Time:</label>
        <label >{{$activity['time']}}</label>
    </div>
    <div class="form-group">
        <label class="bold_text">Duration:</label>
        <label >{{$activity['duration']}}</label>
    </div>
    <div class="form-group">
        <label class="bold_text">Owner:</label>
        <label >{{$activity['owner_name']}}</label>
    </div>
    <div class="form-group">
        <label class="bold_text">Assigned To:</label>
        <label >{{$activity['assigned_name']}}</label>
    </div>
