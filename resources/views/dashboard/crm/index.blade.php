@extends('dashboard.layouts.master')



@section('content')
    <link rel="stylesheet" type="text/css" media="screen" href="https://code.jquery.com/ui/jquery-ui-git.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.devbridge-autocomplete/1.2.24/jquery.autocomplete.js"></script>

    <link rel="stylesheet" href="{{url('css')}}/classic.css">
    <link rel="stylesheet" href="{{url('css')}}/classic.date.css">
    <script src="{{url('js')}}/picker.js"></script>
    <script src="{{url('js')}}/picker.date.js"></script>


    <style>
        .margin-left-10{
            margin-left:10px;
        }
        .radio-inline{
            margin-left:10px;
        }
        .radio-inline input[type="radio"]{
            width:auto !important;
            height:auto;
        }
        .div-bordered{
            border:1px solid #bbb;
        }


        .sort_records {
            border: 1px solid #eee;
            min-height: 400px;
            list-style-type: none;
            margin: 0;
            padding: 0 5px 20px 5px;
            float: left;
            background:#e2e4e6;
            width:270px;
            margin-top: 36px;
            position: inherit;
        }
        .sort_records li{
            padding: 5px;
            width: 100%;
            margin-bottom:5px;
            min-height:80px;
    }

        .empty {
            border: 1px solid #eee;
            min-height: 100px;
            list-style-type: none;
            margin: 0;
            padding: 0 0 20px 10px;
            float: left;
            background:#e2e4e6;
            width:270px;
        }
        .empty li{
            padding: 5px;
            width: 95%;
            min-height:80px;
        }
        .link{
            color:#57889c !important;
        }
        .left-section{
            width:30px;
        }
        .left-section button, .left-section a{
            margin-bottom:3px !important;
            width:31px;
            color:#fff;
        }
        .container{
            width:100%;
        }

        .scrollable_container{
            width: 100%;
            height:auto;
            border: 1px solid #bed5cd;
            overflow-x: scroll;
            overflow-y: hidden;
            position: relative;
        }
        .color-orange{ color:#FD9940; }
        .link{color:#868686 !important;}

        .btn-success{
            color:#fff !important;
            background-color:#739e73 !important;
            border-color:#659265 !important;
        }
        h2{margin:10px 0;}
        table.table thead tr th{ text-align: center !important;}

        /** new add deal css **/
        #organization_success{color:forestgreen;}
        #organization_error{color:#f00;}
        #organization_error a{font-weight:bold;}
        .ui-autocomplete {
            max-height:200px;
            max-width: 100%;
            overflow-x:scroll;
            overflow-y:scroll;
        }
        .general_error{color:#f00}

        .stages_scroll{width:285px;overflow-x:hidden !important;overflow-y:auto;margin:0 10px;}

        .side-modal-button{margin:2px 0;}
        #custom_table tr td{ width:110px;padding:8px; }
    </style>


    <div class="container">
        {{--<h2>Deals</h2>--}}

        @include('dashboard.crm.crm_search', ['display' => $display])

        @include('errors.success')
        @include('errors.errors')


        <?php
        // define array keys
        $array['empty']['html'] = '';
        $array['empty']['header'] = '';
        $stages_id[] = 'empty';
        ?>
        @foreach($deals as $deal)
            <?php
            if(count($stages)>=1 && in_array($deal['stage'], $stages_id)){
                $array[$deal['stage']]['html'] .= '<li data-id="'.$deal['deal_id'].'" class="ui-state-default">
                                 <div class="pull-right left-section">
                                    <!--<a href="'.url('deal_view').'/'.$deal['deal_id'].'" class="btn btn-default btn-sm"><i class="fa fa-info color-orange"></i></a>-->
                                    <button type="button" class="btn btn-default btn-sm show_menu_buttons" data-id="'.$deal['deal_id'].'" data-title="'.$deal['deal_title'].'"  ><i class="fa fa-pencil color-orange"></i></button>
                                 </div>
                                  <a href="'.url('deal_view').'/'.$deal['deal_id'].'" class="link" >'.$deal['deal_title'].'</a>
                                 <br/>
                                  '.$deal['organization_name'].' <br/>'.
                        '</li>';

            }
            ?>
        @endforeach

        @if(count($stages)>=1)
            <div class="scrollable_container" >

                <table class="table table-stripe" style="min-width:400px;">
                    <tbody>
                    <tr>
                        @foreach($array as $key => $content)
                            @if($key != 'empty')
                                <td style="padding:8px 0 !important">
                                        <div style="width: 270px;margin:0 10px;position: absolute;z-index: 2;text-align:center;font-weight:bold;background: #F7DCB4 !important;border-bottom: 2px solid #E67D44;padding:8px 0;">
                                            {{$content['header']}}
                                        </div>
                                        <div class="stages_scroll">
                                            <ul id="sortable1" data-group="{{$key}}" class="sort_records connectedSortable col-xs-12">
                                                {!! $content['html'] !!}
                                            </ul>
                                        </div>
                                </td>
                            @endif
                        @endforeach
                        @if(count($array)<5)
                            @for($x=count($array);$x<=5;$x++)
                                <td width="270">&nbsp;</td>
                            @endfor
                        @endif
                    </tr>
                    </tbody>
                </table>

            </div>
        @else
            <div>
                {{trans("crm_index.No stages had been set yet.")}}
            </div>
        @endif
    </div>

    <!-- Modal -->
    <div class="modal  fade" id="editDeal" tabindex="-1" role="dialog" aria-labelledby="editDealLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="editDealLabel">{{trans("crm_index.Edit a Deal")}}</h4>
                </div>
                <div class="modal-body">
                    <div id="deal_model_content">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal  fade" id="editMenuDeal" tabindex="-1" role="dialog" aria-labelledby="editMenuDealLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="editMenuDealLabel"></h4>
                </div>
                <div class="modal-body">
                    <table id="custom_table">
                        <tr>
                            <td>
                                <button id="crm_menu_edi" class="btn btn-default edit_deal" data-id=""><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</button>
                            </td>
                            <td>
                                <a id="crm_menu_manage" href="{{url('deal_view')}}" class="btn btn-default side-modal-button"><span class="glyphicon glyphicon-share" aria-hidden="true"></span> Manage</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button id="crm_menu_won" data-id="" class="btn btn-default side-modal-button"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Won</button>
                            </td>
                            <td>
                                <button id="crm_menu_lost" data-id="" data-target="#lostModal" class="btn btn-default side-modal-button"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> Lost</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <form action="{{url('archived_deal')}}" method="POST">
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="deal_id" id="archive_deal_id" value="">
                                    <button type="submit" id="crm_menu_archive" class="btn btn-default side-modal-button"><span class="glyphicon glyphicon-time" aria-hidden="true"></span> Archive</button>
                                </form>
                            </td>
                            <td>
                                <form action="{{url('delete_deal')}}" method="POST" id="delete_deal" >
                                    {!! csrf_field() !!}
                                    <input type="hidden" name="deal_id" id="delete_deal_id" value=""/>
                                    <button type="submit" id="crm_menu_delete" data-id="" class="btn btn-default side-modal-button"><span class="glyphicon glyphicon-alert" aria-hidden="true"></span> Delete</button>
                                </form>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade bs-example-modal-sm" id="wonModal" tabindex="-1" role="dialog" aria-labelledby="wonModalLabel">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="wonModalLabel">{{trans("crm_deal_view.Deal Won")}}</h4>
                </div>

                <form action="{{url('win_lose_update')}}" method="POST" class="eventInsForm">
                    <div class="modal-body">

                        {!! csrf_field() !!}
                        <input type="hidden" name="user_id" value="{{ $user['id'] }}">
                        <input type="hidden" name="deal_id" id="set_won_dea_id" value="">

                        <div class="form-group">
                            <label>{{trans("crm_deal_view.On date")}}</label>
                            <input type="text" name="won_date" value="" class="dateapicker form-control delete_fields" placeholder="Select date" required />
                            <label class="popup_error" style="color:#f00;display:none;">Please select date</label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("crm_deal_view.Close")}}</button>
                        <button type="submit" class="btn btn-primary">{{trans("crm_deal_view.Save changes")}}</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade bs-example-modal-sm" id="lostModal" tabindex="-1" role="dialog" aria-labelledby="lostModalLabel">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="lostModalLabel">{{trans("crm_deal_view.Deal Lost")}}</h4>
                </div>

                <form action="{{url('win_lose_update')}}" method="POST" class="eventInsForm">
                    <div class="modal-body">

                        {!! csrf_field() !!}
                        <input type="hidden" name="user_id" value="{{ $user['id'] }}">
                        <input type="hidden" name="deal_id" id="set_lost_dea_id" value="" >
                        <div class="form-group">
                            <label>{{trans("crm_deal_view.On date")}}</label>
                            <input type="text" name="lost_date" value="" class="dateapicker form-control delete_fields" required placeholder="Select date" />
                            <label class="popup_error" style="color:#f00;display:none;">Please select date</label>
                        </div>
                        <div class="form-group">
                            <label>{{trans("crm_deal_view.Reason")}}</label>
                            <textarea name="lost_reason" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("crm_deal_view.Close")}}</button>
                        <button type="submit" class="btn btn-primary">{{trans("crm_deal_view.Save changes")}}</button>
                    </div>
                </form>

            </div>
        </div>
    </div>






    <!-- Modal -->
    <div class="modal fade" id="newAddDeal" tabindex="-1" role="dialog" aria-labelledby="newAddDealLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="newAddDealLabel">{{trans("crm_index.Add a Deal")}}</h4>
                </div>
                <form action="{{ url('/create_deal') }}" method="POST" class="new_deal_from">
                    <div class="modal-body">
                        {!! csrf_field() !!}
                        <input type="hidden" name="user_id" value="{{ $user['id'] }}">
                        <input type="hidden" name="crm_board_id" value="{{ $board_id }}">

                        <div class="form-group">
                            <label>Organization Name</label>
                            <input class="form-control get_organization" id="organization_field" type="text" name="organization" value="" placeholder="" />
                            <input id="create_organization_id" type="hidden" name="organization_id" value="" />
                            <p id="organization_error" style="display:none;">This Organization hasn't been created yet, please <a href="javascript:;" id="create_organization">click here</a> to add it</p>
                            <p id="organization_success" style="display:none;"><span class="glyphicon glyphicon-check" aria-hidden="true"></span>Organization successfully created!.</p>
                        </div>
                        <div class="form-group">
                            <label>Contact Name</label>
                            <input class="form-control get_contact" id="contact_field" type="text" name="contact" value="" placeholder="" />
                            <input id="create_contact_id" type="hidden" name="contact_id" value="" />
                            <p id="contact_error" style="color:f00;display:none;">This contact doesn't exist, please <a href="javascript:;" id="create_contact">click here</a> to add it.</p>
                            <p id="contact_error2" style="color:f00;display:none;">Please Select organisation first before contact!</p>
                            <p id="contact_success" style="color:#3c763d;display:none;"><span class="glyphicon glyphicon-check" aria-hidden="true"></span>Contact successfully created!.</p>
                        </div>

                        <div class="form-group">
                            <label>{{trans("crm_index.Deal Title")}}</label>
                            <input class="form-control delete_fields" type="text" name="deal_title" id="create_deal_title" value="" placeholder="" />
                        </div>
                        <div class="form-group">
                            <label>{{trans("crm_index.Stage")}}</label>
                            <select name="stage" class="form-control">
                                @foreach($array as $key => $content)
                                    @if($key !== 'empty')
                                     <option value="{{$key}}">{{$content['header']}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>{{trans("crm_index.Expected Value")}}</label>
                            <input class="form-control delete_fields" type="text" name="deal_value" id="create_deal_value" value="" placeholder="" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("crm_index.Close")}}</button>
                        <button type="button" class="btn btn-primary" id="create_btn" >{{trans("crm_index.Save changes")}}</button>
                    </div>

                </form>
            </div>
        </div>
    </div>


    <div class="modal fade" id="sample_modal" tabindex="-1" role="dialog" aria-labelledby="sample_modalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="sample_modalLabel">{{trans("crm_index.Add a new Contact")}}</h4>
                </div>
                <div class="modal-body">
                    <form id="create_contact_form">
                        <div class="row">
                            <div class="alert alert-danger error_message_in_contact" style="display:none;"></div>
                            {!! csrf_field() !!}
                            <input class="form-control" type="hidden" name="contact_organization_id" id="contact_organization_id" value="" placeholder="" required />

                            <div class="form-group">
                                <label>{{trans("contact.Contact Name")}}</label>
                                <input class="form-control" type="text" name="contact_name" value="" placeholder="ex.John" required />
                            </div>

                            <div class="form-group">
                                <label>{{trans("contact.Contact Email")}}</label>
                                <input class="form-control" type="text" name="contact_email" value="" placeholder="sample@example.com" required />
                            </div>

                            <div class="form-group">
                                <label>{{trans("contact.Contact Phone")}}</label>
                                <input class="form-control " type="text" name="contact_phone" value="" placeholder="##-####-####" />
                            </div>

                            <div class="form-group">
                                <label>{{trans("contact.Position")}}</label>
                                <input class="form-control " type="text" name="position" value="" placeholder="" />
                            </div>

                            <div class="form-group">
                                <label>{{trans("contact.Address")}}</label>
                                <input class="form-control " type="text" name="address" value="" placeholder="" />
                            </div>

                            <div class="form-group">
                                <label>{{trans("contact.Notes")}}</label>
                                <textarea class="form-control " name="contact_notes" id=""></textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("crm_index.Close")}}</button>
                    <button type="button" class="btn btn-primary" id="submit_contact">{{trans("crm_index.Save changes")}}</button>
                </div>
            </div>
        </div>
    </div>

    <!-- delete confirmation -->
    <div class="modal fade deleteModalLabel" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><span aria-hidden="true">&times;</span></span></button>
                    <h4 class="modal-title" id="deleteModalLabel">Delete Confirmation</h4>
                </div>
                <div class="modal-body">
                    <p>Please confirm you want to delete the deal?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" id="confirm_delete_deal" class="btn btn-danger">Confirm</button>
                </div>
            </div>
        </div>
    </div>

@endsection




@section('page-script')
<script>
(function($) {
    $.fn.hasScrollBar = function() {
        return this.get(0).scrollHeight > this.height();
    }
})(jQuery);

$(document).ready(function(){
    $( ".sort_records" ).sortable({
        connectWith: ".connectedSortable",
        containment: ".container",
        stop: function(e,ui){
        },
        update: function(event, ui){
            if(ui.sender){

                $('.stages_scroll').each(function(){
                    var size = {
                        width: window.innerWidth || document.body.clientWidth,
                        height: window.innerHeight || document.body.clientHeight
                    }

                    var scrollable_container_height = (size.height)-200;
                    $(this).css({'height':scrollable_container_height-30});
                    if($(this).hasScrollBar() == true){
                        $(this).find('ul').css({'width':'270px'});
                    }else{
                        $(this).find('ul').css({'width':'270px'});
                    }
                })


                var group = ui.item.parent().data('group');
                var id = ui.item.data('id');

                $.ajax({
                    url : '{{url('/stage_update')}}',
                    type : 'POST',
                    data : {'id':id, 'group':group, '_token':'{{ csrf_token() }}'},
                    dataType:'json',
                    success:function(response){
                        console.log(response);
                    }
                })
            }
        }
    }).disableSelection();


    $('.show_menu_buttons').on('click', function(e){
        var id = $(this).data('id');
        var title = $(this).data('title');

        $('#crm_menu_manage').attr('href','{{url('deal_view')}}/'+id);
        $('#crm_menu_edi').attr('data-id',id);
        $('#crm_menu_won').attr('data-id',id);
        $('#crm_menu_lost').attr('data-id',id);
        $('#archive_deal_id').val(id)
        $('#delete_deal_id').val(id);

        $('#editMenuDealLabel').html(title);
        $('#editMenuDeal').modal('show');
    })


    $('.edit_deal').click(function(e){

        $('#editMenuDeal').modal('hide');

        var id = $(this).data('id');

        $.ajax({
            url : '{{url('/deal_edit_form')}}/'+id,
            type : 'GET',
            data : 'id='+id,
            success:function(html){

                $('#deal_model_content').html(html);
                $('#editDeal').modal('show');
            }

        });
    });


    $('ol.breadcrumb').append('<li>Deals</li>');


    $('.visibility-change').on('change', function(){
        var checked = $('.visibility-change:checked').val();

        if(checked == 'team'){
            $('#select_team').fadeIn();
        }else{
            $('#select_team').fadeOut();
        }

    })


    /**
     * new add deal form
     */
    $( ".get_organization" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "{{ url('/organization_list') }}",
                dataType: "json",

                data: {
                    q: request.term
                },
                success: function( data ) {
                    if(data.length <= 0){
                        $('#organization_error').fadeIn();
                    }else{
                        response(data);
                        $('#organization_error').fadeOut();
                    }
                    $('.get_organization').delay(2000).removeClass('ui-autocomplete-loading');
                }
            });
        },
        select: function(e,ui){
            $('#create_organization_id').val(ui.item.id);
        }
    });

    $( ".get_organization" ).autocomplete( "option", "appendTo", ".new_deal_from" );

    $('#create_organization').on('click', function(){
        $('#organization_field').delay(1000).addClass('ui-autocomplete-loading');
        $('#organization_error').fadeOut();
        $.ajax({
            dataType: 'JSON',
            type:'POST',
            data: {'organization_name':$('#organization_field').val(), '_token':'{{ csrf_token() }}'},
            url: '{{ url('/create_organization_in_contact') }}',
            success:function(response){
                if(response != false && response != 'false'){
                    $('#organization_success').fadeIn().delay(2000).fadeOut();
                    $('#create_organization_id').val(response.id)
                    $('#organization_field').delay(1000).removeClass('ui-autocomplete-loading');
                }
            }
        })
    })



    $( ".get_contact" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "{{ url('/contact_list') }}",
                dataType: "json",

                data: {
                    q: request.term,'organization_id':$('#create_organization_id').val()
                },
                success: function( data ) {
                    if($('#create_organization_id').val() == ""){
                        $('#contact_field').delay(2000).removeClass('ui-autocomplete-loading');
                        $('#contact_error2').fadeIn().delay(3000).fadeOut();
                    }else if(data.length <= 0){
                        $('#contact_error').fadeIn();
                    }else{
                        $('#contact_error').fadeOut();
                        $('#contact_field').delay(2000).removeClass('ui-autocomplete-loading');
                        response(data);
                    }
                }
            });
        },
        select: function(e,ui){
            $('#create_contact_id').val(ui.item.id);
        }
    });

    $( ".get_contact" ).autocomplete( "option", "appendTo", ".new_deal_from" );

    $('#create_contact').on('click', function(){

        $('#contact_organization_id').val($('#create_organization_id').val());
        $('#contact_field').delay(1000).removeClass('ui-autocomplete-loading');

        height = $('#newAddDeal').find('.modal-body').css('height');
        $('#contact_error').fadeOut();
        $('#sample_modal').modal('show');

    })

    $('#submit_contact').on('click', function(){
        $.ajax({
            dataType: 'JSON',
            type:'POST',
            data: $('#create_contact_form').serialize(),
            url: '{{ url('/contact_json') }}',
            success:function(response){
                if(response[0] != 'error'){
                    $('#sample_modal').modal('hide');
                    $('#contact_field').val(response.contact_name);
                    $('#create_contact_id').val(response.id);
                    $('#contact_success').fadeIn().delay(3000).fadeOut();
                    $('#contact_field').delay(2000).removeClass('ui-autocomplete-loading');
                }else{
                    $('.error_message_in_contact').html(response[1]).fadeIn();
                    $('#sample_modal').animate({ scrollTop: 0 }, 'slow');
                }
            }
        })
    })

    // submit deal
    $('#create_btn').on('click', function(){
        form = $('.new_deal_from').serialize();
        var submit = true;

        if($('#organization_field').val().length == 0){
            $('#organization_field').after('<div class="general_error">This field is required</div>');
            submit = false;
        }
        if($('#contact_field').val().length == 0){
            $('#contact_field').after('<div class="general_error">This field is required</div>');
            submit = false;
        }
        if($('#create_deal_title').val().length == 0){
            $('#create_deal_title').after('<div class="general_error">This field is required</div>');
            submit = false;
        }

        $('.general_error').delay(3000).fadeOut(300, function(){ $(this).remove(); });

        if(submit){
            $.ajax({
                dataType: 'JSON',
                type:'POST',
                data: form,
                url: '{{ url('/create_deal') }}',
                success:function(response){
                    if(response==true||response=="true"){
                        $('#create_success_msg').fadeIn().delay(3000).fadeOut();
                        location.reload();
                    }
                }
            })
        }

    })


    $( '.dateapicker' ).pickadate({
        formatSubmit: 'yyyy-mm-dd',
        min: [<?=date('Y')?>, 01, 01],
        //container: '#container',
        closeOnSelect: true,
        closeOnClear: true
    })


    $('.eventInsForm').submit(function(event){

        if($(this).find('.dateapicker').val() == ''){
            $('.popup_error').show();
            event.preventDefault();
        }

    })

    $('#crm_menu_won').on('click', function(){
        var id = $(this).attr('data-id');

        $('#set_won_dea_id').val(id);
        $('#wonModal').modal('show');
    })
    $('#crm_menu_lost').on('click', function(){
        var id = $(this).attr('data-id');

        $('#set_lost_dea_id').val(id);
        $('#lostModal').modal('show');
    })


    var confirm_delete = false;
    $("form#delete_deal").submit(function (e) {
        if(!confirm_delete){
            e.preventDefault(); // this will prevent from submitting the form.
            $('.deleteModalLabel').modal('show');
        }
    });

    $('#confirm_delete_deal').on('click',function(){
        confirm_delete = true;
        $("form#delete_deal").submit();
    })





})
$('#main').css({'padding-bottom':0});
$(window).on("load resize",function(e){
    var size = {
        width: window.innerWidth || document.body.clientWidth,
        height: window.innerHeight || document.body.clientHeight
    }

    var scrollable_container_height = (size.height)-200;
    $('.scrollable_container').css({'height':scrollable_container_height});

    $('.stages_scroll').each(function(){
        $(this).css({'height':scrollable_container_height-30});

        if($(this).hasScrollBar() == true){
            $(this).find('ul').css({'width':'270px'});
        }else{
            $(this).find('ul').css({'width':'270px'});
        }
    })

})


</script>
@endsection
