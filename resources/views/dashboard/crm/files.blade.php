@extends('dashboard.layouts.master')


@section('content')
<style>
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
h2{
    margin:0 0 0 15px;
}
</style>
<a href="{{url('deal_view').'/'.$deal_id}}" class="btn btn-primary btn-sm pull-left">{{trans("crm_files.Back")}}</a>
<h2 class="pull-left">{{trans("crm_files.Files")}}</h2>

<div class="col-sm-6 col-md-offset-3">
    <hr>
    <table class="table table-stripe">
        <thead>
            <tr>
                <td colspan="2"></td>
                <td>
                    <button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#myModal" >{{trans("crm_files.Add")}}</button>
                </td>
            </tr>
        </thead>
        <tbody>
        @foreach($files as $file)
            <tr>
                <td>• {{$file['file_name']}} &nbsp;&nbsp;&nbsp;</td>
                <td><a href="{{$file['file_path']}}{{$file['file_name']}}">{{trans("crm_files.Download")}}</a></td>
                <td></td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3">
                    {!! $files->render() !!}
                </td>
            </tr>
        </tfoot>
    </table>
</div>







@endsection

@section('page-script')
<script>
$(document).ready(function(){

})
</script>
@endsection