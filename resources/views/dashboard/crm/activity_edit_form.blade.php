<form action="{{url('edit_activity')}}" method="POST" class="activityForm{{$id}}">
    <div class="modal-body">

        <div class="alert alert-danger" role="alert" style="display:none"> </div>

        {!! csrf_field() !!}
        <input type="hidden" name="user_id" value="{{ $user['id'] }}">
        <input type="hidden" name="activity_id" value="{{ $id }}">
        <input type="hidden" name="deal_id" value="{{ $post['deal_id'] }}">


        <div class="form-gourp">
            <label>{{trans("crm_activity_edit.Activity Title")}}</label>
            <input class="form-control delete_fields" type="text" name="activity_title" value="{{$activity['activity_title']}}" placeholder="" required />
        </div>

        <div class="form-gourp">
            <label>{{trans("Activity Type")}}</label>
            <div class="btn-group" data-toggle="buttons" style="width:100%;">
                <label class="btn btn-default {{$activity['activity_type']=='call'?'active':''}}" >
                    {{trans("crm_activity_edit.Call")}} <input type="radio" name="activity_type" value="call" {{$activity['activity_type']=='call'?'checked':''}} />
                </label>
                <label class="btn btn-default {{$activity['activity_type']=='meeting'?'active':''}}" >
                     {{trans("crm_activity_edit.Meeting")}} <input type="radio" name="activity_type" value="meeting" {{$activity['activity_type']=='meeting'?'checked':''}} >
                </label>
                <label class="btn btn-default {{$activity['activity_type']=='email'?'active':''}}" >
                    {{trans("crm_activity_edit.Email")}} <input type="radio" name="activity_type" value="email" {{$activity['activity_type']=='email'?'checked':''}} >
                </label>
                <label class="btn btn-default {{$activity['activity_type']=='deadline'?'active':''}}" >
                   {{trans("crm_activity_edit.Deadline")}} <input type="radio" name="activity_type" value="deadline" {{$activity['activity_type']=='deadline'?'checked':''}} >
                </label>
                <label class="btn btn-default {{$activity['activity_type']=='trash'?'active':''}}" >
                    {{trans("crm_activity_edit.Trash")}} <input type="radio" name="activity_type" value="trash" {{$activity['activity_type']=='trash'?'checked':''}} >
                </label>
                <label class="btn btn-default {{$activity['activity_type']=='other'?'active':''}}" >
                    {{trans("crm_activity_edit.Other")}} <input type="radio" name="activity_type" value="other" {{$activity['activity_type']=='other'?'checked':''}} >
                </label>
            </div>
        </div>

         <div class="form-gourp unique-form-group">
            <label class="pull-left one-line-label">{{trans("crm_activity_edit.Date")}}</label>
            <input type="text" name="date" value="{{$activity['date']}}" id="activity_date" class="dateapicker{{$id}} form-control delete_fields pull-left date-textbox" required />
            <label class="pull-left one-line-label">{{trans("crm_activity_edit.Time")}}</label>
            <input type="text" name="time" value="{{date("g:i a", strtotime($activity['time']))}}" id="activity_time" class="timepicker{{$id}} form-control delete_fields pull-left time-textbox" required />
            <label class="pull-left one-line-label">{{trans("crm_activity_edit.Duration")}}</label>
            {{--<input id="activity_duration" class="form-control delete_fields pull-left duration-textbox" type="text" name="duration" value="{{$activity['duration']}}" placeholder="" required />--}}
            <select name="duration" class="form-control delete_fields pull-left duration-textbox">
                <option <?=($activity['duration']=='15min'?'selected':'')?> value="15min">15min</option>
                <option <?=($activity['duration']=='30min'?'selected':'')?> value="30min">30min</option>
                <option <?=($activity['duration']=='45min'?'selected':'')?> value="45min">45min</option>
                <option <?=($activity['duration']=='1h'?'selected':'')?> value="1h">1h</option>
                <option <?=($activity['duration']=='1h30m'?'selected':'')?> value="1h30m">1h 30min</option>
                <option <?=($activity['duration']=='2h'?'selected':'')?> value="2h">2h</option>
                <option <?=($activity['duration']=='2h30m'?'selected':'')?> value="2h30m">2h 30min</option>
                <option <?=($activity['duration']=='3h'?'selected':'')?> value="3h">3h</option>
                <option <?=($activity['duration']=='3h30m'?'selected':'')?> value="3h30m">3h 30min</option>
                <option <?=($activity['duration']=='4h'?'selected':'')?> value="4h">4h</option>
                <option <?=($activity['duration']=='4h30m'?'selected':'')?> value="4h30m">4h 30min</option>
                <option <?=($activity['duration']=='5h30m'?'selected':'')?> value="5h30m">5h</option>
                <option <?=($activity['duration']=='5h30m'?'selected':'')?> value="5h30m">5h 30min</option>
                <option <?=($activity['duration']=='6h'?'selected':'')?> value="6h">6h</option>
                <option <?=($activity['duration']=='6h30m'?'selected':'')?> value="6h30m">6h 30min</option>
                <option <?=($activity['duration']=='7h'?'selected':'')?> value="7h">7h</option>
                <option <?=($activity['duration']=='7h30m'?'selected':'')?> value="7h30m">7h 30min</option>
                <option <?=($activity['duration']=='8h'?'selected':'')?> value="8h">8h</option>
                <option <?=($activity['duration']=='8h30m'?'selected':'')?> value="8h30m">8h 30min</option>
            </select>
         </div>

        <div class="form-gourp">
            <label>{{trans("crm_activity_edit.Owner")}}</label>
            <input class="form-control delete_fields user_auto_complete{{$id}}" type="text" name="owner" value="{{$activity['owner_name']}}" placeholder="" required />
            <input class="form-control delete_fields" type="hidden" name="owner_id" value="{{$activity['owner_id']}}" placeholder="" />
        </div>
        <div class="form-gourp">
            <label>{{trans("crm_activity_edit.Assigned")}}</label>
            <input class="form-control delete_fields user_auto_complete{{$id}}" type="text" name="assigned" value="{{$activity['assigned_name']}}" placeholder="" required />
            <input class="form-control delete_fields" type="hidden" name="assigned_user_id" value="{{$activity['assigned_user_id']}}" placeholder="" />
        </div>
        <div class="form-gourp">
            <label class="radio-inline">
                {{trans("crm_activity_edit.Done")}}
                <input type="checkbox" name="done" value="1" class="" {{$activity['done']==1?'checked':''}} />
            </label>

        </div>


    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("crm_activity_edit.Close")}}</button>
        <button type="submit" class="btn btn-primary">{{trans("crm_activity_edit.Save changes")}}</button>
    </div>
</form>


<script>
$(document).ready(function(){

    $( ".user_auto_complete{{$id}}" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "{{ url('/company_user_list') }}",
                dataType: "json",

                data: {
                    q: request.term,
                    '_token':'{{ csrf_token() }}'
                },
                type: 'POST',
                success: function( data ) {
                console.log(data);
                   response(data);
                }
            });
        },
        select: function(e,ui){
            $(this).next().attr('value',ui.item.user_id);

                console.log($(this).next().val());
        }
    });

    $( ".user_auto_complete{{$id}}" ).autocomplete( "option", "appendTo", ".activityForm{{$id}}" );

    $( '.dateapicker{{$id}}' ).pickadate({
        formatSubmit: 'yyyy-mm-dd',
        // min: [2015, 7, 14],
        //container: '#container',
        // editable: true,
        closeOnSelect: true,
        closeOnClear: true
    })

    $('.timepicker{{$id}}').pickatime()

    $('input[name="date_submit"]').val('<?=$activity['date'];?>');

})
</script>