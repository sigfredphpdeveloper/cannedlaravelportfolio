@extends('dashboard.layouts.master')

@section('content')

<script src="{{ url('js/plugin/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('js/plugin/datatable-responsive/datatables.responsive.min.js') }}"></script>

<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/data_tables.css') }}">
<!-- widget grid -->
<style>
table thead tr th {
    background-color: #F7DCB4 !important;
}

tfoot {
    display: table-header-group !important;
}
thead {
    display: table-row-group !important;
}

</style>

    {{--<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/jquery-ui-1.10.4.custom.min.css') }}">--}}
    {{--<script src="{{ asset('js/jquery-ui.custom.min.js') }}"></script>--}}
    <link rel="stylesheet" type="text/css" media="screen" href="https://code.jquery.com/ui/jquery-ui-git.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.devbridge-autocomplete/1.2.24/jquery.autocomplete.js"></script>

<style>
.ui-autocomplete {
z-index: 100 !important;
}

ul.pagination li{
    background: none !important;
}
.margin-bottom{
    /*margin-bottom: 50px;*/
}
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
.bold_text{
    font-weight: bold;
}
.btn-default{
    background:#e7e7e7;
}
.left-form-group-dropdown{
    margin: 0;
    border: 0;
    padding: 0;
    width: 165px;
    -webkit-border-radius: 5px 0 0 5px !important;
    border-radius: 5px 0 0 5px !important;
}
.left-form-group-dropdown select{
    -webkit-border-radius: 5px 0 0 5px !important;
    border-radius: 5px 0 0 5px !important;
}
#file_upload_form{
    margin-right:5px;
}
</style>

<div class="row">
    @include('errors.success')
    @include('errors.errors')

    <div class="col-xs-12 margin-bottom">
        <div class="col-sm-2">
            <select name="page" class="form-control" id="page-location">
                <option value="contacts" selected >{{trans("contact.View Contacts")}}</option>
                <option value="organization" >{{trans("contact.View Organizations")}}</option>
            </select>
        </div>
        <div class="col-sm-4 pull-right">

            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary pull-left;" data-toggle="modal" data-target="#myModal">{{trans("contact.Add")}}</button>
            @if($is_admin)
                <a href="{{url('organization_setting')}}" class="btn btn-primary">{{trans("contact.Customize")}}</a>
            @endif

            <form action="{{ url('upload_contact') }}" method="POST" id="file_upload_form" enctype="multipart/form-data" class="pull-left">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="user_id" value="{{ $user['id'] }}">
                <input type="hidden" name="redirect" value="contact">
                <span class="btn btn-primary btn-file" id="bulkupload" data-toggle="tooltip" data-placement="top" title="Please follow column header. Contact Name, Contact Email, Organization, Contact Phone, Position, Address, Notes">
                    <input type="file" name="upload" id="upload_excel" multiple >{{trans("contact.Bulk Upload")}}
                </span>
            </form>
        </div>

        <div class="col-sm-6 pull-right">
            <form action="{{ url('contact_search') }}" method="POST">
                <div class="input-group">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="user_id" value="{{ $user['id'] }}">
                    <div class="input-group-addon left-form-group-dropdown">
                        <select name="column_name" class="form-control" id="search_filter">
                            <option value="contact_name" <?=(!empty($post['column_name']) && $post['column_name']=='contact_name')?'selected="selected"':'';?> >{{trans("contact.Contact Name")}}</option>
                            <option value="organization_name" <?=(!empty($post['column_name']) && $post['column_name']=='organization_name')?'selected="selected"':'';?> >{{trans("contact.Organization Name")}}</option>
                            <option value="contact_email" <?=(!empty($post['column_name']) && $post['column_name']=='contact_email')?'selected="selected"':'';?> >{{trans("contact.Email")}}</option>
                        </select>
                    </div>
                    <input type="text" name="search" id="search_box" class="form-control" value="{{!empty($post['search'])?$post['search']:''}}" placeholder='{{trans("contact.Search for...")}}'>
                    <span class="input-group-btn">
                        <button class="btn btn-default" id="search_btn" type="submit">{{trans("contact.Search")}}</button>
                    </span>
                </div>
            </form>
        </div>
        <div class="col-xs-12" style="height:30px;padding-top:5px;">
            <b>{{trans_choice('contact.contact_count_info', $total, ['total'=>$total])}}</b>
        </div>

        <table class="table table-striped datatable-dom-position">
            <thead>
                <tr>
                    <th>{{trans("contact.Contact Name")}}</th>
                    <th class="hidden-xs">{{trans("contact.Organization")}}</th>
                    <th>{{trans("contact.Email")}}</th>
                    <th class="hidden-xs">{{trans("contact.Owner")}}</th>
                    <th>{{trans("contact.Actions")}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($contacts as $key => $contact)
                <tr>
                    <td><a href="#" class="contact_info" data-key="{{$key}}" data-toggle="modal" data-target="#contactInfo" >{{$contact->contact_name}}</a></td>
                    <td class="hidden-xs">{{$contact->organization_name}}</td>
                    <td>{{$contact->contact_email}}</td>
                    <td class="hidden-xs">{{$contact->title}} {{$contact->first_name}} {{$contact->last_name}}</td>
                    <td>

                        <button type="button" class="btn btn-primary btn-sm btn-edit" data-id="{{$contact->contact_id}}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button>
                        <button type="button" class="btn btn-danger btn-sm deleter" data-id="{{$contact->contact_id}}"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>

                        {{--<div class="input-group-btn" style="width: auto;">--}}
                            {{--<button type="button" class="btn btn-default" tabindex="-1">{{trans("expenses_index.Action")}}</button>--}}
                            {{--<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1" aria-expanded="false">--}}
                                {{--<span class="caret"></span>--}}
                            {{--</button>--}}
                            {{--<ul class="dropdown-menu pull-right" role="menu">--}}
                                {{--<li><a class="btn-edit" data-id="{{$contact->contact_id}}">{{trans("contact.Edit")}}</a></li>--}}
                                {{--<li class="divider"></li>--}}
                                {{--<li><a class="deleter" data-id="{{$contact->contact_id}}">{{trans("contact.Delete")}}</a></li>--}}
                            {{--</ul>--}}
                        {{--</div>--}}
                        {{--<button class="btn btn-info btn-sm btn-edit" data-id="{{$contact->contact_id}}">{{trans("contact.Edit")}}</button>--}}
                        {{--<button class="btn btn-danger btn-sm deleter" data-id="{{$contact->contact_id}}">{{trans("contact.Delete")}}</button>--}}
                    </td>
                </tr>
                @endforeach

            </tbody>
            <tfooot>
                <tr>
                    <td colspan="5"> {!! $contacts->render() !!}</td>
                </tr>
            </tfooot>
        </table>
    </div>
</div>




<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="myModalLabel">{{trans("contact.Add a new Contact")}}</h4>
            </div>
            <form action="{{ url('/contact') }}" method="POST" class="eventInsForm">
                <div class="modal-body">
                    {!! csrf_field() !!}
                    <input type="hidden" name="user_id" value="{{ $user['id'] }}">


                    <div class="form-group">
                        <label>{{trans("contact.Contact Name")}}</label>
                        <input class="form-control delete_fields" type="text" name="contact_name" value="" placeholder="ex.John" required />
                    </div>

                    <div class="form-group">
                        <label>{{trans("contact.Contact Email")}}</label>
                        <input class="form-control delete_fields" type="text" name="contact_email" value="" placeholder="sample@example.com" required />
                    </div>

                    <div class="form-group">
                        <label>{{trans("contact.Contact Phone")}}</label>
                        <input class="form-control delete_fields" type="text" name="contact_phone" value="" placeholder="##-####-####" />
                    </div>

                    <div class="form-group">
                        <label>{{trans("contact.Organization")}}</label>
                        <input class="form-control delete_fields custom_auto_complete" autocomplete="off" type="text" value="" placeholder="" required />
                        <input class="form-control delete_fields" type="hidden" name="organization_id" id="organization_id" value="" placeholder="" required />
                    </div>

                    <div class="form-group">
                        <label>{{trans("contact.Position")}}</label>
                        <input class="form-control delete_fields" type="text" name="position" value="" placeholder="" />
                    </div>

                    <div class="form-group">
                        <label>{{trans("contact.Address")}}</label>
                        <input class="form-control delete_fields" type="text" name="address" value="" placeholder="" />
                    </div>

                    <div class="form-group">
                        <label>{{trans("contact.Notes")}}</label>
                        <textarea class="form-control delete_fields" name="contact_notes" id=""></textarea>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("contact.Close")}}</button>
                    <button type="submit" class="btn btn-primary">{{trans("contact.Save changes")}}</button>
                </div>
            </form>
        </div>
    </div>
</div>




<!-- Modal -->
<div class="modal fade" id="contact_modal" tabindex="-1" role="dialog" aria-labelledby="contact_modal">
    <div class="modal-dialog" role="document">

        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="contact_modal">{{trans("contact.Edit Contact")}}</h4>
            </div>
            <div class="modal-body" id="contact_modal_body">

             </div>
        </div>
    </div>
</div>




<div class="modal fade" tabindex="-1" role="dialog" id="confirm">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{trans("contact.Confirmation")}}</h4>
      </div>
      <div class="modal-body">
        <p>{{trans("contact.Are you sure you want to delete this record ?")}}</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("contact.No")}}</button>
       <button type="button" class="btn btn-primary" id="delete_proceed" alt="">{{trans("contact.Yes")}}</button>

      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




<!-- Modal -->
<div class="modal fade" id="contactInfo" tabindex="-1" role="dialog" aria-labelledby="contactInfoLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="contactInfoLabel">{{trans("contact.View Contact Details")}}</h4>
            </div>
            <div id="contacts_info_modal" class="modal-body">

                <div class="form-group">
                    <label class="bold_text">{{trans("contact.Contact Name:")}}</label>
                    <label id="info_contact_name"></label>
                </div>
                <div class="form-group">
                    <label class="bold_text">{{trans("contact.Position:")}}</label>
                    <label id="info_position"></label>
                </div>
                <div class="form-group">
                    <label class="bold_text">{{trans("contact.Contact Email:")}}</label>
                    <label id="info_contact_email"></label>
                </div>
                <div class="form-group">
                    <label class="bold_text">{{trans("contact.Contact Phone:")}}</label>
                    <label id="info_contact_phone"></label>
                </div>
                <div class="form-group">
                    <label class="bold_text">{{trans("contact.Address:")}}</label>
                    <label id="info_address"></label>
                </div>
                <div class="form-group">
                    <label class="bold_text">{{trans("contact.Organization:")}}</label>
                    <label id="info_organization"></label>
                </div>
                <div class="form-group">
                    <label class="bold_text">{{trans("contact.Organization Address:")}}</label>
                    <label id="info_org_address"></label>
                </div>
                <div class="form-group">
                    <label class="bold_text">{{trans("contact.Contact Note:")}}</label>
                    <label id="info_contact_note"></label>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("contact.Close")}}</button>
            </div>
        </div>
    </div>
</div>


@endsection



@section('page-script')
<script>
$(document).ready(function(){

    $('.deleter').click(function(){

        var id = $(this).data('id');
        $('#delete_proceed').attr('alt',id);
        $('#confirm').modal('show');

    });

    $('#delete_proceed').click(function(){

        var id = $(this).attr('alt');

        $('#confirm').modal('hide');

        window.location.href='{{ url('contact_delete') }}/'+id;

    });

    $( ".custom_auto_complete" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "{{ url('/organization_list') }}",
                dataType: "json",

                data: {
                    q: request.term
                },
                success: function( data ) {
//                console.log(data);
                   response(data);
                }
            });
        },
        select: function(e,ui){
//            console.log(ui);
            $('#organization_id').val(ui.item.id);
        }
    });

    $( ".custom_auto_complete" ).autocomplete( "option", "appendTo", ".eventInsForm" );

//    $('.custom_auto_complete').on('keyup',function(){
//        console.log('11111111111111111')
//    })

    //setup before functions
    var typingTimer;                //timer identifier
    var doneTypingInterval = 2000;  //time in ms, 5 second for example
    var $input = $('.custom_auto_complete');

    //on keyup, start the countdown
    $input.on('keyup', function () {
      clearTimeout(typingTimer);
      typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });

    //on keydown, clear the countdown
    $input.on('keydown', function () {
      clearTimeout(typingTimer);
    });

    //user is "finished typing,"
    function doneTyping () {
        $.ajax({
            dataType: 'JSON',
            type:'POST',
            data: {'organization':$input.val(), '_token':'{{ csrf_token() }}'},
            url: '{{ url('/get_contact') }}',
            success:function(response){
                if(response != false && response != 'false'){
                    $('#organization_id').val(response.id);
                }else{
                    $('.custom_auto_complete').prev().prev().html('Organization <button type="button" class="create_or_in_contact btn-success">Not exist! Create this Organization now</button>');
                }
            }
        })
    }

    $('form.eventInsForm').on('click','.create_or_in_contact',function(){
        $('.custom_auto_complete').prev().prev().html('Organization <img src="{{url()}}/img/select2-spinner.gif">')

        $.ajax({
            dataType: 'JSON',
            type:'POST',
            data: {'organization_name':$('.custom_auto_complete').val(), '_token':'{{ csrf_token() }}'},
            url: '{{ url('/create_organization_in_contact') }}',
            success:function(response){
                if(response != false && response != 'false'){
                    $('.custom_auto_complete').prev().prev().html('Organization <span class="label label-success">'+response.message+'</span>');
                    $('#organization_id').val(response.id);
                }
            }
        })
    });


    $('.btn-edit').click(function(){
        var id = $(this).data('id');

        $.ajax({
            url : '{{url('/contact_edit/')}}/'+id,
            type : 'GET',
            data : 'id='+id,
//            dataType:'json',
            success:function(html){
                $('#contact_modal_body').html(html);
                $('#contact_modal').modal('show');
            }

        });
    });

	$('#bulkupload').tooltip();

    $('#upload_excel').on('change', function(){
        $('#file_upload_form').submit();
    })
	
	
	$('form.eventInsForm').on('submit', function(e){
		var org_id = $('#organization_id').val();
		var me = $(this);
		
		if(org_id == ''){
			$('#organization_id').parent().find('label').html('Organization <label style="color:#f00;"> This organization in not part of your existing organizations. Would you like to <a href="{{ url('organization') }}">create a new organization</a>?</label>');
			e.preventDefault();
		}
	})

    var contacts = <?=json_encode($json_contacts)?>;
    $('.contact_info').on('click', function(){
        var key = $(this).data('key');

        $('#contacts_info_modal label#info_contact_name').html(contacts[key].contact_name);
        $('#contacts_info_modal label#info_contact_email').html(contacts[key].contact_email);
        $('#contacts_info_modal label#info_contact_phone').html(contacts[key].contact_phone);
        $('#contacts_info_modal label#info_position').html(contacts[key].position);
        $('#contacts_info_modal label#info_address').html(contacts[key].address);
        $('#contacts_info_modal label#info_organization').html(contacts[key].organization_name);
        $('#contacts_info_modal label#info_org_address').html(contacts[key].organization_address);
        $('#contacts_info_modal label#info_contact_note').html(contacts[key].contact_notes);

    })

    $('ol.breadcrumb').append('<li>Contacts</li>');

    $('#page-location').on('change', function(){
        var val = $(this).val();

        if(val == 'contacts'){
            location.href ='{{ url("contact") }}';
        }else if(val == 'organization'){
            location.href ='{{ url("organization") }}';
        }
    })

       var table =  $('.datatable-dom-position').DataTable({
           dom: '<"datatable-header length-left"><"datatable-scroll"t><"datatable-footer info-right"lp>'
       });

})
</script>
@endsection