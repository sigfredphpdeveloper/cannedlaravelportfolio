@extends('dashboard.layouts.master')

@section('content')

    <h2>{{trans("contact.Contacts")}}</h2>

    <div class="col-xs-12">
        <div class="col-sm-2 pull-right">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">{{trans("contact.Add")}}</button>
        </div>
        <div class="col-sm-2 pull-right">
         <button class="btn btn-info ">{{trans("contact.Bulk Upload")}}</button>
        </div>

        <div class="col-sm-5 pull-right">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="{{trans("contact.Search for")}}" >
                   <span class="input-group-btn"> <button class="btn btn-info" type="button">{{trans("contact.Search")}}</button></span>
            </div>
        </div>

        <table class="table table-striped">
            <thead>
                <tr>
                    <th>{{trans("contact.Contact Name")}}</th>
                    <th>{{trans("contact.Organization")}}</th>
                    <th>{{trans("contact.Email")}}</th>
                    <th>{{trans("contact.Owner")}}</th>
                    <th>{{trans("contact.Actions")}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($contact as $v)
                <tr>
                    <td>{{$v['contact_name']}}</td>
                    <td>{{$v['organization_id']}}</td>
                    <td>{{$v['contact_email']}}</td>
                    <td>{{$v['ownder_id']}}</td>
                    <td>
                        <button class="btn btn-info btn-sm">{{trans("contact.Edit")}}</button>
                        <button class="btn btn-danger btn-sm">{{trans("contact.Delete")}}</button>
                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>


    </div>



<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">

                <form action="{{ url('/contact') }}" method="POST">
                {!! csrf_field() !!}
                <input type="hidden" name="user_id" value="{{ $user['id'] }}">


                    <div class="form-gourp">
                        <label>{{trans("contact.Contact Name")}}</label>
                        <input class="form-control delete_fields" type="text" name="contact_name" value="" placeholder="ex.John" />
                    </div>

                    <div class="form-gourp">
                        <label>{{trans("contact.Contact Email")}}</label>
                        <input class="form-control delete_fields" type="text" name="contact_email" value="" placeholder="sample@example.com" />
                    </div>

                    <div class="form-gourp">
                        <label>{{trans("contact.Contact Phone")}}</label>
                        <input class="form-control delete_fields" type="text" name="contact_phone" value="" placeholder="##-####-####" />
                    </div>

                    <div class="form-gourp">
                        <label>{{trans("contact.Organization")}}</label>
                        <input class="form-control delete_fields" type="text" name="organization" value="" placeholder="" />
                    </div>

                    <div class="form-gourp">
                        <label>{{trans("contact.Position")}}</label>
                        <input class="form-control delete_fields" type="text" name="position" value="" placeholder="" />
                    </div>

                    <div class="form-gourp">
                        <label>{{trans("contact.Address")}}</label>
                        <input class="form-control delete_fields" type="text" name="address" value="" placeholder="" />
                    </div>

                    <div class="form-gourp">
                        <label>{{trans("contact.Notes")}}</label>
                        <textarea class="form-control delete_fields" name="notes" id="" cols="30" rows="10"></textarea>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" id="delete_text_field" class="btn btn-danger">{{trans("contact.Delete")}}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("contact.Close")}}</button>
                    <button type="submit" class="btn btn-primary">{{trans("contact.Save changes")}}</button>
                </div>
            </form>
        </div>
    </div>
</div>


@endsection

