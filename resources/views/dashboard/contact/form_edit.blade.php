<form action="{{ url('/contact_edit/') }}/{{$contact['contact_id']}}" method="POST" class="eventInsForm{{$contact['contact_id']}}" >

{!! csrf_field() !!}
<input type="hidden" name="user_id" value="{{ $user['id'] }}">


<div class="form-gourp">
    <label>{{trans("contact.Contact Name")}}</label>
    <input class="form-control delete_fields" type="text" name="contact_name" value="{{$contact['contact_name']}}" placeholder="ex.John" />
</div>

<div class="form-gourp">
    <label>{{trans("contact.Contact Email")}}</label>
    <input class="form-control delete_fields" type="text" name="contact_email" value="{{$contact['contact_email']}}" placeholder="sample@example.com" />
</div>

<div class="form-gourp">
    <label>{{trans("contact.Contact Phone")}}</label>
    <input class="form-control delete_fields" type="text" name="contact_phone" value="{{$contact['contact_phone']}}" placeholder="##-####-####" />
</div>

<div class="form-gourp">
    <label>{{trans("contact.Organization")}}</label>
    <input class="form-control delete_fields added_custom_auto_complete{{$contact['contact_id']}} " type="text" value="{{$contact['organization_name']}}" placeholder="" />
    <input class="form-control delete_fields" type="hidden" name="organization_id" id="added_organization_id{{$contact['contact_id']}}" value="{{$contact['organization_id']}}" placeholder="" />
</div>

<div class="form-gourp">
    <label>{{trans("contact.Position")}}</label>
    <input class="form-control delete_fields" type="text" name="position" value="{{$contact['position']}}" placeholder="" />
</div>

<div class="form-gourp">
    <label>{{trans("contact.Address")}}</label>
    <input class="form-control delete_fields" type="text" name="address" value="{{$contact['address']}}" placeholder="" />
</div>

<div class="form-gourp">
    <label>{{trans("contact.Notes")}}</label>
    <textarea class="form-control delete_fields" name="contact_notes" id="">{{$contact['contact_notes']}}</textarea>
</div>

 <div class="modal-footer">
   <button type="button" id="delete_text_field" class="btn btn-danger">{{trans("contact.Delete")}}</button>
   <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("contact.Close")}}</button>
   <button type="submit" class="btn btn-primary">{{trans("contact.Save changes")}}</button>
</div>


 </form>

    {{--<link rel="stylesheet" type="text/css" media="screen" href="https://code.jquery.com/ui/jquery-ui-git.css">--}}
    {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.devbridge-autocomplete/1.2.24/jquery.autocomplete.js"></script>--}}

 <script>
 $(document).ready(function(){

    $( ".added_custom_auto_complete{{$contact['contact_id']}}" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "{{ url('/organization_list') }}",
                dataType: "json",

                data: {
                    q: request.term
                },
                success: function( data ) {
					if(data == ''){
						$('#added_organization_id{{$contact['contact_id']}}').val('');
					}
                   response(data);
                }
            });
        },
        select: function(e,ui){
            console.log(ui);
            $('#added_organization_id{{$contact['contact_id']}}').val(ui.item.id);
        }
    });
    $( ".added_custom_auto_complete{{$contact['contact_id']}}" ).autocomplete( "option", "appendTo", ".eventInsForm{{$contact['contact_id']}}" );



   $('#delete_text_field').on('click', function(){
       $('.delete_fields').val('');
   });
   
   $('form.eventInsForm{{$contact['contact_id']}}').on('submit', function(e){
		var org_id = $('#added_organization_id{{$contact['contact_id']}}').val();
		var me = $(this);
		
		if(org_id == ''){
			$('#added_organization_id{{$contact['contact_id']}}').parent().find('label').html('Organization <label style="color:#f00;"> *is not exist!, and please select the recommended organization</label> &nbsp;&nbsp;<a class="btn btn-info btn-sm" style="padding:2px;margin:3px 0;" href="{{ url('organization') }}">Create Organization</a>');
			e.preventDefault();
		}
		
	})

	//setup before functions
        var typingTimer;                //timer identifier
        var doneTypingInterval = 2000;  //time in ms, 5 second for example

        //on keyup, start the countdown
        $( ".added_custom_auto_complete{{$contact['contact_id']}}" ).on('keyup', function () {
          clearTimeout(typingTimer);
          typingTimer = setTimeout(doneTyping2, doneTypingInterval);
        });

        //on keydown, clear the countdown
        $( ".added_custom_auto_complete{{$contact['contact_id']}}" ).on('keydown', function () {
          clearTimeout(typingTimer);
        });

        //user is "finished typing,"
        function doneTyping2 () {
            $.ajax({
                dataType: 'JSON',
                type:'POST',
                data: {'organization':$('.added_custom_auto_complete{{$contact['contact_id']}}').val(), '_token':'{{ csrf_token() }}'},
                url: '{{ url('/get_contact') }}',
                success:function(response){
                    if(response != false && response != 'false'){
                        $('#added_organization_id{{$contact['contact_id']}}').val(response.id);
                    }else{
                        $('.added_custom_auto_complete{{$contact['contact_id']}}').prev().prev().html('Organization <button type="button" class="create_or_in_contact{{$contact['contact_id']}} btn-success">Not exist! Create this Organization now</button>');
                    }
                }
            })
        }

        $('form.eventInsForm{{$contact['contact_id']}}').on('click','.create_or_in_contact{{$contact['contact_id']}}',function(){
            $('.added_custom_auto_complete{{$contact['contact_id']}}').prev().prev().html('Organization <img src="{{url()}}/img/select2-spinner.gif">')

            $.ajax({
                dataType: 'JSON',
                type:'POST',
                data: {'organization_name':$('.added_custom_auto_complete{{$contact['contact_id']}}').val(), '_token':'{{ csrf_token() }}'},
                url: '{{ url('/create_organization_in_contact') }}',
                success:function(response){
                    if(response != false && response != 'false'){
                        $('.added_custom_auto_complete{{$contact['contact_id']}}').prev().prev().html('Organization <span class="label label-success">'+response.message+'</span>');
                        $('#added_organization_id{{$contact['contact_id']}}').val(response.id);
                    }
                }
            })
        })

 })
 </script>