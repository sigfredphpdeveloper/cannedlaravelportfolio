        <div class="col-sm-12 col-md-12">
        <h1>{{trans("complete_profile.Complete Your Registration")}}</h1>
        </div>

         <div class="col-md-12">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ Session::get('success') }}</p>
                </div>
            @endif
            </div>

            <div class="col-md-12">
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <p>{{ Session::get('error') }}</p>
                    </div>
                @endif
            </div>

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>{{trans("complete_company.Whoops!")}}</strong> {{trans("complete_company.There were some problems with your input.")}}<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif





        <div class="alert alert-warning">
            <p>{{trans("complete_profile.Please fill in the form to configure the platform for your company (Step 1)")}}</p>
        </div>

        <article class="col-sm-6 col-md-6 col-lg-6">

            <form id="smart-form-register" class="smart-form client-form" method="POST" enctype="multipart/form-data" action="{{url('/complete_profile')}}">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="user_id" value="{{ $user['id'] }}">

                <header>

                     {{trans("complete_profile.Your Profile")}}

                </header>


                <fieldset>

                     <section>
                        <label class="label">{{trans("complete_profile.Position")}}</label>
                        <label class="input">
                        <input type="text" name="position" placeholder="{{trans("complete_profile.Position")}}" value="{{ $user['position'] }}" />

                        </label>
                    </section>

                     <section>
                        <label class="label">{{trans("complete_profile.Phone Number")}}</label>
                        <label class="input">
                            <input type="text" name="phone_number" placeholder="{{trans("complete_profile.Phone Number")}}" value="{{ $user['phone_number'] }}">
                            <b class="tooltip tooltip-bottom-right">{{trans("complete_profile.Optional")}}</b> </label>
                    </section>


                </fieldset>

                <fieldset>

                    <div class="row">
                    <section class="col col-2">
                        <label class="label">{{trans("complete_profile.Title")}}</label>
                        <label class="input">
                            <select name="title" class="form-control">
                                    <option value="Mr" <?php echo ($user['title']=='Mr')?'selected':'';?>>{{trans("complete_profile.Mr")}}</option>
                                    <option value="Mrs" <?php echo ($user['title']=='Mrs')?'selected':'';?>>{{trans("complete_profile.Mrs.")}}</option>
                                    <option value="Ms" <?php echo ($user['title']=='Ms')?'selected':'';?>>{{trans("complete_profile.Ms.")}}</option>
                            </select>
                        </label>
                    </section>
                        <section class="col col-5">
                        <label class="label">{{trans("complete_profile.First Name")}} *</label>
                            <label class="input">
                                <input type="text" name="first_name" placeholder="{{trans("complete_profile.First Name *")}}" value="{{ $user['first_name'] }}" >
                            </label>
                        </section>
                        <section class="col col-5">
                            <label class="label">{{trans("complete_profile.Last Name")}} *</label>
                            <label class="input">
                                <input type="text" name="last_name" placeholder="{{trans("complete_profile.Last Name *")}}" value="{{ $user['last_name'] }}" >
                            </label>
                        </section>
                    </div>

                    <section >
                        <label class="label">{{trans("complete_profile.Website")}} *</label>
                        <label class="input">
                            <input type="text" name="company_url" value="{{ $company['url'] }}" placeholder="{{trans("complete_profile.Company URL *")}}" disabled>
                        </label>
                    </section>

                    <section>
                        <label class="label">{{trans("complete_profile.Email")}}</label>
                        <label class="input"> <i class="icon-append fa fa-lock"></i>
                            <input type="email" name="email" placeholder="{{trans("complete_profile.Email *")}}" value="{{ $user['email'] }}"
                            class="form-control" disabled/>
                            <b class="tooltip tooltip-bottom-right">{{trans("complete_profile.Used to log in")}}</b> </label>
                    </section>

                    <section >
                        <label class="label">{{trans("complete_profile.Password")}}</label>
                        <label class="input">
                            <input type="password" name="password"  placeholder="{{trans("complete_profile.Change Password *")}}" >
                        </label>
                    </section>

                    <section>
                        <label class="label">{{trans("complete_profile.Profile Picture")}}</label>

                        @if($user['photo'])
                            <img src="{{$user['photo']}}" />
                        @endif
                        <br />

                        <label class="input">
                            {!! Form::file('image', null) !!}
                        </label>


                    </section>


                    <?php echo view('dashboard.user.custom_fields',compact('user_fields','custom_keypairs_array')); ?>


                </fieldset>


                <footer>
                    <button type="submit" class="btn btn-success">
                        {{trans("complete_profile.Update")}}
                    </button>
                </footer>


            </form>

        </article>
