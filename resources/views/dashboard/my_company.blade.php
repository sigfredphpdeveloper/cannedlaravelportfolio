
@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->

    <div class="row">

        <div class="col-md-12">
                <h1>{{trans("my_company.Update Company")}}</h1>
        </div>

        <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        </div>


        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>{{trans("directory_invite_user.Warning.")}}<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <article class="col-sm-6 col-md-6 col-lg-6">

            <form id="smart-form-register" class="smart-form client-form" action="{{url('my_company_save')}}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="company_id" value="{{ $company['id'] }}">

                <header>

                     {{trans("my_company.My Company")}}

                </header>

                <fieldset>

                     <section>
                        <label class="label">{{trans("my_company.Company")}}</label>
                        <label class="input">
                        <input type="text" name="company_name" placeholder="{{trans("my_company.Name of Company")}}" value="{{ $company['company_name'] }}" />

                        </label>
                    </section>

                    <section>
                        <label class="label">{{trans("my_company.Website")}}</label>
                        <label class="input">
                        <input type="text" name="url" placeholder="{{trans("my_company.Website")}}" value="{{ $company['url'] }}" />

                        </label>
                    </section>

                    <section>
                        <label class="label">{{trans("my_company.Industry")}} *</label>
                        <label class="input">
                        <select name="industry" class="form-control" placeholder="Select Industry">
                           <option value="">{{trans("my_company.Select Industry")}}</option>
                           <option value="Agency (branding/design/creative/video)" <?php echo ($company['industry']=='Agency (branding/design/creative/video)')?'selected':'';?>>{{trans("my_company.Agency (branding/design/creative/video)")}}</option>
                           <option value="Agency (web)" <?php echo ($company['industry']=='Agency (web)')?'selected':'';?>>{{trans("my_company.Agency (web)")}}</option>
                           <option value="Agency (SEM/SEO/social media)" <?php echo ($company['industry']=='Agency (SEM/SEO/social media)')?'selected':'';?>>{{trans("my_company.Agency (SEM/SEO/social media)")}}</option>
                           <option value="Agency (other)" <?php echo ($company['industry']=='Agency (other)')?'selected':'';?>>{{trans("my_company.Agency (other)")}}</option>
                           <option value="Church/religious organisation" <?php echo ($company['industry']=='Church/religious organisation')?'selected':'';?>>{{trans("my_company.Church/religious organisation")}}</option>
                           <option value="E-Commerce" <?php echo ($company['industry']=='E-Commerce')?'selected':'';?>>{{trans("my_company.E-Commerce")}}</option>
                           <option value="Engineering" <?php echo ($company['industry']=='Engineering')?'selected':'';?>>{{trans("my_company.Engineering")}}</option>
                           <option value="Health/beauty" <?php echo ($company['industry']=='Health/beauty')?'selected':'';?>>{{trans("my_company.Health/beauty")}}</option>
                           <option value="Hospitality" <?php echo ($company['industry']=='Hospitality')?'selected':'';?>>{{trans("my_company.Hospitality")}}</option>
                           <option value="Industrials" <?php echo ($company['industry']=='Industrials')?'selected':'';?>>{{trans("my_company.Industrials")}}</option>
                           <option value="IT/technology" <?php echo ($company['industry']=='IT/technology')?'selected':'';?>>{{trans("my_company.IT/technology")}}</option>
                           <option value="Media/publishing" <?php echo ($company['industry']=='Media/publishing')?'selected':'';?>>{{trans("my_company.Media/publishing")}}</option>
                           <option value="Non profit" <?php echo ($company['industry']=='Non profit')?'selected':'';?>>{{trans("my_company.Non profit")}}</option>
                           <option value="Other" <?php echo ($company['industry']=='Other')?'selected':'';?>>{{trans("my_company.Other")}}</option>
                           <option value="Retail/consumer merchandise" <?php echo ($company['industry']=='Retail/consumer merchandise')?'selected':'';?>>{{trans("my_company.Retail/consumer merchandise")}}</option>
                           <option value="School/education" <?php echo ($company['industry']=='School/education')?'selected':'';?>>{{trans("my_company.School/education")}}</option>
                           <option value="Software/SaaS" <?php echo ($company['industry']=='Software/SaaS')?'selected':'';?>>{{trans("my_company.Software/SaaS")}}</option>
                           <option value="Travel/leisure" <?php echo ($company['industry']=='Travel/leisure')?'selected':'';?>>{{trans("my_company.Travel/leisure")}}</option>
                        </select>
                        </label>
                    </section>

                    <section>
                        <label class="label">{{trans("my_company.Company Size")}}</label>
                        <label class="input">
                        <select name="size" class="form-control">
                           <option value="1-10" <?php echo ($company['size']=='1-10')?'selected':'';?>>1-10</option>
                           <option value="10-50" <?php echo ($company['size']=='10-50')?'selected':'';?>>10-50</option>
                           <option value="50-500" <?php echo ($company['size']=='50-500')?'selected':'';?>>50-500</option>
                           <option value="500+" <?php echo ($company['size']=='500+')?'selected':'';?>>500+</option>
                         </select>
                        </label>
                    </section>


                     <section>
                        <label class="label">{{trans("my_company.Address")}} *</label>
                        <label class="input">
                        <input type="text" name="address" placeholder="{{trans("my_company.Address")}}" value="{{ $company['address'] }}" />

                        </label>
                    </section>


                     <section>
                        <label class="label">{{trans("my_company.Zip Code")}}</label>
                        <label class="input">
                        <input type="text" name="zip" placeholder="{{trans("my_company.Zip Code")}}" value="{{ $company['zip'] }}" />

                        </label>
                    </section>


                    <div class="row">
                        <section class="col col-3">
                            <label class="label">{{trans("my_company.City")}}</label>
                            <label class="input">
                               <input type="text" class="form-control" name="city" placeholder="{{trans("my_company.City")}}" value="{{ $company['city'] }}" />
                            </label>
                        </section>
                        <section class="col col-3">
                            <label class="label">{{trans("my_company.Country")}} *</label>
                            <label class="input">


                            {!! Form::select('country', array
                                (
                                '' => 'Select Country',
                                    'AF' => 'Afghanistan',
                                    'AX' => 'Aland Islands',
                                    'AL' => 'Albania',
                                    'DZ' => 'Algeria',
                                    'AS' => 'American Samoa',
                                    'AD' => 'Andorra',
                                    'AO' => 'Angola',
                                    'AI' => 'Anguilla',
                                    'AQ' => 'Antarctica',
                                    'AG' => 'Antigua And Barbuda',
                                    'AR' => 'Argentina',
                                    'AM' => 'Armenia',
                                    'AW' => 'Aruba',
                                    'AU' => 'Australia',
                                    'AT' => 'Austria',
                                    'AZ' => 'Azerbaijan',
                                    'BS' => 'Bahamas',
                                    'BH' => 'Bahrain',
                                    'BD' => 'Bangladesh',
                                    'BB' => 'Barbados',
                                    'BY' => 'Belarus',
                                    'BE' => 'Belgium',
                                    'BZ' => 'Belize',
                                    'BJ' => 'Benin',
                                    'BM' => 'Bermuda',
                                    'BT' => 'Bhutan',
                                    'BO' => 'Bolivia',
                                    'BA' => 'Bosnia And Herzegovina',
                                    'BW' => 'Botswana',
                                    'BV' => 'Bouvet Island',
                                    'BR' => 'Brazil',
                                    'IO' => 'British Indian Ocean Territory',
                                    'BN' => 'Brunei Darussalam',
                                    'BG' => 'Bulgaria',
                                    'BF' => 'Burkina Faso',
                                    'BI' => 'Burundi',
                                    'KH' => 'Cambodia',
                                    'CM' => 'Cameroon',
                                    'CA' => 'Canada',
                                    'CV' => 'Cape Verde',
                                    'KY' => 'Cayman Islands',
                                    'CF' => 'Central African Republic',
                                    'TD' => 'Chad',
                                    'CL' => 'Chile',
                                    'CN' => 'China',
                                    'CX' => 'Christmas Island',
                                    'CC' => 'Cocos (Keeling) Islands',
                                    'CO' => 'Colombia',
                                    'KM' => 'Comoros',
                                    'CG' => 'Congo',
                                    'CD' => 'Congo, Democratic Republic',
                                    'CK' => 'Cook Islands',
                                    'CR' => 'Costa Rica',
                                    'CI' => 'Cote D\'Ivoire',
                                    'HR' => 'Croatia',
                                    'CU' => 'Cuba',
                                    'CY' => 'Cyprus',
                                    'CZ' => 'Czech Republic',
                                    'DK' => 'Denmark',
                                    'DJ' => 'Djibouti',
                                    'DM' => 'Dominica',
                                    'DO' => 'Dominican Republic',
                                    'EC' => 'Ecuador',
                                    'EG' => 'Egypt',
                                    'SV' => 'El Salvador',
                                    'GQ' => 'Equatorial Guinea',
                                    'ER' => 'Eritrea',
                                    'EE' => 'Estonia',
                                    'ET' => 'Ethiopia',
                                    'FK' => 'Falkland Islands (Malvinas)',
                                    'FO' => 'Faroe Islands',
                                    'FJ' => 'Fiji',
                                    'FI' => 'Finland',
                                    'FR' => 'France',
                                    'GF' => 'French Guiana',
                                    'PF' => 'French Polynesia',
                                    'TF' => 'French Southern Territories',
                                    'GA' => 'Gabon',
                                    'GM' => 'Gambia',
                                    'GE' => 'Georgia',
                                    'DE' => 'Germany',
                                    'GH' => 'Ghana',
                                    'GI' => 'Gibraltar',
                                    'GR' => 'Greece',
                                    'GL' => 'Greenland',
                                    'GD' => 'Grenada',
                                    'GP' => 'Guadeloupe',
                                    'GU' => 'Guam',
                                    'GT' => 'Guatemala',
                                    'GG' => 'Guernsey',
                                    'GN' => 'Guinea',
                                    'GW' => 'Guinea-Bissau',
                                    'GY' => 'Guyana',
                                    'HT' => 'Haiti',
                                    'HM' => 'Heard Island & Mcdonald Islands',
                                    'VA' => 'Holy See (Vatican City State)',
                                    'HN' => 'Honduras',
                                    'HK' => 'Hong Kong',
                                    'HU' => 'Hungary',
                                    'IS' => 'Iceland',
                                    'IN' => 'India',
                                    'ID' => 'Indonesia',
                                    'IR' => 'Iran, Islamic Republic Of',
                                    'IQ' => 'Iraq',
                                    'IE' => 'Ireland',
                                    'IM' => 'Isle Of Man',
                                    'IL' => 'Israel',
                                    'IT' => 'Italy',
                                    'JM' => 'Jamaica',
                                    'JP' => 'Japan',
                                    'JE' => 'Jersey',
                                    'JO' => 'Jordan',
                                    'KZ' => 'Kazakhstan',
                                    'KE' => 'Kenya',
                                    'KI' => 'Kiribati',
                                    'KR' => 'Korea',
                                    'KW' => 'Kuwait',
                                    'KG' => 'Kyrgyzstan',
                                    'LA' => 'Lao People\'s Democratic Republic',
                                    'LV' => 'Latvia',
                                    'LB' => 'Lebanon',
                                    'LS' => 'Lesotho',
                                    'LR' => 'Liberia',
                                    'LY' => 'Libyan Arab Jamahiriya',
                                    'LI' => 'Liechtenstein',
                                    'LT' => 'Lithuania',
                                    'LU' => 'Luxembourg',
                                    'MO' => 'Macao',
                                    'MK' => 'Macedonia',
                                    'MG' => 'Madagascar',
                                    'MW' => 'Malawi',
                                    'MY' => 'Malaysia',
                                    'MV' => 'Maldives',
                                    'ML' => 'Mali',
                                    'MT' => 'Malta',
                                    'MH' => 'Marshall Islands',
                                    'MQ' => 'Martinique',
                                    'MR' => 'Mauritania',
                                    'MU' => 'Mauritius',
                                    'YT' => 'Mayotte',
                                    'MX' => 'Mexico',
                                    'FM' => 'Micronesia, Federated States Of',
                                    'MD' => 'Moldova',
                                    'MC' => 'Monaco',
                                    'MN' => 'Mongolia',
                                    'ME' => 'Montenegro',
                                    'MS' => 'Montserrat',
                                    'MA' => 'Morocco',
                                    'MZ' => 'Mozambique',
                                    'MM' => 'Myanmar',
                                    'NA' => 'Namibia',
                                    'NR' => 'Nauru',
                                    'NP' => 'Nepal',
                                    'NL' => 'Netherlands',
                                    'AN' => 'Netherlands Antilles',
                                    'NC' => 'New Caledonia',
                                    'NZ' => 'New Zealand',
                                    'NI' => 'Nicaragua',
                                    'NE' => 'Niger',
                                    'NG' => 'Nigeria',
                                    'NU' => 'Niue',
                                    'NF' => 'Norfolk Island',
                                    'MP' => 'Northern Mariana Islands',
                                    'NO' => 'Norway',
                                    'OM' => 'Oman',
                                    'PK' => 'Pakistan',
                                    'PW' => 'Palau',
                                    'PS' => 'Palestinian Territory, Occupied',
                                    'PA' => 'Panama',
                                    'PG' => 'Papua New Guinea',
                                    'PY' => 'Paraguay',
                                    'PE' => 'Peru',
                                    'PH' => 'Philippines',
                                    'PN' => 'Pitcairn',
                                    'PL' => 'Poland',
                                    'PT' => 'Portugal',
                                    'PR' => 'Puerto Rico',
                                    'QA' => 'Qatar',
                                    'RE' => 'Reunion',
                                    'RO' => 'Romania',
                                    'RU' => 'Russian Federation',
                                    'RW' => 'Rwanda',
                                    'BL' => 'Saint Barthelemy',
                                    'SH' => 'Saint Helena',
                                    'KN' => 'Saint Kitts And Nevis',
                                    'LC' => 'Saint Lucia',
                                    'MF' => 'Saint Martin',
                                    'PM' => 'Saint Pierre And Miquelon',
                                    'VC' => 'Saint Vincent And Grenadines',
                                    'WS' => 'Samoa',
                                    'SM' => 'San Marino',
                                    'ST' => 'Sao Tome And Principe',
                                    'SA' => 'Saudi Arabia',
                                    'SN' => 'Senegal',
                                    'RS' => 'Serbia',
                                    'SC' => 'Seychelles',
                                    'SL' => 'Sierra Leone',
                                    'SG' => 'Singapore',
                                    'SK' => 'Slovakia',
                                    'SI' => 'Slovenia',
                                    'SB' => 'Solomon Islands',
                                    'SO' => 'Somalia',
                                    'ZA' => 'South Africa',
                                    'GS' => 'South Georgia And Sandwich Isl.',
                                    'ES' => 'Spain',
                                    'LK' => 'Sri Lanka',
                                    'SD' => 'Sudan',
                                    'SR' => 'Suriname',
                                    'SJ' => 'Svalbard And Jan Mayen',
                                    'SZ' => 'Swaziland',
                                    'SE' => 'Sweden',
                                    'CH' => 'Switzerland',
                                    'SY' => 'Syrian Arab Republic',
                                    'TW' => 'Taiwan',
                                    'TJ' => 'Tajikistan',
                                    'TZ' => 'Tanzania',
                                    'TH' => 'Thailand',
                                    'TL' => 'Timor-Leste',
                                    'TG' => 'Togo',
                                    'TK' => 'Tokelau',
                                    'TO' => 'Tonga',
                                    'TT' => 'Trinidad And Tobago',
                                    'TN' => 'Tunisia',
                                    'TR' => 'Turkey',
                                    'TM' => 'Turkmenistan',
                                    'TC' => 'Turks And Caicos Islands',
                                    'TV' => 'Tuvalu',
                                    'UG' => 'Uganda',
                                    'UA' => 'Ukraine',
                                    'AE' => 'United Arab Emirates',
                                    'GB' => 'United Kingdom',
                                    'US' => 'United States',
                                    'UM' => 'United States Outlying Islands',
                                    'UY' => 'Uruguay',
                                    'UZ' => 'Uzbekistan',
                                    'VU' => 'Vanuatu',
                                    'VE' => 'Venezuela',
                                    'VN' => 'Viet Nam',
                                    'VG' => 'Virgin Islands, British',
                                    'VI' => 'Virgin Islands, U.S.',
                                    'WF' => 'Wallis And Futuna',
                                    'EH' => 'Western Sahara',
                                    'YE' => 'Yemen',
                                    'ZM' => 'Zambia',
                                    'ZW' => 'Zimbabwe',
                                ), $company['country'], ['class' => 'form-control']) !!}



                            </label>
                        </section>
                        <section class="col col-3">
                            <label class="label">{{trans("my_company.State")}}</label>
                            <label class="input">
                                <input type="text" class="form-control" name="state" placeholder="{{trans("my_company.State")}}" value="{{ $company['state'] }}" />
                            </label>
                        </section>
                    </div>

                    <section>
                        <label class="label">{{trans("my_company.Phone Number")}}</label>
                        <label class="input">

                        <input type="text" name="phone_number" placeholder="{{trans("my_company.Phone Number")}}" value="{{ $company['phone_number'] }}" />

                        </label>
                    </section>

                    <section>

                        <label class="label">{{trans("my_company.Company Logo")}}</label>

                        <div id="photoCont_logo" >

                            @if($company['logo'])
                                <img src="{{$company['logo']}}" />
                            @endif
                            <br />

                        </div>

                        <label class="input">

                            <br class="clr" />

                            <input type="file" class="form-control" name="file_upload" id="pic_upload"/>

                            <input type="text" name="logo" id="photo_value" class="hide"/>

                        </label>


                    </section>


                </fieldset>

                <footer>
                    <button type="submit" class="btn btn-primary">
                        {{trans("my_company.Update")}}
                    </button>
                </footer>

            </form>

        </article>

    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->


<script type="text/javascript">
    $(document).ready(function(){

        $('#pic_upload').uploadifive({
            'buttonText' : '<i class="fa fa-upload"></i> Upload Company Logo',
            'uploadScript' : '{{url('/upload_company_pic')}}',
            'fileType' : 'image/*',
            'fileSizeLimit' : '5MB',
            'width' : 160,
            'multi':false,
            'onUploadComplete' : function(file, data, response) {

                $('#photo_value').val(data);

                $('#pic_upload').uploadifive('clearQueue');

                var img = '<img src="';
                img = img + data + '?version=<?php echo time();?>" />';
                $('#photoCont_logo').html(img);

            },
            'formData'         : {
                'company_id' : '{{$company['id']}}',
                '_token':'{{ csrf_token() }}'}
        });


    });
</script>
@endsection