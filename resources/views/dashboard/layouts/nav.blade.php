<?php
if(isset($user) AND $user['completed_profile'] == 2):?>

<?php

 /*
  *
  *     DEBUG CODES : don't remove for now as it is important as of this stage
        dump($user_roles);

        1 Edit Company / Role / Team
        2 View Company / Role / Team
        3 Assign Roles
        4 Assign Teams
        5 Edit Employees
        6 Edit General Permissions

        $edit_crt = TRUE;
        $view_crt = TRUE;
        $assign_roles = TRUE;
        $assign_teams = TRUE;
        $edit_employees = TRUE;
        $edit_general_permissions = TRUE;
         echo '<p>$edit_crt</p>';
        dump($edit_crt);

         echo '<p>$view_crt</p>';
        dump($view_crt);
         echo '<p>$assign_roles</p>';
        dump($assign_roles);
         echo '<p>$assign_teams</p>';
        dump($assign_teams);
         echo '<p>$edit_employees</p>';
        dump($edit_employees);
         echo '<p>$edit_general_permissions</p>';
        dump($edit_general_permissions);
 */

?>
<nav>
    <ul>
        <li <?=($action=='dashboard')?'class="active"':'';?>>
            <a href="{{ url('/dashboard') }}" title="Dashboard">
            <img src="<?php echo url('/');?>/img/tools-icon/dashboard.png" alt="me" class="menu_icon"/>
            <span class="menu-item-parent">{{trans("dashboard_layouts_nav.Dashboard")}}</span></a>
        </li>
        @if(!$is_guest)

        <li <?=($action=='directory' OR $action=='invite_user' OR $action=='user_view')?'class="active"':'';?>>
            <a href="{{ url('/directory') }}">
                <img src="<?php echo url('/');?>/img/tools-icon/directory.png" alt="me" class="menu_icon"/>
                <span class="menu-item-parent">{{trans("dashboard_layouts_nav.Directory")}}</span>
            </a>
        </li>


        @if(isset($company) AND $company['file_sharing']==1)
        <li <?=($action=='files_manager') ? 'class="active"' : ''; ?> >
            <a href="{{ url('/files_manager') }}">
             <img src="<?php echo url('/');?>/img/tools-icon/files.png" alt="me" class="menu_icon"/>
             <span class="menu-item-parent">{{trans("dashboard_layouts_nav.Files")}}</span></a>
        </li>
        @endif

        @if(isset($company) AND $company['contact_setting']==1)
        <li class=" ">
            <a href="{{ url('/contact') }}" >
                <img src="<?php echo url('/');?>/img/tools-icon/contacts.png" alt="me" class="menu_icon"/>
                <span class="menu-item-parent">{{trans("dashboard_layouts_nav.Contacts")}}</span>
            </a>
        </li>
        @endif

        @if(isset($company) AND $company['crm_setting']==1)
        <li <?=($action=='crm' OR $action=='deal_view')?'class="active"':'';?>>
            <a href="{{url('/crm-board')}}" >
                <img src="<?php echo url('/');?>/img/tools-icon/crn.png" alt="me" class="menu_icon"/>
                <span class="menu-item-parent">{{trans("dashboard_layouts_nav.CRM")}}</span></a>
        </li>
        @endif
        @if(isset($company))
        <li <?=($action=='notes_view' || $action=='notes')?'class="active"':'';?>>
            <a href="{{ url('/notes_view') }}">
                <img src="<?php echo url('/');?>/img/tools-icon/notes-icon.png" alt="me" class="menu_icon"/>
                <span class="menu-item-parent">{{ trans("dashboard_layouts_nav.Notes") }}</span>
                </a>
        </li>
        @endif


        @if((isset($company) AND $company['asset_management']==1) AND ( $user->hasPermission('view_asset_management') OR $user->hasPermission('edit_asset_management') ))
        <li <?=($action=='asset_management')?'class="active"':'';?>>
            <a href="{{ url('/asset_management') }}">
                <img src="<?php echo url('/'); ?>/img/asset_management_icon_b.png" alt="me" class="menu_icon" />
                <span class="menu-item-parent">{{ trans("dashboard_layouts_nav.Assets")}}</span></a>
        </li>
        @endif

        


       <?php if( isset($company) AND $company['expenses']==1):?>

            <li class=" ">
                <a href="#" >
                    <img src="<?php echo url('/');?>/img/tools-icon/expenses.png" alt="me" class="menu_icon"/>
                    <span class="menu-item-parent">{{trans("dashboard_layouts_nav.Expenses")}}</span></a>
                <ul>
                    <li <?=($action=='my-expenses' OR $action=='expense-item')?'class="active"':'';?>>
                        <a href="{{ url('/my-expenses') }}">{{trans("dashboard_layouts_nav.My Expenses")}} </a>
                    </li>
                    <?php if( $approve_expenses  OR $action=='approve-expenses-item'): ?>
                    <li <?=($action=='approve-expenses' OR $action=='approve-expenses-item')?'class="active"':'';?>>
                        <a href="{{ url('approve-expenses') }}">{{trans("dashboard_layouts_nav.Approve Expenses")}}</a>
                    </li>
                    <?php endif; ?>
                    <?php if( $pay_expenses): ?>
                    <li <?=($action=='pay-expenses' OR $action=='pay-expenses-item')?'class="active"':'';?>>
                        <a href="{{ url('pay-expenses') }}">{{trans("dashboard_layouts_nav.Pay Expenses")}}</a>
                    </li>
                    <li <?=($action=='expenses-history')?'class="active"':'';?>>
                        <a href="{{ url('expenses-history') }}">{{trans("dashboard_layouts_nav.History")}}</a>
                    </li>
                    <li <?=($action=='expenses-report')?'class="active"':'';?>>
                        <a href="{{ url('expenses-report') }}">{{trans("dashboard_layouts_nav.Reports")}}</a>
                    </li>
                    <?php endif; ?>
                </ul>
            </li>

       <?php endif;?>

    @if( isset($company) AND $company['training']==1 )

        <li class=" ">
            <a href="#" ><i class="fa fa-lg fa-fw fa-graduation-cap"></i>
                <span class="menu-item-parent">{{trans("dashboard_layouts_nav.Trainings")}}</span></a>
            <ul>
                <li <?=($action=='trainings')?'class="active"':'';?>>
                    <a href="{{ url('trainings') }}">{{trans("dashboard_layouts_nav.Trainings")}}</a>
                </li>
                @if( $manage_trainings )
                    <li <?=($action=='all-topics')?'class="active"':'';?>>
                        <a href="{{ url('all-topics') }}">{{trans("dashboard_layouts_nav.Manage Trainings")}}</a>
                    </li>
                @endif
            </ul>
        </li>

    @endif

    @if( isset($company) AND $company['task_setting']==1 )
		<li <?=($action=='boards' OR $action=='tasks')?'class="active"':'';?>>
			<a href="{{url('/boards')}}" >
				<img src="<?php echo url('/');?>/img/tools-icon/directory.png" alt="me" class="menu_icon"/>
				<span class="menu-item-parent">Tasks</span></a>
		</li>
            <!--
        <li <?=($action=='projects' OR $action=='people-view')?'class="active"':'';?>>
    		<a href="{{ url('projects') }}" >
                <i class="fa fa-lg fa-fw fa-graduation-cap"></i>
                <span class="menu-item-parent">Projects</span></a>
        </li>-->
    @endif

    @if( isset($company) AND $company['payroll']==1  AND ($company['country'] =='SG' OR $company['country'] =='Singapore'))

        <li <?=
        ($action=='payroll-manage-employees'
        OR $action=='payroll-update-employee'
        OR $action=='payroll-new-employee'
        OR $action == 'process_payroll'
        OR $action == 'show-payslip-item'
        OR $action =='payroll-item'
        OR $action == 'payroll-item-employee'
        )?'class="active"':'';?>>
            <a href="#" >
                <i class="fa fa-fw fa-tag"></i>
                <span class="menu-item-parent">Payroll</span></a>
            <ul>
                <li <?=($action=='my-payslips' OR $action == 'show-payslip-item')?'class="active"':'';?>>
                    <a href="{{ url('my-payslips') }}">My Payslips</a>
                </li>

                @if($manage_payroll)
                    <li <?=($action=='payroll-company')?'class="active"':'';?>>
                        <a href="{{ url('payroll-company') }}">Company</a>
                    </li>
                    <li <?=($action=='payroll-manage-employees' OR $action=='payroll-update-employee' OR $action=='payroll-new-employee')?'class="active"':'';?>>
                        <a href="{{ url('payroll-manage-employees') }}">Manage Employees</a>
                    </li>
                    <li <?=($action=='process_payroll' )?'class="active"':'';?>>
                        <a href="{{ url('process_payroll') }}">Process</a>
                    </li>
                    <li <?=($action=='payroll-history' OR $action =='payroll-item' OR $action=='payroll-item-employee')?'class="active"':'';?>>
                        <a href="{{ url('payroll-history') }}">History</a>
                    </li>
                    <li <?=($action=='cpf-reports')?'class="active"':'';?>>
                        <a href="{{ url('cpf_reports') }}">CPF Reports</a>
                    </li>
                @endif

            </ul>
        </li>
    @endif

<!--
     @if( isset($company) AND $company['leaves']==1 )
        <li class=" ">
            <a href="#" >
                <img src="<?php echo url('/');?>/img/tools-icon/calendar-wyt.png" alt="me" class="menu_icon"/>
                <span class="menu-item-parent">{{trans("dashboard_layouts_nav.Leaves")}}</span></a>
            <ul>
                <li <?=($action=='apply-leave')?'class="active"':'';?>>
                    <a href="{{ url('apply-leave') }}">{{trans("dashboard_layouts_nav.Apply")}}</a>
                </li>

                    <li <?=($action=='leaves-history')?'class="active"':'';?>>
                        <a href="{{ url('leaves-history') }}">{{trans("dashboard_layouts_nav.History")}}</a>
                    </li>

                    <li <?=($action=='leaves-account')?'class="active"':'';?>>
                        <a href="{{ url('leaves-account') }}">{{trans("dashboard_layouts_nav.My Account")}}</a>
                    </li>

                    @if($manage_leaves OR $approve_leaves)
                    <li <?=($action=='approve-leaves')?'class="active"':'';?>>
                        <a href="{{ url('approve-leaves') }}">{{trans("dashboard_layouts_nav.Approve")}}</a>
                    </li>

                    <li <?=($action=='leaves-summary')?'class="active"':'';?>>
                        <a href="{{ url('leaves-summary') }}">{{trans("dashboard_layouts_nav.Summary")}}</a>
                    </li>

                    @endif


                    @if($view_calendar)
                    <li <?=($action=='leaves_calendar')?'class="active"':'';?>>
                        <a href="{{ url('leaves_calendar') }}">{{trans("dashboard_layouts_nav.Calendar")}}</a>
                    </li>
                    @endif

                    @if($is_admin)
                    <li <?=($action=='all_leaves' )?'class="active"':'';?>>
                        <a href="{{ url('/all_leaves') }}">{{trans("leaves.All Leaves")}}</a>
                    </li>
                    @endif

                </ul>
            </li>

        @endif

        -->

        @if( isset($company) AND $company['leaves']==1 )
        <li class=" ">
            <a href="#" >
                <img src="<?php echo url('/');?>/img/tools-icon/calendar-wyt.png" alt="me" class="menu_icon"/>
                <span class="menu-item-parent">{{trans("dashboard_layouts_nav.Leaves")}}</span></a>
            <ul>


                    <li <?=($action=='leaves-my-account')?'class="active"':'';?>>
                        <a href="{{ url('leaves-my-account') }}">{{trans("dashboard_layouts_nav.My Account")}}</a>
                    </li>

                    @if($is_admin )
                    <li <?=($action=='manage-leaves')?'class="active"':'';?>>
                        <a href="{{ url('manage-leaves') }}">Manage Leaves</a>
                    </li>
                    @endif

                </ul>
            </li>

        @endif


    @else

        <li <?=($action=='chats')?'class="active"':'';?>>
            <a href="{{ url('/chats') }}" title="Dashboard">
            <img src="<?php echo url('/');?>/img/tools-icon/chat-icon.png" alt="me" class="menu_icon"/>
            <span class="menu-item-parent">{{trans("dashboard_layouts_nav.Chats")}}</span></a>
        </li>

    @endif

        


    </ul>
</nav>
<?php endif;?>