<table class="table table-striped">

    <tr>
        <th></th>

        @foreach($permissions as $permission)
            <th>{{$permission['feature']}}</th>
        @endforeach

    </tr>

    @foreach($roles as $role)

        <tr>
            <td>
            {{$role->role_name}}
            </td>

            @foreach($permissions as $permission)

                <td>
                    <input type="checkbox" name="user_roles_permissions[]"

                    <?php echo ($role['role_name'] =='Administrator')?'checked':'';?>
                    <?php echo ($role['role_name'] =='Administrator')?'disabled':'';?>

                    value="{{$role['id']}}-{{$permission['id']}}"
                     <?php if(count($role->role_permissions) AND in_array($permission['id'],$role->role_permissions->pluck('id')->toArray())):?>
                        checked="checked"
                     <?php endif;?>

                     />
                </td>
            @endforeach

        </tr>

    @endforeach

</table>