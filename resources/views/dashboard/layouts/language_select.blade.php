<!-- multiple lang dropdown : find all flags in the flags page -->
<ul class="header-dropdown-list hidden-xs" style="display:none;">
	<li>
	<?php $language = config('app.languages')[\App::getLocale()]; ?>
		<a href="#" class="dropdown-toggle" data-toggle="dropdown"> <img src="<?php echo url('/');?>/img/blank.gif" class="flag flag-{{$language['flag_code']}}" alt="{{$language['display']}}"> <span> {{$language['display']}}</span> <i class="fa fa-angle-down"></i> </a>
		<ul class="dropdown-menu pull-right">
			@foreach(config('app.languages') as $key => $lang)
				@if($key === \App::getLocale())
					<li class="active">
						<a style="pointer:cursor;">
				@else
					<li>
						<a onclick="changeLang('{{$key}}')" style="pointer:cursor;">
				@endif
							<img src="<?php echo url('/');?>/img/blank.gif" class="flag flag-{{$lang['flag_code']}}" alt="{{$lang['display']}}"> {{$lang['display']}}
						</a>
					</li>
			@endforeach

		</ul>
	</li>
</ul>
<!-- end multiple lang -->