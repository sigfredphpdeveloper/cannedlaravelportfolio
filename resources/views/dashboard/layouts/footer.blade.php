
@if( !Auth::check() )
<div class="page-footer">
			<div class="row">
				<div class="col-xs-12 col-sm-6">
					<span class="txt-color-white">Zen Intra 1.7.0 <span class="hidden-xs"> - Web Application Framework</span> © 2014-2015</span>
				</div>

				<div class="col-xs-6 col-sm-6 text-right hidden-xs">
					<div class="txt-color-white inline-block">
						<i class="txt-color-blueLight hidden-mobile">Last account activity <i class="fa fa-clock-o"></i> <strong>52 mins ago &nbsp;</strong> </i>
						<div class="btn-group dropup">
							<button class="btn btn-xs dropdown-toggle bg-color-blue txt-color-white" data-toggle="dropdown">
								<i class="fa fa-link"></i> <span class="caret"></span>
							</button>
							<ul class="dropdown-menu pull-right text-left">
								<li>
									<div class="padding-5">
										<p class="txt-color-darken font-sm no-margin">Download Progress</p>
										<div class="progress progress-micro no-margin">
											<div class="progress-bar progress-bar-success" style="width: 50%;"></div>
										</div>
									</div>
								</li>
								<li class="divider"></li>
								<li>
									<div class="padding-5">
										<p class="txt-color-darken font-sm no-margin">Server Load</p>
										<div class="progress progress-micro no-margin">
											<div class="progress-bar progress-bar-success" style="width: 20%;"></div>
										</div>
									</div>
								</li>
								<li class="divider"></li>
								<li>
									<div class="padding-5">
										<p class="txt-color-darken font-sm no-margin">Memory Load <span class="text-danger">*critical*</span></p>
										<div class="progress progress-micro no-margin">
											<div class="progress-bar progress-bar-danger" style="width: 70%;"></div>
										</div>
									</div>
								</li>
								<li class="divider"></li>
								<li>
									<div class="padding-5">
										<button class="btn btn-block btn-default">refresh</button>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END PAGE FOOTER -->
@endif


@if(Session::has('success_mail'))
<script>
    $(document).ready(function(){
        $('#groove_sent').modal('show');
    })
</script>
@endif

@if(Session::has('success_mail'))
	<!-- Add Groove success Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="groove_sent">
    <div class="modal-dialog">
        <div class="modal-content">
            <div role="content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"> Support Email </h4>
                </div>

                <div class="modal-body">
                    <div class="message-field">
                        <h5>Your message has been sent, our support team will get back to you as soon as possible</h5>
                    </div>
                    <button type="button" class="btn btn-default dismiss-support" data-dismiss="modal" style="">OK</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Groove success Modal -->
@endif

<script type="text/javascript">
	$(document).ready(function($){

		$('#support_trigger').on('click', function(e){
			zen_ajax({
                type:'GET',
                url: base_url+'/support-form'
            }).done(function(data){
                    var modalClass = ".modal";
                    $(modalClass).remove();
                    $("body").append(data);
                    $(modalClass).modal('show');
                }).fail(function(xhr) {
                    console.log('Something went wrong. Please try again later.');
                });
		});


		
	});
</script>

@if(Session::has('success_mail'))
<script type="text/javascript">
	$(document).ready(function($){

		$('#groove_sent').modal('show');

		
	});
</script>
@endif

		<!-- SHORTCUT AREA : With large tiles (activated via clicking user name tag)
		Note: These tiles are completely responsive,
		you can add as many as you like
		-->
		<div id="shortcut">
			<ul>
				<li>
					<a href="inbox.html" class="jarvismetro-tile big-cubes bg-color-blue"> <span class="iconbox"> <i class="fa fa-envelope fa-4x"></i> <span>Mail <span class="label pull-right bg-color-darken">14</span></span> </span> </a>
				</li>
				<li>
					<a href="calendar.html" class="jarvismetro-tile big-cubes bg-color-orangeDark"> <span class="iconbox"> <i class="fa fa-calendar fa-4x"></i> <span>Calendar</span> </span> </a>
				</li>
				<li>
					<a href="gmap-xml.html" class="jarvismetro-tile big-cubes bg-color-purple"> <span class="iconbox"> <i class="fa fa-map-marker fa-4x"></i> <span>Maps</span> </span> </a>
				</li>
				<li>
					<a href="invoice.html" class="jarvismetro-tile big-cubes bg-color-blueDark"> <span class="iconbox"> <i class="fa fa-book fa-4x"></i> <span>Invoice <span class="label pull-right bg-color-darken">99</span></span> </span> </a>
				</li>
				<li>
					<a href="gallery.html" class="jarvismetro-tile big-cubes bg-color-greenLight"> <span class="iconbox"> <i class="fa fa-picture-o fa-4x"></i> <span>Gallery </span> </span> </a>
				</li>
				<li>
					<a href="profile.html" class="jarvismetro-tile big-cubes selected bg-color-pinkDark"> <span class="iconbox"> <i class="fa fa-user fa-4x"></i> <span>My Profile </span> </span> </a>
				</li>
			</ul>
		</div>
		<!-- END SHORTCUT AREA -->

		<!--================================================== -->

		<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->

		<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
		<script>
			if (!window.jQuery) {
				document.write('<script src="{{ asset('js/libs/jquery-2.1.1.min.js') }}"><\/script>');
			}
		</script>

		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="{{ asset('js/libs/jquery-ui-1.10.3.min.js') }}"><\/script>');
			}
		</script>

		<!-- IMPORTANT: APP CONFIG -->
		<script src="{{ asset('js/app.config.js') }}"></script>

		<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
		<script src="{{ asset('js/plugin/jquery-touch/jquery.ui.touch-punch.min.js') }}"></script>



		<!-- CUSTOM NOTIFICATION -->
		<script src="{{ asset('js/notification/SmartNotification.min.js') }}"></script>

		<!-- JARVIS WIDGETS -->
		<script src="{{ asset('js/smartwidgets/jarvis.widget.min.js') }}"></script>

		<!-- EASY PIE CHARTS -->
		<script src="{{ asset('js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js') }}"></script>

		<!-- SPARKLINES -->
		<script src="{{ asset('js/plugin/sparkline/jquery.sparkline.min.js') }}"></script>

		<!-- JQUERY VALIDATE -->
		<script src="{{ asset('js/plugin/jquery-validate/jquery.validate.min.js') }}"></script>

		<!-- JQUERY MASKED INPUT -->
		<script src="{{ asset('js/plugin/masked-input/jquery.maskedinput.min.js') }}"></script>

		<!-- JQUERY SELECT2 INPUT -->
		<script src="{{ asset('js/plugin/select2/select2.min.js') }}"></script>

		<!-- JQUERY UI + Bootstrap Slider -->
		<script src="{{ asset('js/plugin/bootstrap-slider/bootstrap-slider.min.js') }}"></script>

		<!-- browser msie issue fix -->
		<script src="{{ asset('js/plugin/msie-fix/jquery.mb.browser.min.js') }}"></script>

		<!-- FastClick: For mobile devices -->
		<script src="{{ asset('js/plugin/fastclick/fastclick.min.js') }}"></script>

		<!--[if IE 8]>

		<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

		<![endif]-->

		<!-- MAIN APP JS FILE -->
		<script src="{{ asset('js/app.min.js') }}"></script>

		<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
		<!-- Voice command : plugin -->
		<script src="{{ asset('js/speech/voicecommand.min.js') }}"></script>

		<!-- SmartChat UI : plugin -->
		<script src="{{ asset('js/smart-chat-ui/smart.chat.ui.min.js') }}"></script>
		<script src="{{ asset('js/smart-chat-ui/smart.chat.manager.min.js') }}"></script>

		<!-- PAGE RELATED PLUGIN(S) -->

		<!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
		<script src="{{ asset('js/plugin/flot/jquery.flot.cust.min.js') }}"></script>
		<script src="{{ asset('js/plugin/flot/jquery.flot.resize.min.js') }}"></script>
		<script src="{{ asset('js/plugin/flot/jquery.flot.time.min.js') }}"></script>
		<script src="{{ asset('js/plugin/flot/jquery.flot.tooltip.min.js') }}"></script>

		<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
		<script src="{{ asset('js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
		<script src="{{ asset('js/plugin/vectormap/jquery-jvectormap-world-mill-en.js') }}"></script>

		<!-- Full Calendar -->
		<script src="{{ asset('js/plugin/moment/moment.min.js') }}"></script>
		<script src="{{ asset('js/plugin/fullcalendar/jquery.fullcalendar.min.js') }}"></script>

		<!-- Socket IO -->
		<script src="{{ asset('js/plugin/socket.io/socket.io.min.js') }}"></script>

		<!-- rivets + sightglass-->
		<script src="{{ asset('js/plugin/rivets/rivets.bundled.min.js') }}"></script>

		<!-- lodash -->
		<script src="{{ asset('js/plugin/lodash/lodash.min.js') }}"></script>

		<!-- dropzone -->
		<script src="{{ asset('/js/dropzone.js') }}"></script>

		<script>
			//SET JS CONFIG + KEYS
			var csrf_token = '{{ csrf_token() }}';
			var zen_config = {
				base_url:'{{ config("app.fe_base_url") }}',
				chat_base_url:'{{ config("app.fe_chat_base_url") }}'
			};
			var current_user = {
				id: {{ Auth::user()->id }},
				first_name: '{{ Auth::user()->first_name }}',
				last_name: '{{ Auth::user()->last_name }}',
				photo: '{{ Auth::user()->photo }}'
			};
			var current_company = {
				id: {{session('current_company')->id}}
			}
			
			var zen_ajax = function(ajax_config){
				//if using default method (get) or specifying get method, don't add csrf token
				if(!ajax_config.method || ajax_config.method.toUpperCase() == 'GET'){
					return $.ajax(ajax_config);
				}

				//otherwise set the crsf token in the headers and send the request
				if(!ajax_config.headers){
					ajax_config.headers = {};	
				}
				ajax_config.headers['X-CSRF-TOKEN'] = csrf_token;

				return $.ajax(ajax_config);
			}

			var socket = io.connect(zen_config.chat_base_url, {
				secure:true,
				'query': 'token=' + $('#jwt').attr('content')+"&company_id="+current_company.id
			});

			socket.on("error", function(error) {
				if (error.type == "UnauthorizedError" || error.code == "invalid_token") {
					// redirect user to login page perhaps?
					console.error("Invalid User Token, it may be missing or expired");
				}
				console.error(error);
			});

			function changeLang(code){
				zen_ajax({
					url:zen_config.base_url + "user/changeLang",
					method:"POST",
					data:{code:code}
				})
				.done(function(data){
					console.log(data);
					window.location.reload(false);
				})
				.fail(function(err){
					console.error("Error changing language:\n", err);
				});
			}

			$(document).ready(function() {

				// DO NOT REMOVE : GLOBAL FUNCTIONS!
				pageSetUp();


				// check and uncheck
				$('.todo .checkbox > input[type="checkbox"]').click(function() {
					var $this = $(this).parent().parent().parent();

					if ($(this).prop('checked')) {
						$this.addClass("complete");

						// remove this if you want to undo a check list once checked
						//$(this).attr("disabled", true);
						$(this).parent().hide();

						// once clicked - add class, copy to memory then remove and add to sortable3
						$this.slideUp(500, function() {
							$this.clone().prependTo("#sortable3").effect("highlight", {}, 800);
							$this.remove();
							countTasks();
						});
					} else {
						// insert undo code here...
					}

				})


				var $on = false;

                $('.tree > ul').attr('role', 'tree').find('ul').attr('role', 'group');
                $('.tree').find('li:has(ul)').addClass('parent_li').attr('role', 'treeitem').find(' > span').attr('title', 'Collapse this branch').on('click', function(e) {
                    var children = $(this).parent('li.parent_li').find(' > ul > li');
                    if (children.is(':visible')) {
                        children.hide('fast');
                        $(this).attr('title', 'Expand this branch').find(' > i').removeClass().addClass('fa fa-lg fa-plus-circle');
                    } else {
                        children.show('fast');
                        $(this).attr('title', 'Collapse this branch').find(' > i').removeClass().addClass('fa fa-lg fa-minus-circle');
                    }
                    e.stopPropagation();
                });

                $('datepicker').datepicker({
                    dateFormat: 'yy-mm-dd'
                });

                $('.datepicker').datepicker({
                    dateFormat: 'yy-mm-dd',
                    prevText: '<i class="fa fa-chevron-left"></i>',
                    nextText: '<i class="fa fa-chevron-right"></i>',
                    onSelect: function (selectedDate) {
                        $('#finishdate').datepicker('option', 'minDate', selectedDate);
                    }
                });


			});

		</script>

		<!-- Your GOOGLE ANALYTICS CODE Below -->
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script');
				ga.type = 'text/javascript';
				ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(ga, s);
			})();

		</script>
