<!DOCTYPE html>
<html lang="en-us">
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title> Zenintra.net </title>
		<meta name="description" content="">
		<meta name="author" content="">
		<meta id="jwt" name="jwt" content="<?php echo session('jwt') ?>">

		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/bootstrap.min.css') }}">
		<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/font-awesome.min.css') }}">
		<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

		<!-- SmartAdmin Styles : Caution! DO NOT change the order -->
		<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/smartadmin-production-plugins.min.css') }}">
		<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/smartadmin-production.min.css') }}">
		<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/smartadmin-skins.min.css') }}">

		<!-- SmartAdmin RTL Support  -->
		<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/smartadmin-rtl.min.css') }}">

		<!-- dropzone -->
		<link rel="stylesheet" href="{{ asset('/css/dropzone.css') }}">

		<!-- We recommend you use "your_style.css" to override SmartAdmin
		     specific styles this will also ensure you retrain your customization with each SmartAdmin update.
		-->
		<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/your_style.css') }}">
		<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/zenintra-dashboard.css') }}">



		@include('frontend.includes.favicon')

		<!-- GOOGLE FONT -->
		<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

		<!-- Specifying a Webpage Icon for Web Clip
			 Ref: //developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
		<link rel="apple-touch-icon" href="{{ asset('img/splash/sptouch-icon-iphone.png') }}">
		<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('img/splash/touch-icon-ipad.png') }}">
		<link rel="apple-touch-icon" sizes="120x120" href="{{ asset('img/splash/touch-icon-iphone-retina.png') }}">
		<link rel="apple-touch-icon" sizes="152x152" href="{{ asset('img/splash/touch-icon-ipad-retina.png') }}">

		<!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">

		<!-- Startup image for web apps -->
		<link rel="apple-touch-startup-image" href="{{ asset('img/splash/ipad-landscape.png') }}" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
		<link rel="apple-touch-startup-image" href="{{ asset('img/splash/ipad-portrait.png') }}" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
		<link rel="apple-touch-startup-image" href="{{ asset('img/splash/iphone.png') }}" media="screen and (max-device-width: 320px)">

		<!-- Autocomplete -->
		<link href="//code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
      	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
      	<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <!-- BOOTSTRAP JS -->
        <script src="{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
        <link rel="manifest" href="{{ asset('js/manifest.json')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/uploadifive.css') }}" />
        <script type="text/javascript" src="{{ asset('js/jquery.uploadifive.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('js/global.js') }}" ></script>

        <!-- DataTable Plugin -->
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
        <!--Date Time Plugin -->
        <script type="text/javascript" src="{{ asset('js/jquery.date-dropdowns.min.js') }}"></script>

        <style type="text/css">
            .content{
              padding:15px;
            }

            .panel-default>.panel-heading {
                color: #333;
                background-color: #FADCB4;
                border-color: #F7DCB4;
            }
            .nav-tabs>li.active>a{
                box-shadow: 0 -2px 0 #EA8E52;
            }

            .table>thead>tr>th {
                vertical-align: bottom;
                border-bottom: 2px solid #E67D44;
            }

            .fc-border-separate thead tr, .table thead tr {
                background: #F7DCB4 !important;
                /*background-image: -webkit-gradient(linear,0 0,0 100%,from(#F7DCB4),to(#fafafa));
                background-image: -webkit-linear-gradient(top,#F7DCB4 0,#fafafa 100%);
                background-image: -moz-linear-gradient(top,#F7DCB4 0,#fafafa 100%);
                background-image: -ms-linear-gradient(top,#F7DCB4 0,#fafafa 100%);
                background-image: -o-linear-gradient(top,#F7DCB4 0,#fafafa 100%);
                background-image: -linear-gradient(top,#F7DCB4 0,#fafafa 100%);*/
            }



        </style>
        @yield('styles')
	</head>


	<body class="" data-site-url="{{ url('/') }}/">
		@include('frontend.includes.google_tag_manager')
		<!-- HEADER -->
		<header id="header" style="height:85px;" >
			<div id="logo-group">

				<!-- PLACE YOUR LOGO HERE -->
				<span id="logo">

				@if(isset($company) AND $company['logo'] == '')
				<a href="{{url('/dashboard')}}">
					<h4 style="font-weight:bold;height:67px;width:auto">{{$company['company_name']}}</h4>
				</a>
                   <!-- <img src="<?php echo url('/');?>/img/zen.jpg" alt="SmartAdmin">-->
				@else
				    <a href="{{url('/dashboard')}}">
			    	<img src="{{$company['logo']}}" style="max-height:67px;"/>
			    	</a>
				@endif

				</span>
				<!-- END LOGO PLACEHOLDER -->

			</div>



			<!-- pulled right: nav area -->
			<div class="pull-right">

				<!-- collapse menu button -->
				<div id="hide-menu" class="btn-header pull-right" style="display:none;">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- end collapse menu -->

				<!-- #MOBILE -->
				<!-- Top menu profile link : this shows only when top menu is active -->
				<div class="user-menu" style="margin-top: 12px;">
                        <div class="row">

                            <div class="col-md-6 user-inner-wrap" style="width:324px;">
                            	<div class="row">

                            	<div class="col-md-7 " style="text-align:right;">
                                    <p style="margin-top:9px;" class="hidden-sm hidden-xs">{{$user['first_name']}} {{$user['last_name']}}</p>


                                    <?php if(!$is_guest):?>
                                        <p>{{$company['company_name']}}</p>
                                    <?php else:?>

                                        <select class="form-control" id="company_select">
                                            <option value="">Select Company</option>
                                            @foreach($guest_companies as $record)
                                                <option value="{{$record->id}}" <?=($company->id == $record->id)?'selected':'';?>>{{$record->company_name}}</option>
                                            @endforeach
                                        </select>

                                    <?php endif;?>

                                </div>

                                <div class="col-md-3 hidden-sm hidden-xs" >

                                    <a href="{{ url('/my_profile') }}" style="color:#333">

                                        @if($user['photo'])
                                        <div class="circle-div" style="margin-top:5px;background:url('{{$user['photo']}}') no-repeat;background-position: center;background-size:50px 50px;width:50px;height:50px; ">
                                        </div>
                                        @else
                                            <div class="circle-div" >
                                                <i class="fa fa-user fa-2" style="margin-top:5px;font-size:44px;color:gray;margin-top:6px;"></i>
                                            </div>
                                        @endif
                                    </a>

                                </div>

                                <div class="col-md-2" style="padding:0">
                                    <a href="#"
                                    class="dropdown-toggle no-margin userdropdown" data-toggle="dropdown" style="float:left;color:#333;">
                                    <i class="icon manage-icon" style="width:35px;height:40px;margin-top:9px;"></i>
                                    </a>

                                    <ul class="dropdown-menu pull-right profilemenu" style="padding:0">
                                        <a href="{{ url('/my_profile') }}" style="color:#333">

                                            <li style="padding-left:10px;">
                                                <i class="fa fa-user"></i> My Profile
                                            </li>

                                        </a>

                                        @if($is_admin)

                                        <a href="{{ url('/my_company') }}" style="color:#333">
                                            <li style="padding-left:10px;">

                                                <i class="fa fa-building"></i> My Company
                                            </li>
                                        </a>
                                        @endif

                                        @if(!$is_guest)

                                            <a href="{{url('/referrals')}}" style="color:#333">
                                                <li style="padding-left:10px;">
                                                    <i class="fa fa-users"></i> Referrals
                                                </li>


                                            </a>

                                            @if($is_admin)

                                            <a href="{{url('/file_settings')}}"  style="color:#333">

                                                <li style="padding-left:10px;">

                                                    <i class="fa fa-database"></i> Storage Space
                                                </li>
                                            </a>

                                            <a href="{{url('/admin_settings')}}" style="color:#333">
                                                <li style="padding-left:10px;">

                                                    <i class="fa fa-cog"></i> Admin Settings
                                                </li>
                                            </a>
                                            @endif
                                            <a href="{{url('/tutorials/5')}}" target="_blank" style="color:#333">
                                                <li style="padding-left:10px;">

                                                    <i class="fa fa-info"></i> Tutorials
                                                </li>
                                            </a>

                                        @endif


                                        <a href="http://support.zenintra.net" target="_blank"
                                         style="color:#333" >
                                            <li style="padding-left:10px;">
                                                 <i class="fa fa-support"></i> Support
                                            </li>
                                        </a>
                                        <a href="{{ url('/logout') }}" style="color:#333"
                                            data-action="userLogout">
                                            <li style="padding-left:10px;">
                                                <i class="fa fa-sign-out fa-lg"></i> <strong><u>L</u>ogout</strong>
                                             </li>
                                        </a>
                                    </ul>
                                </div>


                            	</div>
                            </div>

                        </div>
				</div>


				<!-- logout button -->
				<div id="logout" class="btn-header transparent pull-right" style="display:none;">
					<span> <a href="{{ url('/logout') }}" title="Sign Out" data-action="userLogout" data-logout-msg="You can improve your security further after logging out by closing this opened browser"><i class="fa fa-sign-out"></i></a> </span>				</div>
				<!-- end logout button -->

				<!-- search mobile button (this is hidden till mobile view port) -->
				<div id="search-mobile" class="btn-header transparent pull-right">
					<span> <a href="javascript:void(0)" title="Search"><i class="fa fa-search"></i></a> </span>
				</div>
				<!-- end search mobile button -->


				@include('dashboard.layouts.language_select')

			</div>
			<!-- end pulled right: nav area -->

		</header>
		<!-- END HEADER -->

		<!-- Left panel : Navigation area -->
		<!-- Note: This width of the aside area can be adjusted through LESS variables -->
		<aside id="left-panel">

			<!-- User info -->
			<div class="login-info">
				<span> <!-- User image size is adjusted inside CSS, it should stay as it -->

					<a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
						<img src="<?php echo url('/');?>/img/avatars/sunny.png" alt="me" class="online" />
						<span>
							{{ $user['first_name'] }}
							{{ $user['last_name'] }}
						</span>
						<i class="fa fa-angle-down"></i>
					</a>

				</span>
			</div>
			<!-- end user info -->

			<!-- NAVIGATION : This navigation is also responsive-->
			@include('dashboard.layouts.nav-main')

			@include('dashboard.layouts.nav')

			<span class="minifyme" data-action="minifyMenu">
				<i class="fa fa-arrow-circle-left hit"></i>
			</span>

		</aside>
		<!-- END NAVIGATION -->

		<!-- MAIN PANEL -->
		<div id="main" role="main">

			<!-- RIBBON -->
			<div id="ribbon">
				<div id="hide-menu" class="btn-header pull-left hidden-lg hidden-md">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" class="mobile-menu" title="Collapse Menu" style="margin-top: 5px !important;"><i class="fa fa-reorder"></i></a> </span>
				</div>
				<!-- breadcrumb
				<ol class="breadcrumb">
					<li>Home</li><li>Dashboard</li>
				</ol>
				 end breadcrumb -->
                <p class="pull-right" style="margin-top:12px;">
                <?php echo date('F d, Y');?>
                </p>

			</div>
			<!-- END RIBBON -->

			<!-- MAIN CONTENT -->
			<div id="content">

                @yield('content')


			</div>
			<!-- END MAIN CONTENT -->

		</div>
		<!-- END MAIN PANEL -->

		<!-- PAGE FOOTER -->

        @include('dashboard.layouts.footer')

        @yield('page-script')
	</body>

</html>