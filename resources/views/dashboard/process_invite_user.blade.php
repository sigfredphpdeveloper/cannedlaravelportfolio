@extends('dashboard.layouts.master')

@section('content')
<style type="text/css">
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
</style>
<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->

    <div class="row">

        <div class="col-md-6">
            <div class="alert alert-primary" style="margin-bottom:0;">


            <p>{{trans("directory_invite_user.Invite your employees")}}</p>

            <p>{{trans("directory_invite_user.You can invite them even if they")}}</p>

            <p>{{trans("directory_invite_user.Please use only one email per member")}}</p>


            </div>
        </div>

        <div class="col-md-6">
            @if($is_admin)


            <form action="{{ url('user_import_process') }}" method="POST" id="file_upload_form" enctype="multipart/form-data">
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <input type="hidden" name="user_id" value="{{ $user['id'] }}">

<a href="{{url('/download_csv_users')}}" class="btn btn-primary">{{trans("directory_invite_user.Download Sample CSV")}}</a>

                 <span class="btn btn-info btn-file" id="bulkupload" data-toggle="tooltip" data-placement="top" title="">
                     <input type="file" name="upload" id="csv_upload" multiple >{{trans("directory_invite_user.Bulk Upload")}}
                 </span>
             </form>

            @endif
        </div>

            <div class="col-md-12">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ Session::get('success') }}</p>
                </div>
            @endif
            </div>

            <div class="col-md-12">
            @if(Session::has('error'))
                <div class="alert alert-danger">
                    <p>{{ Session::get('error') }}</p>
                </div>
            @endif
            </div>

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>{{trans("directory_invite_user.Warning.")}}<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <article class="col-sm-12 col-md-12 col-lg-12">

                <form  id="smart-form-register" class="client-form" role="form" method="POST" action="{{ url('/send_invitation') }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <header>
                         {{trans("directory_invite_user.Add Employee")}}
                    </header>

                    <div id="form_contains">

                        <?php for($i=1;$i<=3;$i++):?>

                        <fieldset >

                         <?php if($i>1):?>
                         <a href="javascript:void(0);" class="btn btn-danger pull-left remover" style="cursor:pointer;">
                            <i class="fa fa-remove"></i> {{trans("directory_invite_user.Remove")}}
                        </a>

                        <br style="clear:both;" />

                        <?php endif;?>

                        <div class="row" style="padding-top:12px;">

                            <div class="form-group col-md-2" <?=($i==1)?'style="padding-top:29px;"':'';?>>
                                  <label class="form-label">{{trans("directory_invite_user.Email *")}}</label>
                                  <input type="email" name="email[]" required class="form-control" required
                                  placeholder="{{trans("directory_invite_user.Enter Email")}}" />
                            </div>

                            <div class="form-group col-md-2" <?=($i==1)?'style="padding-top:29px;"':'';?>>
                               <label class="form-label">{{trans("directory_invite_user.First Name")}} *</label>
                               <input type="text" name="first_name[]" value="" class="form-control" required />
                            </div>

                            <div class="form-group col-md-2" <?=($i==1)?'style="padding-top:29px;"':'';?>>
                               <label class="form-label">{{trans("directory_invite_user.Last Name")}} *</label>
                               <input type="text" name="last_name[]" class="form-control" required />
                            </div>

                            @if($i==1)

                            <div class="col-md-6">
                               <p>
                               {{trans("directory_invite_user.You can select multiple")}}
                              </p>
                            </div>
                            @endif

                            <div class="form-group col-md-3">
                               <label class="form-label">


                               {{trans("directory_invite_user.Employee Roles")}}</label>

                               <select name="roles[][]" multiple class="form-control">

                                   @foreach($roles as $role)
                                       <option value="{{$role['id']}}">{{$role['role_name']}}</option>
                                   @endforeach

                               </select>

                            </div>

                            <div class="form-group col-md-3">
                               <label class="form-label">{{trans("directory_invite_user.Employee Teams")}}</label>
                               <select name="teams[][]" multiple class="form-control">

                                   @foreach($teams as $team)
                                       <option value="{{$team['id']}}">{{$team['team_name']}}</option>
                                   @endforeach

                               </select>
                            </div>
                     </div>

                     </fieldset>
                    <?php endfor; ?>



                    <hr />

                    </div>


                    <footer>

                        <a href="javascript:void(0);" class="btn-default btn pull-left" id="add_more">
                            <i class="fa fa-plus-square"></i> {{trans("directory_invite_user.Invite more users")}}
                        </a>

                        <button type="submit" class="btn btn-success pull-right">
                            {{trans("directory_invite_user.Send Invitation")}}
                        </button>
<!--
                        <a href="{{url('/directory')}}" class="btn btn-default">{{trans("directory_invite_user.Back")}}</a>
                        -->
                    </footer>


                </form>

            </article>

    </div>


<div id="clone_from" style="display:none;">

        <?php for($i=1;$i<=3;$i++):?>

        <fieldset>

        <a href="javascript:void(0);" class="btn btn-danger pull-left remover" style="cursor:pointer;">
            <i class="fa fa-remove"></i> {{trans("directory_invite_user.Remove")}}
        </a>

        <br style="clear:both;" />

        <div class="row" style="padding-top:12px;">

            <div class="form-group col-md-2">
                  <label class="form-label">{{trans("directory_invite_user.Email *")}}</label>
                  <input type="email" name="email[]" required class="form-control"
                  placeholder="{{trans("directory_invite_user.Enter Email")}}" />
            </div>

            <div class="form-group col-md-2">
               <label class="form-label">{{trans("directory_invite_user.First Name")}} *</label>
               <input type="text" name="first_name[]" value="" class="form-control" required />
            </div>

            <div class="form-group col-md-2">
               <label class="form-label">{{trans("directory_invite_user.Last Name")}} *</label>
               <input type="text" name="last_name[]" class="form-control"  required />
            </div>

            <div class="form-group col-md-3">
               <label class="form-label">{{trans("directory_invite_user.Employee Roles")}}</label>

               <select name="roles[][]" multiple class="form-control">

                   @foreach($roles as $role)
                       <option value="{{$role['id']}}">{{$role['role_name']}}</option>
                   @endforeach

               </select>

            </div>

            <div class="form-group col-md-3">
               <label class="form-label">{{trans("directory_invite_user.Employee Teams")}}</label>
               <select name="teams[][]" multiple class="form-control">

                   @foreach($teams as $team)
                       <option value="{{$team['id']}}">{{$team['team_name']}}</option>
                   @endforeach

               </select>
            </div>
        </div>

     </fieldset>
    <?php endfor; ?>




</div>

    <!-- end row -->

</section>
<!-- end widget grid -->

<script type="text/javascript">

    $(document).ready(function(){

        //form_contains
        //clone_from

        $('#add_more').click(function(){

            //$("#from").clone().appendTo($("#to"));
            var clone = $('#clone_from').clone();

            $('#form_contains').append(clone);

            $('#form_contains').find('#clone_from').attr('id',false);

            $('#form_contains').children().show();

        });

        $(document).on('click','.remover',function(e){
            $(this).parent().remove();
        });

        $('#csv_upload').on('change', function(){

            $(this).html('{{trans("directory_invite_user.Loading please wait")}}');
            $('#file_upload_form').submit();
        })

    });

</script>


@endsection