<table class="table table-striped">

<tr>
    <td>{{trans("referrals_view.Company")}} </td>
    <td>
        {{$referral->url}}
    </td>
</tr>

<tr>
    <td>{{trans("referrals_view.User")}} </td>
    <td>{{$referral->email}}</td>
</tr>

<tr>
    <td>{{trans("referrals_view.Date Created")}}</td>
    <td>
        {{date('m/d/Y',strtotime($referral->referral_credit_date))}}
    </td>
</tr>
</table>