@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-md-12">
            <h1 style="margin-bottom:20px;">{{trans("referrals_manager.Manage Referrals")}}</h1>
            <p style="margin-bottom:20px;"> {{trans("referrals_manager.more_space_invite_text")}}</p>
        </div>

        <div class="col-md-12">
            @if(Session::has('success'))
                <div class="alert alert-success">
                    <p>{{ Session::get('success') }}</p>
                </div>
            @endif
        </div>

        <div class="col-md-3 pull-left">
            <a href="{{ url('/referrals_add') }}" class="btn btn-success">{{trans("referrals_manager.Add Referral")}}</a>
        </div>

        <div class="col-md-3 pull-right">
            <a href="{{ url('/referrals_import') }}" class="btn btn-primary">{{trans("referrals_manager.Import CSV")}}</a>
        </div>

        <div class="col-md-6 pull-right">
            <div class="input-group pull-right">

                <form class="form-inline" method="GET">
                    <div class="form-group">
                        <div class="input-group">
                           <input type="text" name="search" id="search_box" class="form-control"
                                                          value="<?php echo (isset($search))?$search:'';?>" placeholder="Enter Keywords">
                            <span class="input-group-btn"> <button class="btn btn-default" id="search_btn" type="submit">{{trans("referrals_manager.Search")}}</button></span>
                        </div>
                    </div>
                </form>




            </div>

        </div>



    </div>
       <br />
    <div class="row">
        <div class="col-md-12">

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>{{trans("referrals_manager.Invitation Sent To")}}</th>
                        <th>{{trans("referrals_manager.Date")}}</th>
                        <th width="200">{{trans("referrals_manager.Status")}}</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($referrals as $i=>$item)
                    <tr>
                        <td>{{$item['invitee_email']}}</td>
                        <td>{{date('m/d/Y',strtotime($item['send_date']))}}</td>
                        <td>

                           <?php if($item['referral_status'] == 'success'):?>
                            <a href="#" class="view_referral" data-id="{{$item['id']}}">
                             {{$item['referral_status']}}
                            </a>
                           <?php else: ?>
                             {{$item['referral_status']}}
                           <?php endif; ?>

                        </td>
                    </tr>
                    @endforeach
                </tbody>

                <tfooot>
                    <tr>
                        <td colspan="6"> {!! $referrals->render() !!}</td>
                    </tr>
                </tfooot>

            </table>

        </div>

    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->

<script type="text/javascript">

    $(document).ready(function(){

        $('.deleter').click(function(){

            var id = $(this).data('id');
            $('#delete_proceed').attr('alt',id);
            $('#confirm').modal('show');

        });

        $('#delete_proceed').click(function(){

            var id = $(this).attr('alt');

            $('#confirm').modal('hide');

            window.location.href='{{ url('teams_delete') }}/'+id;

        });

        $('.view_referral').click(function(){

            var id = $(this).data('id');



            $.ajax({

                url : '{{url('/view_referral_details')}}/'+id,
                type : 'GET',
                success:function(html){
                    $('#details_modal_body').html(html);
                    $('#details_modal').modal('show');
                }
            });

        });


    });

</script>

<div class="modal fade" tabindex="-1" role="dialog" id="confirm">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{trans("referrals_manager.Confirmation")}}</h4>
      </div>
      <div class="modal-body">
        <p>{{trans("referrals_manager.Are you sure you want to delete this record ?")}}" </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("referrals_manager.No")}}</button>
       <button type="button" class="btn btn-primary" id="delete_proceed" alt="">{{trans("referrals_manager.Yes")}}</button>

      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" tabindex="-1" role="dialog" id="details_modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{trans("referrals_manager.Referral Details")}}</h4>
      </div>
      <div class="modal-body" id="details_modal_body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("referrals_manager.OK")}}</button>

      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">

</script>
@endsection