@extends('dashboard.layouts.master')

@section('content')

<style type="text/css">
.input_fields_wrap input[type=text]{
width:90%;
}
</style>
<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-sm-12 col-md-12">
        <h1>{{trans("referrals_add.Add Referral")}}</h1>
        <h3>{{trans("referrals_add.You need more space ?")}} </h3>

        <p>
        {{trans("referrals_add.You need more space? Invite other companies to join Zenintra and earn 200Mb additional Zenintra disk space for each company joining (up to 5Gb).")}}
        </p>
        </div>

        <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Warning.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <article class="col-sm-6 col-md-6 col-lg-6">

            <form  id="smart-form-register" class="smart-form client-form" role="form" method="POST" action="{{ url('/referrals_add_save') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="user_id" value="{{ $user['id'] }}">
            <input type="hidden" name="company_id" value="{{ $company['id'] }}">

                <fieldset>

                     <section>
                        <label class="label">{{trans("referrals_add.Email Address")}} </label>
                        <label class="input">

                            <div class="input_fields_wrap">
                            <a href="#" class="add_field_button" type="button">{{trans("referrals_add.Add more")}} <i class="fa fa-plus"></i></a>
                            <br />
                                <div><input type="text" name="email_address[]" placeholder="{{trans("referrals_add.Please enter email")}}" required="required"></div>
                                <br />
                            </div>

                        </label>
                    </section>

                    <section style="display:none;">
                        <label class="label">{{trans("referrals_add.Invitation Email Title")}}</label>
                        <label class="input">
                        <input name="invite_title" class="form-control" placeholder="{{trans("referrals_add.Enter your Invitation Title (optional)")}}" />
                        </label>
                    </section>

                    <section style="display:none;">
                        <label class="label">{{trans("referrals_add.Invitation Message")}}</label>
                        <label class="input">
                        <textarea name="invite_message" class="form-control" placeholder="{{trans("referrals_add.Please enter your invitational message. (optional)")}}" style="height:120px;" ></textarea>
                        </label>
                    </section>

                </fieldset>

                <footer>

                    <button type="submit" class="btn btn-success">
                        {{trans("referrals_add.Invite")}}
                    </button>

                    <a href="{{ url('/referrals') }}" class="btn btn-primary">{{trans("referrals_add.Back")}}</a>
                </footer>

            </form>




        </article>
    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->



@endsection

@section('page-script')
<script type="text/javascript">
$(document).ready(function(){

        var max_fields      = 10; //maximum input boxes allowed
        var wrapper         = $(".input_fields_wrap"); //Fields wrapper
        var add_button      = $(".add_field_button"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
            if(x < max_fields){ //max input box allowed
                x++; //text box increment
                $(wrapper).append('<div><input type="text" name="email_address[]" placeholder="Please enter email"/><a href="#" class="remove_field">Remove Email</a><br /></div>'); //add input box
            }
        });

        $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
            e.preventDefault(); $(this).parent('div').remove(); x--;
        })


        $('.add_field_button').tooltip();
});
</script>
@endsection
