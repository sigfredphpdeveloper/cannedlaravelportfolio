<table class="table table-striped">
<tr>
    <td>{{trans("emergency_contact_view.Employee")}}</td>
    <td>{{$ec['email']}}</td>
</tr>
<tr>
    <td>{{trans("emergency_contact_view.Emergency Contact Name")}}</td>
    <td>{{$ec['contact_name']}}</td>
</tr>
<tr>
    <td>{{trans("emergency_contact_view.Emergency Contact Phone")}}</td>
    <td>{{$ec['contact_phone']}}</td>
</tr>
<tr>
    <td>{{trans("emergency_contact_view.Emergency Contact Address")}}</td>
    <td>{{$ec['contact_address']}}</td>
</tr>
<tr>
    <td>{{trans("emergency_contact_view.Emergency Contact Relationship")}}</td>
    <td>{{$ec['relationship']}} {{$ec['relationship_other']}}</td>
</tr>
</table>