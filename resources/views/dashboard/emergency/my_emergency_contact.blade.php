@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-sm-12 col-md-12">
        <h1>{{trans("my_emergency_contact.Emergency Contact")}}</h1>
        </div>

        <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Warning.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <article class="col-sm-6 col-md-6 col-lg-6">

            <form  id="smart-form-register" class="smart-form client-form" role="form" method="POST" action="{{ url('/emergency_contacts') }}">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="user_id" value="{{ $user['id'] }}">
            <input type="hidden" name="company_id" value="{{ $company['id'] }}">
            <input type="hidden" name="id" value="{{$ec['id']}}" />

                <fieldset>

                     <section>
                        <label class="label">{{trans("my_emergency_contact.Contact Name")}}</label>
                        <label class="input">
                        <input type="text" name="contact_name" placeholder="{{trans("my_emergency_contact.Contact Name")}}" value="{{$ec['contact_name']}}" class="form-control"/>
                        </label>
                    </section>

                    <section>
                        <label class="label">{{trans("my_emergency_contact.Phone Number")}}</label>
                        <label class="input">
                        <input type="text" name="contact_phone" placeholder="{{trans("my_emergency_contact.Phone Number")}}" value="{{$ec['contact_phone']}}" class="form-control"/>
                        </label>
                    </section>

                    <section>
                        <label class="label">{{trans("my_emergency_contact.Address")}}</label>
                        <label class="input">
                        <textarea name="contact_address" class="form-control">{{$ec['contact_address']}}</textarea>
                        </label>
                    </section>

                    <section>
                        <label class="label">{{trans("my_emergency_contact.Relationship")}}</label>
                        <label class="input">
                            <select name="relationship" id="relationship" class="form-control">
                                <option value="Spouse" <?php echo $ec['relationship']=='Spouse'?'selected':'';?>>{{trans("my_emergency_contact.Spouse")}}</option>
                                <option value="Parent" <?php echo $ec['relationship']=='Parent'?'selected':'';?>>{{trans("my_emergency_contact.Parent")}}</option>
                                <option value="Child" <?php echo $ec['relationship']=='Child'?'selected':'';?>>{{trans("my_emergency_contact.Child")}}</option>
                                <option value="Friend" <?php echo $ec['relationship']=='Friend'?'selected':'';?>>{{trans("my_emergency_contact.Friend")}}</option>
                                <option value="Other" <?php echo $ec['relationship']=='Other'?'selected':'';?>>{{trans("my_emergency_contact.Other")}}</option>
                            </select>
                        </label>
                    </section>

                     <section id="secion_other" <?php echo $ec['relationship_other']==''?'style="display:none;"':'';?>>
                        <label class="label">{{trans("my_emergency_contact.Other Relationship")}}</label>
                        <label class="input">
                        <input type="text" name="relationship_other"  value="{{$ec['relationship_other']}}" class="form-control" />
                        </label>
                    </section>

                </fieldset>

                <footer>
                    <button type="submit" class="btn btn-success">
                        {{trans("my_emergency_contact.Save")}}
                    </button>
                </footer>

            </form>

        </article>
    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->

<script type="text/javascript">
    $(document).ready(function(){
        $('#relationship').change(function(){

            var v = $(this).val();

            if(v=='Other'){
                $('#secion_other').show();
            }else{
                $('#secion_other').hide();
            }

        });
    });
</script>

@endsection