@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-md-12">

         <a href="{{ url('/admin_settings') }}" class="btn btn-xs btn-default"><i class="fa fa-caret-left"></i> Back To Settings</a>


        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif



        </div>

        <form action="{{url('/save_role_permission_emergency_contact')}}" method="POST" >
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="col-md-12">

                <h1>{{trans("manage_permissions.Manage Permissions")}}</h1>

                <?php
                    $count = count($permissions);
                ?>

                @include('dashboard.layouts.permission_select')

            </div>

            <div class="col-md-12">
                <input type="submit" value="{{trans("manage_permissions.Save")}}" class="btn btn-primary" data-toggle="tooltip" title="{{trans("manage_permissions.Save Assigned Permissions")}}" />
            </div>

        </form>

    </div>
    <!-- end row -->

</section>
<!-- end widget grid -->


<script type="text/javascript">

    $(document).ready(function(){

        $('[data-toggle="tooltip"]').tooltip();

    });

</script>
@endsection
