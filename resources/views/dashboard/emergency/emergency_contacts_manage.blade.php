@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-md-12">
            @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
            @endif
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Warning.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="col-md-3">
        </div>

        <div class="col-md-9 pull-right">

        <form class="form-inline pull-right" method="GET" style="display:inline-block;" >
            <div class="form-group">
                <div class="input-group">
                    <div id="change_here">
                        <input type="text" name="search" id="search_box"
                        value="<?php echo (isset($search))?$search:'';?>" class="form-control"
                         placeholder="{{trans("emergency_contact_manage.Enter Keywords")}}">
                    </div>
                    <span class="input-group-btn"> <button class="btn btn-primary" id="search_btn" type="submit">Search</button></span>
                </div>
            </div>
        </form>




        </div>

        <div class="col-md-12">

            <h1>{{trans("emergency_contact_manage.Emergency Contacts")}}</h1>

            <table class="table table-striped">

                <thead>
                    <tr>
                        <th>{{trans("emergency_contact_manage.Employee")}}</th>
                        <th width="200">{{trans("emergency_contact_manage.Emergency Contact")}}</th>
                        <th width="200">{{trans("emergency_contact_manage.Emergency Phone")}}</th>
                        <th ></th>
                    </tr>
                </thead>

                <tbody>

                @foreach($emergency_contacts as $contact)
                <tr>
                    <td>{{$contact->email}}</td>
                    <td>{{$contact->contact_name}}</td>
                    <td>{{$contact->contact_phone}}</td>
                    <td>
                        @if($view_emergency_contacts)
                            <a href="#" data-id="{{$contact->id}}" class="btn-view icon view-icon" >{{trans("emergency_contact_manage.View")}}</a>
                        @endif

                        @if($edit_emergency_contacts)
                            <a href="#" data-id="{{$contact->id}}" class="btn-edit icon edit-icon" >{{trans("emergency_contact_manage.Edit")}}</a>
                        @endif
                    </td>
                </tr>
                @endforeach

                </tbody>

                <tfooot>
                    <tr>
                        <td colspan="4"> {!! $emergency_contacts->render() !!}</td>
                    </tr>
                </tfooot>

            </table>

             <a href="{{url('/export_emergency_contacts')}}" class="pull-right btn btn-success" >{{trans("emergency_contact_manage.Export CSV")}}</a>

        </div>
    </div>
    <!-- end row -->

</section>
<!-- end widget grid -->

<div class="modal fade" tabindex="-1" role="dialog" id="confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{{trans("emergency_contact_manage.Confirmation")}}</h4>
            </div>
            <div class="modal-body">
                <p>{{trans("emergency_contact_manage.Are you sure you want to delete this User ?")}} </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("emergency_contact_manage.No")}}</button>
                <button type="button" class="btn btn-primary" id="delete_proceed" alt="">{{trans("emergency_contact_manage.Yes")}}</button>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="emergency_contact_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans("emergency_contact_manage.View Emergency Contact")}}</h4>
            </div>

                <div class="modal-body" id="emergency_contact_modal_body">
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("emergency_contact_manage.OK")}}</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="edit_modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">{{trans("emergency_contact_manage.View Emergency Contact")}}</h4>
            </div>

                <div class="modal-body" id="edit_modal2_body">
                </div>


        </div>
    </div>
</div>

<!-- end widget grid -->

<script type="text/javascript">

    $(document).ready(function(){

        $('.deleter').click(function(){

            var id = $(this).data('id');
            $('#delete_proceed').attr('alt',id);
            $('#confirm').modal('show');

        });

        $('#delete_proceed').click(function(){

            var id = $(this).attr('alt');

            $('#confirm').modal('hide');

            window.location.href='{{ url('user_delete') }}/'+id;

        });

        $( ".custom_auto_complete" ).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "{{ url('/companies_list') }}",
                    dataType: "json",

                    data: {
                        q: request.term
                    },
                    success: function( data ) {
                        console.log(data);
                        response(data);
                    }
                });
            },
            select: function(e,ui){
                console.log(ui);
                $('#company_id').val(ui.item.id);
            }
        });

        $( ".custom_auto_complete" ).autocomplete( "option", "appendTo", ".eventInsForm" );

        $('.btn-edit').click(function(){

            var id = $(this).data('id');

            $.ajax({
                url : '{{url('/emergency_contact_edit/')}}/'+id,
                type : 'GET',
                success:function(html){
                    $('#edit_modal2_body').html(html);
                    $('#edit_modal2').modal('show');
                }

            });
        });

         $('.btn-view').click(function(){

            var id = $(this).data('id');

            $.ajax({
                url : '{{url('/emegency_contact_view/')}}/'+id,
                type : 'GET',
                success:function(html){
                    $('#emergency_contact_modal_body').html(html);
                    $('#emergency_contact_modal').modal('show');
                }
            });
        });

    });

</script>


@endsection
