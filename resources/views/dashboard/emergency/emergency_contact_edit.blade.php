<form action="{{ url('/emergency_contact_edit') }}/{{$ec['id']}}" method="POST" class="eventInsForm2 form-horizontal">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

       <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">{{trans("emergency_contact_edit.Contact Name")}}</label>
        <div class="col-sm-9">
            <input type="text" name="contact_name" placeholder="Contact Name" value="{{$ec['contact_name']}}" class="form-control"/>
        </div>
      </div>

      <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">{{trans("emergency_contact_edit.Phone Number")}}</label>
        <div class="col-sm-9">
            <input type="text" name="contact_phone" placeholder="Phone Number" value="{{$ec['contact_phone']}}" class="form-control"/>
        </div>
      </div>

      <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">{{trans("emergency_contact_edit.Address")}}</label>
        <div class="col-sm-9">
            <textarea name="contact_address" class="form-control">{{$ec['contact_address']}}</textarea>
        </div>
      </div>

      <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">{{trans("emergency_contact_edit.Relationship")}}</label>
        <div class="col-sm-9">
            <select name="relationship" id="relationship" class="form-control">
                <option value="Spouse" <?php echo $ec['relationship']=='Spouse'?'selected':'';?>>{{trans("emergency_contact_edit.Spouse")}}</option>
                <option value="Parent" <?php echo $ec['relationship']=='Parent'?'selected':'';?>>{{trans("emergency_contact_edit.Parent")}}</option>
                <option value="Child" <?php echo $ec['relationship']=='Child'?'selected':'';?>>{{trans("emergency_contact_edit.Child")}}</option>
                <option value="Friend" <?php echo $ec['relationship']=='Friend'?'selected':'';?>>{{trans("emergency_contact_edit.Friend")}}</option>
                <option value="Other" <?php echo $ec['relationship']=='Other'?'selected':'';?>>{{trans("emergency_contact_edit.Other")}}</option>
            </select>
        </div>
      </div>

      <div class="form-group">
        <label for="inputEmail3" class="col-sm-3 control-label">{{trans("emergency_contact_edit.Other Relationship")}}</label>
        <div class="col-sm-9">
            <input type="text" name="relationship_other"  value="{{$ec['relationship_other']}}" class="form-control" />
        </div>
      </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("emergency_contact_edit.Close")}}</button>
        <button type="submit" class="btn btn-primary">{{trans("emergency_contact_edit.Save changes")}}</button>
    </div>
</form>

