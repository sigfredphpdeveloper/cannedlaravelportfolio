@extends('dashboard.layouts.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('js/plugin/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
			<h1 class="page-title txt-color-blueDark">
				<i class="fa fa-money fa-fw "></i>
					<a href="{{ url('my-payslips') }}">My Payslips</a> <span>> Payslip Item</span>
			</h1>
		</div>
		@if( $is_admin )
		   <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4 pull-right text-right">
				<h3 class="page-title txt-color-blueDark">
						<a href="{{ url('/payroll-settings') }}" class="btn btn-primary">Manage</a>
				</h3>
			</div>
	    @endif
	</div>
	@if(Session::has('success'))
		<div class="alert alert-block alert-success">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('success') }}</h4>
		</div>
	@elseif(Session::has('error'))
		<div class="alert alert-block alert-danger">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('error') }}</h4>
		</div>
	@endif

	<!-- widget grid -->
	<section id="widget-grid" class="">
		<div class="row">
			<div class="col-md-10 col-xs-12 col-md-offset-1">
				<div class="text-right">
					 <a href="{{ url('download-payroll-item', $employee->id) }}" class="btn btn-primary">Download</a>
				</div>
				@include('dashboard.payroll.history.partials.payslip')
			</div>
		</div>
	</section>

@endsection

@section('page-script')

<!-- <script type="text/javascript" src="{{ url('/js/my-expenses.js') }}"></script> -->
<script type="text/javascript">
	jQuery(function($){
		$('#bulk-select').click(function () {    
		    $('.payroll-item-id').prop('checked', this.checked);    
		});

		$('.payroll-item-id').click(function () {    
		    $('#bulk-select').prop('checked', this.checked);    
		});
	})
</script>

@endsection