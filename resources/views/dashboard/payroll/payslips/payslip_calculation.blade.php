
@extends('dashboard.layouts.master')

@section('styles')
<style>
.bordered-container{ border:2px solid #e5e5e5;}
.payslip_container h3{ margin:5px 0;}
.payslip_container h3.header{ margin:20px 0; text-align: center;}
</style>

@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-money fa-fw "></i>
                Payslip
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-7 ">
            <div class="col-xs-12 bordered-container payslip_container">


                <div class="col-xs-7 ">
                    <h2>{{$company->company_name}}</h2>
                    <h3>{{$company->address}}</h3>
                    <h3>Registration No.: #{{$company->id}}</h3>
                </div>
                <div class="col-xs-4 pull-right"><img src="{{$company->logo}}" ></div>
                <div class="col-xs-12">
                    <h3 class="header">Payslip for [Month][Year]</h3>
                </div>

                <table class="table table-stripe">
                    <thead>
                        <tr>
                            <th>Payment Date (Item)</th>
                            <th>Mode of Payment (Amount)</th>
                            <th> &nbsp; </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Basig Pay</td>
                            <td>4,000</td>
                            <td>(A)</td>
                        </tr>
                        <tr>
                            <td>Total Allowance <br> (Breakdown)</td>
                            <td>4,000</td>
                            <td>(A)</td>
                        </tr>
                        <tr>
                            <td> &nbsp; </td>
                            <td> Sum here </td>
                            <td>(B)</td>
                        </tr>

                    </tbody>
                </table>


            </div>
        </div>
    </div>



@endsection

@section('page-script')

@endsection