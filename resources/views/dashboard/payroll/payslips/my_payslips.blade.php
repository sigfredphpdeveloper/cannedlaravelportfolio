
@extends('dashboard.layouts.master')

@section('styles')
<style>
</style>

@endsection

@section('content')
<div class="row">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            My Payslips
        </h1>
    </div>
</div>

<div class="row">

    <div class="col-xs-5">

        <table class="table table-stripe">
            <thead>
            <tr>
                <th>Month</th>
                <th>Year</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>January</td>
                <td>2016</td>
                <td>
                    <button class="btn btn-sm btn-primary btn-sm">View</button>
                    <button class="btn btn-sm btn-primary btn-sm">Download</button>
                </td>
            </tr>
            </tbody>
        </table>

    </div>

</div>



@endsection

@section('page-script')

@endsection