<label name="employees">Select Employees</label>
<label class="select select-multiple">
	<select multiple="" name="employees[]" id="employee-select" class="custom-scroll" style="width: 100%;" required>
		@foreach($sgemployees as $employee)
			<option value="{{ $employee->id }}">{{ $employee->full_name_1 }}</option>
		@endforeach
	</select>
</label>