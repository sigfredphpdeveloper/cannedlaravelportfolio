@foreach($sgemployees as $row => $employee)
	<div class="employee-container <?= ($row == 0) ? 'active' : '' ?>  <?= ( $row == count($sgemployees)-1 ) ? 'last-employee' : '' ?>" id="step-{{ ($row+1) }}">
        <header>{{  $employee->full_name_1 }}</header>
		<fieldset>
			<div class="row">
				<section class="col col-6">
					<label>Compensation for loss of office amount</label>
				</section>
				<section class="col col-2">
					<label class="input">
						<input type="number" name="compensation_for_loss[{{ $employee->id }}]" class="compensation_for_loss" placeholder="0">
					</label>
				</section>
				<section class="col col-4"></section>
			</div>
			<div class="row show-if-compensation-for-loss" style="display: none">
				<section class="col col-6">
					<label style="padding-left: 15px"><em>Approval Obtained by IRAS</em></label>
				</section>
				<section class="col col-2">
					<div class="inline-group">
						<label class="radio">
							<input type="radio" name="apprv_obtained_by_iras[{{ $employee->id }}]" value="Y">
							<i></i>Yes</label>
						<label class="radio">
							<input type="radio" name="apprv_obtained_by_iras[{{ $employee->id }}]" value="N">
							<i></i>No</label>
					</div>
				</section>
				<section class="col col-4"></section>
			</div>
			<div class="row show-if-compensation-for-loss" style="display: none">
				<section class="col col-6">
					<label style="padding-left: 15px"><em>Date of Approval</em></label>
				</section>
				<section class="col col-2">
					<label class="input">
						<input type="text" name="date_of_approval[{{ $employee->id }}]" class="datepicker" placeholder="">
					</label>
				</section>
				<section class="col col-4"></section>
			</div>
			<div class="row">
				<section class="col col-6">
					<label>Retirement benefits amount accrued from 1993</label>
				</section>
				<section class="col col-2">
					<label class="input">
						<input type="number" name="retirement_accrued_1993[{{ $employee->id }}]" class="retirement_accrued_1993" placeholder="0">
					</label>
				</section>
				<section class="col col-4">
				</section>
			</div>
			<div class="row show-if-retirement-benefits" style="display: none">
				<section class="col col-6">
					<label style="padding-left: 15px"><em>Name of Fund</em></label>
				</section>
				<section class="col col-2">
					<label class="input">
						<input type="text" name="name_of_fund[{{ $employee->id }}]">
					</label>
				</section>
				<section class="col col-4"></section>
			</div>
			<div class="row show-if-retirement-benefits" style="display: none">
				<section class="col col-6">
					<label style="padding-left: 15px"><em>Retirement benefits accrued up to 31.12.92</em></label>
				</section>
				<section class="col col-2">
					<label class="input">
						<input type="number" name="retirement_accrued_31_12_92[{{ $employee->id }}]" class="" placeholder="">
					</label>
				</section>
				<section class="col col-4"></section>
			</div>
			<div class="row">
				<section class="col col-6">
					<label>Contributions made by employer to any Pension/Provident Fund constituted outside Singapore without Tax concession</label>
				</section>
				<section class="col col-2">
					<label class="input">
						<input type="number" name="contribution_made_by_employer[{{ $employee->id }}]" placeholder="0">
					</label>
				</section>
				<section class="col col-4">
				</section>
			</div>
			<div class="row">
				<section class="col col-6">
					<label>Sales Commision Period (if applicable)</label>
				</section>
				<section class="col col-2">
					<label class="input">
						<input type="text" class="datepicker" placeholder="From" name="sales_commision_from[{{ $employee->id }}]">
					</label>
					<label class="input">
						<input type="text" class="datepicker" placeholder="To" name="sales_commision_to[{{ $employee->id }}]">
					</label>
				</section>
				<section class="col col-4">
				</section>
			</div>
			<div class="row">
				<section class="col col-6">
					<label>Excess/Voluntary contribution to CPF by employer</label>
				</section>
				<section class="col col-2">
					<label class="input">
						<input type="number" name="excess_voluntary_cpf[{{ $employee->id }}]" class="excess_voluntary_cpf" placeholder="0">
					</label>
				</section>
				<section class="col col-4">
				</section>
			</div>
			<div class="row show-if-excess-voluntary" style="display: none">
				<section class="col col-6">
					<label style="padding-left: 15px"><em>Employer's Contribution</em></label>
				</section>
				<section class="col col-2">
					<label class="input">
						<input type="number" name="employer_contribution[{{ $employee->id }}]" placeholder="0">
					</label>
				</section>
				<section class="col col-4"></section>
			</div>
			<div class="row show-if-excess-voluntary" style="display: none">
				<section class="col col-6">
					<label style="padding-left: 15px"><em>Employee's Contribution</em></label>
				</section>
				<section class="col col-2">
					<label class="input">
						<input type="number" name="employee_contribution[{{ $employee->id }}]" placeholder="0">
					</label>
				</section>
				<section class="col col-4"></section>
			</div>
			<div class="row">
				<section class="col col-6">
					<label>Gains and Profits from share options for S10 (1)(b)</label>
				</section>
				<section class="col col-2">
					<label class="input">
						<input type="number" name="gains_profits_1_b[{{ $employee->id }}]" placeholder="0">
					</label>
				</section>
				<section class="col col-4"></section>
			</div>
			<div class="row">
				<section class="col col-6">
					<label>Gains and Profits from share options for S10 (1)(g)</label>
				</section>
				<section class="col col-2">
					<label class="input">
						<input type="number" name="gains_profits_1_g[{{ $employee->id }}]" placeholder="0">
					</label>
				</section>
				<section class="col col-4"></section>
			</div>
			<div class="row">
				<section class="col col-6">
					<label>Life Insurance premiums deducted from salaries</label>
				</section>
				<section class="col col-2">
					<label class="input">
						<input type="number" name="life_insurance[{{ $employee->id }}]" placeholder="0">
					</label>
				</section>
				<section class="col col-4"></section>
			</div>
			<div class="row">
				<section class="col col-6">
					<label>Date of Declaration of Bonus</label>
				</section>
				<section class="col col-2">
					<label class="input">
						<input type="text" name="date_of_declaration_bonus[{{ $employee->id }}]" class="datepicker" placeholder="">
					</label>
				</section>
				<section class="col col-4"></section>
			</div>
			<div class="row">
				<section class="col col-6">
					<label>Date of Approval of Director's Fee</label>
				</section>
				<section class="col col-2">
					<label class="input">
						<input type="text" name="date_of_approval_director_fee[{{ $employee->id }}]" class="datepicker" placeholder="">
					</label>
				</section>
				<section class="col col-4">
					@if( count($sgemployees) != 1 )
						@if( $row == count($sgemployees)-1 )
							<a href="javascript:void(0)" class="prev-employee btn btn-default pull-left" data-curr-step="#step-{{ ($row+1) }}" data-step="#step-{{ $row }}" style="margin-right: 5px">Previous</a>
						@else
							@if( ($row+1) >= 2 )
								<a href="javascript:void(0)" class="prev-employee btn btn-default pull-left" data-curr-step="#step-{{ ($row+1) }}" data-step="#step-{{ $row }}" style="margin-right: 5px">Previous</a>
							@endif
							<a href="javascript:void(0)" class="next-employee btn btn-default pull-left" data-curr-step="#step-{{ ($row+1) }}" data-step="#step-{{ ($row+1)+1 }}">Next</a>
						@endif
					@endif
				</section>
			</div>
		</fieldset>
    </div>
@endforeach