@extends('dashboard.layouts.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('js/plugin/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
			<h1 class="page-title txt-color-blueDark">
				<i class="fa fa-money fa-fw "></i> 
					Generate IRAS Form
			</h1>
		</div>
		@if( $is_admin )
		   <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4 pull-right text-right">
				<h3 class="page-title txt-color-blueDark">
						<a href="{{ url('/payroll-settings') }}" class="btn btn-primary">Manage</a>
				</h3>
			</div>
	    @endif
	</div>
	@if(Session::has('success'))
		<div class="alert alert-block alert-success">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('success') }}</h4>
		</div>
	@elseif(Session::has('error'))
		<div class="alert alert-block alert-danger">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('error') }}</h4>
		</div>
	@endif

	<!-- widget grid -->
	<section id="widget-grid" class="">
		<!-- row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="jarviswidget" id="wid-id-3" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-togglebutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>Generate IRAS Form</h2>
					</header>
					<div>
						<div class="jarviswidget-editbox"></div>
						<div class="widget-body no-padding" id="area-data">
							<form action="" method="post" class="smart-form">
								{{ csrf_field() }}
								<fieldset>
									<div class="row">
										<section class="col col-4">
											<label name="year">Select Year</label>
											<label class="select">
												<select name="year" class="select-2" style="width: 100%;" required>
													<option value="">---</option>
													@foreach($years as $year)
														<option value="{{ $year->year }}">{{ $year->year }}</option>
													@endforeach
												</select>
												<i></i>
											</label>
										</section>
										<section class="col col-4" id="employee-selection">
											<!-- show employee selection -->
										</section>
										<section class="col col-4">
											<label name="user_id">&nbsp;</label>
											<label class="text" style="display: block">
												<a href="javascript:void(0)" class="btn btn-default" id="submit-employees" style="display: none">Submit</a>
											</label>
										</section>
									</div>
								</fieldset>
								<div id="employee-iras-form"></div>
								<footer>
									<button type="submit" id="generate-btn" class="btn btn-primary" disabled="disabled">
										Generate
									</button>
								</footer>
							</form>
						</div>
					</div>
				</div> <!-- end widget -->
			</div>
		</div>
	</section>
	
@endsection

@section('page-script')

<script type="text/javascript" src="{{ url('/js/generate-iras.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('keyup', '.active .compensation_for_loss', function(e){
			var self = $(e.currentTarget);
			if(self.val() != '' && self.val() != '0'){
				$('.active .show-if-compensation-for-loss').show();
				$('.active .show-if-compensation-for-loss input').attr('required','required');
			}
			else{
				$('.active .show-if-compensation-for-loss').hide();
				$('.active .show-if-compensation-for-loss input').removeAttr('required');
			}
		});

		$(document).on('keyup', '.active .retirement_accrued_1993', function(e){
			var self = $(e.currentTarget);
			if(self.val() != '' && self.val() != '0'){
				$('.active .show-if-retirement-benefits').show();
				$('.active .show-if-retirement-benefits input').attr('required','required');
			}
			else{
				$('.active .show-if-retirement-benefits').hide();
				$('.active .show-if-retirement-benefits input').removeAttr('required');
			}
		});

		$(document).on('keyup', '.active .excess_voluntary_cpf', function(e){
			var self = $(e.currentTarget);
			if(self.val() != '' && self.val() != '0'){
				$('.active .show-if-excess-voluntary').show();
				$('.active .show-if-excess-voluntary input').attr('required','required');
			}
			else{
				$('.active .show-if-excess-voluntary').hide();
				$('.active .show-if-excess-voluntary input').removeAttr('required');
			}
		});
	});
</script>

@endsection