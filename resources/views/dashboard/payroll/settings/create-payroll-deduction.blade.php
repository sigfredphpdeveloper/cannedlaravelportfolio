<div class="modal fade" tabindex="-1" role="dialog" id="add-payroll-addition">
    <div class="modal-dialog">
        <div class="modal-content">
            <div role="content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"> New Payroll Deduction </h4>
                </div>

                <form action="{{ url('new-payroll-deduction') }}" class="smart-form" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <fieldset>
                            <section>
                                <label for="title">Title</label>
                                <label class="input">
                                    <input type="text" class="form-control" name="title" required="required">
                                </label>
                            </section>
                            <section>
                                <label class="checkbox">
                                    <input type="checkbox" value="1" name="cpf_deductible">
                                    <i></i>
                                    CPF Deductible
                                </label>
                            </section>
                        </fieldset>
                    </div>
                    <div class="modal-footer tex-right">
                        <fieldset>
                            <section>
                                 <input type="submit" class="btn btn-primary" name="save_payroll_addition" value="save">
                            </section>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>