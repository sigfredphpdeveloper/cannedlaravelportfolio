<div class="modal fade" tabindex="-1" role="dialog" id="add-payroll-addition">
    <div class="modal-dialog">
        <div class="modal-content">
            <div role="content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"> Update Payroll Addition </h4>
                </div>

                <form action="{{ url('update-payroll-addition', $payroll_addition->id) }}" class="smart-form" method="POST">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <fieldset>
                            <section>
                                <label for="title">Title</label>
                                <label class="input">
                                    <input type="text" class="form-control" name="title" required="required" value="{{ $payroll_addition->title }}">
                                </label>
                            </section>
                            <section>
                                <div class="row" style="padding: 0 15px">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <label class="checkbox">
                                            <input type="checkbox" value="1" name="other_allowance" <?= $payroll_addition->other_allowance == 1 ? 'checked' : '' ?>>
                                            <i></i>
                                            Display this Addition in the “Other Allowance Section”
                                        </label>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <label class="checkbox">
                                            <input type="checkbox" value="1" name="cpf_payable" <?= $payroll_addition->cpf_payable == 1 ? 'checked' : '' ?> >
                                            <i></i>
                                            CPF Payable
                                        </label>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <label class="checkbox">
                                            <input type="checkbox" value="1" name="taxable" <?= $payroll_addition->taxable == 1 ? 'checked' : '' ?>>
                                            <i></i>
                                            taxable
                                        </label>
                                    </div>

                                </div>
                            </section>
                            <section id="tax-categories" <?= $payroll_addition->taxable == 1 ? '' : 'style="display:none"' ?>>
                                <label for="tax_category">Tax Category</label>
                                <label class="select">
                                    <select name="tax_category">
                                        @foreach($tax_categories as $tax_category)
                                            <option value="{{ $tax_category->id }}" <?= ( $payroll_addition->category_id == $tax_category->id ) ? 'selected' : '' ?> >{{ $tax_category->name }}</option>
                                        @endforeach
                                    </select>
                                    <i></i>
                                </label>
                            </section>
                        </fieldset>
                    </div>
                    <div class="modal-footer tex-right">
                        <fieldset>
                            <section>
                                 <input type="submit" class="btn btn-primary" name="save_payroll_addition" value="save">
                            </section>
                        </fieldset>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('input[name=taxable]').on('click', function(e){
        if($(this).is(":checked")){
            $('#tax-categories').slideDown('slow');
        } else {
            $('#tax-categories').slideUp('slow');
        }
    });
</script>