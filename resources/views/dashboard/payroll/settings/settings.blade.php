@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-md-12">
        <a href="{{ url('/admin_settings') }}" class="btn btn-xs btn-default"><i class="fa fa-caret-left"></i> Back To Settings</a>

        <br /><br />

            @if(Session::has('success'))
                <div class="alert alert-block alert-success">
                    <a class="close" data-dismiss="alert" href="#">×</a>
                    <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('success') }}</h4>
                </div>
            @elseif(Session::has('error'))
                 <div class="alert alert-block alert-danger">
                    <a class="close" data-dismiss="alert" href="#">×</a>
                    <h4 class="alert-heading"><i class="fa-fw fa fa-times"></i> {{ Session::get('error') }}</h4>
                </div>
            @elseif(Session::has('max-error'))
                <div class="alert alert-block alert-danger">
                    <a class="close" data-dismiss="alert" href="#">×</a>
                    <h4 class="alert-heading"><i class="fa-fw fa fa-times"></i> {{ Session::get('max-error') }}</h4>
                </div>
            @endif
        </div>

    </div>
    <!-- end row -->
    <br />
    <div class="row">
        <article class="col-sm-6 col-md-6 col-lg-6">
            <div class="well well-sm well-light">
                <form  id="" class="smart-form client-form" role="form"
                method="POST" action="">
                    {{ csrf_field() }}
                    <fieldset>
    		            <section>
    			    		<section class="col col-5">
            			         <label class="toggle">
            			               <input type="checkbox" name="payroll"
            			               value="1" <?php echo ($company['payroll']=='1')?'checked="checked"':'';?> >
            			               <i data-swchon-text="ON" data-swchoff-text="OFF"></i> Payroll
                                    </label>
    			             </section>
    		            </section>
                    </fieldset>
                    <footer>
                        <button type="submit" class="btn btn-primary">
                            Save
                        </button>
                    </footer>
                </form>
            </div>
        </article>
        <article class="col-sm-6 col-md-6 col-lg-6">
            <div class="well">

                    <form action="{{url('/save_role_permission_payroll')}}" method="POST" >

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <h3 >Payroll Permissions</h3>

                            <?php
                                $count = count($permissions);
                            ?>

                            @include('dashboard.layouts.permission_select')

                            <input type="submit" value="{{trans("leaves.Save")}}" class="btn btn-primary pull-right" data-toggle="tooltip" title="Save" />

                            <br style="clear:both;">
                    </form>

                </div>
        </article>




    </div>

    <br />
    <div class="devider"></div>
    <br />

    <div class="row">
        <article class="col-sm-6 col-md-6 col-lg-6">
            <div class="well well-sm well-light">
                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <h4>Payroll Additions</h4>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-6 text-right">
                        <a href="javascript:;" class="btn btn-primary" id="add-payroll-addition">Add Payroll Addition</a>
                    </div>
                </div>
                <br />
                <table class="table table-bordered table-striped" id="payroll-addition-table">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>CPF Payable</th>
                            <th>Taxable</th>
                            <th>Other Allowance</th>
                            <th>Tax Category</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($company->payroll_additions) < 1)
                            <tr>
                                <td colspan="4" align="center">
                                    No Payroll Additions
                                </td>
                            </tr>
                        @else
                            @foreach($company->payroll_additions as $payroll_addition)
                                <tr data-id="{{ $payroll_addition->id }}">
                                    <td>{{ $payroll_addition->title }}</td>
                                    <td>{{ $payroll_addition->cpf_payable == 1 ? 'Yes' : 'No' }}</td>
                                    <td>{{ $payroll_addition->taxable == 1 ? 'Yes' : 'No' }}</td>
                                    <td>{{ $payroll_addition->other_allowance == 1 ? 'Yes' : 'No' }}</td>
                                    <td>{{ $payroll_addition->taxable == 1 ? ucfirst($payroll_addition->tax_category->name) : '--' }}</td>
                                    <td>

                                        @if($payroll_addition->default_addition != 1)
                                        <button data-id="{{ $payroll_addition->id }}" class="btn btn-primary btn-sm edit_additional">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </article>

        <article class="col-sm-6 col-md-6 col-lg-6">
            <div class="well well-sm well-light">
                <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-6">
                        <h4>Payroll Deductions</h4>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-6 text-right">
                        <a href="javascript:;" class="btn btn-primary" id="add-payroll-deduction">Add Payroll Deduction</a>
                    </div>
                </div>
                <br />
                <table class="table table-bordered table-striped" id="payroll-duduction-table">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>CPF Deductible</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($company->payroll_deductions) < 1)
                            <tr>
                                <td colspan="2" align="center">
                                    No Payroll Deductions
                                </td>
                            </tr>
                        @else
                            @foreach($company->payroll_deductions as $payroll_addition)
                                <tr data-id="{{ $payroll_addition->id }}">
                                    <td>{{ $payroll_addition->title }}</td>
                                    <td>{{ $payroll_addition->cpf_deductible == 1 ? 'Yes' : 'No' }}</td>
                                    <td>
                                        <button data-id="{{ $payroll_addition->id }}" class="btn btn-primary btn-sm edit_deduction">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
        </article>
    </div>


</section>

@endsection

@section('page-script')
<script type="text/javascript" src="{{ url('/js/payroll-settings.js') }}"></script>
<style type="text/css">
    table tr{
        cursor: pointer;
    }
</style>
@endsection