@extends('dashboard.layouts.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('js/plugin/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
			<h1 class="page-title txt-color-blueDark">
				<i class="fa fa-money fa-fw "></i>
					Payroll History
			</h1>
		</div>
		@if( $is_admin )
		   <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4 pull-right text-right">
				<h3 class="page-title txt-color-blueDark">
						<a href="{{ url('/payroll-settings') }}" class="btn btn-primary">Manage</a>
				</h3>
			</div>
	    @endif
	</div>
	@if(Session::has('success'))
		<div class="alert alert-block alert-success">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('success') }}</h4>
		</div>
	@elseif(Session::has('error'))
		<div class="alert alert-block alert-danger">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('error') }}</h4>
		</div>
	@endif

	<!-- widget grid -->
	<section id="widget-grid" class="well well-sm">
		<div class="row">
			<div class="col-sm-12">
				<table id="payroll-history" class="table table-striped table-bordered" width="100%">
					<thead>
						<tr style="background-color: transparent !important; background-image: none !important;">
							<th class="hasinput">
								<!-- <input id="payroll-month" type="text" placeholder="Filter Month" class="form-control" data-dateformat="mm"> -->
								<select id="payroll-month" class="form-control" style="width: 100%">
									<option value="">--</option>
									@foreach($months as $month)
									<option value="{{ $month }}">{{ $month }}</option>
									@endforeach
								</select>
							</th>
							<th class="hasinput">
								<input id="payroll-year" type="text" placeholder="Filter Year" class="form-control" data-dateformat="yy">
							</th>
							<th class="hasinput" style="">
								<input type="text" class="form-control" placeholder="Filter Status" />
							</th>	
							<th></th>
						</tr>
						<tr>
							<th>Month</th>
							<th>Year</th>
							<th>Status</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						@foreach($payrolls as $payroll)
							<tr>
								<td>
									{{ $months[$payroll->month] }}
								</td>
								<td>{{ $payroll->year }}</td>
								<td>{{ ($payroll->status == 'saved') ? 'Generated' : 'Draft' }}</td>
								<td>
                                    @if($payroll->status == 'saved')
									    <a href="{{ url('payroll-item', $payroll->id) }}">View</a>
									@endif
									@if($payroll->status == 'draft')
											<a href="{{ url('/load_payroll/'.$payroll->id)}}">Edit</a>
									@endif
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</section>

@endsection

@section('page-script')

<!-- <script type="text/javascript" src="{{ url('/js/my-expenses.js') }}"></script> -->
<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{ url('js/plugin/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('js/plugin/datatables/dataTables.colVis.min.js') }}"></script>
<script src="{{ url('js/plugin/datatables/dataTables.tableTools.min.js') }}"></script>
<script src="{{ url('js/plugin/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ url('js/plugin/datatable-responsive/datatables.responsive.min.js') }}"></script>

<script type="text/javascript">
	$(document).ready(function($){
		var responsiveHelper_datatable_fixed_column = undefined;
		var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};

		var responsiveHelper_datatable_fixed_column = undefined;
		var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
		var otable = $('#payroll-history').DataTable({
			"order": [],
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_datatable_fixed_column) {
					responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#payroll-history'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
					responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_datatable_fixed_column.respond();
			}
		});

		// Apply the filter
		$("#payroll-history thead th input[type=text]").on( 'keyup change', function () {
		    otable
		        .column( $(this).parent().index()+':visible' )
		        .search( this.value )
		        .draw();
		} );

		$("#payroll-history thead th select").on( 'change', function () {
		    otable
		        .column( $(this).parent().index()+':visible' )
		        .search( this.value )
		        .draw();
		} );

	});
</script>

@endsection