<!--
Guide
https://github.com/barryvdh/laravel-dompdf
-->

<div>
	<div class="row">
		<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
			<h3 style="margin: 0">{{ $company->company_name }}</h3>
			<span>{{ $company->address }}</span><br />
			<span>{{ $company->zip }} {{ $company->city }}</span><br />
			<span><strong>Registration Number:</strong> {{ !empty($company->sg_company_details->org_id)?$company->sg_company_details->org_id:'' }}</span>
		</div>
		<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 text-right">
			<img src="{{ $company->logo }}" style="max-height:67px;"/>
		</div>
	</div>
	<br />
	<br />
	<div class="row">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 text-center">
			<h4>

            Payroll from {{ $months[$payroll->month] }} 1, {{ $payroll->year }} to  {{ $months[$payroll->month] }}
            <?php echo date('t',strtotime($payroll->year.'-'.$payroll->month.'-01'));?>, {{ $payroll->year }}

            </h4>
		</div>
	</div>
	<br />
	<br />
	<div class="row">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
			<?php
				$user = App\User::find($employee->user_id);
			?>
			{{ $user->employee_data->full_name_1 }}
			<br />
			<strong>Payment Date: </strong> {{ $payroll->payment_date }}
			<br />
			<strong>Mode of Payment: </strong> {{ $user->employee_data->salary_payment_mode }}
		</div>
	</div>
	<br />
	<br />
	<br />
	<br />
	<div class="row">
		<div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Item</th>
						<th>Amount</th>
						<th></th>
					</tr>
				</thead>

				<tbody>
					<tr>
						<td>
							@if( $user->employee_data->fee_frequency == 'monthly' )
								Basic Pay
							@elseif( $user->employee_data->fee_frequency == 'hourly' )
								Basic Pay: ( Hourly Rate {{ $user->employee_data->daily_rate }} *
                                Hours {{ $employee->no_frequency }} )
							@elseif( $user->employee_data->fee_frequency == 'daily' )

								Basic Pay: ( Daily Rate {{ $user->employee_data->daily_rate }} *
                                days {{ $employee->no_frequency }} )

							@endif
						</td>
						<td>
							@if( $user->employee_data->fee_frequency == 'monthly' )
								{{ number_format($user->employee_data->basic_pay, 2, '.', ',') }}
							@elseif( $user->employee_data->fee_frequency == 'hourly' )
								{{ number_format($user->employee_data->hourly_rate*$employee->no_frequency, 2, '.', ',') }}
							@elseif( $user->employee_data->fee_frequency == 'daily' )
								{{ number_format($user->employee_data->daily_rate*$employee->no_frequency, 2, '.', ',') }}
							@endif
						</td>
						<td>(A)</td>
					</tr>
					<tr>
						<td>
							Total Allowances <br />
							(Breakdown Shown Below)
						</td>
						<td>
							<?php
								$additions = 0;
							?>
							@foreach($employee->other_allowance->where('other_allowance',0) as $addition)
								<?php $additions += $addition->amount ?>
							@endforeach
							{{ number_format($additions, 2, '.', ',') }}
						</td>
						<td>(B)</td>
					</tr>

					@if(!empty($employee->other_allowance) && count($employee->other_allowance)>0)

                        @foreach($employee->other_allowance->where('other_allowance',0) as $additions)

                                <tr style="background: #f1f1f1">
                                    <td>{{ $additions->title }}</td>
                                    <td>{{ number_format($additions->amount, 2, '.', ',') }}</td>
                                    <td></td>
                                </tr>

                        @endforeach

                    @endif


					<tr>
						<td>
							Total Deductions <br />
							(Breakdown Shown Below)
						</td>
						<td>
							<?php
								$deductions = 0;
							?>
							@foreach($employee->item_deductions as $deduction)
								<?php $deductions += $deduction->amount ?>
							@endforeach
							{{ number_format($deductions, 2, '.', ',') }}
						</td>
						<td>(C)</td>
					</tr>
					@foreach($employee->item_deductions as $deduction)

                            @if(!empty($deduction->deduction))
                                <tr style="background: #f1f1f1">
                                    <td>{{ $deduction->deduction->title }}</td>
                                    <td>{{ number_format($deduction->amount, 2, '.', ',') }}</td>
                                    <td></td>
                                </tr>
                            @else
                                <tr style="background: #f1f1f1">
                                    <td>{{ $deduction->title }}</td>
                                    <td>{{ number_format($deduction->amount, 2, '.', ',') }}</td>
                                    <td></td>
                                </tr>
                            @endif
						
					@endforeach
					<tr>
						<td colspan="3"> &nbsp; </td>
					</tr>

					<tr>
						<td colspan="3">
							<strong>Overtime Details</strong>
						</td>
					</tr>

                    <tr>
						<td>Overtime Payment Period</td>
						<td>

                            @if($employee->overtime_from != '0000-00-00' AND $employee->overtime_from != NULL)
                                {{ date('F d Y',strtotime($employee->overtime_from)) }}
                            @endif

						</td>
						<td>

                            @if($employee->overtime_to != '0000-00-00' AND $employee->overtime_to != NULL)
                                {{ date('F d Y',strtotime($employee->overtime_to)) }}
                            @endif

                        </td>
					</tr>

					<tr>
						<td>Overtime Hours Worked</td>
						<td>{{ ($employee->overtime1_hours)+($employee->overtime2_hours) }}hrs</td>
						<td></td>
					</tr>

					<tr>
						<td>Total Overtime Pay</td>
						<td>{{ number_format(($employee->overtime1_rate * $employee->overtime1_hours)+($employee->overtime2_rate * $employee->overtime2_hours), 2, '.', ',') }}</td>
						<td>(D)</td>
					</tr>

					@if( ($employee->overtime1_rate * $employee->overtime1_hours) != 0 )
                        <tr style="background: #f1f1f1">
                            <td>Overtime 1</td>
                            <td>{{ number_format(($employee->overtime1_rate * $employee->overtime1_hours), 2, '.', ',') }}</td>
                            <td></td>
                        </tr>
                    @endif
                    @if( ($employee->overtime2_rate * $employee->overtime2_hours) != 0 )
                    <tr style="background: #f1f1f1">
                        <td>Overtime 2</td>
                        <td>{{ number_format(($employee->overtime2_rate * $employee->overtime2_hours), 2, '.', ',') }}</td>
                        <td></td>
                    </tr>
                    @endif

					<tr>
						<td>
							Other Additional Payments <br />
							(Breakdown Shown Below)
						</td>
						<td>
                            <?php
								$additions = 0;
							?>
							@foreach($employee->other_allowance->where('other_allowance',1) as $addition)
								<?php $additions += $addition->amount ?>
							@endforeach
							{{ number_format($additions, 2, '.', ',') }}
						</td>
						<td>(E)</td>
					</tr>

					@if(!empty($employee->other_allowance) && count($employee->other_allowance)>0)

					    @foreach($employee->other_allowance->where('other_allowance',1) as $additions)

                                <tr style="background: #f1f1f1">
                                    <td>{{ $additions->title }}</td>
                                    <td>{{ number_format($additions->amount, 2, '.', ',') }}</td>
                                    <td></td>
                                </tr>

					    @endforeach

					@endif


					<!-- breakdonw here -->

					<tr>
						<td>Net Pay (A+B-C+D+E)</td>
						<td>{{ number_format($employee->total_paid_salary, 2, '.', ',') }}</td>
						<td></td>
					</tr>
					<tr>
					<td colspan="3">
							<strong>CPF</strong>
						</td>
					</tr>

					<tr>
						<td>Employer's CPF Computation</td>
						<td>{{ number_format(round($employee->cpf_employer), 2, '.', ',') }}</td>
						<td></td>
					</tr>

				</tbody>
			</table>
		</div>
	</div>
</div>