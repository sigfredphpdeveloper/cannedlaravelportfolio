<!--
Guide
https://github.com/barryvdh/laravel-dompdf
-->

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<style>
.next_pahe {page-break-after: always;}
</style>

@foreach($records as $record)

	<div >
		<div class="row">
            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
                <h3 style="margin: 0">{{ $company->company_name }}</h3>
                <span>{{ $record['company']->address }}</span><br />
                <span>{{ $record['company']->zip }} {{ $record['company']->city }}</span><br />
                <span><strong>Registration Number:</strong> {{ !empty($record['company']->sg_company_details->org_id)?$record['company']->sg_company_details->org_id:'' }}</span>
            </div>
            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 text-right">
                <img src="{{ $record['company']->logo }}" style="max-height:67px;"/>
            </div>
        </div>
        <br />
        <br />
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12 text-center">
                <h4>Payslip for {{ $months[$record['payroll']->month] }} {{ $record['payroll']->year }}</h4>
            </div>
        </div>
        <br />
        <br />
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                <?php
                $user = App\User::find($record['employee']->user_id);
                ?>
                {{ $user->employee_data->full_name_1 }}
                <br />
                <strong>Payment Date: </strong> {{ $record['payroll']->payment_date }}
                <br />
                <strong>Mode of Payment: </strong> {{ $user->employee_data->salary_payment_mode }}
            </div>
        </div>
        <br />
        <br />
        <br />
        <br />
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Item</th>
                        <th>Amount</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                @if( $user->employee_data->fee_frequency == 'monthly' )
                                    Basic Pay
                                @elseif( $user->employee_data->fee_frequency == 'hourly' )

                                    ( Hourly Rate {{ $user->employee_data->daily_rate }} *
                                    Hours {{ $record['employee']->no_frequency }} )

                                @elseif( $user->employee_data->fee_frequency == 'daily' )

                                    ( Daily Rate {{ $user->employee_data->daily_rate }} *
                                    days {{ $record['employee']->no_frequency }} )

                                @endif
                            </td>
                            <td>
                                @if( $user->employee_data->fee_frequency == 'monthly' )

                                    {{ number_format($user->employee_data->basic_pay, 2, '.', ',') }}

                                @elseif( $user->employee_data->fee_frequency == 'hourly' )

                                    {{ number_format(($user->employee_data->hourly_rate*$record['employee']->no_frequency), 2, '.', ',') }}

                                @elseif( $user->employee_data->fee_frequency == 'daily' )

                                    {{ number_format(($user->employee_data->daily_rate*$record['employee']->no_frequency), 2, '.', ',') }}

                                @endif
                            </td>
                            <td>(A)</td>
                        </tr>
                    <tr>
                        <td>
                            Total Allowances <br />
                            (Breakdown Shown Below)
                        </td>
                        <td>
                            <?php
                                $additions = 0;
                            ?>
                            @foreach($record['employee']->other_allowance->where('other_allowance',0) as $addition)
                                <?php $additions += $addition->amount ?>
                            @endforeach
                            {{ number_format($additions, 2, '.', ',') }}
                        </td>
                        <td>(B)</td>
                    </tr>

                    @if(!empty($record['employee']->other_allowance) && count($record['employee']->other_allowance)>0)

                        @foreach($record['employee']->other_allowance->where('other_allowance',0) as $additions)

                                <tr style="background: #f1f1f1">
                                    <td>{{ $additions->title }}</td>
                                    <td>{{ number_format($additions->amount, 2, '.', ',') }}</td>
                                    <td></td>
                                </tr>

                        @endforeach

                    @endif


                    <tr>
                        <td>
                            Total Deductions <br />
                            (Breakdown Shown Below)
                        </td>
                        <td>
                            <?php
                            $deductions = 0;
                            ?>
                            @foreach($record['employee']->item_deductions as $deduction)
                                <?php $deductions += $deduction->amount ?>
                            @endforeach
                            {{ number_format($deductions, 2, '.', ',') }}
                        </td>
                        <td>(C)</td>
                    </tr>
                    @foreach($record['employee']->item_deductions as $deduction)

                        @if(!empty($deduction->deduction))
                            <tr style="background: #f1f1f1">
                                <td>{{ $deduction->deduction->title }}</td>
                                <td>{{ number_format($deduction->amount, 2, '.', ',') }}</td>
                                <td></td>
                            </tr>
                        @else
                            <tr style="background: #f1f1f1">
                                <td>{{ $deduction->title }}</td>
                                <td>{{ number_format($deduction->amount, 2, '.', ',') }}</td>
                                <td></td>
                            </tr>
                        @endif

                    @endforeach
                    <tr>
                        <td colspan="3"> &nbsp; </td>
                    </tr>

                    <tr>
                        <td colspan="3">
                            <strong>Overtime Details</strong>
                        </td>
                    </tr>
                    <tr>
                        <td>Overtime Hours Worked</td>
                        <td>{{ ($record['employee']->overtime1_hours)+($record['employee']->overtime2_hours) }}hrs</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Total Overtime Pay</td>
                        <td>{{ number_format(($record['employee']->overtime1_rate * $record['employee']->overtime1_hours)+($record['employee']->overtime2_rate * $record['employee']->overtime2_hours), 2, '.', ',') }}</td>
                        <td>(D)</td>
                    </tr>

                    @if( ($record['employee']->overtime1_rate * $record['employee']->overtime1_hours) != 0 )
                        <tr style="background: #f1f1f1">
                            <td>Overtime 1</td>
                            <td>{{ number_format(($record['employee']->overtime1_rate * $record['employee']->overtime1_hours), 2, '.', ',') }}</td>
                            <td></td>
                        </tr>
                    @endif
                    @if( ($record['employee']->overtime2_rate * $record['employee']->overtime2_hours) != 0 )
                        <tr style="background: #f1f1f1">
                            <td>Overtime 2</td>
                            <td>{{ number_format(($record['employee']->overtime2_rate * $record['employee']->overtime2_hours), 2, '.', ',') }}</td>
                            <td></td>
                        </tr>
                    @endif


                    <tr>
                        <td>
                            Other Additional Payments <br />
                            (Breakdown Shown Below)
                        </td>
                        <td>
                            <?php
                                $additions = 0;
                            ?>
                            @foreach($record['employee']->other_allowance->where('other_allowance',1) as $addition)
                                <?php $additions += $addition->amount ?>
                            @endforeach
                            {{ number_format($additions, 2, '.', ',') }}
                        </td>
                        <td>(E)</td>
                    </tr>

                    @if(!empty($record['employee']->other_allowance) && count($record['employee']->other_allowance)>0)

                        @foreach($record['employee']->other_allowance->where('other_allowance',1) as $additions)

                                <tr style="background: #f1f1f1">
                                    <td>{{ $additions->title }}</td>
                                    <td>{{ number_format($additions->amount, 2, '.', ',') }}</td>
                                    <td></td>
                                </tr>

                        @endforeach

                    @endif


                                <!-- breakdonw here -->

                        <tr>
                            <td>Net Pay (A+B-C+D+E)</td>
                            <td>{{ number_format($record['employee']->total_paid_salary, 2, '.', ',') }}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <strong>CPF</strong>
                            </td>
                        </tr>

                        <tr>
                            <td>Employer's CPF Computation</td>
                            <td>{{ number_format(round($record['employee']->cpf_employer), 2, '.', ',') }}</td>
                            <td></td>
                        </tr>

                    </tbody>
                </table>
            </div>
        </div>
	</div>
@endforeach