@extends('dashboard.layouts.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('js/plugin/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
			<h1 class="page-title txt-color-blueDark">
				<i class="fa fa-money fa-fw "></i>
					Manage Employees
			</h1>
		</div>
		@if( $is_admin )
		   <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4 pull-right text-right">
				<h3 class="page-title txt-color-blueDark">
						<a href="{{ url('/payroll-settings') }}" class="btn btn-primary">Manage</a>
				</h3>
			</div>
	    @endif
	</div>
	@if(Session::has('success'))
		<div class="alert alert-block alert-success">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('success') }}</h4>
		</div>
	@elseif(Session::has('error'))
		<div class="alert alert-block alert-danger">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('error') }}</h4>
		</div>
	@endif

	<!-- widget grid -->
	<section id="widget-grid" class="">

		<br />
		<div class="row">
			<div class="col-sm-12 col-md-12 col-xs-12">
				<a href="{{ url('payroll-new-employee') }}" class="btn btn-primary">Add New Employee</a>
			</div>
		</div>
		<br />
		<!-- row -->
		<div class="row">
			<div class="col-sm-12">
				<table id="all-sg-employees" class="table table-striped table-bordered" width="100%">
					<thead>
						<tr>
							<th>Name</th>
							<th>Job Title</th>
							<th>Nationality</th>
							<th>Employment Date</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($company->payroll_employees as $employee)
							<tr>
								<td>{{ $employee->full_name_1 }}</td>
								<td>{{ $employee->job_title }}</td>
								<td>{{ (!empty($nationalities[$employee->Nationality]))?$nationalities[$employee->Nationality]:'N/A' }}</td>
								<td>{{ $employee->employment_start_date }}</td>
								<td><a href="{{ url('payroll-update-employee', $employee->id) }}">Edit</a> | <a href="{{ url('payroll-delete-employee', $employee->id) }}" onclick="return confirm('Are you sure?')" class="text-danger">Delete</a></td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</section>

@endsection

@section('page-script')

<!-- <script type="text/javascript" src="{{ url('/js/my-expenses.js') }}"></script> -->
<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{ url('js/plugin/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('js/plugin/datatables/dataTables.colVis.min.js') }}"></script>
<script src="{{ url('js/plugin/datatables/dataTables.tableTools.min.js') }}"></script>
<script src="{{ url('js/plugin/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ url('js/plugin/datatable-responsive/datatables.responsive.min.js') }}"></script>

<script type="text/javascript">
	$(document).ready(function($){
		var responsiveHelper_datatable_fixed_column = undefined;
		var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};

		$('#all-sg-employees').DataTable();
	});
</script>

@endsection