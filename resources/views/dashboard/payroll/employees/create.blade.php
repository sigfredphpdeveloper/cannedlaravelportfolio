@extends('dashboard.layouts.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('js/plugin/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
			<h1 class="page-title txt-color-blueDark">
				<i class="fa fa-money fa-fw "></i> 
					<a href="{{ url('payroll-manage-employees') }}">Manage Employees</a> <span>> Create New Employee</span>
			</h1>
		</div>
		@if( $is_admin )
		   <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4 pull-right text-right">
				<h3 class="page-title txt-color-blueDark">
						<a href="{{ url('/payroll-settings') }}" class="btn btn-primary">Manage</a>
				</h3>
			</div>
	    @endif
	</div>
	@if(Session::has('success'))
		<div class="alert alert-block alert-success">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('success') }}</h4>
		</div>
	@elseif(Session::has('error'))
		<div class="alert alert-block alert-danger">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('error') }}</h4>
		</div>
	@endif



	<!-- widget grid -->
	<section id="widget-grid" class="">
		<!-- row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="jarviswidget" id="wid-id-3" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-togglebutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>New Employee Form</h2>
					</header>
					<div>
						<div class="jarviswidget-editbox"></div>
						<div class="widget-body no-padding">
							<form action="{{ url('payroll-new-employee') }}" method="post" class="smart-form" id="emp_form">
								{{ csrf_field() }}
								<header>Basic Information</header>
								<fieldset>
									<div class="row">
										<section class="col col-4">
											<label name="user_id">User</label>
											<label class="select">
												<select name="user_id" style="width: 100%;" required>
													<option value="">---</option>
													@foreach($users as $user)
														<option value="{{ $user->id }}" {{ (isset($employee_id) AND $employee_id == $user->id)?'selected':''  }}>{{ $user->first_name.' '.$user->last_name }}</option>
													@endforeach
												</select>
											</label>
										</section>
										<section class="col col-4">
											<label name="full_name_1">Full Name 1</label>
											<label class="input">
												<input type="text" name="full_name_1" placeholder="Enter Complete Name" required="required" maxlength="40" />
											</label>
										</section>
										<section class="col col-4">
											<label name="full_name_2">Full Name 2</label>
											<label class="input">
												<input type="text" name="full_name_2" placeholder="Enter Complete Name" maxlength="40" />
											</label>
										</section>
									</div>
									<div class="row">
										<section class="col col-4">
											<label for="dob">Date of Birth</label>
											<label class="input"> 
												<i class="icon-append fa fa-calendar"></i>
												<input type="text" name="dob" id="dob" placeholder="Birth Date"   required="required">
											</label>
										</section>
										<section class="col col-4">
											<label name="id_number">ID Number</label>
											<label class="input">
												<input type="text" name="id_number" placeholder="e.g. GR21412AGD4-2423"  maxlength="12"/>
											</label>
										</section>
										<section class="col col-4">
											<label name="id_type">ID Type</label>
											<label class="select">
												<select name="id_type" id="id_type" required="required">
													<option value="">---</option>
													<option value="NRIC">NRIC</option>
													<option value="FIN">FIN</option>
													<option value="IMS">IMS</option>
													<option value="WP">WP</option>
													<option value="MIC">MIC</option>
													<option value="Passport no">Passport no.</option>
												</select>
												<i></i>
											</label>
										</section>
									</div>
									<div class="row pr-date-wrap" style="display: none">
										<section class="col col-4">
										</section>
										<section class="col col-4">
										</section>
										<section class="col col-4">
											<label name="pr_date">PR Date</label>
											<label class="input">
												<i class="icon-append fa fa-calendar"></i>
												<input type="text" name="pr_date" id="pr_date" placeholder="Enter Date" class="pr_date" />
											</label>
										</section>
									</div>
									<div class="row">
										<section class="col col-6">
											<label name="address">Address</label>
											<label class="select">
												<select name="address" id="address_select" required="required">
													<option value="">---</option>
													<option value="local">Local</option>
													<option value="foreign">Foreign</option>
													<option value="local c/o">Local c/o</option>
												</select>
												<i></i>
											</label>
										</section>
										<section class="col col-6">
											<label name="block_house">Block House</label>
											<label class="input">
												<input type="number" name="block_house" placeholder="e.g. Genius Blockhouse Property" maxlength="10" step="1" />
											</label>
										</section>
									</div>
									<div class="row local-address" style="display: none">
										<section class="col col-4">
											<label name="street">Street</label>
											<label class="input">
												<input type="text" name="street" placeholder="e.g. 1234 Main Street" maxlength="32" />
											</label>
										</section>
										<section class="col col-4">
											<label name="level">Level</label>
											<label class="input">
												<input type="text" name="level" placeholder="e.g. 14" maxlength="3" />
											</label>
										</section>
										<section class="col col-4">
											<label name="unit">Unit</label>
											<label class="input">
												<input type="text" name="unit" placeholder="e.g. 24" maxlength="5"  />
											</label>
										</section>
									</div>
									<div class="row foreign-address" style="display: none">
										<section class="col col-4">
											<label name="line_1">Line 1</label>
											<label class="input">
												<input type="text" name="line_1" placeholder="e.g. 422-4324-123" maxlength="30" />
											</label>
										</section>
										<section class="col col-4">
											<label name="line_2">Line 2</label>
											<label class="input">
												<input type="text" name="line_2" placeholder="e.g. 422-4324-123" maxlength="30" />
											</label>
										</section>
										<section class="col col-4">
											<label name="line_3">Line 3</label>
											<label class="input">
												<input type="text" name="line_3" placeholder="e.g. 422-4324-123" maxlength="30" />
											</label>
										</section>
									</div>
									<div class="row">
										<section class="col col-3">
											<label name="postal_code">Postal Code</label>
											<label class="input">
												<input type="text" name="postal_code" placeholder="e.g. 6200" maxlength="6" />
											</label>
										</section>
										<section class="col col-3">
											<label name="country">Country</label>
											<label class="select">
												<select name="country" id="country_select" style="width: 100%;" class="select-2" required="required">
													@foreach(App\Http\Utilities\Country::countries() as $key => $country)
														<option value="{{ $key }}" <?= ($country == 'SINGAPORE') ? 'selected' : '' ?>>{{ $country }}</option>
													@endforeach
												</select>
											</label>
										</section>
										<section class="col col-3">
											<label name="nationality">Nationaliy</label>
											<label class="select">
												<select name="nationality" class="select-2" style="width: 100%" id="select_nationality" required="required">
													@foreach(App\Http\Utilities\Country::nationalities() as $key => $nationality)
														<option value="{{ $key }}" <?= ($nationality == 'SINGAPORE CITIZEN') ? 'selected' : '' ?>>{{ $nationality }}</option>
													@endforeach
												</select>
											</label>
										</section>
										<section class="col col-3">
											<label name="sex">Sex</label>
											<label class="select">
												<select name="sex">
													<option value="M">Male</option>
													<option value="F">Female</option>
												</select>
												<i></i>
											</label>
										</section>
									</div>
								</fieldset>
								<header>Employment Information</header>
								<fieldset>
									<div class="row">
										<section class="col col-6">
											<label name="employment_start_date">Employment Start Date</label>
											<label class="input">
												<i class="icon-append fa fa-calendar"></i>
												<input type="text" name="employment_start_date" id="employment_start_date" placeholder="Date employed"  required="required" />
											</label>
										</section>
										<section class="col col-6">
											<label name="probation_period">Probation Period</label>
											<label class="input">
												<input type="number" name="probation_period" placeholder="Number of probation days" />
											</label>
										</section>
									</div>
									<div class="row">
										<section class="col col-6">
											<label name="confirmation_date">Confirmation Date</label>
											<label class="input">
												<i class="icon-append fa fa-calendar"></i>
												<input type="text" name="confirmation_date" id="confirmation_date" placeholder="Date officially employed"  required="required" />
											</label>
										</section>
										<section class="col col-6">
											<label name="termination_date">Termination Date</label>
											<label class="input">
												<i class="icon-append fa fa-calendar"></i>
												<input type="text" name="termination_date" id="termination_date" placeholder=""  />
											</label>
										</section>
									</div>
									<section>
										<label class="textarea">
											<label for="termination_reason">Termination Reason</label>
											<textarea name="termination_reason" rows="4"></textarea>
										</label>
									</section>
									<div class="row">
										<section class="col col-6">
											<label name="job_title">Job Title</label>
											<label class="input">
												<input type="text" name="job_title" placeholder="" class="" required="required" />
											</label>
										</section>
										<section class="col col-6">
											<label name="fee_frequency">Fee Frequency</label>
											<label class="select">
												<select name="fee_frequency">
													<option value="monthly">Monthly</option>
													<option value="hourly">Hourly</option>
													<option value="daily">Daily</option>
												</select>
												<i></i>
											</label>
										</section>
									</div>
									<div class="row">
										<section class="col col-4">
											<label name="basic_pay">Basic Pay</label>
											<label class="input">
												<input type="number" step="any" name="basic_pay" placeholder="e.g 4000" class="" />
											</label>
										</section>
										<section class="col col-4">
											<label name="hourly_rate">Hourly Rate</label>
											<label class="input">
												<input type="number" step="any" name="hourly_rate" placeholder="e.g 8" class=""/>
											</label>
										</section>
										<section class="col col-4">
											<label name="daily_rate">Daily Rate</label>
											<label class="input">
												<input type="number" step="any" name="daily_rate" placeholder="e.g 25" class="" />
											</label>
										</section>
									</div>
									<div class="row">
										<section class="col col-3">
											<label>Overtime Entitlement</label>
											<div class="inline-group">
												<label class="radio">
													<input type="radio" name="overtime_entitlement" value="1" checked="">
													<i></i>Yes
												</label>
												<label class="radio">
													<input type="radio" name="overtime_entitlement" value="0">
													<i></i>No
												</label>
											</div>
										</section>
										<section class="col col-3">
											<label>CPF Entitlement</label>
											<div class="inline-group">
												<label class="radio">
													<input type="radio" name="cpf_entitlement" checked="" value="1">
													<i></i>Yes
												</label>
												<label class="radio">
													<input type="radio" name="cpf_entitlement" value="0">
													<i></i>No
												</label>
											</div>
											<select name="employee_group" class="form-control" style="display:none" >
												<option value="PrvSec">Private Sector Employees</option>
												<option value="PubXPG">Non-Pensionable Government Employees</option>
												<option value="PenGov">Pensionable Government Employees (pensionable component only)</option>
											</select>
										</section>
										<section class="col col-3">
											<label name="overtime_1_rate">Overtime 1 Rate</label>
											<label class="input">
												<input type="number" step="any" name="overtime_1_rate" placeholder="e.g 4000" class="" />
											</label>
										</section>
										<section class="col col-3">
											<label name="overtime_2_rate">Overtime 2 Rate</label>
											<label class="input">
												<input type="number" step="any" name="overtime_2_rate" placeholder="e.g 80" class=""/>
											</label>
										</section>
									</div>
									<div class="row">
										<section class="col col-6">
											<label name="funds_selection">Funds Selection</label>
											<div class="row">
												<div class="col col-6">
													<label class="checkbox">
														<input type="checkbox" name="funds_selection[]" class="funds_selection" value="CDAC">
														<i></i>CDAC
													</label>
													<label class="checkbox">
														<input type="checkbox" name="funds_selection[]" class="funds_selection" value="MBMF">
														<i></i>MBMF
													</label>
												</div>
												<div class="col col-6">
													<label class="checkbox">
														<input type="checkbox" name="funds_selection[]" class="funds_selection" value="ECF">
														<i></i>ECF
													</label>
													<label class="checkbox">
														<input type="checkbox" name="funds_selection[]" class="funds_selection" value="SINDA">
														<i></i>SINDA
													</label>
												</div>
											</div>
										</section>
										<section class="col col-6">
											<label name="salary_payment_mode">Salary Payment Mode</label>
											<label class="select">
												<select name="salary_payment_mode" required="required">
													<option value="cheque">Cheque</option>
													<option value="cash">Cash</option>
													<option value="bank transfer">Bank Transfer</option>
												</select>
												<i></i>
											</label>
										</section>
									</div>
									<div class="row" id="bank-details" style="display: none">
										<section class="col col-4">
											<label name="bank_acct_name">Bank Account Name</label>
											<label class="input">
												<input type="text" name="bank_acct_name">
											</label>
										</section>
										<section class="col col-4">
											<label name="bank">Bank Name</label>
											<label class="select">
												<select name="bank" class="select-2" style="width: 100%">
													@foreach(App\Http\Utilities\SGBanks::banks() as $key => $bank)
														<option value="{{ $key }}" >{{ $bank }}</option>
													@endforeach
												</select>
											</label>
										</section>
										<section class="col col-4">
											<label name="acct_number">Account Number</label>
											<label class="input">
												<input type="text" name="acct_number">
											</label>
										</section>
									</div>
								</fieldset>
								<footer>
									<input type="submit" value="Save Employee" class="btn btn-primary" />
								</footer>
							</form>

							  <div class="alert alert-block alert-danger" id="common_error" style="display:none;">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> <span id="common_error_msg"></span></h4>
    </div>

						</div>
					</div>
				</div> <!-- end widget -->
			</div>
		</div>
	</section>
	
@endsection

@section('page-script')

<!-- <script type="text/javascript" src="{{ url('/js/my-expenses.js') }}"></script> -->
<script type="text/javascript">
	$(document).ready(function(){
		$('#confirmation_date, #dob, #employment_start_date, #termination_date, .pr_date').datepicker({
				changeYear: true,
				changeMonth: true,
				yearRange: "-100:+0",
	            dateFormat: 'yy-mm-dd',
	            nextText: '>>',
	             prevText: '<<',
	            maxDate: '0'
			});
		$('select[name=user_id], .select-2').select2();

		$('select[name=address]').on('change', function(e){
			val = $(this).val();

			if( val == 'local' || val == 'local c/o' ){
				$('.foreign-address').slideUp();
				$('.local-address').slideDown();
			}
			else if( val == 'foreign' ){
				$('.local-address').slideUp();
				$('.foreign-address').slideDown();
			}
			else {
				$('.foreign-address').slideUp();
				$('.local-address').slideUp();
			}

		});


		$('select[name=id_type]').on('change', function(e){

		    var nationality = $('#select_nationality').val();

			if($(this).val() == 'NRIC'){
				$('.pr-date-wrap').slideDown();

				    if(nationality != '301'){
				        //$('.pr_date').attr('required', 'required');
				    }
			} else {
				$('.pr-date-wrap').slideUp();
				$('.pr_date').removeAttr('required');
			}
		});

		$('input[name=overtime_entitlement]').click(function(e){
			if($(this).val() == '0'){
				$('input[name=overtime_1_rate], input[name=overtime_2_rate]').attr('disabled','disabled');
			} else {
				$('input[name=overtime_1_rate], input[name=overtime_2_rate]').removeAttr('disabled');
			}
		});

		$('input[name=cpf_entitlement]').click(function(e){
			if($(this).val() == '0'){
				$('.funds_selection').parent().addClass('state-disabled');
				$('.funds_selection').attr('disabled','disabled');
			} else {
				$('.funds_selection').parent().removeClass('state-disabled');
				$('.funds_selection').removeAttr('disabled');
			}
			 
		});

		$('select[name=salary_payment_mode]').on('change', function(e){
			if($(this).val() == 'bank transfer'){
				$('#bank-details').slideDown();
			} else {
				$('#bank-details').slideUp();
			}
		});

        $('#emp_form').submit(function(e){

            //select_nationality pr_date id_type
            var nationality = $('#select_nationality').val();
            var pr_date = $('#pr_date').val();
            var id_type = $('#id_type').val();
            var dob = $('#dob').val();

            //singaporean
            if(nationality == '301'){

                if(id_type =='NRIC'){

                     $('#pr_date').val(dob);

                     $('#common_error').hide();

                }

            }else{

                 if(id_type =='NRIC' && pr_date == ''){
                       $('#common_error').show();
                       $('#common_error_msg').html('PR Date is required');

                       e.preventDefault();

                }


            }

            //if ID type = NRIC and PR date = empty and nationality = SG then PR date = DOB
            //if ID type = NRIC and PR date = empty and nationality != SG then error (need to fill the PR date)
        });

        $('#address_select').change(function(){

            var type = $(this).val();

            if(type =='local'){

                    $('#country_select').val('301').change();

            }

        });

	});
</script>

@endsection