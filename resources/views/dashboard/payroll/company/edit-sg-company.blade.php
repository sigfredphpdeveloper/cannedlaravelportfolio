@extends('dashboard.layouts.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('js/plugin/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
			<h1 class="page-title txt-color-blueDark">
				<i class="fa fa-money fa-fw "></i> 
					Company Information
			</h1>
		</div>
		@if( $is_admin )
		   <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4 pull-right text-right">
				<h3 class="page-title txt-color-blueDark">
						<a href="{{ url('/payroll-settings') }}" class="btn btn-primary">Manage</a>
				</h3>
			</div>
	    @endif
	</div>
	@if(Session::has('success'))
		<div class="alert alert-block alert-success">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('success') }}</h4>
		</div>
	@elseif(Session::has('error'))
		<div class="alert alert-block alert-danger">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('error') }}</h4>
		</div>
	@endif

	<!-- widget grid -->
	<section id="widget-grid" class="">
		<!-- row -->
		<div class="row">
			<div class="col-sm-12">
				<div class="jarviswidget" id="wid-id-3" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-togglebutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>Company Information Form</h2>
					</header>
					<div>
						<div class="jarviswidget-editbox"></div>
						<div class="widget-body no-padding">
							<form action="{{ url('payroll-company') }}" method="post" class="smart-form">
								{{ csrf_field() }}
								<input type="hidden" name="sg_company_id" value="{{ $sg_company->id }}">
								<header>Company Details</header>
								<fieldset>
									<div class="row">
										<section class="col col-4">
											<label name="org_id">Organization ID</label>
											<label class="input">
												<input type="text" name="org_id" placeholder="Enter Organization ID" required="required" value="{{ $sg_company->org_id }}" maxlength="12" />
											</label>
										</section>
										<section class="col col-4">
											<label name="authorized_person">Authorized Person</label>
											<label class="input">
												<input type="text" name="authorized_person" placeholder="Full Name" required="required" value="{{ $sg_company->authorized_person }}" maxlength="30" />
											</label>
										</section>
										<section class="col col-4">
											<label name="designation_authorized_person">Designation</label>
											<label class="input">
												<input type="text" name="designation_authorized_person" placeholder="Designation of Authorized Person" value="{{ $sg_company->designation_authorized_person }}" maxlength="30" />
											</label>
										</section>
									</div>
									<div class="row">
										<section class="col col-4">
											<label name="org_type">Organization Type</label>
											<label class="select">
												<select name="org_type"  style="width: 100%;"  required="required">
													<option value="">---</option>
													<option value="7" <?= $sg_company->org_type == '7' ? 'selected' : '' ?>>(UEN) Business Registration Number issued by ACRA</option>
													<option value="8" <?= $sg_company->org_type == '8' ? 'selected' : '' ?>>(UEN) Local Company Registration Number issued by ACRA</option>
													<option value="A" <?= $sg_company->org_type == 'A' ? 'selected' : '' ?>>(ASGD) Tax Refference Number assigned by IRAS</option>
													<option value="I" <?= $sg_company->org_type == 'I' ? 'selected' : '' ?>>(ITR) Income Tax Refference Number assigned by IRAS</option>
													<option value="U" <?= $sg_company->org_type == 'U' ? 'selected' : '' ?>>(UENO) Unique Entity Number Others (e.g. Foreign Company Registration Number)</option>
												</select>
												<i></i>
											</label>
										</section>
										<section class="col col-4">
											<label for="phone_number">Phone Number</label>
											<label class="input"> 
												<input type="text" name="phone_number" id="phone_number" placeholder="" value="{{ $sg_company->phone_number }}" maxlength="20">
											</label>
										</section>
										<section class="col col-4">
											<label name="email">Email</label>
											<label class="input">
												<input type="email" name="email" placeholder="e.g. johndoe@gmail.com" value="{{ $sg_company->email }}" maxlength="60" />
											</label>
										</section>
									</div>
								</fieldset>
								<footer>
									<input type="submit" value="Save Details" class="btn btn-primary" />
								</footer>
							</form>
						</div>
					</div>
				</div> <!-- end widget -->
			</div>
		</div>
	</section>
	
@endsection

@section('page-script')

<!-- <script type="text/javascript" src="{{ url('/js/my-expenses.js') }}"></script> -->

@endsection