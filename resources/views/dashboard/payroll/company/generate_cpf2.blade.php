@extends('dashboard.layouts.master')

@section('styles')
    <link rel="stylesheet" href="{{ asset('js/plugin/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('content')

<div class="row">
    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
        <h1 class="page-title txt-color-blueDark">
            <i class="fa fa-money fa-fw "></i>
            Generate CPF
        </h1>
    </div>
</div>

<div class="row">
	@if(Session::has('success'))
		<div class="alert alert-block alert-success">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('success') }}</h4>
		</div>
	@elseif(Session::has('error'))
		<div class="alert alert-block alert-danger">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('error') }}</h4>
		</div>
	@endif
</div>

<div class="row">

    <section id="widget-grid" class="">
        <article class="col-sm-12 col-md-12 col-lg-12">
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false">

                <header>
                    <span style="display:none;" class="widget-icon"> <i class="fa fa-check"></i> </span>
                </header>

                <div class="widget-body">
                    <form id="wizard-1" novalidate="novalidate" method="POST" action="{{ url('/generate-cpf') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />

                        <div id="bootstrap-wizard-1" class="col-sm-12" style="margin-top:20px;">

                            <div class="tab-content">

                                <!--- tab1 --->
                                <div class="tab-pane active" id="tab1">
                                    <br>
                                    <h3> Generate CPF for the month of  </h3>
                                    <div class="row">

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-calendar fa-lg fa-fw"></i></span>
                                                    <select name="year" class="form-control input-lg" id="cpf_year">
                                                        <option value="" selected="selected">Select Year</option>
                                                        @foreach($years as $year => $data)
                                                            <option value="{{$year}}">{{$year}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-calendar fa-lg fa-fw"></i></span>
                                                    <select name="month" class="form-control input-lg" id="cpf_month" disabled="disabled">
                                                        <option value="" selected="selected">Select Month</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <!--- tab2 --->
                                <div class="tab-pane" id="tab2">
                                    <br>
                                    <h3><strong>Step 2 </strong> - Select Employees  </h3>
                                    <div class="row">
                                        <img style="display:block;margin:0 auto;" src="{{url('img/ajax-loader.gif')}}">
                                    </div>
                                </div>



                            </div>

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <ul class="pager wizard no-margin">
                                            <li class="next">
                                               <input style="float:right;" type="submit" class="btn bnt-lg btn-success" value="Save" id="saver"/>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </form>
                </div>
            </div>

        </article>
    </section>

</div>



@endsection

@section('page-script')

<script type="text/javascript">
(function(){
    // Your base, I'm in it!
    var originalAddClassMethod = jQuery.fn.addClass;

    jQuery.fn.addClass = function(){
        // Execute the original method.
        var result = originalAddClassMethod.apply( this, arguments );

        // trigger a custom event
        jQuery(this).trigger('cssClassChanged');

        // return the original result
        return result;
    }
})();


var monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];

var years = <?=json_encode($years);?>;

$(document).ready(function(){

    $('header .jarviswidget-ctrls').hide();
    $('header .widget-toolbar').hide();


    $('#cpf_year').on('change', function(){
        val = $(this).val();
        var string = '';

        for(var x=0;years[val].length>x;x++){
            string += '<option value="'+years[val][x]+'">'+monthNames[(years[val][x]-1)]+'</option>';
        }

        $('#cpf_month').html(string);

        if(val == ''){
            $('#cpf_month').prop("disabled", true);
        }else{
            $('#cpf_month').prop("disabled", false);
            $("#cpf_month").trigger("change");
        }

    })

    $("#cpf_month").on('change', function(){
        var month = $('#cpf_month').val();
        var year = $('#cpf_year').val();

        $.ajax({
            type:'POST',
//            dataType:'JSON',
            url:'{{url('get-cpf-employees')}}',
            data:{'year':year, 'month':month, '_token':'{{ csrf_token() }}'},
            success:function(response){
                var string = '<div class="col-xs-offset-2 col-xs-10">';


                var total = 0;
                for(var x=0; response.length>x; x++){

                    if(response[x].cpf_entitlement == 1){

                        string += '<div class="checkbox"><label>';
                        string += '<input type="checkbox" name="employees[]" value="'+response[x].user_id+'" class="checkstatus checkbox style-0" checked="checked" disabled /> ';
                        string += '<span>' + response[x].full_name_1 + ' ' + response[x].full_name_2 + '</span>';
                        string += '</label></div>';

                        string += '<input type="hidden" name="employees[]" value="'+response[x].user_id+'" /> ';

                        total++;

                    }
                }

                string += '</div>';

                $('#total_employee').attr('data-value',total);
                $('#total_employee').val(total);

                $('#tab2 .row').html(string);
            }
        })
    });


    $('#tab2').on('change', '.checkstatus', function(){
        total_employee = parseInt($('#total_employee').val());

        if(this.checked){
            total_employee = total_employee + 1;
        }else{
            total_employee = total_employee - 1;
        }

        $('#total_employee').attr('data-value',total_employee);
        $('#total_employee').val(total_employee);
    })


    $(".keypresshere").keypress(function (e) {
        me = $(this);
         //if the letter is not digit then display error and don't type anything
         if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            me.parent().next().html("Digits Only").show().fadeOut("slow");
                   return false;
        }
    });

    $('#below').on('keyup', function(){
        value = $(this).val();
        morethan = $('#morethan').val();

        if(morethan == ""){ morethan = 0; }

        if(value != ""){
            $('#total_employee').val(parseInt($('#total_employee').data('value')) - (parseInt(morethan) + parseInt(value)));
        }else{
            $('#total_employee').val(parseInt($('#total_employee').data('value')) - parseInt(morethan));
        }

    })

    $('#morethan').on('keyup', function(){
        value = $(this).val();
        below = $('#below').val();

        if(below == ""){ below = 0; }

        if(value != ""){
            $('#total_employee').val(parseInt($('#total_employee').data('value')) - (parseInt(below) + parseInt(value)));
        }else{
            $('#total_employee').val(parseInt($('#total_employee').data('value')) - parseInt(below));
        }

    })


});

</script>
@endsection