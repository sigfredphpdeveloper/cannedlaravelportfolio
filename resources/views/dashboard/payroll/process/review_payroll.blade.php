@extends('dashboard.layouts.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('js/plugin/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
			<h1 class="page-title txt-color-blueDark">
				<i class="fa fa-money fa-fw "></i> 
					Recap Payslip Generation
			</h1>
		</div>
	</div>
@if(Session::has('success'))
    <div class="alert alert-block alert-success">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('success') }}</h4>
    </div>
@elseif(Session::has('error'))
    <div class="alert alert-block alert-danger">
        <a class="close" data-dismiss="alert" href="#">×</a>
        <h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('error') }}</h4>
    </div>
@endif

<!-- widget grid -->
<section id="widget-grid" class="">

    <br />
    <br />
    <!-- row -->
    <div class="row">
        <div class="col-sm-12">

        <!-- widget grid -->
        <section id="widget-grid" class="">

            <!-- row -->
            <div class="row">

                <!-- NEW WIDGET START -->
                <article class="col-sm-12 col-md-12 col-lg-12">

                    <form action="{{ url('/process_payroll_save_finalize') }}" method="POST" id="generate_payroll_form" >
                    <input type="hidden" name="payroll_id" value="{{ $payroll->id }}" />
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <div class="row">

                        <div class="col-md-3">
                            <h4>Payslip for the month of:</h4>
                        </div>

                        <div class="col-md-9">
                            <?php
                             $monthNum = $payroll->month;
                             $monthName = date("F", mktime(0, 0, 0, $monthNum, 10));
                             echo $monthName; // Output: May
                             ?>

                             <?php echo $payroll->year; ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <h4>Payment Date:</h4>
                        </div>

                        <div class="col-md-9">

                            <label class="input">
                                <i class="icon-append fa fa-calendar"></i>
                                <input type="text" name="payment_date" value="{{ ($payroll->payment_date != '0000-00-00' AND $payroll->payment_date != NULL)?$payroll->payment_date:'' }}" id="payment_date" placeholder="Enter Payment Date"  required="required">
                            </label>

                        </div>

                    </div>
                     <div class="row">
                        <div class="col-md-3">
                            <h4>Employees Details</h4>
                        </div>

                        <div class="col-md-9">


                        </div>

                    </div>

                     <div class="row">
                        <div class="col-md-12">

                          <table class="table">
                            <tr>
                                <th>Name</th>
                                <th>Basic</th>
                                <th>Additions</th>
                                <th>Deductions</th>
                                <th>Overtime 1</th>
                                <th>Overtime 2</th>
                                <th>CPF Employee</th>
                                <th>CPF Employer</th>
                                <th>Total Pay</th>
                            </tr>

                            @foreach($items as $item)

                            <tr>

                                <td>{{ $item->user->first_name }} {{ $item->user->last_name }}</td>
                                <td>

                                    @if($item->salary_frequency =='monthly')

                                    {{ number_format($item->salary_rate,2) }}

                                    @elseif($item->salary_frequency == 'daily')

                                    {{ number_format($item->salary_rate,2) }} x {{$item->no_frequency}} days

                                    @elseif($item->salary_frequency == 'hourly')

                                    {{ number_format($item->salary_rate,2) }} x {{$item->no_frequency}} hours

                                    @endif
                                </td>



                                <td>

                                    @if(!empty($item->item_additions))

                                        @foreach($item->item_additions as $add)

                                            <p>{{$add->addition->title}} ({{ number_format($add->amount,2)}})</p>

                                        @endforeach

                                    @endif

                                </td>
                                <td>
                                    @if(!empty($item->item_deductions))

                                        @foreach($item->item_deductions as $ded)


                                            @if(!empty($ded->title) && $ded->title != "employee cpf")

                                                @if(!empty($ded->deduction))
                                                    <p>{{$ded->deduction->title}} ({{ number_format($ded->amount,2)}})</p>
                                                @else
                                                    <p>{{$ded->title}} ({{ number_format($ded->amount,2)}})</p>
                                                @endif

                                            @endif


                                        @endforeach

                                    @endif

                                </td>

                                <td>

                                    @if($item->overtime1_hours != 0 AND $item->overtime1_hours != '')

                                        {{ $item->overtime1_rate .' x '. $item->overtime1_hours .' hours' }}

                                        {{ number_format(($item->overtime1_rate * $item->overtime1_hours),2) }}
                                    @endif

                                </td>

                                <td>

                                    @if($item->overtime2_hours != 0 AND $item->overtime2_hours != '')


                                        {{ $item->overtime2_rate .' x '. $item->overtime2_hours .' hours' }}

                                        {{ number_format(($item->overtime2_rate * $item->overtime2_hours),2) }}
                                    @endif

                                </td>

                                <td>{{$item->cpf_employee}}</td>
                                <td>{{$item->cpf_employer}}</td>
                                <td>{{ number_format($item->total_paid_salary,2) }}</td>

                            </tr>



                            @endforeach



                          </table>

                        </div>



                    </div>

                    @if($payroll->status == 'draft')
                        <a href="{{ url('/load_payroll/'.$payroll->id) }}" class="btn btn-primary">Edit</a>
                        <input type="button" id="generate_final" class="btn btn-primary" value="Generate" />
                    @endif

                    </form>

                </article>
                <!-- WIDGET END -->


            </div>

            <!-- end row -->

        </section>

        </div>
    </div>
</section>


<div class="modal fade" tabindex="-1" role="dialog" id="confirm">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Confirmation</h4>
      </div>
      <div class="modal-body">
        <p>Once the Payroll has been generated, it cannot be edited anymore. Please make sure everything is correct before proceeding</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
       <button type="button" class="btn btn-primary" id="proceed" alt="">Proceed</button>

      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

	
@endsection

@section('page-script')

<!-- <script type="text/javascript" src="{{ url('/js/my-expenses.js') }}"></script> -->
<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{ url('js/plugin/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('js/plugin/datatables/dataTables.colVis.min.js') }}"></script>
<script src="{{ url('js/plugin/datatables/dataTables.tableTools.min.js') }}"></script>
<script src="{{ url('js/plugin/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ url('js/plugin/datatable-responsive/datatables.responsive.min.js') }}"></script>




<script type="text/javascript">
	$(document).ready(function(){
		$('#payment_date').datepicker({
            changeYear: true,
            changeMonth: true,
            yearRange: "-100:+30",
            dateFormat: 'yy-mm-dd',
            nextText: '>>',
             prevText: '<<'
        });

        $('#generate_final').click(function(e){

            e.preventDefault();

            $('#confirm').modal('show');

        });

        $('#proceed').click(function(){

            $('#generate_payroll_form').submit();

        });

	});
</script>




@endsection