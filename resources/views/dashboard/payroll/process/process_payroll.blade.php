@extends('dashboard.layouts.master')

@section('styles')
<link rel="stylesheet" href="{{ asset('js/plugin/bootstrap-daterangepicker/daterangepicker.css') }}">
@endsection

<style type="text/css">

    .help-block{
        display:inline !important;
        font-size:12px;
    }

</style>

@section('content')
	<div class="row">
		<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
			<h1 class="page-title txt-color-blueDark">
				<i class="fa fa-money fa-fw "></i>
					Process Payroll
			</h1>
		</div>

	</div>
	@if(Session::has('success'))
		<div class="alert alert-block alert-success">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('success') }}</h4>
		</div>
	@elseif(Session::has('error'))
		<div class="alert alert-block alert-danger">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('error') }}</h4>
		</div>
	@endif

	<!-- widget grid -->
	<section id="widget-grid" class="">

		<br />

		<br />
		<!-- row -->
		<div class="row">
			<div class="col-sm-12">

<!-- widget grid -->
<section id="widget-grid" class="">

	<!-- row -->
	<div class="row">

		<!-- NEW WIDGET START -->
		<article class="col-sm-12 col-md-12 col-lg-12">

			<!-- Widget ID (each widget will need unique ID)-->
			<div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false">

				<header>
					<span style="display:none;" class="widget-icon"> <i class="fa fa-check"></i> </span>
				</header>

				<!-- widget div-->
				<div>

					<!-- widget edit box -->
					<div class="jarviswidget-editbox">
						<!-- This area used as dropdown edit box -->

					</div>
					<!-- end widget edit box -->

					<!-- widget content -->
					<div class="widget-body">

						<div class="row">
							<form id="payroll_form" novalidate="novalidate" method="POST" action="{{ url('/process_payroll_save') }}">

							<input type="hidden" name="_token" value="{{ csrf_token() }}" />

								<div id="bootstrap-wizard-1" class="col-sm-12">
									<div class="form-bootstrapWizard">
										<ul class="bootstrapWizard form-wizard">
											<li style="width:20%;" class="active" data-target="#step1">
												<a href="#tab1" data-toggle="tab"> <span class="step">1</span> <span class="title">Select Month / Year</span> </a>
											</li>
											<li style="width:20%;" data-target="#step2">
												<a href="#tab2" data-toggle="tab"> <span class="step">2</span> <span class="title">Select Employees</span> </a>
											</li>
											<li style="width:20%;" data-target="#step3">
												<a href="#tab3" data-toggle="tab"> <span class="step">3</span> <span class="title">Generate Payroll</span> </a>
											</li>
											<li style="width:20%;" data-target="#step4">
												<a href="#tab4" data-toggle="tab"> <span class="step">4</span> <span class="title">Additions</span> </a>
											</li>
											<li style="width:20%;" data-target="#step5">
												<a href="#tab5" data-toggle="tab"> <span class="step">5</span> <span class="title">Deductions</span> </a>
											</li>
										</ul>
										<div class="clearfix"></div>
									</div>
									<div class="tab-content">
										<div class="tab-pane active" id="tab1">
											<br>
											<h3><strong>Step 1 </strong> - Process Payroll for the month of  </h3>

											<div class="row">

												<div class="col-sm-6">
													<div class="form-group">
														<div class="input-group">
															<span class="input-group-addon"><i class="fa fa-calendar fa-lg fa-fw"></i></span>
															<select name="month" class="form-control input-lg">
																<option value="" selected="selected">Select Month</option>
                                                                @foreach($months as $k => $month)
                                                                    <option value="{{ $k }}" {{ ($k == date('m'))?'selected':'' }}>{{$month}}</option>
                                                                @endforeach
															</select>
														</div>
													</div>
												</div>

												<div class="col-sm-6">
													<div class="form-group">
														<div class="input-group">
															<span class="input-group-addon"><i class="fa fa-calendar fa-lg fa-fw"></i></span>
															<select name="year" class="form-control input-lg">
																<option value="" selected="selected">Select Year</option>
                                                                @for($i=date('Y',strtotime('-5 years'));$i <= date('Y',strtotime('+5 years'));$i++)
                                                                    <option value="{{ $i  }}" {{ ($i==date('Y'))?'selected':'' }}>{{ $i }}</option>
                                                                @endfor
															</select>
														</div>
													</div>
												</div>

											</div>

										</div>
										<div class="tab-pane" id="tab2">
											<br>
											<h3><strong>Step 2</strong> - Select Employees</h3>

											<div class="row">
												<div class="col-sm-8">
													<div class="form-group">
                                                        <label class="col-md-2 control-label">Employees</label>
                                                        <div class="col-md-10">

                                                            @foreach($employees as $employee)

                                                                @if($employee->first_name.$employee->last_name != '')

                                                                    <div class="checkbox">
                                                                        <label>
                                                                          <input type="checkbox" name="employees[]"
                                                                          value="{{$employee->id}}" class="employee_selects checkbox style-0"

                                                                          @if(!in_array($employee->id,$payroll_employees_user_ids)) {{ 'disabled' }}
                                                                          @endif

                                                                          >
                                                                          <span>
                                                                          <?=$employee->first_name;?> <?=$employee->last_name;?>

                                                                           @if(!in_array($employee->id,$payroll_employees_user_ids))
                                                                          (<b style="color:red;">Not yet on payroll
                                                                          <a href="{{ url('/payroll-new-employee/'.$employee->id) }}">add now</a></b>)
                                                                           @endif
                                                                          </span>
                                                                        </label>

                                                                    </div>

                                                                @endif

                                                            @endforeach

                                                        </div>
                                                    </div>
												</div>

											</div>


										</div>
										<div class="tab-pane" id="tab3">
											<br>
											<h3><strong>Step 3</strong> - Generate Payroll</h3>


                                            <div class="row">

												<div class="col-sm-12">

                                                        @foreach($payroll_employees as $pe)

                                                            <input type="hidden" name="cpf_entitlement[{{$pe->user_id}}]" value="{{!empty($pe->cpf_entitlement)?$pe->cpf_entitlement:'0'}}" />
                                                            <input type="hidden" name="cpf_employee_group[{{$pe->user_id}}]" value="{{!empty($pe->cpf_employee_group)?$pe->cpf_employee_group:''}}" />
                                                            <input type="hidden" name="dob[{{$pe->user_id}}]" value="{{!empty($pe->dob)?$pe->dob:''}}" />

                                                            <div class="row" id="employee_set_{{$pe->user_id}}" style="display:none;">


                                                                <div style="display:none;">

                                                                    <input type="text" name="basic_pay[{{$pe->user_id}}]" class="form-control inliner"
                                                                    value="{{ $pe->basic_pay}}" />
                                                                    <input type="text" name="hourly_rate[{{$pe->user_id}}]" class="form-control inliner"
                                                                    value="{{ $pe->hourly_rate}}" />
                                                                    <input type="text" name="daily_rate[{{$pe->user_id}}]" class="form-control inliner"
                                                                    value="{{ $pe->daily_rate}}" />

                                                                    <input type="text" name="salary_frequency[{{$pe->user_id}}]" class="form-control inliner"
                                                                    value="{{ $pe->fee_frequency}}" />


                                                                    <input type="text" name="overtime_1_rate[{{$pe->user_id}}]" class="form-control inliner"
                                                                    value="{{ $pe->overtime_1_rate}}" />


                                                                    <input type="text" name="overtime_2_rate[{{$pe->user_id}}]" class="form-control inliner"
                                                                    value="{{ $pe->overtime_2_rate}}" />

                                                                </div>


                                                                <div class="col-md-2">

                                                                 <p style="display:inline-block;">{{ $pe->full_name_1 }} {{ $pe->full_name_2 }}</p>

                                                                    @if($pe->fee_frequency == 'monthly')

                                                                        <p style="display:inline-block;">(Monthly)</p>

                                                                    @elseif($pe->fee_frequency == 'hourly')

                                                                        <p style="display:inline-block;">(Hourly)</p>

                                                                    @elseif($pe->fee_frequency == 'daily')
                                                                        <p style="display:inline-block;">(Daily)</p>

                                                                    @endif



                                                                </div>
                                                                <div class="col-md-10">

                                                                </div>

                                                                    @if($pe->fee_frequency == 'monthly')

                                                                    @elseif($pe->fee_frequency == 'hourly')

                                                                    <input type="number" required="required" name="hours[{{$pe->user_id}}]" class="form-control inliner" placeholder="No. of hours" />

                                                                    @elseif($pe->fee_frequency == 'daily')

                                                                    <input type="number" required="required" name="days[{{$pe->user_id}}]" class="form-control inliner" placeholder="No. of days" />

                                                                    @endif

                                                                   @if($pe->overtime_entitlement == 1)

                                                                     <input type="number" name="overtime1[{{$pe->user_id}}]" class="form-control overtime_hours inliner"
                                                                     placeholder="Overtime 1 ( hours )" />
                                                                     <input type="number" name="overtime2[{{$pe->user_id}}]" class="form-control overtime_hours inliner"
                                                                     placeholder="Overtime 2 ( hours )" />

                                                                     <div class="overtime_dates_div" style="display:none;">
                                                                      <input type="text" name="overtime_from[{{$pe->user_id}}]" class="form-control overtime_dates inliner2"
                                                                     placeholder=" Overtime From" />
                                                                      <input type="text" name="overtime_to[{{$pe->user_id}}]" class="form-control overtime_dates inliner2"
                                                                      placeholder=" Overtime To" />
                                                                      </div>

                                                                    @endif


                                                            </div>

                                                        @endforeach

												</div>

											</div>



										</div>
										<div class="tab-pane" id="tab4">
											<br>
											<h3><strong>Step 4</strong> - Additions</h3>

                                                <div id="place_here_1">

                                                    <div class="row" style="margin-top:20px;">

                                                        <div class="col-md-2">

                                                            <select class="form-control addition_titles" name="addition_title[]">

                                                                <option value="">Select Addition Title</option>

                                                                @foreach($additions as $av)
                                                                    <option value="{{$av->id}}">{{$av->title}}</option>
                                                                @endforeach

                                                            </select>


                                                        </div>

                                                         <div class="col-md-2">

                                                            <input type="number" name="addition_amount_0"  class="form-control addition_amounts" placeholder="Enter Amount"/>

                                                        </div>

                                                        <div class="col-md-1">

                                                        <p>For</p>

                                                        </div>


                                                        <div class="col-md-3">

                                                            <select multiple name="employees_additions_0[]" class="form-control addition_employees">

                                                                @foreach($payroll_employees as $pe)

                                                                    <option value="{{ $pe->user_id }}" class="employee_{{ $pe->user_id }}">{{ $pe->full_name_1 }} {{ $pe->full_name_2 }}</option>

                                                                @endforeach

                                                            </select>

                                                        </div>

                                                    </div>

                                                </div>


                                                <br />

                                                <a href="javascript:void(0);" id="adder1"><i class="fa fa-plus"></i> Add Another Addition</a>

                                                <div id="clone_here_1" style="display:none;">

                                                    <div class="row" style="margin-top:20px;">

                                                        <div class="col-md-2">

                                                            <select class="form-control addition_titles" name="addition_title[]" >

                                                                <option value="">Select Addition Title</option>

                                                                @foreach($additions as $av)
                                                                    <option value="{{$av->id}}">{{$av->title}}</option>
                                                                @endforeach

                                                            </select>

                                                        </div>

                                                         <div class="col-md-2">

                                                            <input type="number" name="" class="form-control addition_amounts" placeholder="Enter Amount"/>

                                                        </div>

                                                        <div class="col-md-1">

                                                        <p>For</p>

                                                        </div>


                                                        <div class="col-md-3">

                                                            <select multiple name="employees_additions_1[]" class="form-control addition_employees">

                                                                @foreach($payroll_employees as $pe)

                                                                    <option value="{{ $pe->user_id }}" class="employee_{{ $pe->user_id }}">{{ $pe->full_name_1 }} {{ $pe->full_name_2 }}</option>

                                                                @endforeach

                                                            </select>

                                                        </div>


                                                    </div>

                                                </div>

											<br>
										</div>

										<div class="tab-pane" id="tab5">
											<br>
											<h3><strong>Step 5</strong> - Deductions</h3>
											<br>

                                                    <div id="place_here_2">

                                                    <div class="row" style="margin-top:20px;">

                                                        <div class="col-md-2">

                                                            <select class="form-control deduction_titles" name="deduction_title[]">

                                                                <option value="">Select Deduction Title</option>

                                                                @foreach($deductions as $dv)
                                                                    <option value="{{$dv->id}}">{{$dv->title}}</option>
                                                                @endforeach

                                                            </select>

                                                        </div>

                                                         <div class="col-md-2">

                                                            <input type="number"  name="deduction_amount_0" class="form-control deduction_amounts" placeholder="Enter Amount"/>

                                                        </div>

                                                        <div class="col-md-1">

                                                        <p>For</p>

                                                        </div>


                                                        <div class="col-md-3">

                                                            <select multiple name="employees_deductions_0[]" class="form-control deduction_employees">

                                                                @foreach($payroll_employees as $pe)

                                                                    <option value="{{ $pe->user_id }}" class="employee_{{ $pe->user_id }}">{{ $pe->full_name_1 }} {{ $pe->full_name_2 }}</option>

                                                                @endforeach

                                                            </select>

                                                        </div>


                                                    </div>

                                                </div>


                                                <br />

                                                <a href="javascript:void(0);" id="adder2"><i class="fa fa-plus"></i> Add Another Deduction</a>

                                                <div id="clone_here_2" style="display:none;">

                                                    <div class="row" style="margin-top:20px;">

                                                        <div class="col-md-2">

                                                            <select class="form-control deduction_titles" name="deduction_title[]">

                                                                <option value="">Select Deduction Title</option>

                                                                @foreach($deductions as $dv)
                                                                    <option value="{{$dv->id}}" >{{$dv->title}}</option>
                                                                @endforeach

                                                            </select>



                                                        </div>

                                                         <div class="col-md-2">

                                                            <input type="number" name=""  class="form-control deduction_amounts" placeholder="Enter Amount"/>

                                                        </div>

                                                        <div class="col-md-1">

                                                        <p>For</p>

                                                        </div>


                                                        <div class="col-md-3">

                                                            <select multiple name="employees_deductions_1[]" class="form-control deduction_employees">

                                                                @foreach($payroll_employees as $pe)

                                                                    <option value="{{ $pe->user_id }}" class="employee_{{ $pe->user_id }}">{{ $pe->full_name_1 }} {{ $pe->full_name_2 }}</option>

                                                                @endforeach

                                                            </select>

                                                        </div>


                                                    </div>

                                                </div>



											<br>
										</div>



										<div class="form-actions">
											<div class="row">
												<div class="col-sm-12">
													<ul class="pager wizard no-margin">
														<!--<li class="previous first disabled">
														<a href="javascript:void(0);" class="btn btn-lg btn-default"> First </a>
														</li>-->
														<li class="previous disabled">
															<a href="javascript:void(0);" class="btn btn-lg btn-default"> Previous </a>
														</li>
														<!--<li class="next last">
														<a href="javascript:void(0);" class="btn btn-lg btn-primary"> Last </a>
														</li>-->
														<li class="next">
															<a href="javascript:void(0);" class="btn btn-lg txt-color-darken" id="nexter"> Next </a>
															<input style="display:none;float:right;" type="submit" class="btn bnt-lg btn-success"
															value="Save" id="saver2"/>
														</li>
													</ul>
												</div>
											</div>
										</div>

									</div>
								</div>
							</form>
						</div>

					</div>
					<!-- end widget content -->

				</div>
				<!-- end widget div -->

			</div>
			<!-- end widget -->

		</article>
		<!-- WIDGET END -->



	</div>

	<!-- end row -->

</section>
<!-- end widget grid -->





			</div>
		</div>
	</section>




@endsection

@section('page-script')

<!-- <script type="text/javascript" src="{{ url('/js/my-expenses.js') }}"></script> -->
<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{ url('js/plugin/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('js/plugin/datatables/dataTables.colVis.min.js') }}"></script>
<script src="{{ url('js/plugin/datatables/dataTables.tableTools.min.js') }}"></script>
<script src="{{ url('js/plugin/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ url('js/plugin/datatable-responsive/datatables.responsive.min.js') }}"></script>

<script type="text/javascript">
	$(document).ready(function($){
		var responsiveHelper_datatable_fixed_column = undefined;
		var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};

		$('#all-sg-employees').DataTable();
	});
</script>



<script type="text/javascript">
	/* DO NOT REMOVE : GLOBAL FUNCTIONS!
	 *
	 * pageSetUp(); WILL CALL THE FOLLOWING FUNCTIONS
	 *
	 * // activate tooltips
	 * $("[rel=tooltip]").tooltip();
	 *
	 * // activate popovers
	 * $("[rel=popover]").popover();
	 *
	 * // activate popovers with hover states
	 * $("[rel=popover-hover]").popover({ trigger: "hover" });
	 *
	 * // activate inline charts
	 * runAllCharts();
	 *
	 * // setup widgets
	 * setup_widgets_desktop();
	 *
	 * // run form elements
	 * runAllForms();
	 *
	 ********************************
	 *
	 * pageSetUp() is needed whenever you load a page.
	 * It initializes and checks for all basic elements of the page
	 * and makes rendering easier.
	 *
	 */

	// PAGE RELATED SCRIPTS

	// pagefunction

	var pagefunction = function() {

		// load bootstrap wizard

		loadScript("js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js", runBootstrapWizard);

		//Bootstrap Wizard Validations

		function runBootstrapWizard() {

			var $validator = $("#payroll_form").validate({

				rules : {
					email : {
						required : true,
						email : "Your email address must be in the format of name@domain.com"
					},
					fname : {
						required : true
					},
					lname : {
						required : true
					},
					country : {
						required : true
					},
					city : {
						required : true
					},
					postal : {
						required : true,
						minlength : 4
					},
					wphone : {
						required : true,
						minlength : 10
					},
					hphone : {
						required : true,
						minlength : 10
					}
				},

				messages : {
					fname : "Please specify your First name",
					lname : "Please specify your Last name",
					email : {
						required : "We need your email address to contact you",
						email : "Your email address must be in the format of name@domain.com"
					}
				},

				highlight : function(element) {
					$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
				},
				unhighlight : function(element) {
					$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
				},
				errorElement : 'span',
				errorClass : 'help-block',
				errorPlacement : function(error, element) {
					if (element.parent('.input-group').length) {
						error.insertAfter(element.parent());
					} else {
						error.insertAfter(element);
					}
				}
			});

			$('#bootstrap-wizard-1').bootstrapWizard({

				'tabClass' : 'form-wizard',
				'onNext' : function(tab, navigation, index) {
					var $valid = $("#payroll_form").valid();
					if (!$valid) {
						$validator.focusInvalid();
						return false;
					} else {
						$('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).addClass('complete');
						$('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).find('.step').html('<i class="fa fa-check"></i>');
					}
				}
			});

		};

		// load fuelux wizard

		loadScript("js/plugin/fuelux/wizard/wizard.min.js", fueluxWizard);

		function fueluxWizard() {

			var wizard = $('.wizard').wizard();

			wizard.on('finished', function(e, data) {
				//$("#fuelux-wizard").submit();
				//console.log("submitted!");
				$.smallBox({
					title : "Congratulations! Your form was submitted",
					content : "<i class='fa fa-clock-o'></i><i>1 seconds ago...</i>",
					color : "#5F895F",
					iconSmall : "fa fa-check bounce animated",
					timeout : 4000
				});

			});

		};

	};

	// end pagefunction

	// Load bootstrap wizard dependency then run pagefunction
	pagefunction();


        // Create a closure
        (function(){
            // Your base, I'm in it!
            var originalAddClassMethod = jQuery.fn.addClass;

            jQuery.fn.addClass = function(){
                // Execute the original method.
                var result = originalAddClassMethod.apply( this, arguments );

                // trigger a custom event
                jQuery(this).trigger('cssClassChanged');

                // return the original result
                return result;
            }
        })();

        $(document).ready(function(){
        // document ready function
            $("#tab3").bind('cssClassChanged', function(){
                //do stuff here

                employee_selects();
            });

         $("#tab5").bind('cssClassChanged', function(){

            $('#nexter').hide();
            $('#saver2').show();

        });

        $("#tab1,#tab2,#tab3,#tab4").bind('cssClassChanged', function(){

            $('#nexter').show();
            $('#saver2').hide();

        });







        function employee_selects(){

            $('.employee_selects').each(function(){
                var employee_id = $(this).val();

                if($(this).is(':checked')){

                    $('#employee_set_'+employee_id).show();
                    $('.employee_'+employee_id).show();

                }else{

                    $('#employee_set_'+employee_id).hide();
                    $('.employee_'+employee_id).hide();

                }
            });


        }

        $('.employee_selects').change(function(){

            employee_selects();

        });

        employee_selects();

        $('#adder1').click(function(){


            var html = $('#clone_here_1').find('.row').clone();

            $('#place_here_1').append(html);

            namings1();


        });

        $('#adder2').click(function(){

            var html = $('#clone_here_2').find('.row').clone();

            $('#place_here_2').append(html);

            namings2();

        });

        //addition_titles addition_amounts addition_employees
        //addition_title_1 addition_amount_1 employees_additions_1


        function namings1(){



            var count = 0;

            $('.addition_amounts:visible').each(function(){

                var name = $(this).attr('name');

                $(this).attr('name','addition_amount_'+count);

                count++;

            });

            var count = 0;

            $('.addition_employees:visible').each(function(){

                var name = $(this).attr('name');

                $(this).attr('name','employees_additions_'+count+'[]');

                count++;

            });



        }

         //deduction_titles deduction_amounts deduction_employees
        //deduction_title_1 deduction_amount_1 employees_deductions_1


        function namings2(){


            var count = 0;

            $('.deduction_amounts:visible').each(function(){

                var name = $(this).attr('name');

                $(this).attr('name','deduction_amount_'+count);

                count++;

            });

            var count = 0;

            $('.deduction_employees:visible').each(function(){

                var name = $(this).attr('name');

                $(this).attr('name','employees_deductions_'+count+'[]');

                count++;

            });

        }

         $('input').keydown( function(e) {
            var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
            if(key == 13) {
                e.preventDefault();
            }
        });


        $('.overtime_dates').datepicker({
            changeYear: true,
            changeMonth: true,
            yearRange: "-100:+25",
            dateFormat: 'yy-mm-dd',
            nextText: '>>',
             prevText: '<<'
        });


        $('.overtime_hours').keyup(function(){

            var v = $(this).val();

            if(v != ''){

                $(this).parent().find('.overtime_dates_div').show();

            }

        });


});

$(document).ready(function(){
    $('.jarviswidget-ctrls').hide();
    $('.widget-toolbar').hide();
})

</script>


@endsection