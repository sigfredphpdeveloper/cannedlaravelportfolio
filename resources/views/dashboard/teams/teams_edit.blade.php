@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-sm-12 col-md-12">
        <a href="{{ url('teams') }}" class="btn btn-primary">{{trans("manage_teams_edit.Back")}}</a>
        <h1>{{trans("manage_teams_edit.Edit Team")}}</h1>
        </div>

        <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>{{trans("manage_teams_edit.Warning.")}}<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <article class="col-sm-6 col-md-6 col-lg-6">

            <form  id="smart-form-register" class="smart-form client-form" role="form" method="POST" action="{{ url('/teams_edit') }}/{{$team['id']}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="user_id" value="{{ $user['id'] }}">

                <fieldset>

                     <section>
                        <label class="label">{{trans("manage_teams_edit.Team Name")}}</label>
                        <label class="input">
                        <input type="text" name="team_name" placeholder="{{trans("manage_teams_edit.Team Name")}}" value="{{ $team['team_name'] }}" />

                        </label>
                    </section>

                    <section>
                        <label class="label">{{trans("manage_teams_edit.Team Description")}}</label>
                        <label class="input">
                        <input type="text" name="team_description" placeholder="{{trans("manage_teams_edit.Team Description")}}" value="{{ $team['team_description'] }}" />

                        </label>
                    </section>

                </fieldset>

                <footer>
                    <button type="submit" class="btn btn-success">
                        {{trans("manage_teams_edit.Save")}}
                    </button>
                </footer>

            </form>

        </article>
    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->


@endsection