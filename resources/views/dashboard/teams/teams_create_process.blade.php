@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-sm-12 col-md-12">
        <h1>{{trans("add_team.Add Team")}}</h1>
        </div>

        <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">

                <p>
                {{trans("add_roles.Success Do you want")}}
                <a href="{{ url('/step5') }}">{{trans("add_roles.Next")}}</a>
                </p>

            </div>
        @endif
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>{{trans("add_roles.Warning")}}.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <article class="col-sm-6 col-md-6 col-lg-6">

            <form  id="smart-form-register" class="client-form form-horizontal" role="form" method="POST" action="{{ url('/teams_create_process_save') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="user_id" value="{{ $user['id'] }}">
            <input type="hidden" name="company_id" value="{{ $company['id'] }}">

                <div id="form_contains">

                    <fieldset>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">{{trans("add_team.Team Name")}}</label>
                            <div class="col-sm-8">
                            <input type="text" name="team_name[]" class="form-control" placeholder="{{trans("add_team.Team Name")}}" value="" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">{{trans("add_team.Team Description")}}</label>
                            <div class="col-sm-8">
                            <input type="text" name="team_description[]" class="form-control"  placeholder="{{trans("add_team.Team Description")}}" value="" />

                            </div>
                        </div>

                    </fieldset>

                </div>

                <footer>

                    <a href="javascript:void(0);" class="btn-default btn pull-left" id="add_more">
                        <i class="fa fa-plus-square"></i>  {{trans("add_team.Next")}}
                    </a>

                    <button type="submit" class="btn btn-success pull-right">
                        {{trans("add_team.Next")}}
                    </button>
                </footer>

            </form>




        </article>
    </div>

    <!-- end row -->

</section>



<div id="clone_from" style="display:none;">


        <fieldset>

            <a href="javascript:void(0);" class="btn btn-danger pull-left remover" style="cursor:pointer;">
                <i class="fa fa-remove"></i> {{trans("add_team.Remove")}}
            </a>

            <br style="clear:both;" />

             <div class="form-group">
                 <label class="col-sm-4 control-label">{{trans("add_team.Team Name")}}</label>
                 <div class="col-sm-8">
                 <input type="text" name="team_name[]" class="form-control" placeholder="{{trans("add_team.Team Name")}}" value="" />
                 </div>
             </div>

             <div class="form-group">
                 <label class="col-sm-4 control-label">{{trans("add_team.Team Description")}}</label>
                 <div class="col-sm-8">
                 <input type="text" name="team_description[]" class="form-control" placeholder="{{trans("add_team.Team Description")}}" value="" />

                 </div>
             </div>

        </fieldset>

</div>

<!-- end widget grid -->


<script type="text/javascript">

    $(document).ready(function(){

        //form_contains
        //clone_from

        $('#add_more').click(function(){

            //$("#from").clone().appendTo($("#to"));
            var clone = $('#clone_from').clone();

            $('#form_contains').append(clone);

            $('#form_contains').find('#clone_from').attr('id',false);

            $('#form_contains').children().show();

        });

        $(document).on('click','.remover',function(e){
            $(this).parent().remove();
        });

    });

</script>


@endsection