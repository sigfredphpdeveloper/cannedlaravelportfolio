@extends('dashboard.layouts.master')

@section('content')

<style>
.row .col-5 label.toggle{
    width: 165px;
}
.btn-primary{padding:6px 12px;}
</style>

    @include('errors.success')
    @include('errors.errors')


<article class="col-sm-6 col-md-6 col-lg-6" >
    <a href="{{ url('/admin_settings') }}" class="btn btn-xs btn-default"><i class="fa fa-caret-left"></i> Back To Settings</a>


    <form  id="smart-form-register" class="smart-form client-form" role="form"  method="POST" action="#">
        {!! csrf_field() !!}
        <input type="hidden" name="user_id" value="{{ $user['id'] }}">

        <fieldset>
            <div class="row">
                <section class="col col-xs-6">
                  <label class="toggle">
                   <input type="checkbox" name="contact_module" id="contact_module" value="1" {{ $company['contact_setting']==1?'checked="checked"':'' }}  >
                   <i data-swchon-text="ON" data-swchoff-text="OFF"></i>{{trans("organization_setting.Enable Contact Module")}}</label>
                 </section>

                @if($is_admin)
                    <a href="{{url('export-contacts/csv')}}" class="btn btn-primary btn-sm pull-right">
                        <span class="glyphicon glyphicon-export" aria-hidden="true"></span> Export (CSV)
                    </a>
                    <a href="{{url('export-contacts/xls')}}" class="btn btn-primary btn-sm pull-right" style="margin-right:10px;">
                        <span class="glyphicon glyphicon-export" aria-hidden="true"></span> Export (XLS)
                    </a>
                @endif
            </div>


            <h4>{{trans("organization_setting.Organization Custom Fields (Up to 5)")}}</h4>

            @if(count($organization_fields)<5)
                <button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#myModal">{{trans("organization_setting.Add")}}</button>
            @else
                <div class="alert alert-info">{{trans("organization_setting.Custom Fields are only up to 5. Delete of edit fields for new one")}}</div>
            @endif
            <br />
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>{{trans("organization_setting.Custom Field Name")}}</th>
                        <th>{{trans("organization_setting.Action")}}</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($organization_fields as $field)
                        <tr>
                            <td>{{$field['field_title']}}</td>
                            <td>

                                <a href="#" class="btn-edit" data-id="{{$field['id']}}"><i class="icon edit-icon"></i></a>
                                <a href="#" class="deleter" data-id="{{$field['id']}}"><i class="icon trash-icon"></i></a>
                            </td>
                        </tr>

                    @endforeach
                </tbody>
            </table>

        </fieldset>

    </form>

    <div style="margin-top:100px;">
        <form action="{{url('/save_role_permission_organization')}}" method="POST" >
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="col-md-12">

                <h1>{{trans("organization_setting.Organization Permissions")}}</h1>

                <?php
                $count = count($permissions);
                ?>

                @include('dashboard.layouts.permission_select')

            </div>

            <div class="col-md-12">
                <input type="submit" value="{{trans("manage_permissions.Save")}}" class="btn btn-primary" data-toggle="tooltip" title="Save" />
            </div>

        </form>
    </div>

</article>






<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
    <div class="modal-dialog" role="document">

        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="myModalLabel">{{trans("organization_setting.Add a new Custom Field For the Organization")}}</h4>
            </div>
            <div class="modal-body" >

                 <form action="{{ url('/create_organization_field/') }}" method="POST">
                    {!! csrf_field() !!}
                    <input type="hidden" name="user_id" value="{{ $user['id'] }}">

						<div class="form-gourp">
                            <label>{{trans("organization_setting.Title")}}</label>
                            <input class="form-control" type="text" name="field_title" value="" maxlength="30" placeholder="" />
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">{{trans("organization_setting.Save changes")}}</button>
                        </div>
                </form>

            </div>
        </div>
    </div>
</div>







<!-- Modal -->
<div class="modal fade" id="open_editmodal" tabindex="-1" role="dialog" aria-labelledby="open_editmodal">
    <div class="modal-dialog" role="document">

        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="myModalLabel">{{trans("organization_setting.Add a new Custom Field For the Organization")}}</h4>
            </div>
            <div class="modal-body" id="edit_organization_field_modal" >



            </div>
        </div>
    </div>
</div>

	
	

<div class="modal fade" tabindex="-1" role="dialog" id="confirm">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{trans("organization_setting.Confirmation")}}</h4>
      </div>
      <div class="modal-body">
        <p>{{trans("organization_setting.Are you sure you want to delete this record ?")}}" </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("organization_setting.No")}}</button>
       <button type="button" class="btn btn-primary" id="delete_proceed" alt="">{{trans("organization_setting.Yes")}}</button>

      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

	
	
	
	
	
	
	
@endsection


@section('page-script')

<script>
$(window).load(function(){
    $('#main').attr('style', 'height:100vh !important');
})
</script>
<script>
$(document).ready(function(){
	$('.btn-edit').on('click', function(){
      var id = $(this).data('id');

      $.ajax({
          url : '{{url('/organization_field_edit/')}}/'+id,
          type : 'GET',
          data : 'id='+id,
//            dataType:'json',
          success:function(html){
              $('#edit_organization_field_modal').html(html);
            $('#open_editmodal').modal('show');
          }

      });

	})




      $('.deleter').click(function(){

        var id = $(this).data('id');
        $('#delete_proceed').attr('alt',id);
        $('#confirm').modal('show');

    });

    $('#delete_proceed').click(function(){

        var id = $(this).attr('alt');

        $('#confirm').modal('hide');

        window.location.href='{{ url('organization_field_delete') }}/'+id;

    });

    $('#contact_module').on('change', function(){
        var value = $(this).is(':checked')?1:0;
//        alert(value);

         $.ajax({
              url : '{{url('contact_module_status')}}',
              type : 'POST',
              data : {'value':value, '_token':'{{ csrf_token() }}'},
              success:function(data){
				location.reload();
              }
         });
    })

    $('ol.breadcrumb').append('<li>Contacts & CRM</li><li>Settings</li>');

    $('#crm_module').on('click', function(){
        if($(this).is(':checked') && !$('#contact_module').is(':checked')){
            $('#contact_module').trigger('click');
        }
    })

})
</script>
@endsection 