<form action="{{ url('/organization_field_edit/') }}/{{$organization_field['id']}}" method="POST">
    {!! csrf_field() !!}
    <input type="hidden" name="user_id" value="{{ $user['id'] }}">

        <div class="form-gourp">
            <label>{{trans("organization_field_edit.Title")}}</label>
            <input class="form-control" type="text" name="field_title" value="{{$organization_field['field_title']}}" placeholder="" maxlength="30" />
            <input class="form-control" type="hidden" name="company_id" value="{{$organization_field['company_id']}}" placeholder="" />
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("organization_field_edit.Close")}}</button>
            <button type="submit" class="btn btn-primary">{{trans("organization_field_edit.Save changes")}}</button>
        </div>
</form>