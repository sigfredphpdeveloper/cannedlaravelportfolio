@extends('dashboard.layouts.master')

@section('content')

<script src="{{ url('js/plugin/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('js/plugin/datatable-responsive/datatables.responsive.min.js') }}"></script>

<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/data_tables.css') }}">
<!-- widget grid -->
<style>
table thead tr th {
    background-color: #F7DCB4 !important;
}

tfoot {
    display: table-header-group !important;
}
thead {
    display: table-row-group !important;
}

</style>


<style>
ul.pagination li{
    background: none !important;
}
.margin-bottom{
    margin-bottom: 50px;
}
.btn-default{
    background:#e7e7e7;
}
.left-form-group-dropdown{
    margin: 0;
    border: 0;
    padding: 0;
    width: 165px;
    -webkit-border-radius: 5px 0 0 5px !important;
    border-radius: 5px 0 0 5px !important;
}
.left-form-group-dropdown select{
    -webkit-border-radius: 5px 0 0 5px !important;
    border-radius: 5px 0 0 5px !important;
}
.btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}
#file_upload_form{
    margin-right:5px;
}
table.table tbody tr td{ font-size: 13px; }
</style>

    @include('errors.success')
    @include('errors.errors')


    <div class="col-xs-12 margin-bottom">
        <div class="col-sm-2" style="padding:0;">
            <select name="page" class="form-control" id="page-location">
                <option value="contacts" >{{trans("manage_organization.View Contacts")}}</option>
                <option value="organization" selected >{{trans("manage_organization.View Organizations")}}</option>
            </select>
        </div>
        <div class="col-sm-4 pull-right">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">{{trans("manage_organization.Add")}}</button>

            @if($is_admin)
                <a href="{{url('organization_setting')}}" class="btn btn-primary">{{trans("manage_organization.Customize")}}</a>
            @endif

            <form action="{{ url('upload_organization') }}" method="POST" id="file_upload_form" enctype="multipart/form-data" class="pull-left">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="user_id" value="{{ $user['id'] }}">
                <input type="hidden" name="redirect" value="organization">
                <span class="btn btn-primary btn-file" id="bulkupload" data-toggle="tooltip" data-placement="top" title="Please follow column header. Organization Name, Organization Address, Visibility">
                    <input type="file" name="upload" id="upload_excel" multiple >{{trans("contact.Bulk Upload")}}
                </span>
            </form>
        </div>
        <div class="col-sm-6 pull-right">
            <form action="{{ url('organization_search') }}" method="POST">
                <div class="input-group">
                        {!! csrf_field() !!}
                        <input type="hidden" name="user_id" value="{{ $user['id'] }}">
                        <div class="input-group-addon left-form-group-dropdown">
                            <select name="column_name" class="form-control" id="search_filter">
                                <option value="organization_name" <?=(!empty($post['column_name'])&&$post['column_name']=='contact_name')?'selected="selected"':'';?> >{{trans("manage_organization.Organization Name")}}</option>
                            </select>
                        </div>
                        <input type="text" name="search" class="form-control" value="{{!empty($post['search'])?$post['search']:''}}" placeholder="{{trans("manage_organization.Search for...")}}">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">{{trans("manage_organization.Search")}}</button>
                        </span>
                </div>
            </form>
        </div>

        <div class="col-xs-12" style="height:30px;padding-top:5px;">
            <b>{{trans_choice('manage_organization.organization_count_info', $total_org, ['total'=>$total_org])}}</b>
        </div>

        <table class="table table-striped datatable-dom-position">
            <thead>
                <tr>
                    <th>{{trans("manage_organization.Organization Name")}}</th>
                    <th>{{trans("manage_organization.Nb of Contact")}}</th>
                    <th>{{trans("manage_organization.Owner")}}</th>
                    <th>{{trans("manage_organization.Actions")}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($organizations as $organization)

                <tr>
                    <td>{{$organization['organization_name']}}</td>
                    <td>{{$organization['nb_contacts']}}</td>
                    <td>{{$organization['title']}} {{$organization['first_name']}} {{$organization['last_name']}}</td>
                    <td>
                        <button class="btn btn-info btn-sm btn-primary" data-id="{{$organization['organization_id']}}"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></button>
                        <button class="btn btn-danger btn-sm deleter" data-id="{{$organization['organization_id']}}"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>

                    </td>
                </tr>
                @endforeach

            </tbody>
            <tfooot>
                <tr>
                    <td colspan="4"> {!! $organizations->render() !!}</td>
                </tr>
            </tfooot>
        </table>

    </div>

<style>
.radio-inline{
    margin-left:10px;
}
.radio-inline input[type="radio"]{
    width:auto !important;
    height:auto;
}
</style>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModal">
    <div class="modal-dialog" role="document">

        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="myModalLabel">{{trans("manage_organization.Add a new Organization")}}</h4>
            </div>
            <div class="modal-body" >

                 <form action="{{ url('/organization/') }}" method="POST">
                    {!! csrf_field() !!}
                    <input type="hidden" name="user_id" value="{{ $user['id'] }}">


                        <div class="form-gourp">
                            <label>{{trans("manage_organization.Organization Name")}}</label>
                            <input class="form-control delete_fields" type="text" name="organization_name" value="" placeholder="" required />
                        </div>

                        <div class="form-gourp">
                            <label>{{trans("manage_organization.Organization Address")}}</label>
                            <input class="form-control delete_fields" type="text" name="organization_address" value="" placeholder="" />
                        </div>
                        @foreach($custom_fields as $custom_field)
                            <div class="form-gourp">
                                <label>{{$custom_field['field_title']}}</label>
                                <input class="form-control delete_fields" type="text" name="{{$custom_field['field_title']}}[]" value="" placeholder="" />
                            </div>
                        @endforeach

                        <div class="form-gourp">
                            <label>{{trans("manage_organization.Visibility")}}</label>
                                <label class="radio-inline">
                                    <input class="form-control " type="radio" name="visibility" value="me" /> {{trans("manage_organization.Me")}}
                                </label>
                                <label class="radio-inline">
                                    <input class="form-control " type="radio" name="visibility" value="team" />{{trans("manage_organization.Team")}}
                                </label>
                                <label class="radio-inline">
                                    <input class="form-control " type="radio" name="visibility" value="everyone" checked="checked" />{{trans("manage_organization.Every One")}}
                                </label>
                        </div>


                        <div class="modal-footer">
                            <button type="button" id="delete_text_field" class="btn btn-danger">{{trans("manage_organization.Delete")}}</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("manage_organization.Close")}}</button>
                            <button type="submit" class="btn btn-primary">{{trans("manage_organization.Save changes")}}</button>
                        </div>
                </form>

             </div>

        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="org_modal" tabindex="-1" role="dialog" aria-labelledby="org_modal">
    <div class="modal-dialog" role="document">

        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="myModalLabel">{{trans("organization_edit.Update Organization")}}</h4>
            </div>
            <div class="modal-body" id="org_modal_body">

             </div>
        </div>
    </div>
</div>


<div class="modal fade" tabindex="-1" role="dialog" id="confirm">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{trans("manage_organization.Confirmation")}}</h4>
      </div>
      <div class="modal-body">
        <p>{{trans("manage_organization.Are you sure you want to delete this record ?")}} </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("manage_organization.No")}}</button>
       <button type="button" class="btn btn-primary" id="delete_proceed" alt="">{{trans("manage_organization.Yes")}}</button>

      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@endsection
<script>
    $(window).load(function(){
        $('#main').attr('style', 'height:100vh !important');
    })
</script>


@section('page-script')
<script>
$(document).ready(function(){
    $('.btn-edit').click(function(){
        var id = $(this).data('id');

        $.ajax({
            url : '{{url('/organization_edit/')}}/'+id,
            type : 'GET',
            data : 'id='+id,
            success:function(html){
                $('#org_modal_body').html(html);
                $('#org_modal').modal('show');
            }

        });
    });


      $('.deleter').click(function(){

        var id = $(this).data('id');
        $('#delete_proceed').attr('alt',id);
        $('#confirm').modal('show');

    });

    $('#delete_proceed').click(function(){

        var id = $(this).attr('alt');

        $('#confirm').modal('hide');

        window.location.href='{{ url('organization_delete') }}/'+id;

    });

    $('ol.breadcrumb').append('<li>Organizations</li>');

    $('#page-location').on('change', function(){
        var val = $(this).val();

        if(val == 'contacts'){
            location.href ='{{ url("contact") }}';
        }else if(val == 'organization'){
            location.href ='{{ url("organization") }}';
        }
    })

    $('[data-toggle="tooltip"]').tooltip()

    $('#upload_excel').on('change', function(){
        $('#file_upload_form').submit();
    })


  var table =  $('.datatable-dom-position').DataTable({
           dom: '<"datatable-header length-left"><"datatable-scroll"t><"datatable-footer info-right"lp>'
       });


})
</script>
@endsection