 <form action="{{ url('/organization_edit/') }}/{{$organization['id']}}" method="POST">
       {!! csrf_field() !!}
       <input type="hidden" name="user_id" value="{{ $user['id'] }}">


       <div class="form-gourp">
           <label>{{trans("organization_edit.Organization Name")}}</label>
           <input class="form-control delete_fields" type="text" name="organization_name" value="{{$organization['organization_name']}}" placeholder="" required />
       </div>

       <div class="form-gourp">
           <label>{{trans("organization_edit.Organization Address")}}</label>
           <input class="form-control delete_fields" type="text" name="organization_address" value="{{$organization['organization_address']}}" placeholder="" />
       </div>

	   <?php 
	   $extra_fields_data = (array)json_decode($organization['extra_fields']);
	   // dd($extra_fields_data['Field_1']);
	   ?>
       @if(!empty($custom_fields))
            @foreach($custom_fields as $custom_field)
			    <div class="form-gourp">
                    <label>{{$custom_field['field_title']}}
					</label>
                    <input class="form-control delete_fields" type="text" name="{{$custom_field['field_title']}}[]" value="{{(!empty($extra_fields_data[str_replace(' ', '_', $custom_field['field_title'])])?$extra_fields_data[str_replace(' ', '_', $custom_field['field_title'])]:'')}}" placeholder="" />
                </div>
            @endforeach
       @endif

       <div class="form-gourp">
           <label>{{trans("organization_edit.Visibility")}}</label>
               <label class="radio-inline">
                   <input class="form-control " type="radio" name="visibility" value="me" <?=($organization['visibility']=='me')?'checked':'';?> />{{trans("organization_edit.Me")}}
               </label>
               <label class="radio-inline">
                   <input class="form-control " type="radio" name="visibility" value="team" <?=($organization['visibility']=='team')?'checked':'';?>/>{{trans("organization_edit.Team")}}
               </label>
               <label class="radio-inline">
                   <input class="form-control " type="radio" name="visibility" value="everyone" <?=($organization['visibility']=='everyone')?'checked':'';?> <?=($organization['visibility']=='')?'checked':'';?> />{{trans("organization_edit.Every One")}}
               </label>
       </div>


       <div class="modal-footer">
           <button type="button" id="delete_text_field" class="btn btn-danger">{{trans("organization_edit.Delete")}}</button>
           <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("organization_edit.Close")}}</button>
           <button type="submit" class="btn btn-primary">{{trans("organization_edit.Save changes")}}</button>
       </div>
</form>

<script>
$(document).ready(function(){
    $('#org_modal_body').on('click', '#delete_text_field', function(){
        $('.delete_fields').val('');
    })
})
</script>