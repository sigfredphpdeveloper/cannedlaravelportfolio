<?php 
function get_one_to_one_user($chat){
	if($chat->chat_type === "one-to-one" || $chat->chat_type === "empty"){
		foreach($chat->users as $oto_user){
			if($oto_user->id !== \Auth::user()->id){
				return $oto_user;
			}
		}
	}
	return 0;
}


function get_chat_photo($chat){
	if($chat->chat_type === "one-to-one" || $chat->chat_type === "empty"){
		$photo_url = get_one_to_one_user($chat)->photo;
		if(!$photo_url){
			$photo_url = asset('img/defaultIcon.png');
		}
		return $photo_url;
	}
	return asset('/img/default_group_chat.png');
}

function get_one_to_one_user_id($chat){
	$oto_user = get_one_to_one_user($chat);
	return ($oto_user ? $oto_user->id : $oto_user);
}

?>
@extends('dashboard.layouts.master')

@section('content')
<style type="text/css">
#chats{
	padding-left:0px;
}

#chats > li{
	list-style: none;
	cursor:pointer;
    margin-bottom: 5px;
    color: #eee;
    font-family: "Open Sans",Arial,Helvetica,Sans-Serif;
    padding-left: 10px;
    line-height: 35px;
    overflow: hidden;
    height: 35px;
}

#chats > li:hover{
    color: #fff;
    background-color: #444;
    border-right: 3px solid #111;
}

#chats > li:hover .profile-pic-sm{
    border:1px solid white;

}


#chat_container{
	padding-left: 0px;
}

#chats-container{
	position: relative;
	overflow: auto;
}

.chat-msg-body{
	overflow: auto;
	height: 800px;
}

.chat-title{
	margin: 0;
	padding-top:10px;
	padding-bottom: 5px;
	color: #eee;
}

.chat-user-list{
	padding:10px 0;
	margin:0;
}

.chat-user-list > li{
	list-style: none;
	display:inline;
}

.chat-user-list > li+li::before{
	content:", ";
}

.selected-chat{
	background: #adadad !important;
}

.chat-lines{
	color: #eee;
	padding:0;
	margin:0;
}

#chat-body::-webkit-scrollbar {
    width: 7px;
}

#chat-body::-webkit-scrollbar-track {
    
}

#chat-body::-webkit-scrollbar-track-piece {
	margin: 5px;
    background-color: #222;
    border-radius: 10px;
}

#chat-body::-webkit-scrollbar-button {
    
}

#chat-body::-webkit-scrollbar-thumb {
    background-color: #666;
    border-radius: 10px;
}


.chat-line{
	position: relative;
	list-style: none;
	padding-left:41px;
	margin-bottom: 10px;
}

.sender-profile-pic{
	margin-top: 5px;
	width:25px;
	height: 25px;
	position: absolute;
	top:0;
	left:0;
	border-radius: 5%;	
}

.sender-name{
    font-size: 12px;
    font-weight: 500;
    margin-right: 5px;
    color: #aaa;
}

.time-sent {
	font-style: italic;
	font-size: 10px;
	color: #bbb;
}

.text-content{
	display:block;
}

.unread-chat{
	font-weight: bold;
}

.chat-photo-wrapper{
	position: relative;
	display: inline-block;
}

.chat-user-online::after{
	content:"";
	border:1px solid white;
	border-radius: 50%;
	width:12px;
	height:12px;
	position: absolute;
	bottom: 2px;
	right: 2px;
	background: #6DC56D;
}

.textarea-controls > .btn{
	margin-left:5px;
}

.dz-message{
	color:#ADADAD;
}

.media-content{
}

.chat-file-link:link,
.chat-file-link:visited,
.chat-file-link:hover,
.chat-file-link:active{
	color: #3276b1;
	text-decoration: none;
}

.chat-file-link{
	position: relative;
	padding: 5px 5px 5px 60px;
	display:block;
	border:1px solid #D0D0D0;
	border-radius: 6px;
	max-width: 600px;
	margin:5px 0;
}

.chat-file-dl-icon{
	position: absolute;
	font-size:250%;
	top:10px;
	left:15px;
}

.chat-file-name{
	font-weight: bold;
	font-size:15px;
}

.chat-file-size{
	margin:0;
	font-size: 12px;
}

.upload-text{
	margin:0px;
	color:#848484;
}

.sticky-btn-bottom{
	position: absolute;
	bottom:0;
	top:800px;
	right:13px;
	left:13px;
}

.chat-list-title{
	border-bottom:2px solid #d3d3d3;
	padding:7px 0;
	margin-bottom: 10px;
}

.chat-list-title .profile-pic-sm{
	margin-left: 10px;
	border-radius: 0%;
	border:none;
}

.profile-pic-sm{
	width:36px;
	height:36px;
}

.chat-list-control{
	color:#777777;
	float:right;
}

#loading {
	position: absolute;
	padding: 50px;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background-color: white;
	text-align: left;
	font-size: 20px;
	font-weight: 100;
	font-family: Helvetica, sans-serif;
	z-index: 1;
}
.connection-lost {
	background-color: #f3e8c9;
}
.loading {
	background-color: #666;
    margin-top: -2px;
    margin-left: 2px;
    border-radius: 4px;
    padding: 2px;
}

.chat-users {
	padding-bottom: 15px;
	border-bottom:3px solid #666;
	font-style: italic;
	font-weight: 100;
}
.chat-status {
	color: #eee;
	font-style: italic;
}
.chat-users .label {
	margin:0 2px;
	font-weight: 100;
}
.retry {
	cursor: pointer;
	color: red;
	font-size: 12px;
	font-style: italic;
}
.chatContainer{
	background-color: #333;
    padding-left: 0px;
    padding-right: 10px;
    border-radius: 5px;
    font-family: "Open Sans",Arial,Helvetica,Sans-Serif;
}

.profile-pic-sm {
	width: 25px;
	height: 25px;
    background-color: #eee;
    border-radius: 100%;

}
.chat-left-nav {
	border-bottom-left-radius: 5px;
	border-top-left-radius: 5px;
	background-color: #dc4d17;
}

.hasGuest {
	background-color: #b93e10;
}

#searchResult {
	margin-top: 20px;
}
#searchResult h3 {
	font-family: "Open Sans",Arial,Helvetica,Sans-Serif;
	font-size: 20px;
	font-weight: 100;
	color: #eee;
}
#searchChatSubmit {
	height: 23px;
    width: 25px;
    padding: 0px;
    border-radius: 2px;
    line-height: 20px;
    margin-top: -4px;
    outline: none;
    border: 0px;
}
.settingsButtonContainer {
	display: inline-block;
	float: right;
	margin-right: 5px;
}
#searchChatBox {
	width: 100px;
    margin: 0px;
    margin-top: 15px;
    /* border-radius: 5px; */
    background-color: #333;
    color: #eee;
    border: 1px solid #999;
    padding: 2px;
}
.settingsButton {
	width: 20px;
	height: 20px;
	padding: 0px;
	border-radius: 100%;
	background-color: #dc4d17 !important;
	border-color: #dc4d17 !important;
}

.chat-left-nav:hover .settingsButton {
	background-color: #eee;
}

.settingsButton .span {
	font-size: 10px;
}
</style>

<div class="alert alert-warning">
	  <strong>Warning!</strong> Connection Lost.
	</div>
<div class="row" style="margin-bottom:20px;text-align:right;margin-right:5px;">
	
	<div class="col-md-5"></div>

</div>
<div class="chatContainer">
	<div class="row">
		<div class="col-md-3">
			<div class="chat-left-nav">
				<div class="chat-list-title">
					<img class="profile-pic-sm" src="{{Auth::user()->photo}}">
					
					<div class="dropdown settingsButtonContainer">
					  <button class="btn btn-default dropdown-toggle settingsButton" type="button" data-toggle="dropdown">	
					  <span class="glyphicon glyphicon-chevron-down"></span></button>
					  <ul class="dropdown-menu dropdown-menu-right">
					  	@if (!Auth::user()->guest_company())
					    <li><a title="Create new group" data-toggle="modal" data-target="#create-group-modal" href="#"><span class="glyphicon glyphicon-plus"></span> Create Group</a></li>
					    @endif
					    <li><a data-toggle="modal" data-target="#search-modal" href="#"><span class="glyphicon glyphicon-search"></span> Search</a></li>
					    <li><a  href="#"><span class="glyphicon glyphicon-flash"></span><span disabled class="js-push-button">Enable Push Notifications</span></a></li>
					  </ul>
					</div>
					<!--<span class="fa-stack fa-lg chat-list-control" title="Create new group" data-toggle="modal" data-target="#create-group-modal"><i class="fa fa-square-o fa-stack-2x"></i><i class="fa fa-plus fa-stack-1x"></i></span>
					-->
				</div>
				<div id="chats-container">
					<ul id="chats">
						@forelse($chats as $chat)
							@if ($chat->hasGuest)
							<li id="chat-{{$chat->id}}" class="chat hasGuest chat-user-{{get_one_to_one_user_id($chat)}}" data-chat-id="{{$chat->id}}" data-chat-type="{{$chat->chat_type}}" data-chat-users="{{implode(',',collect($chat->users)->pluck('id')->all())}}" data-chat-users-names="{{implode(',',collect($chat->users)->pluck('first_name')->all())}}" data-chat-name="{{$chat->chat_name}}">
							@else
							<li id="chat-{{$chat->id}}" class="chat chat-user-{{get_one_to_one_user_id($chat)}}" data-chat-id="{{$chat->id}}" data-chat-type="{{$chat->chat_type}}" data-chat-users="{{implode(',',collect($chat->users)->pluck('id')->all())}}" data-chat-users-names="{{implode(',',collect($chat->users)->pluck('first_name')->all())}}" data-chat-name="{{$chat->chat_name}}">
							@endif
								<div class="chat-photo-wrapper">
									<img class="profile-pic-sm" src="{{get_chat_photo($chat)}}" >
								</div>
								<span>{{$chat->chat_name}}</span>
								<img  class="loading" style="display:none" width="15px" src="{{asset('/img/loading.gif')}}" >
							</li>
						@empty
							<p style="text-align:center">You don't have any messages yet.</p>
						@endforelse
					</ul>
				</div>
			</div>
		</div>
		<div class="col-md-9" >
			<div 	 id="searchResult" rv-show="search_results.active">
				<h3>Search results: </h3>
				<ul class="chat-lines">
					<li rv-each-message="search_results.results" class="chat-line" >
						<img class="sender-profile-pic"  rv-profile-photo="message.user.photo"></img>
						<span class="sender-name">{message.user.first_name} {message.user.last_name}</span>
						<span class="sender-name">to {message.reciever.first_name} {message.reciever.last_name}</span>
						<span class="time-sent">{message.time_sent | date}</span>
						<div class="media-content" rv-if="message.media_content">
							<p class="upload-text">{{trans("chat.Uploaded a file")}}</p>
							<a class="chat-file-link" rv-href="message.media_content" target="_blank">
								<div class="fa fa-download chat-file-dl-icon"></div>
								<h4 class="chat-file-name">{message.media_name}</h4>
								<p class="chat-file-size">{message.media_size_in_bytes | displayMB}</p>
							</a>
						</div>
						<div class="text-content">{message.text_content}</div>
					</li>
					<li rv-display-if-array-empty="search_results.results">
							No results found.
						</li>
				</ul>
			</div>
			<div id="chat_container" rv-show="active_chat.chat_type" style="display:none;">
				<div id="loading">
					<div class="label" id="loading-centre">
						<!--<img  src="{{asset('/img/loading.gif')}}" > Loading...-->
					</div>
				</div>
				<div class="row">
					<div class="col-md-8">
						<h2 rv-text="active_chat.chat_name" class="chat-title"></h2>
					</div>
					<div class="col-md-4" id="searchByChatContainer">
						<div class="form-group" style="text-align:right">
						  <input type="text" class="" id="searchChatBox">
				          <a class="btn btn-default" id="searchChatSubmit" href="#"><span class="fa fa-search"></span></a>
				        </div>
					</div>
				</div>
				<div class="chat-users">
					<span rv-each-user="active_chat.users" class="label label-default" >
						{user.first_name} 
					</span>
				</div>
				
				<div id="chat-body" class="chat-msg-body">
					<div class="inflate-spacing"></div>

					<ul class="chat-lines">
						<li rv-each-message="active_chat.chat_lines" class="chat-line">
							<img class="sender-profile-pic" rv-profile-photo="message.user.photo"></img>
							<span class="sender-name">{message.user.first_name} {message.user.last_name}</span>
							<span class="time-sent">{message.time_sent | date}</span>
							<div class="media-content" rv-if="message.media_content">
								<p class="upload-text">{{trans("chat.Uploaded a file")}}</p>
								<a class="chat-file-link" rv-href="message.media_content" target="_blank">
									<div class="fa fa-download chat-file-dl-icon"></div>
									<h4 class="chat-file-name">{message.media_name}</h4>
									<p class="chat-file-size">{message.media_size_in_bytes | displayMB}</p>
								</a>
							</div>
							<div class="text-content">{message.text_content}</div>
							<div rv-show="message.error" onClick="retry(this)" class="retry">not send</div>
						</li>
						<li rv-display-if-array-empty="active_chat.chat_lines">
							{{trans("chat.This Chat is empty!")}}
						</li>
					</ul>
					<p class="chat-status">{active_chat.status}</p>
				</div>
				<div class="chat-footer">
					<div class="textarea-div">
						<div class="typearea">
							<textarea id="chat-message-text-content"></textarea>
						</div>
					</div>
					<span class="textarea-controls">
						<button onclick="sendMessage()" class="btn btn-primary pull-right">{{trans("chat.Send")}}</button>
						<button class="btn btn-primary pull-right" data-toggle="modal" data-target="#upload-chat-file-modal"><span class="fa fa-paperclip"></span></button>
					</span>
				</div>
			</div>
			<div id="chat-body-no-chat" class="col-md-9">
				<h2>{{trans("chat.Select a chat")}}</h2>
				<p>{{trans("chat.Choose a chat from the list on the left")}}</p>
			</div>
		</div>
	</div>
	<!-- <a href="#" class="border-rounded" data-toggle="modal" data-target="#upload-chat-file-modal">Upload</a> -->
	<!-- Modal -->
	<div class="modal fade" id="search-modal" tabindex="-1" role="dialog" >
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	      	<label for="searchBox">Search:</label>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      </div>
	      <div class="modal-body">
	  		<div class="form-group">
			  <input type="text" class="form-control" id="searchBox">
			</div>
	      <div class="modal-footer">
	        <input type="submit" class="btn btn-default" id="searchSubmit" value="Search" />
	      </div>
	    </div>
	  </div>
	</div>


	<div class="modal fade" id="upload-chat-file-modal" tabindex="-1" role="dialog" aria-labelledby="upload-chat-file-label">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="upload-chat-file-label">{{trans("chat.Send File")}}</h4>
	      </div>
	      <div class="modal-body">
	  		<!-- <div class="form-group">
	    		<label for="chat-filename">Filename</label>
	    		<input type="text" name="chat_filename" id="chat-filename" class="form-control"/>
	    	</div> -->
	    	<label for="chat-file-upload">{{trans("chat.Choose File")}}</label>
	        <form action="{{route('upload_chat_file')}}" class="dropzone form-group" id="chat-file-upload" enctype="multipart/form-data">
	        	
	        	{!! csrf_field() !!}
	        	<div class="dz-message">
	        		<span class="fa fa-plus-circle fa-5x"></span>
	        	</div>
				<div class="fallback">
					<input name="file" type="file" />
					<input type="submit" value="Submit" />
				</div>
	        </form>
	        <div class="form-group">
	    		<label for="chat-file-message">{{trans("chat.Message")}}</label>
	    		<textarea type="text" name="chat_file_message" id="chat-file-message" class="form-control" rows="3"></textarea>
	    	</div>
	        <div id="contact-return" class="bg-success" style="display:none;">
	            <p id="contact-return-message"></p>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <button class="btn btn-default" data-dismiss="modal">{{trans("chat.Close")}}</button>
	        <button id="send-chat-file" class="btn btn-primary">{{trans("chat.Send")}}</button>
	      </div>
	    </div>
	  </div>
	</div>


	<div class="modal fade" id="create-group-modal" tabindex="-1" role="dialog" aria-labelledby="create-group-label">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-body">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<div class="form-group">
			    		<label for="group-name">Group Name</label>
			    		<input type="text" name="group_name" id="group-name" class="form-control" />
			    	</div>	
			    	<ul class="nav nav-tabs" id="group-type">
					  <li class="active"><a data-toggle="tab" id="group-people" href="#users">Users</a></li>
					  <li><a data-toggle="tab" href="#teams" id="group-teams" >Teams</a></li>
					</ul>
			    	<div class="tab-content">
					  <div id="users" class="tab-pane fade in active">
					    <div id="group-people-list" class="form-group">
			    			<label for="group-name">Select People</label>
			    			<select multiple class="form-control" id="group-people-select">
			    				<option rv-each-person="people_list.people" rv-value="person.id">{person.first_name} {person.last_name}</option>
			    			</select>
			    		</div>
					  </div>
					  <div id="teams" class="tab-pane fade">
					  	<div id="group-teams-list" class="form-group">
						    <label for="group-name">Select Team</label>
				    		<select  class="form-control" id="group-teams-select">
				    			<option rv-each-team="teams_list.teams" rv-value="team.id">{team.team_name} </option>
				    		</select>
				    	</div>
					  </div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" onClick="createGroup()">Create Group</button>
				</div>
			</div>
		</div>
	</div>
	</div>
	<div id="sound" style="visibility:hidden"></div>
</div>
@endsection

@section('page-script')
<script type="text/javascript">
	var jwt = $("#jwt").attr("content");
	/********************************************************************************
	 * Active chat declaration
	 ********************************************************************************/
	var ActiveChat = function(active_chat){

		var assignValues = function(chat_obj){
			if(chat_obj){

				var keys = Object.keys(chat_obj);
				for(var i = 0; i < keys.length; i++){
					this[keys[i]] = chat_obj[keys[i]];
				}
				console.log(chat_obj.status);
				if (typeof chat_obj.status == "undefined") {
					console.log("in");
					this.status="";
				}
				this.init = false;
				this.position=0;
				//this will be updated after all of the other values
				//meaning any rivet bindings using more than the passed value
				//will have access to all of the fields
				this.update_count++;
			}else{
				//put default values
				this.position=0;
				this.chat_name = "";
				this.chat_type = false;
				this.users = [];
				this.chat_lines = [];
				this.chat_id = null;
				this.chat_intent = "one-to-one";
				this.status="";
				//use to track if this is the initial active chat object
				//if true no chat has been loaded yet
				this.init = true;
			}
		};

		this.update = function(update_chat){
			assignValues.call(this,update_chat);
		}

		this.no_chat_lines = function(){
			var chat = {};
			var keys = Object.keys(this);
			for(var i = 0; i < keys.length; i++){
				if(keys[i] != 'chat_lines' && typeof this[keys[i]] !== 'function'){
					chat[keys[i]] = this[keys[i]];
				}
			}

			return JSON.stringify(chat);
		}

		this.append = function(array){
			console.log(array);
			this.position++;
			this.chat_lines=array.chat_lines.concat(this.chat_lines);
			console.log(array.chat_lines);
			console.log(this.chat_lines);
		}

		this.update_count = 0;
		assignValues.call(this,active_chat);
	}

	var active_chat = new ActiveChat();
	$("#loading").hide();
	/********************************************************************************
	 * Chat functionality
	 ********************************************************************************/

	function drawChat(scrollToBottom){
		var $chat_body = $("#chat-body");
		var $chats_container = $("#chats-container");
		var $chat_lines = $(".chat-lines");
		var $inflate_spacing = $(".inflate-spacing");
		var $create_group_btn = $('.sticky-btn-bottom');

		//TODO fix chat body height calculation to be accurate
		var height = $(window).height() - 357;
		$chat_body.height(height);

		var chat_body_h = $chat_body.height();
		var chat_lines_h = $chat_lines.height();

		if(chat_lines_h < chat_body_h){
			//add padding to push chat_lines to bottom of chat_body
			$inflate_spacing.height(chat_body_h - chat_lines_h);
		}else{
			$inflate_spacing.height(0);
		}

		$chats_container.height($("#chat_container").height() - $('.chat-list-title').outerHeight());

		//if nothing passed or true passed
		//scroll to bottom of chat
		if(typeof scrollToBottom === 'undefined' || scrollToBottom){
			$chat_body.scrollTop($("#chat-body")[0].scrollHeight);
		}
	}

	function updateChatLine(chat_line){
		if(active_chat.id){
			var $el = $("#chat-" + active_chat.id);
			$el.parent().prepend($el);
		}

		active_chat.chat_lines.push(chat_line);

		drawChat();
	}

	function updateAfterMessage(notif){
		if(active_chat.chat_type == "empty"){
			//sent a message for an empty chat
			//update the appropriate fields
			active_chat.update({
				id:notif.chat.id,
				chat_type:notif.chat.chat_type
			})
		}
	}

	function getChatPhoto(chat){
		if(chat.chat_type === "one-to-one" || chat.chat_type === "empty"){
			var photo_url;
			for(var i = 0; i < chat.users.length; i++){
				if(chat.users[i].id !== current_user.id){
					photo_url = chat.users[i].photo;
				}
			}
			if(!photo_url){
				photo_url = "{{asset('img/defaultIcon.png')}}";
			}
			return photo_url;
		}
		return "{{asset('/img/default_group_chat.png')}}";
	}

	function sendMessage(message){
		//TODO ignore empty message
		if (message) {
			console.log("111111");
			var chat_message = message;
		} else {
			console.log("222222");
			var chat_message = document.getElementById("chat-message-text-content").value;
			document.getElementById("chat-message-text-content").value = "";
		}
		var data = {
			text:chat_message,
			jwt:jwt,
			active_chat:active_chat.no_chat_lines()
		};



		zen_ajax({
			url: zen_config.base_url + 'chat/send_message',
			data:data,
			method:"POST"
		})
		.done(function(data){
			//notification is returned
			//unsure what logic to implement here yet
			//confirm message was sent?
			active_chat.status="";
			updateChatLine({
				time_sent: moment().utc().format('YYYY-MM-DD HH:mm:ss'),
				text_content: chat_message,
				user:current_user,
				error:false
			});
			var notif = JSON.parse(data);
			updateAfterMessage(notif);
		})
		.fail(function(xhr,text,error){
			//TODO handle message send error
			//retry send message functionality?
			updateChatLine({
				time_sent: moment().utc().format('YYYY-MM-DD HH:mm:ss'),
				text_content: chat_message,
				user:current_user,
				error:true
			});
			console.log(error);
			console.log(text);
			console.log(xhr);
			console.error("Message failed to send");
		});
	}
	function enableNotif() {
	  if (!("Notification" in window)) {
	    console.log("This browser does not support desktop notification");
	    return false;
	  }
      else if (Notification.permission === "granted") {
	    return true;
	  }
	  else if (Notification.permission !== 'denied') {
	    Notification.requestPermission(function (permission) {
	      if (permission === "granted") {
	        return true;
	      } else {
	      	return false;
	      }
	    });
	  }
	}
	enableNotif();
	function notifyMe(title,message,icon) {
	  if (enableNotif()) {
	  	var options = {
		    body: message,
		    icon: icon,
		    isClickable: true,
		    sound: "{{asset('sounds/1.mp3')}}"
		}
	    var notification = new Notification(title,options);
	    notification.onclick= function() {
	    	window.focus(); 
	    	this.close();
	    } 
	    filename= "{{asset('sounds/1.mp3')}}";
        $("#sound").html('<audio autoplay="autoplay"><source src="' + filename + '" type="audio/mpeg" /><source src="' + filename + '.ogg" type="audio/ogg" /><embed hidden="true" autostart="true" loop="false" src="' + filename +'.mp3" /></audio>');
	  }
	}
	function createGroup(){
		console.log($("#group-type .active").children().attr("href"));
		
		if ($("#group-type .active").children().attr("href")=="#users") {
			var group_people = $("#group-people-select").val();
			console.log(group_people);

			if(group_people && group_people.length){
				var group_name = $("#group-name").val();
				var users = [];
				for(var i = 0; i < group_people.length; i++){
					users.push({
						id:group_people[i],
						first_name: $("#group-people-select [value="+group_people[i]+"]").html()
					});
				}
				console.log(group_name);

				$("#chat-body").show();
				$("#chat-body-no-chat").hide();

				new_chat = {
					id:null,
					chat_name: group_name,
					chat_type: 'empty',
					users: users,
					chat_lines: [],
					chat_id: null,
					chat_intent:'group'
				};

				active_chat.update(new_chat);
				$("#create-group-modal").modal('hide');
				$("#chat-message-text-content").focus();
			}
		};
		if ($("#group-type .active").children().attr("href")=="#teams") {
			console.log('nnn');
			var group_people = $("#group-teams-select").val();
			if(group_people && group_people.length){
				var group_name = $("#group-name").val();
				var users = [];
				console.log(teams_list.teams);
				
				for (var i = 0; i < teams_list.teams.length; i++) {
					if (teams_list.teams[i].id==group_people) {
						console.log("yess");
						var rawUsers= teams_list.teams[i].users;
						console.log(rawUsers);
					};
				};
				for (var i = 0; i < rawUsers.length; i++) {
					users.push({
						id: rawUsers[i].id,
						first_name: rawUsers[i].first_name
					});
				};
				//return true;
				
				console.log(group_name);

				$("#chat-body").show();
				$("#chat-body-no-chat").hide();

				new_chat = {
					id:null,
					chat_name: group_name,
					chat_type: 'empty',
					users: users,
					chat_lines: [],
					chat_id: null,
					chat_intent:'group'
				};

				active_chat.update(new_chat);
				$("#create-group-modal").modal('hide');
				$("#chat-message-text-content").focus();
			}
		};
		
		
	}

	$(window).resize(function(){
		var scrollToBottom = false;
		drawChat(scrollToBottom);
	});
	$("#searchSubmit").on("click", function(){
		console.log("n4444nn");
		var query= $("#searchBox").val();
		console.log(query);
		var data = {
			query:query,
			jwt:jwt,
			active_chat:active_chat.no_chat_lines()
		};
		zen_ajax({
			method:'POST',
			url:zen_config.base_url+'searchelastic',
			data: data
		})
		.done(function(data){

			console.log(data);
			search_results.results= data;
			search_results.active=true;
			active_chat.chat_type=false;
			$("#search-modal").modal("hide");
			console.log(search_results);
		})
		.fail(function(err){
			console.error(err);
			$("#search-modal").modal("hide");
		});
	});
	$("#searchChatSubmit").on("click", function(){
		console.log("n4444nn");
		var query= $("#searchChatBox").val();
		console.log(query);
		var data = {
			query:query,
			jwt:jwt,
			active_chat:active_chat.no_chat_lines(),
			chat_id:active_chat.id
		};
		zen_ajax({
			method:'POST',
			url:zen_config.base_url+'searchelasticchat',
			data: data
		})
		.done(function(data){

			console.log(data);
			search_results.results= data;
			search_results.active=true;
			active_chat.chat_type=false;
			console.log(search_results);
		})
		.fail(function(err){
			console.error(err);
		});
	});
	$("#chat-message-text-content").on("focus", function(){
		console.log(active_chat);
		if ($("#chat-"+active_chat.id).hasClass('unread-chat')) {
			console.log("read")
			$("#chat-"+active_chat.id).removeClass('unread-chat');
			zen_ajax({
				method:'POST',
				data:{
					jwt:jwt
				},
				url:zen_config.base_url+'read_chat/'+active_chat.id
			})
			.done(function(data){
			})
			.fail(function(err){
				console.error(err);
			});
		};
	});
	$("#chats").on("click", ".chat", function(e){
		$("#loading").show();
		search_results.active=false;
		var $selected_chat = $(this);
		$selected_chat.find(".loading").show();
		$("#chat-body").show();
		$("#chat-body-no-chat").hide();
		
		$('.selected-chat').removeClass('selected-chat');
		$selected_chat.removeClass('unread-chat');
		$selected_chat.addClass('selected-chat');

		if($selected_chat.data('chatType') == 'empty'){
			var users = [];
			var ids = $selected_chat.data("chatUsers").split(',');
			var names = $selected_chat.data("chatUsersNames").split(',');
			for(var i = 0; i < ids.length; i++){
				console.log(ids[i]);
				users.push({
					id:ids[i],
					first_name:names[i]
				});
			}
			var chat = {
				chat_type:'empty',
				users:users,
				chat_lines:[],
				chat_name:$selected_chat.data('chatName'),
				id:null,
				chat_intent:'one-to-one'
			}

			active_chat.update(chat);
			drawChat();
			$("#loading").hide();
			$selected_chat.find(".loading").hide();
		} else{
			var chat_id = $selected_chat.data('chatId');

			zen_ajax({
				url: zen_config.base_url + 'chat_messages/' + chat_id + "/0",
				data:{
					jwt:jwt
				},
				method:'POST'
			})
			.done(function(data){
				active_chat.update(JSON.parse(data));

				drawChat();
				$selected_chat.find(".loading").hide();
				$("#loading").hide();
			})
			.fail(function(err){
				//TODO handle failed get chat req
				console.error(err);
				$("#loading").hide();
				$selected_chat.find(".loading").hide();
			});
		}
	});
	function append(){
		zen_ajax({
				url: zen_config.base_url + 'chat_messages/' + active_chat.id + "/" + (active_chat.position+1),
				data:{
					jwt:jwt
				},
				method:'POST'
			})
			.done(function(data){
				console.log(data);
				active_chat.append(JSON.parse(data));
				drawChat();
				$("#chat-body").scrollTop(5);
			})
			.fail(function(err){
				//TODO handle failed get chat req
				console.error(err);
			});

	}
	$("#chat-body").scroll(function (event) {
	    var scroll = $("#chat-body").scrollTop();
	    console.log(scroll);
	    if (scroll==0) {
	    	append();
	    };
	});
	$("#chat-message-text-content").keypress(function(e){
		if($(this).is(":focus")){
			if(e.keyCode == 10 || e.keyCode == 13){
				e.preventDefault();
				sendMessage();
			}
		}
	});

	function retry(element){
		console.log('clicked');
		console.log($(element));
		console.log($(element).parents('.chat-line'));
		console.log($(element).parents('.chat-line').first());
		console.log($(element).parents('.chat-line').first().children('.text-content'));
		message= $(element).parents('.chat-line').first().children('.text-content').html();
		sendMessage(message);
	};

	
	$("#create-group-modal").on('show.bs.modal',function(e){
		zen_ajax({
			method:'GET',
			url:zen_config.base_url+'user/getUsersInCompanyExceptSelf/'+current_company.id
		})
		.done(function(data){
			var json = JSON.parse(data);
			console.log(json);
			people_list.people = json;
		})
		.fail(function(err){
			console.error(err);
		});
		zen_ajax({
			method:'GET',
			url:zen_config.base_url+'company/getTeamsInCompany/'+current_company.id
		})
		.done(function(data){
			var json = JSON.parse(data);
			console.log(json);
			teams_list.teams = json;
		})
		.fail(function(err){
			console.error(err);
		});
	});

	/********************************************************************************
	 * Dropzone setup
	 ********************************************************************************/

	Dropzone.autoDiscover = false;
	var chatFileUpload = new Dropzone("form#chat-file-upload", {
		url: "{{url('/chat/upload_file')}}",
		autoProcessQueue:false,
		maxFilesize: {{config('filesystems.chat_max_file_size_mb')}}, // MB
		maxFiles:1,
		addRemoveLinks: true,
		dictDefaultMessage:'{{trans("chat.Choose or drag file")}}'
	});

	function clearUploadChatFileForm(){
		$('#chat-file-message').val("");
		chatFileUpload.removeAllFiles();
	}

	chatFileUpload.on("sending", function(file, xhr, data) {

		var file_message = $('#chat-file-message').val();
		data.append("text", file_message);
		data.append("active_chat", active_chat.no_chat_lines());
		data.append("_token", csrf_token);
		data.append("jwt", jwt);
	});

	chatFileUpload.on("success", function(file, data) {
		var json = JSON.parse(data);
		console.log(json);
		if(json.event === 'chat.new'){
			updateChatLine(json.chat.chat_lines[0]);
		}else{
			updateChatLine(json.chat_line);
		}
		updateAfterMessage(json);
		$('#upload-chat-file-modal').modal('hide');
		clearUploadChatFileForm();
	});

	chatFileUpload.on("error", function( data) {
		console.log("noooonono!!");
		console.log(data);
	});

	$('#send-chat-file').click(function(){

		chatFileUpload.processQueue();
	});

	$('#upload-chat-file-modal').on('hidden.bs.modal',function(e){
		clearUploadChatFileForm();
	});

	/********************************************************************************
	 * Custom Rivets binders
	 ********************************************************************************/

	rivets.binders['profile-photo'] = function(el, photo_url){
		if(photo_url){
			el.src = photo_url;
		}else{
			el.src = "{{asset('/img/defaultIcon.png')}}";
		}
	}

	rivets.binders['display-if-array-empty'] = function(el, arr){
		if(arr && arr.length > 0){
			el.style.display = "none";
		}else{
			el.style.display = "block";
		}
	}

	rivets.binders['display-if-array-full'] = function(el, arr){
		if(arr && arr.length > 0){
			el.style.display = "block";
		}else{
			el.style.display = "none";
		}
	}

	rivets.formatters.date = function(value){
		var date;
		//parse the moment with Z to let moment know this is time is in UTC
		if(value.date){
			date = moment(value.date + "Z");	
		}else{
			date = moment(value + "Z");	
		}
		
		var today = moment();

		if(date.isSame(today,'day')){
			return date.format('[Today], h:mm a');
		}

		return date.format('D/M/YY, h:mm a');
	}

	rivets.formatters.displayMB = function(bytes){
		var mb = bytes/100000;
		return mb.toFixed(2) + 'MB';
	}

	var people_list = {people:[]};
	var teams_list= {teams:[]};
	var search_results= {results:[]};
	var chat_container_binding = rivets.bind($('#chat_container'), {active_chat: active_chat});
	var group_people_list_binding = rivets.bind($('#group-people-list'), {people_list:people_list});
	var group_teams_list_binding = rivets.bind($('#group-teams-list'), {teams_list:teams_list});
	var search_results_binding = rivets.bind($('#searchResult'), {search_results:search_results});

	/********************************************************************************
	 * Socket IO Chat Handlers
	 ********************************************************************************/

	socket.on('chat.message',function(message){
		var message = JSON.parse(message);
		console.log("MESSAGE\n",message);
		console.log("ACTIVE CHAT",active_chat);
		if( parseInt(message.chat_line.chat_id) === active_chat.id){
			//TODO check if user who sent message is current user and handle differently
			//perhaps consider message status as 'delivered'

			if(message.chat_line.user.id === current_user.id){
				//TODO figure out what to do here
				//maybe change message status to delivered?
			}
			else{
				active_chat.status="";
				active_chat.chat_lines.push(message.chat_line);
				drawChat();
			}
		}
		//update not selected chat
		if(message.chat_line.user.id !== current_user.id){
			var $chat = $("#chat-" + message.chat_line.chat_id);
			$chat.parent().prepend($chat);
			$chat.addClass("unread-chat");
			if (Notification.permission === "granted") {
			   notifyMe(message.chat_line.user.first_name,message.chat_line.text_content,message.chat_line.user.photo);
			}
		}
		
	});

	socket.on('chat.seen',function(message){
		var message = JSON.parse(message);
		active_chat.status="seen";
		console.log("seen");
		console.log(message);
	});

	socket.on('chat.new', function(message){
		var message = JSON.parse(message);
		var chat = message.chat;
		
		// console.log(chat);
		var html = "";
		var user_ids = _.pluck(chat.users,'id');

		if(chat.chat_type === 'group'){
			//prepend new chat item to top of ul
			if(active_chat.id === chat.id){
				html += "<li id='chat-"+chat.id+"' class='chat selected-chat' data-chat-id='"+chat.id+"' data-chat-type='"+chat.chat_type+"' data-chat-users='"+user_ids.join(',')+"' data-chat-name='"+chat.chat_name+"'>";
			}else{
				html += "<li id='chat-"+chat.id+"' class='chat unread-chat-chat' data-chat-id='"+chat.id+"' data-chat-type='"+chat.chat_type+"' data-chat-users='"+user_ids.join(',')+"' data-chat-name='"+chat.chat_name+"'>";
			}
			html += "<li id='chat-"+chat.id+"' class='chat unread-chat' data-chat-id='"+chat.id+"' data-chat-type='"+chat.chat_type+"' data-chat-users='"+user_ids.join(',')+"' data-chat-name='"+chat.chat_name+"'>";
			html += "<div class='chat-photo-wrapper'><img class='profile-pic-sm' src='"+getChatPhoto(chat)+"'  /></div>";
			html += "<span>"+chat.chat_name+"</span>";
			html += "</li>";

			$('#chats').prepend(html);
		}else{
			//if oto chat search for chat with user ids in data attr and remove
			var chats1 = $("[data-chat-users='"+user_ids[0]+","+user_ids[1]+"'][data-chat-type='empty']");
			var chats2 = $("[data-chat-users='"+user_ids[1]+","+user_ids[0]+"'][data-chat-type='empty']");

			if(chats1 && chats1.length > 0){
				//there is an existing element
				//lets update it
				chats1.attr("data-chat-type", chat.chat_type);
				chats1.data("chatType", chat.chat_type);
				chats1.attr("data-chat-id", chat.id);
				chats1.data("chatId", chat.id);
				chats1.attr("id", "chat-"+chat.id);
				if(!chats1.hasClass("selected-chat")){
					chats1.addClass("unread-chat");
				}
				chats1.parent().prepend(chats1[0]);
			}else if(chats2 && chats2.length > 0){
				chats2.attr("data-chat-type", chat.chat_type);
				chats2.data("chatType", chat.chat_type);
				chats2.attr("data-chat-id", chat.id);
				chats2.data("chatId", chat.id);
				if(!chats2.hasClass("selected-chat")){
					chats2.addClass("unread-chat");
				}
				chats2.parent().prepend(chats2[0]);
			}
		}
	});

	socket.on('chat.online_users',function(data){
		var keys = Object.keys(data);

		for(var i = 0; i < keys.length; i++){
			var user_id = keys[i].split(':')[1];
			var $user_chat_photo_wrapper = $('.chat-user-'+user_id+' .chat-photo-wrapper');
			$user_chat_photo_wrapper.addClass("chat-user-online");
		}
	});

	socket.on('chat.connect',function(user_id){
		console.log("Connected user ID: " + user_id);
		var $user_chat_photo_wrapper = $('.chat-user-'+user_id+' .chat-photo-wrapper');
		$user_chat_photo_wrapper.addClass("chat-user-online");
	});

	socket.on('chat.disconnect',function(user_id){
		
		console.log("Disconnected user ID: " + user_id);
		var $user_chat_photo_wrapper = $('.chat-user-'+user_id+' .chat-photo-wrapper');
		$user_chat_photo_wrapper.removeClass("chat-user-online");
	});

	socket.on('connect_error',function(err){
		$("#chat-message-text-content").addClass("connection-lost");
		$(".alert").show();
	});
	socket.on('connect',function(err){
		$("#chat-message-text-content").removeClass("connection-lost");
		$(".alert").hide();
	});
	/********************************************************************************
	 * Push Notification
	 ********************************************************************************/
	var isPushEnabled= false;
	var serviceWorkerRegistration;
	window.addEventListener('load', function() {  
		var pushButton = document.querySelector('.js-push-button');  
		  pushButton.addEventListener('click', function() {  
		    if (isPushEnabled) {  
		      unsubscribe();  
		    } else {  
		      subscribe();  
		    }  
		  });
	  if ('serviceWorker' in navigator) {  
	    navigator.serviceWorker.register("{{asset('service-worker.js')}}")  
	    .then(function(registration) {
	    	serviceWorkerRegistration= registration;
	    	//var messenger = serviceWorkerRegistration.installing || navigator.serviceWorker.controller;
  			//messenger.postMessage({token: $('[name="_token"]').val()});
	    	initialiseState();
	    });  
	  } else {  
	    console.warn('Service workers aren\'t supported in this browser.');  
	  }  
	});

	function initialiseState() {  
	  // Are Notifications supported in the service worker?  
	  if (!('showNotification' in ServiceWorkerRegistration.prototype)) {  
	    console.warn('Notifications aren\'t supported.');  
	    return;  
	  }

	  // Check the current Notification permission.  
	  // If its denied, it's a permanent block until the  
	  // user changes the permission  
	  if (Notification.permission === 'denied') {  
	    console.warn('The user has blocked notifications.');  
	    return;  
	  }

	  // Check if push messaging is supported  
	  if (!('PushManager' in window)) {  
	    console.warn('Push messaging isn\'t supported.');  
	    return;  
	  }
	  console.log("ddddddd");
	  // We need the service worker registration to check for a subscription  
	  //navigator.serviceWorker.ready.then(function(serviceWorkerRegistration) {  
	    // Do we already have a push message subscription?  
	    console.log("nnn");
	    serviceWorkerRegistration.pushManager.getSubscription()  
	      .then(function(subscription) {  
	        // Enable any UI which subscribes / unsubscribes from  
	        // push messages.  
	        
	        var pushButton = document.querySelector('.js-push-button');  
	        pushButton.disabled = false;
	        console.log("bbbbbbb");
	        if (!subscription) {  
	          // We aren't subscribed to push, so set UI  
	          // to allow the user to enable push  
	          console.log("aaaaaaa");
	          return;  
	        }

	        // Keep your server in sync with the latest subscriptionId
	        //sendSubscriptionToServer(subscription);

	        // Set your UI to show they have subscribed for  
	        // push messages  
	        pushButton.textContent = 'Disable Push Messages';  
	        isPushEnabled = true;  
	      })  
	      .catch(function(err) {  
	        console.warn('Error during getSubscription()', err);  
	      });  
	}

	function subscribe() {  
	  // Disable the button so it can't be changed while  
	  // we process the permission request  
	  var pushButton = document.querySelector('.js-push-button');  
	  pushButton.disabled = true;

	  //navigator.serviceWorker.ready.then(function(serviceWorkerRegistration) { 
	     
	    serviceWorkerRegistration.pushManager.subscribe({userVisibleOnly:true})  
	      .then(function(subscription) {  
	      	serviceWorkerRegistration.active.postMessage(JSON.stringify({uid: {{Auth::user()->id}}, token: $('[name="_token"]').val()}));
	        console.log('message send');
	        // The subscription was successful  
	        isPushEnabled = true;  
	        pushButton.textContent = 'Disable Push Messages';  
	        pushButton.disabled = false;

	        // TODO: Send the subscription.endpoint to your server 
	        formData=new FormData(); 
	        formData.append('endpoint',subscription.endpoint);
	        $.ajax({
				method:'POST',
				url:zen_config.base_url+'user/endpoint',
				headers: {
		    		'X-CSRF-TOKEN': $('[name="_token"]').val()
			    },
			    data: formData,
			    dataType: 'json',
			    cache: false,
			    contentType: false,
			    processData: false,
			})
			.done(function(data){
				console.log(data);
			})
			.fail(function(err){
				console.error(err);
			});
	        // and save it to send a push message at a later date
	       // return sendSubscriptionToServer(subscription);  
	      })  
	      .catch(function(e) {  
	        if (Notification.permission === 'denied') {  
	          // The user denied the notification permission which  
	          // means we failed to subscribe and the user will need  
	          // to manually change the notification permission to  
	          // subscribe to push messages  
	          console.warn('Permission for Notifications was denied');  
	          pushButton.disabled = true;  
	        } else {  
	          // A problem occurred with the subscription; common reasons  
	          // include network errors, and lacking gcm_sender_id and/or  
	          // gcm_user_visible_only in the manifest.  
	          console.error('Unable to subscribe to push.', e);  
	          pushButton.disabled = false;  
	          pushButton.textContent = 'Enable Push Notifications';  
	        }  
	      });  
	}
	function unsubscribe() {  
	  var pushButton = document.querySelector('.js-push-button');  
	  pushButton.disabled = true;

	    // To unsubscribe from push messaging, you need get the  
	    // subscription object, which you can call unsubscribe() on.  
	    serviceWorkerRegistration.pushManager.getSubscription().then(  
	      function(pushSubscription) {  
	        // Check we have a subscription to unsubscribe  
	        if (!pushSubscription) {  
	          // No subscription object, so set the state  
	          // to allow the user to subscribe to push  
	          isPushEnabled = false;  
	          pushButton.disabled = false;  
	          pushButton.textContent = 'Enable Push Notifications';  
	          return;  
	        }  

	        var subscriptionId = pushSubscription.subscriptionId;  
	        // TODO: Make a request to your server to remove  
	        // the subscriptionId from your data store so you
	        // don't attempt to send them push messages anymore

	        // We have a subscription, so call unsubscribe on it  
	        pushSubscription.unsubscribe().then(function(successful) {  
	          pushButton.disabled = false;  
	          pushButton.textContent = 'Enable Push Notifications';  
	          isPushEnabled = false;  
	        }).catch(function(e) {  
	          // We failed to unsubscribe, this can lead to  
	          // an unusual state, so may be best to remove
	          // the users data from your data store and
	          // inform the user that you have done so

	          console.log('Unsubscription error: ', e);  
	          pushButton.disabled = false;
	          pushButton.textContent = 'Enable Push Notifications';
	        });  
	      }).catch(function(e) {  
	        console.error('Error thrown while unsubscribing from push messaging.', e);  
	      });  
	   
	}
	/********************************************************************************
	 * Init
	 ********************************************************************************/
	drawChat();
</script>
@endsection