@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->

    <div class="row">

        <div class="col-md-12">

        <a href="{{ url('/admin_settings') }}" class="btn btn-xs btn-default"><i class="fa fa-caret-left"></i> Back To Settings</a>


        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif



        </div>

        <article class="col-sm-6 col-md-6 col-lg-6">

            <form  id="smart-form-register" class="smart-form client-form" role="form" 
            method="POST" action="{{ url('/files_settings_save') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="user_id" value="{{ $user['id'] }}">

					<fieldset>
		            <div class="row">
		            
			    		<section class="col col-5">
			              <label class="toggle">
			               <input type="checkbox" name="file_sharing" 
			               value="1" <?php echo ($company['file_sharing']=='1')?'checked="checked"':'';?> >
			               <i data-swchon-text="ON" data-swchoff-text="OFF"></i>{{trans("file_sharing_settings.File Sharing")}}</label>
			             </section>

		            </div>

		             <div class="row">

                        <section class="col col-3">
                             <label class="label">{{trans("file_sharing_settings.Storage Remaining")}}</label>

                         </section>
                          <section class="col col-5">
                            {{App\Classes\Helper::formatBytes($total_size)}} {{trans("file_sharing_settings.out of")}} {{App\Classes\Helper::formatBytes($company['file_capacity'])}}
                          </section>

                    </div>

		           </fieldset>
		           
                <footer>
                    <button type="submit" class="btn btn-primary">
                        {{trans("file_sharing_settings.Save")}}
                    </button>
                </footer>


            </form>

        </article>

    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->


@endsection