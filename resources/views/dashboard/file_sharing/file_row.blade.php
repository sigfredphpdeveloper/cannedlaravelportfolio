<tr class="sortable-row"
    data-content-type="<?=($content['content_type'] == 0)?'folder':'file';?>"
    data-content-id="{{$content['id']}}" data-content-name="{{ $content['content_name'] }}">
        @if($is_admin)
        <td>
            <input type="checkbox" name="content_ids[]" class="content_ids" value="{{$content['id']}}" />
        </td>
        @endif

        <td>

        <i class="icon fa <?php echo ($content['content_type']==0)?'folder-icon':'paper-icon';?>"></i>
            @if($content['content_type']==0)
                @if($content['read_access']==1)
                <a href="{{ url('/files_manager') }}/{{$content['id']}}">
                    {{ $content['content_name'] }}
                </a>
                @else
                 {{ $content['content_name'] }}
                @endif
            @else

                <a href="{{ url('/download_file') }}/{{$content['id']}}">
                  {{ $content['content_name'] }}
                </a>

            @endif

        </td>
        <td class="hidden-xs">
                {{App\Classes\Helper::formatBytes($content['size'])}}
        </td>
        <td>{{$content['first_name']}} {{$content['last_name']}}</td>
        <td class="hidden-xs">{{date('F d',strtotime($content['created_at']))}}</td>
        <td>

            @if($content['content_type']==0)

                @if($content['read_access'] == 1 AND $content['write_access'] == 1)
                <a href="javascript:void(0);"
                data-content-id="{{$content['id']}}"
                data-toggle="tooltip"
                data-parent-content-id="{{ $parent_content_id }}"
                class="manage_folder" title="{{trans("file_sharing_manager.Manage Permissions")}}"><i class="icon manage-icon"></i></a>

                <a href="javascript:void(0);" data-content-id="{{$content['id']}}" data-toggle="tooltip" class="delete_folder" title="{{trans("file_sharing_manager.Remove Folder")}}"><i class="icon trash-icon"></i></a>
                @endif

            @else
                <a href="{{ url('/download_file') }}/{{$content['id']}}" data-toggle="tooltip" title="{{trans("file_sharing_manager.Download File")}}" class="">
                <i class="icon download-icon"></i>
                </a>

                @if($content['write_access'] == 1)

                <a href="javascript:void(0);" data-toggle="tooltip" data-content-id="{{$content['id']}}" title="{{trans("file_sharing_manager.Delete File")}}" class="delete_file ">
                    <i class="icon trash-icon" ></i>
                </a>
                @endif

            @endif
        </td>
    </tr>