@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<div class="content">
    <!-- row -->
    <div class="row">

        <div class="col-sm-12 col-md-12">
            <h1>Add File to {{$current_folder['content_name']}}</h1>
        </div>

        <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Warning.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <article class="col-sm-6 col-md-6 col-lg-6">

            {!! Form::open(
            array(
            'url' => 'upload_file_process',
            'files' => true,
            'id'=>'smart-form-register',
            'class'=>'smart-form client-form'
            )) !!}

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="company_id" value="{{ $company['id'] }}">
            <input type="hidden" name="parent_content_id" value="{{ $parent_content_id }}">
            <input type="hidden" name="role_id" value="{{ $role['id'] }}">
            <input type="hidden" name="team_id" value="{{ $team['id'] }}">


                <div class="row">
                    <div class="col-md-4">File</div>
                    <div class="col-md-8">

                        <label class="input">
                            {!! Form::file('file', null) !!}
                        </label>

                    </div>
                </div>

                <footer>
                    <button type="submit" class="btn btn-success">
                        Save
                    </button>
                </footer>

            </form>

        </article>
    </div>
</div>
    <!-- end row -->

<!-- end widget grid -->


@endsection