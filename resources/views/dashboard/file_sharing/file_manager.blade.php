@extends('dashboard.layouts.master')

@section('content')

<link rel="stylesheet" type="text/css" media="screen" href="https://code.jquery.com/ui/jquery-ui-git.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.devbridge-autocomplete/1.2.24/jquery.autocomplete.js"></script>

<script type="text/javascript">
    $(document).ready(function(){

       $( "#files_table" ).sortable({

            items: 'tr.sortable-row',
            stop: function(e,ui){
            },
            update: function(event, ui){

            },
            receive: function (event, ui) {
                //alert('a');
            }

        }).disableSelection();

        $('.breads').droppable({
        tolerance: "pointer",
            accept: '#files_table tr',
            activeClass: "ui-state-default",
            hoverClass: "ui-state-hover",
            over: function(event, ui) {

                var move = ui.draggable;
                var to = $(this).data('content-name');

                var message = 'Move ' + move.data('content-name') + ' To '+to;

                preview(message);

            },
            drop: function( event, ui ) {

                var element = ui.draggable;
                element.off('click');

                var content_id = element.data('content-id');
                var parent_id = $(this).data('content-id');

                initialize_folder_move(parent_id, content_id);
            }
        });

        $('[data-content-type="folder"]').droppable({
            accept: "#files_table tr",
            activeClass: "ui-state-default",
            hoverClass: "ui-state-hover",
            over: function(event, ui) {

                var move = ui.draggable;
                var to = $(this).data('content-name');

                var message = 'Move ' + move.data('content-name') + ' To '+to;

                preview(message);

            },
            drop: function( event, ui ) {

                var element = ui.draggable;
                element.off('click');

                var content_id = element.data('content-id');
                var parent_id = $(this).data('content-id');

                initialize_folder_move(parent_id, content_id);
            }
        });


        $('[data-content-type="folder"]').draggable({
            cursor: 'pointer',
            connectWith: '#breadcrumb',
	        helper: 'clone',
	        opacity: 0.5,
	        zIndex: 10

        });



        function preview(message){


            $('#preview').show();

            $('#preview').html('<p>'+message+'</p>');

            setTimeout(function() {
                $('#preview').fadeOut('fast');
            }, 1500); // <-- time in milliseconds

        }


        function initialize_folder_move(parent_id,content_id){

            var baseUrl = "{{ url('/') }}";

            var post =  {
                parent_id: parent_id,
                content_id: content_id ,
                '_token':'{{ csrf_token() }}'
            };

            $.ajax({
                url: baseUrl+'/initialize_folder_move',
                data: post,
                type:'POST',
                success:function(){
                    $('#files_table').find('[data-content-id="'+content_id+'"]').remove();
                }
            });




        }

    });
</script>
<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->

    <div class="row">

        <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        </div>

        <div class="col-md-3">
        </div>
        
        <div class="col-md-9">


            <form class="form-inline pull-right" method="GET" style="display:inline-block;" action="{{$action_url}}" >
                <div class="form-group">
                    <div class="input-group">
                        <div id="change_here">
                            <input type="text" name="key"
                            value="<?php echo (!empty($_GET['key']))?$_GET['key']:'';?>" class="form-control"
                             placeholder="Search Name">
                        </div>
                        <span class="input-group-btn"> <button class="btn btn-default" id="search_btn" type="submit">{{trans("crm.Search")}}</button></span>
                    </div>
                </div>
            </form>


            @if($is_admin)

                <a href="{{ url('/file_settings') }}" class="btn btn-primary pull-right" >
                <i class="fa fa-cog"></i> {{trans("file_sharing_manager.Customize")}}</a>

            @endif


            <a href="javascript:void(0);" class="btn btn-primary pull-right" id="upload_file_trigger" style="margin-right:5px;" data-parent-content-id="{{ $parent_content_id }}" >{{trans("file_sharing_manager.Upload File")}}</a>
            <a href="javascript:void(0);" class="btn btn-primary pull-right" id="add_folder" style="margin-right:5px;" data-parent-content-id="{{ $parent_content_id }}" >{{trans("file_sharing_manager.Add Folder")}}</a>

            @if($is_admin)
                <a href="javascript:void(0);"
                class="btn btn-primary pull-right"
                id="delete_contents_admin"
                style="margin-right:5px;"
                >
                    {{trans("file_sharing_manager.Delete All")}}
                </a>


            @endif
        </div>

        <div class="col-md-12" style="margin-top:4px;">

            <ul class="breadcrumb" id="breadcrumb">
                <li>You are here </li>
                <?php echo $file_breadcrumb;?>
            </ul>

            <div class="container_table">



                     <table class="table " id="files_table">
                        <thead>
                            @if($is_admin)
                            <th width="25">
                                <input type="checkbox" id="check_all">
                            </th>
                            @endif

                            <th>
                                <a href="{{$sort_name}}">
                                    {{trans("file_sharing_manager.Name")}}
                                    <i class="fa fa-caret-{{$caret_name}}"></i>
                                </a>
                            </th>
                            <th class="hidden-xs">

                                <a href="{{$sort_size}}">
                                    {{trans("file_sharing_manager.Size")}}
                                    <i class="fa fa-caret-{{$caret_size}}"></i>
                                </a>

                            </th>
                            <th>
                                    {{trans("file_sharing_manager.Owner")}}
                            </th>
                            <th class="hidden-xs">
                                <a href="{{$sort_date}}">
                                    {{trans("file_sharing_manager.Upload Date")}}
                                    <i class="fa fa-caret-{{$caret_date}}"></i>
                                </a>
                            </th>
                            <th></th>
                        </thead>
                        <tbody id="content_tbody" >

                            @foreach($contents as $content)

                                 @if($content['no_access']==0)

                                    <tr class="sortable-row"
                                    data-content-type="<?=($content['content_type'] == 0)?'folder':'file';?>"
                                    data-content-id="{{$content['id']}}" data-content-name="{{ $content['content_name'] }}">
                                        @if($is_admin)
                                        <td>
                                            <input type="checkbox" name="content_ids[]" class="content_ids" value="{{$content['id']}}" />
                                        </td>
                                        @endif

                                        <td>

                                        <i class="icon fa <?php echo ($content['content_type']==0)?'folder-icon':'paper-icon';?>"></i>
                                            @if($content['content_type']==0)
                                                @if($content['read_access']==1)
                                                <a href="{{ url('/files_manager') }}/{{$content['id']}}">
                                                    {{ $content['content_name'] }}
                                                </a>
                                                @else
                                                 {{ $content['content_name'] }}
                                                @endif
                                            @else

                                                <a href="{{ url('/download_file') }}/{{$content['id']}}">
                                                  {{ $content['content_name'] }}
                                                </a>

                                            @endif

                                        </td>
                                        <td class="hidden-xs">
                                                {{App\Classes\Helper::formatBytes($content['size'])}}
                                        </td>
                                        <td>{{$content['first_name']}} {{$content['last_name']}}</td>
                                        <td class="hidden-xs">{{date('F d',strtotime($content['created_at']))}}</td>
                                        <td>

                                            @if($content['content_type']==0)

                                                @if($content['read_access'] == 1 AND $content['write_access'] == 1)
                                                <a href="javascript:void(0);"
                                                data-content-id="{{$content['id']}}"
                                                data-toggle="tooltip"
                                                data-parent-content-id="{{ $parent_content_id }}"
                                                class="manage_folder" title="{{trans("file_sharing_manager.Manage Permissions")}}"><i class="icon manage-icon"></i></a>

                                                <a href="javascript:void(0);" data-content-id="{{$content['id']}}" data-toggle="tooltip" class="delete_folder" title="{{trans("file_sharing_manager.Remove Folder")}}"><i class="icon trash-icon"></i></a>
                                                @endif

                                            @else
                                                <a href="{{ url('/download_file') }}/{{$content['id']}}" data-toggle="tooltip" title="{{trans("file_sharing_manager.Download File")}}" class="">
                                                <i class="icon download-icon"></i>
                                                </a>

                                                @if($content['write_access'] == 1)

                                                <a href="javascript:void(0);" data-toggle="tooltip" data-content-id="{{$content['id']}}" title="{{trans("file_sharing_manager.Delete File")}}" class="delete_file ">
                                                    <i class="icon trash-icon" ></i>
                                                </a>
                                                @endif

                                            @endif
                                        </td>
                                    </tr>

                                @endif

                            @endforeach

                        </tbody>

                     </table>

         <div id="preview" class="alert alert-info" style="display:none;">

                        </div>
                        
             </div>

        </div>
    </div>

    <!-- end row -->

</section>

<!-- end widget grid -->

<div class="modal fade" tabindex="-1" role="dialog" id="add_folder_model">

  <div class="modal-dialog">

    <div class="modal-content">

        <form id="smart-form-register" class="" role="form" method="POST" action="{{ url('/add_folder_save') }}">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{trans("file_sharing_manager.Add Folder")}}</h4>
          </div>

          <div class="modal-body">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="company_id" value="{{ $company['id'] }}">
            <input type="hidden" name="parent_content_id" value="{{ $parent_content_id }}">

                <fieldset>
                     <section>
                        <label class="input" style="width:100%;">
                            <input type="text" class="form-control" placeholder="{{trans("file_sharing_manager.Enter Folder Name *")}}" name="content_name" required>
                        </label>
                    </section>
                </fieldset>

                <p>{{trans("file_sharing_manager.Set Permission")}}</p>

                <table class="table table-striped">
                    <tr>
                        <th>{{trans("file_sharing_manager.Role")}}</th>
                        <th>{{trans("file_sharing_manager.Read")}}</th>
                        <th>{{trans("file_sharing_manager.Write")}}</th>
                        <th>{{trans("file_sharing_manager.No Access")}}</th>
                    </tr>


                    @foreach($roles as $role)

                        <tr>
                        <td>{{$role['role_name']}}</td>
                            <td>
                                <input type="hidden" name="roles_ids[]" value="{{$role['id']}}" />

                                @if($role['role_name']=='Administrator')
                                    <i class="fa fa-check-square-o"></i>
                                    <input type="checkbox" name="read_access[]" value="{{ $role['id'] }}" checked style="display:none;" />
                                @else
                                    <input type="checkbox" name="read_access[]" value="{{ $role['id'] }}" <?php echo ($role['role_name']=='Administrator')?'checked':'';?> />
                                @endif

                            </td>
                            <td>
                                @if($role['role_name']=='Administrator')
                                    <i class="fa fa-check-square-o"></i>
                                    <input type="checkbox" name="write_access[]"  value="{{ $role['id'] }}" checked style="display:none;" />
                                @else
                                    <input type="checkbox" name="write_access[]"  value="{{ $role['id'] }}" <?php echo ($role['role_name']=='Administrator')?'checked':'';?>/>
                                @endif

                            </td>
                            <td>
                                @if($role['role_name']=='Administrator')
                                    <i class="fa fa-square-o"></i>
                                    <input type="checkbox" name="no_access[]"  value="{{ $role['id'] }}" <?php echo ($role['role_name']=='Administrator')?'':'';?> style="display:none;"/>
                                @else
                                    <input type="checkbox" checked name="no_access[]"  value="{{ $role['id'] }}" <?php echo ($role['role_name']=='Administrator')?'':'';?>/>
                                @endif

                            </td>
                        </tr>

                    @endforeach

                </table>
          </div>
          <div class="modal-footer">

            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("file_sharing_manager.No")}}</button>
            <button type="submit" class="btn btn-success"  alt="">{{trans("file_sharing_manager.Save")}}</button>

          </div>
    </form>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="upload_file_modal">

  <div class="modal-dialog">

    <div class="modal-content">

        <form id="smart-form-register" method="POST" action='{{url("/upload_file_process")}}' enctype="multipart/form-data">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{trans("file_sharing_manager.Upload File")}}</h4>
          </div>

          <div class="modal-body">
            <div id="error_file_msg" class="alert alert-danger" style="display:none;">


            </div>
            <!--
            <input type="text" name="filename" id="file_name" value="" class="form-control"  placeholder="{{trans("file_sharing_manager.Please Enter File Name")}}"/>
            -->
            <br />

          <div class="dropzone" id="dropzoneFileUpload">
          <div class="dz-message" data-dz-message><span>click here to select a file or drag and drop your file here</span></div>
                      </div>
                      <!--

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="company_id" value="{{ $company['id'] }}">
            <input type="hidden" name="parent_content_id" value="{{ $parent_content_id }}">
            <input type="hidden" name="role_id" value="{{ $role['id'] }}">
            <input type="hidden" name="team_id" value="{{ $team['id'] }}">


                <div class="row">
                    <div class="col-md-4">File</div>
                    <div class="col-md-8">

                        <label class="input">
                            {!! Form::file('file', null) !!}
                        </label>

                    </div>
                </div>
                -->
          </div>
          <div class="modal-footer">

            <div class="alert alert-danger" id="file_name_alert" style="display: none;"><p>{{trans("file_sharing_manager.Please enter file name..")}}</p></div>

            <button type="button" class="btn btn-default" id="dismiss_add_file" data-dismiss="modal">{{trans("file_sharing_manager.Cancel")}}</button>
            <button type="button" class="btn btn-success" id="upload_now" alt="">{{trans("file_sharing_manager.Start Upload")}}</button>
          </div>
        </form>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->

</div><!-- /.modal -->


<div class="modal fade" tabindex="-1" role="dialog" id="delete_file_confirmation_modal">

  <div class="modal-dialog">

    <div class="modal-content">


          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{trans("file_sharing_manager.Confirmation")}}</h4>
          </div>

          <div class="modal-body">

                <p>{{trans("file_sharing_manager.Are you sure you want to delete this file ?")}} </p>

          </div>
          <div class="modal-footer">

            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("file_sharing_manager.No")}}</button>
            <button type="button" class="btn btn-danger" id="yes_delete_file"  alt="">{{trans("file_sharing_manager.Yes")}}</button>

          </div>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="delete_folder_confirmation_modal">

  <div class="modal-dialog">

    <div class="modal-content">


          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">{{trans("file_sharing_manager.Confirmation")}}</h4>
          </div>

          <div class="modal-body">

                <p>{{trans("file_sharing_manager.Are you sure you want to delete this folder ?")}}</p>

          </div>
          <div class="modal-footer">

            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("file_sharing_manager.No")}}</button>
            <button type="button" class="btn btn-danger" id="yes_delete_folder"  alt="">{{trans("file_sharing_manager.Yes")}}</button>

          </div>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="manage_folder_modal">

  <div class="modal-dialog">

    <div class="modal-content" id="manage_folder_modal_content">


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="messages_modal">

  <div class="modal-dialog">

    <div class="modal-content">
         <div class="modal-header">
         Warning
        </div>

        <div class="alert alert-info" id="info_msg">

        </div>


    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="delete_confirmation_modal">

  <div class="modal-dialog">

    <div class="modal-content">
         <div class="modal-header">
         Confirmation
        </div>

        <div class="alert alert-danger" id="info_msg">
            You are about to delete all checked items please confirm you want to proceed
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-success" id="delete_all_now_proceed" alt="">Proceed</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("file_sharing_manager.Cancel")}}</button>
        </div>

    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->

</div><!-- /.modal -->


      <script type="text/javascript">
        $(document).ready(function(){
                var baseUrl = "{{ url('/') }}";
                var token = "{{ Session::getToken() }}";
                var company_id = "{{ $company['id'] }}";
                var parent_content_id = "{{ $parent_content_id }}";
                var role_id = "{{ $role['id'] }}";
                var team_id = "{{ $team['id'] }}";

                Dropzone.autoDiscover = false;

                 var myDropzone = new Dropzone("div#dropzoneFileUpload", {
                    maxFiles:100,
                    maxFilesize:100,
                     url: baseUrl+"/upload_file_process",
                     autoProcessQueue:false,
                     params: {
                        _token: token,
                        company_id : company_id,
                        parent_content_id : parent_content_id,
                        role_id : role_id,
                        team_id : team_id
                      },
                      error: function(response){

                        $('#error_file_msg').html(response.xhr.responseText);
                        $('#error_file_msg').show();
                        this.removeAllFiles();

                        return false;

                      },
                      parallelUploads: 10
                     //, autoProcessQueue : false
                 });
                 Dropzone.options.myAwesomeDropzone = {
                    paramName: "file", // The name that will be used to transfer the file
                    maxFilesize: 10, // MB
                    maxFiles:10,
                    addRemoveLinks: true,

                    accept: function(file, done) {

                    }
                  };

                  myDropzone.on("success", function(file,xhr, data) {

                    $('#content_tbody').append(xhr);

                    $('#upload_file_modal').modal('hide');

                  });



                  myDropzone.on("sending", function(file, xhr, data) {

                    var file_name = $('#file_name').val();

                      data.append("parent_content_id", '{{ $parent_content_id }}');

                  });


                  $('#upload_now').click(function(){

                    var file_name = $('#file_name').val();

                    $(this).html('Uploading now...');

                    $('#file_name_alert').hide();

                    myDropzone.processQueue();

                    /*

                    if(file_name == ''){

                        $('#file_name_alert').show();
                    }else{

                        $(this).html('Uploading now...');

                        $('#file_name_alert').hide();

                        myDropzone.processQueue()
                    }
                    */

                  });

                  $('#dismiss_add_file').click(function(){
                        myDropzone.removeAllFiles();
                  });
              });
         </script>

<style type="text/css">
    .folder_td:hover{
        background:#d1d1d1;
    }
    .active_folder_id{
        background:#d1d1d1 !important;
    }
</style>

<script type="text/javascript">

    $(document).ready(function(){

        $('[data-toggle="tooltip"]').tooltip();

        $('#add_folder').click(function(){

            $('#add_folder_model').modal('show');

        });

        $('.folder_td').click(function(){

            $('.folder_td').removeClass('active_folder_td');
            $(this).addClass('active_folder_id');

        });


        $('#upload_file_trigger').click(function(){

            $('#upload_file_modal').modal('show');

        });


        $(document).on("click", ".delete_file", function(){
            var content_id = $(this).data('content-id');
            $('#delete_file_confirmation_modal').modal('show');
            $('#yes_delete_file').attr('alt',content_id);
        });


        $('#yes_delete_file').click(function(){

            var content_id = $(this).attr('alt');

            window.location.href='{{ url('delete_file') }}/'+content_id;

        });

        $('.manage_folder').click(function(){

            var content_id = $(this).data('content-id');
            var parent_content_id = $(this).data('parent-content-id');

            $.ajax({
                url : '{{url('/edit_folder')}}/'+parent_content_id+'/'+content_id,
                type : 'GET',
                success : function(html){
                    $('#manage_folder_modal_content').html(html);
                    $('#manage_folder_modal').modal('show');
                }
            });

        });

         $('.delete_folder').click(function(){
            var content_id = $(this).data('content-id');
            $('#delete_folder_confirmation_modal').modal('show');
            $('#yes_delete_folder').attr('alt',content_id);

        });

        $('#yes_delete_folder').click(function(){

            var content_id = $(this).attr('alt');

            window.location.href='{{ url('delete_folder') }}/'+content_id;

        });

        @if($is_admin)

            $('#check_all').click(function(){
                $(".content_ids").prop('checked', $(this).prop("checked"));
            });

            $('#delete_contents_admin').click(function(){

                var count_checked = $(".content_ids:checked").length; // count the checked rows

                if(count_checked==0){
                    $('#messages_modal').modal('show');
                    $('#info_msg').html('Please check atleast one checkbox');

                }else{

                $('#delete_all_now_proceed').text('Proceed');
                    $('#delete_confirmation_modal').modal('show');
                }

            });

            $('#delete_all_now_proceed').click(function(){


                $('#delete_all_now_proceed').text('Loading please wait...');

                var dt = '';
                $(".content_ids:checked").each(function(){
                    dt = dt + $(this).val() +'##';
                });

                var to_post = {
                    ids : dt,
                    _token : '{{ csrf_token() }}'
                };

                $.ajax({
                    url : '{{url('/delete_file_all')}}',
                    type : 'POST',
                    data:to_post,
                    success : function(html){
                        window.location.reload();
                    }
                });
            });
        @endif


    });

</script>

@endsection

