<form id="smart-form-register" class="" role="form" method="POST" action="{{ url('/edit_folder_save') }}/{{$parent_content_id}}/{{$content_id}}">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title">Edit Folder ({{$current_content['content_name']}})</h4>
  </div>

  <div class="modal-body">

    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="hidden" name="company_id" value="{{ $company['id'] }}">
    <input type="hidden" name="parent_content_id" value="{{ $parent_content_id }}">
    <input type="hidden" name="content_id" value="{{ $content_id }}">


        <fieldset>
             <section>
                <label class="input" style="width:100%;">
                    <input type="text" class="form-control" placeholder="Enter Folder Name *" name="content_name" value="{{$current_content['content_name']}}" required>
                </label>
            </section>
        </fieldset>

        <p>Set Permission</p>

        <table class="table table-striped">
            <tr>
                <th>Role</th>
                <th>Read</th>
                <th>Write</th>
                <th>No Access</th>
            </tr>

            @foreach($roles as $role)
                <tr>
                <td>{{$role['role_name']}}</td>
                    <td>
                        <input type="hidden" name="roles_ids[]" value="{{$role['id']}}" />

                        @if($role['role_name'] == 'Administrator')

                        <input type="checkbox" name="read_access[]" value="{{ $role['id'] }}"
                            checked="checked" style="display:none;"
                         />
                        <i class="fa fa-check-square-o"></i>
                        @else

                        <input type="checkbox" name="read_access[]" value="{{ $role['id'] }}"
                            <?php if($role['permission']['read_access'] == '1'){ echo 'checked="checked"'; } ?>
                        />

                        @endif


                    </td>
                    <td>

                        @if($role['role_name'] == 'Administrator')

                        <input type="checkbox" name="write_access[]"
                            checked="checked" style="display:none;"
                            value="{{ $role['id'] }}"
                            <?php if($role['permission']['write_access'] == '1'){ echo 'checked="checked"'; } ?>
                        />
                        <i class="fa fa-check-square-o"></i>

                        @else

                        <input type="checkbox" name="write_access[]"
                            value="{{ $role['id'] }}"
                            <?php if($role['permission']['write_access'] == '1'){ echo 'checked="checked"'; } ?>
                        />

                        @endif

                    </td>
                    <td>

                        @if($role['role_name'] == 'Administrator')


                        <input type="checkbox" name="no_access[]"
                            value="{{ $role['id'] }}"
                            style="display:none;"
                        />

                        <i class="fa fa-square-o"></i>

                        @else

                        <input type="checkbox" name="no_access[]"
                            value="{{ $role['id'] }}"
                            <?php if($role['permission']['no_access'] == '1'){ echo 'checked="checked"'; } ?>
                        />

                        @endif

 

                    </td>
                </tr>

            @endforeach

        </table>
  </div>
  <div class="modal-footer">

    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
    <button type="submit" class="btn btn-success"  alt="">Save</button>

  </div>
</form>