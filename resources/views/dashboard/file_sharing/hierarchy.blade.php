@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->

    <div class="row">

        <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        </div>

        <div class="col-md-6">
        	 
        </div>
        
        <div class="col-md-6">

            <a href="" class="btn btn-primary" >Upload</a>
            <a href="{{ url('/add_folder') }}" class="btn btn-primary" >Add Folder</a>
            <a href="" class="btn btn-primary" >Manage Folder Permission</a>

        </div>

        <div class="col-md-12">

             <h1>File Manager</h1>


             <div class="tree smart-form">
                <ul>
                    <li>
                        <span><i class="fa fa-lg fa-folder-open"></i> Parent</span>
                        <ul>
                            <li>
                                <span><i class="fa fa-lg fa-plus-circle"></i> Administrators</span>
                                <ul>
                                    <li style="display:none">
                                        <span> <label class="checkbox inline-block">
                                                <input type="checkbox" name="checkbox-inline">
                                                <i></i>Michael.Jackson</label> </span>
                                    </li>
                                    <li style="display:none">
                                        <span> <label class="checkbox inline-block">
                                                <input type="checkbox" checked="checked" name="checkbox-inline">
                                                <i></i>Sunny.Ahmed</label> </span>
                                    </li>
                                    <li style="display:none">
                                        <span> <label class="checkbox inline-block">
                                                <input type="checkbox" checked="checked" name="checkbox-inline">
                                                <i></i>Jackie.Chan</label> </span>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <span><i class="fa fa-lg fa-minus-circle"></i> Child</span>
                                <ul>
                                    <li>
                                        <span><i class="icon-leaf"></i> Grand Child</span>
                                    </li>
                                    <li>
                                        <span><i class="icon-leaf"></i> Grand Child</span>
                                    </li>
                                    <li>
                                        <span><i class="fa fa-lg fa-plus-circle"></i> Grand Child</span>
                                        <ul>
                                            <li style="display:none">
                                                <span><i class="fa fa-lg fa-plus-circle"></i> Great Grand Child</span>
                                                <ul>
                                                    <li style="display:none">
                                                        <span><i class="icon-leaf"></i> Great great Grand Child</span>
                                                    </li>
                                                    <li style="display:none">
                                                        <span><i class="icon-leaf"></i> Great great Grand Child</span>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li style="display:none">
                                                <span><i class="icon-leaf"></i> Great Grand Child</span>
                                            </li>
                                            <li style="display:none">
                                                <span><i class="icon-leaf"></i> Great Grand Child</span>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <span><i class="fa fa-lg fa-folder-open"></i> Parent2</span>
                        <ul>
                            <li>
                                <span><i class="icon-leaf"></i> Child</span>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>

        </div>
    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->


@endsection