<div class="modal fade view-note-modal" tabindex="-1" role="dialog" id="view-note">
    <div class="modal-dialog">
        <div class="modal-content">
            <div role="content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"> Update Note </h4>
                </div>
                <form method="POST" action="{{ url('update-note', $note->id) }}">
                    {!! csrf_field() !!}
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" class="form-control" value="{{ $note->title }}">
                        </div>
                        <div class="form-group">
                            <label for="content">Content</label>
                            <textarea name="content" class="form-control" rows="4">{{ $note->content }}</textarea>
                        </div>
                    </div>
                    <div class="modal-footer text-right">
                        <input type="submit" class="btn btn-success" name="" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>