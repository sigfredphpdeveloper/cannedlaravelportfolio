@extends('dashboard.layouts.master')

@section('content')

<link href="css/jquery.stickynote.css" rel="stylesheet" />
<link href="css/jquery.ui.resizable.css" rel="stylesheet" />

<style>
	#divStickyNotesContainer {
		border: 4px solid #FD9940;
		height: 1000px;
		margin: 0px;
	}
</style>

<div class="row">
	<div class="col-md-4">
		<select name="notes-view-toggle" class="form-control">
			<option value="sticky-note" selected="selected">Sticky Notes</option>
			<option value="list-view">List View</option>
		</select>
	</div>
</div>
<br />
<br />
<section id="widget-grid" class="">
	<div class="row">
		<div class="col-md-12">
			<div style="text-align: center; margin: 10px">
            	<h1>Double click on blank space to add a new note</h1>
        	</div>

			<div id="divStickyNotesContainer" data-note-list="{{ $notes_list }}">
			</div>
		</div>
	</div>
</section>

@endsection

@section('page-script')
<script type="text/javascript" src="{{asset('js/jquery.stickynote.js')}}"></script>
<script type="text/javascript" src="{{asset('js/StickyNotes.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('select[name=notes-view-toggle]').on('change', function(e){
			val = $(this).val();
			if( val == 'sticky-note') {
				window.location.href = base_url+'/notes_view';
			}
			else if( val == 'list-view') {
				window.location.href = base_url+'/notes';
			}
		});
	});
</script>
@endsection