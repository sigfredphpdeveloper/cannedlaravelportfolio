@extends('dashboard.layouts.master')

@section('content')
	@if(Session::has('success'))
		<div class="alert alert-block alert-success">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('success') }}</h4>
		</div>
	@elseif(Session::has('error'))
		<div class="alert alert-block alert-danger">
			<a class="close" data-dismiss="alert" href="#">×</a>
			<h4 class="alert-heading"><i class="fa fa-check-square-o"></i> {{ Session::get('error') }}</h4>
		</div>
	@endif


	<div class="row">
		<div class="col-md-4">
			<select name="notes-view-toggle" class="form-control">
				<option value="sticky-note">Sticky Notes</option>
				<option value="list-view" selected="selected">List View</option>
			</select>
		</div>
	</div>
	<br />
	<br />
	<!-- widget grid -->
	<section id="widget-grid" class="">
		<!-- row -->
		<div class="row">
			<div class="col-sm-12">
				<!-- Widget ID (each widget will need unique ID)-->
				<div class="jarviswidget" id="wid-id-3" data-widget-editbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-togglebutton="false">
					<header>
						<span class="widget-icon"> <i class="fa fa-table"></i> </span>
					<h2>{{trans("notes.All Notes")}}</h2>
					</header>
					<div>
						<div class="jarviswidget-editbox"></div>
						<div class="widget-body no-padding">
							<table id="all_notes_table" class="table table-striped table-bordered" width="100%">
							<thead>
								<tr style="background-color: transparent !important; background-image: none !important;">
									<th class="hasinput">
										<input id="" type="text" placeholder="{{trans("notes.Filter Name")}}" class="form-control" 
									</th>
									<th class="hasinput" style="">
										<input type="text" class="form-control" placeholder="{{trans("notes.Filter Content")}}" />
									</th>
									<th class="hasinput" style="">
										
									</th>
								</tr>
								<tr>
									<th>{{trans("notes.Title")}}</th>
									<th>{{trans("notes.Content")}}</th>
									<th>{{trans("notes.Action")}}</th>
								</tr>
							</thead>
							<tbody>
								@foreach($notes as $note)
									<tr>
										<td>{{ $note->title }}</td>
										<td>{{ mb_strimwidth($note->content, 0, 50, "...") }}</td>
										<td>
											<a href="javascript:void(0)" class="btn btn-primary view-note" data-id="{{ $note->id }}">{{trans("notes.View")}}</a>
											<a href="javascript:void(0)" class="btn btn-primary delete-note" data-id="{{ $note->id }}">{{trans("notes.Delete")}}</a>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<div class="modal fade" tabindex="-1" role="dialog" id="delete-note">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div role="content">
	                <div class="modal-header">
	                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	                    <h4 class="modal-title"> {{trans("notes.Delete Note")}} </h4>
	                </div>

					<form action="{{ url('delete-note') }}" method="post">
						{{ csrf_field() }}
						<input type="hidden" name="note_id" value="">
		                <div class="modal-body">
							<span>{{trans("notes.Are you sure you want to delete this note?")}}</span>
		                </div>
		                <div class="modal-footer">
		                	<button type="button" class="btn btn-default" data-dismiss="modal">{{trans("notes.No")}}</button>
		                	<input type="submit" class="btn btn-primary" name="submit" value="{{trans("notes.Yes")}}">
		                </div>
	                </form>
	            </div>
	        </div>
	    </div>
	</div>
@endsection

@section('page-script')

<!-- PAGE RELATED PLUGIN(S) -->
<script src="{{ url('js/plugin/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('js/plugin/datatables/dataTables.colVis.min.js') }}"></script>
<script src="{{ url('js/plugin/datatables/dataTables.tableTools.min.js') }}"></script>
<script src="{{ url('js/plugin/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ url('js/plugin/datatable-responsive/datatables.responsive.min.js') }}"></script>


<script type="text/javascript">
	$(document).ready(function(){
		pageSetUp();
		

		var responsiveHelper_datatable_fixed_column = undefined;
		var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
		var otable = $('#all_notes_table').DataTable({
			"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
			"oTableTools": {
		        	 "aButtons": [
		             "copy",
		             "csv",
		             "xls",
		                {
		                    "sExtends": "pdf",
		                    "sTitle": "Zenitranet_PDF",
		                    "sPdfMessage": "Zenintranet PDF Export",
		                    "sPdfSize": "letter"
		                },
		             	{
	                    	"sExtends": "print",
	                    	"sMessage": "Generated by Zenintranet <i>(press Esc to close)</i>"
	                	}
		             ],
		            "sSwfPath": "js/plugin/datatables/swf/copy_csv_xls_pdf.swf"
		        },
			"autoWidth" : true,
			"preDrawCallback" : function() {
				// Initialize the responsive datatables helper once.
				if (!responsiveHelper_datatable_fixed_column) {
					responsiveHelper_datatable_fixed_column = new ResponsiveDatatablesHelper($('#all_notes_table'), breakpointDefinition);
				}
			},
			"rowCallback" : function(nRow) {
					responsiveHelper_datatable_fixed_column.createExpandIcon(nRow);
			},
			"drawCallback" : function(oSettings) {
				responsiveHelper_datatable_fixed_column.respond();
			}
		});

		// Apply the filter
		$("#all_notes_table thead th input[type=text]").on( 'keyup change', function () {
		    otable
		        .column( $(this).parent().index()+':visible' )
		        .search( this.value )
		        .draw();
		} );

		$('.delete-note').on('click', function(e){
			id = $(this).data('id');
			$('input[name=note_id]').val(id);
			$('#delete-note').modal('show');
		});

		$('.view-note').on('click', function(e){
			id = $(this).data('id');
			
			zen_ajax({
                type:'GET',
                url: base_url+'/view-note/'+id
            }).done(function(data){
                var modalClass = ".view-note-modal";
                $(modalClass).remove();
                $("body").append(data);
                $(modalClass).modal('show');
            }).fail(function(xhr) {
                console.log('Something went wrong. Please try again later.');
            });
		});

		$('select[name=notes-view-toggle]').on('change', function(e){
			val = $(this).val();
			if( val == 'sticky-note') {
				window.location.href = base_url+'/notes_view';
			}
			else if( val == 'list-view') {
				window.location.href = base_url+'/notes';
			}
		});
	});
</script>

@endsection