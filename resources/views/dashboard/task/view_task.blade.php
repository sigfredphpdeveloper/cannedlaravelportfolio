<style>
.sm-icon{
    width: 15px;
    height: 15px;
}
.margin-10-0{
    margin:10px 0;
}
.float-left{ float:left;padding:0 5px; }

#check_list_container{ padding:0; margin-top:10px; }
#check_list_container li{ list-style-type:none; }
#check_list_container li label div.chk_container{ padding-left: 0 !important; }
#check_list_container li label div.chk_container input{ transform: translateY(50%); }
#check_list_container li label div a.edit_chk_list{ position: absolute;right: 0;top: 25% }

#check_list_container li label div.padding-none{ padding-left:0 !important; }
#check_list_container li label div textarea{ width: 337px;max-width: 337px; }
</style>

    {!! csrf_field() !!}
    <input type="hidden" name="id" value="<?=$task['task_id']?>" />

    <div class="row margin-10-0">
        <div class="col-xs-9 ">
            <label class="float-left"><h2 style="margin:0">#{{$task['task_id']}}</h2></label>
            <label class="float-left"><h2 style="margin:0">{{$task['task_title']}}</h2></label>
            <input type="text" name="task_title" id="view_task_title" value="{{$task['task_title']}}" class="form-control float-left" style="display:none;width:160px;" required />
            <a class="change_details" href="javascript:;"><i class="icon edit-icon sm-icon" style="margin-top:10px;"></i></a>
        </div>
    </div>

    <div class="row margin-10-0">
        <div class="col-xs-12">
            <h5>Details</h5>
            <label><?=nl2br($task['task_details'])?></label>
            <textarea name="task_details" id="view_task_details" class="form-control" style="display:none">{{$task['task_details']}}</textarea>
            <a class="change_details" href="javascript:;"><i class="icon edit-icon sm-icon"></i></a>
        </div>
    </div>
    <div class="row margin-10-0">
        <div class="col-xs-3"> Assign To </div>
        <div class="col-xs-4">
            <input id="view_assignto_id<?=$task['task_id']?>" class="form-control delete_fields assignTo_autoComplete<?=$task['task_id']?>" autocomplete="off" type="text" value="<?=!empty($task['first_name'])?$task['first_name'].' '.$task['last_name']:''?>" placeholder="Assigned to"  />
            <input class="form-control delete_fields" type="hidden" name="assignto_id" id="assignto_id<?=$task['task_id']?>" value="{{$task['assigned_user_id']}}" placeholder=""  />
        </div>
    </div>
    <div class="row margin-10-0">
        <div class="col-xs-3"> Category </div>
        <div class="col-xs-4">
            <input id="view_task_category<?=$task['task_id']?>" class="form-control delete_fields category_autoComplete<?=$task['task_id']?>" autocomplete="off" type="text" value="{{$task['category_name']}}" placeholder="Category"  />
            <input type="hidden" name="category" id="task_category<?=$task['task_id']?>" value="{{$task['category']}}" class="form-control" placeholder="Category"  />
        </div>
    </div>
    <div class="row margin-10-0">
        <div class="col-xs-3"> Start Date </div>
        <div class="col-xs-4 hide-this-start-date">{{$task['start_date']}}</div>
        <input type="text" name="start_date" value="{{date('Y-m-d',strtotime($task['start_date']))}}" id="view_start_date" class="dateapicker<?=$task['task_id']?> form-control delete_fields" placeholder="Select date" required style="display:none;width:120px;margin-left:10px;float:left;" />
        <input type="text" name="start_date_time" value="{{date('H:i:s',strtotime($task['start_date']))}}" id="view_start_date_time" class="timepicker2<?=$task['task_id']?> form-control delete_fields" placeholder="Select date" required style="display:none;width:120px;margin-left:10px;float:left;" />
        <a class="date-start-popup" href="javascript:;"><i class="icon edit-icon sm-icon"></i></a>
    </div>
    <div class="row margin-10-0">
        <div class="col-xs-3"> Due Date </div>
        <div class="col-xs-4 hide-this-date">{{$task['due_date']}}</div>
        <input type="text" name="due_date" value="{{date('Y-m-d',strtotime($task['due_date']))}}" id="view_due_date" class="dateapicker<?=$task['task_id']?> form-control delete_fields" placeholder="Select date" required style="display:none;width:120px;margin-left:10px;float:left;" />
        <input type="text" name="due_date_time" value="{{date('H:i:s',strtotime($task['due_date']))}}" id="view_due_date_time" class="timepicker3<?=$task['task_id']?> form-control delete_fields" placeholder="Select date" required style="display:none;width:120px;margin-left:10px;float:left;" />
        <a class="date-popup" href="javascript:;"><i class="icon edit-icon sm-icon"></i></a>
    </div>

    <div class="row margin-10-0">
        <div class="col-xs-3"> Files </div>
        <div class="col-xs-9">
            @if(empty($files))
                <p><b>None</b></p>
            @else
                <ul>
                @foreach($files as $file)
                    <li><a href="{{$file['file_path']}}/{{$file['file_name']}}">{{$file['file_name']}}</a></li>
                @endforeach
                </ul>
            @endif
            <input style="float:left;" type="file" name="file_upload[]" value="" placeholder="File to Upload" multiple>
        </div>
    </div>

    <div class="row margin-10-0">
        <div class="col-xs-3"> Check List </div>
        <div class="col-xs-9">
            <button type="button" id="add_check_list" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
            <ul id="check_list_container">
            @foreach($check_lists as $check_list)
                <li>
                    <label style="width: 100%">
                        <div class="col-xs-1 chk_container" style="line-height: 0;"><input type="checkbox" name="check_list[{{$check_list['id']}}]" value="1" <?=($check_list['check_status']==1)?'checked="checked"':''?> style="margin-top:0;vertical-align:top;" /></div>
                        <div class="col-xs-11 padding-none">
                            <label>{{$check_list['check_list_title']}}</label>
                            <textarea name="title[{{$check_list['id']}}]" style="display:none;">{{$check_list['check_list_title']}}</textarea>
                            <a class="edit_chk_list" href="javascript:;"><i class="icon edit-icon sm-icon"></i></a>
                        </div>
                    </label>
                </li>
            @endforeach
            </ul>
        </div>
    </div>


<script>
$(document).ready(function(){
    $( ".assignTo_autoComplete<?=$task['task_id']?>" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "{{ url('/user-list') }}",
                dataType: "json",

                data: {
                    q: request.term
                },
                success: function( data ) {
                   response(data);
                }
            });
        },
        select: function(e,ui){
            $('#assignto_id<?=$task['task_id']?>').val(ui.item.id);
        }
    });

    $( ".assignTo_autoComplete<?=$task['task_id']?>" ).autocomplete( "option", "appendTo", ".taskEditForm" );

    $( ".category_autoComplete<?=$task['task_id']?>" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "{{ url('/category-list') }}",
                dataType: "json",

                data: {
                    q: request.term
                },
                success: function( data ) {
                   response(data);
                }
            });
        },
        select: function(e,ui){
            $('#task_category<?=$task['task_id']?>').val(ui.item.id);
        }
    });

    $( ".category_autoComplete<?=$task['task_id']?>" ).autocomplete( "option", "appendTo", ".taskEditForm" );

    $( '.dateapicker<?=$task['task_id']?>' ).pickadate({
        formatSubmit: 'yyyy-mm-dd',
        // min: [2015, 7, 14],
        //container: '#container',
        // editable: true,
        closeOnSelect: true,
        closeOnClear: true
    })

    $('input[name="due_date_submit"]').val('{{$task['due_date']}}');

    $('.timepicker<?=$task['task_id']?>').pickatime()
    $('.timepicker2<?=$task['task_id']?>').pickatime()
    $('.timepicker3<?=$task['task_id']?>').pickatime()

    $('.taskEditForm').submit(function(event){

        if($(this).find('.dateapicker<?=$task['task_id']?>').val() == ''){
            $('.popup_error').show();
            event.preventDefault();
        }

    })

    // edit functionalities
    $('.change_details').on('click', function(){
        $(this).prev().prev().hide();
        $(this).prev().show();
        $(this).hide();
    })

    $('.date-popup').on('click', function(){
        $('.hide-this-date').hide();
        //$('.dateapicker<?=$task['task_id']?>').show();
        $('#view_due_date').show();
        $('#view_due_date_time').show();
        $(this).hide();
    })


    $('.date-start-popup').on('click', function(){
        $('.hide-this-start-date').hide();
        $('#view_start_date').show();
        $('#view_start_date_time').show();
        $(this).hide();
    })

    $('#view_assignto_id<?=$task['task_id']?>,#view_task_category<?=$task['task_id']?>').on('keyup',function(){
        var txt = $(this).val();
        if(txt == ''){
            $(this).next().val('');
        }

    })

    /**
     * checklist functionalities
     *
     */
    var chk_num = 1;
    $('#add_check_list').on('click', function(){
        var new_chk_list ='<li><label>' +
                '<div class="col-xs-1 chk_container"><input type="checkbox" name="check_list[a'+chk_num+']" value="1"/></div>' +
                '<div class="col-xs-11 padding-none"><textarea name="title[a'+chk_num+']" value="" class="form-control" ></textarea></div>' +
                '</label></li>';
        chk_num++;
        $('#check_list_container').append(new_chk_list);
    })

    $('.edit_chk_list').on('click', function(){
        $(this).prev().show();
        $(this).prev().prev().hide();
        $(this).hide();
    })



})
</script>