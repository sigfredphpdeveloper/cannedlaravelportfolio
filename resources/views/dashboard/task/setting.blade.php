@extends('dashboard.layouts.master')

@section('content')


<link rel="stylesheet" type="text/css" media="screen" href="https://code.jquery.com/ui/jquery-ui-git.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.devbridge-autocomplete/1.2.24/jquery.autocomplete.js"></script>

<style>
.task-tables{ padding-top:15px; }
.task-tables h2{ margin-top:0; }
article{ margin-bottom:50px !important;}
.left-10{margin-left:10px;}

#sort_records { list-style-type: none; margin: 0; padding: 0;  }
#sort_records li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; }
#sort_records li span { position: absolute; margin-left: -1.3em; }
.padding-left-none{padding-left:0 !important;}
</style>

    @include('errors.success')
    @include('errors.errors')


<article class="col-sm-6 col-md-6 col-lg-6">

    <form  id="smart-form-register" class="smart-form client-form" role="form"  method="POST" action="#">
        {!! csrf_field() !!}
        <input type="hidden" name="user_id" value="{{ $user['id'] }}">

        <fieldset>
            <!--
             <div class="row">
                <section class="col col-5 padding-left-none">
                    <label class="toggle">
                        <input type="checkbox" name="pm_module" id="pm_module" class="" value="1" <?=($company->pm_setting==1)?'checked="checked"':''?> >
                        <i data-swchon-text="ON" data-swchoff-text="OFF"></i>
                        PM Module
                    </label>
                </section>
            </div>
            -->
            <div class="row">
                <section class="col col-5 padding-left-none">
                    <label class="toggle">
                        <input type="checkbox" name="task_module" id="task_module" class="" value="1" <?=($company->task_setting==1)?'checked="checked"':''?> >
                        <i data-swchon-text="ON" data-swchoff-text="OFF"></i>
                        Task Module
                    </label>
                </section>

                @if($is_admin)
                    <a href="{{ url('/export_task_data/csv') }}" class="btn btn-primary pull-right">Export Tasks(csv)</a>
                    <a href="{{ url('/export_task_data/xls') }}" class="btn btn-primary pull-right" style="margin-right:5px;">Export Tasks(xls)</a>
                @endif

            </div>

        </fieldset>
    </form>

@if($company->pm_setting == 1)
    <div class=" task-tables">

        <!--
        <h2 class="pull-left">List Details</h2>

        <button class="btn btn-primary pull-right left-10" id="manage_position">Manage Position</button>
        <button class="btn btn-primary pull-right" id="add_tasks_list">Add a list</button>

        <table class="table table-striped">
            <thead>
                <tr>
                    <th>List Name</th>
                    <th>Position</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($task_lists as $task_list)
                    <tr>
                        <td>{{$task_list['list_title']}}</td>
                        <td>{{$task_list['position']}}</td>
                        <td>
                            <div class="input-group-btn pull-right" style="width: auto;">
                                <button type="button" class="btn btn-default" tabindex="-1">Action</button>
                                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1" aria-expanded="false">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li><a data-id="{{$task_list['id']}}" data-title="{{$task_list['list_title']}}" class="btn-edit">Edit</a></li>
                                    <li class="divider"></li>
                                    <li><a data-id="{{$task_list['id']}}" class="btn-delete">Delete</a></li>
                                </ul>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <hr />

-->

    @if($company->task_setting == 1)
        <div class=" task-tables">
            <h2 class="pull-left">Task Categories</h2>
            <button class="btn btn-primary pull-right" id="add_task_category">Add a Category</button>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Category Name</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($task_cats as $task_cat)
                        <tr>
                            <td>{{$task_cat['category_name']}}</td>
                            <td align="right">

                                <a data-id="{{$task_cat['id']}}" data-name="{{$task_cat['category_name']}}" class="btn-edit-cat btn btn-primary">Edit</a>

                                <a data-id="{{$task_cat['id']}}" class="btn-delete-category btn btn-primary">Delete</a>

                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endif
@endif

</article>
<article class="col-sm-6 col-md-6 col-lg-6" style="padding-top:30px;">



</article>




<div id="add-list-modal" class="modal fade add-list-modal" tabindex="-1" role="dialog" aria-labelledby="listModalLabel">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="listModalLabel">Add a List</h4>
            </div>
            <form id="list_form">
                <div class="modal-body">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" value="" id="task_list_id" />
                    <div class="margi-bottom">
                        <div class="form-group">
                            <label>List Name</label>
                            <input type="text" name="list_title" id="task_list_title" class="form-control" placeholder="List Name" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="save_list" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div id="add-category-modal" class="modal fade add-category-modal" tabindex="-1" role="dialog" aria-labelledby="categoryModalLabel">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="categoryModalLabel">Add a Category</h4>
            </div>
            <form id="category_form">
                <div class="modal-body">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" value="" id="task_category_id" />
                    <div class="margi-bottom">
                        <div class="form-group">
                            <label>Category Name</label>
                            <input type="text" name="category_name" id="task_category_name" class="form-control" placeholder="Category Name" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="save_category" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>




<div id="manage-position-modal" class="modal fade manage-position-modal" tabindex="-1" role="dialog" aria-labelledby="positionModalLabel">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="positionModalLabel">Manage PositionPosition</h4>
            </div>
            <form id="category_form">
                <div class="modal-body">
                    <ul id="sort_records">
                        @foreach($task_lists as $task_list)
                        <li class="ui-state-default" data-id="{{$task_list['id']}}">
                            <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
                            <div ><b>{{$task_list['list_title']}}</b></div>
                        </li>
                        @endforeach
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="sorting_rak_btn" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>








<div id="confirm-modal" class="modal fade confirm-modal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="confirmModalLabel">Delete</h4>
            </div>
            <form action="{{url('task-setting-delete')}}" method="POST">
                <div class="modal-body">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" value="" id="comfirm_id" />
                    <input type="hidden" name="table" value="" id="confirm_table" />
                    Click Ok for delete configmation..
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" id="save_category" class="btn btn-primary">Ok</button>
                </div>
            </form>
        </div>
    </div>
</div>



@endsection



@section('page-script')
<script>
$(document).ready(function(){

    $('#list_form').submit(function (e) {
        e.preventDefault();
        var form = $(this).serialize();
        $.ajax({
            type : 'POST',
            dataType : 'JSON',
            data : form,
            url : '{{url('/create_list')}}',
            success:function(response){
                if(response==true||response=='true'){
                    location.reload();
                }
            }
        });

        return false;
    });

    $('.btn-edit').on('click',function(){
        var id = $(this).data('id');
        var title = $(this).data('title');

        $('#task_list_id').val(id);
        $('#task_list_title').val(title);

        $('#add-list-modal').modal('show');
    })

    $('#add_tasks_list').on('click', function(){
        $('#task_list_id').val('');
        $('#task_list_title').val('');
        $('#add-list-modal').modal('show');
    })


    $('.btn-edit-cat').on('click',function(){
        var id = $(this).data('id');
        var name = $(this).data('name');

        $('#task_category_id').val(id);
        $('#task_category_name').val(name);

        $('#add-category-modal').modal('show');
    })

    $('#add_task_category').on('click', function(){
        $('#task_category_id').val('');
        $('#task_category_name').val('');
        $('#add-category-modal').modal('show');
    })

    $('#category_form').submit(function (e) {
        e.preventDefault();
        var form = $(this).serialize();
        $.ajax({
            type : 'POST',
            dataType : 'JSON',
            data : form,
            url : '{{url('/create_category')}}',
            success:function(response){
                if(response==true||response=='true'){
                    location.reload();
                }
            }
        });

        return false;
    });

    $('.btn-delete').on('click', function(){
        var id = $(this).data('id');

        $('#confirmModalLabel').html('Delete List Details')

        $('#comfirm_id').val(id);
        $('#confirm_table').val('list');

        $('#confirm-modal').modal('show');
    })
    $('.btn-delete-category').on('click', function(){
        var id = $(this).data('id');

        $('#confirmModalLabel').html('Delete Category')

        $('#comfirm_id').val(id);
        $('#confirm_table').val('category');

        $('#confirm-modal').modal('show');
    })

    $('#confirm-modal').on('hidden.bs.modal', function () {
        $('#comfirm_id').val('');
        $('#confirm_table').val('');
    })



    $('#manage_position').on('click', function(){
        $('#manage-position-modal').modal('show');
    })

    $( "#sort_records" ).sortable({
        connectWith: ".connectedSortable",
        stop: function(e,ui){
        },
        update: function(event, ui){
            if(ui.sender){

                var group = ui.item.parent().data('group');
                var id = ui.item.data('id');

                $.ajax({
                    url : '{{url('/stage_update')}}',
                    type : 'POST',
                    data : {'id':id, 'group':group, '_token':'{{ csrf_token() }}'},
                    dataType:'json',
                    success:function(response){
                        console.log(response);
                    }
                })
            }
        }
    }).disableSelection();

    $('#sorting_rak_btn').on('click', function(){
        var me = $(this);
        var stages = [];
        $('#sort_records li').each(function(){
            stages.push($(this).data('id'));
        })

        me.html('saving.....');
        me.prev().hide();

         $.ajax({
            url : '{{url('/save_list_position')}}',
            type : 'POST',
            data : {'id':stages, '_token':'{{ csrf_token() }}'},
            dataType:'json',
            success:function(response){
                if(response){
                    location.reload();
                }
            }
        })
    })

    $('#pm_module').on('click', function(){
        var value = $('#pm_module').is(":checked")

        $.ajax({
            url : '{{url('/save_pm_module')}}',
            type : 'POST',
            data : {'status':value, '_token':'{{ csrf_token() }}'},
            dataType:'json',
            success:function(response){
                if(response){
                    location.reload();
                }
            }
        })
    })

    $('#task_module').on('click', function(){
        var value = $('#task_module').is(":checked")


        $.ajax({
            url : '{{url('/save_task_module')}}',
            type : 'POST',
            data : {'status':value, '_token':'{{ csrf_token() }}'},
            dataType:'json',
            success:function(response){
                if(response){
                    location.reload();
                }
            }
        })
    })


})
</script>
@endsection