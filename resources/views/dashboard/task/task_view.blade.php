@extends('dashboard.layouts.master')


@section('content')


<style>
.margin-left-10{
    margin-left:10px;
}
.radio-inline{
    margin-left:10px;
}
.radio-inline input[type="radio"]{
    width:auto !important;
    height:auto;
}
.div-bordered{
    border:1px solid #bbb;
}


.sort_records {
    border: 1px solid transparent;
    min-height: 400px;
    list-style-type: none;
    margin: 0;
    padding: 0 0 20px 10px;
    float: left;
    width:270px;
    z-index: 99;
    margin-top:5px;
}
.sort_records li{
    padding: 5px;
    width: 95%;
    @if($user['user_level']==1)
        min-height:115px;
    @else
        min-height:80px;
    @endif
}

.left-section{
    width:30px;
}
.left-section button, .left-section a{
    margin-bottom:3px !important;
    width:31px;
    color:#fff;
}
.container{
    width:100%;
}

.scrollable_container{
    width: 100%;
    height:auto;
    border: 1px solid #bed5cd;
    overflow-x: scroll;
    overflow-y: hidden;
}
.color-orange{ color:#FD9940; }
.inside-scroll{
    width:{{(count($lists)*300)}}px;
    float:left;
    height:auto;
    padding-bottom: 50px;
}
.list_header{
    height:30px !important;
    padding:5px 0;
    background: #F7DCB4 !important;
    text-align: center;
    min-height: auto !important;
    border-bottom: 2px solid #E67D44;
    font-weight: bold;
    width: 270px;
    z-index: 100;
    margin:0 10px;
}
.view_task{padding-left:5px;}
.delete_selected_task{padding-left:10px;}

.visibility_options, .edit_visibility_options{
    vertical-align: -12px !important;
    margin-right: 5px !important;
}



#create_check_list_container{ padding:0; margin-top:10px; }
#create_check_list_container li{ list-style-type:none; }
#create_check_list_container li label div.chk_container{ padding-left: 0 !important; }
#create_check_list_container li label div.chk_container input{ transform: translateY(50%); }
#create_check_list_container li label div a.edit_chk_list{ position: absolute;right: 0;top: 25% }

#create_check_list_container li label div.padding-none{ padding-left:0 !important; }
#create_check_list_container li label div textarea{ width: 337px;max-width: 337px; }


/*** remove bottom padding to remove scroll***********/
#main{padding-bottom: 0 !important}
#content{padding-bottom: 0 !important}
.list_container{ width:270px;float:left;margin: 0 10px 0 10px;overflow-x:hidden;background:#e2e4e6; }

</style>
<div class="container">

    @include('errors.errors')
    @include('errors.success')

    @include('dashboard.task.task_search')

<?php

$html = array();
foreach($tasks as $k => $task){


if(!array_key_exists($task['list_id'], $html)){
    $html[$task['list_id']] = '';
}

$start_date = ($task['start_date']!="0000-00-00 00:00:00")?date('M d, Y (h:i A)',strtotime($task['start_date'])):'--';
$due_date = ($task['due_date']!="0000-00-00 00:00:00")?date('M d, Y (h:i A)',strtotime($task['due_date'])):'--';

$html[$task['list_id']] .= '<li data-id="'.$task['task_id'].'" class="ui-state-default">
                                <div class="pull-right left-section">

                                   <button type="button" class="btn btn-default btn-sm view_task" data-id="'.$task['task_id'].'" ><i class="icon view-icon"></i></button>
                                   '.(($user['user_level']==1)?'<button type="button" class="btn btn-danger delete_selected_task" data-id="'.$task['task_id'].'" ><i class="fa fa-times"></i></button>':'').'
                                </div>
                                '.$task['task_title'].'<br/>'.$task['first_name'].' '.$task['last_name'].'
                                <br>
                                Start: '.$start_date.'<br>
                                Due: '.$due_date.'
                            </li>';
}
?>


    <div class="scrollable_container" >
        @if(!empty($lists))
            <div class="inside-scroll">
                @foreach($lists as $list)
                    <div style="width:270px;float:left;margin: 0 10px 0 10px;">
                        <div class="list_header">
                            {{ucfirst($list['list_title'])}}
                        </div>
                        <div class="list_container" >
                            <ul id="sortable1" data-group="{{$list['id']}}" class="sort_records connectedSortable">
                                <?php
                                    if(!empty($html[$list['id']])) echo $html[$list['id']];
                                ?>
                            </ul>
                        </div>
                    </div>
                @endforeach
            </div>
        @else
            <div class="col-xs-12" style="padding:20px;">No Column assigned, Please edit the Board!</div>
        @endif
    </div>


</div>




<link rel="stylesheet" type="text/css" media="screen" href="https://code.jquery.com/ui/jquery-ui-git.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.devbridge-autocomplete/1.2.24/jquery.autocomplete.js"></script>

<link rel="stylesheet" href="{{url('css')}}/classic.css">
<link rel="stylesheet" href="{{url('css')}}/classic.date.css">
<link rel="stylesheet" href="{{url('css')}}/classic.time.css">
<script src="{{url('js')}}/picker.js"></script>
<script src="{{url('js')}}/picker.date.js"></script>
<script src="{{url('js')}}/picker.time.js"></script>

<!-- add task modal -->
<div class="modal fade" id="addtaskModal" tabindex="-1" role="dialog" aria-labelledby="addtaskModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addtaskModalLabel">Add new task</h4>
      </div>

      <form action="{{url('/create_task')}}/{{$board_id}}" method="POST" class="form-inline eventInsForm" enctype="multipart/form-data" >
          <div class="modal-body">
              <div class="alert alert-danger popup_error2" style="display:none;">
                  Task title is required.
              </div>
              <div class="alert alert-danger popup_error" style="display:none;">
                Start and Due date is required.
              </div>
              {!! csrf_field() !!}
            <div class="margi-bottom">
                <div class="form-group">
                    <label>Title</label>
                    <input type="text" name="title" value="" id="task_title" class="form-control" placeholder="Title" required>
                </div>
            </div>
            <div class="margi-bottom">
                <div class="form-group">
                    <label>Details</label>
                    <textarea name="details" class="form-control" placeholder="Details" style="width:250px;" ></textarea>
                </div>
            </div>
            <div class="margi-bottom">
                <div class="form-group">
                    <label>List Name</label>
                    <select name="list_id" class="form-control" style="width:250px;">
                        @foreach($lists as $list)
                            <option value="{{$list['id']}}">{{ucfirst($list['list_title'])}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="margi-bottom">
                <div class="form-group">
                    <label>Assigned to</label>
                    <input id="task_assigned_to" class="form-control delete_fields assignTo_autoComplete" autocomplete="off" type="text" value="" placeholder="Assigned to"  />
                    <input class="form-control delete_fields" type="hidden" name="assignto_id" id="assignto_id" value="" placeholder=""  />
                    <p style="margin-left:125px;color:#f00;display:none;">Assigned to field is not exist</p>
                </div>
            </div>
            <div class="margi-bottom">
                <div class="form-group">
                    <label>Category</label>
                    <input id="task_category_label" class="form-control delete_fields category_autoComplete" autocomplete="off" type="text" value="" placeholder="Category"  />
                    <input type="hidden" name="category" id="task_category" value="" class="form-control" placeholder="Category" >
                    <p style="margin-left:125px;color:#f00;display:none;">Category field is not exist!</p>
                </div>
            </div>
            <div class="margi-bottom">
                <div class="form-group">
                    <label>Start Date</label>
                    <input type="text" name="start_date" value="" class="dateapicker form-control delete_fields" placeholder="Select date"  />
                </div>
            </div>
            <div class="margi-bottom">
                <div class="form-group">
                    <label>Due Date</label>
                    <input type="text" name="due_date" value="" class="dateapicker form-control delete_fields" placeholder="Select date"  />
                </div>
            </div>
            <div class="margi-bottom">
                <div class="form-group">
                    <label style="float:left;">File to Upload</label>
                    <input style="float:left;" type="file" name="file_upload[]" id="file_upload" value="" placeholder="File to Upload" multiple>
                </div>
            </div>
              <div class="margi-bottom">
                  <div class="form-group">
                  <label style="float:left;"> Check List </label>
                  <div style="float:left">
                      <button type="button" id="create_add_check_list" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>
                      <ul id="create_check_list_container">
                      </ul>
                  </div>
                  </div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default hide-me-after" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary hide-me-after">Save changes</button>
          </div>
      </form>
    </div>
  </div>
</div>


<!-- view task modal -->
<div class="modal fade" id="viewTaskModal" tabindex="-1" role="dialog" aria-labelledby="viewTaskModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="padding-top:10px;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="viewTaskModalLabel">View/Edit Task</h4>
      </div>
      <form action="{{url('save-view-task')}}" method="POST" class="taskEditForm" enctype="multipart/form-data">
          <div class="modal-body">

          </div>
          <div class="modal-footer">
            <button type="button" id="archive_task" data-id="" class="btn btn-primary">Archive</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary" data-id_to_edit="">Save changes</button>
          </div>
      </form>
    </div>
  </div>
</div>
@endsection


@section('page-script')

<script>
$(document).ready(function(){
    $( ".assignTo_autoComplete" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "{{ url('/user-list') }}",
                dataType: "json",

                data: {
                    q: request.term
                },
                success: function( data ) {
                   response(data);
                }
            });
        },
        select: function(e,ui){
            $('#assignto_id').val(ui.item.id);

            if(ui.item.id != 0 || ui.item.id.length > 0){
                $('#task_assigned_to').parent().removeClass('has-error');
                $('#task_assigned_to').next().next().fadeOut();
            }
        }
    });

    $( ".assignTo_autoComplete" ).autocomplete( "option", "appendTo", ".eventInsForm" );


    $( ".category_autoComplete" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "{{ url('/category-list') }}",
                dataType: "json",

                data: {
                    q: request.term
                },
                success: function( data ) {
                   response(data);
                }
            });
        },
        select: function(e,ui){
            $('#task_category').val(ui.item.id);

            if(ui.item.id != 0 || ui.item.id.length > 0){
                $('#task_category_label').parent().removeClass('has-error');
                $('#task_category_label').next().next().fadeOut();
            }
        }
    });

    $( ".category_autoComplete" ).autocomplete( "option", "appendTo", ".eventInsForm" );

    $( '.dateapicker' ).pickadate({
        formatSubmit: 'yyyy-mm-dd',
        // min: [2015, 7, 14],
        //container: '#container',
        // editable: true,
        closeOnSelect: true,
        closeOnClear: true
    })

    $('.timepicker').pickatime()

    $('.eventInsForm').submit(function(event){
        if($('#task_title').val() == ''){
            $('.popup_error2').show();
            event.preventDefault();
        }

        $('.hide-me-after').hide().delay(2000).fadeIn();
    })


    $( ".sort_records" ).sortable({
        connectWith: "ul.connectedSortable",
        containment: ".container",
        zIndex: 999,
        stop: function(e,ui){
        },
        update: function(event, ui){
            if(ui.sender){

                var size = {
                    width: window.innerWidth || document.body.clientWidth,
                    height: window.innerHeight || document.body.clientHeight
                };
                var scrollable_container_height = (size.height)-229;
                $('.list_container').each(function(){
                    $(this).css({'height':scrollable_container_height-30});
                    if($(this).hasScrollBar() == true){
                        $(this).find('ul').css({'width':'260px'});
                    }else{
                        $(this).find('ul').css({'width':'270px'});
                    }
                })

                var group = ui.item.parent().data('group');
                var id = ui.item.data('id');

                $.ajax({
                        url : '{{url('/change_task_list')}}',
                    type : 'POST',
                    data : {'id':id, 'group':group, '_token':'{{ csrf_token() }}', 'board_id':'<?=$board_id;?>'},
                    dataType:'json',
                    success:function(response){
//                        console.log(response);
                    }
                })
            }
        },
        sort: function(event, ui){

        }
    }).disableSelection();


    $('.view_task').on('click', function(){
        var id = $(this).data('id');
        $('#archive_task').attr('data-id',id);
        $('#save_view_task').attr('data-id_to_edit',id); //set id to button to trigget which id is

        $.ajax({
            type : 'POST',
            dataType : 'JSON',
            data : {'id':id, '_token':'{{ csrf_token() }}'},
            url : '{{url('view-task')}}',
            success:function(response){

                $('#viewTaskModal .modal-body').html(response);
                $('#viewTaskModal').modal('show');

            }
        });
    })

    $('#save_view_task').on('click', function(e){
        var me = $(this);

        var myfile = $('.file_here'+me.data('id_to_edit'));
        var myfile = document.getElementById('file_here'+me.data('id_to_edit'));
        file = myfile.files;

        me.html('Saving..').removeClass('btn-primary').addClass('btn-default');
        var form = $('.taskEditForm').serialize();
        $.ajax({
            type : 'POST',
            dataType : 'JSON',
            data : form,
            url : '{{url('update_task')}}',
            success:function(response){

                if(response == true || response == 'true'){

                    me.html('Save changes').removeClass('btn-default').addClass('btn-primary');
                    $('#view_task_title').hide();
                    $('#view_task_title').prev().html('<h2 style="margin:0">'+$('#view_task_title').val()+'</h2>').show();

                    $('#view_task_details').hide();
                    $('#view_task_details').prev().html($('#view_task_details').val()).show();

                    $('.change_details').show();
                }

            }
    });

    })

    $('.delete_selected_task').on('click', function(){
        var id = $(this).data('id');
        $.ajax({
            type : 'POST',
            dataType : 'JSON',
            data : {'id':id, '_token':'{{ csrf_token() }}'},
            url : '{{url('delete-task')}}',
            success:function(response){
                if(response == true || response == 'true'){
                    location.reload();
                }
            }
        });

    })

})

$(window).on("load resize",function(e){
    var size = {
        width: window.innerWidth || document.body.clientWidth,
        height: window.innerHeight || document.body.clientHeight
    }

    var scrollable_container_height = (size.height)-229;
    $('.scrollable_container').css({'height':scrollable_container_height});

    /**
     * checklist functionalities
     *
     */
    var chk_num = 1;
    $('#create_add_check_list').on('click', function(){
        var new_chk_list ='<li><label>' +
                '<div class="col-xs-1 chk_container"><input type="checkbox" name="check_list[a'+chk_num+']" value="1"/></div>' +
                '<div class="col-xs-10 "><textarea name="chklst_title[a'+chk_num+']" value="" class="form-control" ></textarea></div>' +
                '</label></li>';
        chk_num++;
        $('#create_check_list_container').append(new_chk_list);
    })





    $.fn.hasScrollBar = function() {
        return this.get(0).scrollHeight > this.height();
    }

    $('.list_container').each(function(){
        $(this).css({'height':scrollable_container_height-30});
        if($(this).hasScrollBar() == true){
            $(this).find('ul').css({'width':'260px'});
        }else{
            $(this).find('ul').css({'width':'270px'});
        }
    })

})



</script>
@endsection
