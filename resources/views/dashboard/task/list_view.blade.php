@extends('dashboard.layouts.master')



@section('content')

<style>
table.tablesorter thead tr th, table.tablesorter tfoot tr th {
    background-color: #F7DCB4 !important;
    font-size: 11px !important;
}
.delete_selected_task{
    margin-left:10px;
}
</style>


<div id="content" class="">

    @include('errors.errors')
    @include('errors.success')

    @include('dashboard.task.task_search')

<link rel="stylesheet" href="{{ asset('css/blue/style.css') }}" type="text/css" id="" media="print, projection, screen" />
<script src="{{ asset('js/jquery.metadata.js') }}"></script>
<script src="{{ asset('js/jquery.tablesorter.min.js') }}"></script>

    <table class="table table-striped tablesorter">
        <thead>
            <tr>
                <th>ID</th>
                <th>Task Title</th>
                <th>Assign To</th>
                <th>List</th>
                <th>Category</th>
                <th>Last Update</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbod>
            @foreach($tasks as $task)
            <tr>
                <td>{{$task['task_id']}}</td>
                <td>{{$task['task_title']}}</td>
                <td>{{$task['first_name']}} {{$task['last_name']}}</td>
                <td>{{ucfirst($task['list_title'])}}</td>
                <td>{{$task['category_name']}}</td>
                <td>{{$task['task_last_update_date']}}</td>
                <td>
                    <a class="view_task btn btn-primary btn-sm" data-id="{{$task['task_id']}}"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
                    @if($is_admin)
                        <a class="btn btn-danger btn-sm delete_selected_task" data-id="{{$task['task_id']}}"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                    @endif
                    <!--
                    <div class="input-group-btn" style="width: auto;"><button type="button" class="btn btn-default" tabindex="-1">Actions</button>
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1" aria-expanded="false">
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu">
                            <li><a class="view_task" data-id="{{$task['task_id']}}">View</a></li>
                            <li class="divider"></li>
                            @if($user['user_level']==1)
                                <li><a class="btn-sm delete_selected_task" data-id="{{$task['task_id']}}">Delete</a></li>
                            @endif
                        </ul>
                    </div>
                    -->
                </td>
            </tr>
            @endforeach
        </tbod>
    </table>

</div>





<link rel="stylesheet" type="text/css" media="screen" href="https://code.jquery.com/ui/jquery-ui-git.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.devbridge-autocomplete/1.2.24/jquery.autocomplete.js"></script>

<link rel="stylesheet" href="{{url('css')}}/classic.css">
<link rel="stylesheet" href="{{url('css')}}/classic.date.css">
<link rel="stylesheet" href="{{url('css')}}/classic.time.css">
<script src="{{url('js')}}/picker.js"></script>
<script src="{{url('js')}}/picker.date.js"></script>
<script src="{{url('js')}}/picker.time.js"></script>





<!-- add task modal -->
<div class="modal fade" id="addtaskModal" tabindex="-1" role="dialog" aria-labelledby="addtaskModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="addtaskModalLabel">Add new task</h4>
      </div>

      <form action="{{url('/create_task')}}/{{$board_id}}" method="POST" class="form-inline eventInsForm" enctype="multipart/form-data" >
          <div class="modal-body">
            <div class="alert alert-danger popup_error" style="display:none;">
                Start and Due date is required.
            </div>
              {!! csrf_field() !!}
        <div class="margi-bottom">
            <div class="form-group">
                <label>Title</label>
                <input type="text" name="title" value="" class="form-control" placeholder="Title" required>
            </div>
        </div>
        <div class="margi-bottom">
            <div class="form-group">
                <label>Details</label>
                <textarea name="details" class="form-control" placeholder="Details" style="width:250px;" required></textarea>
            </div>
        </div>
        <div class="margi-bottom">
            <div class="form-group">
                <label>Assigned to</label>
                <input class="form-control delete_fields assignTo_autoComplete" autocomplete="off" type="text" value="" placeholder="Assigned to" required />
                <input class="form-control delete_fields" type="hidden" name="assignto_id" id="assignto_id" value="" placeholder="" required />
            </div>
        </div>
        <div class="margi-bottom">
            <div class="form-group">
                <label>Category</label>
                <input class="form-control delete_fields category_autoComplete" autocomplete="off" type="text" value="" placeholder="Category" required />
                <input type="hidden" name="category" id="task_category" value="" class="form-control" placeholder="Category" required>
            </div>
        </div>
        <div class="margi-bottom">
            <div class="form-group">
                <label>Start Date</label>
                <input type="text" name="start_date" value="" class="dateapicker form-control delete_fields" placeholder="Select date" required />
            </div>
        </div>
        <div class="margi-bottom">
            <div class="form-group">
                <label>Due Date</label>
                <input type="text" name="due_date" value="" class="dateapicker form-control delete_fields" placeholder="Select date" required />
            </div>
        </div>
        <div class="margi-bottom">
            <div class="form-group">
                <label style="float:left;">File to Upload</label>
                <input style="float:left;" type="file" name="file_upload[]" id="file_upload" value="" placeholder="File to Upload" multiple>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default hide-me-after" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary hide-me-after">Save changes</button>
          </div>
      </form>
    </div>
  </div>
</div>


<!-- view task modal -->
<div class="modal fade" id="viewTaskModal" tabindex="-1" role="dialog" aria-labelledby="viewTaskModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="viewTaskModalLabel">View task</h4>
      </div>
        <form action="{{url('save-view-task')}}" method="POST" class="taskEditForm" enctype="multipart/form-data">
            <input type="hidden" name="return_page" value="list-view" />
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" id="archive_task" data-id="" class="btn btn-primary">Archive</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" data-id_to_edit="">Save changes</button>
            </div>
        </form>
    </div>
  </div>
</div>




@endsection


@section('page-script')

<script>
$(document).ready(function(){
    $( ".assignTo_autoComplete" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "{{ url('/user-list') }}",
                dataType: "json",

                data: {
                    q: request.term
                },
                success: function( data ) {
//                console.log(data);
                   response(data);
                }
            });
        },
        select: function(e,ui){
//            console.log(ui);
            $('#assignto_id').val(ui.item.id);
        }
    });

    $( ".assignTo_autoComplete" ).autocomplete( "option", "appendTo", ".eventInsForm" );


    $( ".category_autoComplete" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "{{ url('/category-list') }}",
                dataType: "json",

                data: {
                    q: request.term
                },
                success: function( data ) {
                   response(data);
                }
            });
        },
        select: function(e,ui){
            $('#task_category').val(ui.item.id);
        }
    });

    $( ".category_autoComplete" ).autocomplete( "option", "appendTo", ".eventInsForm" );

    $( '.dateapicker' ).pickadate({
        formatSubmit: 'yyyy-mm-dd',
        // min: [2015, 7, 14],
        //container: '#container',
        // editable: true,
        closeOnSelect: true,
        closeOnClear: true
    })

    $('.timepicker').pickatime()

    $('.eventInsForm').submit(function(event){

        if($(this).find('.dateapicker').val() == ''){
            $('.popup_error').show();
            event.preventDefault();
        }

    })



    $( ".sort_records" ).sortable({
        connectWith: ".connectedSortable",
        containment: ".container",
        stop: function(e,ui){
        },
        update: function(event, ui){
            if(ui.sender){

                var group = ui.item.parent().data('group');
                var id = ui.item.data('id');
                console.log(group);
                console.log(id);

                $.ajax({
                        url : '{{url('/change_task_list')}}',
                    type : 'POST',
                    data : {'id':id, 'group':group, '_token':'{{ csrf_token() }}', 'board_id':'<?=$board_id;?>'},
                    dataType:'json',
                    success:function(response){
                        console.log(response);
                    }
                })
            }
        }
    }).disableSelection();



    $('#save_task').on('click', function(){
        var form = $('form#create_task_form').serialize();

        $.ajax({
            type : 'POST',
            dataType : 'JSON',
            data : form,
            url : '{{url('/create_task').'/'.$board_id}}',
            success:function(response){
                if(response==true||response=='true'){
                    //location.reload();
                }

                location.reload();

            }
        });
    })

    $('.view_task').on('click', function(){
        var id = $(this).data('id');
        $('#archive_task').attr('data-id',id);

        $.ajax({
            type : 'POST',
            dataType : 'JSON',
            data : {'id':id, '_token':'{{ csrf_token() }}'},
            url : '{{url('view-task')}}',
            success:function(response){

                $('#viewTaskModal .modal-body').html(response);
                $('#viewTaskModal').modal('show');

            }
        });
    })

    $('#save_view_task').on('click', function(e){

        var form = $('.taskEditForm').serialize();
        $.ajax({
            type : 'POST',
            dataType : 'JSON',
            data : form,
            url : '{{url('save-view-task')}}',
            success:function(response){

                if(response == true || response == 'true'){
                    $('#view_task_title').hide();
                    $('#view_task_title').prev().html('<h2 style="margin:0">'+$('#view_task_title').val()+'</h2>').show();

                    $('#view_task_details').hide();
                    $('#view_task_details').prev().html($('#view_task_details').val()).show();

                    $('#view_due_date').hide();
                    $('#view_due_date').prev().html($('#view_due_date').val()).show();

                    $('.change_details').show();
                }

            }
        });

    })

    $('.delete_selected_task').on('click', function(){
        var id = $(this).data('id');
        $.ajax({
            type : 'POST',
            dataType : 'JSON',
            data : {'id':id, '_token':'{{ csrf_token() }}'},
            url : '{{url('delete-task')}}',
            success:function(response){
                if(response == true || response == 'true'){
                    location.reload();
                }
            }
        });

    })

    $("table").tablesorter({debug:true});



})
</script>
@endsection