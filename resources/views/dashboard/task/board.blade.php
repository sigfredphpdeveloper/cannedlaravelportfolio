@extends('dashboard.layouts.master')



@section('content')
<link rel="stylesheet" type="text/css" media="screen" href="https://code.jquery.com/ui/jquery-ui-git.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.devbridge-autocomplete/1.2.24/jquery.autocomplete.js"></script>


<style>
.margin-left-10{
    margin-left:10px;
}
.radio-inline{
    margin-left:10px;
}
.radio-inline input[type="radio"]{
    width:auto !important;
    height:auto;
}
.div-bordered{
    border:1px solid #bbb;
}


.sort_records {
    border: 1px solid #eee;
    min-height: 400px;
    list-style-type: none;
    margin: 0;
    padding: 0 0 20px 10px;
    float: left;
    background:#e2e4e6;
    min-width:270px;
}
.sort_records li{
    padding: 5px;
    width: 95%;
    @if($user['user_level']==1)
        min-height:115px;
    @else
        min-height:80px;
    @endif
}

.empty {
    border: 1px solid #eee;
    min-height: 100px;
    list-style-type: none;
    margin: 0;
    padding: 0 0 20px 10px;
    float: left;
    background:#e2e4e6;
    width:270px;
}
.empty li{
    padding: 5px;
    width: 95%;
    min-height:80px;
}
.link{
    color:#57889c !important;
}
.left-section{
    width:30px;
}
.left-section button, .left-section a{
    margin-bottom:3px !important;
    width:31px;
    color:#fff;
}
.container{
    width:100%;
}

.scrollable_container{
    width: 100%;
    height:auto;
    border: 1px solid #bed5cd;
    overflow-x: scroll;
    overflow-y: hidden;
    /*padding-top: 20px;*/
}
.color-orange{ color:#FD9940; }
.link{color:#868686 !important;}

.btn-success{
    color:#fff !important;
    background-color:#739e73 !important;
    border-color:#659265 !important;
}

h2{margin:10px 0;}
table.table thead tr th{ padding-left:30px !important;}
</style>
<div class="container">
    {{--<h2>Deals</h2>--}}

    @include('dashboard.crm.crm_search', ['display' => $display])

    @include('errors.success')
    @include('errors.errors')


<?php
    // define array keys
    $array['empty']['html'] = '';
    $array['empty']['header'] = '';
    $stages_id[] = 'empty';
?>
    @foreach($deals as $deal)
    <?php
        $status = '';

        if($deal["won_date"] != "0000-00-00 00:00:00" || $deal["lost_date"] != "0000-00-00 00:00:00"){
            $deal['stage'] = 'empty';

            if($deal['won_date']!='0000-00-00 00:00:00'){
                $status = '<label style="color:limegreen">WON</label>';
            }elseif($deal['lost_date']!='0000-00-00 00:00:00'){
                $status = '<label style="color:#f00000">LOST</label>';
            }
        }

        if(count($stages)>=1 && in_array($deal['stage'], $stages_id)){
            $array[$deal['stage']]['html'] .= '<li data-id="'.$deal['deal_id'].'" class="ui-state-default">
                                     <div class="pull-right left-section">
                                        <a href="'.url('deal_view').'/'.$deal['deal_id'].'" class="btn btn-default btn-sm"><i class="fa fa-info color-orange"></i></a>
                                        <button type="button" class="btn btn-default btn-sm edit_deal" data-id="'.$deal['deal_id'].'" ><i class="fa fa-pencil color-orange"></i></button>
                                        '.(($user['user_level']==1)?'<button type="button" class="btn btn-danger delete_selected_deal" data-id="'.$deal['deal_id'].'" ><i class="fa fa-times"></i></button>':'').'
                                     </div>
                                     Deal: <a href="'.url('deal_view').'/'.$deal['deal_id'].'" class="link" >'.$deal['deal_title'].'</a>
                                     <br/>
                                     Org: '.$deal['organization_name'].' <br/>'.$status.'</li>';

        }
    ?>
    @endforeach


    @if(count($stages)>=1)
    <div class="scrollable_container" >

        <table class="table table-stripe" style="min-width:400px;min-height:400px;">
            <thead>
                <tr>
                    @foreach($array as $key => $content)
                        @if($key != 'empty')
                            <th width="270">{{$content['header']}}</th>
                        @endif
                    @endforeach
                    @if(count($array)<5)
                        @for($x=count($array);$x<=5;$x++)
                            <th width="270">&nbsp;</th>
                        @endfor
                    @endif
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    @foreach($array as $key => $content)
                        @if($key != 'empty')
                            <td>
                                <ul id="sortable1" data-group="{{$key}}" class="sort_records connectedSortable col-xs-12">
                                    {!! $content['html'] !!}
                                </ul>
                            </td>
                        @endif
                    @endforeach
                    @if(count($array)<5){
                        @for($x=count($array);$x<=5;$x++)
                            <td width="270">&nbsp;</td>
                        @endfor
                    @endif
                </tr>
            </tbody>
        </table>

    </div>
    @else
    <div>
        No stages had been set yet.
    </div>
    @endif
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="myModalLabel">{{trans("crm_index.Add a Deal")}}</h4>
            </div>
            <form action="{{ url('/create_deal') }}" method="POST" class="eventInsForm">
                <div class="modal-body">

                    {!! csrf_field() !!}
                    <input type="hidden" name="user_id" value="{{ $user['id'] }}">


                    <div class="form-group">
                        <label>{{trans("crm_index.Deal Title")}}</label>
                        <input class="form-control delete_fields" type="text" name="deal_title" value="" placeholder="" />
                    </div>
                    <div class="form-group">
                        <label>{{trans("crm_index.Contact Name")}}</label>
                        <input class="form-control delete_fields contact_auto_complete" autocomplete="off" type="text" value="" placeholder="" />
                        <input class="form-control delete_fields" type="hidden" name="contact_id" id="contact_id" value="" placeholder="" />
                    </div>
                    <div class="form-group">
                        <label>{{trans("crm_index.Stage")}}</label>
                        <select name="stage" class="form-control">
                            @foreach($array as $key => $content)
                                @if($key !== 'empty')
                                 <option value="{{$key}}">{{$content['header']}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>{{trans("crm_index.Visibility")}}</label>

                        <label class="radio-inline">
                            <input class="form-control " type="radio" name="visibility" value="me" checked="checked" /> {{trans("crm_index.Me")}}
                        </label>
                        <label class="radio-inline">
                            <input class="form-control " type="radio" name="visibility" value="team" />{{trans("crm_index.Team")}}
                        </label>
                        <label class="radio-inline">
                            <input class="form-control " type="radio" name="visibility" value="everyone" />{{trans("crm_index.Every One")}}
                        </label>
                    </div>
                    <div class="form-group">
                        <label>{{trans("crm_index.Expected Value")}}</label>
                        <input class="form-control delete_fields" type="text" name="deal_value" value="" placeholder="" />
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{trans("crm_index.Close")}}</button>
                    <button type="submit" class="btn btn-primary" id="create_btn">{{trans("crm_index.Save changes")}}</button>
                </div>
            </form>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="editDeal" tabindex="-1" role="dialog" aria-labelledby="editDealLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="editDealLabel">{{trans("crm_index.Edit a Deal")}}</h4>
            </div>
            <div id="deal_model_content">

            </div>
        </div>
    </div>
</div>


@endsection


@section('page-script')
<script>
$(document).ready(function(){

    $( ".contact_auto_complete" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "{{ url('/contact_list') }}",
                dataType: "json",

                data: {
                    q: request.term
                },
                success: function( data ) {
                console.log(data);
                   response(data);
                }
            });
        },
        select: function(e,ui){
            console.log(ui);
            $('#contact_id').val(ui.item.id);
        }
    });

    $( ".contact_auto_complete" ).autocomplete( "option", "appendTo", ".eventInsForm" );

    $( ".custom_auto_complete" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "{{ url('/organization_list') }}",
                dataType: "json",

                data: {
                    q: request.term
                },
                success: function( data ) {
                console.log(data);
                   response(data);
                }
            });
        },
        select: function(e,ui){
            console.log(ui);
            $('#organization_id').val(ui.item.id);
        }
    });

    $( ".custom_auto_complete" ).autocomplete( "option", "appendTo", ".eventInsForm" );


    $( ".sort_records" ).sortable({
        connectWith: ".connectedSortable",
        containment: ".container",
        stop: function(e,ui){
        },
        update: function(event, ui){
            if(ui.sender){

                var group = ui.item.parent().data('group');
                var id = ui.item.data('id');

                $.ajax({
                    url : '{{url('/stage_update')}}',
                    type : 'POST',
                    data : {'id':id, 'group':group, '_token':'{{ csrf_token() }}'},
                    dataType:'json',
                    success:function(response){
                        console.log(response);
                    }
                })
            }
        }
    }).disableSelection();


    $('.edit_deal').click(function(){
        var id = $(this).data('id');

        $.ajax({
                       url : '{{url('/deal_edit_form')}}/'+id,
                       type : 'GET',
                       data : 'id='+id,
                       success:function(html){
                           $('#deal_model_content').html(html);
                           $('#editDeal').modal('show');
                       }

                   });
    });


    $('ol.breadcrumb').append('<li>Deals</li>');

    $('form.eventInsForm').on('submit', function(e){
        var org_id = $('#organization_id').val();
        var cont_id = $('#contact_id').val();
        var me = $(this);

        $('#create_btn').attr('disabled', 'disabled');
        setTimeout(function(){
            $('#create_btn').removeAttr('disabled');
        }, 2500)

        if(cont_id == ''){
            $('#contact_id').parent().find('label').html('Contact <label style="color:#f00;"> This contact in not part of your existing contacts. Would you like to <a href="{{ url('contact') }}">create a new Contact</a>?</label>');
            e.preventDefault();
        }

        {{--if(org_id == ''){--}}
            {{--$('#organization_id').parent().find('label').html('Organization <label style="color:#f00;"> This organization in not part of your existing organizations. Would you like to <a href="{{ url('organization') }}">create a new organization</a>?</label>');--}}
            {{--e.preventDefault();--}}
        {{--}--}}
    })

})
</script>
@endsection
