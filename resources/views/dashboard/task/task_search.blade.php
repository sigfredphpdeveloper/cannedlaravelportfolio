
<style>
.put-left-radius{
    -webkit-border-radius: 6px 0 0 6px !important;
    border-radius: 6px 0 0 6px !important;
}
.margi-bottom{
    margin-bottom: 10px;
}
.margi-bottom label{width:120px;}
.margi-bottom input[type="text"]{
    -webkit-border-radius: 6px 6px 6px 6px !important;
    border-radius: 6px 6px 6px 6px !important;
    width:250px;
}

/**
 * Sortable css
 **/
#sort_records { list-style-type: none; margin: 0; padding: 0;  }
#sort_records li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 12px; }
#sort_records li span { float:left;margin-left: -1.3em; margin-top:7px; }
.padding-left-none{padding-left:0 !important;}

.sort_records_edit { list-style-type: none; margin: 0; padding: 0;  }
.sort_records_edit li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 12px; }
.sort_records_edit li span { float:left;margin-left: -1.3em; margin-top:7px; }

.main-label{
    width:100px;
}

</style>
<div class="row" style="margin-bottom:10px;">
    <div class="col-sm-3 col-xs-12" style="padding-right:5px !important;" >
        <select id="board_name" class="form-control">
            @foreach($boards as $board)
                <option value="{{$board['id']}}" <?=($board['id']==$board_id)?'selected="selected"':'';?> >{{$board['board_title']}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-sm-3 pull-right" style="padding-left:5px">
        <button type="button" class="btn btn-primary board_edit_icon pull-right" data-id="{{$board_id}}" style="margin-left:5px;">Edit board</button>
        <button type="button" class="btn btn-primary pull-right" data-toggle="modal" data-target="#addBoardModal">Add new Board</button>
    </div>
</div>

<div class="col-sm-5 col-xs-12" style="padding-left: 0;">
    <form class="form-inline" action="{{$url}}" method="POST">
        <div class="form-group">
            <label class="sr-only" for="exampleInputAmount">{{trans("crm.Amount (in dollars)")}}</label>
            <div class="input-group">
                <div class="input-group-addon put-left-radius" style="padding: 0;border: 0;">
                    {!! csrf_field() !!}
                    <select name="column_name" class="form-control put-left-radius" id="search_filter">
                        <option value="task_title">Task Name</option>
                        <option value="assigned_name">Assigned Name</option>
                        <option value="category">Category</option>
                    </select>
                </div>
                <div id="change_here">
                    <input type="text" name="column_value" value="" class="form-control" placeholder="{{trans("crm.Search")}}">
                </div>
                <span class="input-group-btn"> <button class="btn btn-primary" id="search_btn" type="submit">{{trans("crm.Search")}}</button></span>
            </div>
        </div>
    </form>

</div>
<div class="col-sm-2 pull-right">
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addtaskModal">Add Task</button>
    <a href="{{url('/task-setting')}}" class="btn btn-primary "><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Customize</a>
</div>
<div class="col-sm-2 col-xs-12 pull-right" style="padding-left:0;">
    <select id="page_display" class="form-control">
        <option value="task_view" <?=($page == 'task_view')?'selected="selected"':''?> >Task View</option>
        <option value="list_view" <?=($page == 'list_view')?'selected="selected"':''?> >List View</option>
    </select>
</div>
<div class="col-md-12" style="height:10px;"></div>




<!-- add board modal -->
<div class="modal fade" id="addBoardModal" tabindex="-1" role="dialog" aria-labelledby="addBoardModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addBoardModalLabel">Add new Board</h4>
            </div>
            <form action="{{url('create_board')}}" method="POST" class="form-inline">
                <div class="modal-body">
                    {!! csrf_field() !!}
                    <div class="margi-bottom">
                        <div class="form-group">
                            <label class="main-label">Board Name</label>
                            <input type="text" name="name" value="" class="form-control" placeholder="Board Name" required>
                        </div>
                    </div>
                    <div class="col-xs-12" style="padding:0;">
                        <div class="">
                            <label class="main-label">Visibility</label>
                            <label style="width:60px"><input type="radio" name="visibility" value="me" class="form-control visibility_options" checked="checked" />Me</label>
                            <label style="width:60px"><input type="radio" name="visibility" value="team" class="form-control visibility_options" />Team</label>
                            <label style="width:80px"><input type="radio" name="visibility" value="everyone" class="form-control visibility_options" />Everyone</label>
                        </div>

                        <select name="teams[]" id="select_team" class="form-control" multiple style="margin-left:100px;display:none;" >
                            @foreach($teams as $team)
                                <option value="{{$team['id']}}">{{$team['team_name']}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="">
                        <label>
                            Columns
                            <button type="button" id="add_column" class="btn btn-primary btn-sm" style="padding: 3px 10px;">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                            </button>
                        </label>
                        <div id="columns_error" style="color:#f00;font-weight:bold;padding-left:5px;"></div>
                        <ul id="sort_records">
                            <li class="ui-state-default" data-id="">
                                <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
                                <input type="text" name="columns[]" value="To Do" required class="form-control column-name" />
                                <button type="button" class="close" data-type="column"><span aria-hidden="true">&times;</span></button>
                            </li>
                            <li class="ui-state-default" data-id="">
                                <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
                                <input type="text" name="columns[]" value="Urgent" required class="form-control column-name" />
                                <button type="button" class="close" data-type="column"><span aria-hidden="true">&times;</span></button>
                            </li>
                        </ul>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit"    class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>






<!-- add board modal -->
<div class="modal fade" id="editBoardModal" tabindex="-1" role="dialog" aria-labelledby="editBoardModalLabel">
    <div class="modal-dialog" role="document">
        <div id="modal-content" class="modal-content">

        </div>
    </div>
</div>


<div class="modal fade" id="archived_confirmation" tabindex="-1" style="z-index: 10000" role="dialog" aria-labelledby="archived_confirmationLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Archived</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to set as archived?</p>
            </div>
            <div class="modal-footer">
                <form action="{{url('set-archived')}}" method="POST">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" value="" id="archived_line_id" />
                    <input type="hidden" name="page" value="{{$page}}" id="archived_line_id" />
                    <input type="hidden" name="board_id" value="{{$board_id}}" id="archived_line_id" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Yes</button>
                </form>
            </div>
        </div>
    </div>
</div>



<script>
$(document).ready(function(){
    $('#board_name').on('change', function(){
        var val = $(this).val();

        if($('#page_display').val() == 'list_view'){
            window.location = "{{url('list-view')}}"+"/"+val;
        }else if($('#page_display').val() == 'task_view'){
            window.location = "{{url('tasks')}}"+"/"+val;
        }

    })

    $('#page_display').on('change', function(){
        var val = $(this).val();
        if(val == 'list_view'){
            location.href='{{url('list-view')}}/{{$board_id}}';
        }else if(val == 'task_view'){
            location.href='{{url('tasks')}}/{{$board_id}}';
        }
    })




    $('.board_edit_icon').on('click', function(){
        var board_id = $(this).data('id');
        var editmodal = $('#editBoardModal');


        $.ajax({
            type: 'POST',
//            dataType: 'JSON',
            data:{'id':board_id, '_token':'{{ csrf_token() }}'},
            url:'{{url('open_board')}}',
            success:function(response){
                $('#modal-content').html(response);
                editmodal.modal('show');
            }
        })
    })

    $( "#sort_records" ).sortable().disableSelection();

    $('#sort_records').on('click', '.close', function(e){
        var type = $(this).data('type');

        if(type == 'column'){
            var li = $('#sort_records').find('li').length;

            if(li <= 2){
                $('#columns_error').html('The minumun Column list is 2').focus();
                setTimeout(function(){
                    $('#columns_error').html('');
                },3000);
            }else{
                $(this).parent().remove();
            }

            e.preventDefault();
        }
    })

    $('#add_column').on('click', function(){
        var column = '<li class="ui-state-default" data-id="">'+
                '<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'+
                '<input type="text" name="columns[]" value="" required class="form-control column-name" />'+
                '<button type="button" class="close" data-type="column"><span aria-hidden="true">&times;</span></button>'+
                '</li>';

        $('#sort_records').append(column);
    })


    $('.visibility_options').on('change',function(){
        var selected = $('.visibility_options:checked').val();

        if(selected == 'team'){
            $('#select_team').show();
        }else{
            $('#select_team').hide();
        }
    })

    $('#archive_task').on('click', function(){
        var id = $(this).attr('data-id');

        $('#archived_line_id').val(id);

        $('#archived_confirmation').modal('show');
    })


})
</script>