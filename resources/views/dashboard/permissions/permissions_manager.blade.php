@extends('dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        </div>

        <form action="{{url('/save_role_permission')}}" method="POST" >
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="col-md-12">
                <a href="{{ url('/admin_settings') }}" class="btn btn-xs btn-default"><i class="fa fa-caret-left"></i> Back To Settings</a>

                <h1>{{trans("manage_permissions.Manage Permissions")}}</h1>

                <?php
                    $count = count($permissions);
                ?>

                <table class="table table-striped">

                    <tr>
                        <th></th>

                        @foreach($permissions as $permission)
                            <th>{{$permission['feature']}}</th>
                        @endforeach

                    </tr>

                    @foreach($roles as $role)

                        <tr>
                            <td>
                            {{$role->role_name}}
                            </td>

                            @foreach($permissions as $permission)

                                <td>
                                    <input type="checkbox" name="user_roles_permissions[]"

                                    <?php echo ($role['role_name'] =='Administrator')?'checked':'';?>
                                    <?php echo ($role['role_name'] =='Administrator')?'disabled':'';?>

                                    value="{{$role['id']}}-{{$permission['id']}}"
                                     <?php if(count($role->role_permissions) AND in_array($permission['id'],$role->role_permissions->pluck('id')->toArray())):?>
                                        checked="checked"
                                     <?php endif;?>

                                     />
                                </td>
                            @endforeach

                        </tr>

                    @endforeach

                </table>

            </div>

            <div class="col-md-12">
                <input type="submit" value="{{trans("manage_permissions.Save")}}" class="btn btn-primary" data-toggle="tooltip" title="{{trans("manage_permissions.Save Assigned Permissions")}}" />
            </div>

        </form>

    </div>
    <!-- end row -->

</section>
<!-- end widget grid -->


<script type="text/javascript">

    $(document).ready(function(){

        $('[data-toggle="tooltip"]').tooltip();

    });

</script>
@endsection
