@extends('dashboard.layouts.master')



@section('content')

<style>
.boards{
    width:300px;
    height:100px;
    border:1px solid #A4A4A4;
    -webkit-border-radius: 20px 20px 20px 20px;
    border-radius: 20px 20px 20px 20px;
    float:left;
    margin: 20px;
    position: relative;
}
a.custom{
    font-weight: bold;
    letter-spacing: -1px;
    font-size: 22px;
    line-height: normal;
    display:block;
    float:left;
    text-align: center;
    width: 100%;
    color:#FD9941!important;
}
.board_edit_icon{
    float:right;
    display:none;
    position: relative;
    margin-right: 10px;
    padding: 2px 10px;
}
.board_delete{
    float:right;
    display:none;
    position: relative;
    margin-right: 10px;
    padding: 2px 10px;
}
.board_edit_icon:hover{
    cursor: pointer;
}
#board_title{
    float:left;
    margin-top: 0;
}
.main-label{
    width:100px;
}

.column-name{
    width:250px !important;
}
.visibility_options{
    vertical-align: -12px !important;
    margin-right: 5px !important;
}


/**
 * Sortable css
 **/
#sort_records { list-style-type: none; margin: 0; padding: 0;  }
#sort_records li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 12px; }
#sort_records li span { float:left;margin-left: -1.3em; margin-top:7px; }
.padding-left-none{padding-left:0 !important;}

.sort_records_edit { list-style-type: none; margin: 0; padding: 0;  }
.sort_records_edit li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 12px; }
.sort_records_edit li span { float:left;margin-left: -1.3em; margin-top:7px; }

</style>

<div id="content" class="">

    <h2 id="board_title">Boards</h2>

    <div class="col-sm-3" >
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addBoardModal">Add new Board</button>
    </div>
    <div class="col-sm-3 pull-right">
        <a href="{{url('/task-setting')}}" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Customize</a>
    </div>
    <div class="col-md-12" style="height:10px;"></div>


    @if(!empty($boards))
        @foreach($boards as $board)
            <div class="boards">
                <div style="height:24px;margin-top:5px;">
                    <button type="button" class="btn btn-danger btn-sm board_delete" data-id="{{$board['id']}}">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                    <button type="button" class="btn btn-primary btn-sm board_edit_icon" data-id="{{$board['id']}}">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </button>
                </div>
                <a class="custom" href="{{url('tasks')}}/{{$board['id']}}">{{$board['board_title']}}</a>
            </div>
        @endforeach
    @else
        <div class="col-xs-12" style="border:3px solid #ccc;padding:20px 15px;">
            Empty Board!
        </div>
    @endif

</div>



<link rel="stylesheet" type="text/css" media="screen" href="https://code.jquery.com/ui/jquery-ui-git.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.devbridge-autocomplete/1.2.24/jquery.autocomplete.js"></script>


<!-- add board modal -->
<div class="modal fade" id="addBoardModal" tabindex="-1" role="dialog" aria-labelledby="addBoardModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addBoardModalLabel">Add new Board</h4>
            </div>
            <form action="{{url('create_board')}}" method="POST" class="form-inline">
                <div class="modal-body">
                    {!! csrf_field() !!}
                    <div class="margi-bottom">
                        <div class="form-group">
                            <label class="main-label">Board Name</label>
                            <input type="text" name="name" value="" class="form-control" placeholder="Board Name" required>
                        </div>
                    </div>
                    <div class="col-xs-12" style="padding:0;">
                        <div class="">
                            <label class="main-label">Visibility</label>
                            <label style="width:60px"><input type="radio" name="visibility" value="me" class="form-control visibility_options" checked="checked" />Me</label>
                            <label style="width:60px"><input type="radio" name="visibility" value="team" class="form-control visibility_options" />Team</label>
                            <label style="width:80px"><input type="radio" name="visibility" value="everyone" class="form-control visibility_options" />Everyone</label>
                        </div>

                        <select name="teams[]" id="select_team" class="form-control" multiple style="margin-left:100px;display:none;" >
                            @foreach($teams as $team)
                            <option value="{{$team['id']}}">{{$team['team_name']}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="">
                        <label>
                            Columns
                            <button type="button" id="add_column" class="btn btn-primary btn-sm" style="padding: 3px 10px;">
                                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                            </button>
                        </label>
                        <div id="columns_error" style="color:#f00;font-weight:bold;padding-left:5px;"></div>
                        <ul id="sort_records">
                            <li class="ui-state-default" data-id="">
                                <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
                                <input type="text" name="columns[]" value="To Do" required class="form-control column-name" />
                                <button type="button" class="close" data-type="column"><span aria-hidden="true">&times;</span></button>
                            </li>
                            <li class="ui-state-default" data-id="">
                                <span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
                                <input type="text" name="columns[]" value="Urgent" required class="form-control column-name" />
                                <button type="button" class="close" data-type="column"><span aria-hidden="true">&times;</span></button>
                            </li>
                        </ul>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit"    class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>



<!-- add board modal -->
<div class="modal fade" id="editBoardModal" tabindex="-1" role="dialog" aria-labelledby="editBoardModalLabel">
    <div class="modal-dialog" role="document">
        <div id="modal-content" class="modal-content">

        </div>
    </div>
</div>


<div class="modal fade" id="DeleteBoardModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="mySmallModalLabel">Delete Board Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure to delete this board?</p>
                <p>There will be no rebirt of this.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <form action="{{url('delete_board')}}" method="POST" class="pull-right" style="margin-left:10px;">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" id="will_delete_id" value="" />
                    <button type="submit" class="btn btn-danger">Yes</button>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection


@section('page-script')

<script>
$(document).ready(function(){

    $('.visibility_options').on('change',function(){
        var selected = $('.visibility_options:checked').val();

        if(selected == 'team'){
            $('#select_team').show();
        }else{
            $('#select_team').hide();
        }
    })

    $('.board_edit_icon').on('click', function(){
        var board_id = $(this).data('id');
        var editmodal = $('#editBoardModal');


        $.ajax({
            type: 'POST',
//            dataType: 'JSON',
            data:{'id':board_id, '_token':'{{ csrf_token() }}'},
            url:'{{url('open_board')}}',
            success:function(response){
                $('#modal-content').html(response);
                editmodal.modal('show');
            }
        })
    })

    $('.board_delete').on('click', function(){
        var board_id = $(this).data('id')
        $('#will_delete_id').val(board_id);
        $('#DeleteBoardModal').modal('show');
    })

    $('.boards').mouseenter(function() {
        $(this).find('.board_edit_icon').show();
        $(this).find('.board_delete').show();
    }).mouseleave(function() {
        $(this).find('.board_edit_icon').hide();
        $(this).find('.board_delete').hide();
    });


    $( "#sort_records" ).sortable({
        connectWith: ".connectedSortable",
        stop: function(e,ui){
        },
        update: function(event, ui){
            if(ui.sender){

                var group = ui.item.parent().data('group');
                var id = ui.item.data('id');

                {{--$.ajax({--}}
                    {{--url : '{{url('/stage_update')}}',--}}
                    {{--type : 'POST',--}}
                    {{--data : {'id':id, 'group':group, '_token':'{{ csrf_token() }}'},--}}
                    {{--dataType:'json',--}}
                    {{--success:function(response){--}}
                        {{--console.log(response);--}}
                    {{--}--}}
                {{--})--}}
            }
        }
    }).disableSelection();

    $('#sort_records').on('click', '.close', function(e){
        var type = $(this).data('type');

        if(type == 'column'){
            var li = $('#sort_records').find('li').length;

            if(li <= 2){
                $('#columns_error').html('The minumun Column list is 2').focus();
                setTimeout(function(){
                    $('#columns_error').html('');
                },3000);
            }else{
                $(this).parent().remove();
            }

            e.preventDefault();
        }
    })

    $('#add_column').on('click', function(){
        var column = '<li class="ui-state-default" data-id="">'+
                            '<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>'+
                        '<input type="text" name="columns[]" value="" required class="form-control column-name" />'+
                        '<button type="button" class="close" data-type="column"><span aria-hidden="true">&times;</span></button>'+
                    '</li>';

        $('#sort_records').append(column);
    })


})
</script>
@endsection