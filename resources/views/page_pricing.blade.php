<!doctype html>
<?php
    function page_finalhome_trans($key,$lang = null){
        if(!$lang){
            $lang = 'en';
        }
        return trans($key,[],null,$lang);
    }
?>
<html lang="en">
<head>

    @include('meta')
    @include('frontend.includes.favicon')

  <!--[if lt IE 9]>
  <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/font-awesome.min.css') }}">

  <link rel="stylesheet" href="//bxslider.com/lib/jquery.bxslider.css" type="text/css" />
  
  <link rel="stylesheet" href="{{ asset('css/custom.css') }}" type="text/css" rel="stylesheet" />
    <link href="{{url('/')}}/assets/public_assets/style.css" rel="stylesheet" type="text/css" />

  <link rel="stylesheet" href="{{ asset('css/jquery.mmenu.all.css') }}">

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src= "{{ asset('js/jquery.mmenu.min.all.js') }}"></script>
  <script src= "{{ asset('js/jquery.bxslider.min.js') }}"></script>
  <script src= "{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>

<style>
ul {
    padding-top: 10px;
}
ul.bullet li{
    background:none !important;
    font-size: 16px !important;
    padding-left: 10px !important;
}

td {
    position: relative;
    padding: 30px 7px !important;
    font-size: 18px !important;
    border: 1px solid #ddd;
    width: 15%;
}

tr:first-child {
    padding: 30px 30px !important;
    border:none;
}

tr td:first-child {
    padding-left: 15px !important;
}

table {
    margin: auto;
    margin-top: 20px;
    width: 80%;
}

h3 {
    padding-top: 15px;
    text-align: center;
}

.check {
    position: absolute;
    top: 45%;
}

.pricing-table-mobile{
    width: 100%;
}

.pricing-table-mobile .check{
    left: 45%;
}

</style>

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src= "{{ asset('js/menu/jquery.mmenu.min.all.js') }}"></script>
  <script src= "{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/menu/script.js') }}"></script>
</head>

<body>


    @include('frontend.includes.google_tag_manager')

    @include('page_head')

    <div id="content"><!--content-->
    	<div class="container-fluid custom-bg2" >
            <div class="container">
                <h2 align="center" style="font-size:36px;color:#fff;">Pricing</h2>

                <!--<p align="center"><a href="{{ url('/register') }}" class="btn btn-orange btn-lg get-access"
                                          >Sign up</a></p>
-->
              <p align="center"><a href="{{url('register')}}" class="btn btn-default btn-md ">Sign Up</a></p>

            </div>
        </div>
        
        <div class="container-fluid visible-md visible-lg" style="padding-top:180px;">
            <div class="container">
                <div class="col-md-12 " >
                    <table class="pricing-table">
                        <tr>
                            <td style="border:0;"></td>
                            <td style="border:0;background:#999;color:#fff;" align="center">Free</td>
                            <td style="border:0;background:#F2611F;color:#fff;" align="center">Standard</td>
                            <td style="border:0;background:#999;color:#fff;" align="center">Entreprise</td>
                        </tr>
                        <tr>
                            <td>Number of employees</td>
                            <td style="border-top:0;" align="center">Up to 10</td>
                            <td style="border-top:0;" align="center">Up to 50</td>
                            <td style="border-top:0;" align="center">Unlimited</td>
                        </tr>
                        <tr>
                            <td bgcolor="#eff3f3">
                            Admin Settings:
                                <ul class="bullet">
                                    <li>choose your features</li>
                                    <li>create teams</li>
                                    <li>create roles</li>
                                    <li>rights & permissions</li>
                                    <li>add your own logo</li>
                                </ul>
                            </td>
                            <td bgcolor="#eff3f3" align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                            <td bgcolor="#eff3f3" align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                            <td bgcolor="#eff3f3" align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td>
                                HR features:
                                <ul class="bullet">
                                    <li>company directory</li>
                                    <li>emergency contacts</li>
                                    <li>announcements</li>
                                    <li>recognition board</li>
                                    <li>expenses claims</li>
                                    <li>documents sharing</li>
                                    <li>assets management</li>
                                    <li>employee self-service</li>
                                </ul>
                            </td>
                            <td align="center"><img class="check"  src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                            <td align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                            <td align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td bgcolor="#eff3f3">
                                Leave management:
                                <ul class="bullet">
                                    <li>leave applications</li>
                                    <li>approval workflow</li>
                                    <li>leave balance tracking</li>
                                    <li>calendars</li>
                                </ul>
                            </td>
                            <td bgcolor="#eff3f3" align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                            <td bgcolor="#eff3f3" align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                            <td bgcolor="#eff3f3" align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td>
                                Learning management system:
                                <ul class="bullet">
                                    <li>eLearning</li>
                                    <li>create trainings</li>
                                    <li>monitor progress</li>
                                </ul>
                            </td>
                            <td align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                            <td align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                            <td align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td bgcolor="#eff3f3">
                                Productivity features:
                                <ul class="bullet">
                                    <li>contacts & CRM</li>
                                    <li>tasks and boards</li>
                                    <li>personal notes</li>
                                </ul>
                            </td>
                            <td bgcolor="#eff3f3" align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                            <td bgcolor="#eff3f3" align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                            <td bgcolor="#eff3f3" align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td>
                                Payroll (Singapore only):
                                <ul class="bullet">
                                    <li>itemized payslips</li>
                                    <li>IRAS reports</li>
                                    <li>CPF reports</li>
                                </ul>
                            </td>
                            <td align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                            <td align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                            <td align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td bgcolor="#eff3f3">
                                Storage space for documents
                            </td>
                            <td bgcolor="#eff3f3" align="center">1GB</td>
                            <td bgcolor="#eff3f3" align="center">20GB</td>
                            <td bgcolor="#eff3f3" align="center">Unlimited</td>
                        </tr>
                        <tr>
                            <td>
                                Pricing
                            </td>
                            <td align="center">Free</td>
                            <td align="center">$49/m</td>
                            <td align="center">Contact us</td>
                        </tr>
                        <tr>
                            <td style="border:0;"></td>
                            <td align="center"><a class="btn btn-primary" style="font-size:20px" href="{{ url('register') }}">Sign Up</a></td>
                            <td align="center"><a class="btn btn-primary" style="font-size:20px" href="{{ url('register') }}">Sign Up</a></td>
                            <td align="center"><a class="btn btn-primary" style="font-size:20px" href="{{ url('register') }}">Sign Up</a></td>
                        </tr>
                    </table>                    
                </div>
            </div>
        </div>

        <div class="container-fluid hidden-md hidden-lg" style="padding-top:180px;padding-bottom:80px;">
            <div class="container">
                <div class="col-md-12 " >
                    <table class="pricing-table-mobile">
                        <tr>
                            <td class="pricing-table-button" colspan="2" style="border:0;background:#999;color:#fff;" align="center">Free</td>
                        </tr>
                        <tr>
                            <td>Number of employees</td>
                            <td style="border-top:0;" align="center">Up to 10</td>
                        </tr>
                        <tr>
                            <td bgcolor="#eff3f3">
                            Admin Settings:
                                <ul class="bullet">
                                    <li>choose your features</li>
                                    <li>create teams</li>
                                    <li>create roles</li>
                                    <li>rights & permissions</li>
                                    <li>add your own logo</li>
                                </ul>
                            </td>
                            <td bgcolor="#eff3f3" align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td>
                                HR features:
                                <ul class="bullet">
                                    <li>company directory</li>
                                    <li>emergency contacts</li>
                                    <li>announcements</li>
                                    <li>recognition board</li>
                                    <li>expenses claims</li>
                                    <li>documents sharing</li>
                                    <li>assets management</li>
                                    <li>employee self-service</li>
                                </ul>
                            </td>
                            <td align="center"><img class="check"  src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td bgcolor="#eff3f3">
                                Leave management:
                                <ul class="bullet">
                                    <li>leave applications</li>
                                    <li>approval workflow</li>
                                    <li>leave balance tracking</li>
                                    <li>calendars</li>
                                </ul>
                            </td>
                            <td bgcolor="#eff3f3" align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td>
                                Learning management system:
                                <ul class="bullet">
                                    <li>eLearning</li>
                                    <li>create trainings</li>
                                    <li>monitor progress</li>
                                </ul>
                            </td>
                            <td align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td bgcolor="#eff3f3">
                                Productivity features:
                                <ul class="bullet">
                                    <li>contacts & CRM</li>
                                    <li>tasks and boards</li>
                                    <li>personal notes</li>
                                </ul>
                            </td>
                            <td bgcolor="#eff3f3" align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td>
                                Payroll (Singapore only):
                                <ul class="bullet">
                                    <li>itemized payslips</li>
                                    <li>IRAS reports</li>
                                    <li>CPF reports</li>
                                </ul>
                            </td>
                            <td align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td bgcolor="#eff3f3">
                                Storage space for documents
                            </td>
                            <td bgcolor="#eff3f3" align="center">1GB</td>
                        </tr>
                        <tr>
                            <td>
                                Pricing
                            </td>
                            <td align="center">Free</td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center"><a class="btn btn-primary" style="font-size:20px" href="{{ url('register') }}">Sign Up</a></td>
                        </tr>
                    </table>               
                    <table class="pricing-table-mobile">
                        <tr>
                            <td class="pricing-table-button" colspan="2" style="border:0;background:#F2611F;color:#fff;" align="center">Standard</td>
                        </tr>
                        <tr>
                            <td>Number of employees</td>
                            <td style="border-top:0;" align="center">Up to 50</td>
                        </tr>
                        <tr>
                            <td bgcolor="#eff3f3">
                            Admin Settings:
                                <ul class="bullet">
                                    <li>choose your features</li>
                                    <li>create teams</li>
                                    <li>create roles</li>
                                    <li>rights & permissions</li>
                                    <li>add your own logo</li>
                                </ul>
                            </td>
                            <td bgcolor="#eff3f3" align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td>
                                HR features:
                                <ul class="bullet">
                                    <li>company directory</li>
                                    <li>emergency contacts</li>
                                    <li>announcements</li>
                                    <li>recognition board</li>
                                    <li>expenses claims</li>
                                    <li>documents sharing</li>
                                    <li>assets management</li>
                                    <li>employee self-service</li>
                                </ul>
                            </td>
                            <td align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td bgcolor="#eff3f3">
                                Leave management:
                                <ul class="bullet">
                                    <li>leave applications</li>
                                    <li>approval workflow</li>
                                    <li>leave balance tracking</li>
                                    <li>calendars</li>
                                </ul>
                            </td>
                            <td bgcolor="#eff3f3" align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td>
                                Learning management system:
                                <ul class="bullet">
                                    <li>eLearning</li>
                                    <li>create trainings</li>
                                    <li>monitor progress</li>
                                </ul>
                            </td>
                            <td align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td bgcolor="#eff3f3">
                                Productivity features:
                                <ul class="bullet">
                                    <li>contacts & CRM</li>
                                    <li>tasks and boards</li>
                                    <li>personal notes</li>
                                </ul>
                            </td>
                            <td bgcolor="#eff3f3" align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td>
                                Payroll (Singapore only):
                                <ul class="bullet">
                                    <li>itemized payslips</li>
                                    <li>IRAS reports</li>
                                    <li>CPF reports</li>
                                </ul>
                            </td>
                            <td align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td bgcolor="#eff3f3">
                                Storage space for documents
                            </td>
                            <td bgcolor="#eff3f3" align="center">20GB</td>
                        </tr>
                        <tr>
                            <td>
                                Pricing
                            </td>
                            <td align="center">$49/m</td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center"><a class="btn btn-primary" style="font-size:20px" href="{{ url('register') }}">Sign Up</a></td>
                        </tr>
                    </table>                
                    <table class="pricing-table-mobile">
                        <tr>
                            <td class="pricing-table-button" colspan="2" style="border:0;background:#999;color:#fff;" align="center">Entreprise</td>
                        </tr>
                        <tr>
                            <td>Number of employees</td>
                            <td style="border-top:0;" align="center">Unlimited</td>
                        </tr>
                        <tr>
                            <td bgcolor="#eff3f3">
                            Admin Settings:
                                <ul class="bullet">
                                    <li>choose your features</li>
                                    <li>create teams</li>
                                    <li>create roles</li>
                                    <li>rights & permissions</li>
                                    <li>add your own logo</li>
                                </ul>
                            </td>
                            <td bgcolor="#eff3f3" align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td>
                                HR features:
                                <ul class="bullet">
                                    <li>company directory</li>
                                    <li>emergency contacts</li>
                                    <li>announcements</li>
                                    <li>recognition board</li>
                                    <li>expenses claims</li>
                                    <li>documents sharing</li>
                                    <li>assets management</li>
                                    <li>employee self-service</li>
                                </ul>
                            </td>
                            <td align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td bgcolor="#eff3f3">
                                Leave management:
                                <ul class="bullet">
                                    <li>leave applications</li>
                                    <li>approval workflow</li>
                                    <li>leave balance tracking</li>
                                    <li>calendars</li>
                                </ul>
                            </td>
                            <td bgcolor="#eff3f3" align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td>
                                Learning management system:
                                <ul class="bullet">
                                    <li>eLearning</li>
                                    <li>create trainings</li>
                                    <li>monitor progress</li>
                                </ul>
                            </td>
                            <td align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td bgcolor="#eff3f3">
                                Productivity features:
                                <ul class="bullet">
                                    <li>contacts & CRM</li>
                                    <li>tasks and boards</li>
                                    <li>personal notes</li>
                                </ul>
                            </td>
                            <td bgcolor="#eff3f3" align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td>
                                Payroll (Singapore only):
                                <ul class="bullet">
                                    <li>itemized payslips</li>
                                    <li>IRAS reports</li>
                                    <li>CPF reports</li>
                                </ul>
                            </td>
                            <td align="center"><img class="check" src="{{ asset('img/check-mark.png') }}" alt="" /></td>
                        </tr>
                        <tr>
                            <td bgcolor="#eff3f3">
                                Storage space for documents
                            </td>
                            <td bgcolor="#eff3f3" align="center">Unlimited</td>
                        </tr>
                        <tr>
                            <td>
                                Pricing
                            </td>
                            <td align="center">Contact us</td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center"><a class="btn btn-primary" style="font-size:20px" href="{{ url('register') }}">Sign Up</a></td>
                        </tr>
                    </table>                     
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="container">
                <div class="col-md-12 ">
                    <h3 style="width: 80%;padding-top: 15px;text-align:right;margin: auto">30-day free trial – No credit card required.</h3>
                </div>
            </div>
        </div>
        <div class="container-fluid" style="padding-bottom:100px;">
        	<div class="col-md-12">

        	<!--
        	    <a href="{{ url('/register') }}" class="btn-lg get-access"  style="background:none;border:0;display:block;margin:0 auto; outline: none;">
            	    <img src="{{url('')}}/img/redtheme/access-btn.png" alt="Sign up" class="img-responsive center-block" />
        	    </a>
        	    -->
            </div>
        </div>
    </div><!--content-->

    <script type="text/javascript">
        $(document).ready(function(){
            $(".pricing-table-button").click(function(){
                $('.pricing-table-mobile tr:not(:first-child)').hide();
                $('.pricing-table-button').show();
                $(this).parents('.pricing-table-mobile').find('tr').show();
            });
            $('.pricing-table-mobile tr:not(:first-child)').hide();
            $('.pricing-table-mobile').first().find('tr').show();
        });
        
    </script>
    @include('page_footer')

    @include('get_access_now')

</body>
</html>