<!doctype html>
<?php
    function page_finalhome_trans($key,$lang = null){
        if(!$lang){
            $lang = 'en';
        }
        return trans($key,[],null,$lang);
    }
?>
<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Zenintra</title>
  <meta name="description" content="{{page_finalhome_trans('finalhome.Free intranet and internal communication for startups and small businesses')}}">
  <meta name="title" content='{{page_finalhome_trans("landingpage.All-in-one intranet")}}'>
<!-- #FAVICONS -->
    @include('frontend.includes.favicon')
  <!--[if lt IE 9]>
  <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/font-awesome.min.css') }}">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  {{--<link rel="stylesheet" href="//bxslider.com/lib/jquery.bxslider.css" type="text/css" />--}}

  <link rel="stylesheet" href="{{ asset('css/custom.css') }}" type="text/css" rel="stylesheet" />

  <link rel="stylesheet" href="{{ asset('css/jquery.mmenu.all.css') }}">



  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src= "{{ asset('js/jquery.mmenu.min.all.js') }}"></script>
  <script src= "{{ asset('js/jquery.bxslider.min.js') }}"></script>
  <script src= "{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/menu/script.js') }}"></script>

<style>
.pagination {
    display: block;
    overflow: hidden;
}
@media (max-width: 1260px) {
    .mobile-margin-bottom{
        margin-bottom:30px;
    }
}
</style>
</head>

<body>
    @include('frontend.includes.google_tag_manager')

    @include('page_head')

  <style>
    #content ul li { background:none !important; }
  </style>

    <div id="content"><!--content-->
        <div class="container-fluid custom-bg2" style="padding-top:163px;">
            <div class="container">
                <h2 align="center" style="font-size:48px;color:#fff;">{{ $topic->title }}</h2>
                <p>&nbsp;</p>
            </div>
        </div>
        <div class="container-fluid" style="padding-top:50px;">
            <div class="container" style="display: block;overflow: hidden;padding: 20px 0;">
                <div class="pull-right">
                    <a href="{{ url('tutorials', $company_id) }}" style="text-decoration: underline;">Back to table of contents</a>
                </div>
            </div>
            <div class="container">
            @if( count($pages)!= 0 )
                @foreach( $pages as $page )
                    <h2>{{ $page->title }}</h2>
                    {!! $page->content !!}
                @endforeach

                <div class="pagination">
                    @if( $pages->currentPage() != 1 )
                    <div class="previous-pages pull-left">
                        <a href="{{ $pages->previousPageUrl() }}">&laquo; Previous</a>
                    </div>
                    @endif
                    @if( $pages->hasMorePages() )
                        <div class="next-pages pull-right">
                            <a href="{{ $pages->nextPageUrl() }}">Next &raquo;</a>
                        </div>
                    @endif
                </div>
            @else
                <p style="text-align: center;"> No available pages </p>
            @endif
            </div>
        </div>
    </div><!--content-->

    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Watch the tour</h4>
                </div>
                <div class="modal-body" style="padding:0;">
                    <div id="popupVid">
                        <iframe width="100%" height="400px"src="https://www.youtube.com/embed/u65pCk6lu4I?rel=0">
                        </iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('page_footer')



<div id="get_access_now_popup" class="modal fade" role="dialog"><!-- formPopModal -->
    <div class="modal-dialog">
        <div class="modal-content"><!-- Modal content-->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Get Access Now</h4>
        </div>



        <div class="modal-body">

        <form id="access_form">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
             <div class="form-group">
                <label for="exampleInputPassword1">Name</label>
                <input type="text" class="form-control" name="name" id="exampleInputPassword1" placeholder="Name">
              </div>

              <div class="form-group">
                <label for="exampleInputEmail1">Email address *</label>
                <input type="email" class="form-control" name="email" required id="exampleInputEmail1" placeholder="Email">
              </div>

              <button type="submit" class="btn btn-default">Submit</button>

              <div id="msg_holder" class="alert" style="display:none;">

              </div>

            </form>
        </div>




        </div><!-- Modal content-->
    </div>
</div><!-- formPopModal -->

<script type="text/javascript">
    //Floating Menu
    $(document).scroll(function() {


    // MOBILE MENU
    $('.mobile-menu-container').mmenu({
        classes: 'mm-light',
        counters: true,
        offCanvas: {
            position  : 'left',
            zposition : 'front',
        }
    });

    $('a.mobile-menu-trigger').click(function() {
        $('.mobile-menu-container').trigger('open.mm');
    });

    $(window).resize(function() {
        $('.mobile-menu-container').trigger("close.mm");
    });

    $(document).ready(function($){

        $('#access_form').submit(function(e){
            e.preventDefault();
            var dt = $(this).serializeArray();

            $.ajax({
                url : '{{url('/get_access_now_subscribe')}}',
                type : 'POST',
                data:dt,
                success : function(code){

                    $('#msg_holder').fadeIn();

                    if(code=='success'){
                        $('#msg_holder').removeClass('alert-danger').addClass('alert-success').html('Successfully Subsribed!');
                    }else{
                        $('#msg_holder').removeClass('alert-success').addClass('alert-danger').html('Error Occured!');
                    }
                }
            });

        });
    });

</script>
</body>
</html>
