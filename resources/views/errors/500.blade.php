<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Error | Zenintra</title>

  <!--[if lt IE 9]>
  <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  
  <link rel="stylesheet" href="{{ asset('css/custom.css') }}" type="text/css" rel="stylesheet" />
  
  <link rel="stylesheet" href="{{ asset('css/jquery.mmenu.all.css') }}">

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src= "{{ asset('js/jquery.mmenu.min.all.js') }}"></script>
  <script src= "{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
</head>

<body>
  <?php 
      function page_feature_trans($key,$lang = null){
          if(!$lang){
              $lang = 'en';
          }
          return trans($key,[],null,$lang);
      }
  ?>

  <div id="header" style="background:white;"><!--header-->
      <div class="container">
          <div id="logo" class="col-md-3 col-xs-12"><img src="{{ asset('img/zenitra-logo.png') }}" alt="Zenintra" class="img-responsive" /></div>
            <div style="height:20px;" class="clear visible-xs"></div>
            <div class="clear"></div>
        </div>
    </div><!--header-->
    <div id="content"><!--content-->
      <div id="banner" class="container-fluid text-white"><!--banner-->
          <div class="container">
              <h1 align="center">Oops!</h1>
              <p>There seems to be a problem with our service right now. We are investigating the problem now, please try again in a few minutes.</p>
              <p align="center"><a href="{{url('/')}}">Home</a>&nbsp;&verbar;&nbsp;<a href="{{url('/dashboard')}}">Dashboard</a></p>
              <p></p>
          </div>
        </div><!--banner-->
    </div><!--content-->
    <div id="footer"><!--footer-->
      <div class="container">
          <p align="center">&copy; {{page_feature_trans('landingpage.Copyright 2015 Zenintra.net')}}</p>
        </div>
    </div><!--footer-->    
</body>