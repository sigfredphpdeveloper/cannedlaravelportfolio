 <div class="col-md-12">
    @if(Session::has('errors'))
        <div class="alert alert-danger">
            <p>{{ Session::get('errors') }}</p>
        </div>
    @endif
</div>