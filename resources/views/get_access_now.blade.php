<div id="get_access_now_popup" class="modal fade" role="dialog"><!-- formPopModal -->
    <div class="modal-dialog">
        <div class="modal-content"><!-- Modal content-->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Request early access</h4>
        </div>

        <div class="modal-body">

        <form id="access_form">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
             <div class="form-group">
                <label for="exampleInputPassword1">Name</label>
                <input type="text" class="form-control" name="name" id="exampleInputPassword1" placeholder="Name">
              </div>

              <div class="form-group">
                <label for="exampleInputEmail1">Email address *</label>
                <input type="email" class="form-control" name="email" required id="exampleInputEmail1" placeholder="Email">
              </div>

              <button type="submit" class="btn btn-warning" style="background:#ff6600;border:#ff6600;color:#fff;">Submit</button>

              <div id="msg_holder" class="alert" style="display:none;">

              </div>

            </form>
        </div>




        </div><!-- Modal content-->
    </div>
</div><!-- formPopModal -->


<script>
 $(document).ready(function($){

      $('.get_access_to_register').on('click', function(){
        $('#get_access_now_popup').modal('show');
    })


    $('.lang-select').on('change', function(){
        var lang =  $(this).val();
        if( lang == 'fr' )
        {
            window.location = '{{ url("fr/finalhome") }}';
        }
        else
        {
            window.location = '{{ url("finalhome") }}';
        }
    });

    $('#access_form').submit(function(e){
        e.preventDefault();
        var dt = $(this).serializeArray();

        $.ajax({
            url : '{{url('/get_access_now_subscribe')}}',
            type : 'POST',
            data:dt,
            success : function(code){

                $('#msg_holder').fadeIn();

                if(code=='success'){
                    $('#msg_holder').removeClass('alert-danger').addClass('alert-success').html('Successfully Subscribed!');
                }else{
                    $('#msg_holder').removeClass('alert-success').addClass('alert-danger').html('Error Occured!');
                }
            }
        });



    });
});
</script>