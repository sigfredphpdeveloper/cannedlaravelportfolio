<style>
@media (max-width: 1260px) {
    #footer .container ul li{
        margin:20px 0;
    }
    .mobile-left{
        float:left;
        margin:0 10px;
    }
}
</style>

<div id="footer"><!--footer-->
    <div class="container">
        <div class="col-xs-12 col-sm-6 col-md-3" style="height:177px">
            <h3>Using Zenintra.net</h3>
            <ul>
                <li><a href="{{url('product')}}">Features</a></li>
                <li><a href="{{url('pricing')}}">Pricing</a></li>
                <!--<li><a href="#" data-toggle="modal" data-target="#groove_support">
                Support</a></li>-->
                <li><a href="http://support.zenintra.net">Support</a></li>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-2" style="height:177px">
            <h3>Company</h3>
            <ul>
                <li><a href="{{ url('about-us') }}">About Us</a></li>
                <li><a href="javascript:;" data-toggle="modal" data-target="#contactModal">Contact Us</a></li>
            </ul>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-2" style="height:177px">
            <h3>Legal</h3>
            <ul>
                <li><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
                <li><a href="{{ url('term-of-use') }}">Terms of use</a></li>
            </ul>
        </div>

        <div class="col-xs-12 col-sm-6 col-md-5 text-right" style="height:177px">
            <p><a class="mobile-left" href="https://www.facebook.com/zenintra">
            <img src="<?php echo url('/');?>/img/icon-fb.png" alt="Facebook" /></a>
            <a class="mobile-left" href="https://twitter.com/zenintra">
            <img src="<?php echo url('/');?>/img/icon-twitter.png" alt="Twitter" /></a>
            <a class="mobile-left" href="https://www.linkedin.com/company/zenintra-net?trk=company_logo">
            <img src="<?php echo url('/');?>/img/icon-linked.png" alt="Linked" /></a></p>
        </div>
    </div>
    <div class="container text-right">
        <p style="color:#959595;">&copy; Copyright 2016 Zenintra.net</p>
    </div>
</div><!--footer-->

<!-- Add Groove Support Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="groove_support">
    <div class="modal-dialog">
        <div class="modal-content">
            <div role="content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"> Submit a request </h4>
                </div>

                <div class="modal-body">
                    <div class="message-field"></div>
                    <button type="button" class="btn btn-default dismiss-support" data-dismiss="modal" style="display: none;">OK</button>
                    <form id="support-email" action="{{ secure_url('/support-form/') }}" method="POST" class="smart-form" enctype="multipart/form-data" accept-charset="UTF-8">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <input type="hidden" name="base_url" id="base_url" value="{{ url('/') }}">
                        <fieldset>
                            <section class="form-group">
                                <label for="name">Name</label>
                                <input required type="text" name="name" class="form-control" id="" />
                            </section>
                            <section class="form-group">
                                <label for="email"> Email </label>
                                <input required type="email" name="email" class="form-control" id="" />
                            </section>
                            <section class="form-group">
                                <label for="request_type">Request Type</label>
                                    <select name="request_type" class="form-control">
                                        <option value="my account">My Account</option>
                                        <option value="technical">Technical</option>
                                        <option value="other">Other</option>
                                    </select>                            </section>
                            <section class="form-group">
                                <label for="title"> Title </label>
                                <input required type="text" name="title" class="form-control" id="" />
                            </section>
                            <section class="form-group">
                                <label for="title"> Include File </label>
                                <input type="file" name="file" class="" placeholder="Include some files" />
                            </section>
                            <section class="form-group">
                                <label for="message">Message</label>
                                <textarea required name="message" class="form-control" rows="4"></textarea>
                            </section>
                        </fieldset>
                        <footer>
                            <input type="submit" class="btn btn-default" alt="" value="Submit" />
                        </footer>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Groove support Modal -->

<!-- Add Groove success Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="groove_sent">
    <div class="modal-dialog">
        <div class="modal-content">
            <div role="content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"> Support Email </h4>
                </div>

                <div class="modal-body">
                    <div class="message-field">
                        <h5>Your message has been sent, our support team will get back to you as soon as possible</h5>
                    </div>
                    <button type="button" class="btn btn-default dismiss-support" data-dismiss="modal" style="">OK</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Groove success Modal -->

<!-- Modal -->
<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="contactModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="contactModalLabel">Contact Us</h4>
            </div>
            <form action="javascript:;" method="POST" id="contact_us_form">
                {!! csrf_field() !!}

                <div class="modal-body">

                    <div id="contact_us_error_msg" class="alert alert-danger" role="alert" style="display:none">
                      <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                      <span class="sr-only">Error:</span>
                      Enter a valid email address
                    </div>
                    <div id="contact_us_success_msg" class="alert alert-success" role="alert" style="display:none" >
                      <span class="glyphicon glyphicon-send" aria-hidden="true"></span>
                      Thanks for contacting us, we will get back to you as soon as possible.
                    </div>

                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control" placeholder="Name" required>
                    </div>
                    <div class="form-group">
                        <label>Email address</label>
                        <input type="email" name="email" class="form-control" placeholder="Email" required>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Your Message</label>
                        <textarea name="question" class="form-control" style="min-height:100px;" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="contact_us_btn" class="btn btn-default">Send</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
    $('#contact_us_btn').on('click', function(){
        var me = $(this);
        var form = me.parent().parent().serialize();
        $.ajax({
            type: "POST",
            dataType: "JSON",
            data: form,
            url: '{{url('contact-us')}}',
            success:function(response){
                if(response==true||response=='true'){
                    $('#contact_us_success_msg').fadeIn();
                    setTimeout(function(){
                        $('#contact_us_close_me').trigger('click');
                    },2000)
                }
//                contact_us_error_msg
            }
        })
    })
    $("form#contact_us_form").submit(function(e){
        e.preventDefault();
        return false;
    })
})
</script>
@if(Session::has('success_mail'))
<script>
    $(document).ready(function(){
        $('#groove_sent').modal('show');
    })
</script>
@endif