<!doctype html>
<?php 
    function landing_page_trans($key,$lang = null){
        if(!$lang){
            $lang = 'en';
        }
        return trans($key,[],null,$lang);
    }
?>
<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Zenintra</title>
  <meta name="description" content="{{landing_page_trans('landingpage.Free intranet and internal communication for startups and small businesses')}}">
  <meta name="title" content='{{landing_page_trans("landingpage.All-in-one intranet")}}'>

    <!-- #FAVICONS -->
    @include('frontend.includes.favicon')

  <!--[if lt IE 9]>
  <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  <style>
  	/*================CSS Reset==========*/
html,body,div,span,applet,object,iframe,h1,h2,h3,h4,h5,h6,p,blockquote,pre,a,abbr,acronym,address,big,cite,code,del,dfn,em,img,ins,kbd,q,s,samp,small,strike,strong,sub,sup,tt,var,b,u,i,center,dl,dt,dd,ol,ul,li,fieldset,form,label,legend,table,caption,tbody,tfoot,thead,tr,th,td,article,aside,canvas,details,embed,figure,figcaption,footer,header,hgroup,menu,nav,output,ruby,section,summary,time,mark,audio,video{border:0;font-size:100%;font:inherit;vertical-align:baseline;margin:0;padding:0}
article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}
body{line-height:1}
ol,ul{list-style:none}
blockquote,q{quotes:none}
blockquote:before,blockquote:after,q:before,q:after{content:none}
table{border-collapse:collapse;border-spacing:0}

/*===================================*/

@import url(//fonts.googleapis.com/css?family=Raleway:400,700);

body {font-family: 'Raleway', sans-serif;font-size:20px;color:#252525;font-weight:400;}
h1,h2,h3,h4,h5,h6 {margin:20px 0;line-height:1.50em;font-weight:700;}
h1 {font-size:54px;}
h2 {font-size:36px;}
h3 {font-size:24px;}
h4 {font-size:22px;}
h5 {font-size:18px;}
h5 {font-size:16px;}

p {margin:20px 0;line-height:1.25em;}
b,strong {font-weight:700;}
a {color:#003471;}
i,em {font-style:italic;}
label{display: inline-block;max-width: 100%;margin-bottom: 5px;font-weight: 700;font-size:14px;}
#contact-return>p{font-size:14px;margin:0;padding:15px;}
/*.form-group>input,.form-group>textarea{border-radius: 4px;}*/
.alignleft {margin-right:20px;}
.alignright {margin-left:20px;}
.aligncenter {display: block;margin-left: auto;margin-right: auto;}
.clear {clear:both;}
.text-white {color:#fff !important;}
.btn {font-weight:700;}
 
#header {padding:20px 0;width:100%;position:fixed;top:0;background:#fff;}
#banner {padding-top:118px;padding-bottom:340px;background:url({{asset('/img/banner-bg.jpg')}}) no-repeat   center center;background-size:cover;-moz-background-size:cover;-ms-background-size:cover;-o-background-size:cover;-webkit-background-size:cover;}
#banner .btn-info {background:none !important;border:2px solid #fff;font-size:36px;padding:20px 28px;border-radius:46px;-moz-border-radius:46px;-ms-border-radius:46px;-o-border-radius:46px;-webkit-border-radius:46px;}

#menu {text-align:right;}
#menu ul li a {color:#666666;font-size:18px;font-weight:700;display:inline-block;}
#menu ul li a.border-rounded {border:1px solid #666666;border-radius:20px;padding:10px 12px;}

#main-content {padding:80px 0;}
#footer {background:#3c3c3c;color:#959595;}
 
@media (min-width: 1200px) {
	.container {width:1000px;padding-left:0;padding-right:0;}
}
  </style>

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src= "{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
</head>

<body>
@include('frontend.includes.google_tag_manager')
	<div id="header"><!--header-->
    	<div class="container">
        	<div id="logo" class="col-md-3 col-xs-12"><img src="{{ asset('img/zenitra-logo.png') }}" alt="Zenintra" class="img-responsive" /></div>
            <div style="height:20px;" class="clear visible-xs"></div>
            <div id="menu" class="col-md-9 col-xs-12">
            	<ul>
                	<li><a href="#" class="border-rounded" data-toggle="modal" data-target="#contact-us-modal">{{landing_page_trans('landingpage.Contact Us',$lang)}}</a></li>
                </ul>
            </div>
            <div class="clear"></div>
            
        </div>
    </div><!--header-->
    <div id="content"><!--content-->
    	<div id="banner" class="container-fluid text-white"><!--banner-->
        	<div class="container">
            	<h1 align="center">{{landing_page_trans('landingpage.Productivity Boost for Startups',$lang)}}</h1>
                <p style="font-size:30px;" align="center">{{landing_page_trans('landingpage.Coming soon! We are working on it',$lang)}}</p>
                <p>&nbsp;</p>
                <p align="center"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#formPop">{{landing_page_trans('landingpage.Get notified when we open',$lang)}}</button></p>
            </div>
        </div><!--banner-->
        <div id="main-content"><!--main-content-->
        	<div class="container">
            	<h2>{{landing_page_trans('landingpage.Free intranet and internal communication for startups',$lang)}}</h2>
                <p>{{landing_page_trans('landingpage.main-content-p-text',$lang)}}</p>
            </div>
        </div><!--main-content-->
    </div><!--content-->
    <div id="footer"><!--footer-->
    	<div class="container">
        	<p align="center">&copy; {{landing_page_trans('landingpage.Copyright 2015 Zenintra.net',$lang)}}</p>
        </div>
    </div><!--footer-->
    
    
    
<div id="formPop" class="modal fade" role="dialog"><!-- Modal -->
    <div class="modal-dialog">
        <div class="modal-content"><!-- Modal content-->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">{{landing_page_trans('landingpage.Get notified when we open',$lang)}}</h4>
        </div>
        <div class="modal-body">
            <!-- Begin MailChimp Signup Form -->
            <link href="//cdn-images.mailchimp.com/embedcode/classic-081711.css" rel="stylesheet" type="text/css">
            <style type="text/css">
            #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
            /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
            We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
            </style>
            <div id="mc_embed_signup">
            <form action="//ilathys.us12.list-manage.com/subscribe/post?u=9590bc219f3343472332769c3&amp;id={{landing_page_trans('landingpage.mailchimplistid',$lang)}}" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
            <div id="mc_embed_signup_scroll">
            <div class="mc-field-group">
            <label for="mce-EMAIL">{{landing_page_trans('landingpage.Email Address',$lang)}} <span class="asterisk">*</span>
            </label>
            <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
            </div>
            <div id="mce-responses" class="clear">
            <div class="response" id="mce-error-response" style="display:none"></div>
            <div class="response" id="mce-success-response" style="display:none"></div>
            </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
            <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_9590bc219f3343472332769c3_3d0d8217b0" tabindex="-1" value=""></div>
            <div class="clear"><input type="submit" value="{{landing_page_trans('landingpage.Subscribe',$lang)}}" name="subscribe" id="mc-embedded-subscribe" class="btn button btn-default"></div>
            </div>
            </form>
            </div>
            <!--End mc_embed_signup-->
        </div>
        <div class="modal-footer">
        	<button type="button" class="btn btn-default" data-dismiss="modal">{{landing_page_trans('landingpage.Close',$lang)}}</button>
        </div>
        </div><!-- Modal content-->
    </div>
</div><!-- Modal -->
    
<!-- Modal -->
<div class="modal fade" id="contact-us-modal" tabindex="-1" role="dialog" aria-labelledby="contact-us-modal-label">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="contact-us-modal-label">{{landing_page_trans('landingpage.Contact Us',$lang)}}</h4>
      </div>
      <div class="modal-body">
        <form id="contact-us-form">
            <div class="form-group">
                <label for="contact_name">{{landing_page_trans('landingpage.Name',$lang)}}</label>
                <input type="text" class="form-control" id="contact_name" name="contact_name" placeholder="{{landing_page_trans('landingpage.John Smith',$lang)}}">
            </div>
            <div class="form-group">
                <label for="contact_email">{{landing_page_trans('landingpage.Email',$lang)}}</label>
                <input type="email" class="form-control" id="contact_email" name="contact_email" placeholder="{{landing_page_trans('landingpage.sample-email',$lang)}}">
            </div>
            <div class="form-group">
                <label for="contact_message">{{landing_page_trans('landingpage.Message',$lang)}}</label>
                <textarea class="form-control" id="contact_message" name="contact_message" rows="3" placeholder="{{landing_page_trans('landingpage.What can we do for you?',$lang)}}"></textarea>
            </div>
        </form>
        <div id="contact-return" class="bg-success" style="display:none;">
            <p id="contact-return-message"></p>
        </div>
      </div>
      <div class="modal-footer">
        <button for="contact-us-form" type="submit" class="btn btn-default" onclick="submit_contact_us()">{{landing_page_trans('landingpage.Submit',$lang)}}</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
var sent = false;
function submit_contact_us(){
    
    if(!sent){
        console.log("sending");
        var data = $("#contact-us-form").serializeArray();
        data.push({name:'lang',value:"{{$lang}}"});
        $.ajax({
            url:'{{route("contact_us")}}',
            method:"POST",
            data:data
        })
        .done(function(){
            sent = true;
            $("#contact-us-form").hide();
            $("#contact-return").show();
            $("#contact-return").addClass("bg-success");
            $("#contact-return").removeClass("bg-danger");
            $("#contact-return-message").text("{!! landing_page_trans('landingpage.Your message was successfully sent! We will get back to you soon\.',$lang) !!}");
        })
        .fail(function(err){
            var data = err.responseJSON;
            $("#contact-return").show();
            $("#contact-return").removeClass("bg-success");
            $("#contact-return").addClass("bg-danger");
            $("#contact-return-message").text(data.message);
        });
    }
}
</script>
</body>
</html>