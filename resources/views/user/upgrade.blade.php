@extends('layouts.master')

@section('content')
upgrade page

<p>You are on the 
@if (!$user->subscribed())
    free
@else
    {{ $user->stripe_plan }} {{ $user->onGracePeriod() ? "(On Grace Period)" : '' }}
@endif
plan
</p>

@if (!$user->subscribed() && !$user->everSubscribed())
<form action="" method="POST">
  Sign up for Starter:
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  {!! Form::hidden('plan', 'basic') !!}
  {!! Form::hidden('action', 'sign_up') !!}
  <script
    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
    data-key="pk_test_cqe0zWsm5Uq2dj8TD9IlPVHW"
    data-amount="4700"
    data-name="Veeroll"
    data-description="Starter Subscription ($47.00)"
    data-image="/128x128.png"
    data-locale="auto">
  </script>
</form>

<form action="" method="POST">
  Sign up for Pro:
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  {!! Form::hidden('plan', 'advanced') !!}
  {!! Form::hidden('action', 'sign_up') !!}
  <script
    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
    data-key="pk_test_cqe0zWsm5Uq2dj8TD9IlPVHW"
    data-amount="9700"
    data-name="Veeroll"
    data-description="Pro Subscription ($97.00)"
    data-image="/128x128.png"
    data-locale="auto">
  </script>
</form>

<form action="" method="POST">
  Sign up for Agency:
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  {!! Form::hidden('plan', 'agency') !!}
  {!! Form::hidden('action', 'sign_up') !!}
  <script
    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
    data-key="pk_test_cqe0zWsm5Uq2dj8TD9IlPVHW"
    data-amount="29700"
    data-name="Veeroll"
    data-description="Agency Subscription ($297.00)"
    data-image="/128x128.png"
    data-locale="auto">
  </script>
</form>
@else
    @if (!$user->onGracePeriod())
      {!! Form::open() !!}
      {!! Form::hidden('plan', 'cancel') !!}
      {!! Form::hidden('action', 'cancel') !!}
      {!! Form::submit('Cancel Subscription') !!}
      {!! Form::close() !!}
    @endif

    {!! Form::open() !!}
    {!! Form::hidden('plan', 'basic') !!}
    {!! Form::hidden('action', 'switch') !!}
    {!! Form::submit('Switch to Starter monthly plan') !!}
    {!! Form::close() !!}

    {!! Form::open() !!}
    {!! Form::hidden('plan', 'cancel') !!}
    {!! Form::hidden('action', 'switch') !!}
    {!! Form::submit('Switch to Pro monthly plan') !!}
    {!! Form::close() !!}

    {!! Form::open() !!}
    {!! Form::hidden('plan', 'cancel') !!}
    {!! Form::hidden('action', 'switch') !!}
    {!! Form::submit('Switch to Agency monthly plan') !!}
    {!! Form::close() !!}
@endif
{{--
{!! Form::open() !!}
{!! Form::hidden('plan', 'starter') !!}
{!! Form::submit('Upgrade to Starter') !!}
{!! Form::close() !!}

{!! Form::open() !!}
{!! Form::hidden('plan', 'advanced') !!}
{!! Form::submit('Upgrade to Advanced') !!}
{!! Form::close() !!}

{!! Form::open() !!}
{!! Form::hidden('plan', 'agency') !!}
{!! Form::submit('Upgrade to Agency') !!}
{!! Form::close() !!}
--}}

@stop

@section('scripts')

@stop