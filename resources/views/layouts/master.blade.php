
<!DOCTYPE html>
<html>
    <head>
        <title>@yield('title')</title>

        @yield('styles')

    </head>
    <body>
        @yield('sidebar')

        <div class="container">
            @yield('content')
        </div>
    </body>
    
    @yield('scripts')
    
</html>
