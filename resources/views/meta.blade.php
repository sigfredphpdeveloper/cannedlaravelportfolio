<meta charset="utf-8">
<meta name="viewport" content="width=device-width" />

<title>Zen intranet: free collaboration tools for smart businesses</title>

<meta name="title" content='All-in-one intranet'>
<meta name="description" content="{{page_finalhome_trans('finalhome.Free intranet and internal communication for startups and small businesses')}}">

<meta property="og:image" content="{{url('/')}}/img/logo.png" />
<meta property="og:description" content="Boost your Business, FREE collaboration tools for smart businesses" />
<meta property="og:url"content="{{url('/')}}" />
<meta property="og:title" content="Zen intranet: free collaboration tools for smart businesses" />

