@extends('frontend.layouts.master2')


@section('content')

<div id="" role="">

			<!-- MAIN CONTENT -->
			<div id="content" class="container">

				<div class="row form-row">
					<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 centered">
						<div class="well no-padding">
                            <form  id="smart-form-register" class="smart-form client-form zen-form" role="form" method="POST"
                             action="{{ url('/register') }}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">

							<?php if(isset($_GET['referrer']) AND $_GET['referrer'] != ''):?>
                            <input type="hidden" name="referrer" value="<?php echo $_GET['referrer'];?>" />
							<?php endif;?>

								<header>
									Registration <br />
                                    <span style="    margin: 5px 0;display: block;font-size: 14px;line-height: 15px;">
                                    Please use a business email matching your website domain name to create your company intranet.<br />
                                    Don’t have a business email or domain name yet? Click <a href="http://www.bluehost.com/track/s3seo" target="_blank"> here </a>
                                    </span>
								</header>

								<div class="col-md-12">
                                    @if(Session::has('warning_message'))
                                        <div class="alert alert-warning">
                                            <p>{!! Session::get('warning_message') !!}</p>
                                        </div>
                                    @endif
                                </div>

                                <div class="col-md-12">
                                    @if(Session::has('success'))
                                        <div class="alert alert-success">
                                            <p>{!! Session::get('success') !!}</p>
                                        </div>
                                    @endif
                                </div>

                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                                <fieldset>

                                    <section>
                                        <label class="input"> <i class="icon-append fa fa-lock"></i>
                                            <input type="email" name="email" placeholder="Business email *" id="password">
                                            <b class="tooltip tooltip-bottom-right">Used to log in</b> </label>
                                    </section>


                                    <section>
                                        <label class="input"> <i class="icon-append fa fa-lock"></i>
                                            <input type="password" name="password" placeholder="Password *" id="password">
                                            <b class="tooltip tooltip-bottom-right">Don't forget your password</b> </label>
                                    </section>

                                    <section>
                                        <label class="input"> <i class="icon-append fa fa-lock"></i>
                                            <input type="password" name="password_confirmation" placeholder="Confirm password *">
                                            <b class="tooltip tooltip-bottom-right">Don't forget your password</b> </label>
                                    </section>


                                </fieldset>

								<fieldset>


                                    <div class="row">
                                    <section class="col-xs-12 col-sm-3" style="padding-right:5px;">
                                        <label class="input">
                                            <select name="title" class="form-control" style="height: 30px; width: 82px;">
                                                    <option value="Mr">Mr.</option>
                                                    <option value="Mrs">Mrs.</option>
                                                    <option value="Ms">Ms.</option>
                                            </select>
                                        </label>
                                    </section>
                                        <section class="col-xs-12 col-sm-4" style="padding:0">
                                            <label class="input">
                                                <input type="text" name="first_name" placeholder="First name *">
                                            </label>
                                        </section>
                                        <section class="col-xs-12 col-sm-5" style="padding-left:5px">
                                            <label class="input">
                                                <input type="text" name="last_name" placeholder="Last name *">
                                            </label>
                                        </section>
                                    </div>

                                     <section>
                                        <label class="checkbox">
                                            <input type="checkbox" name="agree_terms"  id="terms" required>
                                            <i></i>I agree with the <a href="{{url('term-of-use')}}" target="_blank" >Terms and Conditions </a>
                                        </label>
                                    </section>

                                </fieldset>

								<footer>
									<button type="submit" class="btn btn-primary zen-signup-button">
										Sign Up
									</button>
								</footer>

								<div class="message">
									<p>
									<i class="fa fa-check"></i>
										Thank you for your registration!
									</p>
								</div>
							</form>

						</div>
						<p class="note text-center">Have a Zenintra account already? <a href="{{ url('login')}}">Login</a></p>
					</div>
				</div>
			</div>

		</div>

		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
							&times;
						</button>
						<h4 class="modal-title" id="myModalLabel">Terms & Conditions</h4>
					</div>
					<div class="modal-body custom-scroll terms-body">

 <div id="left">



            <h1>Zen Intra TERMS & CONDITIONS TEMPLATE</h1>



            <h2>Introduction</h2>



            </div>

			<br><br>




					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">
							Cancel
						</button>
						<button type="button" class="btn btn-primary" id="i-agree">
							<i class="fa fa-check"></i> I Agree
						</button>

						<button type="button" class="btn btn-danger pull-left" id="print">
							<i class="fa fa-print"></i> Print
						</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

    <style type="text/css">
        .alert-danger {
            font-size: 13px !important;
        }
    </style>
@endsection