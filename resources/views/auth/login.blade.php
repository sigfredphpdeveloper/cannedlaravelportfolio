@extends('frontend.layouts.master2')


@section('content')


<div id="" role="" >

    <!-- MAIN CONTENT -->
    <div id="content" class="container" style="min-height:500px;">

        <div class="row form-row">
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 centered">
                <div class="well no-padding">
                    <form class="smart-form client-form zen-form" id="login-form" role="form" method="POST" action="{{ url('/login') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <header>
                            Sign In
                        </header>

                        <div class="col-md-12">
                            @if(Session::has('warning_message'))
                                <div class="alert alert-warning">
                                    <p>{!! Session::get('warning_message') !!}</p>
                                </div>
                            @endif
                        </div>


                         @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if(Session::has('success'))
                        <div class="col-md-12">
                            <div class="alert alert-success">
                                <p>{{ Session::get('success') }}</p>
                            </div>
                        </div>
                        @endif

                        <fieldset>

                            <section>
                                <label class="label">E-mail</label>
                                <label class="input"> <i class="icon-append fa fa-user"></i>
                                    <input type="email" name="email" value="{{ old('$emails') }}">
                                    <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Please enter email address/username</b></label>
                            </section>


                            <section>
                                <label class="label">Password</label>
                                <label class="input"> <i class="icon-append fa fa-lock"></i>
                                    <input type="password" name="password">

                                    <b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> </label>
                                <div class="note">
                                    <a href='{{ url("/password/email") }}'>Forgot Your Password?</a>
                                </div>
                            </section>


                        </fieldset>
                        <footer>
                            <button type="submit" class="btn btn-primary zen-signup-button">
                                Sign in
                            </button>


                        </footer>
                    </form>

                </div>
                <p class="note text-center">Doesn't have a Zenintra account? <a href="{{ url('register')}}">Sign Up</a></p>
            </div>
        </div>
    </div>

</div>


@endsection