@extends('frontend.layouts.master2')


@section('content')

<div id="" role="">

			<!-- MAIN CONTENT -->
			<div id="content" class="container">

				<div class="row form-row">
					<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 centered">
						<div class="well no-padding">
                            <form  id="smart-form-register" class="smart-form client-form" role="form" method="POST" action="{{ url('/accept_invite_process') }}">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="hidden" name="user_id" value="{{ $invited_user->id }}" />

                                    <header>
                                        Accept Invitation From ({{ $invitor_user_company['company_name'] }}) *
                                    </header>

                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif

                                    <fieldset>
                                        <section>
                                            <label class="label">Your Email/Username</label>
                                            <label class="input">
                                                <input type="email" name="email" placeholder="Email *" id="password" class="form-control" value="{{ $invited_user->email }}" disabled/>
                                        </section>

                                        <section>
                                            <label class="label">Your Company</label>
                                            <label class="input">
                                                <input type="email" name="company_name" placeholder="" class="form-control" value="{{ $invitor_user_company['company_name'] }}" disabled />
                                        </section>

                                        <section>
                                            <label class="label">Set Your Password</label>
                                            <label class="input"> <i class="icon-append fa fa-lock"></i>
                                                <input type="password" name="password" placeholder="Password *" id="password">
                                                <b class="tooltip tooltip-bottom-right">Don't forget your password</b> </label>
                                        </section>

                                        <section>
                                            <label class="label">Confirm Password</label>
                                            <label class="input"> <i class="icon-append fa fa-lock"></i>
                                                <input type="password" name="password_confirmation" placeholder="Confirm password *">
                                                <b class="tooltip tooltip-bottom-right">Don't forget your password</b> </label>
                                        </section>


                                    </fieldset>

                                    <fieldset>


                                         <section>
                                            <label class="checkbox">
                                                <input type="checkbox"  id="terms">
                                                <i></i>I agree with the <a href="#" data-toggle="modal" data-target="#myModal">
                                                Terms and Conditions </a>
                                            </label>
                                        </section>

                                    </fieldset>


                                    <footer>
                                        <button type="submit" class="btn btn-primary">
                                            Register
                                        </button>
                                    </footer>

                                    <div class="message">
                                        <i class="fa fa-check"></i>
                                        <p>
                                            Thank you for your registration!
                                        </p>
                                    </div>
                                </form>

						</div>
					</div>
				</div>
			</div>

		</div>

	<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">Terms & Conditions</h4>
            </div>
            <div class="modal-body custom-scroll terms-body">

<div id="left">


    <h1>Zen Intra TERMS & CONDITIONS TEMPLATE</h1>

    <h2>Introduction</h2>

    </div>

    <br><br>




            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancel
                </button>
                <button type="button" class="btn btn-primary" id="i-agree">
                    <i class="fa fa-check"></i> I Agree
                </button>

                <button type="button" class="btn btn-danger pull-left" id="print">
                    <i class="fa fa-print"></i> Print
                </button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

    <style type="text/css">
        .alert-danger {
            font-size: 13px !important;
        }
    </style>
@endsection

