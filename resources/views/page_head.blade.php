<div class="mobile-menu-container hidden-md hidden-lg">
    <div id="mobile-menu">
        <ul>
            <li><a href="{{url('product')}}">Features</a></li>
            <li><a href="{{ url('/free-payroll-sg') }}" >Payroll</a></li>
            <li><a href="{{url('faq')}}">FAQ</a></li>
            <li><a href="{{url('pricing')}}">Pricing</a></li>
            <!--<li><a href="#" data-toggle="modal" data-target="#groove_support" >Support</a></li>-->
            <li><a href="http://support.zenintra.net">Support</a></li>
            <li><a href="{{url('login')}}">Login</a></li>
        </ul>
    </div>
</div>
<div id="header" class="container-fluid"><!--header-->
    <div class="container container1300">
        <div id="logo" class="col-md-4 col-xs-10">
            <a href="{{url('/')}}"><img src="{{url('/')}}/assets/public_assets/images/logo.png" class="img-responsive" alt="Zenintra" /></a>
        </div>
        <div id="menu" class="col-md-8 visible-md visible-lg">
            <ul>
                <li><a href="{{url('product')}}">Features</a></li>
                <li><a href="{{ url('/free-payroll-sg') }}" >Payroll</a></li>
                <li><a href="{{url('faq')}}">FAQ</a></li>
                <li><a href="{{url('pricing')}}">Pricing</a></li>
                <!--<li><a href="#" data-toggle="modal" data-target="#groove_support" >Support</a></li>
                <li><a href="http://support.zenintra.net" target="_blank">Support</a></li>-->
                <li><a href="{{url('login')}}">Login</a></li>
            </ul>
        </div>
        <div class="col-xs-2 hidden-md hidden-lg">
            <a class="mobile-menu-trigger"></a>
        </div>
    </div>
</div><!--header-->