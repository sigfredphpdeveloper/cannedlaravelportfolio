<!doctype html>
<?php
    function page_about_trans($key,$lang = null){
        if(!$lang){
            $lang = 'en';
        }
        return trans($key,[],null,$lang);
    }
?>
<?php
    function page_finalhome_trans($key,$lang = null){
        if(!$lang){
            $lang = 'en';
        }
        return trans($key,[],null,$lang);
    }
?>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width" />

  @include('meta')
  <meta name="description" content="{{page_about_trans('about.Free intranet and internal communication for startups and small businesses')}}">
<!-- #FAVICONS -->
    @include('frontend.includes.favicon')
  <!--[if lt IE 9]>
  <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/font-awesome.min.css') }}">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

  <link rel="stylesheet" href="{{ asset('css/custom.css') }}" type="text/css" rel="stylesheet" />

  <link rel="stylesheet" href="{{ asset('css/jquery.mmenu.all.css') }}">

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src= "{{ asset('js/jquery.mmenu.min.all.js') }}"></script>
  <script src= "{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
</head>

<body>
    @include('frontend.includes.google_tag_manager')
    <div class="mobile-menu-container hidden-lg">
		<div id="mobile-menu">
			<ul>
                <li><a href="{{ url('features') }}">{{page_about_trans('about.Features',$lang)}}</a></li>
                <li><a href="{{ url('pricing') }}">{{page_about_trans('about.Pricing',$lang)}}</a></li>
                <li><a href="">{{page_about_trans('about.Sign up',$lang)}}</a></li>
                <li><a href="">{{page_about_trans('about.Log in',$lang)}}</a></li>
            </ul>
		</div>
	</div>

    <div id="floating-header"><!--floating-header-->
    	<div class="container">
        	<div id="logo" class="col-md-3 col-sm-11 col-xs-11"><a href="{{ url('finalhome') }}"><img src="{{ asset('img/zenitra-logo.png') }}" alt="Zenintra" class="img-responsive" /></a></div>
            <div id="menu" class="col-md-9 visible-lg visible-md">
            	<ul>
                	<li><a href="{{page_about_trans('about.Blog-url',$lang)}}" target="_blank">{{page_about_trans('about.Blog',$lang)}}</a></li>
                    <li><a href="{{ url('pricing') }}">{{page_about_trans('about.Pricing',$lang)}}</a></li>
                    <li>
                    <form>
                    	<select class="lang-select">
                            <option value="en" <?php echo ( $lang == 'en') ? 'selected' : ''; ?> >EN</option>
                            <option value="fr" <?php echo ( $lang == 'fr') ? 'selected' : ''; ?> >FR</option>
                        </select>
                    </form>
                    </li>
                </ul>
            </div>
            <div class="col-sm-1 col-xs-1 hidden-lg hidden-md">
                <a class="mobile-menu-trigger" style="padding-top:0;font-size: 35px;"><span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span></a>
            </div>
            <div class="clear"></div>

        </div>
    </div><!--floating-header-->

<div id="header" class="container-fluid"><!--header-->
            	<div class="container container1300">
                    <div id="logo" class="col-md-4 col-xs-10">
                        <a href="{{url('/')}}"><img src="<?php echo url('/');?>/img/logo.png" class="img-responsive" alt="Zenintra" /></a>
                    </div>
                    <div id="menu" class="col-md-8 visible-md visible-lg">
                        <ul>
                            <li><a href="{{url('/product')}}">Features</a></li>
                            <li><a href="{{url('pricing')}}">Pricing</a></li>
                            <!--<li><a href="#" data-toggle="modal" data-target="#groove_support">Support</a></li>
                            <li><a href="http://support.zenintra.net" target="_blank">Support</a></li>-->
            <li><a href="{{ url('/free-payroll-sg') }}" >Payroll</a></li>
                        </ul>
                    </div>
                    <div class="col-xs-2 hidden-md hidden-lg">
                        <a class="mobile-menu-trigger" style="padding-top:0;font-size: 35px;"><span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span></a>
                    </div>
                </div>
            </div><!--header-->
    <div id="content"><!--content-->
        <div id="main-content"><!--main-content-->
        	<div class="container-fluid" style="background:#f4f4f4;padding:40px 0;">
            	<div class="container">
                	<h1 align="center" style="color:#555555;">{{page_about_trans('about.About Us',$lang)}}</h1>
                </div>
            </div>
           <div class="container">
                <div class="row padded">
                    <p>{{page_about_trans('about.paragraph-1',$lang)}}</p>
                    <p>{{page_about_trans('about.paragraph-2',$lang)}}</p>
                    <p>{{page_about_trans('about.paragraph-3',$lang)}}</p>
                    <p>{{page_about_trans('about.paragraph-4',$lang)}}</p>
                    <p>{{page_about_trans('about.paragraph-5',$lang)}}</p>

                    <div class="row padded">
                        <div class="col-md-4">
                            <img src="{{ asset('img/sophie.jpg') }}" class="head-img" alt="sophie" width="200" style="border-radius: 100%;" />
                        </div>
                        <div class="col-md-8">
                            <h2>Sophie Normand</h2>
                            <p>{{page_about_trans('about.sophie',$lang)}}</p>
                        </div>
                    </div>
                    <div class="row padded">
                        <div class="col-md-4">
                            <img src="{{ asset('img/jc.png') }}" class="head-img" alt="jc" width="200" style="border-radius: 100%;" />
                        </div>
                        <div class="col-md-8">
                            <h2>Jean-Christophe Bouglé</h2>
                             <p>{{page_about_trans('about.jc',$lang)}}</p>
                        </div>
                    </div>
                </div>
           </div>
        </div><!--main-content-->
    </div><!--content-->

   @include('page_footer')

<!-- Modal -->
<div class="modal fade" id="contact-us-modal" tabindex="-1" role="dialog" aria-labelledby="contact-us-modal-label">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="contact-us-modal-label">{{page_about_trans('about.Contact Us',$lang)}}</h4>
      </div>
      <div class="modal-body">
        <form id="contact-us-form">
            <div class="form-group">
                <label for="contact_name">{{page_about_trans('about.Name',$lang)}}</label>
                <input type="text" class="form-control" id="contact_name" name="contact_name" placeholder="{{page_about_trans('about.John Smith',$lang)}}">
            </div>
            <div class="form-group">
                <label for="contact_email">{{page_about_trans('about.Email',$lang)}}</label>
                <input type="email" class="form-control" id="contact_email" name="contact_email" placeholder="{{page_about_trans('about.sample-email',$lang)}}">
            </div>
            <div class="form-group">
                <label for="contact_message">{{page_about_trans('about.Message',$lang)}}</label>
                <textarea class="form-control" id="contact_message" name="contact_message" rows="3" placeholder="{{page_about_trans('about.What can we do for you?',$lang)}}"></textarea>
            </div>
        </form>
        <div id="contact-return" class="bg-success" style="display:none;">
            <p id="contact-return-message"></p>
        </div>
      </div>
      <div class="modal-footer">
        <button for="contact-us-form" type="submit" class="btn btn-default" onclick="submit_contact_us()">{{page_about_trans('about.Submit',$lang)}}</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
var sent = false;
function submit_contact_us(){

    if(!sent){
        console.log("sending");
        var data = $("#contact-us-form").serializeArray();
        data.push({name:'lang',value:"{{$lang}}"});
        $.ajax({
            url:'{{route("contact_us")}}',
            method:"POST",
            data:data
        })
        .done(function(){
            sent = true;
            $("#contact-us-form").hide();
            $("#contact-return").show();
            $("#contact-return").addClass("bg-success");
            $("#contact-return").removeClass("bg-danger");
            $("#contact-return-message").text("{!! page_about_trans('about.Your message was successfully sent! We will get back to you soon\.',$lang) !!}");
        })
        .fail(function(err){
            var data = err.responseJSON;
            $("#contact-return").show();
            $("#contact-return").removeClass("bg-success");
            $("#contact-return").addClass("bg-danger");
            $("#contact-return-message").text(data.message);
        });
    }
}
</script>

<script type="text/javascript">
	//Floating Menu
	$(document).scroll(function() {
	  var y = $(this).scrollTop();
	  if (y > 140) {
		$('#floating-header').fadeIn();
	  } else {
		$('#floating-header').fadeOut();
	  }
	});

	// MOBILE MENU
	$('.mobile-menu-container').mmenu({
		classes: 'mm-light',
		counters: true,
		offCanvas: {
			position  : 'left',
			zposition :	'front',
		}
	});

	$('a.mobile-menu-trigger').click(function() {
		$('.mobile-menu-container').trigger('open.mm');
	});

	$(window).resize(function() {
		$('.mobile-menu-container').trigger("close.mm");
	});

    $(document).ready(function($){
        $('.lang-select').on('change', function(){
            var lang =  $(this).val();
            if( lang == 'fr' )
            {
                window.location = '{{ url("fr/about-us") }}';
            }
            else
            {
                window.location = '{{ url("about-us") }}';
            }
        });
    });

</script>
</body>
</html>