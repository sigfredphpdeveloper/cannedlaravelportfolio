<!doctype html>
<?php
    function page_finalhome_trans($key,$lang = null){
        if(!$lang){
            $lang = 'en';
        }
        return trans($key,[],null,$lang);
    }
?>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width" />
    @include('meta')

  <meta name="description" content="{{page_finalhome_trans('finalhome.Free intranet and internal communication for startups and small businesses')}}">
<!-- #FAVICONS -->
    @include('frontend.includes.favicon')
  <!--[if lt IE 9]>
  <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/font-awesome.min.css') }}">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  {{--<link rel="stylesheet" href="//bxslider.com/lib/jquery.bxslider.css" type="text/css" />--}}

  <link rel="stylesheet" href="{{ asset('css/custom.css') }}" type="text/css" rel="stylesheet" />

  <link rel="stylesheet" href="{{ asset('css/jquery.mmenu.all.css') }}">



  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src= "{{ asset('js/jquery.mmenu.min.all.js') }}"></script>
  <script src= "{{ asset('js/jquery.bxslider.min.js') }}"></script>
  <script src= "{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/menu/script.js') }}"></script>

<style>
@media (max-width: 1260px) {
    .mobile-margin-bottom{
        margin-bottom:30px;
    }
}
.padding-bottom{
    padding-bottom:50px;
}
</style>
</head>

<body>
    @include('frontend.includes.google_tag_manager')

    @include('page_head')

  <style>
    #content ul li { background:none !important; }
  </style>

    <div id="content"><!--content-->
    	<div class="container-fluid custom-bg2" style="padding-top:163px;">
            <div class="container">
                <h2 align="center" style="font-size:48px;color:#fff;">Features</h2>
                <p>&nbsp;</p>
                <p align="center">
                <a href="javascript:void(0);" id="start_yt_modal">
                <img src="{{asset('img/tour-btn.png')}}" alt="Watch the tour" class="img-responsive"  />
                </a>
                </p>
            </div>
        </div>
        <div class="container-fluid padding-bottom" style="padding-top:50px;">
        	<div class="container container1300">
                <div class="col-md-6 mobile-margin-bottom"><img src="{{asset('img/files.png')}}" alt="" class="img-responsive" /></div>
                <div class="col-md-6">
                    <h2 style="margin-bottom:52px;"><img style="margin-top:-10px;" src="{{asset('img/product-icon1.png')}}" alt="" class="alignleft" /><strong>File Sharing</strong></h2>
                    <ul class="bullet">
                        <li>Upload and share any file</li>
                        <li>Customize access rights</li>
                        <li>3 GB Free (get more with our referral program)</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid padding-bottom" style="padding-top:50px;background:#f8f8f8;">
        	<div class="container container1300">
                <div class="col-md-6 pull-right mobile-margin-bottom"><img src="{{asset('img/directory.png')}}" alt="" class="img-responsive" /></div>
                <div class="col-md-6">
                    <h2 style="margin-bottom:52px;"><img style="margin-top:-10px;" src="{{asset('img/product-icon2.png')}}" alt="" class="alignleft" /><strong>Directory</strong></h2>
                    <ul class="bullet">
                        <li>List all your team members contact details</li>
                        <li>Emergency contact details with custom access rights</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid padding-bottom" style="padding-top:50px;">
        	<div class="container container1300">
                <div class="col-md-6 mobile-margin-bottom"><img src="{{asset('img/crm.png')}}" alt="" class="img-responsive" /></div>
                <div class="col-md-6">
                    <h2 style="margin-bottom:52px;"><img style="margin-top:-10px;" src="{{asset('img/product-icon3.png')}}" alt="" class="alignleft" /><strong>CRM</strong></h2>
                    <ul class="bullet">
                        <li>Manage your sales pipeline</li>
                        <li>Customize your own CRM</li>
                        <li>Share customers and prospects contact details</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid padding-bottom" style="padding-top:50px;background:#f8f8f8;">
        	<div class="container container1300">
                <div class="col-md-6 pull-right mobile-margin-bottom"><img src="{{asset('img/dashboard.png')}}" alt="" class="img-responsive" /></div>
                <div class="col-md-6">
                    <h2 style="margin-bottom:52px;"><img style="margin-top:-10px;" src="{{asset('img/product-icon4.png')}}" alt="" class="alignleft" /><strong>Announcements</strong></h2>
                    <ul class="bullet">
                        <li>Improve your internal communication </li>
                        <li>Share the latest news of your business with all employees or specific teams</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid padding-bottom" style="padding-top:50px;">
        	<div class="container container1300">
                <div class="col-md-6 mobile-margin-bottom"><img src="{{asset('img/trainings.png')}}" alt="" class="img-responsive" /></div>
                <div class="col-md-6">
                    <h2 style="margin-bottom:52px;"><img style="margin-top:-10px;" src="{{asset('img/product-icon5.png')}}" alt="" class="alignleft" /><strong>Knowledge Sharing</strong></h2>
                    <ul class="bullet">
                        <li>Improve knowledge sharing</li>
                        <li>Facilitate onboarding and training </li>
                        <li>Customize access rights</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid padding-bottom" style="padding-top:50px;background:#f8f8f8;">
        	<div class="container container1300">
                <div class="col-md-6 pull-right mobile-margin-bottom"><img src="{{asset('img/expenses.png')}}" alt="" class="img-responsive" /></div>
                <div class="col-md-6">
                    <h2 style="margin-bottom:52px;"><img style="margin-top:-10px;" src="{{asset('img/product-icon6.png')}}" alt="" class="alignleft" /><strong>Expenses Management</strong></h2>
                    <ul class="bullet">
                        <li>Expenses tracking made easy </li>
                        <li>Customize approval process</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid padding-bottom" style="padding-top:50px;">
        	<div class="container container1300">
                <div class="col-md-6 mobile-margin-bottom"><img src="{{asset('img/directory.png')}}" alt="" class="img-responsive" /></div>
                <div class="col-md-6">
                    <h2 style="margin-bottom:52px;"><img style="margin-top:-10px;" src="{{asset('img/product-icon9.png')}}" alt="" class="alignleft" /><strong>Granularity in access rights</strong></h2>
                    <ul class="bullet">
                        <li>Decide who can access what in your intranet</li>
                        <li>Access rights can be defined by role and by team</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid padding-bottom" style="padding-top:50px;padding-bottom:50px;background:#f8f8f8;">
        	<div class="container container1300">
                <div class="col-md-6 mobile-margin-bottom pull-right"><img src="{{asset('img/leave-features.png')}}" alt="" class="img-responsive center-block" /></div>
                <div class="col-md-6">
                    <h2 style="margin-bottom:52px;"><img style="margin-top:-10px;" src="{{asset('img/product-icon7.png')}}" alt="" class="alignleft" /><strong>Leave Management</strong></h2>
                    <ul class="bullet">
                        <li>Simple leave application process</li>
                        <li>Track outstanding leave days left</li>
                        <li>Customize types of leaves</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid padding-bottom" style="padding-top:50px;padding-bottom:50px;">
        	<div class="container container1300">
                <div class="col-md-6 mobile-margin-bottom"><img src="{{asset('img/coming-soon.png')}}" alt="" class="img-responsive center-block" /></div>
                <div class="col-md-6">
                    <h2 style="margin-bottom:52px;"><img style="margin-top:-10px;" src="{{asset('img/product-icon8.png')}}" alt="" class="alignleft" /><strong>Project Management</strong></h2>
                    <ul class="bullet">
                        <li>Easily manage simple or complex projects</li>
                        <li>Allocate tasks to team members</li>
                        <li>Multiple views (task, calendar, team member)</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid padding-bottom" style="padding-top:50px;padding-bottom:50px;background:#f8f8f8;">
        	<div class="container container1300">
                <div class="col-md-5 col-centered">
                    <h2 style="margin-bottom:52px;"><img style="margin-top:-10px;" src="{{asset('img/product-icon10.png')}}" alt="" class="alignleft" /><strong>The essentials</strong></h2>
                    <ul class="bullet">
                        <li>Your information is never shared</li>
                        <li>Single login for all features</li>
                        <li>Secured environment</li>
                        <li>Outstanding support</li>
                        <li>100% free (up to 3GB)</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container-fluid padding-bottom" style="padding-top:100px;padding-bottom:100px;">
        	<div class="col-md-12">
                <a class="btn-lg get-access" href="{{ url('/register') }}" style="background:none;border:0;display:block;margin:0 auto; outline: none;">
                    <img src="{{url('')}}/img/redtheme/access-btn.png" alt="Get early access" class="img-responsive center-block" />
                </a>
        	</div>
        </div>
    </div><!--content-->



    <div class="modal fade bs-example-modal-lg" id="yt_modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Watch the tour</h4>
                </div>
                <div class="modal-body" style="padding:0;">
                    <div id="popupVid">

                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('page_footer')



<div id="get_access_now_popup" class="modal fade" role="dialog"><!-- formPopModal -->
    <div class="modal-dialog">
        <div class="modal-content"><!-- Modal content-->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Get Access Now</h4>
        </div>



        <div class="modal-body">

        <form id="access_form">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
             <div class="form-group">
                <label for="exampleInputPassword1">Name</label>
                <input type="text" class="form-control" name="name" id="exampleInputPassword1" placeholder="Name">
              </div>

              <div class="form-group">
                <label for="exampleInputEmail1">Email address *</label>
                <input type="email" class="form-control" name="email" required id="exampleInputEmail1" placeholder="Email">
              </div>

              <button type="submit" class="btn btn-default">Submit</button>

              <div id="msg_holder" class="alert" style="display:none;">

              </div>

            </form>
        </div>




        </div><!-- Modal content-->
    </div>
</div><!-- formPopModal -->

<script type="text/javascript">
	//Floating Menu

	// MOBILE MENU
	$('.mobile-menu-container').mmenu({
		classes: 'mm-light',
		counters: true,
		offCanvas: {
			position  : 'left',
			zposition :	'front',
		}
	});

	$('a.mobile-menu-trigger').click(function() {
		$('.mobile-menu-container').trigger('open.mm');
	});

	$(window).resize(function() {
		$('.mobile-menu-container').trigger("close.mm");
	});

    $(document).ready(function($){

        $('#access_form').submit(function(e){
            e.preventDefault();
            var dt = $(this).serializeArray();

            $.ajax({
                url : '{{url('/get_access_now_subscribe')}}',
                type : 'POST',
                data:dt,
                success : function(code){

                    $('#msg_holder').fadeIn();

                    if(code=='success'){
                        $('#msg_holder').removeClass('alert-danger').addClass('alert-success').html('Successfully Subsribed!');
                    }else{
                        $('#msg_holder').removeClass('alert-success').addClass('alert-danger').html('Error Occured!');
                    }
                }
            });

        });


        $('#start_yt_modal').click(function(){

            var iframe = '<iframe width="100%" id="yt_frame" height="400px" src="https://www.youtube.com/embed/o-vcRz0RXL8?rel=0"></iframe>';

            $('#popupVid').html(iframe);
            $('#yt_modal').modal('show');
        });

        $('#yt_modal').on('hidden.bs.modal', function () {
            $('#popupVid').html('');
        });

    });

</script>
</body>
</html>
