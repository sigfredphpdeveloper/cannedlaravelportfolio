<!doctype html>
<?php 
    function page_price_trans($key,$lang = null){
        if(!$lang){
            $lang = 'en';
        }
        return trans($key,[],null,$lang);
    }
?>
<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Pricing | Zenintra</title>
  <meta name="description" content="{{page_price_trans('landingpage.Free intranet and internal communication for startups and small businesses')}}">
  <meta name="title" content='{{page_price_trans("landingpage.All-in-one intranet")}}'>
<!-- #FAVICONS -->
    @include('frontend.includes.favicon')
  <!--[if lt IE 9]>
  <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/font-awesome.min.css') }}">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  
  <link rel="stylesheet" href="{{ asset('css/custom.css') }}" type="text/css" rel="stylesheet" />
  
  <link rel="stylesheet" href="{{ asset('css/jquery.mmenu.all.css') }}">

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src= "{{ asset('js/jquery.mmenu.min.all.js') }}"></script>
  <script src= "{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
</head>

<body>
    @include('frontend.includes.google_tag_manager')
    <div class="mobile-menu-container hidden-lg">
		<div id="mobile-menu">
			<ul>
                <li><a href="{{ url('features') }}">{{page_price_trans('price.Features',$lang)}}</a></li>
                <li><a href="{{ url('price') }}">{{page_price_trans('price.Pricing',$lang)}}</a></li>
                <li><a href="">{{page_price_trans('price.Sign up',$lang)}}</a></li>
                <li><a href="">{{page_price_trans('price.Log in',$lang)}}</a></li>
            </ul>
		</div>
	</div>
    
    <div id="floating-header"><!--header-->
    	<div class="container">
        	<div id="logo" class="col-md-3 col-sm-11 col-xs-11"><a href="{{ url('finalhome') }}"><img src="{{ asset('img/zenitra-logo.png') }}" alt="Zenintra" class="img-responsive" /></a></div>
            <div id="menu" class="col-md-9 visible-lg visible-md">
            	<ul>
                	<li><a href="{{page_price_trans('price.Blog-url',$lang)}}" target="_blank">{{page_price_trans('price.Blog',$lang)}}</a></li>
                    <li><a href="{{ url('price') }}">{{page_price_trans('price.Pricing',$lang)}}</a></li>
                    <li>
                    <form>
                    	<select class="lang-select">
                            <option value="en" <?php echo ( $lang == 'en') ? 'selected' : ''; ?> >EN</option>
                            <option value="fr" <?php echo ( $lang == 'fr') ? 'selected' : ''; ?> >FR</option>
                        </select>
                    </form>
                    </li>
                </ul>
            </div>
            <div class="col-sm-1 col-xs-1 hidden-lg hidden-md">
                <a class="mobile-menu-trigger"></a>
            </div>
            <div class="clear"></div>
            
        </div>
    </div><!--header-->

	<div id="header" style="background:#ff8521;"><!--header-->
    	<div class="container">
        	<div id="logo" class="col-md-3 col-sm-11 col-xs-11"><a href="{{ url('finalhome') }}"><img src="{{ asset('img/zenitra-logo-white.png') }}" alt="Zenintra" class="img-responsive" /></a></div>
            <div id="menu" class="col-md-9 visible-lg visible-md">
            	<ul>
                	<li><a href="{{page_price_trans('price.Blog-url',$lang)}}" target="_blank">{{page_price_trans('price.Blog',$lang)}}</a></li>
                    <li><a href="{{ url('price') }}">{{page_price_trans('price.Pricing',$lang)}}</a></li>
                    <li>
                    <form>
                    	<select class="lang-select">
                            <option value="en" <?php echo ( $lang == 'en') ? 'selected' : ''; ?> >EN</option>
                            <option value="fr" <?php echo ( $lang == 'fr') ? 'selected' : ''; ?> >FR</option>
                        </select>
                    </form>
                    </li>
                </ul>
            </div>
            <div class="col-sm-1 col-xs-1 hidden-lg hidden-md">
                <a class="mobile-menu-trigger"></a>
            </div>
            <div class="clear"></div>
            
        </div>
    </div><!--header-->
    <div id="content"><!--content-->
        <div id="main-content"><!--main-content-->
        	<div class="container-fluid" style="background:#f4f4f4;padding:40px 0;">
            	<div class="container">
                	<h1 align="center" style="color:#555555;">{{page_price_trans('price.Pricing-title',$lang)}}</h1>
                </div>
            </div>
            <div class="container" style="padding:50px 0;">
            	<h2 align="center">Zenintra.net is <span style="color:#fc9934;font-weight:bold;">FREE!</span> <br/>
                <em>$ 0 / month / user</em></h2>
            </div>
            <div class="container" style="padding:40px 0;">
            	<h3><img class="alignleft" style="margin-top:-40px;" src="{{ asset('img/icon-storage1.png') }}" alt="" />{{page_price_trans('price.Storage Space',$lang)}}</h3>
                <div class="clear"></div>
                <ul style="padding-left:40px;padding-right:40px;">
                	<li>{{page_price_trans('price.Storage-Space1',$lang)}}</li>
                    <li>{{page_price_trans('price.Storage-Space2',$lang)}}</li>
                    <li>{{page_price_trans('price.Storage-Space3',$lang)}}</li>
                </ul>
            </div>
            <div class="container" style="padding:40px 0 120px 0;">
                <h3><img class="alignleft" style="margin-top:-60px;" src="{{ asset('img/icon-storage2.png') }}" alt="" />{{page_price_trans('price.What if you need more space',$lang)}}</h3>
                <div class="clear"></div>
                <div class="col-md-6">
                	<ul style="padding-left:40px;padding-right:40px;">
                        <li>{{page_price_trans('price.More-Space1',$lang)}}</li>
                        <li>{{page_price_trans('price.More-Space2',$lang)}}</li>
                	</ul>
                </div>
                <div class="col-md-6">
                	<ul style="padding-left:40px;padding-right:40px;">
                        <li>{{page_price_trans('price.More-Space3',$lang)}}</li>
                        <li>{{page_price_trans('price.More-Space4',$lang)}}</li>
                        <li>{{page_price_trans('price.More-Space5',$lang)}}</li>
                	</ul>
                </div>
            </div>
        </div><!--main-content-->
    </div><!--content-->
    <div id="footer"><!--footer-->
    	<div class="container">
        	<div class="col-md-4">
            	<ul>
                    <li><a href="{{ url('privacy-policy') }}">{{page_price_trans('price.Footer-Privacy-Policy',$lang)}}</a></li>
                    <li><a href="{{ url('term-of-use') }}">{{page_price_trans('price.Footer-Terms-of-use',$lang)}}</a></li>
                </ul>
            </div>
            <div class="col-md-3">
            	<ul>
                    <li><a href="{{ url('about-us') }}">{{page_price_trans('price.Footer-About-us',$lang)}}</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#contact-us-modal">{{page_price_trans('price.Footer-Contact-us',$lang)}}</a></li>
                </ul>
            </div>
            <div class="col-md-3">
            	<ul>
                    <li><a href="https://zenintra.zendesk.com/hc/en-us/requests/new" target="_blank">{{page_price_trans('price.Footer-Support',$lang)}}</a></li>
                    <li><a href="https://zenintra.zendesk.com/hc/en-us" target="_blank">{{page_price_trans('price.Footer-Faq',$lang)}}</a></li>
                </ul>
            </div>
            <div class="col-md-2">
            	<ul>
                    <li><a href="{{ url('about-us') }}">{{page_price_trans('price.Footer-Follow-us',$lang)}}</a></li>
                </ul>
                <ul class="list-inline">
                    <li>
                        <a href="{{ page_price_trans('price.Facebook-url',$lang) }}" target="_blank" class="btn-circle"><img src="{{ asset('img/icon-fb.jpg') }}" alt="" /></a>
                    </li>
                    <li>
                        <a href="{{ page_price_trans('price.Twitter-url',$lang) }}" target="_blank" class="btn-circle"><img src="{{ asset('img/icon-twitter.png') }}" alt="" /></a>
                    </li>
                     <li>
                        <a href="{{ page_price_trans('price.Youtube-url',$lang) }}" target="_blank" class="btn-circle"><img src="{{ asset('img/icon-yt.png') }}" alt="" /></a>
                    </li>
                    <li>
                        <a href="{{ page_price_trans('price.Linkedin-url',$lang) }}" target="_blank" class="btn-circle"><img src="{{ asset('img/icon-linked.png') }}" alt="" /></a>
                    </li>
                </ul>

            </div>
        	<div class="clear"></div>
            <p style="font-size:14px;">&copy; {{page_price_trans('price.Copyright',$lang)}}</p>
        </div>
    </div><!--footer-->
    
 <!-- Modal -->
<div class="modal fade" id="contact-us-modal" tabindex="-1" role="dialog" aria-labelledby="contact-us-modal-label">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="contact-us-modal-label">{{page_price_trans('price.Contact Us',$lang)}}</h4>
      </div>
      <div class="modal-body">
        <form id="contact-us-form">
            <div class="form-group">
                <label for="contact_name">{{page_price_trans('price.Name',$lang)}}</label>
                <input type="text" class="form-control" id="contact_name" name="contact_name" placeholder="{{page_price_trans('landingpage.John Smith',$lang)}}">
            </div>
            <div class="form-group">
                <label for="contact_email">{{page_price_trans('price.Email',$lang)}}</label>
                <input type="email" class="form-control" id="contact_email" name="contact_email" placeholder="{{page_price_trans('landingpage.sample-email',$lang)}}">
            </div>
            <div class="form-group">
                <label for="contact_message">{{page_price_trans('price.Message',$lang)}}</label>
                <textarea class="form-control" id="contact_message" name="contact_message" rows="3" placeholder="{{page_price_trans('landingpage.What can we do for you?',$lang)}}"></textarea>
            </div>
        </form>
        <div id="contact-return" class="bg-success" style="display:none;">
            <p id="contact-return-message"></p>
        </div>
      </div>
      <div class="modal-footer">
        <button for="contact-us-form" type="submit" class="btn btn-primary" onclick="submit_contact_us()">{{page_price_trans('landingpage.Submit',$lang)}}</button>
      </div>
    </div>
  </div>
</div>
    
<script type="text/javascript">
	//Floating Menu
	$(document).scroll(function() {
	  var y = $(this).scrollTop();
	  if (y > 140) {
		$('#floating-header').fadeIn();
	  } else {
		$('#floating-header').fadeOut();
	  }
	});
		
	// MOBILE MENU
	$('.mobile-menu-container').mmenu({
		classes: 'mm-light',
		counters: true,
		offCanvas: {
			position  : 'left',
			zposition :	'front',
		}
	});

	$('a.mobile-menu-trigger').click(function() {
		$('.mobile-menu-container').trigger('open.mm');
	});
	
	$(window).resize(function() {
		$('.mobile-menu-container').trigger("close.mm");
	});

    $(document).ready(function($){
        $('.lang-select').on('change', function(){
            var lang =  $(this).val();
            if( lang == 'fr' )
            {
                window.location = '{{ url("fr/price") }}';
            }
            else
            {
                window.location = '{{ url("price") }}';
            }
        });
    });

</script>
</body>
</html>