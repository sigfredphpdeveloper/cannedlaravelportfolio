<!doctype html>
<?php
    function page_finalhome_trans($key,$lang = null){
        if(!$lang){
            $lang = 'en';
        }
        return trans($key,[],null,$lang);
    }
?>
<html lang="en">
<head>

    @include('meta')
    @include('frontend.includes.favicon')

  <!--[if lt IE 9]>
  <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/font-awesome.min.css') }}">

  <link rel="stylesheet" href="//bxslider.com/lib/jquery.bxslider.css" type="text/css" />
  
  <link rel="stylesheet" href="{{ asset('css/custom.css') }}" type="text/css" rel="stylesheet" />
    <link href="{{url('/')}}/assets/public_assets/style.css" rel="stylesheet" type="text/css" />

  <link rel="stylesheet" href="{{ asset('css/jquery.mmenu.all.css') }}">

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src= "{{ asset('js/jquery.mmenu.min.all.js') }}"></script>
  <script src= "{{ asset('js/jquery.bxslider.min.js') }}"></script>
  <script src= "{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>

<style>
.bullet li{
    background:none !important;
}
</style>

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src= "{{ asset('js/menu/jquery.mmenu.min.all.js') }}"></script>
  <script src= "{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/menu/script.js') }}"></script>
</head>

<body>


    @include('frontend.includes.google_tag_manager')

    @include('page_head')

    <div id="content"><!--content-->
    	<div class="container-fluid custom-bg2" style="padding-top:130px;">
            <div class="container">
                <h2 align="center" style="font-size:48px;color:#fff;">FAQ</h2>
                <p align="center" class="pricing-head" style="font-size:72px;color:#fff;"></p>
            </div>
        </div>
        <div class="container-fluid padded" style="border-bottom:1px solid #ebebeb;">
            <div class="container container1300">
                <div class="col-md-12">
                    <div id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="zenHeading1">
                              <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#zenCollapse1" aria-expanded="true" aria-controls="zenCollapse1">
                                  What is Zenintranet?
                                </a>
                              </h4>
                            </div>
                            <div id="zenCollapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="zenHeading1">
                                <p>Zenintranet offers a series of tools for startups and SMEs to run their day to day operations from a single platform, with a single sign-in. At the moment, the tools include:</p>
                                <ul>
                                    <li>Employee directory </li>
                                    <li>CRM </li>
                                    <li>File sharing</li>
                                    <li>Claims </li>
                                    <li>Leaves Management </li> 
                                    <li>Trainings</li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="zenHeading2">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#zenCollapse2" aria-expanded="false" aria-controls="zenCollapse2">
                                        Will Zenintranet really free? Will you start charging me at a later date?
                                    </a>
                                </h4>
                            </div>
                            <div id="zenCollapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="zenHeading2">
                                <p>
                                    Yes Zenintranet as you see today is  and will continue to be a free platform. The number of users are unlimited as well. However, we do change a minimal amount for file sharing  storage space beyond 1 GB. You can find more details <a href="{{ url('pricing') }}">here</a>.      
                                </p>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="zenHeading3">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#zenCollapse3" aria-expanded="false" aria-controls="zenCollapse3">
                                    What if I want to use only selected modules at Zenintranet?
                                    </a>
                                </h4>
                            </div>
                            <div id="zenCollapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="zenHeading3">
                                <p>Yes, it is possible. You can activate and deactivate modules though  the admin settings.</p>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="zenHeading4">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#zenCollapse4" aria-expanded="false" aria-controls="zenCollapse4">
                                    How do I know that my data is private and secure with Zenintranet?
                                    </a>
                                </h4>
                            </div>
                            <div id="zenCollapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="zenHeading4">
                                <p>All the data you share in Zenintranet is 100% secure. Zenintranet uses some of the most advanced technology to ensure that your data is safe, secure, and available only to registered users in your organization.  When you access our site using a supported web browser, Secure Socket Layer (SSL) technology protects your information. When you login, you will see a small lock icon at the bottom of your browser display, indicating that a secure connection has been established to our server. Your personal Information will be stored under our Amazon cloud servers, and therefore benefit from Amazon protection against hackers as well.</p>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="zenHeading5">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#zenCollapse5" aria-expanded="false" aria-controls="zenCollapse5">
                                    Is there a mobile application for Zenintranet?
                                    </a>
                                </h4>
                            </div>
                            <div id="zenCollapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="zenHeading5">
                                <p>Not yet! For the moment, Zenintranet is desktop only. The mobile application version should be ready in the next 3 months. However, the present version of Zenintranet is 100% mobile and tablet responsive! You can therefore use  Zenintranet on your tablet and smartphone</p>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="zenHeading6">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#zenCollapse6" aria-expanded="false" aria-controls="zenCollapse6">
                                    Is Zenintranet a software?
                                    </a>
                                </h4>
                            </div>
                            <div id="zenCollapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="zenHeading6">
                                <p>No, Zenintranet is a cloud computing platform (Software as a Service). You don’t need to download or install any software to start using Zenintranet, just visit our website <a href="www.zenintra.net" target="_blank">www.zenintra.net</a> and get started  by creating your company account.</p>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="zenHeading7">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#zenCollapse7" aria-expanded="false" aria-controls="zenCollapse7">
                                    Will you allow me to export my data in case I decide to change platforms?
                                    </a>
                                </h4>
                            </div>
                            <div id="zenCollapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="zenHeading7">
                                <p>We would hate to see you go but if you do decide to leave, our team will ensure you have access to your data and help you export it as needed. </p>
                            </div>
                        </div>


                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="zenHeading8">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#zenCollapse8" aria-expanded="false" aria-controls="zenCollapse8">
                                        Is Zenintranet really free?
                                    </a>
                                </h4>
                            </div>
                            <div id="zenCollapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="zenHeading8">
                                <p>Yes, Zenintranet as you see today is and will continue to be a free platform. Zenintranet is a new approach to software for small businesses: we think that most software should be free and under one roof. You can use Zenintranet for free forever. We are on a mission to help small businesses and charge only for what really costs us (memory above your free allowance mainly). This is enough to keep us running Zenintranet. We may add some premium features in the future but the current list of available features will remain free. </p>
                            </div>
                        </div>


                    </div><!-- end accordion -->
                </div>
            </div>
        </div>
    </div><!--content-->
    @include('page_footer')

    @include('get_access_now')

</body>
<style type="text/css">
    .panel-collapse {
        padding: 15px;
        font-size: 16px;
    }
    .panel-collapse p {
        font-size: 16px;
    }
    .collapsed {
        display: block;
    }
</style>
</html>