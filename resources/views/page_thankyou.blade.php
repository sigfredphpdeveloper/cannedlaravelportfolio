<!doctype html>
<?php 
    function page_thankyou_trans($key,$lang = null){
        if(!$lang){
            $lang = 'en';
        }
        return trans($key,[],null,$lang);
    }
?>
<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Thank you! | Zenintra</title>
  <meta name="description" content="{{page_thankyou_trans('landingpage.Free intranet and internal communication for startups and small businesses')}}">
  <meta name="title" content='{{page_thankyou_trans("landingpage.All-in-one intranet")}}'>
<!-- #FAVICONS -->
    @include('frontend.includes.favicon')
  <!--[if lt IE 9]>
  <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/font-awesome.min.css') }}">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  
  <link rel="stylesheet" href="{{ URL::asset('css/custom.css') }}" type="text/css" rel="stylesheet" />
  
  <link rel="stylesheet" href="{{ URL::asset('css/jquery.mmenu.all.css') }}">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src= "{{ URL::asset('js/jquery.mmenu.min.all.js') }}"></script>
  <script src= "{{ URL::asset('js/bootstrap/bootstrap.min.js') }}"></script>
</head>

<body>
    
    <div class="mobile-menu-container hidden-lg">
		<div id="mobile-menu">
			<?php /*?><ul>
                <li><a href="/features">{{page_thankyou_trans('price.Features',$lang)}}</a></li>
                <li><a href="/price">{{page_thankyou_trans('price.Pricing',$lang)}}</a></li>
                <li><a href="">{{page_thankyou_trans('price.Sign up',$lang)}}</a></li>
                <li><a href="">{{page_thankyou_trans('price.Log in',$lang)}}</a></li>
            </ul><?php */?>
		</div>
	</div>
    
    <?php /*?><div id="floating-header"><!--header-->
    	<div class="container">
        	<div id="logo" class="col-md-3 col-sm-11 col-xs-11"><img src="{{ URL::asset('img/zenitra-logo.png') }}" alt="Zenintra" class="img-responsive" /></div>
            <div id="menu" class="col-md-9 visible-lg visible-md">
            	<ul>
                	<li><a href="/features">{{page_thankyou_trans('price.Features',$lang)}}</a></li>
                    <li><a href="/price">{{page_thankyou_trans('price.Pricing',$lang)}}</a></li>
                    <li><a href="" class="border-rounded">{{page_thankyou_trans('price.Sign up',$lang)}}</a></li>
                	<li><a href="" class="border-rounded">{{page_thankyou_trans('price.Log in',$lang)}}</a></li>
                </ul>
            </div>
            <div class="col-sm-1 col-xs-1 hidden-lg hidden-md">
                <a class="mobile-menu-trigger"></a>
            </div>
            <div class="clear"></div>
            
        </div>
    </div><!--header--><?php */?>

	<div id="header" style="background:#ffffff;"><!--header-->
    	<div class="container">
        	<div id="logo" class="col-md-3 col-sm-11 col-xs-11"><a href="/zenintra/public/finalhome"><img src="{{ URL::asset('img/zenitra-logo.png') }}" alt="Zenintra" class="img-responsive" /></a></div>
            <div id="menu" class="col-md-9 visible-lg visible-md">
            	<?php /*?><ul>
                	<li><a href="/features">{{page_thankyou_trans('price.Features',$lang)}}</a></li>
                    <li><a href="/price">{{page_thankyou_trans('price.Pricing',$lang)}}</a></li>
                    <li><a href="" class="border-rounded">{{page_thankyou_trans('price.Sign up',$lang)}}</a></li>
                	<li><a href="" class="border-rounded">{{page_thankyou_trans('price.Log in',$lang)}}</a></li>
                </ul><?php */?>
            </div>
            <div class="col-sm-1 col-xs-1 hidden-lg hidden-md">
                <a class="mobile-menu-trigger"></a>
            </div>
            <div class="clear"></div>
            
        </div>
    </div><!--header-->
    <div id="content"><!--content-->
        <div id="main-content"><!--main-content-->
        	<div class="container-fluid" style="background:#F2611F;padding:40px 0;">
            	<div class="container">
                	<h1 align="center" style="color:#fff;">{{page_thankyou_trans('thankyou.Thankyou-title',$lang)}}</h1>
                </div>
            </div>
            <div class="container" style="padding:80px 0;">
            	<div class="col-md-6">
                	<p>{{page_thankyou_trans('thankyou.Thankyou-paragraph1',$lang)}}</p>
                    <p>{{page_thankyou_trans('thankyou.Thankyou-paragraph2',$lang)}}</p>
                    <p><a href="{{ page_thankyou_trans('thankyou.Facebook-url',$lang) }}" target="_blank" class="btn-circle"><img src="{{ URL::asset('img/icon-fb2.png') }}" alt="" /></a> 
                    <a href="{{ page_thankyou_trans('thankyou.Twitter-url',$lang) }}" target="_blank" class="btn-circle"><img src="{{ URL::asset('img/icon-twitter2.png') }}" alt="" /></a> 
                    <a href="{{ page_thankyou_trans('thankyou.YouTube-url',$lang) }}" target="_blank" class="btn-circle"><img src="{{ URL::asset('img/icon-yt2.png') }}" alt="" /></a> 
                    <a href="{{ page_thankyou_trans('thankyou.Linkedin-url',$lang) }}" target="_blank" class="btn-circle"><img src="{{ URL::asset('img/icon-linked2.png') }}" alt="" /></a></p>
                </div>
                <div class="col-md-6">
                	<br />
                	<img class="center-block img-responsive" style="margin-top:-40px;" src="{{ URL::asset('img/thankyou-img.jpg') }}" alt="" />
                </div>
                <div class="clear"></div>
            </div>
        </div><!--main-content-->
    </div><!--content-->
    <div id="footer"><!--footer-->
    	<div class="container">
        	<div class="col-md-4">
            	<ul>
                    <li><a href="/zenintra/public/privacy-policy">{{page_thankyou_trans('thankyou.Footer-Privacy-Policy',$lang)}}</a></li>
                    <li><a href="/zenintra/public/term-of-use">{{page_thankyou_trans('thankyou.Footer-Terms-of-use',$lang)}}</a></li>
                </ul>
            </div>
            <div class="col-md-3">
            	<ul>
                    <li><a href="/zenintra/public/about-us">{{page_thankyou_trans('thankyou.Footer-About-us',$lang)}}</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#contact-us-modal">{{page_thankyou_trans('thankyou.Footer-Contact-us',$lang)}}</a></li>
                </ul>
            </div>
            <div class="col-md-3">
            	<ul>
                    <li><a href="https://zenintra.zendesk.com/hc/en-us/requests/new" target="_blank">{{page_thankyou_trans('thankyou.Footer-Support',$lang)}}</a></li>
                    <li><a href="https://zenintra.zendesk.com/hc/en-us" target="_blank">{{page_thankyou_trans('thankyou.Footer-Faq',$lang)}}</a></li>
                </ul>
            </div>
            <div class="col-md-2">
            	<ul>
                    <li>{{page_thankyou_trans('thankyou.Footer-Follow-us',$lang)}}</li>
                </ul>
                <ul class="list-inline">
                    <li>
                        <a href="{{ page_thankyou_trans('thankyou.Facebook-url',$lang) }}" target="_blank" class="btn-circle"><img src="{{ URL::asset('img/icon-fb.jpg') }}" alt="" /></a>
                    </li>
                    <li>
                        <a href="{{ page_thankyou_trans('thankyou.Twitter-url',$lang) }}" target="_blank" class="btn-circle"><img src="{{ URL::asset('img/icon-twitter.png') }}" alt="" /></a>
                    </li>
                     <li>
                        <a href="{{ page_thankyou_trans('thankyou.Youtube-url',$lang) }}" target="_blank" class="btn-circle"><img src="{{ URL::asset('img/icon-yt.png') }}" alt="" /></a>
                    </li>
                    <li>
                        <a href="{{ page_thankyou_trans('thankyou.Linkedin-url',$lang) }}" target="_blank" class="btn-circle"><img src="{{ URL::asset('img/icon-linked.png') }}" alt="" /></a>
                    </li>
                </ul>
                
            </div>
        	<div class="clear"></div>
            <p style="font-size:14px;">&copy; {{page_thankyou_trans('thankyou.Copyright',$lang)}}</p>
        </div>
    </div><!--footer-->
    
<!-- Modal -->
<div class="modal fade" id="contact-us-modal" tabindex="-1" role="dialog" aria-labelledby="contact-us-modal-label">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="contact-us-modal-label">{{page_thankyou_trans('thankyou.Contact Us',$lang)}}</h4>
      </div>
      <div class="modal-body">
        <form id="contact-us-form">
            <div class="form-group">
                <label for="contact_name">{{page_thankyou_trans('thankyou.Name',$lang)}}</label>
                <input type="text" class="form-control" id="contact_name" name="contact_name" placeholder="{{page_thankyou_trans('thankyou.John Smith',$lang)}}">
            </div>
            <div class="form-group">
                <label for="contact_email">{{page_thankyou_trans('thankyou.Email',$lang)}}</label>
                <input type="email" class="form-control" id="contact_email" name="contact_email" placeholder="{{page_thankyou_trans('thankyou.sample-email',$lang)}}">
            </div>
            <div class="form-group">
                <label for="contact_message">{{page_thankyou_trans('thankyou.Message',$lang)}}</label>
                <textarea class="form-control" id="contact_message" name="contact_message" rows="3" placeholder="{{page_thankyou_trans('thankyou.What can we do for you?',$lang)}}"></textarea>
            </div>
        </form>
        <div id="contact-return" class="bg-success" style="display:none;">
            <p id="contact-return-message"></p>
        </div>
      </div>
      <div class="modal-footer">
        <button for="contact-us-form" type="submit" class="btn btn-primary" onclick="submit_contact_us()">{{page_thankyou_trans('thankyou.Submit',$lang)}}</button>
      </div>
    </div>
  </div>
</div>
    
<script type="text/javascript">
	//Floating Menu
	$(document).scroll(function() {
	  var y = $(this).scrollTop();
	  if (y > 140) {
		$('#floating-header').fadeIn();
	  } else {
		$('#floating-header').fadeOut();
	  }
	});
		
	// MOBILE MENU
	$('.mobile-menu-container').mmenu({
		classes: 'mm-light',
		counters: true,
		offCanvas: {
			position  : 'left',
			zposition :	'front',
		}
	});

	$('a.mobile-menu-trigger').click(function() {
		$('.mobile-menu-container').trigger('open.mm');
	});
	
	$(window).resize(function() {
		$('.mobile-menu-container').trigger("close.mm");
	});

</script>
</body>
</html>
