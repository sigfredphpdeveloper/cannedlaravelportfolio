
<nav>
    <ul>
        <li class="active">
            <a href="index.html" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Dashboard</span></a>
        </li>

        <li>
            <a href="#"><i class="fa fa-lg fa-fw fa-bar-chart-o"></i> <span class="menu-item-parent">Users</span></a>
            <ul>
                <li>
                    <a href="flot.html">Manage</a>
                </li>
                <li>
                    <a href="morris.html">Morris Charts</a>
                </li>
                <li>
                    <a href="inline-charts.html">Inline Charts</a>
                </li>
                <li>
                    <a href="dygraphs.html">Dygraphs</a>
                </li>
                <li>
                    <a href="chartjs.html">Chart.js <span class="badge pull-right inbox-badge bg-color-yellow">new</span></a>
                </li>
            </ul>
        </li>
        <li>
            <a href="#"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">Company</span></a>
            <ul>
                <li>
                    <a href="table.html">Normal Tables</a>
                </li>
                <li>
                    <a href="datatables.html">Data Tables <span class="badge inbox-badge bg-color-greenLight">v1.10</span></a>
                </li>
                <li>
                    <a href="jqgrid.html">Jquery Grid</a>
                </li>
            </ul>
        </li>


    </ul>
</nav>
