    <div id="footer"><!--footer-->
    	<div class="container">
        	<div class="col-md-4">
            	<ul>
                    <li><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
                    <li><a href="{{ url('term-of-use') }}">Terms of use</a></li>
                </ul>
            </div>
            <div class="col-md-3">
            	<ul>
                    <li><a href="{{ url('about-us') }}">About us</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#contact-us-modal">Contact us</a></li>
                </ul>
            </div>
            <div class="col-md-3">
            	<ul>
                    <li><a href="https://zenintra.zendesk.com/hc/en-us/requests/new" target="_blank">Support</a></li>
                    <li><a href="https://zenintra.zendesk.com/hc/en-us" target="_blank">FAQ</a></li>
                </ul>
            </div>
            <div class="col-md-2">
            	<ul>
                    <li>Follow us on</li>
                </ul>
                <ul class="list-inline">
                    <li>
                        <a href="https://www.facebook.com/zenintra" target="_blank"><img src="{{ asset('img/icon-fb.jpg') }}" alt="" /></a>
                    </li>
                    <li>
                        <a href="https://twitter.com/zenintra" target="_blank" ><img src="{{ asset('img/icon-twitter.png') }}" alt="" /></a>
                    </li>
                     <li>
                        <a href="http://www.youtube.com/c/ZenintraNetOfficial" target="_blank" ><img src="{{ asset('img/icon-yt.png') }}" alt="" /></a>
                    </li>
                    <li>
                        <a href="https://www.linkedin.com/company/zenintra-net?trk=company_logo" target="_blank" ><img src="{{ asset('img/icon-linked.png') }}" alt="" /></a>
                    </li>
                </ul>

            </div>
        	<div class="clear"></div>
            <p style="font-size:14px;">&copy; Copyright 2016 Zenintra.net</p>
        </div>
    </div><!--footer-->