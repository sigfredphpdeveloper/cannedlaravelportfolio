<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">

		<title>Zenintra.net</title>

		<!-- #Bootstrap Core CSS -->
		<link type="text/css" href="{{ asset('frontend/css/bootstrap.min.css') }}" rel="stylesheet">

		<!-- #SLIDER REVOLUTION 4.x CSS SETTINGS -->
		<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/extralayers.css') }}" media="screen">
		<link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/settings.css') }}" media="screen">


        <!-- SmartAdmin Styles : Caution! DO NOT change the order -->
        <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/smartadmin-production-plugins.min.css') }}">
        <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/smartadmin-production.min.css') }}">
        <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/smartadmin-skins.min.css') }}">

        <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/app.css') }}">
		<!-- #RELATED CSS -->
		<link type="text/css" href="{{ asset('frontend/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
		<link type="text/css" href="{{ asset('frontend/css/nivo-lightbox.css') }}" rel="stylesheet">
		<link type="text/css" href="{{ asset('frontend/css/nivo-lightbox-theme/default/default.css') }}" rel="stylesheet">
		<link type="text/css" href="{{ asset('frontend/css/animate.css') }}" rel="stylesheet">

		<!-- #SMARTADMIN LANDING CSS -->
		<link type="text/css" href="{{ asset('frontend/css/main.css') }}" rel="stylesheet">
		<link type="text/css" href="{{ asset('frontend/color/default.css') }}" rel="stylesheet">

		<!-- CUSTOM ZEN CSS -->
		<link type="text/css" href="{{ asset('frontend/css/frontend-custom.css') }}" rel="stylesheet">

		<!-- #FAVICONS -->
		@include('frontend.includes.favicon')

		<!-- #GOOGLE FONT -->
		<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">

	</head>

	<!--

	TABLE OF CONTENTS.
	
	Use search to find needed section.
	
	===================================================================
	
	|  01. #MENU                                                      |
	|  02. #INTRO                                                     |
	|  03. #PRICING                                                   |
	|  04. #TEAM                                                      |
	|  05. #FEATURES                                                  |
	|  06. #SCREENSHOT                                                |
	|  07. #UPDATES                                                   |
	|  08. #QUOTES                                                    |
	|  09. #CONTACT                                                   |
	|  10. #BOTTOM CONTENT                                            |
	|  11. #FOOTER                                                    |
	|  12. #Core Javascript                                           |
	|  13. #REVOLUITION SLIDER                                        |
	|  14. #PAGE SCRIPT                                               |
	
	===================================================================
	
	-->

	<body data-spy="scroll">

		@include('frontend.layouts.header')

        @yield('content')

		<!-- #FOOTER -->
		@include('frontend.layouts.footer2')



		<!-- / #FOOTER -->

		<!-- # Core JavaScript Files -->

		<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="{{ asset('frontend/js/libs/jquery-2.0.2.min.js') }}"><\/script>');
			}
		</script>

		<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="{{ asset('frontend/js/libs/jquery-ui-1.10.3.min.js') }}"><\/script>');
			}
		</script>


		<!-- BOOTSTRAP JS -->
		<script src="{{ asset('frontend/js/bootstrap/bootstrap.min.js') }}"></script>

		<!--# JS PLUGINS -->
		<script src="{{ asset('frontend/js/plugins/classie.js') }}"></script>
		<script src="{{ asset('frontend/js/plugins/gnmenu.js') }}"></script>
		<script src="{{ asset('frontend/js/plugins/jquery.scrollUp.js') }}"></script>
		<script src="{{ asset('frontend/js/plugins/nivo-lightbox.min.js') }}"></script>
		<script src="{{ asset('frontend/js/plugins/smoothscroll.js') }}"></script>
		<script src="{{ asset('frontend/js/plugins/jquery.themepunch.plugins.min.js') }}"></script>
		<script src="{{ asset('frontend/js/plugins/jquery.themepunch.revolution.min.js') }}"></script>

		<!-- # Custom Theme JavaScript -->
		<script src="{{ asset('frontend/js/custom.js') }}"></script>

		<!-- #PAGE SCRIPT -->
		<script type="text/javascript">
			jQuery(document).ready(function() {

				jQuery('.tp-banner').show().revolution({
					dottedOverlay : "none",
					delay : 8000,
					startwidth : 1170,
					startheight : 700,
					hideThumbs : 200,

					thumbWidth : 100,
					thumbHeight : 50,
					thumbAmount : 5,

					navigationType : "bullet",
					navigationArrows : "solo",
					navigationStyle : "preview4",

					touchenabled : "on",
					onHoverStop : "off",

					swipe_velocity : 0.7,
					swipe_min_touches : 1,
					swipe_max_touches : 1,
					drag_block_vertical : false,

					parallax : "mouse",
					parallaxBgFreeze : "on",
					parallaxLevels : [7, 4, 3, 2, 5, 4, 3, 2, 1, 0],

					keyboardNavigation : "off",

					navigationHAlign : "center",
					navigationVAlign : "bottom",
					navigationHOffset : 0,
					navigationVOffset : 20,

					soloArrowLeftHalign : "left",
					soloArrowLeftValign : "center",
					soloArrowLeftHOffset : 20,
					soloArrowLeftVOffset : 0,

					soloArrowRightHalign : "right",
					soloArrowRightValign : "center",
					soloArrowRightHOffset : 20,
					soloArrowRightVOffset : 0,

					shadow : 0,
					fullWidth : "off",
					fullScreen : "on",

					spinner : "spinner4",

					stopLoop : "off",
					stopAfterLoops : -1,
					stopAtSlide : -1,

					shuffle : "off",

					autoHeight : "off",
					forceFullWidth : "off",

					hideThumbsOnMobile : "off",
					hideNavDelayOnMobile : 1500,
					hideBulletsOnMobile : "off",
					hideArrowsOnMobile : "off",
					hideThumbsUnderResolution : 0,

					hideSliderAtLimit : 0,
					hideCaptionAtLimit : 0,
					hideAllCaptionAtLilmit : 0,
					startWithSlide : 0,
					fullScreenOffsetContainer : ""
				});

			});
			//ready

			$.scrollUp();
		</script>

	</body>

</html>
