<div id="header-signup"><!--header-->
        <div class="container">
            <div id="singup-logo" class="col-md-3 col-xs-12"><img src="{{ asset('img/zenitra-logo-white.png') }}" alt="Zenintra" class="img-responsive" /></div>
            <div style="height:20px;" class="clear visible-xs"></div>
            <div id="menu" class="col-md-9 col-xs-12">
                <ul>
                    <li><a href="{{ url('register')}}" class="border-rounded">Sign Up</a></li>
                    <li><a href="{{ url('login')}}" class="border-rounded">Login</a></li>
                </ul>
            </div>
            <div class="clear"></div>
            
        </div>
    </div><!--header-->