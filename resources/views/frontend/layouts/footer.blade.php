 <div id="footer"><!--footer-->
    	<div class="container">
        	<div class="">
            	<ul>
                    <li><a href="{{ url('privacy-policy') }}">Privacy Policy</a></li>
                    <li><a href="{{ url('term-of-use') }}">Terms of Use</a></li>
                    <li><a href="{{ url('about-us') }}">About Us</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#contact-us-modal">Contact Us</a></li>
                </ul>
            </div>
        	<div class="clear"></div>
            <p style="font-size:14px;">&copy; Copyright 2016 Zenintra.net</p>
        </div>
    </div><!--footer-->