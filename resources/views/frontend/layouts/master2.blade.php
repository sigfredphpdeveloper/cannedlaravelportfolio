<!doctype html>
<?php
    function page_finalhome_trans($key,$lang = null){
        if(!$lang){
            $lang = 'en';
        }
        return trans($key,[],null,$lang);
    }
?>
<html lang="en">
<head>

    @include('meta')
    @include('frontend.includes.favicon')
  <!--[if lt IE 9]>
  <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('/css/font-awesome.min.css') }}">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="//bxslider.com/lib/jquery.bxslider.css" type="text/css" />

  <link rel="stylesheet" href="{{ asset('css/custom.css') }}" type="text/css" rel="stylesheet" />

  <link rel="stylesheet" href="{{ asset('css/jquery.mmenu.all.css') }}">

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src= "{{ asset('js/jquery.mmenu.min.all.js') }}"></script>
  <script src= "{{ asset('js/jquery.bxslider.min.js') }}"></script>
  <script src= "{{ asset('js/bootstrap/bootstrap.min.js') }}"></script>
</head>

<body>

<style>
#menu ul li a{
    color:#000 !important;
    font-weight: normal !important;
}
</style>
    @include('frontend.includes.google_tag_manager')


    @include('page_head')


    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('css/app.css') }}">

    <link type="text/css" href="{{ asset('frontend/css/frontend-custom.css') }}" rel="stylesheet">
    <link type="text/css" href="{{ asset('frontend/css/custom-new-template.css') }}" rel="stylesheet">
<style>
#footer{
    border-top:0 !important;
}
</style>



    @yield('content')

   @include('page_footer2')


<script type="text/javascript">
	//Floating Menu
	$(document).scroll(function() {
	  var y = $(this).scrollTop();
	  if (y > 140) {
		$('#floating-header').fadeIn();
	  } else {
		$('#floating-header').fadeOut();
	  }
	});

	$('.testi-slide').bxSlider({
		mode: 'horizontal',
		auto: true,
		pager: false,
		controls: true,
		pause: 7000,
		speed: 1000
	});

	// MOBILE MENU
	$('.mobile-menu-container').mmenu({
		classes: 'mm-light',
		counters: true,
		offCanvas: {
			position  : 'left',
			zposition :	'front',
		}
	});

	$('a.mobile-menu-trigger').click(function() {
		$('.mobile-menu-container').trigger('open.mm');
	});

	$(window).resize(function() {
		$('.mobile-menu-container').trigger("close.mm");
	});

    $(document).ready(function($){
        $('.lang-select').on('change', function(){
            var lang =  $(this).val();
            if( lang == 'fr' )
            {
                window.location = '{{ url("fr/finalhome") }}';
            }
            else
            {
                window.location = '{{ url("finalhome") }}';
            }
        });

        $('#access_form').submit(function(e){
            e.preventDefault();
            var dt = $(this).serializeArray();

            $.ajax({
                url : '{{url('/get_access_now_subscribe')}}',
                type : 'POST',
                data:dt,
                success : function(code){

                    $('#msg_holder').fadeIn();

                    if(code=='success'){
                        $('#msg_holder').removeClass('alert-danger').addClass('alert-success').html('Successfully Subsribed!');
                    }else{
                        $('#msg_holder').removeClass('alert-success').addClass('alert-danger').html('Error Occured!');
                    }
                }
            });

        });
    });

    // $(window).scroll(function(){
    //     var dh = $(document).height();
    //     var sh = $(this).scrollTop();

    //     if(sh>20){
    //         $('#header').attr('style', 'background: #F2611F !important');
    //         // $('#logo a img').attr('src','{{url('img/zenitra-logo-white.png')}}');
    //         $('#menu ul li a').css('color','#fff');
    //     }else{
    //         $('#header').attr('style', 'background: #fff !important');
    //         $('#logo a img').attr('src','{{url('img/logo.png')}}');
    //         $('#menu ul li a').css('color','#000');
    //     }
    // });

</script>
<style type="text/css">
    .modal-backdrop {
        position: fixed !important;
    }
    .modal {
        z-index: 9999 !important;
    }
</style>
</body>
</html>
