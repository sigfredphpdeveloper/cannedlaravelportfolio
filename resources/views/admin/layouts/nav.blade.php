
<nav>

    <ul>
        <li >
            <a href="{{ url('/dashboard') }}" title="Dashboard">
            <i class="fa fa-lg fa-fw fa-dashboard"></i> <span class="menu-item-parent">Dashboard</span></a>
        </li>

        <li>
            <a href="#"><i class="fa fa-lg fa-fw fa-user"></i> <span class="menu-item-parent">Users</span></a>
            <ul>
                <li>
                    <a href="{{ url('/my_profile') }}">Manage Users</a>
                </li>
            </ul>
        </li>


    </ul>
</nav>
