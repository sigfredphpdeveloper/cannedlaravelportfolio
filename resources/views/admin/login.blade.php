@extends('admin.layouts.master')


@section('content')

@include('admin.layouts.header')

<div id="main" role="main">

    <!-- MAIN CONTENT -->
    <div id="content" class="container">

        <div class="row">
            <div class="text-center col-md-6 col-md-offset-3">
                <h1 class="txt-color-red login-header-big">ZenIntra</h1>

            </div>

            <div class="text-center col-md-6 col-md-offset-3">
                <div class="well no-padding">

                    <form class="smart-form client-form" id="login-form" role="form" method="POST" action="{{ url('/admin_login') }}">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        <header>
                            Administrator Login
                        </header>


                        @if(Session::has('error'))
                        <div class="row">
                            <div class="col-md-12 text-center ">
                                <div class="alert alert-danger">
                                    <p>{{ Session::get('error') }}</p>
                                </div>
                            </div>
                        </div>
                        @endif


                        @if(Session::has('success'))
                        <div class="col-md-10">
                            <div class="alert alert-success">
                                <p>{{ Session::get('success') }}</p>
                            </div>
                        </div>
                        @endif

                        <fieldset>
                            <section>
                                <label class="label">Username</label>
                                <label class="input"> <i class="icon-append fa fa-user"></i>
                                    <input type="text" name="username" value="" required>
                                    <b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Please enter email address/username</b></label>
                            </section>


                            <section>
                                <label class="label">Password</label>
                                <label class="input"> <i class="icon-append fa fa-lock"></i>
                                    <input type="password" name="password">
                                    <b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> </label>

                            </section>


                        </fieldset>
                        <footer>
                            <button type="submit" class="btn btn-primary">
                                Sign in
                            </button>


                        </footer>
                    </form>

                </div>


            </div>
        </div>
    </div>

</div>


@endsection