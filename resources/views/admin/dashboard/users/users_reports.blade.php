@extends('admin.dashboard.layouts.master')

@section('content')

<div class="row">
	<div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
		<h1 class="page-title txt-color-blueDark">
			<i class="fa fa-bar-chart-o fa-fw "></i> 
				{{trans("admin_reports.Reports")}}
		</h1>
	</div>
</div>

<div class="row">

    @include('admin.dashboard.users.users_graph')
    @include('admin.dashboard.users.users_graph_cumulative')


</div>

<!-- Morris Chart Dependencies -->
<script src="{{ asset('/js/plugin/morris/raphael.min.js') }}"></script>
<script src="{{ asset('/js/plugin/morris/morris.min.js') }}"></script>
<style type="text/css">
	#ui-datepicker-div {
	    z-index: 1100 !important;
	}
</style>
<script type="text/javascript">
	$(document).ready(function(){
		
		ajax_report(30);

		$('.filter-graph2').on('change', function(){
			days = $(this).val();
			if( days != 'custom_range' ){
				$('#range-form').hide();
				ajax_report(days);
			} else {
				$('#range-form').show();
			}
			
		});

		$('#range-form').on('submit', function(e){
			e.preventDefault();
			days = $('.filter-graph').val();
			ajax_report(days);
		});

		$('#from').datepicker();
		$('#to').datepicker({ 
			maxDate: new Date
		});

		function ajax_report( days ) {
			var from = $('#from').val();
			var to = $('#to').val();

			if ($('#users-report').length) {
				$.ajax({
					type: 'GET',
					dataType: 'json',
					url: '{{ url('users-graphs') }}',
					data: { days: days, from: from, to: to }
				})
				.done(function( result ) {
					$('.morris-hover').remove();
				    var chart = Morris.Area({
					    element: 'users-report',
					    data: result, // Set initial data (ideally you would provide an array of default data)
					    xkey: 'date', // Set the key for X-axis
					    ykeys: ['value'], // Set the key for Y-axis
					    labels: ['Total'],
					    resize: true
					});
			    })
			    .fail(function() {
			      // alert( "error occured" );
			    });
			}
		}


		ajax_report4(30);

        $('.filter-graph4').on('change', function(){
            days = $(this).val();
            if( days != 'custom_range' ){
                $('#range-form').hide();
                ajax_report4(days);
            } else {
                $('#range-form').show();
            }

        });


        function ajax_report4( days ) {
            var from = $('#from').val();
            var to = $('#to').val();

            if ($('#users-report-cumulative').length) {
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: '{{ url('users-report-cumulative') }}',
                    data: { days: days, from: from, to: to }
                })
                .done(function( result ) {
                  //  $('.morris-hover').remove();
                    var chart = Morris.Area({
                        element: 'users-report-cumulative',
                        data: result, // Set initial data (ideally you would provide an array of default data)
                        xkey: 'date', // Set the key for X-axis
                        ykeys: ['value'], // Set the key for Y-axis
                        labels: ['Total'],
                        resize: true
                    });
                })
                .fail(function() {
                  // alert( "error occured" );
                });
            }
        }

				
	});
</script>

@endsection