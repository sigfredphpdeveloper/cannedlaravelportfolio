@extends('admin.dashboard.layouts.master')

@section('content')

<link rel="stylesheet" href="{{ asset('css/blue/style.css') }}" type="text/css" id="" media="print, projection, screen" />
<style>
table.tablesorter thead tr th, table.tablesorter tfoot tr th {
    background-color: #F7DCB4 !important;
    font-size: 12pt;
}
</style>
{{--<script src="{{ asset('js/jquery-latest.js') }}"></script>--}}
<script src="{{ asset('js/jquery.metadata.js') }}"></script>
<script src="{{ asset('js/jquery.tablesorter.min.js') }}"></script>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-md-12">
            @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
            @endif
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Warning.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

<!--

        <div class="col-md-6 col-xs-6 col-xs-offset-3">
            <button type="button" class="pull-right btn btn-primary" data-toggle="modal" data-target="#user_modal" style="margin-left:10px;">Add New</button>
            <div class="input-group pull-right">
                <form class="form-inline" method="GET">
                    <div class="form-group">
                        <select name="type" class="form-control">
                            <option value="id" <?php echo ($type AND !empty($type) AND $type=='id')?'selected':'';?>>Id</option>
                            <option value="username" <?php echo ($type AND !empty($type) AND $type=='username')?'selected':'';?>>Username</option>
                            <option value="name" <?php echo ($type AND !empty($type) AND $type=='name')?'selected':'';?>>Name</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="text" name="search" id="search_box" class="form-control"
                               value="<?php echo (isset($search))?$search:'';?>" placeholder="Enter Keywords">
                    </div>
                    <button type="submit" class="btn btn-warning">Search</button>
                </form>

            </div>
            {{--<div class="col-md-3 pull-right">--}}
                {{--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#user_modal">Add New</button>--}}
            {{--</div>--}}

        </div>
-->
        <div class="col-md-12">

            <h1>Manage Users</h1>

             <a href="{{url('/export_users')}}" class="pull-left btn btn-success" >Export CSV</a>


            <table  class="table table-striped tablesorter users-table">

                <thead>
                    <tr>
                        <th></th>
                        <th>Username</th>
                        <th>Name</th>
                        <th>Companies</th>
                        <th>Is Master</th>
                        <th>Last Login</th>
                        <th width="160"></th>
                    </tr>
                </thead>

                <tbody>

                @foreach($users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->first_name}} {{$user->last_name}}</td>
                    <td>
                        <?php foreach($user->companies as $comp):?>
                            <?php echo $comp->company_name.'<br />'; ?>
                        <?php endforeach; ?>
                    </td>
                    <td><?=($user->is_master)?'Yes':'No';?></td>
                    <td>

                        @if($user->last_login)
                           {{date('F d, Y H:iA',strtotime($user->last_login))}}
                        @endif

                    </td>
                    <td>
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default" tabindex="-1">Action</button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1" aria-expanded="false">
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu " role="menu">



                                <?php foreach($user->companies as $comp):?>
                                    <li><a href="{{url('/users_edit')}}/{{$user->id}}/{{$comp->id}}" class="btn-edit" >Edit (<?php echo $comp->url; ?>)</a></li>
                                <?php endforeach; ?>

                                <li><a href="#" data-id="{{$user->id}}" class="deleter" >Delete</a></li>
                                <li><a href="{{url('/admin_login_user')}}/{{$user->id}}" >Login</a></li>
                                <li><a href="{{url('/admin_add_leave')}}/{{$user->id}}" >Add Leave</a></li>
                            </ul>
                        </div>
                    </td>
                </tr>
                @endforeach

                </tbody>



            </table>




        </div>

    </div>
    <!-- end row -->

</section>
<!-- end widget grid -->

<div class="modal fade" tabindex="-1" role="dialog" id="confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this User ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-primary" id="delete_proceed" alt="">Yes</button>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="user_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add User</h4>
            </div>
            <form action="{{ url('/admin_users_add') }}" method="POST" class="eventInsForm">
                <div class="modal-body">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <label>Username/Email *</label>
                        <input class="form-control" type="text" name="email" value="" required placeholder="" />
                    </div>

                    <div class="form-group">
                        <label>Password *</label>
                        <input class="form-control" type="password" name="password" value="" required placeholder="" />
                    </div>

                    <div class="form-group">
                        <label>Title</label>
                        <input class="form-control" type="text" name="title" value="" placeholder="" />
                    </div>

                    <div class="form-group">
                        <label>First Name</label>
                        <input class="form-control" type="text" name="first_name" value="" placeholder="" />
                    </div>

                    <div class="form-group">
                        <label>Last Name</label>
                        <input class="form-control" type="text" name="last_name" value="" placeholder="" />
                    </div>

                    <div class="form-group">
                        <label>Position</label>
                        <input class="form-control" type="text" name="position" value="" placeholder="" />
                    </div>

                    <div class="form-group">
                        <label>Phone</label>
                        <input class="form-control" type="text" name="phone_number" value="" placeholder="##-####-####"/>
                    </div>

                    <div class="form-group">
                        <label>Company URL *</label>
                        <input class="form-control delete_fields custom_auto_complete" required name="company_url" autocomplete="off" type="text" value="" placeholder="" />
                        <input class="form-control delete_fields" type="hidden" name="company_id" id="company_id" value="" placeholder="" />

                    </div>

                    <div class="form-group">
                        <label>Role</label>
                        Master <input  type="radio" name="role" value="is_master" />
                        Guest <input type="radio" name="role" value="is_guest" checked />
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="org_modal">
    <div class="modal-dialog" role="document">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body" id="edit_modal_body">

            </div>
        </div>
    </div>
</div>

<!-- end widget grid -->

<script type="text/javascript">

    $(document).ready(function(){

        $('.deleter').click(function(){

            var id = $(this).data('id');
            $('#delete_proceed').attr('alt',id);
            $('#confirm').modal('show');

        });

        $('#delete_proceed').click(function(){

            var id = $(this).attr('alt');

            $('#confirm').modal('hide');

            window.location.href='{{ url('user_delete') }}/'+id;

        });

        $( ".custom_auto_complete" ).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "{{ url('/companies_list') }}",
                    dataType: "json",

                    data: {
                        q: request.term
                    },
                    success: function( data ) {
                        console.log(data);
                        response(data);
                    }
                });
            },
            select: function(e,ui){
                console.log(ui);
                $('#company_id').val(ui.item.id);
            }
        });

        $( ".custom_auto_complete" ).autocomplete( "option", "appendTo", ".eventInsForm" );

        $('.btn-edit').click(function(){

            var id = $(this).data('id');

            $.ajax({
                url : '{{url('/users_edit/')}}/'+id,
                type : 'GET',
                success:function(html){
                    $('#edit_modal_body').html(html);
                    $('#edit_modal').modal('show');
                }

            });
        });

      //  $("table").tablesorter({debug:true});

         $('.users-table').DataTable({

           "aoColumns": [
              null,
              null,
              null,
              null,
              null,
              null,
              { "bSortable": false }
            ],
            "aaSorting": [[ 0, "desc" ]],
            "pageLength": 30

        });
    });


</script>


@endsection
