<div class="col-xs-12">
    <!-- Widget ID (each widget will need unique ID)-->
    <div class="jarviswidget jarviswidget-color-darken " id="wid-id-8" data-widget-editbutton="false">
        <header>
            <span class="widget-icon"> <i class="fa fa-user"></i> </span>
            <h2>{{trans("admin_reports.Cumulative Number of Users Per Day")}}</h2>
        </header>
        <!-- widget div-->
        <div>
            <!-- widget edit box -->
            <div class="jarviswidget-editbox">
                <!-- This area used as dropdown edit box -->
            </div>
            <!-- end widget edit box -->

            <!-- widget content -->
            <div class="widget-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="pull-right">
                            <form id="range-form" class="form-inline" role="form" style="margin-left: 15px;display:none">
                                <fieldset>
                                    <div class="form-group">
                                        <label class="sr-only" for="from">{{trans("admin_reports.From")}}</label>
                                        <input type="text" class="form-control" id="from" placeholder="{{trans("admin_reports.Date From")}}">
                                    </div>
                                    <div class="form-group">
                                        <label class="sr-only" for="to">{{trans("admin_reports.To")}}From</label>
                                        <input type="text" class="form-control" id="to" placeholder="{{trans("admin_reports.Date To")}}">
                                    </div>
                                    <button type="submit" class="btn btn-primary">
                                        {{trans("admin_reports.Filter")}}
                                    </button>
                                </fieldset>
                            </form>
                        </div>
                        <div class="pull-right">
                            <form action="">
                                <div class="form-group">
                                    <select class="filter-graph4 form-control">
                                        <option value="30" selected>{{trans("admin_reports.Last 30 days")}}</option>
                                        <option value="6-months-ago">{{trans("admin_reports.Last 6 Months")}}</option>
                                        <option value="12-months-ago">{{trans("admin_reports.Last 12 Months")}}</option>
                                        <option value="all-time">{{trans("admin_reports.Beginning of time")}}</option>
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="users-report-cumulative" class="chart"></div>
            </div>
            <!-- end widget content -->

        </div>
        <!-- end widget div -->

    </div>
    <!-- end widget -->
</div>