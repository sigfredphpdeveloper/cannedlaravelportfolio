@extends('admin.dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->

    <div class="row">

    <div class="col-md-12">

    <a href="{{url('/manage_users')}}" class="btn btn-primary">Back</a>

     <h1>Edit User</h1>

                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <p>{{ Session::get('success') }}</p>
                    </div>
                @endif
                </div>

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>{{trans("directory_invite_user.Warning.")}}<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <article class="col-sm-6 col-md-6 col-lg-6">


                <form action="{{ url('/users_edit') }}/{{$user->id}}/{{$company_user->id}}" method="POST" class="eventInsForm2">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                    <label>Username/Email *</label>
                    <input class="form-control" type="text" name="email" value="{{$user->email}}" required placeholder="" />
                </div>

                <div class="form-group">
                    <label>Password (only fill if you want to change)</label>
                    <input class="form-control" type="password" name="password" value=""  placeholder="" />
                </div>

                <div class="form-group">
                    <label>Title</label>
                    <input class="form-control" type="text" name="title" value="{{$user->title}}" placeholder="" />
                </div>

                <div class="form-group">
                    <label>First Name</label>
                    <input class="form-control" type="text" name="first_name" value="{{$user->first_name}}" placeholder="" />
                </div>

                <div class="form-group">
                    <label>Last Name</label>
                    <input class="form-control" type="text" name="last_name" value="{{$user->last_name}}" placeholder="" />
                </div>

                <div class="form-group">
                    <label>Position</label>
                    <input class="form-control" type="text" name="position" value="{{$user->position}}" placeholder="" />
                </div>

                <div class="form-group">
                    <label>Phone</label>
                    <input class="form-control" type="text" name="phone_number" value="{{$user->phone_number}}" placeholder="##-####-####"/>
                </div>

                <div class="form-group">
                    <label>Company URL *</label>
                    <input class="form-control delete_fields custom_auto_complete2" required name="company_url" autocomplete="off" type="text" value="{{$company_user->url}}" placeholder="" />
                    <input class="form-control delete_fields" type="hidden" name="company_id" id="company_id2" value="" placeholder="" />

                </div>

                <div class="form-group">
                    <label>Role</label>
                    Master <input  type="radio" name="role" value="is_master" <?php echo ($company_user->is_master==1)?'checked':'';?> />
                    Guest <input type="radio" name="role" value="is_guest" <?php echo ($company_user->is_guest==1)?'checked':'';?> />
                </div>

                <button type="submit" class="btn btn-primary">Save changes</button>
        </form>

     </article>

    </div>

    <!-- end row -->

</section>

<script type="text/javascript">
    $(document).ready(function(){

        $( ".custom_auto_complete2" ).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "{{ url('/companies_list') }}",
                    dataType: "json",

                    data: {
                        q: request.term
                    },
                    success: function( data ) {
                        console.log(data);
                        response(data);
                    }
                });
            },
            select: function(e,ui){
                console.log(ui);
                $('#company_id2').val(ui.item.id);
            }
        });

        $( ".custom_auto_complete2" ).autocomplete( "option", "appendTo", ".eventInsForm2" );

    });
</script>



<!-- end widget grid -->


@endsection