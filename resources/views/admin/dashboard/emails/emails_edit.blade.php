@extends('admin.dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->

    <div class="row">

    <div class="col-md-12">

    <a href="{{url('email_templates')}}" class="btn btn-primary">{{trans("admin.Back")}}</a>

     <h1>{{trans("admin.Edit Email Template")}}</h1>

                @if(Session::has('success'))
                    <div class="alert alert-success">
                        <p>{{ Session::get('success') }}</p>
                    </div>
                @endif
                </div>

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>{{trans("directory_invite_user.Warning.")}}<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <article class="col-sm-6 col-md-6 col-lg-6">


                <form method="POST" action="{{url('/email_templates')}}/{{$email_template->id}}" accept-charset="UTF-8" class="">
                <input name="_method" type="hidden" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group">
                    <label>{{trans("admin.Slug")}}</label>
                    <input class="form-control" type="text" name="slug" disabled value="{{$email_template->slug}}"/>
                </div>

                <div class="form-group">
                    <label>{{trans("admin.Subject")}}</label>
                    <input class="form-control" type="text" name="subject"  value="{{$email_template->subject}}" />
                </div>

                 <div class="form-group">
                    <label>{{trans("admin.Message Body")}}</label>
                    <textarea name="body" class="form-control" style="height:400px;width:100%;">{{$email_template->body}}</textarea>
                </div>

                <button type="submit" class="btn btn-primary">{{trans("admin.Save changes")}}</button>

            {!! Form::close() !!}

     </article>

    </div>

    <!-- end row -->

</section>

<script type="text/javascript">
    $(document).ready(function(){

        $( ".custom_auto_complete2" ).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "{{ url('/companies_list') }}",
                    dataType: "json",

                    data: {
                        q: request.term
                    },
                    success: function( data ) {
                        console.log(data);
                        response(data);
                    }
                });
            },
            select: function(e,ui){
                console.log(ui);
                $('#company_id2').val(ui.item.id);
            }
        });

        $( ".custom_auto_complete2" ).autocomplete( "option", "appendTo", ".eventInsForm2" );

    });
</script>



<!-- end widget grid -->


@endsection