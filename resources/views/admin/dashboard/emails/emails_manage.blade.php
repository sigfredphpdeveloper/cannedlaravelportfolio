@extends('admin.dashboard.layouts.master')

@section('content')

<link rel="stylesheet" href="{{ asset('css/blue/style.css') }}" type="text/css" id="" media="print, projection, screen" />
<style>
table.tablesorter thead tr th, table.tablesorter tfoot tr th {
    background-color: #F7DCB4 !important;
    font-size: 12pt;
}
</style>
{{--<script src="{{ asset('js/jquery-latest.js') }}"></script>--}}
<script src="{{ asset('js/jquery.metadata.js') }}"></script>
<script src="{{ asset('js/jquery.tablesorter.min.js') }}"></script>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-md-12">
            @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
            @endif
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Warning.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="col-md-12">

            <h1>{{trans("admin.Manage Email Templates")}}</h1>

            <table  class="table table-striped tablesorter">

                <thead>
                    <tr>
                        <th></th>
                        <th>{{trans("admin.Subject")}}</th>
                        <th>{{trans("admin.Slug")}}</th>
                        <th width="160"></th>
                    </tr>

                </thead>

                <tbody>

                @foreach($email_templates as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->subject}}</td>
                    <td>{{$item->slug}}</td>
                    <td>
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default" tabindex="-1">{{trans("admin.Action")}}</button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1" aria-expanded="false">
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu " role="menu">
                            <li><a href=" {{ route('email_templates.edit', ['id' => $item->id]) }} ">{{trans("admin.Edit")}}</a></li>
                            </ul>
                        </div>
                    </td>
                </tr>
                @endforeach

                </tbody>

            </table>

        </div>

    </div>
    <!-- end row -->

</section>
<!-- end widget grid -->


<!-- end widget grid -->

<script type="text/javascript">

    $(document).ready(function(){

        $("table").tablesorter({debug:true});

    });


</script>


@endsection
