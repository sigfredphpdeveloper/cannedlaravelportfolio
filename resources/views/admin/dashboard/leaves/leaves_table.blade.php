@extends('admin.dashboard.layouts.master')

@section('content')

<link rel="stylesheet" href="{{ asset('css/blue/style.css') }}" type="text/css" id="" media="print, projection, screen" />
<style>
table.tablesorter thead tr th, table.tablesorter tfoot tr th {
    background-color: #F7DCB4 !important;
    font-size: 12pt;
}
</style>
{{--<script src="{{ asset('js/jquery-latest.js') }}"></script>--}}
<script src="{{ asset('js/jquery.metadata.js') }}"></script>
<script src="{{ asset('js/jquery.tablesorter.min.js') }}"></script>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-md-12">
            @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
            @endif
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Warning.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="col-md-3">

        </div>

        <div class="col-md-9 pull-right">

            <div class="input-group pull-right">

                <form class="form-inline" method="GET">
                    <div class="form-group">
                        <input type="text" name="search" id="search_box" class="form-control"
                               value="<?php echo (isset($search))?$search:'';?>" placeholder="{{trans("leaves.Enter Keywords")}}">
                    </div>
                    <button type="submit" class="btn btn-warning">{{trans("leaves.Search")}}</button>
                </form>

            </div>

        </div>

        <div class="col-md-12">

            <h1>{{$title}}</h1>

            <table class="table table-striped datatable-dom-position">
                <thead>
                    <tr>
                        <th>{{trans("leaves.ID")}}</th>
                        <th>{{trans("leaves.User")}}</th>
                        <th>{{trans("leaves.Date")}}</th>
                        <th>{{trans("leaves.Type")}}</th>
                        <th>{{trans("leaves.Details")}}</th>
                        <th>{{trans("leaves.Credits")}}</th>
                        <th>{{trans("leaves.Status")}}</th>
                        <th style="width:100px;" ></th>
                    </tr>
                </thead>

                <tbody>

                @foreach($leaves as $leave)
                <tr>
                    <td>{{$leave->id}}</td>
                    <td>{{$leave->user->email}}</td>
                    <td>
                        @if($leave->leave_application_type =='single')
                            {{$leave->date}} ({{$leave->date_type}})
                        @else
                            From {{$leave->date_from}} ({{$leave->date_from_type}})
                            To {{$leave->date_to}} ({{$leave->date_to_type}})
                        @endif
                    </td>
                    <td>{{$leave->Leave_types->type}}</td>
                    <td>{{$leave->details}}</td>
                    <td>{{$leave->credits}}
                    <td>{{$leave->status}}</td>
                    <td>
                        <!--
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default" tabindex="-1">Action</button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1" aria-expanded="false">
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu " role="menu">
                                <li><a href="#" data-id="{{$leave->id}}" class="deleter" >Delete</a></li>
                            </ul>
                        </div>
                        -->
                    </td>
                </tr>
                @endforeach

                </tbody>


            </table>


        </div>

    </div>
    <!-- end row -->

</section>
<!-- end widget grid -->



<!-- end widget grid -->

<script type="text/javascript">

    $(document).ready(function(){

        $('.deleter').click(function(){

            var id = $(this).data('id');
            $('#delete_proceed').attr('alt',id);
            $('#confirm').modal('show');

        });

        $('#delete_proceed').click(function(){

            var id = $(this).attr('alt');

            $('#confirm').modal('hide');

            window.location.href='{{ url('company_delete') }}/'+id;

        });

        $( ".custom_auto_complete" ).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "{{ url('/companies_list') }}",
                    dataType: "json",

                    data: {
                        q: request.term
                    },
                    success: function( data ) {
                        console.log(data);
                        response(data);
                    }
                });
            },
            select: function(e,ui){
                console.log(ui);
                $('#company_id').val(ui.item.id);
            }
        });

        $( ".custom_auto_complete" ).autocomplete( "option", "appendTo", ".eventInsForm" );

        $("table").tablesorter({debug:true});

    });

</script>


@endsection
