@extends('admin.dashboard.layouts.master')

@section('content')

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-md-6">
            <h1>{{trans("leaves.Add Leave")}} - {{$user->first_name}} {{$user->last_name}}</h1>
        </div>

        <div class="col-md-6 pull-right">

        </div>


        <div class="col-md-12">
        @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>{{trans("leaves.Warning")}}.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <article class="col-sm-6 col-md-6 col-lg-6">

            <form  id="submit_apply" class="smart-form client-form" role="form" method="POST" action="{{ url('/apply-leave-process') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="user_id" value="{{$user->id}}" />
            <input type="hidden" name="admin" value="1">

                @include('admin.dashboard.leaves.form')

                <footer>

                    <button type="submit" class="btn btn-success" id="submit_apply_btn">
                         {{trans("leaves.Save")}}
                    </button>
                    <a href="{{url('/manage_users')}}" class="btn btn-primary">Back</a>
                </footer>

            </form>

        </article>
    </div>

    <!-- end row -->

</section>
<!-- end widget grid -->


@endsection


@section('page-script')
<script type="text/javascript" src="{{ url('/js/leaves.js') }} "></script>
@endsection