
<fieldset>

    <section>
        <label class="label">{{trans("leaves.Leave Type")}}</label>
        <div class="row">
            <div class="col col-4">
                <label class="radio">
                    <input type="radio" name="leave_application_type" checked="checked" class="leave_selector" value="single" />
                    <i></i>{{trans("leaves.Single Day")}}</label>
            </div>
            <div class="col col-4">
                <label class="radio">
                    <input type="radio" name="leave_application_type" class="leave_selector" value="multiple" />
                    <i></i>{{trans("leaves.Multiple Days")}}</label>
            </div>
        </div>
    </section>

    <div id="single_day">
        <section>
            <label class="label">{{trans("leaves.Date")}} *</label>

            <div class="row">
                <div class="col col-6">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" name="date" placeholder="Select a date"
                             class="form-control datepicker "
                             id="datepicker" required data-dateformat="mm/dd/yy" id="dp1462167530325">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                </div>



                    <div class="col col-6" @if($company->leaves_halfday == 0) style="display:none;" @endif>

                         <div class="form-group">
                             <div class="input-group">
                                 <select name="date_type" class="form-control">
                                    <option value="All Day">{{trans("leaves.All Day")}}</option>
                                    <option value="Morning">{{trans("leaves.Morning")}}</option>
                                    <option value="Afternoon">{{trans("leaves.Afternoon")}}</option>
                                 </select>
                             </div>
                         </div>
                    </div>


            </div>
        </section>
    </div>

    <div id="multiple_day" style="display:none;">
        <section>
            <label class="label">{{trans("leaves.From")}}</label>

            <div class="row">

                <div class="col col-6">

                    <div class="form-group">
                        <div class="input-group">
                            <input class="form-control " id="from" name="from" type="text" placeholder="From">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>

                </div>

                <div class="col col-6">
                    <div class="form-group">
                         <div class="input-group">
                             <select name="date_type_from" class="form-control">
                                <option value="All Day">{{trans("leaves.All Day")}}</option>
                                <option value="Morning">{{trans("leaves.Morning")}}</option>
                                <option value="Afternoon">{{trans("leaves.Afternoon")}}</option>
                             </select>
                         </div>
                     </div>
                </div>

            </div>
            </section>

            <section>
                <label class="label">{{trans("leaves.To")}}</label>

                <div class="row">

                    <div class="col col-6">

                        <div class="form-group">
                            <div class="input-group">
                                <input class="form-control " id="to" name="to" type="text" placeholder="Select a date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>

                    </div>

                    <div class="col col-6">
                        <div class="form-group">
                             <div class="input-group">
                                 <select name="date_type_to" class="form-control">
                                    <option value="All Day">{{trans("leaves.All Day")}}</option>
                                    <option value="Morning">{{trans("leaves.Morning")}}</option>
                                    <option value="Afternoon">{{trans("leaves.Afternoon")}}</option>
                                 </select>
                             </div>
                         </div>
                    </div>
                </div>
        </section>

    </div>


     <section>
        <label class="label">{{trans("leaves.Type of Leave")}}</label>
        <label class="input">

            <select name="leave_types_id" class="form-control">
                @foreach($leave_types_set as $lt)
                    <option value="{{$lt->id}}">{{ $lt->type }}</option>
                @endforeach
             </select>

        </label>
    </section>

    <section>
        <label class="label">{{trans("leaves.Details")}}</label>
        <label class="input">
            <textarea name="details" class="form-control" style="height:120px;"></textarea>
        </label>
    </section>


</fieldset>

