<nav>

    <ul>
        <li >
            <a href="{{ url('/admin_dashboard') }}" title='{{trans("admin.Dashboard")}}'>
            <i class="fa fa-lg fa-fw fa-dashboard"></i> <span class="menu-item-parent">{{trans("admin.Dashboard")}}</span></a>
        </li>

        <li>
            <a href="#"><i class="fa fa-lg fa-fw fa-user"></i> <span class="menu-item-parent">{{trans("admin.Users")}}</span></a>
            <ul>
                <li>
                    <a href="{{ url('/manage_users') }}">{{trans("admin.Manage Users")}}</a>
                </li>
                <li>
                    <a href="{{ url('/users-reports') }}">Reports</a>
                </li>
            </ul>
        </li>

        <li>
            <a href="#"><i class="fa fa-lg fa-fw fa-building-o"></i> <span class="menu-item-parent">{{trans("admin.Companies")}}</span></a>
            <ul>
                <li>
                    <a href="{{ url('/manage_companies') }}">{{trans("admin.Manage Companies")}}</a>
                </li>
                <li>
                    <a href="{{ url('/companies-reports') }}">Reports</a>
                </li>
                <li>
                     <a href="{{ url('/admin-company-stats') }}">Company Stats</a>
                 </li>
            </ul>
        </li>

        <li>
            <a href="#"><i class="fa fa-lg fa-fw fa-envelope"></i> <span class="menu-item-parent">{{trans("admin.Email Templates")}}</span></a>
            <ul>
                <li>
                    <a href="{{ url('email_templates') }}">{{trans("admin.Manage")}}</a>
                </li>
            </ul>
        </li>
        
        <li>
            <a href="#"><i class="fa fa-lg fa-fw fa-clock-o"></i> <span class="menu-item-parent">
            {{trans("leaves.User Leaves")}}</span></a>
            <ul>

                <li>
                    <a href="{{ url('/admin_approved_leaves') }}">{{trans("leaves.Approved Leaves")}}</a>
                </li>

                <li>
                    <a href="{{ url('/admin_pending_leaves') }}">{{trans("leaves.Pending Leaves")}}</a>
                </li>

                <li>
                    <a href="{{ url('/admin_rejected_leaves') }}">{{trans("leaves.Rejected Leaves")}}</a>
                </li>

            </ul>
        </li>

    </ul>
</nav>
