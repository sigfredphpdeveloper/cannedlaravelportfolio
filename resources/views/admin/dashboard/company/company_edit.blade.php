<form action="{{ url('/company_edit') }}/{{$company['id']}}" method="POST" class="eventInsForm2">
    <div class="modal-body">

        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="form-group">
            <label>Company Name</label>
            <input class="form-control" type="text" name="company_name" value="{{$company['company_name']}}" required  />
        </div>

        <div class="form-group">
            <label>Company URL</label>
            <input class="form-control" type="text" name="url" value="{{$company['url']}}" required  />
        </div>

        <div class="form-group">
            <label>Industry</label>
            <select name="industry" class="form-control" placeholder="Select Industry">
                <option value="">--- Select Industry ---</option>
                <option value="Agency (branding/design/creative/video)" <?php echo ($company['industry']=='Agency (branding/design/creative/video)')?'selected':'';?>>Agency (branding/design/creative/video)</option>
                <option value="Agency (web)" <?php echo ($company['industry']=='Agency (web)')?'selected':'';?>>Agency (web)</option>
                <option value="Agency (SEM/SEO/social media)" <?php echo ($company['industry']=='Agency (SEM/SEO/social media)')?'selected':'';?>>Agency (SEM/SEO/social media)</option>
                <option value="Agency (other)" <?php echo ($company['industry']=='Agency (other)')?'selected':'';?>>Agency (other)</option>
                <option value="Church/religious organisation" <?php echo ($company['industry']=='Church/religious organisation')?'selected':'';?>>Church/religious organisation</option>
                <option value="E-Commerce" <?php echo ($company['industry']=='E-Commerce')?'selected':'';?>>E-Commerce</option>
                <option value="Engineering" <?php echo ($company['industry']=='Engineering')?'selected':'';?>>Engineering</option>
                <option value="Health/beauty" <?php echo ($company['industry']=='Health/beauty')?'selected':'';?>>Health/beauty</option>
                <option value="Hospitality" <?php echo ($company['industry']=='Hospitality')?'selected':'';?>>Hospitality</option>
                <option value="Industrials" <?php echo ($company['industry']=='Industrials')?'selected':'';?>>Industrials</option>
                <option value="IT/technology" <?php echo ($company['industry']=='IT/technology')?'selected':'';?>>IT/technology</option>
                <option value="Media/publishing" <?php echo ($company['industry']=='Media/publishing')?'selected':'';?>>Media/publishing</option>
                <option value="Non profit" <?php echo ($company['industry']=='Non profit')?'selected':'';?>>Non profit</option>
                <option value="Other" <?php echo ($company['industry']=='Other')?'selected':'';?>>Other</option>
                <option value="Retail/consumer merchandise" <?php echo ($company['industry']=='Retail/consumer merchandise')?'selected':'';?>>Retail/consumer merchandise</option>
                <option value="School/education" <?php echo ($company['industry']=='School/education')?'selected':'';?>>School/education</option>
                <option value="Software/SaaS" <?php echo ($company['industry']=='Software/SaaS')?'selected':'';?>>Software/SaaS</option>
                <option value="Travel/leisure" <?php echo ($company['industry']=='Travel/leisure')?'selected':'';?>>Travel/leisure</option>
            </select>
        </div>

        <div class="form-group">
            <label>Size</label>
            <select name="size" class="form-control">
                <option value="1-10" <?php echo ($company['size']=='1-10')?'selected':'';?>>1-10</option>
                <option value="10-50" <?php echo ($company['size']=='10-50')?'selected':'';?>>10-50</option>
                <option value="50-500" <?php echo ($company['size']=='50-500')?'selected':'';?>>50-500</option>
                <option value="500+" <?php echo ($company['size']=='500+')?'selected':'';?>>500+</option>
            </select>
        </div>

        <div class="form-group">
            <label>Address</label>
            <input type="text" name="address" class="form-control"  value="{{$company['address']}}" required/>
        </div>

        <div class="form-group">
            <label>Zip Code</label>
            <input type="text" name="zip" class="form-control"  value="{{$company['zip']}}" />
        </div>

        <div class="form-group">
            <label>City</label>
            <input type="text" class="form-control" name="city"  value="{{$company['city']}}" />
        </div>

        <div class="form-group">
            <label>Country</label>
            @include('common.countries', ['default' => $company['country'] ])
        </div>

        <div class="form-group">
            <label>State</label>
            <input type="text" class="form-control" name="state"  value="{{$company['state']}}" />
        </div>

        <div class="form-group">
            <label>Phone Number</label>
            <input type="text" name="phone_number" class="form-control" value="{{$company['phone_number']}}" />
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
    </div>
</form>
