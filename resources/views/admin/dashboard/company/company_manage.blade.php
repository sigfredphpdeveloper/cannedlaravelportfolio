@extends('admin.dashboard.layouts.master')

@section('content')

<link rel="stylesheet" href="{{ asset('css/blue/style.css') }}" type="text/css" id="" media="print, projection, screen" />
<style>
table.tablesorter thead tr th, table.tablesorter tfoot tr th {
    background-color: #F7DCB4 !important;
    font-size: 12pt;
}
</style>
{{--<script src="{{ asset('js/jquery-latest.js') }}"></script>--}}
<script src="{{ asset('js/jquery.metadata.js') }}"></script>
<script src="{{ asset('js/jquery.tablesorter.min.js') }}"></script>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-md-12">
            @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
            @endif
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Warning.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="col-md-3">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#company_modal">Add</button>

        </div>

        <div class="col-md-9 pull-right">

            <div class="input-group pull-right">

                <form class="form-inline" method="GET">
                    <div class="form-group">
                        <input type="text" name="search" id="search_box" class="form-control"
                               value="<?php echo (isset($search))?$search:'';?>" placeholder="Enter Keywords">
                    </div>
                    <button type="submit" class="btn btn-warning">Search</button>
                </form>

            </div>

        </div>

        <div class="col-md-12">

            <h1>Manage Companies</h1>

            <table class="table table-striped tablesorter">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Company</th>
                        <th>URL</th>
                        <th>Country</th>
                        <th>City</th>
                        <th>Employees</th>
                        <th>Industry</th>
                        <th>Master</th>
                        <th>Modules</th>
                        <th style="width:100px;" ></th>
                    </tr>
                </thead>

                <tbody>

                @foreach($companies as $company)
                <tr>
                    <td>{{$company->id}}</td>
                    <td>{{$company->company_name}}</td>
                    <td>{{$company->url}}</td>
                    <td>{{App\Classes\Helper::code_to_country($company->country)}}</td>
                    <td>{{$company->city}}</td>
                    <td>{{$company->no_employees}}</td>
                    <td>{{$company->industry}}</td>
                    <td>{{$company->master_email}}</td>
                    <td>{{$company->facilities}}</td>
                    <td>
                        <div class="input-group-btn">
                            <button type="button" class="btn btn-default" tabindex="-1">Action</button>
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" tabindex="-1" aria-expanded="false">
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu " role="menu">
                                <li><a href="#" data-id="{{$company->id}}" class="btn-edit" >Edit</a></li>
                                <li><a href="#" data-id="{{$company->id}}" class="deleter" >Delete</a></li>
                            </ul>
                        </div>
                    </td>
                </tr>
                @endforeach

                </tbody>

                <tfooot>
                    <tr>
                        <td colspan="10"> {!! $companies->render() !!}</td>
                    </tr>
                </tfooot>

            </table>

             <a href="{{url('/export_companies')}}" class="pull-right btn btn-success" >Export CSV</a>


        </div>

    </div>
    <!-- end row -->

</section>
<!-- end widget grid -->


<!-- Modal -->
<div class="modal fade" id="company_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Company</h4>
            </div>
            <form action="{{ url('/admin_company_add') }}" method="POST" class="eventInsForm">
                <div class="modal-body">

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <label>Company Name</label>
                        <input class="form-control" type="text" name="company_name" value="" required placeholder="" />
                    </div>

                    <div class="form-group">
                        <label>Company URL</label>
                        <input class="form-control" type="text" name="url" value="" required placeholder="" />
                    </div>

                    <div class="form-group">
                        <label>Industry</label>
                        <select name="industry" class="form-control" placeholder="Select Industry">
                            <option value="Agency (branding/design/creative/video)">Agency (branding/design/creative/video)</option>
                            <option value="Agency (web)">Agency (web)</option>
                            <option value="Agency (SEM/SEO/social media)" >Agency (SEM/SEO/social media)</option>
                            <option value="Agency (other)" >Agency (other)</option>
                            <option value="Church/religious organisation" >Church/religious organisation</option>
                            <option value="E-Commerce" >E-Commerce</option>
                            <option value="Engineering" >Engineering</option>
                            <option value="Health/beauty" >Health/beauty</option>
                            <option value="Hospitality" >Hospitality</option>
                            <option value="Industrials" >Industrials</option>
                            <option value="IT/technology" >IT/technology</option>
                            <option value="Media/publishing" >Media/publishing</option>
                            <option value="Non profit" >Non profit</option>
                            <option value="Other" >Other</option>
                            <option value="Retail/consumer merchandise" >Retail/consumer merchandise</option>
                            <option value="School/education" >School/education</option>
                            <option value="Software/SaaS" >Software/SaaS</option>
                            <option value="Travel/leisure" >Travel/leisure</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Size</label>
                        <select name="size" class="form-control">
                            <option value="1-10" >1-10</option>
                            <option value="10-50" >10-50</option>
                            <option value="50-500" >50-500</option>
                            <option value="500+" >500+</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Address</label>
                        <input type="text" name="address" class="form-control" placeholder="" value="" required/>
                    </div>

                    <div class="form-group">
                        <label>Zip Code</label>
                        <input type="text" name="zip" class="form-control" placeholder="" value="" />
                    </div>

                    <div class="form-group">
                        <label>City</label>
                        <input type="text" class="form-control" name="city" placeholder="" value="" />
                    </div>

                    <div class="form-group">
                        <label>Country</label>
                        @include('common.countries', ['default' => '' ])
                    </div>

                    <div class="form-group">
                        <label>State</label>
                        <input type="text" class="form-control" name="state" placeholder="" value="" />
                    </div>

                    <div class="form-group">
                        <label>Phone Number</label>
                        <input type="text" name="phone_number" class="form-control" placeholder="Phone Number" value="" />
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" tabindex="-1" role="dialog" id="confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this Company ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-primary" id="delete_proceed" alt="">Yes</button>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="org_modal">
    <div class="modal-dialog" role="document">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body" id="edit_modal_body">

            </div>
        </div>
    </div>
</div>

<!-- end widget grid -->

<script type="text/javascript">

    $(document).ready(function(){

        $('.deleter').click(function(){

            var id = $(this).data('id');
            $('#delete_proceed').attr('alt',id);
            $('#confirm').modal('show');

        });

        $('#delete_proceed').click(function(){

            var id = $(this).attr('alt');

            $('#confirm').modal('hide');

            window.location.href='{{ url('company_delete') }}/'+id;

        });

        $( ".custom_auto_complete" ).autocomplete({
            source: function( request, response ) {
                $.ajax({
                    url: "{{ url('/companies_list') }}",
                    dataType: "json",

                    data: {
                        q: request.term
                    },
                    success: function( data ) {
                        console.log(data);
                        response(data);
                    }
                });
            },
            select: function(e,ui){
                console.log(ui);
                $('#company_id').val(ui.item.id);
            }
        });

        $( ".custom_auto_complete" ).autocomplete( "option", "appendTo", ".eventInsForm" );

        $('.btn-edit').click(function(){

            var id = $(this).data('id');

            $.ajax({
                url : '{{url('/company_edit/')}}/'+id,
                type : 'GET',
                success:function(html){
                    $('#edit_modal_body').html(html);
                    $('#edit_modal').modal('show');
                }

            });
        });

        $("table").tablesorter({debug:true});

    });

</script>


@endsection
