@extends('admin.dashboard.layouts.master')

@section('content')

<link rel="stylesheet" href="{{ asset('css/blue/style.css') }}" type="text/css" id="" media="print, projection, screen" />
<style>
table.tablesorter thead tr th, table.tablesorter tfoot tr th {
    background-color: #F7DCB4 !important;
    font-size: 12pt;
}
.fa-check{
    color:green !important;
}
.fa-remove{
    color:red !important;
}
</style>
{{--<script src="{{ asset('js/jquery-latest.js') }}"></script>--}}
<script src="{{ asset('js/jquery.metadata.js') }}"></script>
<script src="{{ asset('js/jquery.tablesorter.min.js') }}"></script>

<!-- widget grid -->
<section id="widget-grid" class="">

    <!-- row -->
    <div class="row">

        <div class="col-md-12">
            @if(Session::has('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
            @endif
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Warning.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif



        <div class="col-md-12">

            <h1>Company Stats</h1>

            <table class="table table-striped tablesorter">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Company</th>
                        <th>No. Users</th>
                        <th>Last Login</th>
                        <th>Expense</th>
                        <th>Knowledge</th>
                        <th>Leave</th>
                        <th>Contact</th>
                        <th>CRM</th>
                        <th>File</th>
                        <th style="width:100px;" ></th>
                    </tr>
                </thead>

                <tbody>

                @foreach($companies as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->company_name}}</td>
                    <td>{{$item->number_users}}</td>
                    <td>
                        <?php

                            if(!empty($item->last_login)){
                                echo $item->last_login->toDayDateTimeString();
                            }else{
                                echo 'N/A';
                            }
                        ?>
                    </td>
                    <td>
                       <?=($item->expenses==1)?'<i class="fa fa-check"></i>':'<i class="fa fa-remove"></i>'; ?>
                    </td>
                    <td><?=($item->training==1)?'<i class="fa fa-check"></i>':'<i class="fa fa-remove"></i>'; ?>
                    </td>
                    <td>
                    <?=($item->leaves==1)?'<i class="fa fa-check"></i>':'<i class="fa fa-remove"></i>'; ?>
                    </td>
                    <td>
                    <?=($item->contact_setting==1)?'<i class="fa fa-check"></i>':'<i class="fa fa-remove"></i>'; ?>
                    </td>
                    <td>
                    <?=($item->crm_setting==1)?'<i class="fa fa-check"></i>':'<i class="fa fa-remove"></i>'; ?>
                    </td>
                    <td>
                    <?=($item->file_sharing==1)?'<i class="fa fa-check"></i>':'<i class="fa fa-remove"></i>'; ?>
                    </td>
                    <td>
                    </td>
                </tr>
                @endforeach

                </tbody>

                <tfooot>
                    <tr>
                        <td colspan="10"> </td>
                    </tr>
                </tfooot>

            </table>


        </div>

    </div>
    <!-- end row -->

</section>
<!-- end widget grid -->

<div class="modal fade" tabindex="-1" role="dialog" id="confirm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to delete this Company ? </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-primary" id="delete_proceed" alt="">Yes</button>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- end widget grid -->

<script type="text/javascript">

    $(document).ready(function(){



        $("table").tablesorter({debug:true});

    });

</script>


@endsection
