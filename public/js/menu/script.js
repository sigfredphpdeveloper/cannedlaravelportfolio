jQuery(document).ready(function(){
	//Floating Menu
	jQuery(document).scroll(function() {
	  var y = $(this).scrollTop();
	  if (y > 90) {
		jQuery('.floating-header').fadeIn();
	  } else {
		jQuery('.floating-header').fadeOut();
	  }
	});
		
	
	// MOBILE MENU
	jQuery('.mobile-menu-container').mmenu({
		classes: 'mm-light',
		counters: true,
		offCanvas: {
			position  : 'left',
			zposition :	'front',
		}
	});

	jQuery('a.mobile-menu-trigger').click(function() {
		jQuery('.mobile-menu-container').trigger('open.mm');
	});
	
	jQuery(window).resize(function() {
		jQuery('.mobile-menu-container').trigger("close.mm");
	});

});