﻿$(function () {
    var url = window.location.protocol + "//" + window.location.host + "/update_personal_note";

    // Advanced demo
    $('#divStickyNotesContainer').coaStickyNote({
        resizable: true,
        availableThemes: [
            { text: "Yellow", value: "sticky-note-yellow-theme" },
            { text: "Green", value: "sticky-note-green-theme" },
            { text: "Blue", value: "sticky-note-blue-theme" },
            { text: "Pink", value: "sticky-note-pink-theme" },
            { text: "Orange", value: "sticky-note-orange-theme" }],
        noteDimension: { width: "200px", height: "200px" },
        noteText: "Note box!",
        noteHeaderText: "Note title!",
        deleteLinkText: "X",
        startZIndex: 50,
        beforeCreatingNoteBox: function (note) {
            // Want to do any thing here?
        },
        onNoteBoxCreated: function (note) {
            // Let's save it on server

            var jsonData = {
                'event': 'create_note',
                'top': note.settings.notePosition.top,
                'left': note.settings.notePosition.left,
                'z_index': note.settings.zIndex,
                'height' : note.settings.noteDimension.height,
                'width': note.settings.noteDimension.width,
                'theme': note.settings.defaultTheme.value,
                'title': note.settings.noteHeaderText,
                'content': note.settings.noteText
            };

            $.ajax({
                type: "GET",
                url: url,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: jsonData,
                success: function(data) {
                    
                }
            });
        },
        onNoteBoxHeaderUpdate: function (note) {
            // Return false, if want to abort the request of header update.
            // Else let's save the updated header text on server, to preserve changes.

            var jsonData = {
                'event' : 'update_title',
                'note_id': note.id,
                'title': note.settings.noteHeaderText
            }

            $.ajax({
                type: "GET",
                url: url,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: jsonData,
                success: function(data) {
                    
                }
            });
        },
        onNoteBoxTextUpdate: function (note) {
            // We can also show confirm box here. Which is common while deleting some thing!
            // Return false, if want to abort the request of text update.
            // Else let's save the updated note text on server, to preserve changes.

            var jsonData = {
                'event': 'update_content',
                'note_id': note.id,
                'content': note.settings.noteText
            }

            $.ajax({
                type: "GET",
                url: url,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: jsonData,
                success: function(data) {
                    
                }
            });
        },
        onNoteBoxDelete: function (note) {
            // Return false, if want to abort the note delete request .
            // Else let's delete the note details from server, to preserve changes.

            var jsonData = {
                'event': 'delete_note',
                'note_id': note.id
            }

            $.ajax({
                type: "GET",
                url: url,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: jsonData,
                success: function(data) {
                    
                }
            });
        },
        onNoteBoxResizeStop: function (note) {
            // Note box dimension got changed.
            // let's save the updated dimension(width/ height) on server, to preserve changes.

            var jsonData = {
                'event': 'update_dimension',
                'note_id': note.id,
                'width': note.settings.noteDimension.width,
                'height': note.settings.noteDimension.height
            }

            $.ajax({
                type: "GET",
                url: url,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: jsonData,
                success: function(data) {
                    
                }
            });
        },
        onNoteBoxDraggingStop: function (note) {
            // Note box position got changed.
            // let's save the updated position(top/ left) on server, to preserve changes.

            var jsonData = {
                'event': 'update_position',
                'note_id': note.id,
                'top': note.settings.notePosition.top,
                'left': note.settings.notePosition.left
            }

            $.ajax({
                type: "GET",
                url: url,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: jsonData,
                success: function(data) {
                    
                }
            });
        },
        onThemeSelectionChange: function (note) {
            // Note box theme got changed.
            // let's save the updated theme on server, to preserve changes.

            var jsonData = {
                'event': 'update_theme',
                'note_id': note.id,
                'theme': note.settings.defaultTheme.value
            }

            $.ajax({
                type: "GET",
                url: url,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: jsonData,
                success: function(data) {
                    
                }
            });
        },
        onMovingNoteBoxOnTop: function (note) {
            // Note box z-index got changed to be on top of all the notes.
            // let's save the updated the z-index on server, to preserve changes.

            var jsonData = {
                'event': 'update_z_index',
                'note_id': note.id,
                'z_index': note.settings.zIndex
            }

            $.ajax({
                type: "GET",
                url: url,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: jsonData,
                success: function(data) {
                    
                }
            });
        },
    });

    function getBackEndStickyObject(note) {
        return {
            Title: note.settings.noteHeaderText,
            NoteText: note.settings.noteText,
            PositionTop: note.settings.notePosition.top,
            PositionLeft: note.settings.notePosition.left,
            DimensionWidth: note.settings.noteDimension.width,
            DimensionHeight: note.settings.noteDimension.height,
            ZIndex: note.settings.zIndex,
            OuterCssClass: note.settings.defaultTheme.value,
            Id: note.id,
			Index: note.index
        };
    }

    function getLocalStickyNoteObject(backEndObj, note) {
        if (note == null) {
            note = {};
            note.settings = {};
            note.settings.notePosition = {};
            note.settings.defaultTheme = {};
            note.settings.noteDimension = {};
        }

        note.settings.noteHeaderText = backEndObj.Title;
        note.settings.noteText = backEndObj.NoteText;
        note.settings.notePosition.top = backEndObj.PositionTop;
        note.settings.notePosition.left = backEndObj.PositionLeft;
        note.settings.noteDimension.width = backEndObj.DimensionWidth;
        note.settings.noteDimension.height = backEndObj.DimensionHeight;
        note.settings.zIndex = backEndObj.ZIndex;
        note.settings.defaultTheme.value = backEndObj.OuterCssClass;
        note.id = backEndObj.Id;
		note.index = backEndObj.index;

        return note;
    }
});