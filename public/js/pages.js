(function(){
	window.Pages = {

		init : function(){
			this.bindEvents();
		},

		bindEvents : function(){
			$('#btn_add_page').on( 'click',  $.proxy(this.addPageModal,this) );
			$('.delete-page-modal').on( 'click',  $.proxy(this.deleteTopicModal,this) );
			// $('.delete-topic-modal').on( 'click',  $.proxy(this.deleteTopicModal,this) );
		},

		addPageModal : function(e){
			var self = $(e.currentTarget);
            var id = self.attr('data-content');

            zen_ajax({
                type:'GET',
                url: base_url+'/add-page/'+id
            }).done(function(data){
                    var modalClass = ".modal";
                    $(modalClass).remove();
                    $("body").append(data);
                    $(modalClass).modal('show');
                }).fail(function(xhr) {
                    console.log('Something went wrong. Please try again later.');
                });
		},

		deleteTopicModal : function(e){
			var self = $(e.currentTarget);
            var id = self.attr('data-content');

            zen_ajax({
                type:'GET',
                url: base_url+'/delete-page/'+id
            }).done(function(data){
                    var modalClass = ".modal";
                    $(modalClass).remove();
                    $("body").append(data);
                    $(modalClass).modal('show');
                }).fail(function(xhr) {
                    console.log('Something went wrong. Please try again later.');
                });
		},

    }

	Pages.init();
})();