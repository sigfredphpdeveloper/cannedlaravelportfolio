(function(){
	window.ShowExpenses = {
		init : function(){
			this.bindEvents();
		},

		bindEvents : function(){
            $('#btn_add_new_line').on( 'click',  $.proxy(this.addExpenseLinesModal,this) );
            $('.edit-expense-line-modal').on( 'click', $.proxy(this.editExpenseLinesModal,this) );
            $('.edit-expense-modal').on('click', $.proxy(this.editExpenseModal,this) );
            $('.delete-expense-modal').on('click', $.proxy(this.deleteExpenseModal,this) );
            $('.delete-expense-line-modal').on('click', $.proxy(this.deleteExpenseLineModal,this) );
        },

        addExpenseLinesModal : function(e){
        	var self = $(e.currentTarget);
        	var id = self.attr('data-content');

            zen_ajax({
                type:'GET',
                url: base_url+'/add-expense-lines/'+id
            }).done(function(data){
                    var modalClass = ".modal";
                    $(modalClass).remove();
                    $("body").append(data);
                    $(modalClass).modal('show');
                }).fail(function(xhr) {
                    console.log('Something went wrong. Please try again later.');
                });
        },

        editExpenseLinesModal : function(e){
            var self = $(e.currentTarget);
            var id = self.attr('data-content');

            zen_ajax({
                type:'GET',
                url: base_url+'/edit-expense-lines/'+id
            }).done(function(data){
                    var modalClass = ".modal";
                    $(modalClass).remove();
                    $("body").append(data);
                    $(modalClass).modal('show');
                }).fail(function(xhr) {
                    console.log('Something went wrong. Please try again later.');
                });
        },

        editExpenseModal : function(e){
            var self = $(e.currentTarget);
            var id = self.attr('data-content');

            zen_ajax({
                type:'GET',
                url: base_url+'/edit-expense/'+id
            }).done(function(data){
                    var modalClass = ".modal";
                    $(modalClass).remove();
                    $("body").append(data);
                    $(modalClass).modal('show');
                }).fail(function(xhr) {
                    console.log('Something went wrong. Please try again later.');
                });
        },

        deleteExpenseModal : function(e){
            var self = $(e.currentTarget);
            var id = self.attr('data-content');

            zen_ajax({
                type:'GET',
                url: base_url+'/delete-expense/'+id
            }).done(function(data){
                    var modalClass = ".modal";
                    $(modalClass).remove();
                    $("body").append(data);
                    $(modalClass).modal('show');
                }).fail(function(xhr) {
                    console.log('Something went wrong. Please try again later.');
                });
        },

        deleteExpenseLineModal : function(e){
            var self = $(e.currentTarget);
            var data = self.attr('data-content').split('-');

            zen_ajax({
                type:'GET',
                url: base_url+'/delete-expense-line/'+data[0]+'/'+data[1]
            }).done(function(data){
                    var modalClass = ".modal";
                    $(modalClass).remove();
                    $("body").append(data);
                    $(modalClass).modal('show');
                }).fail(function(xhr) {
                    console.log('Something went wrong. Please try again later.');
                });
        }
	},

	ShowExpenses.init();
})();