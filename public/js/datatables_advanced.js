/* ------------------------------------------------------------------------------
*
*  # Advanced datatables
*
*  Specific JS code additions for datatable_advanced.html page
*
*  Version: 1.0
*  Latest update: Aug 1, 2015
*
* ---------------------------------------------------------------------------- */

$(function() {

    $('.datatable-dom-position').DataTable({
        dom: '<"datatable-header length-left"f><"datatable-scroll"t><"datatable-footer info-right"lip>'
    });

});
