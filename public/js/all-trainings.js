(function(){
	window.allTrainings = {

		init : function(){
			this.bindEvents();
		},

		bindEvents : function(){
			$('#btn_add_topic').on( 'click',  $.proxy(this.addTopicModal,this) );
			$('.edit-topic-modal').on( 'click',  $.proxy(this.editTopicModal,this) );
			$('.delete-topic-modal').on( 'click',  $.proxy(this.deleteTopicModal,this) );
		},

		addTopicModal : function(e){
			var self = $(e.currentTarget);

            zen_ajax({
                type:'GET',
                url: base_url+'/add-topic'
            }).done(function(data){
                    var modalClass = ".modal";
                    $(modalClass).remove();
                    $("body").append(data);
                    $(modalClass).modal('show');
                }).fail(function(xhr) {
                    console.log('Something went wrong. Please try again later.');
                });
		},

		editTopicModal : function(e){
			var self = $(e.currentTarget);
			var id = self.attr('data-content');

            zen_ajax({
                type:'GET',
                url: base_url+'/edit-topic/'+id
            }).done(function(data){
                    var modalClass = ".modal";
                    $(modalClass).remove();
                    $("body").append(data);
                    $(modalClass).modal('show');
                }).fail(function(xhr) {
                    console.log('Something went wrong. Please try again later.');
                });
		},

		deleteTopicModal : function(e){
			var self = $(e.currentTarget);
			var id = self.attr('data-content');

            zen_ajax({
                type:'GET',
                url: base_url+'/delete-topic/'+id
            }).done(function(data){
                    var modalClass = ".modal";
                    $(modalClass).remove();
                    $("body").append(data);
                    $(modalClass).modal('show');
                }).fail(function(xhr) {
                    console.log('Something went wrong. Please try again later.');
                });
		},

	}

	allTrainings.init();
})();