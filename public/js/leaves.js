
function eventajaxmodal(id){

    var site_url = $('body').data('site-url');

    $.ajax({
        url : site_url+'view_leave_record/'+id,
        type : 'GET',
        data : 'id='+id,
        success:function(html){
            $('#event_view_leave_body').html(html);
            $('#event_view_leave').modal('show');
        }

    });

}

 $(document).ready(function(){

    $('.leave_selector').change(function(){

        var selected = $(".leave_selector:checked").val();

        if(selected == 'single'){
            $('#single_day').show();
            $('#multiple_day').hide();
            $('#datepicker').attr('required','required');
            $('#to').attr('required',false);
            $('#from').attr('required',false);
        }else if(selected == 'multiple'){
            $('#single_day').hide();
            $('#multiple_day').show();
            $('#datepicker').attr('required',false);
            $('#to').attr('required','required');
            $('#from').attr('required','required');
        }
    });

     // START AND FINISH DATE
    $('#startdate').datepicker({
        dateFormat: 'yy-mm-dd',
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',
        onSelect: function (selectedDate) {
            $('#finishdate').datepicker('option', 'minDate', selectedDate);
        }
    });
    $('#finishdate').datepicker({
        dateFormat: 'yy-mm-dd',
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',
        onSelect: function (selectedDate) {
            $('#startdate').datepicker('option', 'maxDate', selectedDate);
        }
    });

     // Date Range Picker
    $("#from").datepicker({
        defaultDate: "+1w",
        dateFormat: 'yy-mm-dd',
        changeMonth: true,
        numberOfMonths: 3,
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',
        onClose: function (selectedDate) {
            $("#to").datepicker("option", "minDate", selectedDate);
        }

    });
    $("#to").datepicker({
        defaultDate: "+1w",
        changeMonth: true,
        dateFormat: 'yy-mm-dd',
        numberOfMonths: 3,
        prevText: '<i class="fa fa-chevron-left"></i>',
        nextText: '<i class="fa fa-chevron-right"></i>',
        onClose: function (selectedDate) {
            $("#from").datepicker("option", "maxDate", selectedDate);
        }
    });

    $('#submit_apply').submit(function(){

        $('#submit_apply_btn').prop('disabled',true).html('Loading Please Wait...');

        return true;

    });

});
