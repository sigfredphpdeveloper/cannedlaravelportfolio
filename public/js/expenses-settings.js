(function(){
    window.ExpensesSettings = {
        init : function(){
            $('[data-toggle="tooltip"]').tooltip();
            this.bindEvents();
        },

        bindEvents : function(){
            $('#btn_add_expense_type').on( 'click',  $.proxy(this.addExpenseModal,this) );
            $('.edit-expense-modal').on( 'click', $.proxy(this.editExpenseModal,this) );
            $('.delete-expense-modal').on( 'click', $.proxy(this.deleteExpenseModal,this) );
            $('.manage-fields-modal').on( 'click', $.proxy(this.manageFieldsModal,this) );
            $('.expense-type-form').on( 'click', $.proxy(this.preventDefaultSubmision,this) );
            $('.permision-toggle').on( 'click', $.proxy(this.permisionToggle,this) );
        },

        preventDefaultSubmision : function(e){
            e.preventDefault();
        },

        addExpenseModal : function(e){
            zen_ajax({
                type:'GET',
                url: base_url+'/add-expense-type'
            }).done(function(data){
                    var modalClass = ".modal";
                    $(modalClass).remove();
                    $("body").append(data);
                    $(modalClass).modal('show');
                }).fail(function(xhr) {
                    console.log('Something went wrong. Please try again later.');
                });
        },

        editExpenseModal : function(e){
            var self = $(e.currentTarget);
            var id = self.attr('data-content');

            zen_ajax({
                type:'GET',
                url: base_url+'/edit-expense-type/'+id
            }).done(function(data){
                     // console.log(data);
                    var modalClass = ".modal";
                    $(modalClass).remove();
                    $("body").append(data);
                    $(modalClass).modal('show');
                    // $('.image-id').attr('count', imgcount);
                }).fail(function(xhr) {
                    console.log('Something went wrong. Please try again later.');
                });
        },

        deleteExpenseModal : function(e){
            var self = $(e.currentTarget);
            var id = self.attr('data-content');

            zen_ajax({
                type:'GET',
                url: base_url+'/delete-expense-type/'+id
            }).done(function(data){
                    // console.log(data);
                var modalClass = ".modal";
                $(modalClass).remove();
                $("body").append(data);
                $(modalClass).modal('show');
                // $('.image-id').attr('count', imgcount);
            }).fail(function(xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        },

        manageFieldsModal : function(e){
            var self = $(e.currentTarget);
            var id = self.attr('data-content');
            zen_ajax({
                type:'GET',
                url: base_url+'/exepenses-type-manage-fields/'+id
            }).done(function(data){
                    var modalClass = ".modal";
                    $(modalClass).remove();
                    $("body").append(data);
                    $(modalClass).modal('show');
                }).fail(function(xhr) {
                    console.log('Something went wrong. Please try again later.');
                });
        },

        permisionToggle : function(e){
            var self = $(e.currentTarget);
            var action = self.attr('data-action');
            var role_id = self.attr('data-id');
            var value = ''

                if( self.is(':checked') ){
                    value = 1;
                } else {
                    value = 0
                }

                zen_ajax({ 
                    url: base_url+'/edit-expenses-role',
                    type: 'GET',
                    data: 'action='+action+'&role_id='+role_id+'&value='+value,
                    success: function (data) {
                    }
                });
        },

        addFields : function(e){
            var MAX_OPTIONS = $('fieldset.type-group').length;
            // alert(MAX_OPTIONS);
            if( MAX_OPTIONS <= 11 ){
                var $template = $('#option-template');
                $clone = $template
                            .clone()
                            .removeClass('hide')
                            .removeAttr('id')
                            .insertBefore($template),
                $option = $clone.find('[name="label[]"]'),
                $clone.find('.expense-id').addClass('active-id');
                $clone.find('.default-id').addClass('active-default');
                $clone.find('.label-input').addClass('active-label');
                $clone.find('.type-input').addClass('active-type');
                $clone.find('.required-input').addClass('active-required');

                $clone.find('.default-id').attr('name', 'fields['+(MAX_OPTIONS-1)+'][default]');
                $clone.find('.expense-id').attr('name', 'fields['+(MAX_OPTIONS-1)+'][expense_type_id]');
                $clone.find('.label-input').attr('name', 'fields['+(MAX_OPTIONS-1)+'][label]');
                $clone.find('.type-input').attr('name', 'fields['+(MAX_OPTIONS-1)+'][type]');
                $clone.find('.required-input').attr('name', 'fields['+(MAX_OPTIONS-1)+'][required]');

                MAX_OPTIONS++;
                if( MAX_OPTIONS == 11 ){
                    $('#manage-fields-form').find('.addButton').attr('disabled', 'disabled');
                }
            }
        },

        removeFields : function(e){
            var MAX_OPTIONS = $('fieldset.type-group').length;
            var self = $(e.currentTarget);
            var $row    = self.parents('.type-group'),
            $option = $row.find('[name="label[]"]');

            // Remove element containing the option
            $row.remove();
            MAX_OPTIONS--;
            if( MAX_OPTIONS < 11 ){
                    $('#manage-fields-form').find('.addButton').removeAttr('disabled');
            }
        },

    }

    ExpensesSettings.init();
})();
