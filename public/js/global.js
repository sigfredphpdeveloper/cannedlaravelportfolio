$(document).ready(function(){


    $('#company_select').change(function(){

        var base = $('body').data('site-url');

        var company_id = $(this).val();

        if(company_id != ''){
            $.ajax({
                url:base+'company_switch',
                data:'company_id='+company_id,
                type:'GET',
                success:function(resp){
                    window.location.href=base+'dashboard';
                }
            });
        }


    });

});