(function(){
	window.PayExpense = {
		init : function(){
			this.bindEvents();
		},

		bindEvents : function(){
			$('.pay-expense-modal').on( 'click', $.proxy(this.payExpenseModal,this) );
		},

		payExpenseModal : function(e){
			var self = $(e.currentTarget);
			var id = self.attr('data-content');

			zen_ajax({
				type:'GET',
                url: base_url+'/pay-expense/'+id
			}).done(function(data){
                    var modalClass = ".modal";
                    $(modalClass).remove();
                    $("body").append(data);
                    $(modalClass).modal('show');
                }).fail(function(xhr) {
                    console.log('Something went wrong. Please try again later.');
                });
		}

	},

	PayExpense.init();
})();