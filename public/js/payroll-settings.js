(function(){
    window.PayrollSettings = {
        init : function(){
            $('[data-toggle="tooltip"]').tooltip();
            this.bindEvents();
        },

        bindEvents : function(){
            $('#add-payroll-addition').on( 'click',  $.proxy(this.addPayrollAddition,this) );
            $('#add-payroll-deduction').on( 'click',  $.proxy(this.addPayrollDeduction,this) );
            //$('#payroll-addition-table tr').on('click',  $.proxy(this.updatePayrollAddition,this) );
            $('.edit_additional').on('click',  $.proxy(this.updatePayrollAddition,this) );
            //$('#payroll-duduction-table tr').on('click',  $.proxy(this.updatePayrollDeduction,this) );
            $('.edit_deduction').on('click',  $.proxy(this.updatePayrollDeduction,this) );
        },

        addPayrollAddition : function(e){
            zen_ajax({
                type:'GET',
                url: base_url+'/new-payroll-addition'
            }).done(function(data){
                    var modalClass = ".modal";
                    $(modalClass).remove();
                    $("body").append(data);
                    $(modalClass).modal('show');
                }).fail(function(xhr) {
                    console.log('Something went wrong. Please try again later.');
                });
        },

        updatePayrollAddition : function(e){
            var self = $(e.currentTarget);
            var id = self.attr('data-id');

            zen_ajax({
                type:'GET',
                url: base_url+'/update-payroll-addition/'+id
            }).done(function(data){
                    var modalClass = ".modal";
                    $(modalClass).remove();
                    $("body").append(data);
                    $(modalClass).modal('show');
                }).fail(function(xhr) {
                    console.log('Something went wrong. Please try again later.');
                });
        },

        addPayrollDeduction : function(e){
            zen_ajax({
                type:'GET',
                url: base_url+'/new-payroll-deduction'
            }).done(function(data){
                    var modalClass = ".modal";
                    $(modalClass).remove();
                    $("body").append(data);
                    $(modalClass).modal('show');
                }).fail(function(xhr) {
                    console.log('Something went wrong. Please try again later.');
                });
        },

        updatePayrollDeduction : function(e){
            var self = $(e.currentTarget);
            var id = self.attr('data-id');

            zen_ajax({
                type:'GET',
                url: base_url+'/update-payroll-deduction/'+id
            }).done(function(data){
                    var modalClass = ".modal";
                    $(modalClass).remove();
                    $("body").append(data);
                    $(modalClass).modal('show');
                }).fail(function(xhr) {
                    console.log('Something went wrong. Please try again later.');
                });
        },

    }

    PayrollSettings.init();
})();
