(function(){
	window.MyExpenses = {

		init : function(){
			this.bindEvents();
		},

		bindEvents : function(){
			$('#btn_add_expensed').on( 'click',  $.proxy(this.addExpenseModal,this) );
			$('.edit-expense-modal').on('click', $.proxy(this.editExpenseModal,this) );
			$('.delete-expense-modal').on('click', $.proxy(this.deleteExpenseModal,this) );			
		},

		addExpenseModal : function(e){
			var self = $(e.currentTarget);

            zen_ajax({
                type:'GET',
                url: base_url+'/add-expense'
            }).done(function(data){
                    var modalClass = ".modal";
                    $(modalClass).remove();
                    $("body").append(data);
                    $(modalClass).modal('show');
                }).fail(function(xhr) {
                    console.log('Something went wrong. Please try again later.');
                });
		},

		editExpenseModal : function(e){
			var self = $(e.currentTarget);
			var id = self.attr('data-content');

            zen_ajax({
                type:'GET',
                url: base_url+'/edit-expense/'+id
            }).done(function(data){
                    var modalClass = ".modal";
                    $(modalClass).remove();
                    $("body").append(data);
                    $(modalClass).modal('show');
                }).fail(function(xhr) {
                    console.log('Something went wrong. Please try again later.');
                });
		},

		deleteExpenseModal : function(e){
			var self = $(e.currentTarget);
            var id = self.attr('data-content');

            zen_ajax({
                type:'GET',
                url: base_url+'/delete-expense/'+id
            }).done(function(data){
                    var modalClass = ".modal";
                    $(modalClass).remove();
                    $("body").append(data);
                    $(modalClass).modal('show');
                }).fail(function(xhr) {
                    console.log('Something went wrong. Please try again later.');
                });
		},

	}

	MyExpenses.init();
})();