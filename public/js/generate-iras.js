(function(){
	window.GenerateIras = {

		init : function(){
			this.bindEvents();
		},

		bindEvents : function(){
            $('select[name=year]').on( 'change',  $.proxy(this.getEmployeesFromYear,this) );
            $('#submit-employees').on( 'click', $.proxy(this.getIrasFormPerEmployee,this) ); 
            $(document).on('click', '.next-employee', $.proxy(this.nextEmployee,this) );
            $(document).on('click', '.prev-employee', $.proxy(this.previousEmployee,this) );
            $(document).on('focus', '.employee-container input', $.proxy(this.removeErrorState,this) );
		},

        getEmployeesFromYear : function(e){
            var self = $(e.currentTarget);
            var token = $('input[name=_token]').val();
            var year = self.val();

            zen_ajax({
                type:'GET',
                data: {
                    _token: token,
                    year: year,
                },
                url: base_url+'/get-employees-from-year'
            }).done(function(data){
                $('#employee-selection').html(data);
                $('#submit-employees').show();
            }).fail(function(xhr) {
                console.log('Something went wrong. Please try again later.');
            });
        },

		getIrasFormPerEmployee : function(e){
			var self = $(e.currentTarget);
            self.html('<i class="fa fa-spinner fa-spin" aria-hidden="true"></i>');
            var token = $('input[name=_token]').val();
            var employees = $('#employee-select').val();

            zen_ajax({
                type:'GET',
                data: {
                    _token: token,
                    employees: employees,
                },
                url: base_url+'/get-iras-per-employee'
            }).done(function(data){
                self.html('Submit');
                $('#employee-iras-form').html(data);
                $('.datepicker').datepicker({
                    changeYear: true,
                    changeMonth: true,
                    yearRange: "-100:+0",
                    dateFormat: 'yymmdd',
                    nextText: '>>',
                     prevText: '<<',
                    maxDate: '0'
                });
                
                if($('.employee-container').length == 1)
                    $('#generate-btn').prop('disabled', false); 
                else
                    $('#generate-btn').prop('disabled', true);

            }).fail(function(xhr) {
                console.log('Something went wrong. Please try again later.');
            });
		},

        nextEmployee : function(e){
            var self = $(e.currentTarget);
            current_step = self.attr('data-curr-step');
            next_step = self.attr('data-step');
            
            check_fields = this.checkFieldsEmpty(current_step);

            if( check_fields ){
                $(next_step).addClass('active');
                $(current_step).removeClass('active');

                if( $(next_step).hasClass('last-employee') ){
                    $('#generate-btn').prop('disabled', false); 
                } 
            }
            else {
                $(current_step).find('input[required]').parent().addClass('state-error');
            }
            
        },

        previousEmployee : function(e){
            var self = $(e.currentTarget);
            current_step = self.attr('data-curr-step');
            next_step = self.attr('data-step');

            $(next_step).addClass('active');
            $(current_step).removeClass('active');

            if( $(current_step).hasClass('last-employee') ){
                $('#generate-btn').prop('disabled', true); 
            } 
            
        },

        checkFieldsEmpty : function(current_step){
            var empty = $(current_step).find('input[required]').filter(function() {
                return this.value === "";
            });

            if(empty.length) {
               return false;
            }

            return true;
        },

        removeErrorState : function(e) {
            var self = $(e.currentTarget);
            self.parent().removeClass('state-error');
        }


	}

	GenerateIras.init();
})();